////-------------------------------------------------
////* File Name   : init.s
////* Code Name   : init
////* Date        : 2008/6/6 �U�� 01:42:05
////* Designed by : Bruce Lee
////* Description : 1. Test the SPI interrupt mode.
////* Revision    : 
////-------------------------------------------------
/*
    start_spi_isr_mode.c

    2008-06-10	Albert Yu
		first release
*/
#include "chipset.h"

#define LED_DELAY	    0x8FFFF

#define SPI_BLOCK_SIZE      (64*1024)
#define SPI_THRESHOLD       16
#define SPI_BAUD_DIV        4

//#define GLOBAL_R7

.globl _start
_start:	
                                                // Vector Address
                B       ResetHandler            // entry for power on reset
                B       UndefHandler            // Undefined instruction
                B       SWIHandler              // SWI
                B       pAbortHandler           // Prefetch abort
                B       dAbortHandler           // Data abort
                NOP											                // Reserved vector
                B       IRQHandler              // IRQ
                B       FIQHandler              // FIQ

UndefHandler:                                   // Undefined Instruction exception
                MOVS    pc,lr                   // do nothing and return immediately
                B       UndefHandler

SWIHandler:                                     // Software Interrupt exception
                MOVS    pc,lr                   // do nothing and return immediately
                B       SWIHandler
                
pAbortHandler:                                  // Prefetch Abort exception
                SUBS    pc,lr,#4                // do nothing and return immediately
                B       pAbortHandler
                
dAbortHandler:                                  // Data Abort exception
	            MOV     r0, lr
	            BL      _PrintHex
	            LDR     r0, [r0]
	            BL      _PrintHex
                LDR     r6, =0x05010000         // GPIO base address
                LDR     r0, =0x0248
                STR     r0, [r6,#0x04]          // set GPIO all output direction
                LDR     r0, =0x08
    showAbortLED:
                STR     r0, [r6]
                EOR     r0, r0, #0x08
                LDR     r1, =0x8FFFF
    wait_loop:     
                SUBS    r1, r1, #1
                BNE     wait_loop               
                B       showAbortLED

FIQHandler:                                     // FIQ exception
                SUBS    pc,lr,#4                // do nothing and return immediately
                B       FIQHandler

IRQHandler:
                SUB     lr, lr, #4              // calculate the return address
                STMFD   sp!, {lr}               // push return address to stack
                MRS     r14, SPSR               // copy SPSR_IRQ r14
                #ifndef GLOBAL_R7
                STMFD   sp!, {r0-r7, r14}       // push work registers and SPSR_IRQ to stack
                #else
                STMFD   sp!, {r0-r6, r14}       // push work registers and SPSR_IRQ to stack
                #endif
                
                LDR     r1, =0x05040060         // r1 = SPI Tx/Rx base address

                #ifndef GLOBAL_R7
                LDR     r7, gSdramDestAddr      // r7 = SDRAM destination address
                #endif
                
                MOV     r5, #SPI_THRESHOLD      // set read loop counter

#if 1
spi_loop:       LDR     r4,[r1]                 // r4 = data in SPI Rx FIFO : byte 0
                MOV     r3,r4                   // byte 0
                LDR     r4,[r1]                 // byte 1
                ORR     r3,r3,r4,LSL#8          // byte 1, byte 0
                LDR     r4,[r1]                 // byte 2
                ORR     r3,r3,r4,LSL#16         // byte 2,  byte 1, byte 0
                LDR     r4,[r1]                 // byte 3
                ORR     r3,r3,r4,LSL#24         // byte 3, byte 2,  byte 1, byte 0
                STR     r3,[r7]                 // write r4 to SDRAM
                ADD     r7,r7,#4                // increment SDRAM address
                SUBS    r5,r5,#4                // decrement loop counter
                BNE     spi_loop                // read more
#else
spi_loop:       LDR     r4,[r1]                 // r4 = data in SPI Rx FIFO
				STRB	r4,[r7]                 // write r4 to SDRAM
				ADD		r7,r7,#1                // increment SDRAM address
				SUBS	r5,r5,#1                // decrement read loop counter
                BNE     spi_loop                // read more
#endif

                #ifndef GLOBAL_R7
                STR     r7, gSdramDestAddr      // increase SDRAM destination address
                #endif

                #ifndef GLOBAL_R7
                LDMFD   sp!, {r0-r7, r14}       // restore work registers and spsr_IRQ
                #else
                LDMFD   sp!, {r0-r6, r14}       // restore work registers and spsr_IRQ
                #endif
                MSR     SPSR, r14        	    // restore SPSR
                LDMFD   sp!, {pc}^              // return from IRQ

/*
 *************************************************************************
 *
 * Startup Code (reset vector)
 *
 * 1.  disable all interrupts in Interrupt Controller
 * 2.  init debug LED, and light debug LED
 * 3.  init SDRAM
 * 4.  set stack for Supervisor mode with sp = 0x0000_1000
 * 5.  init BSS segment
 * 6.  init SPI controller
 * 7.  init SPI flash
 * 8.  load Manufacture/Test program or firmware in SPI flash to SDRAM
 * 9.  branch to real SRAM address
 * 10. remap
 * 11. jump to SDRAM address 0x00000000 to run Manufacture/Test program or firmware
 *
 *************************************************************************
 */

/*
 * Note: armboot_end is defined by the (board-dependent) linker script -- armboot.lds
 */	
.globl _armboot_end
_armboot_end:
            	.word armboot_end
	
.globl _bss_end
_bss_end:
            	.word bss_end

#ifndef GLOBAL_R7
.global gSdramDestAddr
gSdramDestAddr:
                .word   0
#endif

programLen:
                .word   0

dAbortStr:
                .string "Data Abort!"
                .align
                

ResetHandler:                                 // Reset exception & main code
                // initialize INTC
                LDR     r1,=0x05050000        // r1 = INTC base address
                MOV     r0,#0x01
                STR     r0,[r1]               // IRQ_INTEN_L = 0x01 (Enable SPI interrupt)
                MOV     r0,#0x00
                STR     r0,[r1,#0x08]         // IRQ_INTMASK_L = 0x00
                MOV     r0,#0x00
                STR     r0,[r1,#0xC0]         // FIQ_INTEN = 0x00
                MOV     r0,#0x00
                STR     r0,[r1,#0xC4]         // FIQ_INTMASK = 0x00
                
                //------ set GPIO port ------
                LDR     r6,=0x05010000        // GPIO port A base address
                LDR     r1,=0x0248
                STR     r1,[r6,#0x04]         // set GPIO port A all output direction

/*===========================================================================*/
/* Init stack                                                                */
/*===========================================================================*/
                LDR     r2, _bss_end
                MRS     r0, cpsr
                BIC     r0, r0, #0x1F
                ORR     r1, r0, #0x12	/* IRQ Mode */
                MSR     cpsr_c, r1
                ADD 	r2, r2, #0x80
                MOV     sp, r2          /* IRQ stack size := 128 bytes */

                MRS     r0, cpsr
                BIC     r0, r0, #0x1F
                ORR     r1, r0, #0x13	/* Supervisor Mode */
                MSR     cpsr_c, r1
                LDR		sp, =0xFF4      /* Set stack pointer to end of SRAM alias address */

/*===========================================================================*/
/* Init BSS segment                                                          */
/*===========================================================================*/
                /*
                * r0 = start address
                * r1 = end address
                * r2 = 0 to write to BSS segment
                */
            	LDR     r0, _armboot_end
            	LDR     r1, _bss_end
            	LDR     r2, = 0
clear_bss:
            	STR	    r2, [r0], #4
            	CMP     r0, r1
            	BNE     clear_bss
                
                //------ enable interrupt ------
                MRS     r0,CPSR                 // read CPSR into r0
                BIC     r0,r0,#0x80             // clear bit 7
                MSR     CPSR_c,r0               // write modified value back 

                BL      InitDbgUART

                // initialize SPI
                LDR     r2,=0x05040000          // r2 = SPI base address
                MOV     r0,#0x00
                STR     r0,[r2,#0x08]           // disable SPI
                MOV     r0,#SPI_BAUD_DIV
                STR     r0,[r2,#0x14]           // set baud rate divisor

                //BL      mx_write_test

#if 0
                LDR     r0, =programLen         
//                LDR     r1, =0x2000 + 8         // header
                 LDR     r1, =0x2000 + 16         // header
                 LDR     r2, =4
                BL      mx_read_data            // read program length field in header

                LDR     r0, programLen
                BL      _PrintHex
#endif
                
                LDR     r0, =0x1000000          // sdramDestAddr
                LDR     r1, =0x2020             // flashSrcAddr
                //LDR     r2, programLen          // len
                LDR     r2, =128*1024
                BL      mx_read_data            // read program in SPI flash to SDRAM

                BL      sum_check

#if 0   
                //------ remap SDRAM ------
                LDR     r1,=0x05000000          // r1 = remap base address
                MOV     r0,#0x01
                STR     r0,[r1,#0x08]           // remap mode enable
                MOV     pc,#0x00                // jump to address 0x00000000
#else
                /*
                * Branch to real SRAM address
                */
                LDR     r0, =S2E_SRAM_BASE
                LDR     r1, _remap
                ADD     r0, r0, r1
                MOV     pc, r0

                /*
                * Remap
                */
remap:
                LDR	    r3, =S2E_REMAP_BASE	
                MOV	    r4, #0x01	            // Set REMAP bit
                STR     r4, [r3, #REMAP_MODE]
                NOP	
                NOP	
                NOP	
                NOP	
                NOP

                /* Wait for completion of Remap */
check_remap:
                LDR     r4, [r3]
                TST     r4, #0x01               // only check bit 0
                BNE     check_remap

                /*
                * Branch to 0x0000_0000 to run program in SDRAM
                */
                LDR     PC,=0

_remap: .word   remap
#endif


#if 1
InitDbgUART:
                //--- initialize DW UART ---
                LDR     r0,=0x05030000          // base address of DW UART
                MOV     r1,#0x83        
                STR     r1,[r0,#0x0C]
                MOV     r1,#8
                STR     r1,[r0,#0x00]
                MOV     r1,#0x00
                STR     r1,[r0,#0x04]
                MOV     r1,#0x03
                STR     r1,[r0,#0x0C]           // LCR: N, 8, 1
                MOV     r1,#0x07
                STR     r1,[r0,#0x08]           // FCR: FIFO enable, Reset TX & RX FIFO
                MOV     r1,#0x03
                STR     r1,[r0,#0x10]           // MCR: DTR & RTS On

                MOV	    pc, lr
#endif

/*======================================================================*/
/* int mx_read_data(UINT32 sdramDestAddr, UINT32 flashSrcAddr, int len) */
/*======================================================================*/
.globl mx_read_data
mx_read_data:
                STMFD   sp!,{r3-r10, lr}        // push work registers to stack

                MOV     r7, r0                  // sdramDestAddr
                MOV     r4, r1                  // flashSrcAddr
                ADD     r8, r7, r2              // sdramFinalAddr = sdramDestAddr + len
                MOV     r3, r2                  // ret = len

                LDR     r10, =0x05040000        // r10 = SPI base address
                MOV     r0, #0x00
                STR     r0, [r10, #0x08]        // disable SPI
                LDR     r0, =0x73C7
                STR     r0, [r10]               // set EEPROM Read mode

#if 1   /* ------------------- Polling Mode -------------------------*/
                MOV     r0, #0x00
                STR     r0, [r10, #0x2C]        // disable all SPI interrupts
                LDR     r0, =0
                STR     r0, [r2,#0x1C]          // set Rx FIFO threshold = 0

_mx_read_block:
                LDR     r0, =0x5A5A
                BL      _PrintHex

                MOV     r0, #0x00
                STR     r0, [r10, #0x08]        // disable SPI

                MOV     r0, r2                  // read_cnt = len            
                CMP     r0, #SPI_BLOCK_SIZE
                BLE     _set_read_cnt           // if (read_cnt > SPI_BLOCK_SIZE)
                LDR     r0, =SPI_BLOCK_SIZE     //      read_cnt = SPI_BLOCK_SIZE

_set_read_cnt:
                ADD     r5, r7, r0              // sdramEndAddr = gSdramDestAddr + read_cnt
                SUB     r0, r0, #1
                STR     r0, [r10, #0x04]        // set number of bytes to read
                BL      _PrintHex

                BL      spi_issue_read_cmd

#if 1
_read_polling:
                LDR		r0, [r10, #0x24]        // Rx FIFO Level register
                CMP     r0, #4                  // check if 1 word has been read
                BLT		_read_polling
                LDR     r4,[r10, #0x60]         // r4 = data in SPI Rx FIFO : byte 0
                MOV     r0,r4                   // byte 0
                LDR     r4,[r10, #0x60]         // byte 1
                ORR     r0,r0,r4,LSL#8          // byte 1, byte 0
                LDR     r4,[r10, #0x60]         // byte 2
                ORR     r0,r0,r4,LSL#16         // byte 2,  byte 1, byte 0
                LDR     r4,[r10, #0x60]         // byte 3
                ORR     r0,r0,r4,LSL#24         // byte 3, byte 2,  byte 1, byte 0
                STR     r0,[r7]                 // write r4 to SDRAM
                ADD     r7,r7,#4                // increment SDRAM address
                CMP     r7, r5                  // (sdramDestAddr < sdramEndAddr) ?
                BLT     _read_polling            // read more data if not end
#else
_read_polling:
                LDR		r0, [r10, #0x28]        // status register
#if 1
                STMFD   sp!, {r0-r3}
                BL      PrintHex
                LDMFD   sp!, {r0-r3}
#endif                
                TST		r0, #0x08		        // test if bit 3 (Rx FIFO Not Empty) is set
                BEQ		_read_polling
                LDR     r0, [r10, #0x60]        // r0 = data in SPI Rx FIFO
				STRB	r0, [r7]                // write r0 to SDRAM
				ADD		r7, r7, #1              // increment SDRAM address
                CMP     r7, r5                  // (sdramDestAddr < sdramEndAddr) ?
                BLT     _read_polling           // read more data if not end
#endif

#else   /* ------------------- ISR Mode -----------------------------*/

                CMP     r2, #32                 // (len > SPI_RX_FIFO_SIZE) ?
                BGT     _mass_read

                // len <= SPI_RX_FIFO_SIZE
                ADD     r5, r7, r2              // sdramEndAddr = sdramDestAddr + read_cnt                
                SUB     r0, r2, #1
                STR     r0, [r10, #0x04]        // set number of bytes to read
                             
                MOV     r0, #0x00
                STR     r0, [r10, #0x2C]        // disable all SPI interrupts
                LDR     r0,=0
                STR     r0,[r2,#0x1C]           // set Rx FIFO threshold = 0

                BL      spi_issue_read_cmd

#if 1
_read_not_ok:
                LDR		r0, [r10, #0x24]        // Rx FIFO Level register
                CMP     r0, r2                
                BNE		_read_not_ok
_read_more:
                LDR     r4,[r10, #0x60]         // r4 = data in SPI Rx FIFO : byte 0
                MOV     r3,r4                   // byte 0
                LDR     r4,[r10, #0x60]         // byte 1
                ORR     r3,r3,r4,LSL#8          // byte 1, byte 0
                LDR     r4,[r10, #0x60]         // byte 2
                ORR     r3,r3,r4,LSL#16         // byte 2,  byte 1, byte 0
                LDR     r4,[r10, #0x60]         // byte 3
                ORR     r3,r3,r4,LSL#24         // byte 3, byte 2,  byte 1, byte 0
                STR     r3,[r7]                 // write r4 to SDRAM
                ADD     r7,r7,#4                // increment SDRAM address
                CMP     r7, r5                  // (sdramDestAddr < sdramEndAddr) ?
                BLT     _read_more              // read more data if not end
#else
_read_polling:
                LDR		r0, [r10, #0x28]        // status register                       
                TST		r0, #0x08		        // test if bit 3 (Rx FIFO Not Empty) is set
                BEQ		_read_polling
                LDR     r0, [r10, #0x60]        // r0 = data in SPI Rx FIFO 
				STRB	r0, [r7]                // write r0 to SDRAM  
				ADD		r7, r7, #1              // increment SDRAM address
                CMP     r7, r5                  // (sdramDestAddr < sdramEndAddr) ?
                BLT     _read_polling           // read more data if not end
#endif                
                B       _end_mx_read_data
                
_mass_read:
                LDR     r0, =SPI_THRESHOLD - 1
                STR     r0, [r10, #0x1C]        // set Rx FIFO threshold
                MOV     r0, #0x18
                STR     r0, [r10, #0x2C]        // enable RX interrupt

                #ifndef GLOBAL_R7
                STR     r7, gSdramDestAddr      // gSdramDestAddr = sdramDestAddr
                #endif

                /* -----------------------*/
                /* Read SPI data to SDRAM */
                /* -----------------------*/
_mx_read_block:
                MOV     r0, #0x00
                STR     r0, [r10, #0x08]        // disable SPI

                MOV     r0, r2                  // read_cnt = len
                CMP     r0, #SPI_BLOCK_SIZE
                BLE     _set_read_cnt           // if (read_cnt > SPI_BLOCK_SIZE)
                LDR     r0, =SPI_BLOCK_SIZE     //      read_cnt = SPI_BLOCK_SIZE

_set_read_cnt:
                ADD     r5, r7, r0              // sdramEndAddr = gSdramDestAddr + read_cnt
                SUB     r0, r0, #1
                ORR     r0, r0, #(SPI_THRESHOLD - 1)    // align to threshold boundary to avoid last interrupt being not triggered
                STR     r0, [r10, #0x04]        // set number of bytes to read               

                BL      spi_issue_read_cmd

_wait_finish:
                #ifndef GLOBAL_R7
                LDR     r7, gSdramDestAddr
                #endif
#if 0
                STMFD   sp!, {r0-r3}
                MOV     r0, r7
                BL      PrintHex
                MOV     r0, r5
                BL      PrintHex
                LDMFD   sp!, {r0-r3}
#endif
                CMP     r7, r5                  // (gSdramDestAddr < sdramEndAddr) ?
                BLT     _wait_finish            // wait for Flash read finished

#endif  /* ---------------------------------------------------------*/


#if 0
                MOV     r0, #0x00
                STR     r0, [r10, #0x08]        // disable SPI

_wait_not_busy:
                LDR		r0, [r10, #0x28]        // status register
                TST		r0, #0x01		        // test if bit 0 (BUSY) is set
                BNE		_wait_not_busy
#endif
                MOV     r0, #0x00
                STR     r0, [r10, #0x10]        // disable slave 0 (CS#0 high)

                MOV     r0, r2                
                SUB     r2, r2, #SPI_BLOCK_SIZE // len -= SPI_BLOCK_SIZE
                ADD     r4, r4, #SPI_BLOCK_SIZE // flashSrcAddr += SPI_BLOCK_SIZE

                CMP     r0, #SPI_BLOCK_SIZE
                BGT     _mx_read_block          // read move blocks if (len > 0)

                MOV     r0, #0x00
                STR     r0, [r10, #0x2C]        // disable all SPI interrupts

_end_mx_read_data:
                MOV     r0, r3                  // return (ret)
                LDMFD   sp!, {r3-r10, lr}       // restore work registers and spsr_IRQ
                MOV     pc, lr


spi_issue_read_cmd:
                MOV     r0, #0x01
                STR     r0, [r10, #0x08]        // enable SPI
                
                // write command & address to the Flash through SPI
                MOV     r0, #0x0B
                STR     r0, [r10, #0x60]        // fast read command
                
                MOV		r0, r4, LSR#16
                STRB    r0, [r10, #0x60]        // address bit 23-16
                MOV		r0, r4, LSR#8
                STRB    r0, [r10, #0x60]        // address bit 15-8
                STRB    r4, [r10, #0x60]        // address bit 7-0
                MOV     r0, #0xAA
                STRB    r0, [r10, #0x60]        // dummy data

                MOV     r0, #0x01
                STR     r0, [r10, #0x10]        // enable slave 0 (CS#0 low and start transmission) 
                
                MOV     pc, lr


.globl mx_write_test
mx_write_test:
                STMFD   sp!,{r0-r10, lr}             // push work registers to stack

                //BL      mx_read_ID

                LDR     r6,=0x05010000        // GPIO base address

                LDR     r0, =0xFFF
    wait_loop1:
                STMFD   sp!, {r0-r3}
                BL      PrintHex
                LDMFD   sp!, {r0-r3}
                SUBS    r0, r0, #1
                BNE     wait_loop1

                // initialize SPI
                LDR     r10,=0x05040000        // r10 = SPI base address
                MOV     r0,#0x00
                STR     r0,[r10,#0x08]         // disable SPI
                MOV     r0,#0x00
                STR     r0,[r10,#0x2C]         // disable all SPI interrupts
                LDR     r0,=0
                STR     r0,[r10,#0x1C]           // set Rx FIFO threshold
                LDR     r0, =0x71C7
                STR     r0,[r10]
                LDR     r0, =5
                STR     r0,[r10, #0x04]        // set number of bytes to read 
                
                MOV     r0, #0x03
                STR     r0, [r6]
                
                MOV     r0, #0x01
                STR     r0, [r10, #0x08]        // enable SPI
                
                // write command & address to the Flash through SPI
                MOV     r0, #0x01
                STR     r0, [r10, #0x60]        // WRSR
                MOV     r0, #0x04
                STR     r0, [r10, #0x60]        //
                MOV     r0, #0x01
                STR     r0, [r10, #0x10]        // enable slave 0 (CS#0 low and start transmission) 

#if 0
         wait_tx_empty:
                LDR     r0, [r10, #0x28]
                BL      _PrintHex
                TST     r0, #0x04   // Tx FIFO Empty
                BEQ     wait_tx_empty

                MOV     r0, #0x06
                STR     r0, [r6]
#endif

         loop2 : b loop2

                MOV     r0, #0x00
                STR     r0, [r10, #0x10]        // disable slave 0

                MOV     r0, #0x00
                STR     r0, [r10, #0x08]         // disable SPI
                

         loop1 : b loop1       

                BL      mx_write_enable

                MOV     r0, #0x02
                STR     r0, [r6]

                //LDR     r0,=0x71C7            // r3 = settings of ctrl0, Transmit Only mode
                LDR     r0,=0x73C7            // r3 = settings of ctrl0, EEPROM Read mode
                STR     r0,[r10]               // set ctrl0

                MOV     r0,#0x01
                STR     r0,[r10,#0x08]         // enable SPI
                MOV     r0,#0x01
                STR     r0,[r10,#0x10]         // enable slave 0
                
                // write command & address to the Flash through SPI
                LDR     r3,=0x000000        // set address
                MOV     r0,#0x20
                STR     r0,[r10,#0x60]         // sector erase command 
                MOV		r0,r3,LSR#16
                STRB    r0,[r10,#0x60]         // address bit 23-16
                MOV		r0,r3,LSR#8
                STRB    r0,[r10,#0x60]         // address bit 15-8
                STRB    r3,[r10,#0x60]         // address bit 7-0

            	BL      spi_wait_TX_FIFO_empty	/* wait TX FIFO empty */

                MOV     r0, #0x03
                STR     r0, [r6]
            	
            	BL      spi_wait_not_busy		/* wait until not busy */

                MOV     r0, #0x04
                STR     r0, [r6]

            	MOV     r0,#0x00
                STR     r0,[r10,#0x08]       /* disable SPI: halt all serial transfers, also clear TX/RX FIFO */
                STR     r0,[r10,#0x10]       /* CS0# high to start Sector Erase in MX flash */

                LDMFD   sp!,{r0-r10, lr}        // restore work registers and spsr_IRQ
                MOV     pc, lr                  // return to caller

_PrintHex:
                STMFD   sp!, {r0-r3, lr}
                BL      PrintHex
                LDMFD   sp!, {r0-r3, lr}
                MOV     pc, lr
