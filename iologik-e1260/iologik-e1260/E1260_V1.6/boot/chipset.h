/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    chipset.h

    Address map and register definitions of S2E system

    2008-06-10	Albert Yu
		first release
*/

#ifndef CHIPSET_H
#define CHIPSET_H

/*  -------------------------------------------------------------------------------
 *   S2E system registers
 *  ------------------------------------------------------------------------------- 
 */

#define S2E_SRAM_BASE               0x03000000	/* SRAM base address */

#define S2E_MEM_CTRL_BASE           0x04000000	/* AHB slave 2: Registers of memory controller */ 
#define S2E_MAC_BASE                0x04010000	/* AHB slave 3: Ethernet MAC */
#define S2E_AHB_CTRL_BASE           0x04012000
#define S2E_AHB2APB_BASE            0x05000000	/* AHB slave 4: AHB2APB Bridge (Remap inside) */
/* APB slaves */
#define S2E_REMAP_BASE              0x05000000	/* APB slave 9: Remap */
#define S2E_GPIO_BASE               0x05010000	/* APB slave 2: GPIO */
#define S2E_I2C_BASE                0x05020000	/* APB slave 3: I2C */
#define S2E_DW_UART_BASE            0x05030000	/* APB slave 4: DesignWare UART */
#define S2E_SPI_BASE                0x05040000	/* APB slave 1: SPI */
#define S2E_INTC_BASE               0x05050000	/* APB slave 8: Interrupt Controller */
#define S2E_TIMER_BASE              0x05060000	/* APB slave 5: Timer */
#define S2E_WDT_BASE                0x05070000	/* APB slave 6: Watch Dog Timer */
#define S2E_MOXA_UART_BASE          0x05080000	/* APB slave 7: MOXA UART */
#define S2E_CLOCK_SOURCE_BASE		0x05090000

/************************************************************************/
/* APB Device system registers											*/
/* Address Offset definitions                                           */
/************************************************************************/

/* -------------------------------------------------------------------------------
 *  SPI definitions
 * -------------------------------------------------------------------------------
 */
#define SPI_CONTROL0				0x00
#define SPI_CONTROL1				0x04
#define SPI_SSI_ENABLE				0x08
#define SPI_MICROWIRE_CTRL			0x0C
#define SPI_SLAVE_SELECT			0x10
#define SPI_CLOCK_DIV				0x14	/* Fsclk_out = Fssi_clk / SCKDV */
#define SPI_TX_FIFO_THRESHOLD		0x18
#define SPI_RX_FIFO_THRESHOLD		0x1C
#define SPI_TX_FIFO_LEVEL			0x20
#define SPI_RX_FIFO_LEVEL			0x24
#define SPI_STATUS_REG				0x28
#define SPI_INT_MASK				0x2C
#define SPI_INT_STATUS				0x30
#define SPI_RAW_INT_STATUS			0x34
#define SPI_TX_FIFO_OVER_INT_CLR	0x38
#define SPI_RX_FIFO_OVER_INT_CLR	0x3C
#define SPI_RX_FIFO_UNDER_INT_CLR	0x40
#define SPI_MULTI_MASTER_INT_CLR	0x44
#define SPI_INT_CLR					0x48
#define SPI_ID						0x58
#define SPI_VERSION					0x58
#define SPI_DATA					0x60	/* read: access RX FIFO, write: access TX FIFO */


/* Interrupt Mask/Status Register Bits Definition */
#define SPI_INT_RX_FIFO_FULL       0x10
#define SPI_INT_RX_FIFO_OVERFLOW   0x08
#define SPI_INT_RX_FIFO_UDNERFLOW  0x04
#define SPI_INT_TX_FIFO_OVERFLOW   0x02
#define SPI_INT_TX_FIFO_EMPTY      0x01

/* -------------------------------------------------------------------------------
 *  Remap definitions
 * -------------------------------------------------------------------------------
 */
#define REMAP_PAUSE_MODE			0x00
#define REMAP_ID					0x04
#define REMAP_MODE					0x08
#define REMAP_VERSION				0x14

/************************************************************************/
/* AHB Device system registers											*/
/* Address Offset definitions                                           */
/************************************************************************/

/* -------------------------------------------------------------------------------
 *  AHB Controller definitions
 * -------------------------------------------------------------------------------
 */
#define AHB_MASTER1_PRIORITY		0x00
#define AHB_MASTER2_PRIORITY		0x04
#define AHB_DEFAULT_MASTER_ID		0x48
#define AHB_VERSION					0x90

/* -------------------------------------------------------------------------------
 *  SDRAM definitions
 * -------------------------------------------------------------------------------
 */
#define SDRAM_CONFIG                0x00	
#define SDRAM_TIMING0               0x04
#define SDRAM_TIMING1               0x08
#define SDRAM_CONTROL               0x0C
#define SDRAM_REFRESH_INTVL			0x10
#define SDRAM_EXTEND_MODE			0xAC
#define SDRAM_CHIP_SELECT0			0x14	/* CS0 for SRAM */
#define SDRAM_CHIP_SELECT1			0x18	/* CS1 for SDRAM */
#define SDRAM_ADDR_MASK0			0x54
#define SDRAM_ADDR_MASK1			0x58
#define SDRAM_ALIAS0				0x74	/* Alias address for SRAM */
#define SDRAM_ALIAS1				0x78	/* Alias address for SDRAM */
#define SDRAM_REMAP0				0x84	/* Remap address for SRAM */
#define SDRAM_REMAP1				0x88	/* Remap address for SDRAM */

/* -------------------------------------------------------------------------------
 *  SRAM definitions
 * -------------------------------------------------------------------------------
 */
#define SRAM_TIMING0				0x94
#define SRAM_TIMING1				0x98
#define SRAM_TIMING2				0x9C
#define SRAM_CONTROL				0xA4


/* -------------------------------------------------------------------------------
 *  GPIO definitions
 * -------------------------------------------------------------------------------
 */
#define GPIO_DATA            	0x00
#define GPIO_DIRECTION          0x04

/* -------------------------------------------------------------------------------
 *  Watch Dog Timer definitions
 * -------------------------------------------------------------------------------
 */
#define WDT_CTRL                    0x00

/* -------------------------------------------------------------------------------
 *  Interrupt Controllers
 * -------------------------------------------------------------------------------
 */
#define IRQ_INT_ENABLE			0x00			// default 0
#define IRQ_INT_MASK			0x08			// default 0
#define IRQ_INT_FORCE			0x10			// default 1
#define IRQ_RAWSTATUS			0x18			// default 0
#define IRQ_MASKSTATUS			0x28			// default 0
#define IRQ_FINALSTATUS			0x30			// default 0

#define FIQ_INT_ENABLE			0xC0			// default 0
#define FIQ_INT_MASK			0xC4			// default 0
#define FIQ_INT_FORCE			0xC8			// default 1
#define FIQ_RAWSTATUS			0xCC			// default 0
#define FIQ_FINALSTATUS			0xD4			// default 0

#define IRQ_PLEVEL				0xD8
#define IRQ_PR_SPI				0xE8
#define IRQ_PR_GPIO				0xEC
#define IRQ_PR_I2C				0xF0
#define IRQ_PR_UART				0xF4
#define IRQ_PR_MAC				0xF8
#define IRQ_PR_WDT				0xFC
#define IRQ_PR_MOXA_UART		0x100
#define IC_COMP_VERSION			0xE0

#define IRQ_SOURCE              IRQ_RAWSTATUS
#define IRQ_MASK                IRQ_INT_ENABLE

#endif
