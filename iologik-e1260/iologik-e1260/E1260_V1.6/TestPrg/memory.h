/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	memory.h

	Define the memory block and memory list.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-24	Chin-Fu Yang		
		modified		
    
*/

#ifndef _MEMORY_H
#define _MEMORY_H


/*------------------------------------------------------ Macro / Define ----------------------------*/
#define MEM_BUF_BLOCK       64


/*------------------------------------------------------ Structure ----------------------------------*/
struct MsgBlkPtr {
	struct MsgBlkPtr 	*next;	/* next message block pointer */
	struct MsgBlkPtr 	*prev;	/* prev message block pointer */
	uchar 		*base;		/* data block address pointer */
	int			size;		/* maximum length of this block */
	int			rndx;		/* read pointer of data block */
	int			wndx;		/* write pointer of data block */
	ulong		flag;		/* message flag 	      */
};//}__attribute__((packed));
typedef struct MsgBlkPtr	msgstr;
typedef struct MsgBlkPtr 	*msgstr_t;


struct MemListPtr {
	msgstr_t    	head;		/* head of memory list			*/
	msgstr_t    	tail;			/* tail of memory list			*/
	int	    		count;		/* blocks of memory on this list	*/
	int	    		size;		/* block size of this list		*/
};//}__attribute__((packed));
typedef struct MemListPtr  memlist;
typedef struct MemListPtr  *memlist_t;


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void	MemInit(void);
msgstr_t	MemAlloc(int bytes);
void	MemFree(msgstr_t msg);
void Memcpy(void * dest,const void *src,long count);
void Memset(void * s, char c, long count);


#endif /* _MEMORY_H */
