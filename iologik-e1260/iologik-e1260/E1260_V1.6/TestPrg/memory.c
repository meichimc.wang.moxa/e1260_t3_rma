/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	memory.c

	Simple memory management.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-24	Chin-Fu Yang		
		modified				
    
*/

#include "types.h"
#include "memory.h"
#include "global.h"


msgstr	MemMsg[MEM_BUF_BLOCK];
memlist MemFreeList;
msgstr_t M_NULL = 0;


/**	\brief
 *
 *	Initial memory blocks.
 *	
 *
 */
void	MemInit(void)
{
	int	i;
	char	*ptr;

	/* link all memory blocks */
	ptr = (char *)SDRAM_GENERAL_MEMORY_BASE;
	for (i=0;i<MEM_BUF_BLOCK;i++) {
		MemMsg[i].base = ptr;
		MemMsg[i].size = GENERAL_MEMORY_BLOCK_MAX_SIZE;
		ptr += GENERAL_MEMORY_BLOCK_MAX_SIZE;
		if (i != 0){
			MemMsg[i].prev = &(MemMsg[i-1]);
		}
		if (i < (MEM_BUF_BLOCK - 1)){
			MemMsg[i].next = &(MemMsg[i+1]);
		}
	}

	/* init the value of MemFreeList */
	MemFreeList.head = &MemMsg[0];
	MemFreeList.tail = &MemMsg[MEM_BUF_BLOCK - 1];
	MemFreeList.count = MEM_BUF_BLOCK;
	MemFreeList.size = GENERAL_MEMORY_BLOCK_MAX_SIZE;
	MemFreeList.head->prev = M_NULL;
	MemFreeList.tail->next = M_NULL;
	
	_armboot_real_end = _bss_end + CONFIG_STACKSIZE;	// End address of boot loader 
}


/**	\brief
 *
 *	allocate a memory block to user
 *
 *	\param[in]	bytes : the free memory size you want
 *
 *	\retval 		pointer of the memory block
 */
msgstr_t	MemAlloc(int bytes)
{
	msgstr_t	msg;
	
	if (MemFreeList.count <= 0){
		return(M_NULL);
	}
	if (MemFreeList.size < bytes){
		return(M_NULL);
	}
	msg = MemFreeList.head;	// get one free memory block
	/* adjust the MemFreeList */
	MemFreeList.head->next = msg->next;
	MemFreeList.head->prev = NULL;
	MemFreeList.count--;
	msg->prev = M_NULL;
	msg->next = M_NULL;
	msg->wndx = msg->rndx = 0;
	msg->flag = 0;
	return(msg);
}


/**	\brief
 *
 *	release a memory block
 *
 *	\param[in]	msg : the memory block you want to release 
 *
 */
void	MemFree(msgstr_t msg)
{
	msg->prev = MemFreeList.tail;
	msg->next = M_NULL;
	MemFreeList.tail->next = msg;
	MemFreeList.count++;
}


/**	\brief
 *
 *	memory copy 
 *
 *	\param[in]	*dest : destination address
 *	\param[in]	*src : source address
 *	\param[in]	count : How many bytes that you want to copy
 *
 */
void Memcpy(void * dest,const void *src,long count)
{
	char *tmp = (char *) dest;
	char *s = (char *) src;

	while (count--){
		*tmp++ = *s++;
	}
}


/**	\brief
 *
 *	Set the value to memory
 *
 *	\param[in]	*s : source address
 *	\param[in]	c : character you want to set to memory
 *	\param[in]	count : How many bytes that you want to set
 *
 */
void Memset(void * s, char c, long count)
{
	char *xs = (char *) s;

	while (count--){
		*xs++ = c;
	}
}

