/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	Arp.c

	Functions of ARP protocol.

	2008-06-10	Chin-Fu Yang	
		new release
	2008-10-15	Chin-Fu Yang		
		modified		
 	2009-08-03	Nolan Yu		
		modified		
   
*/



#include "types.h"
#include "chipset.h"
#include "timer.h"
#include "moxauart.h"
#include "memory.h"
#include "lib.h"
#include "mac.h"
#include "global.h"
#include "arp.h"
#include "proc232.h"

long  ArpTableNo;	// total records in ARP table
struct addrtab ArpTable[DARP_MAX_TABLE];





 /**	\brief
 *
 *	Remove a record in ARP table.
 *
 *	\param[in]	ip : MAC port
 *
 */
static void ArpRemove(ulong ip)
{
	int i,j;

	for (i=0;i<ArpTableNo;i++) {
		if (ArpTable[i].ip == ip){
			break;
		}
	}
	// 
	for (j=i;j<ArpTableNo - 1;j++){
		ArpTable[j] = ArpTable[j+1];
	}
	ArpTableNo--;
}




 /**	\brief
 *
 *	Adjute ArpTable
 *
 *	\retval 		push itself again
 */
int ArpTimeOut(void)
{
	int i = 0;

	// Scan all the records in ARP Table to remove ARP record which is timeout.
	while (i < ArpTableNo) {
		if ((long)(Gsys_msec - ArpTable[i].timeout) >= 0){
			ArpRemove(ArpTable[i].ip);
		}else{
			i++;
		}
	}
	
	return ( TimerOut(ArpTimeOut,ARP_TABLE_ADJUST_TIME) );	// push self again.
}


 

 /**	\brief
 *
 *	Response the ARP request packet which is sent to us
 *
 *	\param[in]	msg : ARP packet
 *
 */
static void ArpReply(msgstr_t msg)
{
	arpinfo_t   arp;	// ARP Packet header
	int	    i;		
	uchar   *ptr;

	arp = (arpinfo_t)&msg->base[14];
	ptr = msg->base;
	for (i=0;i<6;i++) {
	    *ptr++ = arp->sha[i];
	    arp->tha[i] = arp->sha[i];
	}
	for (i=0;i<6;i++) {
	    *ptr++ = MacSrc[G_LanPort][i];
	    arp->sha[i] = MacSrc[G_LanPort][i];
	}
	arp->op = DARP_OP_REPLY;
	arp->tpa = arp->spa;
	arp->spa = NicIp[G_LanPort];
//	Printf("NicArpReplay\r\n");
	mac_send(G_LanPort, msg);
}
extern char	MAC_GEN_Mac[6];
static void ArpReply1(msgstr_t msg)
{
	arpinfo_t   arp;	// ARP Packet header
	int	    i;		
	uchar   *ptr;

	arp = (arpinfo_t)&msg->base[14];
	ptr = msg->base;
	for (i=0;i<6;i++) {
	    *ptr++ = arp->sha[i];
	    arp->tha[i] = arp->sha[i];
	}
	for (i=0;i<6;i++) {
	    *ptr++ = MAC_GEN_Mac[i];
	    arp->sha[i] = MAC_GEN_Mac[i];
	}
	arp->op = DARP_OP_REPLY;
	arp->tpa = arp->spa;
	arp->spa = MAC_GEN_IP;
	mac_send(G_LanPort, msg);
}


 

 /**	\brief
 *
 *	Init ARP Table
 *
 */
void	ArpInit(void)
{
	ArpTableNo = 0;
	TimerOut(ArpTimeOut,ARP_TABLE_ADJUST_TIME);
}





 /**	\brief
 *
 *	Add an entry to ARP table
 *
 *	\param[in]	ip : IP
 *	\param[in]	*addr : MAC address
 *
 *	\retval 		error code
 */
int	ArpAdd(ulong ip,uchar *addr)
{
	int	i;

	for (i=0;i<ArpTableNo;i++) {
	    if (ArpTable[i].ip == ip) {
		ArpTable[i].timeout = (Gsys_msec + ARP_TABLE_TIMEOUT);
		return(0);
	    }
	}
	if (ArpTableNo >= DARP_MAX_TABLE){
	    return(-1);
	}
	ArpTable[ArpTableNo].ip = ip;
	ArpTable[ArpTableNo].timeout = (Gsys_msec + ARP_TABLE_TIMEOUT);
	for (i=0;i<6;i++){
	    ArpTable[ArpTableNo].netaddr[i] = addr[i];
	}
	ArpTableNo++;
	return(0);
}


 
 /**	\brief
 *
 *	Process ARP packet
 *
 *	\param[in]	msg : a packet to process
 *
 */
void	ArpReceive(msgstr_t msg)
{
	arpinfo_t   arp;

	arp = (arpinfo_t)&msg->base[14];
	if (arp->tpa != NicIp[G_LanPort] && arp->tpa != MAC_GEN_IP) {
		MemFree(msg);
		return;
	}

	ArpAdd(arp->spa,arp->sha);
	if (arp->op == DARP_OP_REQUEST){
		if(arp->tpa == NicIp[G_LanPort])
		    ArpReply(msg);
		else if(arp->tpa == MAC_GEN_IP)
			ArpReply1(msg);
		else
			MemFree(msg);
	}else{
	    MemFree(msg);
	}
	
}




 /**	\brief
 *
 *	Search ethernet address by input ip address
 *
 *	\param[in]	ip : IP
 *
 *	\retval 		error code
 */
int	ArpSearch(ulong ip)
{
	int	    i;
	int	    len;
	arpinfo_t   arp;
	msgstr_t    msg;
	uchar   *ptr;

	for (i=0;i<ArpTableNo;i++) {
	    if (ip == ArpTable[i].ip){
		break;
	    }
	}
	if (i < ArpTableNo){
	    return(i);
	}
	len = 14 + sizeof(struct arpinfo);
	msg = MemAlloc(64);
	if (msg == NULL){
	    return(-1);
	}
	ptr = msg->base;
	for (i=0;i<6;i++){
	    *ptr++ = 0xff;
	}
	for (i=0;i<6;i++){
	    *ptr++ = MacSrc[G_LanPort][i];
	}
	*(ushort *)ptr = NIC_ARP;
	ptr += 2;
	arp = (arpinfo_t)ptr;
	arp->hrd = DARP_HRD_ETHER;
	arp->pro = DARP_PRO_IP;
	arp->hln = DARP_HLN;
	arp->pln = DARP_PLN;
	arp->op = DARP_OP_REQUEST;
	for (i=0;i<6;i++) {
	    arp->sha[i] = MacSrc[G_LanPort][i];
	    arp->tha[i] = 0x00;
	}
	arp->spa = NicIp[G_LanPort];
	arp->tpa = ip;
	msg->wndx = 64;
//	msg->wndx = len & 0xffff;
//	Printf("len = %d",len);
	mac_send(G_LanPort, msg);
	return(-2);
}
