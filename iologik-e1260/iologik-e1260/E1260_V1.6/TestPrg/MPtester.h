/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    mptester.h

    Header file of mptester.c

    2009-05-12	Nolan Yu
		new release
*/

#define mp_port 0

#define mp_tran_OK		0
#define mp_tran_Fail	1
#define mp_command_fail	2
#define mp_time_out		3
#define mp_NG			4

#define MP_DO	0
#define MP_RY	1
#define MP_LD	2
#define MP_GI	3
#define MP_GO	4

#define	LED_GREEN	0x1
#define LED_RED		0x2
#define BUZZER		0x4

void mptester_communication_init(void);
void mptester_communication_stop(void);
UINT8 tr_mptester(UINT8 parameter,UINT16* channel);
UINT8 mptest_DO(UINT16 parameter);
UINT8 mptest_relay(UINT16 parameter);
UINT8 mptest_led(UINT16 parameter);
UINT8 mptest_GPIO_input(UINT16* parameter);
UINT8 mptest_GPIO_output(UINT16 parameter);
