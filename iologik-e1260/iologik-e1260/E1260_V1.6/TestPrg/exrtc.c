/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	exrtc.c

	Functions for external Real-Time Clock. 
	Clock source from emulation of GPIO.

	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified	
		
*/


#include "types.h"
#include "chipset.h"
#include "global.h"
#include "exrtc.h"
#include "gpio.h"

uchar const sys_daysofmonth[12]={31,28,31,30,31,30,31,31,30,31,30,31};
uchar *const DayString[7] = {"Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};



/**	\brief
 *
 *	pull-up RESET line , start transfer
 *
 */

/*		//Disable RTC - Jerry Wu
void RTCTransferStart(void)
{
	GpioWriteData(PIO_EXRTC_RST, 1);
//	sleep(5);		// GPIO is too slow, so we should not delay.
}
*/


/**	\brief
 *
 *	pull-down RESET line , stop transfer
 *
 */

/*		//Disable RTC - Jerry Wu
void RTCTransferStop(void)
{
	GpioWriteData(PIO_EXRTC_RST, 0);
//	sleep(5);		// GPIO is too slow, so we should not delay.
}
*/


/**	\brief
 *
 *	write a byte
 *
 * 	\param[in]	data : byte to write
 *
 */
/*		//	Jerry Wu
void RTCWriteRegister(uchar data)
{
    	int i;

	GpioSetDir(PIO_EXRTC_DATA, PIO_OUT);
     	// write data byte
	for (i=0; i<8; i++){
		GpioWriteData(PIO_EXRTC_SCLK, 0);
			if((data >> i) & 0x01){					// get command bit
				GpioWriteData(PIO_EXRTC_DATA, 1);
			}else{
				GpioWriteData(PIO_EXRTC_DATA, 0);
			}
		GpioWriteData(PIO_EXRTC_SCLK, 1);
	//		sleep(5);		// GPIO is too slow, so we should not delay.
	}
	
	GpioWriteData(PIO_EXRTC_SCLK, 0);
	GpioSetDir(PIO_EXRTC_DATA, PIO_IN);
}
*/



/**	\brief
 *
 *	read a byte
 *
 *	\retval 		a byte of data
 */
/*
uchar RTCReadRegister(void)
{
	int i;
	uchar data=0;

	GpioSetDir(PIO_EXRTC_DATA, PIO_IN);
    // read data byte
	for (i=0; i<8; i++){
		GpioWriteData(PIO_EXRTC_SCLK, 0);
		if((GpioReadInputReg() & (0x1 << PIO_EXRTC_DATA)) != 0){
			data |= 0x01 << i;
		}
		GpioWriteData(PIO_EXRTC_SCLK, 1);
	//		sleep(5);		// GPIO is too slow, so we should not delay.
	}
	
	GpioWriteData(PIO_EXRTC_SCLK, 0);
	return data;
}
*/



/**	\brief
 *
 *	A complete writing process.
 *
 * 	\param[in]	command : RTC Command
 * 	\param[in]	data : byte to write
 *
 *//*		//	Jerry Wu
void RTCWrite(uchar command, uchar data)
{
	RTCTransferStart();
	RTCWriteRegister(command);
	RTCWriteRegister(data);
	RTCTransferStop();
}
*/


/**	\brief
 *
 *	A complete reading process.
 *
 *	\retval 		a byte of data
 *//*		// Jerry Wu
uchar RTCRead(uchar command)
{
	char data=0;

	RTCTransferStart();
	RTCWriteRegister(command);
	data = RTCReadRegister();
	RTCTransferStop();
	return data;
}
*/


/*************************************************************************************/
/*               							RTC DISPLAY FUNCTIONS         			                      */
/*************************************************************************************/

/**	\brief
 *
 *	External RTC initialization
 *
 */
/*
void ex_rtc_Init(void)
{
	struct rtc_time btime;

    btime.t_sec = RTCRead(RTC_SECONDS_R);
	if((btime.t_sec & 0x80) != 0){
	    	RTCWrite(RTC_PROTECT_W,0);		// Disable Write Protect
		RTCWrite(RTC_SECONDS_W,0);		// Enable OSC
		RTCWrite(RTC_PROTECT_W,0x80);   // Enable Write Protect
	}
}
*/

/**	\brief
 *
 *	Get RTC time information and store in memory
 *
 *	\param[in]	struct rtc_time *timep : store the RTC register data here
 *
 *//*		//	Jerry Wu
void	ex_rtc_gettime(struct rtc_time *timep)
{
	uchar data;
	uchar Year, Month, Date, Day;
	uchar Hours, Minutes, Seconds;

	Year = RTCRead(RTC_YEAR_R);
	Month = RTCRead(RTC_MONTH_R);
	Date = RTCRead(RTC_DATE_R);
	Day = RTCRead(RTC_DAY_R);
	Hours = RTCRead(RTC_HOURS_R);
	Minutes = RTCRead(RTC_MINUTES_R);
	Seconds = RTCRead(RTC_SECONDS_R);

	data = Year;
	timep->t_year = ((data & 0xf0) >> 4)*10 + (data & 0x0f) + 2000;		// +2000 is from NP5150 BIOS v1.5
	data = Month;
	timep->t_mon = ((data & 0x10) >> 4)*10 + (data & 0x0f);
	data = Date;
	timep->t_mday = ((data & 0x30) >> 4)*10 + (data & 0x0f);
	data = Day;
	timep->t_wday = (data & 0x07) - 1;
	data = Hours;
	timep->t_hour = ((data & 0x70) >> 4) * 10 + (data & 0x0f);
	data = Minutes;
	timep->t_min = ((data & 0x70) >> 4) * 10 + (data & 0x0f);
	data = Seconds;
	timep->t_sec = ((data & 0x70) >> 4) * 10 + (data & 0x0f);
}
*/




/**	\brief
 *
 *	Set RTC time information to each register.
 *
 *	\param[in]	struct rtc_time *timep : The time information we will write to.
 *
 *//*		//	Jerry Wu
void	ex_rtc_settime(struct rtc_time *timep)
{
	uchar Hours,Minutes,Seconds;
	uchar Year,Month,Date,Day;

	timep->t_year -= 2000;		// add from NP5150 v1.5 bios
	Year = ((timep->t_year/10) << 4) | timep->t_year%10;
	Month = ((timep->t_mon/10) << 4) | timep->t_mon%10;
	Date = ((timep->t_mday/10) << 4) | timep->t_mday%10;
	Day = timep->t_wday;

	Hours = ((((timep->t_hour/10) << 4)) & 0xf0) | ((timep->t_hour%10) & 0x0f);  // 24 mode
	Minutes = (((timep->t_min/10) << 4) & 0xf0) | ((timep->t_min%10)&0x0f);
	Seconds = (((timep->t_sec/10) << 4) & 0xf0) | ((timep->t_sec%10)&0x0f);

	RTCWrite(RTC_PROTECT_W,0);   // Disable Write Protect
	RTCWrite(RTC_YEAR_W, Year);
	RTCWrite(RTC_MONTH_W, Month);
	RTCWrite(RTC_DATE_W, Date);
	RTCWrite(RTC_DAY_W,Day);
	RTCWrite(RTC_HOURS_W,Hours);
	RTCWrite(RTC_MINUTES_W,Minutes);
	RTCWrite(RTC_SECONDS_W,Seconds);
	RTCWrite(RTC_PROTECT_W,0x80);   // Enable Write Protect
}
*/



/**	\brief
 *
 *	Get the index of day of week
 *
 *	\param[in]	struct rtc_time *timep : store the RTC register data here
 *
 *	\retval 		index of day of week
 */
/*int	ex_rtc_get_day_of_week(struct rtc_time *timep)
{
// For Faraday FA526
	int	i, n, m, day;

	day = 0;
	n = timep->t_year - 2000;
	for ( i=0; i<n; i++ ) {
		day += 365;
		if ( (i & 3) == 0 )
			day++;
	}
	m = timep->t_mon - 1;
	for ( i=0; i<m; i++ )
		day += sys_daysofmonth[i];
	if ( (i > 1) && ((n & 3) == 0) )
		day++;
	day += (timep->t_mday - 1);
	day += 6;		// 2000-01-01 is Saturday
	n = (day % 7);
	return( n );
}
*/