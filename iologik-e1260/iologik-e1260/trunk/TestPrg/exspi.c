/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	exspi.c

	2009-08-03	Nolan Yu
		new release
*/

#include "types.h"
#include "chipset.h"
#include "spi_reg.h"
#include "spi.h"
#include "exspi.h"
#include "isr.h"
#include "lib.h"
#include "global.h"
#include "console.h"
#include "gpio.h"
#include "timer.h"

/* It has be defined in spi_asm.S */
extern UINT32 gSpiSdramDestAddr;
extern UINT32 gSpiSdramSrcAddr;
extern UINT32 gSpiSdramEndAddr;

void EXSPI_INIT(void){
	int spi_reg_ctrl0;

	SPI_DISABLE();
	SPI_ENABLE_SLAVE1();
 
    /* Save current serial mode */
    spi_reg_ctrl0 = VPlong(S2E_SPI_BASE + SPI_CONTROL0);
    /* Set 16bit serial mode */
	spi_reg_ctrl0 |= 0xf;
    /* Set SPI serial mode and Transmit & Receive mode */
#if GPIO_CHIP_SELECT
	spi_reg_ctrl0 &= 0xFFFFFCCF;	//ADS1216
#else
	spi_reg_ctrl0 &= 0xFFFFFC0F;	//MAX7301
#endif
    VPlong(S2E_SPI_BASE + SPI_CONTROL0) = spi_reg_ctrl0;
#if GPIO_CHIP_SELECT
	SPI_SET_CLK_DIV(40); //SPI_CLK = 75/2/40 = 937.5KHz
#endif
	SPI_ENABLE();
}

void EXSPI_DIO_INIT(void){
	int spi_reg_ctrl0;

	SPI_DISABLE();
	SPI_ENABLE_SLAVE1();
 
    // ---	Save current serial mode	---
    spi_reg_ctrl0 = VPlong(S2E_SPI_BASE + SPI_CONTROL0);
    // ---	Set 16bit serial mode	---
	spi_reg_ctrl0 |= 0xf;
    // ---	Set SPI serial mode and Transmit & Receive mode	---

  VPlong(S2E_SPI_BASE + SPI_CONTROL0) = spi_reg_ctrl0;
	SPI_SET_CLK_DIV(8); //SPI_CLK = 75/2/8 = 4.6875 MHz (For DIO shift register)
	SPI_ENABLE();
}


void SPI_Delay_10us(void)
{
	int i;
	for (i=0;i<10;i++);
}

#if GPIO_CHIP_SELECT
void SPI_Tx(UINT32 CS,UINT32 data,int length){ //Data length must be less than 32 by Nolan 09-01-08
	GpioWriteData(CS,0);
#if E1240
	sleep(10);
#endif
	do{
		if(length > 16){
			SetSPI_Len(length - 17);
			length = 16;
			SPI_Delay_10us();
			SPI_WRITE_DATA(data >> 16);
			data = (data & 0xffff);
		}
		else{
			SetSPI_Len(length-1);
			length = 0;
			SPI_Delay_10us();
			SPI_WRITE_DATA(data);
		}
//		sleep(1);
		while(SPI_BUSY());
	}while(length);
	GpioWriteData(CS,1);
}

void SPI_SR_DIO_Tx(UINT32 CS,UINT32 data,int length){ //Data length must be less than 32 by Nolan 09-01-08

	GpioWriteData(CS,0);

	SetSPI_Len(length-1);
	SPI_WRITE_DATA(data);
	while(SPI_BUSY());

	GpioWriteData(CS,1);
}

void SPI_SR_DIO_Rx(UINT32 CS,UINT32 data,int length){ //Data length must be less than 32 by Nolan 09-01-08
	
	GpioWriteData(CS,1);
	GpioWriteData(CS,0);
	GpioWriteData(CS,1);

	SetSPI_Len(length-1);
	SPI_WRITE_DATA(data);
	while(SPI_BUSY());
}
#else
void SPI_Tx(UINT32 data,int length){ 
	SetSPI_Len(length - 1);
	SPI_WRITE_DATA(data);
	while(SPI_BUSY());
}
#endif


UINT32 SPI_ReadFIFO(void){
	return (VPlong(S2E_SPI_BASE + SPI_DATA));
}

void SetSPI_Len(int length){
	UINT32 tmp;

	tmp = VPlong(S2E_SPI_BASE + SPI_CONTROL0) & 0xFFFFFFF0;
	tmp |= length;
	SPI_DISABLE();	
	VPlong(S2E_SPI_BASE + SPI_CONTROL0) = tmp;
	SPI_ENABLE();
}

UINT8 SPI_BUSY(void){
	if(SPI_STATUS() & SPI_STATUS_TX_FIFO_EMPTY){
		if(!SPI_IS_BUSY())
			return 0;
	}
	return 1;
}

