#ifndef _STRING_H_
#define _STRING_H_



/*------------------------------------------------------ Extern / Function Declaration -----------------*/
char * strcpy(char * dest,const char *src);
char * strcat(char * dest, const char * src);
int strcmp(const char * cs,const char * ct);
char * strchr(const char * s, int c);
int strlen(const char * s);
char * strrchr(const char * s, int c);
int strspn(const char *s, const char *accept);
char * strpbrk(const char * cs,const char * ct);
char * strtok(char * s,const char * ct);
void * memset(void * s, char c, int count);
char * bcopy(const char * src, char * dest, int count);
void * memcpy(void * dest,const void *src,int count);
void * memmove(void * dest,const void *src,int count);
int memcmp(const void * cs,const void * ct,int count);
void * memscan(void * addr, int c, int size);
char * strstr(const char * s1,const char * s2);

#endif /* _STRING_H_ */
