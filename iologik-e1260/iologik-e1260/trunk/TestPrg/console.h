/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	Console.h

	Console library
	Function declaration.
		
	2008-09-24	Chin-Fu Yang		
		new release
    
*/


#ifndef _CONSOLE_H
#define _CONSOLE_H


/*------------------------------------------------------ Macro / Define -----------------------------*/
#define CR		0x0D
#define NL		0x0A
#define BR		0x08

/*------------------------------------------------------ Structure ----------------------------------*/


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void ConInit(int port,ulong baud);
void ConClose(int port);
uchar Getch(void);
int Kbhit(void);
void Printf(const char *fmt, ...);
void PrintStr(char *str);
void PrintHex(ulong value);
void	Write(char *buf,int len);
void	WriteHex(ushort value);
void	WriteByte(uchar ch);
int	ConGetString(char *str, int len);
void Runbar(u32 count, u32 speed);






#endif
