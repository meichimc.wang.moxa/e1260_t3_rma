/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    mac.c

    Routines for accessing MAC module

	2008-06-11	Albert Yu
		new release
	2008-10-03	Chin-Fu Yang		
		modified	
		
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "timer.h"
#include "memory.h"
#include "lib.h"
#include "mac.h"
#include "mii.h"
#include "global.h"
#include "console.h"
#include "ip.h"
#include "arp.h"


// S2E has only one MAC port.
ulong MAC_BASE[MAC_MAX_AMOUNT] = {S2E_MAC_BASE,S2E_MAC_BASE,S2E_MAC_BASE,S2E_MAC_BASE,S2E_MAC_BASE};
int MAC_IRQ[MAC_MAX_AMOUNT] = {IRQ_MAC,IRQ_MAC,IRQ_MAC,IRQ_MAC,IRQ_MAC};
MacInfo MoxaMacInfo[MAC_MAX_AMOUNT];
int G_LanPort = 0;

/* PHY_ADDR : 0 for internal phy, 1 ~ 4 for 88E6060 phy . 
	These address are used in Mii Interface. 
	The value of PHY_ADDR was writen in HW Spec.*/
ulong PHY_ADDR[MAC_MAX_AMOUNT] = {S2E_PHY_ADDR, 0x11, 0x12, 0x13, 0x14};		
																				
int mac_is_open = 0;

int MAC_LINK_FAIL = 0;


/**	\brief
 *
 *	Init MAC descriptors.
 *
 *	\param[in]	port : MAC port
 *
 */
static void GMAC_descriptor_init(int port)
{
	mac_rdes_p rxdes;		// RX Descriptor pointer
	mac_tdes_p txdes;		// TX Descriptor pointer
	ulong addr;				// 
	int i;


	// 1. init RX
	addr = SDRAM_MAC_RX_BUF_BASE(port);					// get the RX buf address in SDRAM
	rxdes = (mac_rdes_p)(MAC_RX_DESCRIPTOR_BASE(port));		// get the RX Descriptor address in SDRAM
	for(i = 0; i < MAC_RX_BLOCK_CNT; i++){
		Memset((uchar *)rxdes,0,sizeof(*rxdes));				// clear RX Descriptor
		rxdes->ReceiveBuffer1Size = MAC_BLOCK_MAX_SIZE;
		rxdes->Buffer1AddressPointer = addr;
		rxdes->OwnBit = 1;		/* Owned by MAC , so that MAC can use this RX Descriptor to receive data*/
		addr += MAC_BLOCK_MAX_SIZE;
		rxdes++;		// init next descriptor
	}
	rxdes--;		// Let the last RX descriptor to be end.
	rxdes->ReceiveEndOfRing = 1;	/* ring end */


	// 2. init TX
	addr = SDRAM_MAC_TX_BUF_BASE(port);					// get the TX buf address in SDRAM
	txdes = (mac_tdes_p)(MAC_TX_DESCRIPTOR_BASE(port));		// get the TX Descriptor address in SDRAM
	for(i = 0; i < MAC_TX_BLOCK_CNT; i++){
		Memset((uchar *)txdes,0,sizeof(*txdes));			// clear TX Descriptor , txdes->OwnBit = 0;
		txdes->TransmitBuffer1Size = MAC_BLOCK_MAX_SIZE;
		txdes->Buffer1AddressPointer = addr;
		addr += MAC_BLOCK_MAX_SIZE;
		txdes++;		// init next descriptor
	}
	txdes--;		// Let the last TX descriptor to be end.
	txdes->TransmitEndOfRing = 1;	/* ring end */

	
}




/**	\brief
 *
 *	Init the software data structure of MAC. 
 *	Just set the Address
 *
 *	\param[in]	port : MAC port
 *
 */
void    mac_init(int port)
{
	int j;
	ulong serial=0;
	char ip0=0;		// IP Bit24..31
	char ip1=0;		// IP Bit16..23
	
	MoxaMacInfo[port].base = MAC_BASE[port];		// set base address
	MoxaMacInfo[port].irq = MAC_IRQ[port];		// set IRQ number
	MoxaMacInfo[port].Tring = (mac_tdes_p)(MAC_TX_DESCRIPTOR_BASE(port));
	MoxaMacInfo[port].Rring = (mac_rdes_p)(MAC_RX_DESCRIPTOR_BASE(port));
	serial = sysModelInfo[board_index].serial;		// Product HW ID

	// 1. IP Address
	/* Generate and Set Random IP Address to each port for Test */
	if( (MacSrc[port][4] == 0xff) || (MacSrc[port][4] == 0x00) ){
		
		if((serial != 255) && (serial != 0)){
			ip1 = (char)(serial);
		}else{
			ip1 = 0x01;		
		}
		
	}else {
		ip1 = MacSrc[port][4];	
	}
	if( (MacSrc[port][5] == 0xff) || (MacSrc[port][5] == 0x00) ){
		
		if((serial != 255) && (serial != 0)){
			ip0 = (char)(serial);
		}else{
			ip0 = 0x01;		
		}
		
	}else{ 
		ip0 = MacSrc[port][5];	
	}
    	MoxaMacInfo[port].ip = ( (NicIp[port] & 0x0000ffff) | ((ip1 << 16) & 0x00ff0000) | ((ip0 << 24) & 0xff000000)); 	// set random IP
	NicIp[port] = MoxaMacInfo[port].ip;	// replace default IP
	// ----- END of set random IP  ------ //

	// 2. PHY Address
	MoxaMacInfo[port].phy_addr = PHY_ADDR[port];		// set default PHY address , for Mii

	// 3. MAC Address
	for(j=0;j<6;j++){
		MoxaMacInfo[port].mac_addr[j] = MacSrc[port][j];	
	}
	
	MoxaMacInfo[port].mode = 0; 
	MoxaMacInfo[port].InterruptCount = 0;

//	Printf("serial= %d, NicIP%d= 0x%08x, phyAddr=0x%x\r\n", serial, port, NicIp[port], MoxaMacInfo[port].phy_addr);
}  




/**	\brief
 *
 *	Get a RX Descriptor from ring structure and extract the data in descriptor. 
 *
 *	\param[in]	port : MAC port
 *
 *	\retval 		Memory Block which is the data receive from MAC.
 */
msgstr_t	mac_read(int port)
{
	int len;
	int i;
	int ret=0;
	msgstr_t msg=NULL;
	uchar *rxbuf;
	mac_rdes_p rdesp = MoxaMacInfo[port].Rring;	// pointer to current RX Descriptor

    	/* check any packet received or not */
	if (rdesp->OwnBit){
		return NULL;		// If Owner bit of RX Descriptor is 1 , it says that no data was in RX Descriptor.
	}

	if (!(rdesp->ReceiveError)) {
		/* Receive Success */
		len = rdesp->FrameLength;		// Frame length = data length + CRC length
		if (((len-=4 /*strip CRC*/) > 0) && (msg=MemAlloc(len))){
			rxbuf = (uchar*)(rdesp->Buffer1AddressPointer);
			for( i = 0 ; i < len ; i++ ){
				msg->base[i] = rxbuf[i];	// get the data
			}
			msg->rndx = 0;
			msg->wndx = len;
			ret = 1;
		}else{
			Printf("MemAlloc(%d) FAIL\r\n", len);
			ret = 0;
		}
	}else{
		/* Receive Error */
		ret = 0;
		Printf("Receive Error (0x%08x)\r\n",rdesp->CRCError);
	}

	rdesp->OwnBit = 1;	// "OwnBit = 1" denotes that Descriptor is Owned by MAC

	/* Take the global ring pointer to pointer to next RX Descriptor */
	if (rdesp->ReceiveEndOfRing){
		MoxaMacInfo[port].Rring = (mac_rdes_p)(MAC_RX_DESCRIPTOR_BASE(port));
	}else{
		MoxaMacInfo[port].Rring++;
	}

	/* trigger MAC to receive the RX Descriptor back*/
	outw(MoxaMacInfo[port].base + MAC_DMA_RPDR,0);

	if(ret){
		return(msg);
	}else{
		return NULL;
	}
	
}




/**	\brief
 *
 *	Set data to TX Descriptor and let MAC to send data.
 *
 *	\param[in]	port : MAC port
 *	\param[in]	msg : data to send
 *
 *	\retval 		error code
 */
int	mac_send(int port,msgstr_t msg)
{
	int i;
	uchar *txbuf;
	mac_tdes_p tdesp = MoxaMacInfo[port].Tring;

	if (tdesp->OwnBit){	// Owned by MAC , we can't use it.
		return(DIAG_FAIL);
	}
	/* Copy Data */
	txbuf = (uchar*)(tdesp->Buffer1AddressPointer);
	for(i=0;i<msg->wndx;i++){
		txbuf[i] = msg->base[i];
	}
	tdesp->TransmitBuffer1Size = msg->wndx;
	tdesp->FirstSegment = 1;	
	tdesp->LastSegment = 1;	// only one segment
	tdesp->OwnBit = 1;

	/* trigger MAC to transmit */
	outw(MoxaMacInfo[port].base + MAC_DMA_TPDR, 0);

	/* pointer to next TX Descriptor */
	if (tdesp->TransmitEndOfRing){
		MoxaMacInfo[port].Tring = (mac_tdes_p)(MAC_TX_DESCRIPTOR_BASE(port));
	}else{
		MoxaMacInfo[port].Tring++;
	}

	MemFree(msg);

	return(DIAG_OK);
}





/**	\brief
 *
 *	Read the packet data and send it to mac without modify immediately.
 *	for testing !!
 *
 *	\param[in]	port : MAC port
 *
 *	\retval 		error code 
 *
 */
int	mac_send_after_read(int port)
{
	int i;
	msgstr_t msg=NULL;
	uchar *txbuf;
	mac_tdes_p tdesp = MoxaMacInfo[port].Tring;
	

	// 1. Receive ...........................................................................
	msg = mac_read(port);
	if(msg != NULL){
			
		// 2. Send ...................................................................................
		if (tdesp->OwnBit){	// Owned by MAC , we can't use it.
			return(DIAG_FAIL);
		}

		/* Copy Data */
		txbuf = (uchar*)(tdesp->Buffer1AddressPointer);
		for(i=0;i<msg->wndx;i++){
			txbuf[i] = msg->base[i];
		}
		/* Swap Src Des address. */
		for (i=0;i<6;i++)	msg->base[i] = msg->base[i+6] ;
		for (i=6;i<12;i++)	msg->base[i] = MacSrc[port][i-6];	
		Printf("My Ack packet = \r\n");
		Printf("Src : %.2x.%.2x.%.2x.%.2x.%.2x.%.2x\r\n" , msg->base[6] , msg->base[7] , msg->base[8] ,
													msg->base[9]  , msg->base[10] , msg->base[11] );
		
		Printf("Des : %.2x.%.2x.%.2x.%.2x.%.2x.%.2x\r\n" , msg->base[0] , msg->base[1] , msg->base[2] ,
													msg->base[3]  , msg->base[4] , msg->base[5] );	
		
		tdesp->TransmitBuffer1Size = msg->wndx;
		tdesp->FirstSegment = 1;	
		tdesp->LastSegment = 1;	// only one segment
		tdesp->OwnBit = 1;

		/* trigger MAC to transmit */
		outw(MoxaMacInfo[port].base + MAC_DMA_TPDR, 0);

		/* pointer to next TX Descriptor */
		if (tdesp->TransmitEndOfRing){
			MoxaMacInfo[port].Tring = (mac_tdes_p)(MAC_TX_DESCRIPTOR_BASE(port));
		}else{
			MoxaMacInfo[port].Tring++;
		}

		MemFree(msg);
		return(DIAG_OK);
	}

	return DIAG_FAIL;

}


















/**	\brief
 *
 *	Shutdown the MAC by disabling interrupt of MAC.
 *
 *	\param[in]	port : MAC port
 *
 */
void	mac_shutdown(int port)
{
	outw(MoxaMacInfo[port].base + MAC_DMA_IER, 0);	/* disable all interrupts */
	DISABLE_INT(MoxaMacInfo[port].irq);
//	Printf("eth0 = %d eth1 = %d\r\n",MoxaMacInfo[0].InterruptCount,MoxaMacInfo[1].InterruptCount);
}



	
/**	\brief
 *
 *	Open the MAC port
 *
 *	\param[in]	port : MAC port
 *	\param[in]	operational_mode : operational mode of MAC. 
 *					(MODE_NORMAL , MODE_LOOPBACK , MODE_DRIVING , MODE_LAN10M , ... )
 *
 *	\retval 		error code
 */

extern int  poweronstatus;
extern char MAC_GEN_Mac[6];
int	mac_open(int port,int operational_mode)
{
	ulong base;			// MAC Base Address
	ulong val;
	ulong phy_ctrl_reg;	// PHY Control Register of MII
	ulong aar_reg;		// AAR Register of MII (Advertisment)
	ulong lpar_reg;		// LPAR Register of MII (Link Partner)
	ulong t;
//	ulong speed100;		// speed : 100Mbps mode
//	ulong full_duplex;	// full duplex mode
	
	int phyAddr;			// PHY Address

	int err;

	mac_init(port);		// init Software data structure

    base = MoxaMacInfo[port].base;

	/* Reset GMAC */
	outw(base + MAC_DMA_BMR, MAC_DMA_BMR_SWR);			// reset all MAC registers
	t = Gsys_msec;		// for timeout protection
	while(inw(base + MAC_DMA_BMR) & MAC_DMA_BMR_SWR){
		if(Gsys_msec - t > 1000){
			PrintStr("Reset GMAC timeout !!!\r\n");
			outw(base + MAC_DMA_BMR, 0);					// not reset
			break;
		}
	}

	GMAC_descriptor_init(port);		// init all TX/RX Descriptors

	/* GMAC initialization . Follow the design flow of the S2E Spec.*/
	/* 1. Write to DMA Register0 to set the Host bus access parameters. */
	/* (AAL=1,4xPBL=1,USP=0,FB=0,PR=0,PBL=32,DSL=0,DA=0) */
	outw(base + MAC_DMA_BMR, MAC_DMA_BMR_AAL | MAC_DMA_BMR_4xPBL | MAC_DMA_BMR_PBL_32);

	/* 2. Write to DMA Register 7 to mask unnecessary interrupt causes. */
	outw(base + MAC_DMA_IER, 0);	/* disable all interrupts of MAC*/

	/* 3. The software driver creates the Transmit and Receive descriptor lists.
	      Then it writes to both DMA Register 3 and DMA Register 4, providing the
	      DMA with the starting address of each list. */
	outw(base + MAC_DMA_RDLAR, MAC_RX_DESCRIPTOR_BASE(port));
	outw(base + MAC_DMA_TDLAR, MAC_TX_DESCRIPTOR_BASE(port));
	
	/* 4. Write to GMAC Registers 1, 2, and 3 for desired filtering options. */
	if(poweronstatus == 3){ //MAC GEN_STATUS
		outw(base + MAC_A0HR, MAC_AHR_A_(MAC_GEN_Mac));		// The Highest 2 bytes
		outw(base + MAC_A0LR, MAC_ALR_A_(MAC_GEN_Mac));			// The Lowest 4 bytes
	}
	else{
		outw(base + MAC_A0HR, MAC_AHR_A_(MacSrc[port]));		// The Highest 2 bytes
		outw(base + MAC_A0LR, MAC_ALR_A_(MacSrc[port]));			// The Lowest 4 bytes
	}
	/* Do nothings, default filter options are suitable. */

	phyAddr = MoxaMacInfo[port].phy_addr;

   	 /* reset phy */
	if (operational_mode != MODE_LOOPBACK){
//		PrintStr("Reset PHY...");
/*		if (ResetPhyChip(phyAddr, operational_mode) != 0){
			PrintStr("Reset PHY...Fail!\r\n");
			return (-6);
		}
*/		err =ResetPhyChip(phyAddr, operational_mode);
		if(err != 0)	return err;
	}



	/* 5. Write to GMAC Register0 to configure and enable the Transmit and
	      Receive operating modes. The PS and DM bits are set based on the
	      auto-negotiation result (read from the PHY). */
	val = MAC_CR_RE | MAC_CR_TE;	/* Enable Transmit and Receive */
	
	if(operational_mode != MODE_NORMAL){	// Normal mode is Half-duplex
		val |= MAC_CR_DM;		/* MAC full-duplex mode is required for self-test to work */
	}

	if(operational_mode & MODE_LOOPBACK){
		val |= MAC_CR_LM;		// Loopback mode
//		PrintStr("[ MAC Loopback ]\r\n");
		
	}else{	// not loopback mode
					
//		speed100 = 0;
//		full_duplex = 0;
//		PrintStr("[ link: ");
		
		phy_ctrl_reg = Lnet_chip_smi_read(phyAddr, REG_MII_BMCR);
		if (!(phy_ctrl_reg & MII_BMCR_ANEN)){
			/* Auto-Nego disable , we should detect our speed*/
			if (phy_ctrl_reg & MII_BMCR_SPEED){
				val |= MAC_CR_FES;	/* 100 Mbps */
//				speed100 = 1;
			}
			if (phy_ctrl_reg & MII_BMCR_DUPLEX){
				val |= MAC_CR_DM;	/* Full-Duplex mode */
//				full_duplex = 1;
			}
		}else{	/* Auto-negotiation enable, should read Link Partner Ability Register */
			aar_reg = Lnet_chip_smi_read(phyAddr, REG_MII_AAR);
			lpar_reg = Lnet_chip_smi_read(phyAddr, REG_MII_LPAR);
			if ((aar_reg & lpar_reg) & (MII_LPAR_TXFD | MII_LPAR_TXHD)){	// TX Full 100 or TX Half 100 
				val |= MAC_CR_FES;	/* 100 Mbps */
//				speed100 = 1;
			}
			if ((aar_reg & lpar_reg) & (MII_LPAR_TXFD | MII_LPAR_TPFD)){	// TX Full 100 or TX Full 10
				val |= MAC_CR_DM;	/* Full-Duplex mode */
//				full_duplex = 1;
			}
		}
/*
		if (speed100){
			PrintStr("100M-");
		}else{
			PrintStr("10M-");
		}
*//*
		if (full_duplex){
			PrintStr("Full ]\r\n");
		}else{
			PrintStr("Half ]\r\n");			
		}
*/	}

	outw(base + MAC_CR, val);		// set configuration to MAC Control register
//	Printf("MAC_CR=0x%x\r\n", inw(base + MAC_CR));



	/* 6. Write to DMA Register 6 to set bits 13 and 1 to start transmission and reception. */
	/*  Forword Undersized Good Frame + Start Transmission Command + Start Receive*/
	outw(base + MAC_DMA_OMR, inw(base + MAC_DMA_OMR) | (MAC_DMA_OMR_FUF | MAC_DMA_OMR_ST | MAC_DMA_OMR_SR));

	mac_is_open = TRUE;
		
	return DIAG_OK;
}






 /**	\brief
 *
 *	Process NIC frame
 *
 */
void	NicProcess(void)
{
	msgstr_t 		msg=NULL;		// pointer to memory block
	ushort 		*type;			// packet type	
	int 			j;

/*
 * I assume one ethernet packet need 1000 ns(maximum) to process.
 * And 100 * 1000ns = 0.1 ms.
 * So this routine will return in 0.1 ms.
 * That's why I use 100 to count the packets processed in this routine.
 */
	for (j=0;j<100;j++) {
		msg = mac_read(G_LanPort);		// get the data from MAC
		if (msg == NULL){
			continue;
		}

		type = (ushort *)&msg->base[12];	// Get Packet type 
		
		switch (*type) {
		case NIC_IP :
/*
Printf("NIC_IP");
for(i=0;i<msg->wndx;i++){
	if(i%16 == 0) Printf("\r\n");
	Printf("%02x ",msg->base[i]);
}
Printf("\r\n\r\n");
*/
			IpReceive(msg);
			break;
		case NIC_ARP :

//Printf("NIC_ARP");
/*for(i=0;i<msg->wndx;i++){
	if(i%16 == 0) Printf("\r\n");
	Printf("%02x ",msg->base[i]);
}
Printf("\r\n\r\n");
*/
			ArpReceive(msg);
			break;
		case NIC_IPX :
/*
Printf("NIC_IPX");
for(i=0;i<msg->wndx;i++){
	if(i%16 == 0) Printf("\r\n");
	Printf("%02x ",msg->base[i]);
}
Printf("\r\n\r\n");
*/
			MemFree(msg);
			break;
		default :
/*
Printf("NIC_UNKNOW");
for(i=0;i<msg->wndx;i++){
	if(i%16 == 0) Printf("\r\n");
	Printf("%02x ",msg->base[i]);
}
Printf("\r\n\r\n");
*/			MemFree(msg);
			break;
		}
	}

}





/**	\brief
 *
 *	Read the PHY register from SMI(MDC/MDIO) interface.
 *	By selecting PHY Address and Register Number in MAC register , you can control the PHY device.
 *
 *	\param[in]	phyAddr : PHY address
 *	\param[in]	regNum : register number
 *
 *	\retval 		the value of register
 */
ushort Lnet_chip_smi_read(int phyAddr, int regNum)
{
	ulong	base;	// Base Address of MAC
//	ushort	val;		
	ulong	val;		
	ulong	t;		// for timeout

	base = S2E_MAC_BASE;
	
	val = inw(base + MAC_GMII_AR);
	val &= MAC_GMII_AR_CR;		// Set CSR Clock Range = Reserved

	outw(base + MAC_GMII_AR,			// Control GMII
		val |							// CSR Clock Range = Reserved
		MAC_GMII_AR_PA_(phyAddr) |	// Select PHY Address which you want to access
		MAC_GMII_AR_GR_(regNum) |	// Select Register Number of PHY
		MAC_GMII_AR_GB);				// GMII Busy to lock PHY

	/* Timeout Protection */
	t = Gsys_msec;
	while (inw(base + MAC_GMII_AR) & MAC_GMII_AR_GB){
		if((Gsys_msec - t) > 1000){
		    	Printf("Lnet_chip_smi_read(0x%x, 0x%x) fail !\r\n", phyAddr, regNum);
			return (-1);
		}
	}
	
	return inw(base + MAC_GMII_DR);	// get the value of the PHY register
}





/**	\brief
 *
 *	Write the PHY register from SMI(MDC/MDIO) interface.
 *	By selecting PHY Address and Register Number in MAC register , you can control the PHY device.
 *
 *	\param[in]	phyAddr : PHY address
 *	\param[in]	regNum : register number
 *	\param[in]	wData : the value you will write into
 *
 *	\retval 		error code
 */
int  Lnet_chip_smi_write(int phyAddr, int regNum, ushort wData)
{
	ulong	base;		// base address
//	ushort	val;			
	ulong	val;			
	ulong	t;			// for timeout
	
	base = S2E_MAC_BASE;

	val = inw(base + MAC_GMII_AR);			// Get the address of the SMI Interface
	val &= MAC_GMII_AR_CR;					// Set Clock Range = 111 (Reserved)
	
	outw(base + MAC_GMII_DR, wData);			// Write to Data Register 
	val |= MAC_GMII_AR_GW;					// WRITE
	
	outw(base + MAC_GMII_AR,				// Base Address
		val |								
		MAC_GMII_AR_PA_(phyAddr) |		// Phy Address
		MAC_GMII_AR_GR_(regNum) |		// Register Number of PHY
		MAC_GMII_AR_GB);					// GMII Busy

	/* timeout protection */
	t = Gsys_msec;
	while(inw(base + MAC_GMII_AR) & MAC_GMII_AR_GB){
		if((Gsys_msec - t) > 1000){
			Printf("Lnet_chip_smi_write(0x%x, 0x%x, 0x%x) fail !\r\n", phyAddr, regNum, wData);
			return (-1);
		}
	}

	return 0;
}





//#define PHY_BUG				// ZarZar
//#define PHY_10M_BUG		// ZarZar
//#define PHY_100M_BUG		// ZarZar
/**	\brief
 *
 *	Reset PHY Chip
 *
 *	\param[in]	phyAddr : PHY address
 *	\param[in]	operational_mode : operational mode of MAC
 *
 *	\retval 		error code
 */ 
int ResetPhyChip(int phyAddr, int operational_mode)
{

	UINT16 WriteData = 0;
	UINT16 WriteData_AAR = 0;
	UINT16 ReadData=0;
	ulong t;
	
#ifdef PHY_BUG	
	UINT16 reg_25 = 0;	// for PHY Bug
	UINT16 reg_22 = 0;	// for PHY bug
	UINT16 reg_24 = 0;
	UINT16 value_to_reg22 = 0x8301;	// for PHY bug
	UINT16 value_to_reg24 = 0x5A11;
#endif

	//Reset PHY switch
//	WriteData = MII_BMCR_RST;
//	Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData);
//	while (Lnet_chip_smi_read(phyAddr,REG_MII_BMCR) & (MII_BMCR_RST | MII_BMCR_RESTART_AN));
//	WriteData = 
//	Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData);

	// 1. Check if 100M 	
	if((operational_mode & MODE_LAN100M) == MODE_LAN100M){	// 100Mbps mode


#ifdef PHY_100M_BUG
		// clear LED
//		ShowIndividualLED(0 , OFF);		
//		ShowIndividualLED(1 , OFF);
//		ShowIndividualLED(2 , OFF);
		
		/* 100M Bug */
		reg_25 = Lnet_chip_smi_read(phyAddr , 25);		// read the 
		reg_25 |= MII_BIT(13);		// set bit 13 = 1;	
		if(Lnet_chip_smi_write(phyAddr , 25 , reg_25) != 0){
			Printf("Set PHY Reg25 Fail !!! \r\n");
		}

		/* 100M Bug Version2 */
		reg_22 = Lnet_chip_smi_read(phyAddr , 22);		// read the 
		reg_22 = value_to_reg22;
		if(Lnet_chip_smi_write(phyAddr , 22 , reg_22) != 0){
			Printf("Set PHY Reg22 Fail !!! \r\n");
		}

		/* 100M Bug Version3 */
		reg_24 = Lnet_chip_smi_read(phyAddr , 24);		// read the 
		reg_24 = value_to_reg24;
		if(Lnet_chip_smi_write(phyAddr , 24 , reg_24) != 0){
			Printf("Set PHY Reg24 Fail !!! \r\n");
		}		

		/* set LED for monitoring PHY state */
		reg_25 = Lnet_chip_smi_read(phyAddr , 25);
		if( (reg_25&MII_BIT(13)) != 0){
//			ShowIndividualLED(0 , ON);
		}
		reg_22 = Lnet_chip_smi_read(phyAddr , 22);
		if( reg_22 == value_to_reg22){
//			ShowIndividualLED(1 , ON);
		}

		reg_24 = Lnet_chip_smi_read(phyAddr , 24);
		if( reg_24 == value_to_reg24){
//			ShowIndividualLED(2 , ON);
		}			
		
#endif

		WriteData = MII_BMCR_SPEED | MII_BMCR_RST;		// Force 100M

		if(operational_mode & MODE_FDUP){
			WriteData |= MII_BMCR_DUPLEX;    // Force 100-TX full duplex operation	
			WriteData_AAR = MII_AAR_TXFD;
		}else{
			WriteData &= ~(MII_BMCR_DUPLEX);    // Force of 100-TX half duplex operation	
			WriteData_AAR = MII_AAR_TXHD;
		}	

		if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData) != 0){
			Printf("***Force 100M error...\r\n");
		}
		if(Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData_AAR) != 0){
			Printf("***AAR 100M error...\r\n");
		}
	
/*		
		WriteData = 0;
		if(operational_mode & MODE_FDUP){
			WriteData = MII_AAR_TXFD;    //capable of 100-TX full duplex operation	
		}else{
			WriteData = MII_AAR_TXHD;    // capable of 10-TP full duplex operation	
		}
		
		if(Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData) == 0){	// AAR is to tell the other side our mode. 0 is ok
			WriteData = MII_BMCR_ANEN | MII_BMCR_RESTART_AN;				// Auto-Nego	
			if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData)){
				return (-1); 
			}  
		}
*/
	// 2. Check if 10M
	}else if((operational_mode & MODE_LAN10M) == MODE_LAN10M){	// 10Mbps mode
	
#if 1
#ifdef PHY_10M_BUG
		/* clear LED for monitoring PHY state */
//		ShowIndividualLED(0 , OFF);
//		ShowIndividualLED(1 , OFF);
//		ShowIndividualLED(2 , OFF);

		/* 10M Bug */
		reg_25 = Lnet_chip_smi_read(phyAddr , 25);		// read the 
		reg_25 |= MII_BIT(13);		// set bit 13 = 1;	
		if(Lnet_chip_smi_write(phyAddr , 25 , reg_25) != 0){
			Printf("Set PHY Reg25 Fail !!! \r\n");
		}

		/* 10M Bug Version2 */
		reg_22 = Lnet_chip_smi_read(phyAddr , 22);		// read the 
		reg_22 = value_to_reg22;
		if(Lnet_chip_smi_write(phyAddr , 22 , reg_22) != 0){
			Printf("Set PHY Reg22 Fail !!! \r\n");
		}

		/* 10M Bug Version3 */
		reg_24 = Lnet_chip_smi_read(phyAddr , 24);		// read the 
		reg_24 = value_to_reg24;
		if(Lnet_chip_smi_write(phyAddr , 24 , reg_24) != 0){
			Printf("Set PHY Reg24 Fail !!! \r\n");
		}		

		/* set LED for monitoring PHY state */
		reg_25 = Lnet_chip_smi_read(phyAddr , 25);
		if( (reg_25&MII_BIT(13)) != 0){
//			ShowIndividualLED(0 , ON);
		}
		reg_22 = Lnet_chip_smi_read(phyAddr , 22);
		if( reg_22 == value_to_reg22){
//			ShowIndividualLED(1 , ON);
		}

		reg_24 = Lnet_chip_smi_read(phyAddr , 24);
		if( reg_24 == value_to_reg24){
//			ShowIndividualLED(2 , ON);
		}		
		
#endif
		WriteData &= ~(MII_BMCR_SPEED);		// Force 10M
		WriteData |= MII_BMCR_RST;

		if(operational_mode & MODE_FDUP){
			WriteData |= MII_BMCR_DUPLEX;    // Force 10-TX full duplex operation	
			WriteData_AAR = MII_AAR_TPFD;
		}else{
			WriteData &= ~(MII_BMCR_DUPLEX);    // Force of 10-TX half duplex operation	
			WriteData_AAR = MII_AAR_TPHD;
		}	

		if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData) != 0){
			Printf("***Force 10M error...\r\n");
		}

		if(Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData_AAR) != 0){
			Printf("***AAR 10M error...\r\n");
		}

/*
		if(operational_mode & MODE_FDUP){
			WriteData = MII_AAR_TPFD;   // capable of 10-TP full duplex operation	
		}else{
			WriteData = MII_AAR_TPHD;   // capable of 10-TP half duplex operation	
		}
		
		// close auto-nego
		if(Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData) == 0){	// AAR is to tell the other side what our mode is.
			WriteData = MII_BMCR_ANEN | MII_BMCR_RESTART_AN;				// Auto-Nego
			if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData)){
				return (-1);		
			}	
		}
*/		
#else
		WriteData = 0;//MII_BMCR_RST;
		if(mode & MODE_FDUP)
			WriteData |= MII_BMCR_DUPLEX;   /* force to full duplex mode */
		if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData))
			return (-7);
#endif

	// 3. Check if Loopback
	}else if((operational_mode & MODE_LOOPBACK) == MODE_LOOPBACK){	// Loopback mode
		WriteData = (MII_BMCR_LPBK | MII_BMCR_ANEN | MII_BMCR_RESTART_AN);	// Auto-Nego & Loopback mode
		if(Lnet_chip_smi_write(phyAddr, REG_MII_BMCR, WriteData)){
			return (-7);
		}

	// 4. Check if Auto-negotiation mode (NORMAL)
	}else if((operational_mode & MODE_NORMAL) == MODE_NORMAL){
		WriteData = MII_AAR_TXFD | MII_AAR_TXHD | MII_AAR_TPFD | MII_AAR_TPHD;		// set it to default (ALL Open)
		Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData);	// 88E6060 won't reset AAR register when reset phy, so we set it to defaut before reset phy
		WriteData = MII_BMCR_RST | MII_BMCR_ANEN | MII_BMCR_RESTART_AN;
//		if(Lnet_chip_smi_write(phyAddr,REG_MII_BMCR,WriteData) != 0){
		if(Lnet_chip_smi_write(phyAddr,REG_MII_BMCR,WriteData)){
			return (-7);
		}
	}else if((operational_mode & MODE_PWRDWN) == MODE_PWRDWN){
		WriteData = MII_AAR_TXFD | MII_AAR_TXHD | MII_AAR_TPFD | MII_AAR_TPHD;		// set it to default (ALL Open)
		Lnet_chip_smi_write(phyAddr, REG_MII_AAR, WriteData);	// 88E6060 won't reset AAR register when reset phy, so we set it to defaut before reset phy
		WriteData = MII_BMCR_RST | MII_BMCR_PWDN;
//		if(Lnet_chip_smi_write(phyAddr,REG_MII_BMCR,WriteData) != 0){
		if(Lnet_chip_smi_write(phyAddr,REG_MII_BMCR,WriteData)){
			return (-7);
		}
	}

	/* must wait for reset & auto-negotiation finished, or a read of REG_MII_SWR will return 0xFFFF during reset */
	t = Gsys_msec;
	while ((ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_BMCR)) & (MII_BMCR_RST | MII_BMCR_RESTART_AN)){	// 0 is finish
		if (Gsys_msec - t > 3000){
			Printf("*** Reset timeout!***  ctrlReg=0x%x\r\n", ReadData);
			//break;
			return (-7);
		}
	}
	
	t = Gsys_msec;
	for(;;){
		if(Gsys_msec - t > 3000){
			Printf("Link not established! statusReg=0x%x\r\n", ReadData);
//			MAC_LINK_FAIL = 1;
			//break;
			return (-6);
//		 	return DIAG_ESC;
		}
		ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_SWR);
		if(((UINT16)ReadData & MII_SWR_LINK) == MII_SWR_LINK){		// Link Established
			break;	
		}
	}

//	PrintPHY(phyAddr);

	if (phyAddr >= 0x11){		// external PHY (Switch PHY)
//		ushort val;
		ulong val;
		/* for 88E6060, set PortState to Forwarding */
		/* 88E6060's PHY is from 0x18 to 0x1D */
		for (phyAddr = 0x18; phyAddr <= 0x1D; phyAddr++){
			Lnet_chip_smi_write(phyAddr, 0x04, 0x03);		// write to 88E6060's MAC port control.
		}
		/* Flush All ATU Entries */
		val = Lnet_chip_smi_read(0x1F, 0x0B);				
		val |= 0x1000;
		Lnet_chip_smi_write(0x1F, 0x0B, val);				// write to 88E6060's MAC global control.
	}

	return 0;
}





/**	\brief
 *
 *	Read all the registers of PHY and print them in the console.
 *
 *	\param[in]	phyAddr : PHY address
 *
 */
void PrintPHY(int phyAddr)
{
	UINT16	ReadData;

	Printf("\r\n");
	ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_BMCR);
	Printf("REG_MII_BMCR = 0x%04x\r\n",ReadData);
	ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_SWR);
	Printf("REG_MII_SWR  = 0x%04x\r\n",ReadData);
	ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_AAR);
	Printf("REG_MII_AAR  = 0x%04x\r\n",ReadData);
	ReadData = Lnet_chip_smi_read(phyAddr,REG_MII_LPAR);
	Printf("REG_MII_LPAR = 0x%04x\r\n",ReadData);
}

int PowerMode(int port,UINT8 state){
	UINT16 WriteData;
	WriteData = Lnet_chip_smi_read(0x10+port,REG_MII_BMCR);
	WriteData |= MII_BMCR_RST;
	if(state)
		WriteData |=  MII_BMCR_PWDN;
	else
		WriteData &=  ~MII_BMCR_PWDN;
	if(Lnet_chip_smi_write(0x10+port,REG_MII_BMCR,WriteData)){
		return (-7);
	}
	sleep(10);
	return 0;
}
