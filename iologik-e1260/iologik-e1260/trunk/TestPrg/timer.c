/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	timer.c

	Down counting timer.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified    
		
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "timer.h"
#include "lib.h"




unsigned long SysTimerTicks = 0;	/* Time Unit: hundredths of a second */
unsigned long Gsys_msec = 0;


//static taskw	waittable[DSYS_MAXTWAIT];
#define DSYS_MAXTWAIT		12
static taskw	waittable[DSYS_MAXTWAIT];
static taskw_t	waitfree;
static taskw_t	waitrun;










/**	\brief
 *
 *	Timer interrupt service routine.
 *	We must clear TIMER1 and TIMER EOI here.
 */
void timerisr(void) 
{
	SysTimerTicks+=1;
	Gsys_msec+=1;

	fiq_occurs[FIQ_TIMER]++;

#if 1
	// just for test
	{
		static unsigned long tmp_SysTimerTicks = 0;
		if (++tmp_SysTimerTicks >= 1000){
			tmp_SysTimerTicks = 0;
			// ShowIndividualLED
//			VPlong(S2E_GPIO_BASE + GPIO_DATA) ^= 0x0020;		// show Debug LED
		}
	}
#endif

	Inw(S2E_TIMER_BASE + TIMER1_EOI);	// clear EOI by reading it.
	Inw(S2E_TIMER_BASE + TIMER_EOI);		// clear EOI by reading it.
}



/**	\brief
 *
 *	Timer Initial function. Including ISR setting , timer count setting and wait queue initialization.
 *
 */
void	timer_init(void)
{
	int i;
	SysTimerTicks = 0;
	Gsys_msec = 0;

	timer_disable();	/* Disable Timer1 */

	DISABLE_FIQ(FIQ_TIMER);

	isr_set_isr_fast(FIQ_TIMER, timerisr);

	timer_set_reload_value(APB_CLK/1000);		//  1 msec

	ENABLE_FIQ(FIQ_TIMER);
	// S2E can choose mode , using User-Defined mode
	timer_enable();	/* Enable Timer1 */

	for ( i=0; i<DSYS_MAXTWAIT; i++ )
		waittable[i].next = &waittable[i+1];
	waittable[DSYS_MAXTWAIT-1].next = NULL;
	waitfree = waittable;
	waitrun = NULL;
}




/**	\brief
 *
 *	Set TIMER1 reload count
 *
 *	\param[in]	reload_value : clock count
 *
 */
void timer_set_reload_value(unsigned long reload_value)
{
	VPlong(S2E_TIMER_BASE + TIMER1_LOAD_COUNT) = (unsigned long)reload_value;
}



/**	\brief
 *
 *	Enable TIMER1
 *
 */
void timer_enable(void)
{
	VPlong(S2E_TIMER_BASE + TIMER1_CONTROL) |= (TIMER1_ENABLE | TIMER1_INT_UNMASK | TIMER1_USER_DEFINE_MODE);	/* Enable Timer1 */
}



/**	\brief
 *
 *	Disable TIMER1
 *
 */
void timer_disable(void)
{
	VPlong(S2E_TIMER_BASE + TIMER1_CONTROL) &= ~(ulong)TIMER1_ENABLE;	/* Disable Timer1 */
}












/**	\brief	
 *
 *	sleep in milliseconds
 *
 *	\param[in]	ms : milliseconds to sleep
 *
 */
void	sleep(int ms) 
{
	unsigned long t;

	t = SysTimerTicks;
	for (;;) {
		if ((SysTimerTicks - t) >= ms){
			break;
		}
	}
}




/**	\brief
 *
 *	Put event to wait queue
 *
 *	\param[in]	(*func)(void) : function to be execute
 *	\param[in]	ms : milliseconds
 *
 *	\retval 0		success
 *  	\retval -1	no free space
 */
int TimerOut(int (*func)(void), int ms)
{
	taskw_t tw;
	taskw_t tp;
	taskw_t tf;
	ulong	tm;

	tw = tp = waitrun;
	while ( tw != NULL ) {
		if ((tw->func) == func ) {
			tm = (Gsys_msec + ms);
			if ( (long)(tm - tw->time) >= 0 ){
				return 0;
			}
			if ( tw == waitrun ) {
				tw->time = tm;
				return 0;
			}
			tp->next = tw->next;	
			tw->next = waitfree;
			waitfree = tw;
			break;
		}
		tp = tw;
		tw = tw->next;
	}

	if ( waitfree == NULL )
		return (-1);
	
	tm = (Gsys_msec + ms);
	waitfree->func = func;
	waitfree->time = tm;
	tf = waitfree;
	waitfree = waitfree->next;
	
	if ( (tw = waitrun) == NULL ) {
		waitrun = tf;
		waitrun->next = NULL;
	} 
	else {
		while ( tw != NULL ) {
			if (((long)tm - (long)tw->time) < 0 ) {
				if ( tw == waitrun ) {
					tf->next = waitrun;
					waitrun = tf;
				} 
				else {
					tp->next = tf;
					tf->next = tw;
				}
				break;
			}	
			tp = tw;
			tw = tw->next;
		}

		if ( tw == NULL ) {
			tp->next = tf;
			tf->next = NULL;
		}
	}
	return 0;
}



/**	\brief
 *
 *	Polling the wait queue and check if the time expired
 *
 *	\retval 0		success
 */
int	TimerPoll(void)
{
	int	n;
	long	tm;
	taskw_t tw;

	n = 0;
	while ( (tw = waitrun) != NULL ) {
	    (ulong)tm = (ulong)((ulong)Gsys_msec - (ulong)waitrun->time);
	    if ( tm < 0 )
		break;
	    waitrun = tw->next;
	    tw->next = waitfree;
	    waitfree = tw;
	    tw->func();
	    n++;
	    if ( n > 5 )
		break;
	}
	return 0;
}
