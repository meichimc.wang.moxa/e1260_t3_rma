/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	console.c

	console library

	2008-06-10	Albert Yu		
		new release
	2008-09-25	Chin-Fu Yang		
		modified		
    
*/

#include "types.h"
#include "lib.h"
#include "moxauart.h"
#include "console.h"
#include "chipset.h"


extern int vsprintf(char *buf, const char *fmt, va_list args);
extern int  ConsolePort;
extern int strlen(const char * s);

static char RUNBAR_2[4] = {'\\', '|', '/', '-'};
static int console_init = FALSE;
static char digits[]="0123456789ABCDEF";
static char *BreakString="\b \b";


#define ASIC_TEST_CHANGE_CLOCK_SOURCE		// ZarZar
/**	\brief
 *
 *	Init console
 *
 *	\param[in]	port : UART port number
 *	\param[in]	baud : UART baudrate
 *
 */
void ConInit(int port,ulong baud)
{

#ifdef ASIC_TEST_CHANGE_CLOCK_SOURCE
	int dll_reg = BAUDRATE_115200; 
	int dlm_reg = 0 ;
#endif	

	m_sio_init();
	if (port == GENERAL_UART_PORT){
		m_sio_open(port, MOXAUART_TX_POLLING_MODE);	/* TX ISR mode will interrupt SPI transmission, so we use polling mode */
		m_sio_fifoctrl(port, 1, 0);
	}else{
		m_sio_open(port, 0);
	}
	
#ifdef ASIC_TEST_CHANGE_CLOCK_SOURCE
	if (port == ConsolePort){
		m_sio_ioctl_dll_dlm(port, dll_reg , dlm_reg, 0x3);   
	}else{
		
		m_sio_ioctl(port, baud, 0x3);   
	}
#else
	m_sio_ioctl(port, baud, 0x3);   
#endif

	m_sio_lctrl(port, DTR_ON|RTS_ON);
	ConsolePort = port;
	console_init = TRUE;
}



/**	\brief
 *
 *	Close the console.
 *
 *	\param[in]	port : UART port number
 *
 */
void ConClose(int port)
{
	console_init = FALSE;
	m_sio_close(port);
}



 
 /**	\brief
 *
 *	Get a byte of date from console port
 *
 *	\retval 		Data read from console port
 */
uchar Getch(void)
{
	uchar	ch;
	int	q;

	if(console_init == FALSE){
		return 0;		// not open console
	}
	for (;;) {
		q = m_sio_iqueue(ConsolePort);
		if (q < 0) {
			ch = 0xff;
			break;
		} else if (q == 0)
			continue;
		else {
			q = m_sio_getch(ConsolePort);
			ch = q & 0xff;
			break;
		}
	}
	return(ch);
}


 

/**	\brief
 *
 *	Return if data available on console rx buffer
 *
 *	\retval 		1:Yes, data is available ; 0: no data available
 */
int Kbhit(void)
{
	int	q;

	if(console_init != 1){		
		return FALSE;		// Console is not open
	}

	q = m_sio_iqueue(ConsolePort);	// return remaining bytes of RX buffer in SDRAM
	if (q <= 0){	
		return FALSE;
	}else{
		return TRUE;
	}
}


 
 /**	\brief
 *
 *	Write a string to console
 *
 *	\param[in]	*fmt : a string 
 *
 */
void Printf(const char *fmt, ...)
{
 	va_list	args;
	uint	i;
	char printbuffer[256];

	if(console_init == FALSE){
		return;
	}
	va_start(args, fmt);

	/* For this to work, printbuffer must be larger than
	 * anything we ever want to print.
	 */
	i = vsprintf(printbuffer, fmt, args);
	va_end(args);

	/* Print the string */
	m_sio_write(ConsolePort, printbuffer, StrLength(printbuffer));
}




/**	\brief
 *
 *	Write a simple string to console.
 *	No Printf( )
 *
 *	\param[in]	*str : a string
 *
 */
void PrintStr(char *str)
{
	Write(str, strlen(str));
}



/**	\brief
 *
 *	Write a simple HEX value to console.
 *	No Printf( ) 
 *
 *	\param[in]	value : a variable with 4 bytes long
 *
 */
void PrintHex(ulong value)
{
	int i, idx;

	for (i = 7; i >= 0; i--)
	{
		idx = value >> (i*4);		// 0x00000000
		m_sio_putch(ConsolePort, digits[idx&0xf]);
	}
}


/**	\brief
 *
 *	Write the data that is in buffer to Console.
 *
 *	\param[in]	*buf : the data to write to console
 *	\param[in]	len : data length
 *
 *	\retval 		error code
 */
void	Write(char *buf,int len)
{
	m_sio_write(ConsolePort, buf, len);
}



/**	\brief
 *
 *	Convert a ushort data to string format and write to console
 *
 *	\param[in]	value : data to be converted
 *
 */
void	WriteHex(ushort value)
{
	uchar		ch;
	static uchar	buf[6];

	ch = value & 0xffff;
	buf[0] = ch >> 4;
	if (buf[0] < 10)
	    buf[0] += '0';
	else
	    buf[0] += ('A' - 10);
	buf[1] = ch & 0x0f;
	if (buf[1] < 10)
	    buf[1] += '0';
	else
	    buf[1] += ('A' - 10);
	ch = value >> 8;
	buf[2] = ch >> 4;
	if (buf[2] < 10)
	    buf[2] += '0';
	else
	    buf[2] += ('A' - 10);
	buf[3] = ch & 0x0f;
	if (buf[3] < 10)
	    buf[3] += '0';
	else
	    buf[3] += ('A' - 10);
	buf[4] = ' ';
	buf[5] = ' ';
	Write(buf,6);
}



 /**	\brief
 *
 *	Convert a uchar data to string format and write to console
 *
 *	\param[in]	ch : data to be converted
 *
 */
void	WriteByte(uchar ch)
{
	static uchar	buf[2];

	buf[0] = ch >> 4;
	if (buf[0] < 10)
	    buf[0] += '0';
	else
	    buf[0] += ('A' - 10);
	buf[1] = ch & 0x0f;
	if (buf[1] < 10)
	    buf[1] += '0';
	else
	    buf[1] += ('A' - 10);
	Write(buf,2);
}




/**	\brief
 *
 *	Get a string from console.
 *
 *	\param[in]	target_address : a specific address
 *
 *	\retval 		error code
 */
int	ConGetString(char *str, int len)
{
	short ret=0;
	char	ch;

	while ( ret < (len-1) ) {
		
		ch = Getch();
		if((ret==0)&&(ch==ENTER))return (-1);
		if ( ch == BR) {
			if ( ret > 0 ) {
				PrintStr(BreakString);
				ret--;
			}
			continue;
		}else if(ch == ESC){
			return (-1);
		}
		
		Write((unsigned char *)&ch,1);
		
		if ( ch == CR || ch == NL ){
			break;
		}
		
		str[ret++] = ch;
	}
	
	str[ret] = 0;
	
	return(ret);
}




/**	\brief
 *
 *	Show runbar to console.
 *
 *	\param[in]	count : runbar counter
 *	\param[in]	speed : runbar's speed of display
 *
 *	\retval 		error code
 */
void Runbar(u32 count, u32 speed)
{
	static int i=0;
	if ((count%speed) == 0)
	{
		Printf("%c\b", RUNBAR_2[(i++)%4]);
	}
	return;
}
