/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	moxauart.c

	Functions of MOXA UART.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified	
    
*/


 
#include "chipset.h"
#include "types.h"
#include "lib.h"
#include "isr.h"
#include "global.h"
#include "moxauart.h"
#include "console.h"


int SlotNum=0;					// for PCI , 0 : no PCI
int MoxauartFoundPortNo=0;		// count
static UINT8 moxauart_interrupt_mask=0xff;		// for MOXAUART interrupt vector register
static UINT32 UartBase[MAX_PCI_SLOT+1];			// define the Base Address of each slot , +1 is for CPU embedded UART
static UINT32 InterruptBase[MAX_PCI_SLOT+1];
static UINT32 ModeReg0[MAX_PCI_SLOT+1];
static UINT32 ModeReg1[MAX_PCI_SLOT+1];
static UINT32 IDBase[MAX_PCI_SLOT+1];

// ZarZar
UINT8 moxa_shadow_dll;
UINT8 moxa_shadow_dlm;


static BOOL moxauart_found_flag = FALSE;
volatile static BOOL moxauart_init = FALSE;					// initialled or not
MoxaUartStruct  moxauart_info[MOXAUART_MAX_PORT];		// Record some information (software data structure)
													

// counting the occurrence times of different interrupts
int isr_total_count=0;
int isr_count[MOXAUART_MAX_PORT];
int isr_rx[MOXAUART_MAX_PORT];
int isr_tx[MOXAUART_MAX_PORT];
int isr_ls[MOXAUART_MAX_PORT];
int isr_ms[MOXAUART_MAX_PORT];
int lsr_bi[MOXAUART_MAX_PORT];
int lsr_fe[MOXAUART_MAX_PORT];
int lsr_pe[MOXAUART_MAX_PORT];
int lsr_oe[MOXAUART_MAX_PORT];
int isr_other[MOXAUART_MAX_PORT];
int int_tx[MOXAUART_MAX_PORT];
int isr_Xchange[MOXAUART_MAX_PORT];
int isr_RCchange[MOXAUART_MAX_PORT];


// replace Serial_reg.h for multiple buese support
// S2E data bus = 32bits , so that APB-BUS address need to be multiplied by 4
int MX_UART_RX[SUPPORT_BUSES] = {UART_RX, UART_RX, UART_RX*4};			// Receiver Buffer Register (READ ONLY)
int MX_UART_TX[SUPPORT_BUSES] = {UART_TX, UART_TX, UART_TX*4};			// Transmitter Buffer Register (WRITE ONLY)
int MX_UART_DLL[SUPPORT_BUSES] = {UART_DLL, UART_DLL, UART_DLL*4};		// Divisor Latch
int MX_UART_TRG[SUPPORT_BUSES] = {UART_TRG, UART_TRG, UART_TRG*4};		// ... for XR16C85x only

int MX_UART_DLM[SUPPORT_BUSES] = {UART_DLM, UART_DLM, UART_DLM*4};		// Latch
int MX_UART_IER[SUPPORT_BUSES] = {UART_IER, UART_IER, UART_IER*4};		// Interrupt Enable Register
int MX_UART_FCTR[SUPPORT_BUSES] = {UART_FCTR, UART_FCTR, UART_FCTR*4};	// ... for XR16C85x only

int MX_UART_IIR[SUPPORT_BUSES] = {UART_IIR, UART_IIR, UART_IIR*4};		// Interrupt Ident. Register (READ ONLY)
int MX_UART_FCR[SUPPORT_BUSES] = {UART_FCR, UART_FCR, UART_FCR*4};		// FIFO Control Register (WRITE ONLY)
int MX_UART_EFR[SUPPORT_BUSES] = {UART_EFR, UART_EFR, UART_EFR*4};		// ... for 16C660 only ___ Extended Features Register

int MX_UART_LCR[SUPPORT_BUSES] = {UART_LCR, UART_LCR, UART_LCR*4};		// Line Control Register
int MX_UART_MCR[SUPPORT_BUSES] = {UART_MCR, UART_MCR, UART_MCR*4};	// Modem Control Register
int MX_UART_LSR[SUPPORT_BUSES] = {UART_LSR, UART_LSR, UART_LSR*4};		// Line Status Register
int MX_UART_MSR[SUPPORT_BUSES] = {UART_MSR, UART_MSR, UART_MSR*4};	// Modem Status Register
int MX_UART_SCR[SUPPORT_BUSES] = {UART_SCR, UART_SCR, UART_SCR*4};		// Scratch Register

int MOXA_MUST_GDL_REGISTER[SUPPORT_BUSES] = {E_UART_GDL, E_UART_GDL, E_UART_GDL*4};		// (Enhanced) Good Data Length (READ ONLY)
int MOXA_MUST_EFR_REGISTER[SUPPORT_BUSES] = {E_UART_EFR, E_UART_EFR, E_UART_EFR*4};		// (Enhanced) Extended Features Register 

/* LCR = 0xBF , EFR bit6 = 0 , EFR bit7 = 0  (BANK 0) */
int MOXA_MUST_XON1_REGISTER[SUPPORT_BUSES] = {E_UART_XON1, E_UART_XON1, E_UART_XON1*4};
int MOXA_MUST_XON2_REGISTER[SUPPORT_BUSES] = {E_UART_XON2, E_UART_XON2, E_UART_XON2*4};
int MOXA_MUST_XOFF1_REGISTER[SUPPORT_BUSES] = {E_UART_XOFF1, E_UART_XOFF1, E_UART_XOFF1*4};
int MOXA_MUST_XOFF2_REGISTER[SUPPORT_BUSES] = {E_UART_XOFF2, E_UART_XOFF2, E_UART_XOFF2*4};

/* LCR = 0xBF , EFR bit6 = 0 , EFR bit7 = 1  (BANK 1) */
int MOXA_MUST_RBRTL_REGISTER[SUPPORT_BUSES] = {E_UART_RBRTL, E_UART_RBRTL, E_UART_RBRTL*4};
int MOXA_MUST_RBRTH_REGISTER[SUPPORT_BUSES] = {E_UART_RBRTH, E_UART_RBRTH, E_UART_RBRTH*4};
int MOXA_MUST_RBRTI_REGISTER[SUPPORT_BUSES] = {E_UART_RBRTI, E_UART_RBRTI, E_UART_RBRTI*4};
int MOXA_MUST_THRTL_REGISTER[SUPPORT_BUSES] = {E_UART_THRTL, E_UART_THRTL, E_UART_THRTL*4};

/* LCR = 0xBF , EFR bit6 = 1 , EFR bit7 = 0  (BANK 2) */
int MOXA_MUST_ENUM_REGISTER[SUPPORT_BUSES] = {E_UART_ENUM, E_UART_ENUM, E_UART_ENUM*4};
int MOXA_MUST_HWID_REGISTER[SUPPORT_BUSES] = {E_UART_HWID, E_UART_HWID, E_UART_HWID*4};
int MOXA_MUST_ECR_REGISTER[SUPPORT_BUSES] = {E_UART_ECR, E_UART_ECR, E_UART_ECR*4};
int MOXA_MUST_CSR_REGISTER[SUPPORT_BUSES] = {E_UART_CSR, E_UART_CSR, E_UART_CSR*4};




/**	\brief
 *
 *	The real ISR of MOXA UART, moxauart_isr() is just only a dispatcher
 *
 *	\param[in]	port : UART ID
 *
 */
static void moxauart_isr2(int port)		// which port generates this interrupt
{
	pMoxaUartStruct info;
	UINT8 iir;
	UINT8 lsr;
	UINT8 gdl;
	UINT8 ch;
	int wptr;
	int rptr;
	int i;
	
	info = &moxauart_info[port];			// point to uart structure
	iir = Inw(info->base+MX_UART_IIR[info->bus_type]);		// get IIR to decide which kind of UART Interrupt occurs
#ifdef MOXAUART_DEBUG
	Printf("\nMoxauart:port %d enter isr2 interrupt service, iir=%x",port, iir);
#endif
	// 1. check the UART Interrupt Pending
	if ( iir & UART_IIR_NO_INT ) {
		// If IIR has no Interrupt pending  , BIT0 is set to 1 ,  otherwise BIT0 is set to 0.
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 interrupt service bad", port);
#endif
		isr_other[port]++;
		return;
	}
	
	if ( !(info->flag & MOXAUART_OPENED) ) {
		// MOXAUART does not set and the interrupt occurs .  It must be error.
		Inw(info->base+MX_UART_LSR[info->bus_type]);
		Inw(info->base+MX_UART_MSR[info->bus_type]);
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 interrupt service not opened", port);
#endif
		isr_other[port]++;
		return;
	}


	// 2. which kind of interrupt occurs
	iir &= MOXA_MUST_IIR_MASK;

	// 3. Handle GDA , RDA , RTO first.
	if ( iir == MOXA_MUST_IIR_GDA ||
		iir == MOXA_MUST_IIR_RDA ||
		iir == MOXA_MUST_IIR_RTO ) {
		gdl = Inw(info->base+MOXA_MUST_GDL_REGISTER[info->bus_type]);		// offset 0x07
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 receive interrupt service, gdl=%x",port, gdl);
#endif

		wptr = info->rxwptr;
		rptr = (info->rxrptr - 1) & MOXAUART_BUFFER_MASK;
		if(info->flag & MOXAUART_HAS_ENHANCE_MODE){
			// If we using MOXAUART Enhance mode , we need to handle the data of GDL because the GDA Mode is belong to Enhanced Mode
			while ( gdl-- ) {	// Good Data Length , so accept the remain data from FIFO
		    		ch = Inw(info->base + MX_UART_RX[info->bus_type]);
		    		if ( rptr != wptr ) {
		    			info->rxbuf[wptr++] = ch;
		    			wptr &= MOXAUART_BUFFER_MASK;
		    		}
			}
		}else{
			// If we don't use Enhanced Mode , do normal operation
			while(1){
				lsr = Inw(info->base+MX_UART_LSR[info->bus_type]);
				if(lsr & UART_LSR_DR){
		    		ch = Inw(info->base + MX_UART_RX[info->bus_type]);
			    		if ( rptr != wptr ) {
			    			info->rxbuf[wptr++] = ch;
			    			wptr &= MOXAUART_BUFFER_MASK;
			    		}				
				}else{
					break;
				}
			}	
		}
		info->rxwptr = wptr;
		isr_rx[port]++;

		
	} else if ( iir == UART_IIR_THRI ) {
	// 4. Handle THRE Interrupt , When this interrupt occurs , it says the FIFO is empty , so we can input our data to FIFO.
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 transmit interrupt service",port);
#endif
		rptr = info->txrptr;
		wptr = info->txwptr;
		for ( i=0; i<MOXA_MUST_TX_FIFO_SIZE; i++ ) {
			if ( rptr == wptr ) {
				// Turn off THRI , It will be turn on again in m_sio_write()
				info->IER &= ~UART_IER_ETBEI;
				Outw((info->base + MX_UART_IER[info->bus_type]),info->IER);
				break;
			}
			Outw((info->base+MX_UART_TX[info->bus_type]),info->txbuf[rptr++]);
			rptr &= MOXAUART_BUFFER_MASK;
		}
		info->txrptr = rptr;
		isr_tx[port]++;

		
	} else if ( iir == UART_IIR_MSI ) {
	// 5. Handle Modem Status Interrupt , It can detect signal change of DCD , DSR and CTS.
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 modem status interrupt service",port);
#endif
		// clear by reading MSR register
		Inw(info->base + MX_UART_MSR[info->bus_type]);	// clear modem change interrupt
		isr_ms[port]++;

		
	} else if ( iir == MOXA_MUST_IIR_LSR ) {
	// 6. Handle the change of Receive Line Status , (Overrun Error , Parity Error , Frame Error , Break Interrupt)
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart:port %d enter isr2 line status interrupt service",port);
#endif
		// clear by reading LSR register
		lsr = Inw(info->base+MX_UART_LSR[info->bus_type]);
		if ( lsr & UART_LSR_SPECIAL ) {
			// LSR has many types of situation , We perform different handlers for different situation
#ifdef MOXAUART_DEBUG
	Printf("\nMoxauart:port %d enter isr2 LSR interrupt service", port);
#endif
			ch = Inw(info->base + MX_UART_RX[info->bus_type]);
			if( lsr&UART_LSR_BI)
				lsr_bi[port]++;
			if(lsr&UART_LSR_FE)
				lsr_fe[port]++;	
			if(lsr&UART_LSR_PE)
				lsr_pe[port]++;					
			if(lsr&UART_LSR_OE)
				lsr_oe[port]++;					
			if ( (lsr&UART_LSR_OE) && !(lsr&UART_LSR_DR) )
				Outw(info->base+MX_UART_FCR[info->bus_type],0x23);				
		}
		isr_ls[port]++;

		
	}else if( iir & UART_IIR_XCH ) {
	// 7. Handle XON/XOFF Change Status  (Only for MOXAUART)
	    isr_Xchange[port]++;

	
	}else if( iir & UART_IIR_RCH){
	// 8. Handle CTS / RTS Change Interrupt  (Only for MOXAUART)
		isr_RCchange[port]++;
	}else{
		isr_other[port]++;
	}
#ifdef MOXAUART_DEBUG
	Printf("\nMoxauart:port %d leave isr2 interrupt service",port);
#endif
}







// UART Interrupt dispatcher 
static void moxauart_isr(void)
{
	UINT8 intpending;
	int i;

	isr_total_count++;
	//	irq_occurs[IRQ_MOXA_UART]++;

#ifdef MOXAUART_DEBUG
	Printf("\nMoxauart: enter main interrupt service");
#endif

	intpending = Inw(InterruptBase[0]) & moxauart_interrupt_mask;		// pending is Active-Low
	if ( intpending == moxauart_interrupt_mask ){		// mask = 0xFF
		// No Interrupt occurs by any Uart , do nothing.
		// If any interrupt occurs by any UART , pending bit will become 0 (active-low)
	}else{
		// UART of MOXA CPU
		for(i=0;i<2;i++){		// 2 MOXA UARTs
			if ( !(intpending & moxauart_info[i].vector_mask) ){	// This equation will decide which UART generate the interrupt.
				isr_count[i]++;
				moxauart_isr2(i);
			}
		}
	}	           	            	            	            	    
        
#ifdef MOXAUART_DEBUG
	Printf("\nMoxauart: leave main interrupt service");
#endif
}


static void generaluart_isr(void)
{
	int port = GENERAL_UART_PORT;
	pMoxaUartStruct info;
	UINT8 iir;
	UINT8 lsr;
	UINT8 ch;
	int wptr;
	int rptr;
	int i;

	isr_count[port]++;
	
	info = &moxauart_info[port];	
	iir = Inw(info->base+MX_UART_IIR[info->bus_type]);		// get IIR to decide which kind of UART Interrupt occurs
	// 1. check the UART Interrupt Pending
	if ( iir & UART_IIR_NO_INT ) {
		// If IIR has no Interrupt pending  , BIT0 is set to 1 ,  otherwise BIT0 is set to 0.
		isr_other[port]++;
		return;
	}
	
	if ( !(info->flag & MOXAUART_OPENED) ) {
		// MOXAUART does not set and the interrupt occurs .  It must be error.
		Inw(info->base+MX_UART_LSR[info->bus_type]);
		Inw(info->base+MX_UART_MSR[info->bus_type]);
		isr_other[port]++;
		return;
	}


	// 2. which kind of interrupt occurs
	iir &= 0x0F;							// by Albert #244
	//iir &= MOXA_MUST_IIR_MASK;				// by source

	// 3. Handle GDA , RDA , RTO first.
	if ( iir == MOXA_MUST_IIR_RDA ||
		iir == MOXA_MUST_IIR_RTO ) {

		wptr = info->rxwptr;
		rptr = (info->rxrptr - 1) & MOXAUART_BUFFER_MASK;
		while(1){
			lsr = Inw(info->base+MX_UART_LSR[info->bus_type]);
			if(lsr & UART_LSR_DR){
	    		ch = Inw(info->base + MX_UART_RX[info->bus_type]);
		    		if ( rptr != wptr ) {
		    			info->rxbuf[wptr++] = ch;
		    			wptr &= MOXAUART_BUFFER_MASK;
		    		}				
			}else{
				break;
			}
		}
		info->rxwptr = wptr;
		isr_rx[port]++;

		
	} else if ( iir == UART_IIR_THRI ) {
	// 4. Handle THRE Interrupt , When this interrupt occurs , it says the FIFO is empty , so we can input our data to FIFO.
		rptr = info->txrptr;
		wptr = info->txwptr;
		for ( i=0; i<GENERAL_UART_TX_FIFO_SIZE; i++ ) {
			if ( rptr == wptr ) {
				// Turn off THRI , it will be turned on again in m_sio_write()
				info->IER &= ~UART_IER_ETBEI;
				Outw((info->base + MX_UART_IER[info->bus_type]),info->IER);
				break;
			}
			if (info->FCR & UART_FCR_ENABLE_FIFO)
				Outw((info->base+MX_UART_TX[info->bus_type]),info->txbuf[rptr++]);
			else {
				if (Inw(info->base+MX_UART_LSR[info->bus_type]) & UART_LSR_THRE)
					Outw(info->base+MX_UART_TX[info->bus_type], info->txbuf[rptr++]);		
			}
			rptr &= MOXAUART_BUFFER_MASK;
		}
		info->txrptr = rptr;
		isr_tx[port]++;

		
	} else if ( iir == UART_IIR_MSI ) {
	// 5. Handle Modem Status Interrupt , It can detect signal change of DCD , DSR and CTS.
		// clear by reading MSR register
		Inw(info->base + MX_UART_MSR[info->bus_type]);	// clear modem change interrupt
		isr_ms[port]++;

		
	} else if ( iir == MOXA_MUST_IIR_LSR ) {
	// 6. Handle the change of Receive Line Status , (Overrun Error , Parity Error , Frame Error , Break Interrupt)
		// clear by reading LSR register
		lsr = Inw(info->base+MX_UART_LSR[info->bus_type]);
		if ( lsr & UART_LSR_SPECIAL ) {
			// LSR has many types of situation , We perform different handlers for different situation
			ch = Inw(info->base + MX_UART_RX[info->bus_type]);
			if( lsr&UART_LSR_BI)
				lsr_bi[port]++;
			if(lsr&UART_LSR_FE)
				lsr_fe[port]++;	
			if(lsr&UART_LSR_PE)
				lsr_pe[port]++;					
			if(lsr&UART_LSR_OE)
				lsr_oe[port]++;					
			#if 0
			if ( (lsr&UART_LSR_OE) && !(lsr&UART_LSR_DR) )
			{
				if (info->FCR & UART_FCR_ENABLE_FIFO)
					Outw(info->base+MX_UART_FCR[info->bus_type],0x03);
				else
					Outw(info->base+MX_UART_FCR[info->bus_type],0x02);
			}
			#endif
		}
		isr_ls[port]++;

	} else if ( iir == 0x07	/* Busy detect indication*/ ) {
		PrintStr("DW UART INT: iir=0x07\r\n");
		// clear by reading the UART status register
		Inw(info->base + 0x7C/* UART Status Register */);
	}
}


//##################################### MOXAUART Function ################################################
//---------------------------------------------------------------------------------------------
// Function Name: moxa_uart_init
// Description:
//---------------------------------------------------------------------------------------------


/**	\brief
 *
 *	1. Setting the software data strcture of MOXA UART
 *	2. Setting ISR
 *
 */
void m_sio_init(void)
{
    	int i;

 	if(moxauart_init == TRUE){		// If we have set it already , skip it.
	    	return;
	}
	SlotNum = 0;
	isr_total_count=0;				// clear
	
	// 1. Initial every value of UART ISR Counting Array.
	for(i=0;i<MOXAUART_MAX_PORT;i++){	
	    	isr_count[i] = isr_rx[i] = isr_tx[i] = isr_ls[i] = isr_ms[i] = isr_other[i] = int_tx[i] = isr_RCchange[i] = isr_Xchange[i] = 0;
		lsr_bi[i] = lsr_fe[i] = lsr_pe[i] = lsr_oe[i] = 0;
	}   

	// 2. Initiate every Base Address , "MAX_PCI_SLOT+1" is because PCI-UART + CPU-UART , the number of S2E's PCI-UART is 0.
	for(i=0; i<(MAX_PCI_SLOT+1); i++){
		// This InterruptBase[] denotes the global interrupts of UART , Active Low !!
		// If any interrupt of UART ocurrs , it will pull down the hardware line to this address. set to pending state.
		// This ModeReg[] denodes connector type including RS-232 , RS-422 and RS-485.
		UartBase[i] = InterruptBase[i] = ModeReg0[i] = ModeReg1[i] = IDBase[i] = 0;
	}

	// 3. Setting the base address of CPU-UART(UART0).
	UartBase[SlotNum] = MOXA_EMBEDDED_UART_BASE_REG; 
	InterruptBase[SlotNum] = MOXA_EMBEDDED_UART_BASE_INT;		// one UART uses 1 bit
	ModeReg0[SlotNum] = MOXA_EMBEDDED_UART_BASE_MODE;		// one UART uses 2 bits
	ModeReg1[SlotNum] = MOXA_EMBEDDED_UART_BASE_MODE;		// S2E has only 2 MoxaUART , so that we need only the ModeReg0 .
	IDBase[SlotNum] = 0;

	// 4. Setting the data structure of CPU UART .
	for(i=0;i<MOXAUART_CPU_MAX;i++){			// MOXAUART_CPU_MAX means CPU embedded UART.
		
		if (i == GENERAL_UART_PORT){		// GENERAL_UART_PORT is a 16550C compatible UART, but is not a MOXA UART.
			/* General UART */
			moxauart_info[i].base = S2E_DW_UART_BASE;
			moxauart_info[i].FCR = UART_FCR_ENABLE_FIFO;
			// There is no mode registers for general UART, we direct it to
			// read only Component Type Register
			moxauart_info[i].mode_reg0 = 0xFC;	// No ModeReg to set , but set as RS-232 mode.					
			moxauart_info[i].mode_reg1 = 0xFC;	// No ModeReg to set
			
		}else{
			/* MOXA UART */
			moxauart_info[i].base = UartBase[0] + (0x00000020 * i);		// multiply by "8 register * 4 bytes" , UartBase[0] = CPU UART's BASE
			moxauart_info[i].FCR = 0;
			// One ModeReg can control 4 MoxaUART , so that we only need one ModeReg in S2E.
			// In this case , ModeReg0[0] is the same as ModeReg1[0].
			moxauart_info[i].mode_reg0 = ModeReg0[0];				// ModeReg0[0] = CPU UART's ModeReg0 Address
			moxauart_info[i].mode_reg1 = ModeReg1[0];				// ModeReg1[0] = CPU UART's ModeReg1 Address
		}

		/* General + MOXA UART */
		moxauart_info[i].txbuf = (uchar *)(SDRAM_UART_TX_BASE(i));		// setting TX/RX memory space of SDRAM
		moxauart_info[i].rxbuf = (uchar *)(SDRAM_UART_RX_BASE(i));
		moxauart_info[i].txrptr = moxauart_info[i].txwptr = 0;			// Pointer of Ring Buffer 
		moxauart_info[i].rxrptr = moxauart_info[i].rxwptr = 0;
		moxauart_info[i].vector_mask = 0x01 << i;	// for each UART , there will be different mask to distiguish which UART generates this interrupt
												// for example, UART0 is 0b00000001 , UART1 is 0b00000010 .
		moxauart_info[i].flag = 0;
		moxauart_info[i].bus_type = MXSER_BUS_APB;				// S2E uses APB Bus
		moxauart_info[i].IER = moxauart_info[i].MCR = 0;			// initial
		moxauart_info[i].LCR = 0;
		moxauart_info[i].DLL = moxauart_info[i].DLM = 0;	   
		moxauart_info[i].XON1 = moxauart_info[i].XON2 = 0;
		moxauart_info[i].XOFF1 = moxauart_info[i].XOFF2 = 0;
		moxauart_info[i].RBRTL = moxauart_info[i].RBRTH = 0;
		moxauart_info[i].RBRTI = moxauart_info[i].THRTL = 0;
		moxauart_info[i].ENUM = moxauart_info[i].EFR = 0;	         		
		
	}

	// 5. Setting ISR 	// S2E's Interrupt source is Active-Low.
	isr_set_isr(IRQ_MOXA_UART, moxauart_isr);		

	// 6. Enable Interrupt 
	ENABLE_IRQ(IRQ_MOXA_UART);
	MoxauartFoundPortNo = MOXAUART_CPU_MAX;		// found 3 UART

    	// 7. Setting General UART's ISR
	isr_set_isr(IRQ_GENERAL_UART, generaluart_isr);		

	// 8. Enable General UART's Interrupt
	ENABLE_IRQ(IRQ_GENERAL_UART);

	moxauart_found_flag = TRUE;

	if(MoxauartFoundPortNo == 0){
	   	 Printf("Not find Moxa Device\r\n");
	   	 return;
	}  	 
    	moxauart_init = TRUE;          
}






// port : from 0 , ID of UART
// mode : SW/HW Flow control ,or Enhanced mode , or 0 (normal operation)
// success return 0 (MOXAUART_OK)

/**	\brief
 *
 *	1. Configure MOXA UART , open the port and set the operational mode
 *
 *	\param[in]	port : UART port
 *	\param[in]	operational_mode : MOXAUART_NORMAL_MODE / MOXAUART_HAS_HW_FLOW_CONTROL / MOXAUART_HAS_ENHANCE_MODE / ...
 *
 *	\retval 		error code
 */
int m_sio_open(int port,int operational_mode)
{
	pMoxaUartStruct info;	// point to Software UART structure

#ifdef MOXAUART_DEBUG     	
	Printf("moxauart open..");
#endif	
	BasicCheck1(port);	// check whether the UART port corrected.
	info = &moxauart_info[port];

	// 0. Set operational mode 
	if(operational_mode & MOXAUART_HAS_ENHANCE_MODE){
		info->flag |= MOXAUART_HAS_ENHANCE_MODE;		// store the Enhanced flag
	}
	if(operational_mode & MOXAUART_TX_POLLING_MODE) {
		info->flag |= MOXAUART_TX_POLLING_MODE;
	}
	if ( info->flag & MOXAUART_OPENED ){
		return MOXAUART_OK;		// If it opened before , then return
	}

	// 1. Disable UART Interrupt
	Outw(((info->base)+MX_UART_IER[info->bus_type]),0);

	// 2. Clear the FIFO of RX and TX in UART chip.
	Outw(((info->base)+MX_UART_FCR[info->bus_type]),(UART_FCR_CLEAR_RCVR|UART_FCR_CLEAR_XMIT));

	// 3. clear these registers by reading them.
	(void)Inw((info->base) + MX_UART_LSR[info->bus_type]);
	(void)Inw((info->base) + MX_UART_RX[info->bus_type]);
	(void)Inw((info->base) + MX_UART_IIR[info->bus_type]);
	(void)Inw((info->base) + MX_UART_MSR[info->bus_type]);

	// 4. setting N81 , RTS , DTR
	// The RTS and CTS control the data flow. We control the RTS to tell whether could them send the data.
	// Our RTS is connected to thier CTS . If we set our RTS as 1 , the CTS of thier side will be 1. So they can thansmit thier data to us.
	// If we set our RTS as 0 , the CTS of thier side will be 0 , too. Then they will monitor thier CTS and stopping transmit data.
	Outw(((info->base) + MX_UART_LCR[info->bus_type]),UART_LCR_N81);    /* reset DLAB */	// N81
	info->MCR |= UART_MCR_DTR|UART_MCR_RTS;		// pull-up DTR and RTS
	Outw(((info->base) + MX_UART_MCR[info->bus_type]),info->MCR);

	/*
	 * And clear the interrupt registers again for luck.
	 */
	 // 5. clear these 4 Registers again
	(void)Inw((info->base) + MX_UART_LSR[info->bus_type]);
	(void)Inw((info->base) + MX_UART_RX[info->bus_type]);
	(void)Inw((info->base) + MX_UART_IIR[info->bus_type]);
	(void)Inw((info->base) + MX_UART_MSR[info->bus_type]);

	// 6. Initial the function of MOXAUART Enhanced mode 
	if(info->flag & MOXAUART_HAS_ENHANCE_MODE){
		ENABLE_MOXA_MUST_ENCHANCE_MODE(info->base,info->bus_type);    	    
	}

	// 7. Set the FCR. General UART = UART_FCR_ENABLE_FIFO ; MOXA UART = 0 
	Outw(((info->base) + MX_UART_FCR[info->bus_type]),info->FCR);

	// 8. Set this port to MOXAUART_OPENED , so that this port will not open again.
	info->flag |= MOXAUART_OPENED;		// setting complete!!

	 // 9. Enable UART's Interrupt.  We enable bit0 for data ready and bit2 for data error.
	info->IER = (UART_IER_ELSI | UART_IER_ERBI);
	Outw(((info->base) + MX_UART_IER[info->bus_type]),info->IER); 
	return MOXAUART_OK;
}




/**	\brief
 *
 *	1. disable the interrupt of UART
 *	2. pull-down DTR / RTS
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		error code
 */
int m_sio_close(int port)
{
	pMoxaUartStruct info;


	BasicCheck1(port);
	info = &moxauart_info[port];
	
	// 1. if UART was not opened before , then Return
	if ( !(info->flag & MOXAUART_OPENED) ){
		return MOXAUART_OK;
	}
	
	// 2. Disable operational mode 
	info->flag = 0;

	// 3. Disable UART Interrupt
	info->IER = 0;
	Outw(((info->base)+MX_UART_IER[info->bus_type]),info->IER);

	// 4. Clear Ring Buffer of SDRAM
	info->txrptr = info->txwptr = info->rxrptr = info->rxwptr = 0;

	// 5. pull-down DTR / RTS
	info->MCR = 0;
	Outw(((info->base)+MX_UART_MCR[info->bus_type]),info->MCR);

	// 6. Clear Enhance Mode Register ...
	// ... to be continued ...
	
    	return MOXAUART_OK;
}



/**	\brief
 *
 *	1. change Baudrate
 *	2. control LCR to switch mode , for example,  N81
 *
 *	\param[in]	port : UART Port
 *	\param[in]	speed : RAW baud rate
 *	\param[in]	reg_lcr : LCR value
 *
 *	\retval 		error code
 */
int m_sio_ioctl(int port, int speed, int reg_lcr)
{
    	pMoxaUartStruct info;
	UINT32 val=0;
	UINT32 baud1;
	UINT32 baud2;
	UINT32 quotient=0;
	

	BasicCheck1(port);
	if ( speed < 50 || speed > SERIAL_RSA_BAUD_BASE ){		// 50 ~ 921600
		return MOXAUART_ERROR_PARAMETER;
	}

	info = &moxauart_info[port];

	// 1. Get the quotient
	quotient = SERIAL_RSA_BAUD_BASE / speed;		

	// 2. Set DLAB as 1 to swap register to DLL/DLM
	Outw((info->base + MX_UART_LCR[info->bus_type]), (reg_lcr | UART_LCR_DLAB) );	

	// 3. Configure baudrate by setting quotient to DLL/DLM
	info->DLL = quotient & 0xff;	// LSB
	info->DLM = quotient >> 8;		// MSB
	Outw((info->base + MX_UART_DLL[info->bus_type]),info->DLL);  		/* LS of divisor */
	Outw((info->base + MX_UART_DLM[info->bus_type]),info->DLM);	  	/* MS of divisor */


	moxa_shadow_dll = Inw(info->base + MX_UART_DLL[info->bus_type]);	// ZarZar
	moxa_shadow_dlm = Inw(info->base + MX_UART_DLM[info->bus_type]);	// ZarZar
	

	// 4. Set DLAB as 0.
	Outw((info->base + MX_UART_LCR[info->bus_type]), reg_lcr);		    	/* reset DLAB as 0*/

	// 5. Any Baudrate Algorithm
	if(info->ENUM == 0){
		
		val = ((OSC_CLK/(2*speed)) - (8*quotient));
		baud1 = OSC_CLK/(16*((quotient) + (val/8)));
		baud2 = OSC_CLK/(16*((quotient) + ((val + 1)/8)));
		
		if ((baud1 - speed) > (speed - baud2)){
			info->ENUM = val+1;
		}else{	
			info->ENUM = val;
		}	
		
	}
	
	if(info->flag & MOXAUART_HAS_ENHANCE_MODE){
		SET_MOXA_MUST_ENUM_VALUE(info->base, info->bus_type, info->ENUM);      
	}
	
	return MOXAUART_OK;
}

/**	\brief
 *
 *	
 *	Force the DLL and DLM setting.
 *
 *	\param[in]	port : UART Port
 *	\param[in]	dll : dll value
 *	\param[in]	dlm : dlm value
 *	\param[in]	reg_lcr : LCR value 
 *
 *	\retval 		error code
 */
int m_sio_ioctl_dll_dlm(int port, UINT8 dll , UINT8 dlm, int reg_lcr)
{
	pMoxaUartStruct info;

	BasicCheck1(port);

	info = &moxauart_info[port];
	Outw((info->base + MX_UART_LCR[info->bus_type]),reg_lcr | UART_LCR_DLAB);
	info->DLL = dll;
	info->DLM = dlm;
	Outw((info->base + MX_UART_DLL[info->bus_type]),info->DLL);  	/* LS of divisor */
	Outw((info->base + MX_UART_DLM[info->bus_type]),info->DLM);	  	/* MS of divisor */
	Outw((info->base + MX_UART_LCR[info->bus_type]),reg_lcr);		    /* reset DLAB */


    return MOXAUART_OK;
}






/**	\brief
 *
 *	1. Enable/Disable the Hardware flow control of Standard mode by control AFE bit in the MCR register.
 *
 *	\param[in]	port : UART port
 *	\param[in]	flag : Operational mode
 *
 *	\retval 		error code
 */
int m_sio_hardware_flowctrl(int port, int flag)
{
	pMoxaUartStruct info;

	BasicCheck2(port);		// BasicCheck1 + opened_check
	info = &moxauart_info[port];

	if ( flag & MOXAUART_HAS_HW_FLOW_CONTROL ) {
		//Enable HW flow control , so we should enable Autoflow Control Enable first
		info->MCR |= UART_MCR_AFE;
		info->flag |= MOXAUART_HAS_HW_FLOW_CONTROL;
	} else {
		// Otherwise, we disable Autoflow Control Enable 
		info->MCR &= ~UART_MCR_AFE;
		info->flag &= ~MOXAUART_HAS_HW_FLOW_CONTROL;
	}
	
    	Outw((info->base+MX_UART_MCR[info->bus_type]),info->MCR);
		
#ifdef MOXAUART_DEBUG    
	Printf("\r\nEnable HW flow!");
#endif    

    	return MOXAUART_OK;
}




/**	\brief
 *
 *	1. setting Software Flow Control
 *
 *	\param[in]	port : UART port
 *	\param[in]	XON1_s : character of XON1
 *	\param[in]	XOFF1_s : character of XOFF1_s
 *	\param[in]	XON2_s : character of XON2_s
 *	\param[in]	XOFF2_s : character of XOFF2_s
 *	\param[in]	configuration : Configuration
 *
 *	\retval 		error code
 */
int m_sio_software_flowctrl(int port, UINT8 XON1_s,UINT8 XOFF1_s,UINT8 XON2_s,UINT8 XOFF2_s,int configuration)
{
	pMoxaUartStruct info;

	BasicCheck1(port);
	info = &moxauart_info[port];    

	if(configuration != 0){    	
		/* Set these characters to corresponding registers */
		SET_MOXA_MUST_XON1_VALUE(info->base,info->bus_type, XON1_s);
		SET_MOXA_MUST_XOFF1_VALUE(info->base, info->bus_type, XOFF1_s);
		SET_MOXA_MUST_XON2_VALUE(info->base,info->bus_type, XON2_s);
		SET_MOXA_MUST_XOFF2_VALUE(info->base, info->bus_type, XOFF2_s);	
		/* Configure the Extended Features Register */
		SET_MOXA_MUST_SOFTWARE_FLOW_CONTROL_CONFIGURATION(info->base,info->bus_type,configuration);
	}else{
		/* Disable all software flow control settings */
		DISABLE_MOXA_MUST_RX1_SOFTWARE_FLOW_CONTROL(info->base, info->bus_type);
		DISABLE_MOXA_MUST_TX1_SOFTWARE_FLOW_CONTROL(info->base, info->bus_type);
		DISABLE_MOXA_MUST_RX2_SOFTWARE_FLOW_CONTROL(info->base, info->bus_type);
		DISABLE_MOXA_MUST_TX2_SOFTWARE_FLOW_CONTROL(info->base, info->bus_type);       
		/* Set "Xon ANY Bit" = 0 */
		DISABLE_MOXA_MUST_XON_ANY_FLOW_CONTROL(info->base, info->bus_type);     	
	}    
	return MOXAUART_OK;
	
}




/**	\brief
 *
 *	Control the FCR register to enable or disable FIFO
 *
 *	\param[in]	port : UART port
 *	\param[in]	enable : enable UART FIFO or not
 *	\param[in]	reg_fcr : some value to set into FCR
 *
 *	\retval 		error code
 */
int m_sio_fifoctrl(int port, int enable, int reg_fcr)
{
	pMoxaUartStruct info;

	BasicCheck1(port);

	info = &moxauart_info[port];
	
	if (enable){
		/* enable FIFO */
		info->FCR = (UART_FCR_CLEAR_RCVR|UART_FCR_CLEAR_XMIT|UART_FCR_ENABLE_FIFO| reg_fcr);
		info->flag |= MOXAUART_FIFO_ENABLED;
	}else{
		/* disable FIFO */
		info->FCR = 0;
		info->flag &= ~(UINT32)MOXAUART_FIFO_ENABLED;
	}
	
    	Outw(((info->base) + MX_UART_FCR[info->bus_type]),info->FCR);
	return MOXAUART_OK;
}




/**	\brief
 *
 *	1. Set water level of hardware flow control of Enhanced Mode.
 *	2. MOXA UART only
 *
 *	\param[in]	port : UART port
 *	\param[in]	RBRTL : Low Water , pull-up RTS when level of FIFO reaches RBRTL
 *	\param[in]	RBRTH : High Water , pull-down RTS when level of FIFO reaches RBRTH
 *	\param[in]	RBRTI : Intermediate Water , Interrupt will be generated when level of FIFO reaching RBRTI
 *	\param[in]	THRTL : TX Low Water 
 *
 *	\retval 		error code
 */
int m_sio_enhance_fifoctrl(int port, int RBRTL, int RBRTH, int RBRTI, int THRTL)
{
	pMoxaUartStruct info;

	BasicCheck1(port);

	info = &moxauart_info[port];
	/* Set water level */
	SET_MOXA_MUST_RBRTL_VALUE(info->base, info->bus_type, RBRTL); 
	SET_MOXA_MUST_RBRTH_VALUE(info->base, info->bus_type, RBRTH); 
	SET_MOXA_MUST_RBRTI_VALUE(info->base, info->bus_type, RBRTI); 
	SET_MOXA_MUST_THRTL_VALUE(info->base, info->bus_type, THRTL);
	info->FCR = (UART_FCR_CLEAR_RCVR|UART_FCR_CLEAR_XMIT|UART_FCR_ENABLE_FIFO);
	return MOXAUART_OK;
}




/**	\brief
 *
 *	Return free space of RX Buffer
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		error code
 */
int m_sio_ifree(int port)
{
	BasicCheck1(port);
	return (moxauart_info[port].rxrptr - moxauart_info[port].rxwptr - 1) & MOXAUART_BUFFER_MASK;
}




/**	\brief
 *
 *	Return free space of TX Buffer 
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		error code
 */
int m_sio_ofree(int port)
{
	BasicCheck1(port);
	return (moxauart_info[port].txrptr - moxauart_info[port].txwptr - 1) & MOXAUART_BUFFER_MASK;
}



/**	\brief
 *
 *	Return remaining bytes of RX Buffer
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		error code
 */
int m_sio_iqueue(int port)
{
	BasicCheck1(port);
	return (moxauart_info[port].rxwptr - moxauart_info[port].rxrptr) & MOXAUART_BUFFER_MASK;
}




/**	\brief
 *
 *	Return remaining bytes of TX Buffer in SDRAM
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		error code
 */
int m_sio_oqueue(int port)
{
	UINT32 num=0;	
	pMoxaUartStruct info;    
	UINT8 val=0;
    	
	BasicCheck1(port);
	info = &moxauart_info[port];
	num = (moxauart_info[port].txwptr - moxauart_info[port].txrptr) & MOXAUART_BUFFER_MASK;
	if(num == 0){	// If there is no data in buffer , we should check THRE Bit.
		m_sio_read_register(port, MX_UART_LSR[info->bus_type], &val);

		if(!(val & UART_LSR_THRE)){
			// If THRE set , then return 0 .  If THRE clear ,then return 1 , This is in order to avoid missing THRE signal
			num = 1;
		}
	}
    	return num;
}



/**	\brief
 *
 *	1. It will check remaining bytes of RX buffer first.
 *	2. Read from RX Buffer to specific location of SDRAM
 *
 *	\param[in]	port : UART port
 *	\param[in]	*buf : a specific location of SDRAM to put data.
 *	\param[in]	len : the length of data you want to read
 *
 *	\retval 		length of reading bytes
 */
int m_sio_read(int port, char *buf, int len)
{
	pMoxaUartStruct info;
	int i, j;

	if ( buf == NULL || len <= 0 ){
#ifdef MOXAUART_DEBUG        
		Printf("len = %d\r\n",len);
#endif    	
		return 0;
	}		
	
    	info = &moxauart_info[port];
	
#ifdef MOXAUART_DEBUG
    	Printf("\nMoxauart(%d): rxwptr=%d, rxrptr=%d", port, info->rxwptr, info->rxrptr);
#endif

	if ( info->rxrptr == info->rxwptr ){	// no data in Buffer
		return 0;
	}
	
    	j = (info->rxwptr - info->rxrptr) & MOXAUART_BUFFER_MASK;	// get the length of total bytes in buffer
		
#ifdef MOXAUART_DEBUG
    	Printf("\nMoxauart(%d): j=%d, len=%d", port, j, len);
#endif

	if ( j <= len ){	// there are only j bytes in buffer
		len = j;
	}
	
    	for ( i=0, j=info->rxrptr; i<len; j&=MOXAUART_BUFFER_MASK, i++ ){
       	*buf++ = info->rxbuf[j++];
    	}
	info->rxrptr = j;
	return len;
}





/**	\brief
 *
 *	1. It will check the remaining bytes of TX Buffer first.
 *	2. Write to TX Buffer from specific location of SDRAM.
 *
 *	\param[in]	port : UART port
 *	\param[in]	*buf : A specific location of SDRAM to get data, and write to TX Buffer.
 *	\param[in]	len : the length of data you want to write
 *
 *	\retval 		error code
 */
int m_sio_write(int port, char *buf, int len)
{
	pMoxaUartStruct info;
    	int	i, j;


	if ( buf == NULL || len <= 0 ){
#ifdef MOXAUART_DEBUG        
		Printf("len = %d\r\n",len);
#endif    	
		return 0;
	}
    
    	info = &moxauart_info[port];

	info->IER &= ~UART_IER_ETBEI;		/* disable IER for exclusive access of r/w ptr */
	Outw(((info->base)+MX_UART_IER[info->bus_type]), info->IER);
    
	j = (info->txrptr - info->txwptr - 1) & MOXAUART_BUFFER_MASK;		// get the length of TX Buffer
	if ( j <= len ){
		len = j;
	}


	/* i is for count , j is for buffer index */	
	for ( i=0, j=info->txwptr ; i < len ; j &= MOXAUART_BUFFER_MASK , i++ ){
		info->txbuf[j++] = *buf++;
	}
	info->txwptr = j;
	
	// If THRI Bit was disable , we enable it.  So that UART could wait for next interrupt of THRE.
	if ( !(info->IER & UART_IER_ETBEI) ) {
#ifdef MOXAUART_DEBUG
		Printf("\nMoxauart(%d): re-enable UART Tx empty interrupt.", port);
#endif
		info->IER |= UART_IER_ETBEI;
		Outw((info->base + MX_UART_IER[info->bus_type]),info->IER);
	}
	
	return len;
}



/**	\brief
 *
 *	Sets the value into specific register.
 *
 *	\param[in]	port : UART port
 *	\param[in]	reg : offset of specfic register
 *	\param[in]	value : the value you want to set into register
 *
 *	\retval 		error code
 */
int m_sio_write_register(int port, int reg, UINT8 value)
{

	BasicCheck1(port);

	Outw(((moxauart_info[port].base)+reg),value);
	return MOXAUART_OK;
}



/**	\brief
 *
 *	Get the value from specific register.
 *
 *	\param[in]	port : UART port
 *	\param[in]	reg : offset of specfic register
 *	\param[in]	*value : the value you want to get from register
 *
 *	\retval 		error code
 */
int m_sio_read_register(int port, int reg, UINT8 *value)
{

    BasicCheck1(port);

    *value = Inw((moxauart_info[port].base)+reg);
	return MOXAUART_OK;
}



/**	\brief
 *
 *	Turns MOXA UART into internal loopback mode
 *	Control the MCR bit4
 *
 *	\param[in]	port : UART port
 *	\param[in]	internal_loopback_option : INTERNAL_LOOPBACK_ON or not
 *
 *	\retval 		error code
 */
int m_sio_internal_loopback(int port, int internal_loopback_option)
{
	pMoxaUartStruct info;

	BasicCheck1(port);
	if ( (internal_loopback_option != INTERNAL_LOOPBACK_ON) && (internal_loopback_option != INTERNAL_LOOPBACK_OFF) ){
		return MOXAUART_ERROR_PARAMETER;
	}
	info = &moxauart_info[port];
	if ( internal_loopback_option == INTERNAL_LOOPBACK_ON ){
		info->MCR |= UART_MCR_LOOP;		// ON
	}else{
		info->MCR &= ~UART_MCR_LOOP;	// OFF
	}
	Outw((info->base+MX_UART_MCR[info->bus_type]),info->MCR);
	return MOXAUART_OK;
}



/**	\brief
 *
 *	Flush RX / TX Buffer
 *
 *	\param[in]	port : UART port
 *	\param[in]	flush_in_out : FLUSH_INPUT / FLUSH_OUTPUT / FLUSH_INOUT
 *
 *	\retval 		error code
 */
int m_sio_flush(int port, int flush_option)
{
	pMoxaUartStruct info;

	BasicCheck2(port);
	if ( !(flush_option & FLUSH_INOUT) ){
		return MOXAUART_OK;
	}
		info = &moxauart_info[port];
	if ( flush_option & FLUSH_INPUT ){
		info->rxrptr = info->rxwptr;
	}
	if ( flush_option & FLUSH_OUTPUT ){
		info->txwptr = info->txrptr;
	}
	return MOXAUART_OK;
}



/**	\brief
 *
 *	Modem Control (MCR)
 *	Control the DTR and RTS .
 *
 *	\param[in]	port : UART port
 *	\param[in]	line_option : DTR_ON / RTS_ON / DTR_OFF / RTS_OFF
 *
 *	\retval 		error code
 */
int m_sio_lctrl(int port, int line_option)
{
	pMoxaUartStruct info;

	BasicCheck1(port);
	info = &moxauart_info[port];
	if ( line_option & DTR_ON ){
		info->MCR |= DTR_ON;
	}else if(line_option & DTR_OFF){
		info->MCR &= ~DTR_ON;
	}
	
	if ( line_option & RTS_ON ){
		info->MCR |= RTS_ON;
	}else if(line_option & RTS_OFF){
		info->MCR &= ~RTS_ON;
	}
	
	Outw((info->base+MX_UART_MCR[info->bus_type]),info->MCR);
	return MOXAUART_OK;
}




/**	\brief
 *
 *	Get the value of Modem Status Register 
 *
 *	\param[in]	port : UART port
 *	\param[in]	line_option : DTR_ON / RTS_ON / DTR_OFF / RTS_OFF
 *
 *	\retval 		MSR
 */
int m_sio_lstatus(int port)
{
    	pMoxaUartStruct info;
	char msr;

	BasicCheck1(port);
	info = &moxauart_info[port];
	msr = Inw(info->base + MX_UART_MSR[info->bus_type]);
	
	return  msr;
}





/**	\brief
 *
 *	Set the interface of MOXA UART (RS232 / RS422 / RS485)
 *
 *	\param[in]	port : UART port
 *	\param[in]	interface_mode : RS232 / RS422 / RS485
 *
 *	\retval 		error code
 */
int m_sio_setopmode(int port, UINT8 interface_mode)
{
	pMoxaUartStruct info;
	UINT32	mode_reg;
	UINT16 	mode16;    
	UINT8 	mode8;    
	UINT8	n;
	int		i;    
    
	mode_reg = mode16 = mode8 = 0;
//    BasicCheck1(port);
   	info = &moxauart_info[port];
	if((info->bus_type) == MXSER_BUS_APB){
		mode_reg = info->mode_reg0;
		mode16 = Inw(mode_reg) ;		
		n =	info->vector_mask;
		for(i=0;;){
			n = n/2;
			if(n == 0){
				break;
			}else{
				i++;
			}
		}				
		mode16 &= ~(0x03 << (i*2));
		mode16 |= interface_mode << (i*2);
		Outw(mode_reg,mode16);		
#ifdef MOXAUART_DEBUG	
		Printf("Port%d mode = %x mode = %x \r\n",port+1,mode16,interface_mode);
#endif		

	}else if((info->bus_type) == MXSER_BUS_PCI){
		if(info->vector_mask & 0x0F){	//MU860 port 0 ~ 3)
			mode_reg = info->mode_reg0;
			mode8 = Inw(mode_reg);		
			n =	info->vector_mask;
			for(i=0;;){
				n = n/2;
				if(n == 0){
					break;
				}else{
					i++;
				}
			}		
			mode8 &= ~(0x03 << (i*2));
			mode8 |= interface_mode << (i*2);
		}else if(info->vector_mask & 0xF0){							//MU860 port 4 ~ 7
			mode_reg = info->mode_reg1;
			mode8 = Inw(mode_reg);		
			n =	(info->vector_mask >> 4);
			for(i=0;;){
				n = n/2;
				if(n == 0){
					break;
				}else{
					i++;
				}
			}		
			mode8 &= ~(0x03 << (i*2));
			mode8 |= interface_mode << (i*2);		
		}	
		Outw(mode_reg,mode8);		
#ifdef MOXAUART_DEBUG		
		Printf("Port%d mode = %x\r\n",port+1,mode8);		
#endif
	} 
	return MOXAUART_OK;
}	




 /**	\brief
 *
 *	Get a character from UART
 *
 *	\param[in]	port : UART port
 *
 *	\retval 		a character from RX Buffer
 */
int	m_sio_getch(int port)
{
	int ret;
	unsigned char	ch;

    	for(;;){	
    		if (m_sio_iqueue(port) > 0){
	        	break;		// wait here until RX Buffer comes of character.
    		}
	}    
	m_sio_read(port,&ch,1);
	ret = ch & 0xff;
	return(ret);
}




 /**	\brief
 *
 *	Put a character to TX Buffer
 *
 *	\param[in]	port : UART port
 *	\param[in]	ch : a character to put to TX Buffer
 *
 */
void	m_sio_putch(int port,unsigned char ch)
{
	m_sio_write(port,&ch,1);
}




/**	\brief
 *
 *	Show the information of UART
 *
 */
void	m_sio_get_uart_usage(void)
{
	int i;
	pMoxaUartStruct info;
	
	for(i=0;i<SlotNum;i++){
		Printf("SlotNum%d Ubase = 0x%08x Ibase = 0x%08x M0base = 0x%08x M1base = 0x%08x\r\n",
			i, UartBase[i], InterruptBase[i], ModeReg0[i], ModeReg1[i]);
	}
	for(i=0;i<MoxauartFoundPortNo;i++){
		info = &moxauart_info[i];
		Printf("uart%d base = 0x%08x mask = 0x%08x\r\n", 
			i,info->base,info->vector_mask);
		Printf("tptr = 0x%08x rptr = 0x%08x\r\n",(int)info->txbuf,(int)info->rxbuf);
		Printf("Btype = 0x%08x mode0 = 0x%08x mode1 = 0x%08x\r\n\r\n",
			  info->bus_type,info->mode_reg0,info->mode_reg1);
	}
}




/**	\brief
 *
 *	Get the number of UARTs in this system.
 *
 *	\retval 		number of UARTs in this system.
 */
int GetMoxaUartCnt(void)
{
	return MoxauartFoundPortNo;
}









///////////////////////////////////////////////////////////////////////
// tony start
// functions to replace macros
//////////////////////////////////////////////////////////////////////


/**	\brief
 *
 *	Enable Enhanced Mode in MOXA UART
 *	Control EFR register BIT4
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_ENCHANCE_MODE(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr |= MOXA_MUST_EFR_EFRB_ENABLE;		// FCR Bit4 set to 1 to enable Enhanced Mode
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}




/**	\brief
 *
 *	Disable Enhanced Mode in MOXA UART
 *	Control EFR register BIT4
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_ENCHANCE_MODE(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_EFRB_ENABLE;
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to XON1 register with Enhanced mode.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_XON1_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	// get into Enhanced mode Register setting area first
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	// Choose the bank of Enhanced Mode 
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR 
	__efr |= MOXA_MUST_EFR_BANK0;			// BANK0 is Software Flow Cntrol
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	// Setting X characters
	Outw(base_address+MOXA_MUST_XON1_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to XON2 register with Enhanced mode.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_XON2_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK0;			// BANK0 is Software Flow Cntrol
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_XON2_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to XOFF1 register with Enhanced mode.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_XOFF1_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK0;			// BANK0 is Software Flow Cntrol
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_XOFF1_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to XOFF2 register with Enhanced mode.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_XOFF2_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK0;			// BANK0 is Software Flow Cntrol
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_XOFF2_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to RBRTL register with Enhanced mode. (Low Water)
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_RBRTL_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	/* Swap EFR bank */
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// ERS0..1 = 00	;	clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK1;			// ERS0..1 = 01	;	BANK1 is Hardware Flow Control
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	/* Set RBRTL value */
	Outw(base_address+MOXA_MUST_RBRTL_REGISTER[bus_type],(UCHAR)(Value));
	/* Swap back to LCR */
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to RBRTH register with Enhanced mode. (High Water)
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_RBRTH_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK1;			// BANK1 is Hardware Flow Control
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_RBRTH_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to RBRTI register with Enhanced mode. (Immediate Water)
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_RBRTI_VALUE(unsigned int base_address, int bus_type, int Value)
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK1;			// BANK1 is Hardware Flow Control
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_RBRTI_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Put a character to THRTL register with Enhanced mode. (Low Water)
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a character to put into register
 *
 */
void SET_MOXA_MUST_THRTL_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;		// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK1;			// BANK1 is Hardware Flow Control
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_THRTL_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}






/**	\brief
 *
 *	Set a deafualt value of the configuration of Hardware Flow Control.
 *
 *	\param[in]	info : The software data structure of UART.
 *
 */
void SET_MOXA_MUST_DEFAULT_FIFO_VALUE(pMoxaUartStruct info)
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(info->base+MX_UART_LCR[info->bus_type]);
	Outw(info->base+MX_UART_LCR[info->bus_type],MOXA_MUST_ENTER_ENCHANCE);  
	__efr = Inw(info->base+MOXA_MUST_EFR_REGISTER[info->bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;			// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK1;				// BANK1 is Hardware Flow Control
	Outw(info->base+MOXA_MUST_EFR_REGISTER[info->bus_type],__efr);
	Outw(info->base+MOXA_MUST_RBRTH_REGISTER[info->bus_type],MOXA_MUST_DEFAULT_RBRH_VALUE);
	Outw(info->base+MOXA_MUST_RBRTI_REGISTER[info->bus_type],(UCHAR)(MOXA_MUST_DEFAULT_RBRH_VALUE-MOXA_MUST_DEFAULT_RBRL_VALUE));
	Outw(info->base+MOXA_MUST_RBRTL_REGISTER[info->bus_type],(UCHAR)(MOXA_MUST_DEFAULT_RBRH_VALUE-(MOXA_MUST_DEFAULT_RBRL_VALUE *2)));
	Outw(info->base+MX_UART_LCR[info->bus_type],__oldlcr);
}




/**	\brief
 *
 *	Set a value to ENUM register. This register control the Any baudrate.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	Value : a value to put into register
 *
 */
void SET_MOXA_MUST_ENUM_VALUE(unsigned int base_address, int bus_type, int Value) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;			// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK2;				// BANK2 
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MOXA_MUST_ENUM_REGISTER[bus_type],(UCHAR)(Value));
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}




/**	\brief
 *
 *	Get a value from HW ID register.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	*pId : a memory to store HW ID
 *
 */
void GET_MOXA_MUST_HARDWARE_ID(unsigned int base_address, int bus_type, UCHAR *pId) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_BANK_MASK;			// clear BIT6 , BIT7 of EFR
	__efr |= MOXA_MUST_EFR_BANK2;				// BANK2 
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	*pId = Inw(base_address+MOXA_MUST_HWID_REGISTER[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Configure the Software Flow Control parameters. It will set value to BIT0..3 of EFR.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	configuration : parameter
 *
 */
void SET_MOXA_MUST_SOFTWARE_FLOW_CONTROL_CONFIGURATION(unsigned int base_address, int bus_type, unsigned char configuration) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);	// LCR = 0xBF
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_MASK;					// clear least 4 bits of EFR
	__efr |= configuration;								// set the parameter to EFR
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);				
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);			// Restore LCR
}



/**	\brief
 *
 *	Enable transmit XON1/XOFF1.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_TX1_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_TX_MASK;			// clear least 4 bits of EFR
	__efr |= MOXA_MUST_EFR_SF_TX1;					
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Enable transmit XON2/XOFF2.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_TX2_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_TX_MASK;			// clear least 4 bits of EFR
	__efr |= MOXA_MUST_EFR_SF_TX2;
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}




/**	\brief
 *
 *	Disable transmit XON1/XOFF1.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_TX1_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_TX_MASK;			// clear BIT2 , BIT3 of EFR
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Disable transmit XON2/XOFF2.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_TX2_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_TX_MASK;			// clear BIT2 , BIT3 of EFR
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Enable receive XON1/XOFF1.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_RX1_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_RX_MASK;		// clear BIT0 , BIT1 of EFR
	__efr |= MOXA_MUST_EFR_SF_RX1;
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Disable receive XON1/XOFF1.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_RX1_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);		// LCR = 0xBF
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);					
	__efr &= ~MOXA_MUST_EFR_SF_RX_MASK;		// clear BIT0 , BIT1 of EFR
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}




/**	\brief
 *
 *	Enable receive XON2/XOFF2.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_RX2_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_RX_MASK;		// clear BIT0 , BIT1 of EFR
	__efr |= MOXA_MUST_EFR_SF_RX2;
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Disable receive XON2/XOFF2.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_RX2_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_RX_MASK;		// clear BIT0 , BIT1 of EFR
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}




/**	\brief
 *
 *	Enable TX/RX  XON1/XOFF1.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_TX1_RX1_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_MASK;		// clear least 4 bits of EFR
	__efr |= (MOXA_MUST_EFR_SF_RX1|MOXA_MUST_EFR_SF_TX1);
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Enable TX/RX  XON2/XOFF2.
 *	Disable Special Character
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_TX2_RX2_SOFTWARE_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldlcr, __efr;
	__oldlcr = Inw(base_address+MX_UART_LCR[bus_type]);
	Outw(base_address+MX_UART_LCR[bus_type],MOXA_MUST_ENTER_ENCHANCE);
	__efr = Inw(base_address+MOXA_MUST_EFR_REGISTER[bus_type]);
	__efr &= ~MOXA_MUST_EFR_SF_MASK;		// clear least 4 bits of EFR
	__efr |= (MOXA_MUST_EFR_SF_RX2|MOXA_MUST_EFR_SF_TX2);
	Outw(base_address+MOXA_MUST_EFR_REGISTER[bus_type],__efr);
	Outw(base_address+MX_UART_LCR[bus_type],__oldlcr);
}



/**	\brief
 *
 *	Enable "XON Any" FlowControl. 
 *	Disable Special Character
 *	If you enable XON Any flow control , 
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void ENABLE_MOXA_MUST_XON_ANY_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldmcr;
	__oldmcr = Inw(base_address+MX_UART_MCR[bus_type]);
	__oldmcr |= MOXA_MUST_MCR_XON_ANY;
	Outw(base_address+MX_UART_MCR[bus_type],__oldmcr);
}



/**	\brief
 *
 *	Enable "XON Any" FlowControl. 
 *	Disable Special Character.
 *	If you enable XON Any flow control, any character will replace the XON character and put in FIFO.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *
 */
void DISABLE_MOXA_MUST_XON_ANY_FLOW_CONTROL(unsigned int base_address, int bus_type) 
{
	UCHAR   __oldmcr;
	__oldmcr = Inw(base_address+MX_UART_MCR[bus_type]);
	__oldmcr &= ~MOXA_MUST_MCR_XON_ANY;
	Outw(base_address+MX_UART_MCR[bus_type],__oldmcr);
}


/**	\brief
 *
 *	You can get the good data length of MOXA UART when any error occur.
 *
 *	\param[in]	base_address : UART port
 *	\param[in]	bus_type : APB / ISA / PCI
 *	\param[in]	*reg_gdl : to store the value of GDL register.
 *
 */
void GET_MOXA_MUST_GDL(unsigned int base_address, int bus_type , unsigned char *reg_gdl)      
{
	*reg_gdl = Inw(base_address+MOXA_MUST_GDL_REGISTER[bus_type]);
}

///////////////////////////////////////////////////////////////////////
// tony end
///////////////////////////////////////////////////////////////////////
