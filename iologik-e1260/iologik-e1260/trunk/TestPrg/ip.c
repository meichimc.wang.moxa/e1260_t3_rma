/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	ip.c

	Routines for implementing IP/ICMP/UDP protocol

	2008-06-10	Albert Yu
		new release
	2008-10-15	Chin-Fu Yang		
		modified
*/


#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "timer.h"
#include "moxauart.h"
#include "memory.h"
#include "lib.h"
#include "mac.h"
#include "global.h"
#include "udpburn.h"
#include "ip.h"
#include "arp.h"
#include "tftp.h"
#include "proc232.h"


extern void Printf(const char *fmt, ...);


static int		IcmpStart = 0;
static ulong	IcmpId = 0;
static ulong	IcmpDestIp;




 /**	\brief
 *
 *	Calculate check sum of IP header
 *
 *	\param[in]	*ptr : IP header pointer
 *	\param[in]	len : length of IP header
 *
 *	\retval 		checksum
 */
static ushort IpCheckSum(ushort *ptr,int len)
{
	static ulong	sum,sum1;
	int		i;

	sum = 0;
	for (i=0;i < (len >> 1);i++){
	    sum += *ptr++;
	}
	sum1 = sum >> 16;
	sum &= 0xffff;
	sum += sum1;
	sum1 = sum >> 16;
	sum += sum1;
	sum = ~(sum);
	return((ushort)sum);
}



 /**	\brief
 *
 *	Process IP packet
 *
 *	\param[in]	msg : MAC port
 *
 */
void	IpReceive(msgstr_t msg)
{
	ipinfo_t    ip;
	ushort	    sum,sum1;
	int	    hlen,dlen,iplen;//,tcphlen,tcpdlen;

	ip = (ipinfo_t)&msg->base[14];
	if ((ip->ver & 0xf0) != 0x40) { /* not version 4 */
//		Printf("Not Version4\r\n");
	    MemFree(msg);
	    return;
	}

	hlen = (ip->ver & 0x0f) * 4;
	dlen = msg->wndx - 14;		/* ip header + data length  */
	if (dlen < hlen) {		/* invalid ip packet */
//		Printf("Dlen Error < hlen\r\n");
	    MemFree(msg);
	    return;
	}

	iplen = htons(ip->len);
	if (dlen < iplen) {		/* invalid ip packet */
//		Printf("Dlen Error < iplen\r\n");
	    MemFree(msg);
	    return;
	}

	msg->wndx = (dlen + 14) & 0xffff;
	sum = ip->sum;
	ip->sum = 0;
	sum1 = IpCheckSum((ushort *)ip,hlen);
	if (sum != sum1) {  /* ip header checksum error */
//Printf("Sum Error\r\n");
	    MemFree(msg);
	    return;
	}

	if ((ip->d_ip != NicIp[G_LanPort]) && (ip->d_ip != 0xffffffff) && ip->d_ip != MAC_GEN_IP) {
//Printf("IP Add Error\r\n");
	    MemFree(msg);
	    return;
	}

	if (ip->d_ip == NicIp[G_LanPort])	/* add to routing table */
	    ArpAdd(ip->s_ip,&msg->base[6]);
	else if(ip->d_ip == MAC_GEN_IP)
	    ArpAdd(ip->s_ip,&msg->base[6]);

	msg->rndx = (hlen & 0xffff) + 14;

	switch (ip->protocol) {
	    case DIP_ICMP :
//			Printf("DIP_ICMP...");
		if (ip->d_ip == NicIp[G_LanPort]) {
//			Printf("IcmpReceive\r\n");
		    IcmpReceive(msg);
		}else if(ip->d_ip == MAC_GEN_IP){
			IcmpReceive(msg);
		} else{
		    MemFree(msg);
		}
		break;
	    case DIP_TCP :
//			Printf("DIP_TCP\r\n");
		MemFree(msg);
		break;
	    case DIP_UDP :
		UdpReceive(msg);
		break;
	    default :
		MemFree(msg);
		break;		
	}
}


 

 /**	\brief
 *
 *	Calculate check sum of ICMP packet
 *
 *	\param[in]	*ptr : ICMP packet pointer
 *	\param[in]	nbytes : length of ICMP
 *
 *	\retval 		checksum
 */
static ushort IcmpCheckSum(ushort *ptr,int nbytes)
{
	long	sum;
	short	oddbyte;
	ushort	answer;

	/*
	 * Our algoritm is simple, using a 32-bit accumulator (sum),
	 * we add sequential 16-bit words to it,and at the end,fold back
	 * all the carry bits from the top 16 bits into the lower 16 bits.
	 */
	sum = 0;
	while (nbytes >1) {
		sum += *ptr++;
		nbytes -= 2;
	}
	/* mop up an odd byte, if necessary */
	if (nbytes == 1) {
		oddbyte = 0;  /* make usre top half is zero */
		*((uchar *)&oddbyte) = *(unsigned char *)ptr;/* one byte								       only */
		sum += oddbyte;
	}
	/*
	 * Add back carry outs from top 16 bits to low 16 bits.
	 */
	sum = (sum >> 16)+ (sum & 0xffff); /* add high-16 to low-16 */
	sum += (sum >> 16);	/* add carry */
	answer = ~(sum); /* ones-complement,then truncate to 16 bits */
	return(answer);
}



 

  /**	\brief
 *
 *	response for ICMP echo packet
 *
 *	\param[in]	msg : memory block pointer of ICMP echo packet
 *	
 */
extern char	MAC_GEN_Mac[6];
static void IcmpReply(msgstr_t msg)
{
	ipinfo_t    ip;
	icmpinfo_t  icmp;
	int	    i,len;
//	ushort	    sum;

	ip = (ipinfo_t)&msg->base[14];
	icmp = (icmpinfo_t)&msg->base[msg->rndx];
	for (i=0;i<6;i++) {
	    msg->base[i] = msg->base[i+6];
		if(ip->d_ip == NicIp[G_LanPort])
			msg->base[i+6] = MacSrc[G_LanPort][i];
		else
			msg->base[i+6] = MAC_GEN_Mac[i];
	}
	icmp->type = ICMP_ECHOREPLY;
	if(ip->d_ip == NicIp[G_LanPort]){
		ip->d_ip = ip->s_ip;
		ip->s_ip = NicIp[G_LanPort];
	}else{
		ip->d_ip = ip->s_ip;
		ip->s_ip = MAC_GEN_IP;
	}
	len = (ip->ver & 0x0f) * 4;
	ip->sum = 0;
	ip->sum = IpCheckSum((ushort *)ip,len);
	len = msg->wndx - msg->rndx;		/* icmp header + data length */
	icmp->sum = 0;
	icmp->sum = IcmpCheckSum((ushort *)icmp,len);
	msg->rndx = 0;
	mac_send(G_LanPort, msg);
}




 /**	\brief
 *
 *	Process ICMP packet
 *
 *	\param[in]	msg : MAC port
 *
 *	\retval 		error code
 */
void	IcmpReceive(msgstr_t msg)
{
	icmpinfo_t  icmp;
//	ushort	    sum;
	int	    len;
	ulong   *id, *time;
//	char	    buf[80];

	len = msg->wndx - msg->rndx;
	len -= sizeof(struct icmpinfo);
	if (len < 0) { /* invalid icmppacket */
	    MemFree(msg);
	    return;
	}
	icmp = (icmpinfo_t)&msg->base[msg->rndx];
	switch (icmp->type) {
	    case ICMP_ECHOREPLY :
		if (len < 4) {	/* Invalid ICMP ECHOREPLY   */
		    break;
		}
		id = (ulong *)&msg->base[msg->rndx + sizeof(struct icmpinfo)];
		time = (ulong *)&msg->base[msg->rndx + sizeof(struct icmpinfo) + 4];
		break;
	    case ICMP_DEST_UNREACH :
		break;
	    case ICMP_SOURCE_QUENCH :
		break;
	    case ICMP_REDIRECT :
		break;
	    case ICMP_ECHO :
//Printf("IcmpReplay\r\n");
		IcmpReply(msg);
		return;
	    case ICMP_TIME_EXCEEDED :
		break;
	    case ICMP_PARAMETERPROB :
		break;
	    case ICMP_TIMESTAMP :
		break;
	    case ICMP_TIMESTAMPREPLY :
		break;
	    case ICMP_INFO_REQUEST :
		break;
	    case ICMP_INFO_REPLY :
		break;
	    case ICMP_ADDRESS :
		break;
	    case ICMP_ADDRESSREPLY :
		break;
	    default : /* unknown icmp type */
		break;
	}
	MemFree(msg);
}


 


 /**	\brief
 *
 *	Send an ICMP echo packet to remote
 *
 *
 *	\retval 		error code
 */
int IcmpPing(void)
{
	int	    i,j;
	msgstr_t    msg;
	ipinfo_t    ip;
	icmpinfo_t  icmp;
	ulong *id,*time;

	i = ArpSearch(IcmpDestIp);
	if (i >= 0) {
	    msg = MemAlloc(74);
	    if (msg != NULL) {
			for (j=0;j<6;j++) {
			    msg->base[j] = ArpTable[i].netaddr[j];
			    msg->base[j+6] = MacSrc[G_LanPort][j];
			}
			*((ushort *)&msg->base[12]) = NIC_IP;
			ip = (ipinfo_t)&msg->base[14];
			ip->ver = 0x45;
			ip->service = 0;
			ip->len = htons(60);
			ip->id = 0;
			ip->frag = 0;
			ip->ttl = 32;
			ip->protocol = DIP_ICMP;
			ip->d_ip = IcmpDestIp;
			ip->s_ip = NicIp[G_LanPort];
			ip->sum = 0;
			ip->sum = IpCheckSum((ushort *)ip,20);
			icmp = (icmpinfo_t)&msg->base[14 + 20];
			icmp->type = ICMP_ECHO;
			icmp->code = 0;
			icmp->sum = 0;
			id = (ulong *)&msg->base[14 + 20 + sizeof(struct icmpinfo)];
			*id = IcmpId++;
			time = (ulong *)&msg->base[14 + 20 + sizeof(struct icmpinfo) + 4];
			*time = Gsys_msec;
			icmp->sum = IcmpCheckSum((ushort *)icmp,40);
			msg->wndx = 74;
			mac_send(G_LanPort, msg);	
		}
	}
	if (IcmpStart)
	    return(TimerOut(IcmpPing,1000));	/* 1 second */
	else
		return 0;
}



 

 /**	\brief
 *
 *	Start to ping remote
 *
 *	\param[in]	ip : target ip address to ping
 *
 */
void	IcmpStartPing(ulong ip)
{
	IcmpDestIp = ip;
	IcmpStart = 1;
	IcmpId = 0;
	TimerOut(IcmpPing,0);
}





 /**	\brief
 *
 *	Stop ping remote
 *
 */
void	IcmpStopPing(void)
{
	IcmpStart = 0;
}




 /**	\brief
 *
 *	UDP receive process routine
 *
 *	\param[in]	msg : memory block pointer with UDP packet
 *
 */
extern char *smptr;
extern int smlen;
void	UdpReceive(msgstr_t msg)
{
	udpinfo_t   udp;
	ipinfo_t    ip;
	ulong	    len,data_len;
	uchar   *ptr;

	len = msg->wndx - msg->rndx;
	if (len < (int)UDPHLEN) {	/* bad udp packet   */
	    MemFree(msg);
	    return;
	}
	ptr = &msg->base[14 + 20];
	udp = (udpinfo_t)ptr;
	len -= UDPHLEN;
	data_len = htons(udp->len) - UDPHLEN;
	if (data_len > len) {	    /* bad udp packet	*/
	    MemFree(msg);
	    return;
	}
	if ((htons(udp->s_port) == AP_UDP) &&
	    (htons(udp->d_port) == CONSOLE_UDP)) {
	    ip = (ipinfo_t)&msg->base[14];
	    ArpAdd(ip->s_ip,&msg->base[6]);
	    ptr = &msg->base[14 + 20 + UDPHLEN];
	    ConUdpReceive(ip->s_ip,ip->d_ip,ptr,data_len);
	}
	else if((htons(udp->d_port) == 0x1E14))
	{
		ip = (ipinfo_t)&msg->base[14];
	  ArpAdd(ip->s_ip,&msg->base[6]);
		ptr = &msg->base[14 + 20 + UDPHLEN];
		tftp_stream_read(ip->s_ip,htons(udp->s_port),ptr,data_len);
	}

	else if((htons(udp->d_port) == MAC_GEN_UDP)) // for series & mac port 4001
	{
		ip = (ipinfo_t)&msg->base[14];
	  ArpAdd(ip->s_ip,&msg->base[6]);
		ptr = &msg->base[14 + 20 + UDPHLEN];
		smptr = ptr;
		smlen = data_len;
		//ConUdpReceive(ip->s_ip,ip->d_ip,ptr,data_len);
	}
/*	else if((htons(udp->s_port) == DDHCP_SERVER_UDP_PORT) &&
	    (htons(udp->d_port) == DDHCP_CLIENT_UDP_PORT)) {
	    ip = (ipinfo_t)&msg->base[14];
	    ArpAdd(ip->s_ip,&msg->base[6]);
	    ptr = &msg->base[14 + 20 + UDPHLEN];
//	    dhcp_receive(ptr,data_len);		
	}
*/
	MemFree(msg);
}

 /**	\brief
 *
 *	UDP send routine
 *
 *	\param[in]	d_ip : dest IP address
 *	\param[in]	s_port : source udp port
 *	\param[in]	d_port : dest udp port
 *	\param[in]	*buf : data pointer to be sent
 *	\param[in]	len : length of udp data to be sent
 *
 *	\retval 		error code
 */
int UdpSend(ulong d_ip,ushort s_port,ushort d_port,uchar *buf,int len)
{
	udpinfo_t   udp;
	ipinfo_t    ip;
	msgstr_t    msg;
	uchar   *ptr;
	int	    i=0,j,broadcast,size;

	if (d_ip == 0xffffffff)
	    broadcast = 1;
	else {
	    broadcast = 0;
	    i = ArpSearch(d_ip);
	    if (i < 0)
		return(-1);
	}
	size = 14 + 20 + UDPHLEN + len;
	
	/* Hank : if data length < 60, Host can't receive?? */	
	if (size<60)
	{
		size = 60;		
	}

	
	msg = MemAlloc(size);
	if (msg == NULL)
	    return(-2);
	for (j=0;j<6;j++) {
	    if (broadcast)
		msg->base[j] = 0xff;
	    else
		msg->base[j] = ArpTable[i].netaddr[j];
	    msg->base[j+6] = MacSrc[G_LanPort][j];
	}
	*((ushort *)&msg->base[12]) = NIC_IP;
	ip = (ipinfo_t)&msg->base[14];
	ip->ver = 0x45;
	ip->service = 0;
	ip->len = htons(len + 20 + UDPHLEN);
	ip->id = 0;
	ip->frag = 0;
	ip->ttl = 32;
	ip->protocol = DIP_UDP;
	ip->d_ip = d_ip;
	ip->s_ip = NicIp[G_LanPort];
	ip->sum = 0;
	ip->sum = IpCheckSum((ushort *)ip,20);
	udp = (udpinfo_t)&msg->base[14 + 20];
	udp->s_port = htons(s_port);
	udp->d_port = htons(d_port);
	udp->len = htons(len + UDPHLEN);
	udp->sum = 0;			/* UDP check sum is not used	*/
	ptr = &msg->base[14 + 20 + UDPHLEN];
	for (i=0;i<len;i++)
	    *ptr++ = *buf++;
	msg->wndx = size;
	if (mac_send(G_LanPort, msg)) {
		MemFree(msg);
		return(-2);
	}
	return(0);
}

int UdpSends(ulong d_ip,ushort s_port,ushort d_port,uchar *buf,int len)
{
	udpinfo_t   udp;
	ipinfo_t    ip;
	msgstr_t    msg;
	uchar   *ptr;
	int	    i=0,j,broadcast,size;

	if (d_ip == 0xffffffff)
	    broadcast = 1;
	else {
	    broadcast = 0;
	    i = ArpSearch(d_ip);
	    if (i < 0)
		return(-1);
	}
	size = 14 + 20 + UDPHLEN + len;
	
	/* Hank : if data length < 60, Host can't receive?? */	
	if (size<60)
	{
		size = 60;		
	}

	msg = MemAlloc(size);
	if (msg == NULL)
	    return(-2);
	for (j=0;j<6;j++) {
	    if (broadcast)
		msg->base[j] = 0xff;
	    else
		msg->base[j] = ArpTable[i].netaddr[j];
	    msg->base[j+6] = MAC_GEN_Mac[j];
	}
	*((ushort *)&msg->base[12]) = NIC_IP;
	ip = (ipinfo_t)&msg->base[14];
	ip->ver = 0x45;
	ip->service = 0;
	ip->len = htons(len + 20 + UDPHLEN);
	ip->id = 0;
	ip->frag = 0;
	ip->ttl = 32;
	ip->protocol = DIP_UDP;
	ip->d_ip = d_ip;
	ip->s_ip = MAC_GEN_IP;
	ip->sum = 0;
	ip->sum = IpCheckSum((ushort *)ip,20);
	udp = (udpinfo_t)&msg->base[14 + 20];
	udp->s_port = htons(s_port);
	udp->d_port = htons(d_port);
	udp->len = htons(len + UDPHLEN);
	udp->sum = 0;			/* UDP check sum is not used	*/
	ptr = &msg->base[14 + 20 + UDPHLEN];
	for (i=0;i<len;i++){
	    *ptr++ = *buf++;
	}
	msg->wndx = size;
	if (mac_send(1, msg)) {
		MemFree(msg);
		return(-2);
	}
	return(0);
}


/**	\brief
 *
 *	Converts a string containing an IPv4 dotted-decimal address into a unsigned long format
 *
 *	\param[in]	*ptr : a string contains IPv4 dotted-decimal address. 
 *
 *	\retval 		IP in a unsigned long format
 */
ulong  inet_addr(char *ptr)
{
	int			len,i;
	ushort	ch;
	ulong		val;
	char		buffer[10];

	for(i=0,len=0,val=0,ch=0; i<4; i++)
	{
		len = get_digit(ptr,&len);
		if((ptr[len] != '.') && (i < 3)) 
		{
			Printf("inet_addr() return none\r\n");
			return INADDR_NONE;
		}
		
		Memcpy(buffer, ptr, len+1);
		buffer[len] = 0;
		
		if((ch=(ushort)Atos(buffer)) >= 0xff)
		{
			Printf("inet_addr() return none\r\n");
			return INADDR_NONE;
		}
		val |= (uchar)ch << (i*8);
		ptr += (len+1);
	}
	return val;
}