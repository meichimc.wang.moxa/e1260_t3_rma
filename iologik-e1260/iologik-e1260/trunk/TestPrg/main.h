/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	main.h

	Data pattern of test functions.
	Function declarations.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-20	Chin-Fu Yang		
		modified		
    
*/

#ifndef _MAIN_H
#define _MAIN_H





/*------------------------------------------------------ Macro / Define -----------------------------*/
//#define BIOS_VER 0x01070000L
//#define BIOS_VER 0x01000000L


#define MM_STATUS  0
#define MP_STATUS  1
#define BR_STATUS  2
#define MAC_STATUS	3

#define DIAG_GET_VALUE_HEX_MODE		0
#define DIAG_GET_VALUE_DEC_MODE		1
#define DIAG_GET_VALUE_NO_ABORT		0x80000000

/* You must modify the Datapattern_vlaue , too */
#define DATA_PATTERN1	(0x5AA55AA5)
#define DATA_PATTERN2	(0xA55AA55A)
#define DATA_PATTERN3	(0x5A5A5A5A) 
#define DATA_PATTERN4	(0xA5A5A5A5) 
#define DATA_PATTERN5	(0x00000000)
#define DATA_PATTERN6	(0xFFFFFFFF)
#define DATA_PATTERN7	(0x41414141)

#define SDRAM_BANKSIZE_1MB   (0x00)
#define SDRAM_BANKSIZE_2MB   (0x01)
#define SDRAM_BANKSIZE_4MB   (0x02)
#define SDRAM_BANKSIZE_8MB   (0x03)
#define SDRAM_BANKSIZE_16MB  (0x04)
#define SDRAM_BANKSIZE_32MB  (0x05)
#define SDRAM_BANKSIZE_64MB  (0x06)
#define SDRAM_BANKSIZE_128MB (0x07)
#define SDRAM_BANKSIZE_256MB (0x08)

#define S2E_SDRAM_BANK_SIZE (4*1024*1024)		// 4MB

#define UART_PATTERN_SIZE	(sizeof(UARTPattern)/sizeof(UINT8))

#define I2C_WRITE_MODE_BYTE	0
#define I2C_WRITE_MODE_PAGE	1







/*------------------------------------------------------ Structure ----------------------------------*/
struct _Packet_Info {
	long   length;				
	uchar	wbuf[2048];
	uchar	rbuf[2048];
}__attribute__((packed));
typedef struct _Packet_Info Packet_Info;
typedef struct _Packet_Info *Packet_Info_t;


/* This is the command menu */
struct burnin_cmd_T{	
   	int index;						// menu index
	char	*string;						/* command name */	
	void (*burnin_routine)(void);		/* implementing routine */	
	char exist;         				 	/* IP exist */
};//__attribute__((packed)); 






/*------------------------------------------------------ Extern / Function Declaration -----------------*/

extern int  ConsolePort;

static int  ManualTesting(burnin_cmd *cmd_list);
static void PrintItemMsg(burnin_cmd *cmd_list);
int ParseCmd(char* Buffer);
UINT8 diag_check_press_ESC(void);
int parse_num(char *s, UINT32 *val, char **es, char *delim);
int diag_get_value(char *title, UINT32 *val, UINT32 min, UINT32 max, int mode);
static int PacketInit(int parameter);

int FlashromProgram(UINT32 dest, UINT32 src, long len);
void diag_do_BIOS_upgrade(void);
void Initboardinfo(int i);

void diag_do_DOWNLOAD_func(void);
void diag_do_JUMPTOGO_func(void);
void diag_do_PRODUCTINFO_func(void);
void diag_do_SERIAL_MAC_func(void);
void diag_do_HWEXID_func(void);
void diag_do_FLAG_func(void);
void diag_do_BURNIN_func(void);
void diag_do_DEBUG_func(void);
void diag_do_Change_module_name(void); // for easy change Standard Model or -T


//\\ 1. SRAM
void diag_do_SRAM_test_func(void);
//void diag_do_SRAM_byte(void);
//static int sram_byte_mptest(int mm, int pattern);
void diag_do_SRAM_short(void);
static int sram_short_mptest(int mm, int pattern);
void diag_do_SRAM_word(void);
static int sram_word_mptest(int mm, int pattern);


//\\ 2. SDRAM
void diag_do_SDRAM_func(void);
void diag_do_get_SDRAM_setting(void);
void diag_do_SDRAM_byte(void);
static int sdram_byte_mptest(int mm, int pattern);
void diag_do_SDRAM_short(void);
static int sdram_short_mptest(int mm, int pattern);
void diag_do_SDRAM_word(void);
static int sdram_word_mptest(int mm, int pattern);
void diag_do_get_SDRAM_usage(void);


//\\ 4. MOXA UART
void diag_do_MOXAUART_func(void);
int moxa_uart_test(UINT8 mode, int parameter);
void diag_do_MOXAUART_register_test(void);
void diag_do_MOXAUART_Internal_test(void);
void diag_do_MOXAUART_Terminal_test(void);
void diag_do_MOXAUART_Usage_test(void);

//\\ 8. WDT
void diag_do_WDT_func(void);
void diag_do_WDT_reboot_test(void);
void diag_do_SYSRESET_func(void);

// Jumper & Switch
void diag_do_JP_SW_func(void);

// LED
void diag_do_LED_func(void);

// MP
void diag_do_MPTEST_func(void);

//\\ 9. SPI to GPIO
void diag_do_IO_func(void);

//\\ 10. SPI Flash
void diag_do_SPI_Flash_func(void);
void diag_do_SPI_Flash_byte(void);
void diag_do_SPI_Flash_short(void);
void diag_do_SPI_Flash_long(void);
UINT8 diag_do_SPI_Flash_protect(void);
static int spi_byte_mptest(int mm , int pattern);
static int spi_short_mptest(int mm , int pattern);
static int spi_long_mptest(int mm , int pattern);


//\\ 11. I2C
void diag_do_EEPROM_test_func(void);
void diag_do_I2C_test_func(void);
void diag_do_I2C_with_EEPROM_test(void);
void diag_do_I2C_show_register(void);


//\\ 12. Ethernet
void diag_do_ETH_func(void);
int diag_do_ETH_test(int mm, int ethport, int mode);
void diag_do_ETH_internal_test(void);
void diag_do_ETH_external_10MF_test(void); 
void diag_do_ETH_external_100MF_test(void);
void diag_do_ETH_ping_test(void);
void diag_do_ETH_88E6060_write_Command_test(void);
void diag_do_ETH_88E6060_read_Command_test(void);


/*
//\\ 14. RTC
void diag_do_EXRTC_func(void);
void diag_do_EXRTC_SetTime(void);
void diag_do_EXRTC_ViewTime(void);
*/

/*
 * fFlag Setting Item
 */
void diag_do_MPFLAG_test(void);
void diag_do_SMFLAG_test(void);
void diag_do_EOTFLAG_test(void);
void diag_do_BRFLAG_test(void);
//void diag_do_HWEXIDFLAG_test(void);
void diag_do_WARMUPFLAG_test(void);
void diag_do_ALLFLAG_test(void);
void diag_do_VIEWFLAG_test(void);
void diag_do_view_now_flag(long parameter);
int	diag_do_set_clean_flag(int mode,long parameter);

// Set MAC address & Serial Number
void diag_do_SETSERIAL_test(void);
void diag_do_SETMAC_test(void);
UINT32 diag_get_data_pattern(int pattern_index);



//\\ 27. Clock Disable Test ( not complete )
void diag_do_clock_disable_test(void);
void diag_clock_disable_mac(void);
void diag_clock_disable_general_uart(void);
void diag_clock_disable_moxa_uart(void);
void diag_clock_disable_i2c(void);















#endif
