/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	spi.c

	Routines for accessing SPI flash

	2008-06-10	Albert Yu
		new release
	2008-10-17	Chin-Fu Yang		
		modified			
*/

#include "types.h"
#include "chipset.h"
#include "spi_reg.h"
#include "spi.h"
#include "isr.h"
#include "lib.h"
#include "global.h"
#include "console.h"

/* It has be defined in spi_asm.S */
extern UINT32 gSpiSdramDestAddr;
extern UINT32 gSpiSdramSrcAddr;
extern UINT32 gSpiSdramEndAddr;
//UINT32 gSpiSdramDestAddr;

int spi_accessing = 0;



/**	\brief
 *
 *	Wait the SPI TX FIFO Empty.
 *	The maximun waiting time is defined in SPI_WAIT_MAX_CNT.
 *
 *	\retval 		
 *				0	: empty
 *				-1	: not empty
 */
int spi_wait_TX_FIFO_empty(void)
{
	int i;
	int ret = (-1);		// not empty

	for (i = 0; i < SPI_WAIT_MAX_CNT; i++){
		if (SPI_STATUS() & SPI_STATUS_TX_FIFO_EMPTY){	 /* TX FIFO Empty */
			ret = 0;		// empty
			break;
		}
	}

	return ret;	
}





/**	\brief
 *
 *	Before you issue a new SPI command, you must wait until the previous command transfer is done,
 *	or the previous command transfer will be interrupted and not be executed.
 *
 *	\retval 		
 *				FALSE	: not busy
 *				TRUE	: busy
 */ 
int spi_wait_not_busy(void)
{
	int i;
	int is_busy = TRUE;		// default : busy

	for (i = 0; i < SPI_WAIT_MAX_CNT; i++){
		if (!SPI_IS_BUSY()){
			is_busy = FALSE;		// not busy
			break;
		}
	}

	return is_busy;
}





/**	\brief
 *
 *	Print out the RX overflow message.
 *	Called by spi_isr()
 *
 */ 
void spi_show_rx_overflow(void)	
{
	PrintStr("!!! SPI RX overflow !!!\r\n");
}






/**	\brief	SPI controller init
 *
 *  	Init SPI.
 *	In SPI chip , we enable SPI chip just when we use it.
 *	And we should disable SPI chip right away.
 *
 */
void spi_init(void)
{
	/* 1. disable SPI to set config registers */
	SPI_DISABLE();
	
	/* 2. set clock divider , APB / CLOCK_DIV */
	SPI_SET_CLK_DIV(CLOCK_DIV);
	
	/* 3. disable all interrupts first. We will enable interrupt as required for transmission */
	SPI_DISABLE_INT();
	
	/* 4. Set TX/RX FIFO threshold */
	VPlong(S2E_SPI_BASE + SPI_TX_FIFO_THRESHOLD) = TX_FIFO_THRESHOLD - 1;
	VPlong(S2E_SPI_BASE + SPI_RX_FIFO_THRESHOLD) = RX_FIFO_THRESHOLD - 1;    
	
	/* 5. set ISR */
	isr_set_isr(IRQ_SPI, spi_isr);
	
	/* 6. Enable SPI IRQ */
	ENABLE_IRQ(IRQ_SPI);	
}






#ifndef BOOT
/**	\brief	Get sector size of SPI flash
 *
 *  \param[in]	type    SPI flash type
 *  \param[in]  sector  Assign which sector to get
 *
 *	\return	sector size in bytes
 */
UINT32 GetSectorSize(int type, int sector)
{
	switch(type){
		case FLASH_TYPE_MX25L: 
			return MX_SECTOR_SIZE;
		default:
			return MX_SECTOR_SIZE;
	}	
}
#endif






/**	\brief	Read status register of MX SPI flash
 *
 *	Issue Read Status Register command to MX SPI flash and get result.
 *
 *	\return	value of status register , -1 is error.
 */
int mx_read_status_reg(void)
{
	int ret = (-1);			// error
	int i;

	SPI_DISABLE();		// SPI
	/* 	
		SPI_SET_MODE() :
		
			Data Frame Size = 8
			Frame Format = Motorola SPI
			Serial Clock toggle at start of first data bit
			Serial Clock Polarity = High
			Transfer Mode = EEPROM Read
			Slave txd is enabled (Slave Output Enable)
			Normal Shift Register Loop 
			Control Frame Size = 8-bit control word
		
	*/
	SPI_SET_MODE(0x73C7);			/* EEPROM Read mode */ // you should disable SPI chip before editing the control0 register.
	SPI_SET_EEPROM_READ_CNT(1);	/* read 1 byte */
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_RDSR);		/* issue Read Status Register command */
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission (Active-Low) */
	spi_wait_TX_FIFO_empty();		/* wait TX FIFO empty */
	
	/* get result */
	for (i = 0; i < SPI_WAIT_MAX_CNT; i++){
		if (SPI_RX_FIFO_ENTRIES() >= 1){
			ret = SPI_READ_DATA();
			break;
		}
	}

	SPI_DISABLE_FLASH();	// SPI Chip-select has two choices , Flash & Expander.
							// The Flash Line of chip-select is BIT0 of "Slave Enable Register (0x10)".
	SPI_DISABLE();

	return ret;
}






/**	\brief	Write status register of MX SPI flash
 *
 *	Issue Write Status Register command to MX SPI flash.
 *
 *	\param[in]	value   Assign the value to be wrote to status register.
 *
 *	\retval 0   OK
 *  	\retval -1  FAIL due to command timeout
 */
int mx_write_status_reg(int value)
{
	int ret;
	SPI_DISABLE();	
	SPI_SET_MODE(0x71C7);		// Transmit Only mode 
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_WRSR);		/* issue Write Status Register command */
	SPI_WRITE_DATA(value);			/* the value to be written to Status Register */
	SPI_ENABLE_FLASH();			/* CS0# low (Active-Low)  */
	ret = spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();				/* wait until not busy */
	SPI_DISABLE_FLASH();
	SPI_DISABLE();

	return ret;
}

/**	\brief
 *
 *	Issue a read command to SPI Flash.
 *	It tell that I want read data from SPI Flash.
 *
 *	\param[in]	port : MAC port
 *
 *	\retval 		error code
 */
void mx_issue_read_command(UINT32 flashSrcAddr)
{
	int i;

	SPI_ENABLE();
	SPI_WRITE_DATA(MX_FAST_READ);		/* issue Read Data command */
	for (i = 2; i >= 0; i--){					/* write address from MSB to LSB */
		SPI_WRITE_DATA((flashSrcAddr >> (i * 8)) & 0xFF);		// using 8 bits data width
	}
	SPI_WRITE_DATA(0xAA);				/* Dummy data to put the remaining address out */
	SPI_ENABLE_FLASH();				/* CS0# low to start transmiting */
}






/**	\brief	Read data from MX SPI flash
 *
 *	Read data from MX SPI flash and write them to SDRAM.
 *
 *	\param[in]	sdramDestAddr	The SDRAM destination address to which the data is write.
 *	\param[in]	flashSrcAddr	The SPI flash source address from which the data is read.
 *	\param[in]	len             The length of data to be read.
 *
 *	\return	length of data be read
 */
int mx_read_data(UINT32 sdramDestAddr, UINT32 flashSrcAddr, int len)
{
	int ret;
	int read_cnt;
	UINT32 sdramEndAddr;		// End address of reading data from SPI Flash

	spi_accessing = 1;

	ret = len;

	SPI_DISABLE();					// disable SPI before SPI_SET_MODE()
	SPI_SET_MODE(0x73C7);			/* EEPROM Read mode */

	if (len <= SPI_FIFO_SIZE){	
		/* get all data in one RX FIFO read */
		SPI_DISABLE_RX_INT();
		VPlong(S2E_SPI_BASE + SPI_CONTROL1) = len - 1;
		mx_issue_read_command(flashSrcAddr);
		while (len > 0){
			if (SPI_STATUS() & SPI_STATUS_RX_FIFO_NOT_EMPTY){
				VPchar(sdramDestAddr++) = SPI_READ_DATA();
				len --;
			}
		}
		SPI_DISABLE_FLASH();
		SPI_DISABLE();
		return ret;
	}

	/* mass read using ISR */
	SPI_ENABLE_RX_INT();
	gSpiSdramDestAddr = sdramDestAddr;	/* gSpiSdramDestAddr will be increased by ISR */
	while (len > 0){
		/* set read counter register, max 65536 for each read */
		read_cnt = len;
		if (read_cnt > SPI_BLOCK_SIZE){
			read_cnt = SPI_BLOCK_SIZE;
		}

		sdramEndAddr = gSpiSdramDestAddr + read_cnt;

//		Printf ("sdramEndAddr = %08x\r\n",sdramEndAddr);
//		Printf ("gSpiSdramDestAddr = %08x\r\n",gSpiSdramDestAddr);
		/* set EEPROM Read mode Rx counter */
		/* align to threshold boundary to avoid last interrupt being not triggered */
		VPlong(S2E_SPI_BASE + SPI_CONTROL1) = (read_cnt - 1) | (RX_FIFO_THRESHOLD - 1);

		mx_issue_read_command(flashSrcAddr);

		while (1){

			/* wait for completion of read */
		    	if (gSpiSdramDestAddr >= sdramEndAddr){
		       	break;
		    	}
		}
		SPI_DISABLE_FLASH();			// disable chip-select of SPI Flash
		SPI_DISABLE();					// disable SPI chip
		len -= SPI_BLOCK_SIZE;
		flashSrcAddr += SPI_BLOCK_SIZE;
	}

	SPI_DISABLE_RX_INT();

	spi_accessing = 0;
	
	return ret;	
}






/**	\brief
 *
 *	Setting Write Enable Latch bit for enable writing to SPI Flash.
 *
 */

void mx_write_enable(void)
{
	int status;
	
	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);			/* Transmit Only mode */
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_WREN);		/* issue Write Enable command to SPI Flash*/
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	spi_wait_TX_FIFO_empty();		/* wait TX FIFO empty */	
	spi_wait_not_busy();				/* wait until not busy */
	SPI_DISABLE_FLASH();			/* CS0# high to stop transmission */
	SPI_DISABLE();
	
	while (1)
	{
		status = mx_read_status_reg();
		if (status & MX_STATUS_BIT_WEL)
		{
			break;
		}
	}
}

/**	\brief
 *
 *	Sets the specific address into I2C_TAR register.
 *
 *	\param[in]	timeout : a duration of timeout protection
 *
 *	\retval 		error code , -1 is error , 0 is ok.
 */
int mx_check_write_ok(int timeout)
{
	int i;
	int status;
	int ret = (-1);			// -1 = error

	i = 0;
	while(1){
		status = mx_read_status_reg();
		//PrintHex(status);
		if (status == -1){		// -1 = error
			break;
		}
		/* WIP may not be 1 at early beginning, so we check WEL */	
//		if (!(status & MX_STATUS_BIT_WEL))	/* WEL will clear after write completion */{
		if ((status&0x03)==0)	/* WEL will clear after write completion */{
			ret = 0;		// 0 = ok
			break;
		}
		if (timeout > 0){
			if (++i > timeout){
				break;
			}
		}
	}

	return ret;
}





/**	\brief
 *
 *	Sets the specific address into I2C_TAR register.
 *
 *	\param[in]	command : Sector , Block , or Chip Erase.
 *	\param[in]	flashAddr : Erasing address of SPI Flash
 *
 *	\retval 		error code
 */
static int mx_issue_erase_command(int command, UINT32 flashAddr)
{
	int ret, i;
	
	mx_write_enable();			/* Setting Write Enable Latch bit */
	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);		/* Transmit Only mode */
	SPI_ENABLE();
	SPI_WRITE_DATA(command);		/* issue Sector Erase command */
	if (command != MX_CE){
		for (i = 2; i >= 0; i--){	/* write address from MSB to LSB */
			SPI_WRITE_DATA((flashAddr >> (i * 8)) & 0xFF);
		}
	}
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();		/* wait until not busy */
	// spi_wait_not_busy() is a must, or command transfer will be interrupt by the following SPI instruction
//	ret = mx_check_write_ok(0);
	ret = mx_check_write_ok(100000);
	SPI_DISABLE_FLASH();
	SPI_DISABLE();

	return ret;	
}






/**	\brief	Erase a sector
 *
 *	Erase a sector of MX SPI flash. Each secotr contains 4K bytes of data.
 *
 *	\param[in]	flashAddr	Assign the sector address to be erased.
 *                          It can be any address in the flash,
 *                          but only address bits [A23-A12] select the sector address.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_sector_erase(UINT32 flashAddr)
{
	return mx_issue_erase_command(MX_SE, flashAddr);
}






/**	\brief	Erase a block
 *
 *	Erase a block of MX SPI flash. Each secotr contains 64K bytes of data.
 *
 *	\param[in]	flashAddr	Assign the block address to be erased.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_block_erase(UINT32 flashAddr)
{
	return mx_issue_erase_command(MX_BE, flashAddr);
}






/**	\brief	Erase a whole chip
 *
 *	Erase whole MX SPI flash.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_chip_erase(void)
{
	return mx_issue_erase_command(MX_CE, 0);
}






/**	\brief	Program a page
 *
 *  There is Page Buffer (256 bytes) where you can temporarily store the data before
 *	they are actually wrote to the Memory Array (see \ref spi_mx_program).
 *  You can read 256 bytes of data or less from SDRAM and write them to a page of MX SPI flash.
 *  \li \c [Note] Before you program a page, you must call mx_sector_erase() to erase the sector
 *  at which the page is located.
 *
 *	\param[in]	flashDestAddr	The SPI flash destination address to which the data is write.
 *	\param[in]	sdramSrcAddr	The SDRAM source address from which the data is read.
 *	\param[in]	len             The length of data to be programmed. <B> Range is 1 ~ 256. </B>
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_page_program(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len)
{
	int i;
	int ret;				// error code
	int first_fill_len;
	int short_write;		// 1 when write length small than "SPI_FIFO_SIZE - 4"
	extern void spi_fill_tx_fifo(UINT32 sdramSrcAddr, int len);	
	
	mx_write_enable();			/* Setting Write Enable Latch bit */

	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);		/* Transmit Only mode */

	if (len <= (SPI_FIFO_SIZE - 4)){	// length was too short
		first_fill_len = len;		
		short_write = 1;
	}else{
		first_fill_len = SPI_FIFO_SIZE - 4;	// the maximun length is "SPI_FIFO_SIZE - 4"
		short_write = 0;		
	}

	/* SPI Flash protocol is :
		Command (1 byte) + Address (3 bytes) + Data ( 0~28 bytes )
	*/
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_PP);		/* issue Page Program command */
	for (i = 2; i >= 0; i--){				/* write 3-byte address from MSB to LSB */
		SPI_WRITE_DATA(flashDestAddr >> (i * 8));
	}
	for (i = 0; i < first_fill_len; i++){		/* write first 28 bytes data to fill the TX FIFO(32 bytes) */
		SPI_WRITE_DATA(VPchar(sdramSrcAddr++));
	}

	if (short_write){
		SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	}else{
		/* NOT short_write . We should deal with the remaining data. */
		extern void printSpiAddr(void);
		len -= (SPI_FIFO_SIZE - 4);			// cut the transmitted data off.
		#ifdef TX_ISR_MODE
		gSpiSdramSrcAddr = sdramSrcAddr;
		gSpiSdramEndAddr = sdramSrcAddr + len;
		SPI_ENABLE_TX_INT();		
		SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
		while (1){
			/* wait for completion of write */
			if (gSpiSdramSrcAddr == gSpiSdramEndAddr){
				break;
			}
		}
		#else
		/* fill remaining data to TX FIFO must be in assembly to catch up the serial transfer */
		/* CS0# low to start transmission is set in spi_fill_tx_fifo() */
		spi_fill_tx_fifo(sdramSrcAddr, len);
		#endif
	}

	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();			/* wait until not busy */
	SPI_DISABLE_FLASH();
	SPI_DISABLE();				/* also clear TX/RX FIFO */
	#ifdef TX_ISR_MODE
	SPI_DISABLE_TX_INT();
	#endif

	ret = mx_check_write_ok(100000);

	return ret;	
}






/**	\brief	Write data to MX SPI flash
 *
 *	Read data from SDRAM and write them to MX SPI flash.
 *
 *	\param[in]	flashDestAddr	The SPI flash destination address to which the data is write.
 *	\param[in]	sdramSrcAddr	The SDRAM source address from which the data is read.
 *	\param[in]	len             The length of data to be wrote.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_write_data(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len)
{
    	int wlen, ret = 0;

	spi_accessing = 1;
	
	while (len > 0){
		
		if (len > MX_PAGE_SIZE){		// too long
			wlen = MX_PAGE_SIZE;
		}else{
			wlen = len;
		}
		
		ret = mx_page_program(flashDestAddr, sdramSrcAddr, wlen);
		if (ret < 0){
			break;
		}
		
		flashDestAddr += wlen;
		sdramSrcAddr += wlen;
		len -= wlen;
		
	}

	spi_accessing = 0;

    return ret;
}

#ifndef BOOT

/**	\brief
 *
 *	Gets the module ID of product from FlashRam.
 *
 *	\param[in]	target_address : a specific address
 *
 *	\retval 		module ID
 */
ulong GetNetModuleID(void)
{
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, SMC_FLASH0_HWEXID, 4);	// using SPI interface
	return *(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE);	
}

#endif
