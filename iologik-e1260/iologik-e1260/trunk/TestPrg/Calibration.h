/* Calibrtaion.h*/
#define cal_port		0

#define DCV				0x30
#define DCA				0x31
#define OHM				0x32
#define	TC				0x33
#define	RTD				0x34
#define	PULSE			0x35
//#define OUT?			0x36

#define	Back_Light_OFF	0x30
#define	Back_Light_ON	0x31

#define SOURCE			0x30
#define SINK			0x31

#define TURN_OFF		0x30
#define TURN_ON			0x31

#define DIVIDE_DISABLE	0x30
#define DIVIDE_ENABLE	0x31

#define DCV_100mV		0x30
#define	DCV_1V			0x31
#define	DCV_10V			0x32
#define	DCV_30V			0x33

#define	DCA_20mA		0x30
#define	DCA_4_20mA		0x31

#define	OHM_500			0x30
#define	OHM_5K			0x31
#define	OHM_50K			0x32
#define OHM_0				0x00

#define	TC_K_TYPE		0x30
#define	TC_E_TYPE		0x31
#define	TC_J_TYPE		0x32
#define	TC_T_TYPE		0x33
#define	TC_R_TYPE		0x34
#define	TC_B_TYPE		0x35
#define	TC_S_TYPE		0x36
#define	TC_N_TYPE		0x37
#define	TC_L_TYPE		0x38
#define	TC_U_TYPE		0x39

#define	RTD_PT100		0x30
#define	RTD_JPT100		0x31

#define	PULSE_100HZ		0x30
#define	PULSE_1000HZ	0x31
#define	PULSE_10KHZ		0x32
#define	PULSE_50KHZ		0x33
#define	PULSE_1000CPM	0x34

#define	TEMPERATURE_VALUE	0x30
#define	VOLTAGE_VALUE		0x31
#define	ROOM_TERPERATURE	0x32

#define QUERY			0x3F	//'?'

#define CA150_tran_OK		0
#define CA150_tran_Fail		1
#define CA150_command_fail	2

#define Fluke_5500A_tran_OK 			0
#define Fluke_5500A_tran_Fail			1
#define Fluke_5500A_command_fail	2


#define calibrator_fluke	1
#define calibrator_ca150	0

void Calibrator_communication_init(void);
void calibrator_reset(void);
void Calibrator_out(void);
void Calibrator_Set_Output_value(UINT8 *parameter);
void Set_Calibrator(UINT8 parameter);
void Calibrator_communication_stop(void);
void calibrator_select(void);
void Calibrator_Set_State(UINT8 parameter);

#define DIAG_GET_VALUE_DEC_MODE		1

/*CA150 command*/
void transmit_CA150(UINT8* transmit_data,UINT8 length, UINT8* receive_data);
void CA150_init(void);
UINT8 CA150_Set_Source_func(UINT8 parameter);
UINT8 CA150_Set_Source_State_func(UINT8 parameter);
UINT8 CA150_Set_Source_Range_func(UINT8 parameter);
UINT8 CA150_Set_TC_RTD_daiplay_func(UINT8 parameter);
UINT8 CA150_Set_divide_output_value(UINT8 dividend,UINT8 divisor);
UINT8 CA150_Set_increase_output_value(UINT8 parameter);
UINT8 CA150_Set_divide_output(UINT8 parameter);
UINT8 CA150_Set_current_type_func(UINT8 parameter);
UINT8 CA150_Set_output_value(UINT8* parameter);
void CA150_setting_information(void);
UINT8 CA150_Set_meausre_State_func(UINT8 parameter);
UINT8 CA150_Backlight(UINT8 parameter);
UINT8 CA150_Read_meausre_value_func(void);

/*Fluke 5500A command*/

void Fluke_5500A_Reset(void);
void Fluke_5500A_Output_value(void);
#if E1262
void Fluke_5500A_Set_mV_value(UINT8* value);
#endif
#if E1260
void Fluke_5500A_Set_R_value(UINT8* value);
#endif
void Fluke_525B_Reset(void);