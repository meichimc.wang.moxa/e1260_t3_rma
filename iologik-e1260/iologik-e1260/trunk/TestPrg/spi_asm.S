/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    spi_asm.S

    Routines for accessing SPI flash

    2008-06-10	Albert Yu
		first release
*/

#include "chipset.h"
#include "spi_reg.h"

.global gSpiSdramDestAddr
gSpiSdramDestAddr:
                .word   0

.global gSpiSdramSrcAddr
gSpiSdramSrcAddr:
                .word   0

.global gSpiSdramEndAddr
gSpiSdramEndAddr:
                .word   0

/*============================================================================*/
/* void spi_isr(void)                                                         */
/*============================================================================*/
.globl spi_isr
spi_isr:
                STMFD   sp!, {r0-r7, lr}        // push work registers and return address to stack
                
                LDR     r1, =0x05040000         // r1 = SPI base address

                LDR     r2,[r1,#0x30]           // Interrupt Status Register
                TST     r2,#0x10                // (RX FIFO Full) ?
                BLNE    _spiHandleRxFull
                TST     r2,#0x08                // (RX FIFO Overflow) ?
                BLNE    _spiHandleRxOverflow
                TST     r2,#0x01                // (TX FIFO Empty) ?  
                BLNE    _spiHandleTxEmpty
    
                LDMFD   sp!, {r0-r7, lr}        // restore work registers and return address
                MOV     pc, lr                  // return to caller


                // ---- handle RX FOFO Full interrupt -----
_spiHandleRxFull:
                LDR     r7, gSpiSdramDestAddr      // r7 = SDRAM destination address
                MOV     r5, #RX_FIFO_THRESHOLD  // set read loop counter
#if 1
_spiRxLoop:     LDR     r3,[r1,#0x60]           // r3 = data in SPI Rx FIFO : byte 0
                LDR     r4,[r1,#0x60]           // byte 1
                ORR     r3,r3,r4,LSL#8          // byte 1, byte 0
                LDR     r4,[r1,#0x60]           // byte 2
                ORR     r3,r3,r4,LSL#16         // byte 2,  byte 1, byte 0
                LDR     r4,[r1,#0x60]           // byte 3
                ORR     r3,r3,r4,LSL#24         // byte 3, byte 2,  byte 1, byte 0
                STR     r3,[r7]                 // write r3 to SDRAM
                ADD     r7,r7,#4                // increment SDRAM address
                SUBS    r5,r5,#4                // decrement loop counter
                BNE     _spiRxLoop              // read more
#else
_spiRxLoop:     LDR     r4,[r1,#0x60]           // r4 = data in SPI Rx FIFO
				STRB	r4,[r7]                 // write r4 to SDRAM
				ADD		r7,r7,#1                // increment SDRAM address
				SUBS	r5,r5,#1                // decrement read loop counter
                BNE     _spiRxLoop              // read more
#endif
                STR     r7, gSpiSdramDestAddr      // restore SDRAM destination address
                MOV     pc, lr                  // return to caller

                
                // ---- handle RX FIFO Overflow interrupt -----
_spiHandleRxOverflow:
                LDR     r0, [r1, #0x3C]         // Clear RX FIFO Overflow interrupt 
                STMFD   sp!, {r0-r3, lr}
                BL      spi_show_rx_overflow
                LDMFD   sp!, {r0-r3, lr}
                MOV     pc, lr                  // return to caller


                // ---- handle TX FIFO Empty interrupt -----
_spiHandleTxEmpty:
                LDR     r7, gSpiSdramSrcAddr    // r7 = SDRAM source address                
                LDR     r6, gSpiSdramEndAddr    // r6 = SDRAM end address              
                MOV     r5, #(SPI_FIFO_SIZE - TX_FIFO_THRESHOLD)    // set write loop counter
_spiTxLoop:     LDRB    r0, [r7]                // r0 = SDRAM data
                STRB    r0, [r1, #0x60]         // write r0 to SPI TX FIFO
				ADD		r7, r7, #1              // increment SDRAM address
				CMP     r7, r6                  // (gSpiSdramSrcAddr < gSpiSdramEndAddr) ?
				BLT     _notEnd
				LDR     r0, [r1, #0x2C]
				BIC     r0, r0, #0x01           // disable TX FIFO Empty interrupt
				STR     r0, [r1, #0x2C]
				B       _txIntEnd
    _notEnd:
				SUBS	r5, r5, #1              // decrement write loop counter
                BNE     _spiTxLoop              // read more
    _txIntEnd:
                STR     r7, gSpiSdramSrcAddr    // restore SDRAM source address
                MOV     pc, lr                  // return to caller
                

/*============================================================================*/
/* void spi_fill_tx_fifo(UINT32 sdramSrcAddr, int len)                        */
/*============================================================================*/
.globl spi_fill_tx_fifo
spi_fill_tx_fifo:
                STMFD   sp!,{r2-r5, lr}         // push work registers to stacks

                ADD     r5, r0, r1              // sdramEndAddr = sdramSrcAddr + len

                LDR     r3, =0x05040000         // r3 = SPI base address

                MOV     r2, #0x01
                STR     r2, [r3, #0x10]         // enable slave 0 (CS#0 low and start transmission) 

_write_more:                
                LDR     r2, [r3, #0x20]         // i = Tx FIFO Level register
    _write_loop:                
                CMP     r2, #32
                BEQ     _write_more             // if (ri < SPI_TX_FIFO_SIZE) {
                LDR     r4, [r0]                //
                STR     r4, [r3, #0x60]         //      Tx FIFO Data register = *(uchar*)sdramSrcAddr
                ADD     r0, r0, #1              //      sdramSrcAddr++
                CMP     r0, r5
                BEQ     end1                    //      if (sdramSrcAddr == sdramEndAddr) return;                
                ADD     r2, r2, #1              //      i++
                B       _write_loop             // }

    end1:
                LDMFD   sp!,{r2-r5, lr}         // restore work registers and spsr_IRQ
                MOV     pc, lr                  // return to caller   
