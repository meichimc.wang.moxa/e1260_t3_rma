/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    isr.c

    Interrupt Service Routine of bootloader

    2008-06-10	Albert Yu
		first release
*/

/*! \file
\ingroup grpBoot
Interrupt dispatcher of system interrupt. A Interrupt Handlers Table is
maintained at which you can register the ISR of SPI controller. SPI controller is
defined to trigger IRQ#0.
*/

#include	"types.h"
#include    "chipset.h"
#include	"isr.h"

static	void	(*InterruptHandlers[MAXHNDLRS])(void);

/**	\brief
 *
 *	Dummy Interrupt Service Routine
 *
 */
static	void	ISR_Dummy(void)
{
//	printf("\r\n * Dummy Interrupt Service Routine !!!");
}

/**	\brief
 *
 *	Disable all interrupts
 *
 */
static	void	clearIrqStatus(void)
{
	VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) = 0x00000000;
	VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) = 0x00000000;	/* 0 : Disable the interrupt source */	
    VPlong(S2E_INTC_BASE + IRQ_PLEVEL)     = 0x00000000;
}

/**	\brief
 *
 *	Initial ISR array and assign the dummy ISR to each interrupt.
 *
 */
static	void	initIrqTable(void)
{
	int	i;

	for ( i=0; i<MAXHNDLRS; i++ )
		InterruptHandlers[i] = ISR_Dummy;
}

/**	\brief
 *
 *	Initial ISR array and assign the dummy ISR to each interrupt.
 *
 */
void	Ssys_isrInit(void)
{
	clearIrqStatus();
	initIrqTable();
}

/**	\brief
 *
 *	Register interrupt handler to ISR array.
 *
 *	\param[in]	vector      Vector number
 *	\param[in]	handler     A function pointer to real interrupt service routine
 *
 */
void	setvect(volatile unsigned long vector, void (*handler)(void))
{
	InterruptHandlers[vector] = handler;
}

/**	\brief
 *
 *	Get interrupt handler
 *
 *	\param[in]	vector      Vector number
 *
 *	\return     Pointer to interrupt handler
 *
 */
void	(*getvect(volatile unsigned long vector))(void)
{
	return(InterruptHandlers[vector]);
}

//=================================================================
//  Exception Handler Function
//=================================================================
void	Ssys_isrUndefHandler(volatile unsigned long *address)
{
	//printf("\r\n * Exception -> Undefined Abort : [0x%lx] : 0x%lx  !!!", (long)address, *address);
	while(1);
}

void	Ssys_isrPrefetchHandler(volatile unsigned long *address)
{
	//printf("\r\n * Exception -> Prefetch  Abort : [0x%lx] : 0x%lx  !!!", (long)address, *address);
	while(1);
}

void	Ssys_isrAbortHandler(volatile unsigned long *address)
{
	//printf("\r\n * Exception -> Data Abort : [0x%lx] : 0x%lx  !!!", (long)address, *address);
	while(1);
}

void	Ssys_isrSwiHandler(volatile unsigned long number)
{
	//printf("\r\n * Exception -> Software Interrupt Number %ld!!! ", number);
}

/**	\brief
 *
 *	ISR dispatcher. Check if the interrupt was be masked first.
 *	Find the number of IRQ and call the corresponding ISR.
 *
 */
void	Ssys_isrIrqHandler(void)
{
	unsigned long IntOffSet,tmp;
	int i,j;

    for(;;){
    	tmp = IntOffSet = VPlong(S2E_INTC_BASE + IRQ_FINALSTATUS);
    	if( IntOffSet == 0 )
    		return;
    	for(j=0,i=0; tmp > 0L;i++){
    	    if(tmp & 0x01){
        		(*InterruptHandlers[i])();
        	}
        	tmp = tmp >> 1;		
        	j++;
        }
    }	
}

/**	\brief
 *
 *	ISR dispatcher. Check if the interrupt was be masked first.
 *	Find the number of FIQ and call the corresponding ISR.
 *
 */
void	Ssys_isrFiqHandler(void)
{
	unsigned long IntOffSet,tmp;
	int i,j;

    for(;;){
    	tmp = IntOffSet = VPlong(S2E_INTC_BASE + FIQ_FINALSTATUS);
    	if( IntOffSet == 0 )
    		return;
    	for(j=0,i=0; tmp > 0L;i++){
    	    if(tmp & 0x01){
        		(*InterruptHandlers[i])();
        	}
        	tmp = tmp >> 1;		
        	j++;
        }
    }	

}

/**	\brief
 *
 *	Enable IRQ interrupts.
 *
 */
void enable_interrupts(void)
{
    unsigned long temp;
    __asm__ __volatile__("mrs %0, cpsr\n"
			 "bic %0, %0, #0x80\n"
			 "msr cpsr_c, %0"
			 : "=r" (temp)
			 :
			 : "memory");
}

/**	\brief
 *
 *	Disable IRQ/FIQ interrupts.
 *	Returns true if interrupts had been enabled before we disabled them.
 *
 */
int disable_interrupts(void)
{
    unsigned long old,temp;
    __asm__ __volatile__("mrs %0, cpsr\n"
			 "orr %1, %0, #0xc0\n"
			 "msr cpsr_c, %1"
			 : "=r" (old), "=r" (temp)
			 :
			 : "memory");
	clearIrqStatus();
    return (old & 0x80) == 0;
}
