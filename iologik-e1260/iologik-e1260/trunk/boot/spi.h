/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    spi.h

    Routines for accessing SPI flash

    2008-06-10	Albert Yu
		new release
*/

#ifndef _SPI_H
#define _SPI_H

/*
 * Flash type definition
 */
#define FLASH_TYPE_MX25L        0

/*
 * MX25 SPI flash command definition
 */
#define MX_WREN			0x06	/* Write Enable */
#define MX_WRDI			0x04	/* Write Disable */
#define MX_RDID 		0x9F	/* Read ID */
#define MX_RDSR			0x05	/* Read Status Register */
#define MX_WRSR			0x01	/* Write Status Register */
#define MX_READ			0x03	/* Read Data */
#define MX_FAST_READ	0x0B	/* Fast Read Data */
#define MX_SE			0x20	/* Sector Erase */
#define MX_BE			0xD8	/* Block Erase */
#define MX_CE			0x60	/* Chip Erase */
#define MX_PP			0x02	/* Page Program */
#define MX_CP			0xAD	/* Continuously Program */

#define MX_STATUS_BIT_WIP	0x01	/* Write In Progress */
#define MX_STATUS_BIT_WEL	0x02	/* Write Enable Latch */

#define MX_SECTOR_SIZE  4096
#define MX_PAGE_SIZE    256
#endif
