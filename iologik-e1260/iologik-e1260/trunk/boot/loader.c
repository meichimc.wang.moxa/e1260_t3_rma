/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    loader.c

    Loader for loading program on SPI flash to SDRAM

    2008-06-10	Albert Yu
		first release
*/

/*! \defgroup grpBoot boot-loader
 *  Source files that compose boot-loader
*/

/*! \file
\ingroup grpBoot
Determines either Manufacture/Test program or Firmware to be loaded,
and call routines in spi.c to load the desired program.
*/

/*! \mainpage S2E boot sequence

\section intro_sec Introduction
This document describes the boot sequence of S2E CPU and the programming of access to SPI Flash.\n

The S2E CPU has built-in 2MB SPI Flash, 4MB SDRAM, and 4KB SRAM (see \subpage cpu_block).
The SPI Flash must be accessed using SPI serial interface, and does not occupy memory space of CPU.
Not like ROM Flash, programs on SPI Flash can not be executed directly by CPU,
they must be loaded to SRAM or SDRAM for execution.
A 4KB SRAM is built in to store the boot-loader program loaded from SPI Flash on system startup.
The boot-loader in SRAM is then executed to initialize SPI controller and SDRAM,
and load Manufacture/Test program or Firmware from SPI Flash to SDRAM, finally transfer
execution to the loaded program in SDRAM.\n

In the very beginning the SPI Flash contains no data, CPU is set to \c Download \c Mode,
then the \subpage bios_file "BIOS file" is downloaded to SPI Flash via outside flash programmer.
After this, CPU is set to \c Normal \c Mode. When CPU is powered on in normal mode,
a boot sequence as described in the following section will take place.

       
\section boot_sec Boot Sequence
Boot process is composed of the following three stages after power-on.
\li \subpage hw_boot
\li \subpage sw_boot
\li \subpage remap

\section file_list Files Description
The file name of boot-loader is \c armboot.bin, which is composed of object codes compiled from
following source codes. The length of \c armboot.bin must be less than or equal to 4K bytes,
so it can be put in the 4KB SRAM for execution on startup (see \subpage bios_file).
\li \c start.S Contains assembly code runs on system startup (boot code).
It will setup SPI controller, SDRAM, and call loader() in loader.c.
\li \c loader.c	Determines either Manufacture/Test program or Firmware to be loaded,
and call routines in spi.c to load the desired program.
\li \c spi.c Initializes SPI controller and contains routines for accessing SPI Flash.
\li \c isr.c Interrupt dispatcher of system interrupt. A Interrupt Handlers Table is
maintained at which you can register the ISR of SPI controller. SPI controller is
defined to trigger IRQ#0.

\section SPI_sec SPI Programming
\li \subpage spi_ctrl
\li \subpage spi_mx_read
\li \subpage spi_mx_erase
\li \subpage spi_mx_program
*/

/*! \page cpu_block S2E CPU block diagram
\image html CPU_block.gif
*/

/*! \page bios_file BIOS file content
The BIOS file is downloaded to the first 256K bytes of 2M SPI Flash.

\image html bios_file.gif
*/

/*! \page hw_boot Hardware boot-loader
Because there is no built-in ROM in the chip, a hardware boot-loader will
automatically load the first 4K bytes code from serial flash after power-on.
When it's in process, all modules are in reset state excluding boot-loader and SRAM,
the system reset signal "sys_rstn" will be asserted until completion of this process.

\image html hw_boot_period.jpg

The program code will be directly loaded into SRAM through "SPI boot-loader"
not SDRAM controller (see \subpage cpu_block). After completion of this process, system reset
signal de-asserts, the system starts to execute the program from SRAM.

\image html hw_boot.gif
*/

/*! \page sw_boot Software boot-loader
System starts to execute program from SRAM alias address 0x0000_0000.\n

start.S
<OL start=1>
<LI>disable all interrupts in Interrupt Controller (FIQ, IRQ of CPSR are already disabled when reset)</LI>
<LI>init debug LED, and light debug LED</LI>
<LI>init SDRAM</LI>
<LI>set stack for Supervisor mode with sp = 0x0000_1000</LI>
<LI>init BSS segment</LI>
<LI>init SPI controller</LI>
<LI>init SPI flash</LI>
<LI>call loader() in loader.c</LI>
</OL>
loader.c :: loader()
<OL start=8>
<LI>set LOAD_SRC_ADDR = 0x00_2000 (Manufacture/Test program)</LI>
<LI>check JP1: if jumper is short, go to step 11</LI>
<LI>read 4 bytes \ref config_data "Manufacture Flag" from address (0x00_1000 + 0x1C) of SPI flash. If (MP, Serial_MAC, BR) flags are all set, LOAD_SRC_ADDR = 0x05_0000 (Firmware)</LI>
<LI>load \ref load_bios "Manufacture/Test program" or \ref load_firm "Firmware" in SPI flash to SDRAM:</LI>
<OL TYPE='i'>
<LI>Read 4 bytes LENGTH filed in file header from address (LOAD_SRC_ADDR + LENGTH_OFFSET(0x08)) of SPI flash</LI>
<LI>Read SPI flash data from address (LOAD_SRC_ADDR + PROGRAM_OFFSET(0x20)), and store it to SDRAM starting from address 0x0100_0000 until LENGTH bytes are all read out</LI>
</OL>
</OL>
<HR>
\anchor config_data Manufacture/Test config data content (address base = 0x00_1000):
\image html config_data.gif
<HR>
\anchor load_bios Load Manufacture/Test program:
\image html load_bios.gif
<HR>
\anchor load_firm Load Firmware:
\image html load_firmware.gif
*/ 

/*! \page remap Remap
<OL start=1>
<LI>Branch to real SRAM address (current program counter + 0x300_0000)</LI>
<LI>Set remap register = 1</LI>
<LI>Branch to 0x0000_0000 to run program in SDRAM</LI>
</OL>
\image html remap.gif
*/ 

/**	\brief	Load program file from MX SPI flash to SDRAM
 *
 *	The first 32 bytes of a program file in flash is the file header, which contains the file
 *  information including file length. This routine will read the file length from the file
 *  header and copy the entire file from SPI flash to SDRAM, so the caller doesn't need to
 *  pass the file length parameter.
 *
 *	\param[in]	sdramDestAddr	The SDRAM destination address to which the program is write.
 *	\param[in]	flashSrcAddr	The SPI flash source address from which the program is read.
 *
 */
 
 /*
    loader.c

    Interrupt Service Routine of bootloader

    2009-08-03	Nolan Yu
		first release
*/

#include	"types.h"
#include	"chipset.h"
#include	"spi.h"

#define MANUFACTURE_CONFIG_FLASH_BASE	0x001000
#define MANUFACTURE_PROGRAM_FLASH_BASE	0x002000
#define FIRMWARE_FLASH_BASE				0x050000

#define SDRAM_ADDR_BASE_BEFORE_REMAP	0x1000000

/*
 * The format of Manufacture/Test program file is :
 *
 * ---------   +---------------+---------------+---------------+---------------+
 * Header      |     magic     |     version   |    Package    |  Program type |
 *   32 bytes  +---------------+---------+-----+---------------+---------------+
 *	       	   |    Length     | checksum|     reserved = 0(10 bytes)	       |
 * ---------   +---------------+---------+-----+-------------------------------+
 * CODE + DATA |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 * ---------   +---------------------------------------------------------------+
 *
 * magic   : 4 bytes
 * version : 4 bytes
 * length  : 4 bytes, length of program (CODE + DATA)
 * Package : 4 bytes
 * Program type : 4 bytes 
 * checksum: 2 bytes, the check sum of CODE + DATA
 * reserve : 12 bytes
 */

#define LENGTH_OFFSET	16
#define PROGRAM_OFFSET	32

ulong program_len;

void showLED(int num)
{
	ulong val;

	val = VPlong(S2E_GPIO_BASE + GPIO_DATA);
/*	num &= (ulong)0xFF;
	num ^= (ulong)0xFF;
	val &= ~(ulong)0xFF;
	val |= num;
*/val = num;
	VPlong(S2E_GPIO_BASE + GPIO_DATA) = val;
}

void haltSystem(void)
{
	while (1)
		showLED(0x0);
}

#if 1
#define Inw(addr)			(*((volatile unsigned long*)(addr)))
#define Outw(addr, val)		*((volatile unsigned long*)(addr)) = (val)

int Dbg_sio_write(char *buf, int len)
{
	int i;

	i = 0;
	while (i < len)
	{
		if (Inw(S2E_DW_UART_BASE + 0x7C/* UART Status Register */) & 0x02/*TX FIFO not full*/)
			Outw((S2E_DW_UART_BASE + 0x00), buf[i++]);
	}
	return len;
}

void Dbg_sio_putch(unsigned char ch)
{
	Dbg_sio_write(&ch,1);
}

int Strlen(const char * s)
{
	const char *sc;

	for (sc = s; *sc != '\0'; ++sc)
		/* nothing */;
	return sc - s;
}


void PrintStr(char *str)
{
	Dbg_sio_write(str, Strlen(str));
}

//static char digits[]="0123456789ABCDEF";
void PrintHex(ulong value)
{
	int i, idx;

	for (i = 7; i >= 0; i--){
		idx = value >> (i*4);
//		Dbg_sio_putch(digits[idx&0xf]);
	}
}

//#define PRINT_SHOW_LOADING_PROGRAM		// print or not
void showLoadingProgram(ulong flashAddr)
{
#ifdef PRINT_SHOW_LOADING_PROGRAM

	PrintStr("\r\nLoading ");
	if (flashAddr == MANUFACTURE_PROGRAM_FLASH_BASE)
		PrintStr("BIOS");
	else
		PrintStr("Firmware");
	PrintStr(" from 0x");
	PrintHex(flashAddr);
	PrintStr(" ...");
	
#endif
}


//#define PRINT_SHOW_NO_PROGRAM_ERR		// print or not
void showNoProgramErr(void)
{
#ifdef PRINT_SHOW_NO_PROGRAM_ERR
	PrintStr("ERR: No program !!!");
#endif
	haltSystem();
}


//#define PRINT_SDRAM_TEST			// print or not
void sdram_test(void)
{
#if 0
	#define START_ADDR	0x1000000	/* SDRAM address before remap */
	#define END_ADDR	(START_ADDR + 4*1024*1024)
	ulong val;
	int i;
	ulong pattern;
	extern int sdram_byte_test(UINT32 sdramStartAddr, UINT32 sdramEndAddr, UINT32 pattern);
	extern int sdram_short_test(UINT32 sdramStartAddr, UINT32 sdramEndAddr, UINT32 pattern);
	extern int sdram_long_test(UINT32 sdramStartAddr, UINT32 sdramEndAddr, UINT32 pattern);
	
	//return;

while (1)
{
#ifdef PRINT_SDRAM_TEST
	PrintStr("\r\nTest SDRAM (0x");
	PrintHex(START_ADDR);
	PrintStr(" - 0x");
	PrintHex(END_ADDR);
	PrintStr(")\r\n");
#endif

#if 1

#ifdef PRINT_SDRAM_TEST
	PrintStr("[write ALL then read ALL test]\r\n");
	PrintStr("Writing...\r\n");
#endif

	pattern = 0x00000000;
	for (i = START_ADDR; i < END_ADDR; i += 4){
		VPlong(i) = pattern++;
	}

#ifdef PRINT_SDRAM_TEST
	PrintStr("Read & Comparing...\r\n");
#endif	
	pattern = 0x00000000;	
	for (i = START_ADDR; i < END_ADDR; i += 4)
	{
#ifdef PRINT_SDRAM_TEST		
		PrintHex(i);
		PrintStr("\r");
#endif		
		val = VPlong(i);
		if (val != pattern){
#ifdef PRINT_SDRAM_TEST			
			PrintStr("\r\nERROR!\r\nAddress= 0x");
			PrintHex(i);
			PrintStr("\r\n");
			PrintStr("read= ");
			PrintHex(val);
			PrintStr(", write= ");
			PrintHex(pattern);
#endif			
			haltSystem();
		}
		pattern++;
	}
#ifdef PRINT_SDRAM_TEST
	PrintStr("[BYTE write then read test]\r\nTesting...\r\n");
#endif	
	for (i = START_ADDR; i < END_ADDR; i++){
		uchar ch;
		VPchar(i) = 0x5A;
#ifdef PRINT_SDRAM_TEST	
		PrintHex(i);
		PrintStr("\r");
#endif		
		ch = VPchar(i);
		if (ch != 0x5A){
#ifdef PRINT_SDRAM_TEST			
			PrintStr("\r\nERROR!\r\nAddress= 0x");
			PrintHex(i);
			PrintStr("\r\n");
			PrintStr("read= ");
			PrintHex(ch);
			PrintStr(", write= ");
			PrintHex(0x5A);
#endif			
			haltSystem();
		}
	}
#endif

#if 0
#ifdef PRINT_SDRAM_TEST
	PrintStr("[BYTE write then read test]\r\nTesting...\r\n");
#endif	
	i = sdram_byte_test(START_ADDR, END_ADDR, 0x5A);
	if (i < END_ADDR){
#ifdef PRINT_SDRAM_TEST		
		PrintStr("ERROR! address= 0x");
		PrintHex(i);
		PrintStr(", write= 0x5A, read= 0x");
		PrintHex(VPchar(i));
#endif		
		haltSystem();
	}
#endif

#if 0
#ifdef PRINT_SDRAM_TEST
	PrintStr("[SHORT write then read test]\r\nTesting...\r\n");
#endif	
	i = sdram_short_test(START_ADDR, END_ADDR, 0x5AA5);
	if (i < END_ADDR)	{
#ifdef PRINT_SDRAM_TEST		
		PrintStr("ERROR! address= 0x");
		PrintHex(i);
		PrintStr(", write= 0x5AA5, read= 0x");
		PrintHex(VPshort(i));
#endif
		haltSystem();
	}
#ifdef PRINT_SDRAM_TEST
	PrintStr("[LONG write then read test]\r\nTesting...\r\n");
#endif	
	i = sdram_long_test(START_ADDR, END_ADDR, 0x5AA5A55A);
	if (i < END_ADDR){
#ifdef PRINT_SDRAM_TEST		
		PrintStr("ERROR! address= 0x");
		PrintHex(i);
		PrintStr(", write= 0x5AA5A55A, read= 0x");
		PrintHex(VPlong(i));
#endif		
		haltSystem();
	}
#endif

#ifdef PRINT_SDRAM_TEST
	PrintStr("Test OK\r\n");
#endif	
	}
#endif
}





//#define PRINT_SUM_CHECK				// print or not
int sum_check(ulong len, ulong sum)
{
	ulong i, sumlen;
	ulong mysum, word;

	mysum = 0;
	sumlen = len & ~(ulong)(0x03);	/* align to word boundary */
	for (i = 0; i < sumlen; i += 4){
		word = VPlong(SDRAM_ADDR_BASE_BEFORE_REMAP + i);
		mysum += word;
	}
#ifdef PRINT_SUM_CHECK	
	PrintStr("OK\r\n");
	PrintStr("len =0x");
	PrintHex(len);
	PrintStr("\r\n");
	PrintStr("sum0=0x");
	PrintHex(sum);
	PrintStr("\r\nsum1=0x");
	PrintHex(mysum);
	PrintStr("\r\n");
#endif

	if (sum != mysum){
#ifdef PRINT_SUM_CHECK		
		PrintStr("Checksum ERROR!!!");
#endif		
		return -1; 
		//haltSystem();
	}else{
#ifdef PRINT_SUM_CHECK		
		PrintStr("run program...\r\n");
#endif		
	}
	return 0;
}
#else

static void load_program(int sdramDestAddr, int flashSrcAddr)
{
	mx_read_data((int)&program_len, flashSrcAddr + LENGTH_OFFSET, 4);
	mx_read_data(sdramDestAddr, flashSrcAddr + PROGRAM_OFFSET, program_len);
}

/**	\brief	Load firmware file from MX SPI flash to SDRAM
 *
 *  This routine is called by start.S after remap (in normal mode).
 *  It will load firmware file located at flash address FIRMWARE_FLASH_BASE to
 *  SDRAM address 0x00000000.
 *
 */
void load_firmware(void)
{
	load_program(0, FIRMWARE_FLASH_BASE);
}

/**	\brief	Check if JP1 is short
 *
 *	\retval 0   JP1 is open
 *  \retval 1   JP1 is short
 *
 */
int JP1Short(void)
{
#define PIO(ch)		ch
#define PIO_JP1		PIO(3)

	if ((VPlong(S2E_GPIO_BASE + GPIO_DATA)) & ((ulong)0x01 << PIO_JP1))
		return 0;
	else
		return 1;
}

ulong m_flag;

#define M_FLAG_FLASH_ADDR	(MANUFACTURE_CONFIG_FLASH_BASE + 0x1C)
#define FLAG_MP				0x00000001      /* MP Flag */
#define FLAG_SERIAL_MAC		0x00000002		/* Serial No. & MAC Flag */
#define FLAG_EOT			0x00000004		/* EOT Flag */
#define FLAG_BR				0x00000008		/* BR Flag */
#define FLAG_HWEXID			0x00000010		/* HWEXID ID Flag */
#define LOAD_FIRM_FLAGS		(FLAG_MP | FLAG_SERIAL_MAC | FLAG_BR)

/**	\brief  Boot mode loader
 *
 *  This routine is called by start.S before remap (in boot mode).
 *  If JP1 is not short and manufacture flags MP, SERIAL_MAC and BR are all set,
 *  it will load firmware located at flash address FIRMWARE_FLASH_BASE, otherwise
 *  it will load manufacture/test program located at flash address
 *  MANUFACTURE_PROGRAM_FLASH_BASE. The destination to which the program is loaded
 *  is SDRAM address SDRAM_ADDR_BASE_BEFORE_REMAP.
 *
 */
void loader(void)
{
	int loadAddr = MANUFACTURE_PROGRAM_FLASH_BASE;

    /* Set Control Register 0: TMOD(9:8)=3 EEPROM Read, CFS(15:12)=7, SCPOL(7)=1, SCPH(6)=1, FRF(5:4)=0, DFS(3:0)=7 */
    VPlong(S2E_SPI_BASE + SPI_CONTROL0) = 0x73C7;

    VPlong(S2E_SPI_BASE + SPI_CLOCK_DIV) = 0x04;    /* Test low speed */

    spi_init();

#if 0
	//if (!JP1Short())
	{	/* JP1 is not short */
		/* Read manufacture flag from SPI flash */
		mx_read_data((int)&m_flag, M_FLAG_FLASH_ADDR, 4);
		/* If MP, SERIAL_MAC and BR flags are all set, load firmware */
		if ((m_flag & LOAD_FIRM_FLAGS) == LOAD_FIRM_FLAGS)
			loadAddr = FIRMWARE_FLASH_BASE;
	}
#endif

	/* load Manufacture/Test program or firmware */
	load_program(SDRAM_ADDR_BASE_BEFORE_REMAP, loadAddr);

    /* Set Control Register 0: TMOD(9:8)=0 Transmit & Receive, CFS(15:12)=7, SCPOL(7)=1, SCPH(6)=1, FRF(5:4)=0, DFS(3:0)=7 */
    VPlong(S2E_SPI_BASE + SPI_CONTROL0) = 0x70C7;
}
#endif
