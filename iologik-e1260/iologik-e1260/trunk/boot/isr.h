
#ifndef _ISR_H
#define _ISR_H
/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    isr.h

    Interrupt Service Routine of bootloader

    2008-06-10	Albert Yu
		first release
*/

//=================================================================
// Default Values
//=================================================================
#define MAXHNDLRS		8

#define LEVEL           0
#define EDGE            1
#define H_ACTIVE        0
#define L_ACTIVE        1

//=================================================================
// Macro Fucntions
//=================================================================
#define ENABLE_IRQ(n)		VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) |= ((unsigned long)1<<(n))
#define ENABLE_FIQ(n)		VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) |= ((unsigned long)1<<(n))

#define DISABLE_IRQ(n)		VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) &= (~((unsigned long)1<<(n)))
#define DISABLE_FIQ(n)		VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) &= (~((unsigned long)1<<(n)))

//=================================================================
// S2E Interrupt Sources
//=================================================================

#define FIQ_TIMER			0
#define IRQ_SPI				0
#define IRQ_GPIO			1
#define IRQ_I2C				2
#define IRQ_GENERAL_UART	3
#define IRQ_MAC				4
#define IRQ_WDT				5
#define IRQ_MOXA_UART		6

//=======================================================================
//	Functions:
//=======================================================================
void enable_interrupts(void);
int disable_interrupts(void);
void	setvect(volatile unsigned long vector, void (*handler)(void));
void	(*getvect(volatile unsigned long vector))(void);

void	Ssys_isrInit(void);
void	Ssys_isrUndefHandler(volatile unsigned long *address);
void	Ssys_isrPrefetchHandler(volatile unsigned long *address);
void	Ssys_isrAbortHandler(volatile unsigned long *address);
void	Ssys_isrSwiHandler(volatile unsigned long number);
void	Ssys_isrIrqHandler(void);
void	Ssys_isrFiqHandler(void);
#endif /* _ISR_H */
