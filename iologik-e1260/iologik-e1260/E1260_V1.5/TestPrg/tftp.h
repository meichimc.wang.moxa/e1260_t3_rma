/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	tftp.h

	Routines for implementing TFTP protocl

	2008-06-10	Albert Yu
		new release
	2008-10-15	Chin-Fu Yang		
		modified		
*/

#ifndef _TFTP_H
#define _TFTP_H


/*------------------------------------------------------ Macro / Define -----------------------------*/
/*
 * Trivial File Transfer Protocol (IEN-133)
 * Now, rev 2 - RFC 1350
 */
#define	SEGSIZE		512		/* data segment size */

/*
 * 5 Packet types.
 */
#define	RRQ					01			/* read request */
#define	WRQ				02			/* write request */
#define	DATA				03			/* data packet */
#define	ACK					04			/* acknowledgement */
#define	ERROR_TFTP			05			/* error code */



#define	th_block	th_u.tu_block
#define	th_code		th_u.tu_code
#define	th_stuff		th_u.tu_stuff
#define	th_msg		th_data

/*
 * Error codes.
 */
#define	EUNDEF		0		/* not defined */
#define	ENOTFOUND	1		/* file not found */
#define	EACCESS		2		/* access violation */
#define	ENOSPACE	3		/* disk full or allocation exceeded */
#define	EBADOP		4		/* illegal TFTP operation */
#define	EBADID		5		/* unknown transfer ID */
#define	EEXISTS		6		/* file already exists */
#define	ENOUSER		7		/* no such user */

/*
 * File transfer modes
 */
#define TFTP_NETASCII   0              // Text files
#define TFTP_OCTET      1              // Binary files

/*
 * Errors
 */
#define	TFTP_ENOTFOUND   1   /* file not found */
#define	TFTP_EACCESS     2   /* access violation */
#define	TFTP_ENOSPACE    3   /* disk full or allocation exceeded */
#define	TFTP_EBADOP      4   /* illegal TFTP operation */
#define	TFTP_EBADID      5   /* unknown transfer ID */
#define	TFTP_EEXISTS     6   /* file already exists */
#define	TFTP_ENOUSER     7   /* no such user */
#define TFTP_TIMEOUT     8   /* operation timed out */
#define TFTP_NETERR      9   /* some sort of network error */
#define TFTP_INVALID    10   /* invalid parameter */
#define TFTP_PROTOCOL   11   /* protocol violation */
#define TFTP_TOOLARGE   12   /* file is larger than buffer */

#define TFTP_TIMEOUT_PERIOD 5
#define TFTP_TIMEOUT_MAX   15
#define TFTP_RETRIES_MAX    5

#define TFTP_PORT	0x45
#define GET_PORT	0x1E14	//7700;




/*------------------------------------------------------ Structure ----------------------------------*/
struct tftphdr {			// TFTP Header
	short th_opcode;					/* packet type : 2 bytes*/
	union {
		unsigned short tu_block;		/* block # : 2 bytes*/
		short tu_code;					/* error code */
		char	tu_stuff[1];				/* request packet stuff */
	} __attribute__ ((packed)) th_u;
	char	th_data[0];					/* data or error string : n bytes */
} __attribute__ ((packed));


typedef struct _DSNET_info{
	ulong server_ip;
	ulong local_ip;
	uchar port;
	uchar reserved[3];
}DSNET_info, *DSNET_info_t;


typedef struct _file_transfer{
	DSNET_info	DsnetInfo;
	ulong		file_length;
	uchar		file_name[16];
}file_transfer , *file_transfer_t;



/*------------------------------------------------------ Extern / Function Declaration -----------------*/
int tftp_start(void);
int tftp_stream_rrq(char * name);
int tftp_stream_ack(ulong d_ip,ushort udp_sport, int block);
char *tftp_error(int err);
int tftp_stream_read(ulong d_ip, ushort udp_sport, char *buf,int len);
int tftp_protocol_recv(char * name);
int tftp_close(void);
int tftp_set_ip(ulong local, ulong server);


#endif
