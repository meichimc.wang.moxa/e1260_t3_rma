/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	Calibration.c

	2009-08-03	Nolan Yu
		new release
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "timer.h"
#include "memory.h"
#include "tftp.h"
#include "global.h"
#include "Calibration.h"
#include "moxauart.h"
#include "global.h"

volatile static UINT8 calibrator; //if   TRUE = fluke
									  //else FALSE = CA150

extern void Printf(const char *fmt, ...);
extern int diag_get_value(char *title, UINT32 *val, UINT32 min, UINT32 max, int mode);
extern int StrLength(const char *ptr);

/*	CA150 communication initial 
*	baudrate = 9600
*	Data length = 8
*	Stop bit = 2 (N 8 2)
*/

/*	Fluke communication initial
*	baudrate = 9600
*	Data length = 8
*	Stop bit = 1 (N 8 1)
*/
void Calibrator_communication_init(void){
//	UINT8 mode = RS485_2WIRE_MODE;
	UINT8 mode = RS232_MODE;

//	calibrator = calibrator_ca150;

	m_sio_init();
	m_sio_open(cal_port,0);
	if(calibrator == calibrator_fluke)
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_9600 , 0, 0x3); //baudrate = 9600
//		m_sio_ioctl(p, baudrate, 0x3);   
	else
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_9600 , 0, DATA_BITS_8|UART_LCR_STOP);
//		m_sio_ioctl(p, baudrate, DATA_BITS_8|UART_LCR_STOP);   
	m_sio_lctrl(cal_port,DTR_ON|RTS_ON);
	m_sio_setopmode(cal_port,mode);

	sleep(500);

//	calibrator_reset();
//	Calibrator_out();
	
	sleep(20);
}

void calibrator_reset(void){
	switch(calibrator){
	case calibrator_fluke:
		Fluke_5500A_Reset();	/*Printf("5500A reset \r\n");*/break;
	case calibrator_ca150:
		CA150_init();	/*Printf("CA150 reset \r\n");*/break;
	default:	break;
	}
}

void Set_Calibrator(UINT8 parameter){
	switch(parameter){
	case calibrator_fluke:
		calibrator = calibrator_fluke;	break;
	case calibrator_ca150:
		calibrator = calibrator_ca150;	break;
	default:
		break;
	}
}

void calibrator_select(void){
	UINT32 calibrator;
	if(diag_get_value("calibrator : (0) CA150 (1) Fluke : ",&calibrator,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	Set_Calibrator((UINT8)calibrator);
	Calibrator_communication_init();
}

void Calibrator_Set_Output_value(UINT8 *parameter){
	switch(calibrator){
	case calibrator_fluke:
#if E1262
		Fluke_5500A_Set_mV_value(parameter);	break;
#endif
#if E1260
		Fluke_5500A_Set_R_value(parameter);	break;
#endif
	case calibrator_ca150:
		CA150_Set_output_value(parameter);	break;
	default:	break;
	}
}

void Calibrator_Set_State(UINT8 parameter){
	switch(calibrator){
	case calibrator_fluke:
/*		Fluke_5500A_Set_mV_value(parameter);	*/break;
	case calibrator_ca150:
		CA150_Set_Source_func(parameter);	break;
	default:	break;
	}
}

void Calibrator_out(void){
	switch(calibrator){
	case calibrator_fluke:
		Fluke_5500A_Output_value();	break;
	case calibrator_ca150:
		CA150_Set_Source_State_func(TURN_ON);	break;
	default:
		break;
	}
}

void Calibrator_communication_stop(void){
	m_sio_close(cal_port);	
}

/*CA150 command*/

void transmit_CA150(UINT8* transmit_data,UINT8 length, UINT8* receive_data){
	int len,i,j;
	unsigned int t;	
	UINT8 ch,tr;
	tr=1;
	j=0;

	t = Gsys_msec;
	while(1) {
		if(Gsys_msec - t > 2000){
//			Memcpy(receive_data,ESCSTRING,10);
			return ;//break;
		}
		if ((len = m_sio_iqueue(cal_port)) > 0) {
			for (i=0;i<len;i++){
				m_sio_read(cal_port,&ch,1);
				receive_data[j]=ch;
				if(ch == 0x0a && receive_data[(j-1)] == 0x0d)
					return;
				j++;
			}			
		}
		if(tr){
			m_sio_write(cal_port, transmit_data, length);
			while(m_sio_oqueue(cal_port)!=0);
			tr=0;
		}
		sleep(1);
	}
}

/*Recall CA150 default setting*/
void CA150_init(void){
	UINT8 command[4] = {'R','C',0x0D,0x0A};

	Printf("\r\nReset CA150 Calibrator.........");

	m_sio_write(cal_port, command, 4);
	while(m_sio_oqueue(cal_port)!=0);
}

/*CA150 Source functions*/

UINT8 CA150_Set_Source_func(UINT8 parameter){
	UINT8 command[5] = {'S','F',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case DCV:
	case DCA:
	case TC:
	case RTD:
	case PULSE:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the source function is ....");
		switch(data[2]){
		case DCV:	Printf("DCV\r\n");	break;
		case DCA:	Printf("DCA\r\n");	break;
		case TC:	Printf("TC\r\n");	break;
		case RTD:	Printf("RTD\r\n");	break;
		case PULSE:	Printf("PULSE\r\n");	break;
		default:	break;
		}
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_Source_State_func(UINT8 parameter){
	UINT8 command[5] = {'S','O',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case TURN_OFF:
	case TURN_ON:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the source value is ....");
		switch(data[2]){
		case TURN_OFF:	Printf("OFF\r\n");	data[2] = 0x30;break;
		case TURN_ON:	Printf("ON\r\n");	data[2] = 0x31;break;
		default:	data[2] = 0;break;
		}
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_Source_Range_func(UINT8 parameter){
	UINT8 command[5] = {'S','R',0x0,0x0D,0x0A};
	UINT8 data[32];

	command[2] = parameter;

	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the source range is .... %s",&data[2]);
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_TC_RTD_daiplay_func(UINT8 parameter){
	UINT8 command[5] = {'T','E',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case TEMPERATURE_VALUE:
	case VOLTAGE_VALUE:
	case ROOM_TERPERATURE:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the TC display type is ....");
		switch(data[2]){
		case TEMPERATURE_VALUE:	Printf("terperature\r\n");	break;
		case VOLTAGE_VALUE:		Printf("voltage\r\n");	break;
		case ROOM_TERPERATURE:	Printf("Room terperature\r\n");	break;
		default:	break;
		}
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_current_type_func(UINT8 parameter){
	UINT8 command[5] = {'A','S',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case SOURCE:
	case SINK:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the current type is ....");
		switch(data[2]){
		case SOURCE:	Printf("Source\r\n");	break;
		case SINK:	Printf("Sink\r\n");	break;
		default:	break;
		}
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_divide_output_value(UINT8 dividend,UINT8 divisor){
	UINT8 command[8] = {'N','D',0x0,0x0,0x0,0x0,0x0D,0x0A};
	UINT8 data[32];

	if(dividend == QUERY){
		command[2] = '?';
		command[3] = 0x0D;
		command[4] = 0x0A;
	}
	else if(dividend < 10){
		command[2] = 0x30;
		command[3] = 0x30 + dividend;
	}else if(dividend < 20){
		command[2] = 0x31;
		command[3] = 0x26 + dividend;
	}else{
		Printf(command);
		return CA150_command_fail;
	}

	if(divisor < 10){
		command[4] = 0x30;
		command[5] = 0x30 + divisor;
	}else if(divisor < 20){
		command[4] = 0x31;
		command[5] = 0x26 + divisor; // 0x30+(A-10)
	}else{
		Printf(command);
		return CA150_command_fail;
	}

	if(command[2] == QUERY)
		transmit_CA150(command,5,data);
	else
		transmit_CA150(command,8,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the divided value is .... %c%c/%c%c",data[2],data[3],data[4],data[5]);
		return CA150_tran_OK;
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_increase_output_value(UINT8 parameter){
	UINT8 command[5] = {'U','P',0x0,0x0D,0x0A};
	UINT8 data[32];

	if(parameter > 0 && parameter < 6){
		command[2] = 0x30 + parameter;
	}else{
		Printf(command);
		return CA150_command_fail;
	}

	transmit_CA150(command,5,data);
	if(data[2] != ',' && data[3] !='O' && data[4] !='K'){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

UINT8 CA150_Set_divide_output(UINT8 parameter){
	UINT8 command[5] = {'N','M',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case DIVIDE_DISABLE:
	case DIVIDE_ENABLE:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the divided output function is ....");
		switch(data[2]){
		case DIVIDE_DISABLE:	Printf("Disable\r\n");	break;
		case DIVIDE_ENABLE:	Printf("Enable\r\n");	break;
		default:	break;
		}
		return CA150_tran_OK;
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
} 

UINT8 CA150_Set_output_value(UINT8* parameter){
	UINT8 command[32] = {'S','D'};
	UINT8 data[32];
	int parameter_length=0;

	parameter_length = StrLength(parameter);
//	Printf("data length = %d",parameter_length);
	Memcpy(&command[2],parameter,parameter_length);
	command[2+parameter_length]=0x0D;
	command[3+parameter_length]=0x0A;
/*	Printf("\r\n");
	Printf(command);
	Printf("\r\n");
*/	transmit_CA150(command,parameter_length+4,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the output value is .... %s",&data[2]);
		return CA150_tran_OK;
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
} 
/*CA150 measure function*/

UINT8 CA150_Set_meausre_State_func(UINT8 parameter){
	UINT8 command[5] = {'M','O',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case TURN_OFF:
	case TURN_ON:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		switch(data[2]){
		case TURN_OFF:	Printf("TURN OFF");	break;
		case TURN_ON:	Printf("TURN ON");	break;
		default:	break;
		}
		Printf(" the meausrement function .....\r\n");
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

/*CA150 other function*/
void CA150_setting_information(void){
	UINT8 command[4] = {'O','S',0x0D,0x0A};
	UINT8 data[100];

	transmit_CA150(command,4,data);
	Printf("\r\n");
	Printf(data);
}

UINT8 CA150_Backlight(UINT8 parameter){
	UINT8 command[5] = {'B','L',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch(parameter){
	case Back_Light_OFF:
	case Back_Light_ON:
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		if(data[2] == Back_Light_OFF)
			Printf("Back light OFF\r\n");
		else
			Printf("Back light ON\r\n");
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}

/*
*	Fluke 5500A command
*/

void Fluke_5500A_Reset(void)
{
	char oper[5]="*RST";
	
	oper[4]=13;
	m_sio_write(cal_port, oper, 5);
	sleep(500);
/*
	int i;
	char command[20]="*RST";
	Printf("\r\nReset Fluke 5500A Calibrator.........");
	command[4]=0x0D;
//	m_sio_write(2,command,5);
//	while(m_sio_oqueue(2)!=0);

	for (i=0;i<5;i++)
	{
		m_sio_write(2,(char*)(command+i),1);
		while(m_sio_oqueue(2)!=0);
		sleep(100);
	}
*/
}
#if E1262
void Fluke_5500A_Set_mV_value(UINT8* value)
{
	char Value[32]="OUT ";
	int len;
	
	len = StrLength(value);
	Memcpy(&Value[4],value,len);
	Value[4+len]='m';
	Value[5+len]='V';
	Value[6+len]=0x0D;

	len=StrLength(Value);

	m_sio_write(cal_port, Value, len);
}

#endif
#if E1260
void Fluke_5500A_Set_R_value(UINT8* value)
{
	char Value[32]="OUT ";
	int len;
	
	len = StrLength(value);
	Memcpy(&Value[4],value,len);
	Value[4+len]='O';
	Value[5+len]='H';
	Value[6+len]='M';
	Value[7+len]=0x0D;

	len=StrLength(Value);

	m_sio_write(cal_port, Value, len);
}
#endif

void Fluke_5500A_Output_value(void){
	char oper[5]="OPER";

	oper[4]=13;
	m_sio_write(cal_port, oper, 5);
	sleep(500);
}


/*
UINT8 Fluke_5500A_Set_Source_func(UINT8 parameter){
	UINT8 command[5] = {'S','F',0x0,0x0D,0x0A};
	UINT8 data[32];

	switch	(parameter){
	case DCV:
	case DCA:
	case TC:
	case RTD:
	case PULSE:
	case OUT:		// Jerry add on 02/12,10' for E1260 MP_Cal
	case QUERY:
		command[2] = parameter;	break;
	default:
		Printf(command);
		return CA150_command_fail;
		break;		
	}
	transmit_CA150(command,5,data);
	if(command[2] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the source function is ....");
		switch(data[2]){
		case DCV:	Printf("DCV\r\n");	break;
		case DCA:	Printf("DCA\r\n");	break;
		case TC:	Printf("TC\r\n");	break;
		case RTD:	Printf("RTD\r\n");	break;
		case PULSE:	Printf("PULSE\r\n");	break;
		case OUT:	Printf("OUT Value\r\n");	break;		// Jerry add on 02/12,10' for E1260 MP_Cal
		default:	break;
		}
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return Fluke_5500A_tran_Fail;
	}
	return Fluke_5500A_tran_OK;
}
*/
/*
UINT8 Fluke_5500A_Set_Source_Range_func(UINT8 parameter){
	UINT8 command[8] = {'S','R',0x4F,0x55,0x54,0x0,0x0D,0x0A};
	UINT8 data[32];

	command[5] = parameter;

	transmit_CA150(command,5,data);
	if(command[5] == QUERY && command[0] == data[0] && command[1] == data[1]){
		Printf("Setting the source range is .... %s",&data[2]);
		return data[2];
	}
	else if(command[2] != data[2]){
		Printf("\r\n");
		Printf(data);
		return CA150_tran_Fail;
	}
	return CA150_tran_OK;
}
*/