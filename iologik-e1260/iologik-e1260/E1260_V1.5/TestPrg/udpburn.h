/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	udpburn.h

	Routines for implementing UDP burnin test

	2008-06-10	Albert Yu
		new release
	2008-10-17	Chin-Fu Yang		
		modified
*/

#ifndef _UDPBURN_H
#define _UDPBURN_H
/* * CN2500 burn test program Console UDP protocol.
 *
 * AP (95/98/NT/UNIX) via socket		       CN2500 burn program
 *												(When initialized	   )
 *												(ser = 0, STATUS_IDLE  )
 *							Can broad cast
 *	CN25_POLL				---------------->
 *	(serial don't care)     <----------------   ack (ser = 0,STATUS_IDLE)
 *						    Only ack when CN25
 *							status = STATUS_IDLE
 *							and serial = 0
 *						    Otherwise
 *							no response
 *
 *							Non braod cast
 *	CN25_QUERY				---------------->
 *							<----------------   ack (ser++)
 *												nak (no change)
 *
 *							Non broad cast
 *	CN25_SET_PARA			---------------->
 *							<----------------   ack (ser++,STATUS_IDLE)
 *												nak (no change)
 *												CN25 will check AP's ser
 *												nak when :
 *												1. invalid para
 *												2. CN25 is not on
 *													STATUS_IDLE
 *
 *							Non broad cast
 *	CN25_START				---------------->
 *							<----------------   ack (ser++,STATUS_BURN)
 *												nak (no change)
 *													CN25 will check AP's ser
 *													nak when :
 *													1. CN25 is not on
 *														STATUS_IDLE
 *													2. unknown CN25 ID
 *							Non broad cast
 *	CN25_STOP				---------------->
 *							<----------------   ack (ser++,STATUS_OK or)
 *													STATUS_FAIL )
 *												nak (no change)
 *													CN25 will check AP's ser
 *													nak when :
 *													1. CN25 is not on
 *														STATUS_BURN
 *													2. Burning no error and
 *														burning time < 1 hour
 *
 *							Non broad cast
 *	CN25_DOWNLOAD			---------------->
 *							<----------------   ack (ser++,STATUS_DOWNLOAD)
 *												nak (no change)
 *													CN25 will check AP's ser
 *													nak when :
 *													1. CN25 is not on
 *														STATUS_OK
 *													2. Invalid download ser no
 *														Non broad cast
 *	CN25_DOWN_RESET 		---------------->
 *							<----------------   ack (ser++, STATUS_OK)
 *												nak (no change)
 *													CN25 will check AP's ser
 *												nak when :
 *													1. CN25 is not on
 *														STATUS_OK	|
 *														STATUS_DOWNLOAD |
 *														STATUS_DOWN_FAIL
 *												note : When ack status is
 *													set to STATUS_OK
 *							Non broad cast
 *	CN25_DOWN_CHK			---------------->
 *							<----------------   ack (ser++,STATUS_DOWN_OK)
 *													( or STATUS_DOWN_FAIL)
 *												nak (no change)
 *													CN25 will check AP's ser
 *													nak when :
 *														1. CN25 is not on
 *															STATUS_DOWNLOAD
 *
 *							Non broad cast
 *	CN25_SAVE				---------------->
 *							<----------------   ack (ser++,STATUS_FINISH)
 *												nak (no change)
 *													CN25 will check AP's ser
 *												nak when :
 *													1. CN25 is not on
 *														STATUS_DOWN_OK
 *													2. from write fail
 *												Note :
 *													when nak CN25 is status
 *													is set to STATUS_OK
 *													Can broad cast
 *	CN25_RESET				---------------->
 *	(serial is don't care)  <----------------   ack (ser=0,STATUS_IDLE)
 *												if broad cast :
 *													ack when < STATUS_OK
 *												else
 *													ack when < STATUS_DOWN_OK
 *
 *							Alwayas broad cast
 *							<--------------------	alive (no change)
 *													When CN25 doesn't receive
 *													any command from AP in 1
 *													second. CN25 will send
 *													this message every second
 *													until it receives any
 *													valid command from AP
 *
 *													Command set (CN25ap.command) :
 */









/*------------------------------------------------------ Macro / Define -----------------------------*/
#define CN25_CMD_MASK	0x3FFF	/* Must be mask to decide what command	    */
#define CN25_POLL		0x0001	/* Polling, can use on broad cast			*/
								/* CN25 will ack this command only when 	*/
								/* 1. status = STATUS_IDLE					*/
								/* 2. command ser = 0						*/
								/*											*/
#define CN25_QUERY		0x0002	/* Query CN25 status, can't use on broad cast*/
#define CN25_SET_PARA	0x0003	/* Set para, can't use on broad cast        */
#define CN25_START		0x0004	/* Start burn, can't use on broad cast      */
#define CN25_STOP		0x0005	/* Stop burn, can't use on broad cast       */
#define CN25_DOWNLOAD	0x0006	/* Download, can't use on broad cast        */
#define CN25_DOWN_RESET 0x0007	/* Re-initialize download					*/
#define CN25_DOWN_CHK	0x0008	/* validate firmware format					*/
#define CN25_SAVE		0x0009	/* Save from, can't use on broad cast       */
#define CN25_RESET		0x3FFF	/* Reset CN25 status to beginning mode	    */
								/* Can use on broad cast mode				*/
								/* If it is not sent by broad cast			*/
								/* This command is useless when CN25 is 	*/
								/* >= STATUS_DOWN_OK						*/
								/* If it is sent by broad cast				*/
								/* This command is useless when CN25 is 	*/
								/* >= STATUS_OK 							*/

#define CN25_PROTOCOL_MASK 0xC000 /* What				    */
#define CN25_DIAG	0xC000	/* cn25 diagnose			    */
#define CN25_ACK	0x8000	/* CN25 ACK bit 			    */
#define CN25_NAK	0x4000	/* CN25 NAK bit 			    */
#define CN25_ALIVE	CN25_ACK/* CN25 alive message			*/
								/* When CN25 doesn't receive any command    */
								/*  >= 3 second. It'll broad cast an alive  */
								/*  message with CN25ack.resp = CN25_ALIVE  */
								/*  every second until it receives any	    */
								/*  valid command from AP					*/


/*
 * Notes for Command set :
 * 1.	If command can use broad cast mode (CN25_RESET,CN25_POLL)
 *	    CN25 won't check command ser no. And will set ser to match AP.
 *	If command can't use broad case mode (CN25_SET_PARA,CN25_START ..)
 *	    CN25 will check command ser no. If not match CN25 will nak the
 *	    command code with REASON_SER
 *
 * 2.	When download.
 *	    The serial (CN25_download.serial) must be initialize to 0.
 *	    And increase by 1 on next packet.
 *
 *
 * Status set (CN25ack.status) :
 */
#define STATUS_IDLE			0x0000 /* CN25 is idle							*/
#define STATUS_BURN			0x0001 /* CN25 is under burning 				*/
#define STATUS_FAIL			0x0002 /* CN25 burning complete but fail		*/
#define STATUS_OK			0x0010 /* CN25 burning complete and ok			*/
#define STATUS_DOWNLOAD 	0x0020 /* CN25 is under downloading				*/
#define STATUS_DOWN_FAIL	0x0030 /* CN25 firmware download fail			*/
#define STATUS_DOWN_OK		0x0100 /* CN25 firmware download ok				*/
#define STATUS_FINISH		0xf000 /* firmware download and save to flashrom*/
/*
 * Reason code (CN25ack.reason) :
 */
#define REASON_UNKNOW	0x00001 	/* unknown command						*/
#define REASON_INVALID	0x00002 	/* Invalid console udp packet			*/
#define REASON_SER		0x00003 	/* Invalid command ser no				*/
#define REASON_PARA		0x00004 	/* Invalid parameters					*/
#define REASON_STATUS	0x00005 	/* Invalid command under currnt status	*/
#define REASON_BURN		0x00006 	/* burning fail 						*/
#define REASON_TIME		0x00007 	/* burning time <= 1 hour				*/
#define REASON_DOWN_SER 0x00008 	/* invalid download ser no				*/
#define REASON_FIRMWARE 0x00009 	/* Invalid firmware format				*/
#define REASON_SAVE		0x0000a 	/* save from fail						*/
#define REASON_ID		0x0000b 	/* unknown CN2500 ID					*/
#define REASON_OPEN		0x0000c 	/* sio_open fail						*/
#define REASON_LENGTH	0x0000d 	/* download length exceed				*/
#define REASON_PRIVATE	0x0000f		/* Private key set */

#define TEST_LEN    256

#define FIRM_MAX_DOWNLOAD_LEN	0x1B0000/* maximum firmware = 1728K bytes*/
#define BIOS_MAX_DOWNLOAD_LEN	0x40000	/* maximum firmware = 256K bytes*/
#define DOWN_MAX_LEN FIRM_MAX_DOWNLOAD_LEN 

#define HEADISDLEN  sizeof(struct head_isd)
#define VERSION		0x30303030	    /* 0000   */
#define BIOS_MAGIC404	0x33336464	    /* dd33   */
#define NPORT_MAGIC404	0x50384732		/* 2G8P	  */	 
#define ASYNC_MAGIC404	0x30313632		/* 2610	  */

#define HEADWEBLEN  sizeof(struct head_web)

#define DIAG_TIMEOUT	    100 	/* send diag packet every 100 ms    */
#define ALIVE_TIMEOUT	    10000	/* send alive timeout = 10 seconds  */
#define POLL_TIMEOUT	    1000	/* 1 second for polling routine     */
#define MIN_BURN_TIME	    3600	/* min burn time = 1 hour	    */





/*------------------------------------------------------ Structure ----------------------------------*/
typedef struct head_isd {
	ulong magic;
	ulong version;
	ulong length;
	ulong prg_start;
	ulong sum;
	uchar reserve[12];
}HEAD_ISD;



typedef struct head_web {
	uchar  web_descr[32];        /* 00-1F	Name of this file system. */
	ulong  version;			 /* 20-23	Version of this file system. Currently, 01000000H. : 1.0.0 */
	ulong  creat_time; 		 /* 24-27	Create Time. seconds from 1970-01-01. */
	ulong  sum_filedisk;		 /* 28-2B	Checksum of File Disk Directory. */
	ulong  sum_filedata;		/*  2C-2F	Checksum of available File Data. */
	ulong  size_header;		/*  30-33	Size of File Disk Header. Currently, 00000100H (256 bytes) */
	ulong  size_directory; 	 /* 34-37	Size of File Disk Directory. (= '38-39' * '3A-3B') */
	ushort size_entry;		/*  38-39	Size of each dirctory entry. Currently, 0040H (64 bytes) */
	ushort directory_entry;    /* 3A-3B	Available directory entries. */
	ulong  size_filedata;         /* 3C-3F	Available File Data size. */
	uchar	reserve[192];
}HEAD_WEB;



struct cn25_ap {	    /* filled by ap, total 128 bytes	*/
	ushort	cmd;	    /* command								*/
	ushort	ser;	    /* command ser no						*/
	ushort	data_len;   /* used download						*/
	ulong	baud;	    /* baud rate to be used on burning		*/
	uchar	tm_sec;		/* GMT time	*/
	uchar	tm_min;
	uchar	tm_hour;
	uchar	tm_mday;
	uchar	tm_mon;
	uchar	tm_year;
	uchar	tm_wday;
	uchar	tm_yday;
	uchar	tm_isdst;
	ushort  monitor_error;/* AP monitor error status */ 
	uchar	reserve[107];/* reserved for future use 		    */
}__attribute__((packed));
typedef struct cn25_ap cn25ap;
typedef struct cn25_ap *cn25ap_t;


struct cn25_download {
	ushort	ser;		/* serial no of download			    */
	uchar	data[1];	/* max len = 1328						*/
}__attribute__((packed));
						/* 1500 - 14 - 20 - 8 - 128 -2 = 1328   */
						/* MAC header + protocol = 14		    */
						/* IP header		 = 20				*/
						/* UDP header		 =  8				*/
						/* cn25ap		 = 128					*/
						/* serial		 =  2					*/
typedef struct cn25_download cn25download;
typedef struct cn25_download *cn25download_t;


/*
 * AP send to CN25 packet structure definition
 */
struct cn25_cmd {
	cn25ap	req;
	uchar	data[1];
}__attribute__((packed));
typedef struct cn25_cmd cn25cmd;
typedef struct cn25_cmd *cn25cmd_t;


/*
 * CN25 send to AP packet structure definition
 */
struct cn25_ack {		/* filled by console, total 128 bytes				*/
	UINT16	resp;				/* command | 0x8000 = ack, command | 0x4000 = nak, 0x0c00 -> diagnostic */
	UINT16	ser;				/* on ACK --> next ser CN25 except to receive	    */
								/* on NAK --> currnet ser CN25 except to receive    */
	UINT32	serial; 			/* CN2500 serial no									*/
	UINT32	ip;					/* CN2500 IP address								*/
	UINT8	id;					/* CN2500 id,0x01=CN2516,0x03=CN2508,0x07=CN2504    */
	UINT16	status; 			/* CN2500 burning status							*/
	UINT16	reason; 			/* reason code if nak								*/
	UINT32	time;				/* burning time in seconds							*/
	UINT32	baud;				/* baud rate to be used for burning					*/
	UINT32	tot_cnt;			/* total test count									*/
	UINT32	con_err;			/* console error count								*/
	UINT32	p_err[16];			/* uart port 0 - 15 error count 					*/
	UINT32	dram_err;			/* dram test error count							*/
	UINT32	nic_tx; 			/* ethernet test error count							*/
	UINT32	nic_rx; 			/* ethernet test error count							*/
	UINT8	tm_sec;				/* RTC time							*/
	UINT8	tm_min;				/* RTC time							*/
	UINT8	tm_hour;			/* RTC time							*/	
	UINT8	tm_mday;			/* RTC time							*/
	UINT8	tm_mon;				/* RTC time							*/
	UINT8	tm_year;			/* RTC time							*/
	UINT32	dlan_err;			/* Dual LAN extern tx count */
	UINT8	reserve[9];	/* reserved for future use							*/
}__attribute__((packed));
typedef struct cn25_ack cn25ack;
typedef struct cn25_ack *cn25ack_t;





/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void ConUdpInit(int ports,ulong baud);
int ConUdpPoll(void);
void ConUdpReceive(ulong s_ip,ulong d_ip,uchar *buf,ulong len);
void ConUdpBurn(void);

void diag_clear_EEPROM_BR(void);		// for Burn-in finish to erase EEPROM to default

int IsValidFormat(ulong *buf);
int IsValidWebFormat(uchar *buf);
int fwcheck(ulong *offset, ulong *length);

#endif
