/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	watch_dog.c

	Functions of watch dog.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified		
    
*/
#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "lib.h"
#include "global.h"
#include "timer.h"
#include "watch_dog.h"
#include "console.h"


/**	\brief
 *
 *	WatchDog ISR. This ISR just gets the WDT status and print it.
 *
 */
static void WatchDog_Isr(void)
{
	UINT32 reg;
	reg = Inw(S2E_WDT_BASE + WDT_STAT);
	
	PrintStr("WatchDog Interrupt Status = 0x");
	PrintHex(reg);
	PrintStr("\r");
	PrintStr("\n");	
	
	PrintStr("Booting.......................................................");
	PrintStr("\r");
	PrintStr("\n");
}


/**	\brief
 *
 *	Insert the watch dog ISR to interrupt vector
 *
 */
void WatchDog_SetIsr(void)
{
	isr_set_isr(IRQ_WDT, WatchDog_Isr);		// setting ISR
}


/**	\brief
 *
 *	Enable WDT IRQ , so that WDT can generate interrupt.
 *
 */
void WatchDog_EnableIrq(void)
{
	ENABLE_IRQ(IRQ_WDT);
}




/**	\brief
 *
 *	Enable Watch Dog Timer . 
 *	By default , the WDT clock is enable.
 *
 */
void WatchDog_Enable(void)
{
	UINT32 tmp;

	tmp = Inw(S2E_WDT_BASE + WDT_CONTROL);	
    	tmp |= WDT_ENABLE;
    	Outw(S2E_WDT_BASE + WDT_CONTROL, tmp);		
}



/**	\brief
 *
 *	Sets the WDT operation mode of reset into mode register.
 *
 *	\param[in]	reset_mode : immediate / interrupt
 *
 */
void WatchDog_SetMode(watchdog_reset_mode reset_mode)			// reset or interrupt-reset
{
	UINT32 reg;
	
	reg = Inw(S2E_WDT_BASE + WDT_CONTROL);	
	
	if(reset_mode == watchdog_reset_mode_immediate){
		reg &= (~WDT_RESPONSE_MODE);
	}else if(reset_mode == watchdog_reset_mode_interrupt){
		reg |= WDT_RESPONSE_MODE;
	}else{
		Printf("Invalid argument!!\r\n");
	}
	
    	Outw(S2E_WDT_BASE + WDT_CONTROL, reg);

}




/**	\brief
 *
 *	Enable Watch Dog Clock . 
 *	By default , the WDT clock is enable.
 *
 */
void WatchDog_Enable_Clock(void)
{
	UINT32 tmp;

	// Enable WDT clock
	tmp = Inw(CLOCK_CONTROL_BASE);	
	tmp &= (~WDT_PAUSE);				// 0 is enable , 1 is Pause
	Outw(CLOCK_CONTROL_BASE, tmp);
}


/**	\brief
 *
 *	Control CLOCK_CONTROL_BASE to disable WDT
 *
 */
void WatchDog_Disable(void)
{
	UINT32 tmp;

	tmp = Inw(CLOCK_CONTROL_BASE);	
    	tmp |= WDT_PAUSE; 				// 1 is disable
    	Outw(CLOCK_CONTROL_BASE, tmp);
}




/**	\brief
 *
 *	Kick The Dog!!
 *
 */
void WatchDog_ReStart(void)
{
	UINT32 tmp;
	tmp = Inw(S2E_WDT_BASE + WDT_CRR);
	tmp &= 0xFFFFFF00	; 		// clear 8bits
	tmp |= WDT_AUTO_RELOAD;
	Outw(S2E_WDT_BASE + WDT_CRR , tmp);  
}




/**	\brief
 *
 *	Config the WDT Timeout Range Register.
 *	S2E needs to config TOP and TOP_INIT.
 *
 *	\param[in]	power : 2^power , the range of power is from 0 to 15
 *
 */
void WatchDog_SetTopInit(UINT32 power)
{
	UINT32 tmp;
	power &= 0x0000000F;	// we only need 4 bits;
	tmp = Inw(S2E_WDT_BASE + WDT_TORR);
	tmp &= 0xFFFFFF0F ;
	tmp |= ( power << 4);
	Outw(S2E_WDT_BASE + WDT_TORR,tmp);			
}




/**	\brief
 *
 *	Config the WDT Timeout Range Register
 *	S2E needs to config TOP and TOP_INIT
 *
 *	\param[in]	power : 2^power , the range of power is from 0 to 15
 *
 */
void WatchDog_SetTop(UINT32 power)
{
	UINT32 tmp;
	power &= 0x0000000F;	// we only need 4 bits;
	tmp = Inw(S2E_WDT_BASE + WDT_TORR);
	tmp &= 0xFFFFFFF0 ;
	tmp |= power ;
	Outw(S2E_WDT_BASE + WDT_TORR,tmp);			
}




/**	\brief
 *
 *	Clear WDT EOI
 *
 */
void WatchDog_ClearStatus(void)
{
    	Inw(S2E_WDT_BASE + WDT_EOI);		// reading register to clear pending
}



/**	\brief
 *
 *	set reset pulse length , default = 16pclk
 *
 *	\param[in]	length : pulse length
 *
 */
void WatchDog_SetResetPulseLength(UINT32 length)
{
	UINT32 tmp;
	length &= 0x00000007;	// we only need 3 bits;
	tmp = Inw(S2E_WDT_BASE + WDT_CONTROL);
	tmp &= 0xFFFFFFE3 ;	// mask bit 2~4
	tmp |= (length << 2);
	Outw(S2E_WDT_BASE + WDT_CONTROL,tmp);	
}




/**	\brief
 *
 *	check if the INT pending
 *
 *	\retval 0		success
 *	\retval !0		Interrupt Pending
 */
int WatchDog_IsInterruptPending(void)
{
	UINT32 tmp;

	tmp = Inw(S2E_WDT_BASE + WDT_STAT);
	return tmp;
}



/**	\brief
 *
 *	Read Current Counting Value
 *
 *	\retval 		Current Counting Value
 */
UINT32 WatchDog_ReadCounter(void)
{
	return Inw(S2E_WDT_BASE + WDT_CCVR);
}

