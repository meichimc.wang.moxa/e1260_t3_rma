/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	exspi.h

	Routines for accessing SPI flash and GPIO expander

	2009-01-05	Nolan Yu
		new release
		
*/

#define GPIO_CHIP_SELECT	1

void EXSPI_INIT(void);
//void EXSPI_DIO_INIT(void);		// For DIO Shift register
void SPI_Delay_10us(void);
UINT32 SPI_ReadFIFO(void);
void SetSPI_Len(int length);
UINT8 SPI_BUSY(void);

#if GPIO_CHIP_SELECT
void SPI_Tx(UINT32 CS,UINT32 data,int length);
void SPI_SR_DIO_Rx(UINT32 CS,UINT32 data,int length);
void SPI_SR_DIO_Tx(UINT32 CS,UINT32 data,int length);
#else
void SPI_Tx(UINT32 data,int length);
#endif
