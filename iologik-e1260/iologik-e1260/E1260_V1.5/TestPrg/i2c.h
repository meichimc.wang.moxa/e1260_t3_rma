/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	i2c.h

	Embedded I2C.
	Function declaration.
		
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified
	2009-04-14 Nolan Yu
		modified
		
*/


#ifndef _I2C_H
#define _I2C_H


#define EEPROM_ADDRESS_2BYTES	1 // if 1 : ADDRESS 2 bytes, 0 : 1 bytes

// S2E EEPROM 
#define EEPROM_MAX_SIZE			64 * 1024 //512K bit =64K byte
#define Cal_EEPROM_MAX_SIZE		256

#define EEPROM_PAGE_SIZE		128			//Try the number OK, but datasheet is 128
#define Cal_EEPROM_PAGE_SIZE	16

//#define EEPROM_WRITE			0x50		// I2C must shift 1 bit to CMD register , so that 0xA0 become 0x50 , 0xA1 become 0x50
//#define EEPROM_READ				0x50
//#define PARMETER_EEPROM_WRITE	0x51		
#define EEPROM_WRITE			0x57		// I2C must shift 1 bit to CMD register , so that 0xA0 become 0x50 , 0xA1 become 0x50
#define EEPROM_READ				0x57
#define PARMETER_EEPROM_WRITE	0x50		

/*------------------------------------------------------ Macro / Define ----------------------------*/
#define TX_BUFFER_DEPTH		8
#define RX_BUFFER_DEPTH		8

// 一些 Define 的值 不知如何設定		// ns
#define SS_MIN_SCL_HIGH         4000
#define SS_MIN_SCL_LOW          4700
#define FS_MIN_SCL_HIGH         600
#define FS_MIN_SCL_LOW          1300
#define HS_MIN_SCL_HIGH_100PF   60
#define HS_MIN_SCL_LOW_100PF    160
#define HS_MIN_SCL_HIGH_400PF   320
#define HS_MIN_SCL_LOW_400PF    120

#define I2C_SLAVE_ADDRESS		0x55

//EEPROM read status
#define CURR_ADDR				0
#define RAND_ADDR				1

/*------------------------------------------------------ Structure ----------------------------------*/
typedef enum i2c_address_mode{
	i2c_7bit_address = 7 ,
	i2c_10bit_address = 10
}i2c_address_mode;


typedef enum i2c_speed_mode{
	i2c_speed_standard = 0x1,   // standard speed (100 kbps)
	i2c_speed_fast = 0x2,       // fast speed (400 kbps)
  	i2c_speed_high = 0x3        // high speed (3400 kbps)
}i2c_speed_mode;


typedef enum i2c_scl_phase{
	i2c_scl_low = 0x0,          // SCL clock count low phase
	i2c_scl_high = 0x1          // SCL clock count high phase	
}i2c_scl_phase;


typedef enum i2c_tx_mode{
	i2c_tx_target = 0x0,        // normal transfer using target address
   	i2c_tx_gen_call = 0x2,      // issue a general call
    	i2c_tx_start_byte = 0x3     // issue a start byte I2C command	
}i2c_tx_mode;



typedef enum i2c_irq{
	i2c_irq_none = 0x000,       // Specifies no interrupt
	i2c_irq_rx_under = 0x001,   // Set if the processor attempts to read
                                // the receive FIFO when it is empty.
	i2c_irq_rx_over = 0x002,    // Set if the receive FIFO was
                                // completely filled and more data
                                // arrived.  That data is lost.
	i2c_irq_rx_full = 0x004,    // Set when the transmit FIFO reaches or
                                // goes above the receive FIFO
                                // threshold. It is automatically
                                // cleared by hardware when the receive
                                // FIFO level goes below the threshold.
	i2c_irq_tx_over = 0x008,    // Set during transmit if the transmit
                                // FIFO is filled and the processor
                                // attempts to issue another I2C command
                                // (read request or write).
	i2c_irq_tx_empty = 0x010,   // Set when the transmit FIFO is at or
                                // below the transmit FIFO threshold
                                // level. It is automatically cleared by
                                // hardware when the transmit FIFO level
                                // goes above the threshold.
	i2c_irq_rd_req = 0x020,     // Set when the I2C is acting as a slave
                                // and another I2C master is attempting
                                // to read data from the slave.
	i2c_irq_tx_abrt = 0x040,    // In general, this is set when the I2C
                                // acting as a master is unable to
                                // complete a command that the processor
                                // has sent.
	i2c_irq_rx_done = 0x080,    // When the I2C is acting as a
                                // slave-transmitter, this is set if the
                                // master does not acknowledge a
                                // transmitted byte. This occurs on the
                                // last byte of the transmission,
                                // indicating that the transmission is
                                // done.
	i2c_irq_activity = 0x100,   // This is set whenever the I2C is busy
                                // (reading from or writing to the I2C
                                // bus).
	i2c_irq_stop_det = 0x200,   // Indicates whether a stop condition
                                // has occurred on the I2C bus.
	i2c_irq_start_det = 0x400,  // Indicates whether a start condition
                                // has occurred on the I2C bus.
	i2c_irq_gen_call = 0x800,   // Indicates that a general call request
                                // was received. The I2C stores the
                                // received data in the receive FIFO.
	i2c_irq_all = 0xfff         // Specifies all I2C interrupts.  This
                                // combined enumeration that can be
                                // used with some functions such as
                                // dw_i2c_clearIrq(), dw_i2c_maskIrq(),
                                // and so on.	
}i2c_irq;


typedef enum i2c_master_slave_type{
	i2c_master = 0,
	i2c_slave = 1
}i2c_master_slave_type;


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
int i2c_init(void);
void i2c_enable(void);
int i2c_disable(void);
BOOL i2c_is_busy(void);
BOOL i2c_is_tx_fifo_empty(void);
BOOL i2c_is_tx_fifo_full(void);
BOOL i2c_is_rx_fifo_empty(void);
BOOL i2c_is_rx_fifo_full(void);
int get_tx_fifo_level(void);
int get_rx_fifo_level(void);
void set_tx_threshold_level(UINT8 level);
int get_tx_threshold_level(void);
void set_rx_threshold_level(UINT8 level);
int get_rx_threshold_level(void);
void i2c_issue_read(void);
UINT8 i2c_read(void);
void i2c_write(UINT8 ch);
int i2c_set_master_address_mode(i2c_address_mode mode);
i2c_address_mode i2c_get_master_address_mode(void);
int i2c_set_slave_address_mode(i2c_address_mode mode);
i2c_address_mode i2c_get_slave_address_mode(void);
int i2c_set_slave_address(UINT16 slave_address);
int i2c_set_target_address(UINT16 target_address);	
int i2c_set_scl_count(i2c_speed_mode speed_mode , i2c_scl_phase phase , UINT16 count);
int i2c_clock_setup(UINT32 ic_clk);
BOOL i2c_is_enable(void);
int i2c_set_speed_mode(i2c_speed_mode speed_mode);
int i2c_enable_restart(void);
int i2c_enable_master(void);
int i2c_enable_slave(void);
int i2c_set_tx_mode(i2c_tx_mode tx_mode);
void i2c_mask_irq(i2c_irq interrupt);
void i2c_unmask_irq(i2c_irq interrupt);
void i2c_clear_irq(i2c_irq interrupt);
UINT16 i2c_get_inturrpt_status(void);
int i2c_auto_init( i2c_master_slave_type i2c_type, 
					 i2c_address_mode address_mode, 
					 i2c_speed_mode speed_mode,
					UINT32 slave_address);
int i2c_write_to_eeprom(UINT32 addr , UINT8 buf);
int i2c_read_from_eeprom(UINT32 addr , UINT8 *buf);
UINT8 eeprom_readb(UINT8 command,UINT16 addr);
int i2c_set_target_addressing_mode(i2c_address_mode addressing_mode);

int i2c_read_from_eeprom_page(UINT32 addr , UINT8 *buf,int length);
int eeprom_write_page(UINT16 addr , UINT8 *data, UINT16 dataSize);

#endif