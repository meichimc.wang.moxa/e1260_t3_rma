/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	udpburn.c

	Routines for implementing UDP burnin test

	2008-06-10	Albert Yu
		new release
	2008-10-17	Chin-Fu Yang		
		modified		
	2009-08-03	Nolan Yu		
		modified		
*/

#include "types.h"
#include "chipset.h"
#include "timer.h"
#include "moxauart.h"
#include "memory.h"
#include "mac.h"
#include "gpio.h"
#include "exrtc.h"
#include "global.h"
#include "udpburn.h"
#include "ip.h"
#include "console.h"
#include "i2c.h"
#include "ai.h"
#include "DIO.h"
#include	"spi_reg.h"

extern int FlashromProgram(UINT32 dest, UINT32 src, long len);
extern int	diag_do_set_clean_flag(int mode,long parameter);
extern void diag_do_SYSRESET_func(void);

extern int  ConsolePort;

static cn25ack Ack;
static int SendAlive = 1;		/* 1 = we need to send an alive     */

/* message to AP		    */
static ulong LastSendTime = 0;	/* Last time we send a packet to AP */
static ulong RemoteIp = 0xffffffff;
static ulong StartBurnTime,StopBurnTime;
int Ports = 1;
int start_port;
int end_port;
static ulong finish_time = 0;
static int BurnNoError = 1;
static ulong ErrorFlashTime = 200;	/* default = 200 msecs		    */
static FRM_HEADER *fd;
static ulong DownLen;

uchar TestBuf[TEST_LEN];

int EE_Address = 0;


/*
 *	subroutine ConUdpInit
 *	function :
 *	    console udp protocol initialization
 *	input :
 *	    ports   --> Uart port no.
 *			if port = 0 --> Port no is decided by CN2500 ID
 *			else	    --> Port no is fixed as input ports
 *	    baud    --> Baud rate
 *	output :
 *	    none
 */
void	ConUdpInit(int ports,ulong baud)
{
	int	i;

	SendAlive = 1;
	RemoteIp = 0xffffffff;
	Memset((UINT8 *)&Ack,0,sizeof(Ack));
	StartBurnTime = StopBurnTime = 0;
   	Ack.baud = 230400;
	Ack.ip = NicIp[G_LanPort];
	Ack.id = sysModelInfo[board_index].burn_id;
	Ack.status = STATUS_IDLE;
	Ack.serial = sysModelInfo[board_index].serial;
//	Ports = sysModelInfo[board_index].port;
#if E1240
	Ports = 9;	//E1240 test 10 ports
				//0: EEPROM , 1: ETH, 2~9: AI
#endif
	start_port = 0;
	end_port = start_port+Ports;
	Printf("Start port = %d End port = %d\r\n",start_port,end_port);
	if (ports != 0){
	    Ports = ports;
	}
	if ((baud > 0) && (baud <= 921600)){
	    Ack.baud = baud;
	}
	BurnNoError = 1;
	for (i=0;i<TEST_LEN;i++){
	    TestBuf[i] = (i & 0xff);
	}
	TimerOut(ConUdpPoll,POLL_TIMEOUT);
}

/*
 *	subroutine ConUdpPoll
 *	function :
 *	    console udp protocol polling routine
 *	input :
 *	    none
 *	output :
 *	    none
 */
int	ConUdpPoll(void)
{
	if ((long)(Gsys_msec - LastSendTime) >= ALIVE_TIMEOUT)
	    SendAlive = 1;
	if (SendAlive) {
	    if ((long)(Gsys_msec - LastSendTime) > 3000) {
		Ack.resp = CN25_ALIVE;
		if (Ack.status == STATUS_BURN)
		    Ack.time = (long)(Gsys_msec - StartBurnTime) / 1000;
		else
		    Ack.time = (long)(StopBurnTime - StartBurnTime) / 1000;
		UdpSend(0xffffffff,CONSOLE_UDP,AP_UDP,(UINT8 *)&Ack,sizeof(Ack));
		LastSendTime = Gsys_msec;
	    }
	}
	return (TimerOut(ConUdpPoll,POLL_TIMEOUT));
}

/*
 *	subroutine ConUdpReceive
 *	function :
 *	    console udp protocol receive routine
 *	input :
 *	    s_ip    --> sender ip address
 *	    d_ip    --> dest   ip address
 *	    buf     --> data pointer
 *	    len     --> length of data
 *	output :
 *	    none
 */
extern	int PowerMode(int port,UINT8 state);

void	ConUdpReceive(ulong s_ip,ulong d_ip,uchar *buf,ulong len)
{
	cn25cmd_t	ap;
	cn25download_t	down;
	ushort	ack;
	int		i,data_len;
//	int		ret;
	//struct rtc_time ctime;

	static ushort	down_ser;
	static uchar *down_ptr;
	ulong serial;
	ulong offset,length;
//	static ulong flag;

//	ConInit(ConsolePort,115200);
	if (len < sizeof(cn25cmd))  /* Invalid packet	*/
	    return;
	ap = (cn25cmd_t)buf;
	ack = 0;
	RemoteIp = s_ip;
	switch (ap->req.cmd) {
	    case CN25_DIAG	:		/* Diagnose ack packet	    */
//			Printf("CN25_DIAG");
			if (d_ip == 0xffffffff) 	/* should not be broad cast */
			    break;
			if (Ack.status == STATUS_BURN) {
			    Ack.nic_rx++;
			    SendAlive = 0;
			    LastSendTime = Gsys_msec;	/* adjust timeout	    */
			}
			break;
	    case CN25_POLL	:
			if ((Ack.status == STATUS_IDLE) && (Ack.ser == 0))
				ack = CN25_ACK;
			break;
	    case CN25_QUERY	:
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
				break;
			if (ap->req.ser != Ack.ser) {	/* Invalid ser no	    */
			    ack = CN25_NAK;
			    Ack.reason = REASON_SER;
				break;
			}
			Ack.ser++;
			ack = CN25_ACK;
			break;
	    case CN25_SET_PARA	:
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
			    break;
			if (ap->req.ser != Ack.ser) {
			    ack = CN25_NAK;
				Ack.reason = REASON_SER;
				break;
			}
			if ((ap->req.baud <= 0) || (ap->req.baud > 921600)) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_PARA;
				break;
			}
			if (Ack.status != STATUS_IDLE) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_STATUS;
				break;
			}	
			Ack.baud = ap->req.baud;
			ack = CN25_ACK;
			Ack.ser++;
			break;
	    case CN25_START	:
			if (d_ip == 0xffffffff){ 	/* broad cast , no response */
			    break;
			}
			if (ap->req.ser != Ack.ser) {
			    ack = CN25_NAK;
				Ack.reason = REASON_SER;
				break;
			}
			if (Ack.status != STATUS_IDLE) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_STATUS;
				break;
			}
			if (Ports <= 0) {
			    ack = CN25_NAK;
				Ack.reason = REASON_ID;
			    break;
			}

			Ack.tot_cnt = Ack.con_err = Ack.dram_err = 
			Ack.nic_tx = Ack.nic_rx = Ack.dlan_err = 0;
			for (i=0;i<Ports;i++)
			    Ack.p_err[i] = 0;
			ack = CN25_ACK;
			Ack.ser++;
			Ack.status = STATUS_BURN;
			StartBurnTime = Gsys_msec;
			BurnNoError = 1;
			break;
		case CN25_STOP	:
//Printf("CN25_STOP\r\n");
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
			    break;
			if (ap->req.ser != Ack.ser) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_SER;
				break;
			}	
			if (Ack.status != STATUS_BURN) {
			    ack = CN25_NAK;
				Ack.reason = REASON_STATUS;
			    break;
			}
			ack = CN25_ACK;
			Ack.ser++;
			StopBurnTime = Gsys_msec;
			Printf("\r\nStop End..\r\n");
			sleep(100);
			if ((ack == CN25_ACK) && (BurnNoError == 1)){// && (ap->req.monitor_error == 0)) {
				Ack.ser++;
				Ack.status = STATUS_OK;
				finish_time = Gsys_msec;
				Printf(".....OK\r\n");
			}else{
				Ack.status = STATUS_FAIL;
				Printf(".....FAIL\r\n");
			}
			break;
		case CN25_DOWNLOAD	:
//			Printf("CN25_DOWNLOAD\r\n");
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
			    break;
			if (ap->req.ser != Ack.ser) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_SER;
				break;
			}
			data_len = len - sizeof(cn25cmd);
			if (data_len <= 0) {
			    ack = CN25_NAK;
				Ack.reason = REASON_INVALID;
				Printf("len\r\n");
			    break;
			}
			if ((Ack.status != STATUS_OK) &&
			    (Ack.status != STATUS_DOWNLOAD)) {
			    ack = CN25_NAK;
				Ack.reason = REASON_STATUS;
				Printf("Status\r\n");
				break;
			}

			if (Ack.status == STATUS_OK) {
				down_ser = 0;
				down_ptr = (uchar *)SDRAM_DOWNLOAD_TEMP_BASE;
				DownLen /*= FileLen = FirmLen */= 0;
			}
			down = (cn25download_t)&(ap->data[0]);

			if (down_ser != down->ser) {
				ack = CN25_NAK;
			    Ack.reason = REASON_DOWN_SER;
				sleep(10);
				Printf("down Ser(%d) -> (%d)\r\n",down_ser,down->ser);
				sleep(10);
				down_ptr = (uchar *)SDRAM_DOWNLOAD_TEMP_BASE;
			    break;
			}
			data_len -= 2;	/* exclude download serial no	*/
			DownLen += data_len;
			if (DownLen > DOWN_MAX_LEN) {
			    ack = CN25_NAK;
			    Ack.reason = REASON_LENGTH;
			    DownLen -= data_len;
				break;
			}
			for (i=0;i<data_len;i++)
			    *down_ptr++ = down->data[i];
			down_ser++;

			ack = CN25_ACK;
			Ack.ser++;
			Ack.status = STATUS_DOWNLOAD;
			serial = sysModelInfo[board_index].serial;
			break;
	    case CN25_DOWN_RESET :
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
				break;
			if (ap->req.ser != Ack.ser) {
				ack = CN25_NAK;
				Ack.reason = REASON_SER;
				break;
			}
			if ((Ack.status != STATUS_OK) &&
				(Ack.status != STATUS_DOWNLOAD) &&
				(Ack.status != STATUS_DOWN_FAIL)) {
				ack = CN25_NAK;
				Ack.reason = REASON_STATUS;
				break;
			}
			ack = CN25_ACK;
			Ack.ser++;
			Ack.status = STATUS_OK;
			sleep(1000);
			break;
		case CN25_DOWN_CHK	:
			if (d_ip == 0xffffffff) 	/* broad cast , no response */
				break;
			if (ap->req.ser != Ack.ser) {
				ack = CN25_NAK;
				Ack.reason = REASON_SER;
				Printf("FAIL!\r\n");
				break;
			}
			if (Ack.status != STATUS_DOWNLOAD) {
				ack = CN25_NAK;
				Ack.reason = REASON_STATUS;
				Printf("FAIL!\r\n");
				break;
			}
			Ack.ser++;

			fd = (FRM_HEADER *)SDRAM_DOWNLOAD_TEMP_BASE;
			if(fwcheck(&offset,&length) == -1){
				Ack.status = STATUS_DOWN_FAIL;
				Ack.reason = REASON_FIRMWARE;
				ack = CN25_NAK;                        
				break;
			}

			Printf("FW = %x, %x\r\n",offset,length);
			Ack.status = STATUS_DOWN_OK;
			ack = CN25_ACK;
			Printf("DOWN_OK \r\n");
			break;
	    case CN25_SAVE	:
	    {
			Printf("CN25_SAVE\r\n");
			if (d_ip == 0xffffffff)
			    break;
			if (ap->req.ser != Ack.ser) {
				ack = CN25_NAK;
				Ack.reason = REASON_SER;
				break;
			}
			if (Ack.status != STATUS_DOWN_OK) {
				ack = CN25_NAK;
				Ack.reason = REASON_STATUS;
				break;
			}
			ack = CN25_ACK;

			if (ack == CN25_ACK) {
				ulong *frm_ptr;
				extern void diag_do_EraseFlash_func(void);
				frm_ptr = (ulong *)(SDRAM_DOWNLOAD_TEMP_BASE);

				diag_do_EraseFlash_func();

				UdpSend(RemoteIp,CONSOLE_UDP,AP_UDP,(uchar *)&Ack,sizeof(Ack));

				if(FlashromProgram(FIRMWARE_FLASH_BASE, (UINT32)frm_ptr,length) == 0)
					Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes .. OK!\r\n", FIRMWARE_FLASH_BASE, (FIRMWARE_FLASH_BASE+length), length);
				else{
					Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes .. FAIL!\r\n", FIRMWARE_FLASH_BASE, (FIRMWARE_FLASH_BASE+length), length);
					ack = CN25_NAK;
					Ack.reason = REASON_STATUS;
				}
			}
			
			if (ack == CN25_ACK) {
				Ack.ser++;
				Ack.status = STATUS_FINISH;
				
				diag_clear_EEPROM_BR();		// for Burn-in finish to erase EEPROM to default
				
				diag_do_set_clean_flag(1,FLAG_BR);		// Set BR flag
/*				
				SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
				SPI_ENABLE_SLAVE0();

		    	mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);	// Modify bu Jerry Wu[12/23,10'] for E1242-T
    	
				if((flag & FLAG_EOT) == FLAG_EOT){
					Printf("Test Model is ...... E1260-T\r\n");
					
					ret = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
					ret = 64;
					mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, ret);
					*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[1].hw_exid;

					if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, ret) != 0){
						Printf("FAIL !\r\n");
						return;
					}
					Initboardinfo(0);
				}
				else{
					Printf("Test Model is ...... E1260\r\n");
				}
*/				
				PowerMode(2,0);
				finish_time = Gsys_msec;
				Printf ("\r\nBurn-in flag ...... Set\r\n");
			}
			break;
		}
	    case CN25_RESET	:
			if (d_ip == 0xffffffff) {
			    if (Ack.status < STATUS_OK) {
					Ack.ser = 0;
					Ack.status = STATUS_IDLE;
					StartBurnTime = StopBurnTime = 0;
					Ack.tot_cnt = 0;
					Ack.con_err = 0;
					for (i=0;i<Ports;i++)
					    Ack.p_err[i] = 0;
					ack = CN25_ACK;
					diag_do_SYSRESET_func();
					break;
				}
			} else {
			    if (ap->req.ser != Ack.ser) {
					ack = CN25_NAK;
					Ack.reason = REASON_SER;
					break;
				}
				if (Ack.status < STATUS_DOWN_OK) {
					Ack.ser = 0;
					Ack.status = STATUS_IDLE;
					Ack.tot_cnt = 0;
					Ack.con_err = 0;
					for (i=0;i<Ports;i++)
					    Ack.p_err[i] = 0;
					StartBurnTime = StopBurnTime = 0;			    
					ack = CN25_ACK;
					diag_do_SYSRESET_func();
					break;
				}
			}
			break;
		default:
			break;
	}
	if (ack) {
		SendAlive = 0;
	    LastSendTime = Gsys_msec;
	    Ack.resp = ap->req.cmd | ack;
		if (Ack.status == STATUS_BURN){
			Ack.time = (ulong)(Gsys_msec - StartBurnTime) / 1000;
		}else{
			Ack.time = (ulong)(StopBurnTime - StartBurnTime) / 1000;
		}
		UdpSend(RemoteIp,CONSOLE_UDP,AP_UDP,(uchar *)&Ack,sizeof(Ack));
	}
}

/*
 *	subroutine ConUdpBurn
 *	function :
 *	    console udp burning routine
 *	input :
 *	    none
 *	output :
 *	    none
 */

extern int diag_EEPROM_MP_page(UINT16 addr,int length);
extern void printEthTestResult(int ret);
extern char EEPROM_address;
extern int diag_do_ETH_burn_test(void);
void	ConUdpBurn(void)
{
	static	ulong	timeout;
	static	int	ready = 1;
	static	int	test;
	int	err;
	static	ulong	diag_t;
	static	int	dram_ind;	/* used for dram test	*/
	int	i;
	
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();

	if (Ack.status == STATUS_FINISH) { /* burinig finish --> flash ready led */
//		Printf("FINISH\r\n");
	    if ((long)(Gsys_msec - finish_time) >= 500) {
			if(ready % 5)
				ReadyLed(OFF);
			else
				ReadyLed(ON);
			finish_time = Gsys_msec;
			ready++;
		}
	} else if (!BurnNoError) {  /* burning error --> flash ready led */
	    if ((long)(Gsys_msec - finish_time) >= ErrorFlashTime) {
		ReadyLed(OFF);
		Lnet_chip_smi_write(0x11,0x19,0x2A);	//Diable PHY LED
		Lnet_chip_smi_write(0x12,0x19,0x2A);
		finish_time = Gsys_msec;
	    }
	}

	if (Ack.status != STATUS_BURN) {
	    test = timeout = diag_t = 0;
	    err = 0;
	    dram_ind = 0;
	    return;
	}
	if (!SendAlive) {
	    if ((long)(Gsys_msec - diag_t) >= DIAG_TIMEOUT) {
		diag_t = Gsys_msec;
		Ack.resp = CN25_DIAG;
		if (Ack.status == STATUS_BURN)
		    Ack.time = (long)(Gsys_msec - StartBurnTime) / 1000;
		else
		    Ack.time = (long)(StopBurnTime - StartBurnTime) / 1000;
		UdpSend(RemoteIp,CONSOLE_UDP,AP_UDP,(UINT8*)&Ack,sizeof(Ack));
		Ack.nic_tx++;
		sleep(1000);
		NicProcess();
	    }
	} else
	    diag_t = Gsys_msec;
	if (timeout != 0) {	/* 5 = waitting for data    */
	    if (test < 5) {	/* 6 = read & check data    */
		if ((long) (Gsys_msec - timeout) < 0)
		    return;
	    }
	    if (test == 7) {	/* 7 = delay a while then next write	*/
		if ((long) (Gsys_msec - timeout) < 0)
		    return;
		test = 0;
		timeout = 0;
	    }
	}

	switch (test) {
	    case 0 :/* Test EEPROM */
			Ack.tot_cnt++;
			if(EE_Address < 0x10000){
				Printf("**************************EEPROM Test***************************\r\n");
#if MODULE_AIO
				i2c_disable();
				i2c_set_target_address(EEPROM_WRITE);
				i2c_enable();
				EEPROM_address = EEPROM_WRITE;
#endif
				Printf("EEPROM test from 0x%04x to 0x%04x and Length = %d bytes..",EE_Address,EE_Address+127,128);
				if(diag_EEPROM_MP_page(EE_Address,128) == -1){
					Ack.p_err[0]++;
					Printf("FAIL\r\n");
				}
				else
					Printf("OK\r\n");
				EE_Address += 128;
#if MODULE_AIO
				i2c_disable();
				i2c_set_target_address(PARMETER_EEPROM_WRITE);
				i2c_enable();
				EEPROM_address = PARMETER_EEPROM_WRITE;
#endif
			}
			test = 1;
			timeout = Gsys_msec;
			break;
	    case 1 :/* Test Ethernet port 2 */
			Printf("\r\n*************************Ethernet Test**************************\r\n");
			if(Ack.tot_cnt < 2){
				PowerMode(2,0);	//port II power on	
				sleep(1000);
			}

			Printf("Ethernet loop back test ..");
			err = diag_do_ETH_burn_test();
			if( err != 0){
				if(Ack.tot_cnt > 10){
					Ack.p_err[1]++;
					Printf("Fail\r\n");
					sleep(10);
				}
			}
			else{
				Printf("PASS\r\n");
			}
			test = 2;
			timeout = Gsys_msec;
			break;
	    case 2 :/* Test I/O */			        
			Printf("\r\n****************************I/O Test****************************\r\n");
#if E1240 | E1242
			err = diag_burn_in_AI();
#endif
#if E1262
			err = diag_burn_in_TC(1);
#endif
#if E1260
			err = diag_burn_in_RTD(1);
//			Printf ("\r\n err = %x\r\n",err);
#endif
			for(i=0;i<CHANNEL_NUMBER;i++){
				if(err & (0x1<<i))
					Ack.p_err[i+2]++;
			}
			
#if E1242
			err = diag_do_SPI_DIO_Burn_in_test();
			
			for (i=0;i<4;i++){
				if (err & (0x1<<i)){
					Ack.p_err[i+6]++;
				}
			}
#endif

			test = 3;
			timeout = Gsys_msec;
			break;
	    case 3 :
#if E1260
			err = diag_burn_in_RTD(0);
//			Printf ("\r\n err = %x\r\n",err);
#endif
			for(i=0;i<CHANNEL_NUMBER;i++){
				if(err & (0x1<<i))
					Ack.p_err[i+2]++;
			}
/*			if(BurnNoError){
				err=0;
				for(i=0;i<9;i++){
					err|=Ack.p_err[i];
				}
				if(err)
					BurnNoError = 0;
			}
*/			test = 4;
			timeout = Gsys_msec;
			break;
	    case 4 :
	    	if(BurnNoError){
				err=0;
				for(i=0;i<9;i++){
					err|=Ack.p_err[i];
				}
				if(err)
					BurnNoError = 0;
			}			
			test = 5;
			timeout = Gsys_msec;
			break;
	    case 5 :
			test = 6;
			timeout = Gsys_msec;
			break;
	    case 6 :	
			test = 0;
			timeout = 0; 
		break;
	    default :
		break;
	}
}
/*
typedef struct _FILEDATA {
	UINT32	file_type;
	UINT32	file_length;
	UINT16	file_checksum;
}FILEDATA , *FILEDATA_t;


typedef struct _FRM_HEADER {
	UINT32		frm_magic;
	UINT32		frm_version;
	UINT32		frm_package;
	FILEDATA	file_info[E1200_FWNO];
	UINT8		rev[10];
}FRM_HEADER;*/

int fwcheck(ulong *offset, ulong *length){
	ulong FWMagic;
	int FwNo,pkt,fwLe1,fWType;
	int i,fwcode[2];
	FRM_HEADER *fd1;

	fwcode[0] = 0xF;
	fwcode[1] = 2;

	fd1 = (FRM_HEADER *)(SDRAM_DOWNLOAD_TEMP_BASE);
	FWMagic = fd1->frm_magic;
	if(FWMagic != DACmagic){
		Printf("FWMagic! = %x\r\n",FWMagic);
		return -1;
	} 
	pkt = fd1->frm_version; 
			
	FwNo = fd1->frm_package;
	Printf("FWMagic = %x pkt = %x FwNo = %x\r\n",FWMagic,pkt,FwNo);
	if(!FwNo){
		Printf("No FW\r\n");
		return -1;	
	}
	else{
		if(FwNo == E1200_FWNO){
			for(i=0;i<FwNo;i++){
				fWType=fd1->file_info[i].file_type;
				if(fWType!=fwcode[i]){
					Printf("FWType Mismatch! = %x\r\n",fWType);
					return -1;
				}
				length[i] = fd1->file_info[i].file_length;			
				fwLe1=fd1->file_info[i].file_checksum;
				Printf("FirmLen = %x, CheckSum = %x \r\n",length[i],fwLe1);
			}
			offset[0] = 12+sizeof(FILEDATA)*FwNo;			
		}
		else{
			Printf("ERROR FW\r\n");
			return -1;
		}
	}
	length[0] = fd1->file_info[0].file_length;	//FW
	return 1;
}

HEAD_ISD header;


