/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	global.h

	Define the memory map and some definition of global using.
	
	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-14	Chin-Fu Yang		
		modified			
    
*/


#ifndef _GLOBAL_H
#define _GLOBAL_H


/*------------------------------------------------------ Macro / Define -----------------------------*/
#define CONFIG_STACKSIZE			(96*1024)	/* regular stack */

/* DiagTest return values */
#define DIAG_OK						0
#define DIAG_FAIL					1
#define DIAG_ESC					2
#define DIAG_FAIL_LENGTH			3
#define DIAG_FAIL_DEST_ADDR		4
#define DIAG_FAIL_SOURCE_ADDR		5
#define DIAG_FAIL_DATA_ERROR		6
#define DIAG_FAIL_PACKET_LOSS		7


/* Define For Memory Block size */ 
#define BIOS_MAX_SIZE                       			0x40000  	/* 256K bytes */
#define GENERAL_MEMORY_MAX_SIZE             	0x20000  	/* 128K bytes */
#define GENERAL_MEMORY_BLOCK_MAX_SIZE     2048     		/* 2K bytes */
#define CONSOLE_TXBUF_SIZE                  		0x1000   		/* 4K bytes */
#define CONSOLE_RXBUF_SIZE                  		0x1000   		/* 4K bytes */
#define MAC_TX_BLOCK_CNT    		        	16	     		/* MAC TX ring blocks */
#define MAC_RX_BLOCK_CNT	    	        		32	     		/* MAC RX ring blocks */
#define DESCRIPTOR_SIZE	                			16       		/* 16 bytes */
#define MAC_TX_DESCRIPTOR_MAX_SIZE            (MAC_TX_BLOCK_CNT * DESCRIPTOR_SIZE) 	/* 256 bytes */
#define MAC_RX_DESCRIPTOR_MAX_SIZE            (MAC_RX_BLOCK_CNT * DESCRIPTOR_SIZE) 	/* 512 bytes */
#define MAC_BLOCK_MAX_SIZE                  		1536		// Ethernet type
#define MAC_TXBUF_MAX_SIZE                 		(MAC_TX_BLOCK_CNT * MAC_BLOCK_MAX_SIZE)
#define MAC_RXBUF_MAX_SIZE                 		(MAC_RX_BLOCK_CNT * MAC_BLOCK_MAX_SIZE)
#define UART_BUF_MAX_SIZE					2048
#define MAC_MAX_AMOUNT                      		5	// only for testing 88E6060
#define UART_MAX_AMOUNT                     		3
#define SRAM_MAX_SIZE						4096		// 4KB
#define SDRAM_MAX_SIZE						0x400000	// 4MB


// S2E SRAM
#define SRAM_REMAP_BASE					0x03000000
#define SRAM_END							(SRAM_REMAP_BASE + SRAM_MAX_SIZE)		//	4KB


// S2E SPI FLASH
#define FLASH_MAX_SIZE						0x200000		// 2MB
#define FLASH_SECTOR_SIZE					4096			// 4KB
#define FLASH_BLOCK_SIZE					(64*1024)		// 64KB

/* SPI flash memory map. These definition should equal to the definition in start.S */
#define SPI_FLASH_SIZE                  			(2*1024*1024)		 // 2MB
#define BOOTLOADER_FLASH_BASE           		0x00000000
#define MANUFACTURE_CONFIG_FLASH_BASE	0x00001000
#define MANUFACTURE_PROGRAM_FLASH_BASE	0x00002000
#define FIRMWARE_FLASH_BASE				0x00050000
/* BIOS Code & Data Size = bss_end + FIQ_STACK + IRQ_STACK + ABORT_STACK + UNDEFINED_STACK + SUPERVISOR_STACK */
/* BIOS CONFIGURATION TABLE Offset */
#define PVERSION_OFFSET		0x00
#define HWID_OFFSET			0x04
#define HWEXID_OFFSET			0x08
#define SERIAL_OFFSET			0x0C
#define MACA_OFFSET			0x10
#define MACB_OFFSET			0x16
#define FLAG_OFFSET				0x1C
#define SERVERIP_OFFSET			0x20
#define LOCALIP_OFFSET			0x24
#define JUMP_TO_FIRM_OFFSET	0x28
//#define JUMP_TO_FIRM_OFFSET	0x20
/* BIOS Header 32 bytes Offset */
#define MAGIC_OFFSET			0x00
#define VERIOSN_OFFSET			0x04
#define FILELEN_OFFSET			0x08
#define PROG_START_OFFSET		0x0C
#define CHECKSUM_OFFSET		0x10
#define RESERVE0_OFFSET		0x14
#define RESERVE1_OFFSET		0x18
#define RESERVE2_OFFSET		0x1C



/* BIOS CONFIGURATION TABLE Offset. There are 4K Bytes for config data */
#define SMC_FLASH0_PVERSION		(MANUFACTURE_CONFIG_FLASH_BASE|PVERSION_OFFSET)
#define SMC_FLASH0_HWID			(MANUFACTURE_CONFIG_FLASH_BASE|HWID_OFFSET)
#define SMC_FLASH0_HWEXID			(MANUFACTURE_CONFIG_FLASH_BASE|HWEXID_OFFSET)
#define SMC_FLASH0_SERIAL			(MANUFACTURE_CONFIG_FLASH_BASE|SERIAL_OFFSET)
#define SMC_FLASH0_MACA			(MANUFACTURE_CONFIG_FLASH_BASE|MACA_OFFSET)
#define SMC_FLASH0_MACB			(MANUFACTURE_CONFIG_FLASH_BASE|MACB_OFFSET)
#define SMC_FLASH0_FLAG			(MANUFACTURE_CONFIG_FLASH_BASE|FLAG_OFFSET)
#define SMC_FLAGH0_SERVERIP		(MANUFACTURE_CONFIG_FLASH_BASE|SERVERIP_OFFSET)
#define SMC_FLAGH0_LOCALIP			(MANUFACTURE_CONFIG_FLASH_BASE|LOCALIP_OFFSET)
#define SMC_FLAGH0_JUMP_FIRM		(MANUFACTURE_CONFIG_FLASH_BASE|JUMP_TO_FIRM_OFFSET)

/* BIOS Header 32 bytes Offset */
#define SMC_FLASH0_MAGIC			(MANUFACTURE_PROGRAM_FLASH_BASE|MAGIC_OFFSET)
#define SMC_FLASH0_VERIOSN			(MANUFACTURE_PROGRAM_FLASH_BASE|VERIOSN_OFFSET)
#define SMC_FLASH0_FILELEN			(MANUFACTURE_PROGRAM_FLASH_BASE|FILELEN_OFFSET)
#define SMC_FLASH0_PROG_START		(MANUFACTURE_PROGRAM_FLASH_BASE|PROG_START_OFFSET)
#define SMC_FLASH0_CHECKSUM		(MANUFACTURE_PROGRAM_FLASH_BASE|CHECKSUM_OFFSET)
#define SMC_FLASH0_RESERVE0		(MANUFACTURE_PROGRAM_FLASH_BASE|RESERVE0_OFFSET)
#define SMC_FLASH0_RESERVE1		(MANUFACTURE_PROGRAM_FLASH_BASE|RESERVE1_OFFSET)
#define SMC_FLASH0_RESERVE2		(MANUFACTURE_PROGRAM_FLASH_BASE|RESERVE2_OFFSET)



/* CONFIG FLAG */
#define FLAG_MP				0x00000001      	/* MP Flag */
#define FLAG_SERIAL_MAC 	0x00000002		/* Serial No. & MAC Flag */
#define FLAG_EOT			0x00000004		/* EOT Flag */
#define FLAG_BR				0x00000008		/* BR Flag */
#define FLAG_HWEXID			0x00000010		/* HWEXID ID Flag */
#define FLAG_CAL			0x00000020		/* Calibration Flag */
#define FLAG_POWER_FAIL		0x00000040		/* Power Fail Flag */
#define FLAG_WARMUP			0x00000080		/* Calibration WARN UP Flag */
#define FLAG_REPORT			0x00000100		/* Calibration report Flag */

/* UART and MAC Operational mode */
#define MODE_NORMAL			0x00000001
#define MODE_LOOPBACK			0x00000002
#define MODE_DRIVING			0x00000004
#define MODE_LAN10M			0x00000008
#define MODE_LAN100M			0x00000010
#define MODE_FDUP				0x00000020

#define MODE_PWRDWN			0x00000040



// Hardware ID
#define MOXA_ioLogik_E1200_MODEL_MAX_NUM 		2
//#define E1240						0x74
//#define E1241						0x76
//#define E1242						0x75
#define E1260						0x77
//#define E1262						0x78
#define Analog_Input				1
//#define Analog_output				0
#define MODULE_AIO					1

//#define NPORT6150_HWEXID	 			0x00006150
//#define NPORT6250_HWEXID	 			0x00006250
//#define NPORT6250_S_SC_HWEXID 		0x00006251
//#define NPORT6250_M_SC_HWEXID 		0x00006252
#define NPORT2150PLUS_US_HWEXID		0x00050007
#define NPORT2150PLUS_EU_HWEXID		0x00050008
#define NPORT2150PLUS_JP_HWEXID		0x00050009
#define NPORT2250PLUS_US_HWEXID		0x00050010
#define NPORT2250PLUS_EU_HWEXID		0x00050011
#define NPORT2250PLUS_JP_HWEXID		0x00050012
#define NPORT2150PLUS_S_US_HWEXID	0x00050013
#define NPORT2150PLUS_S_EU_HWEXID	0x00050014
#define NPORT2150PLUS_S_JP_HWEXID	0x00050015
#define NPORT2250PLUS_S_US_HWEXID	0x00050016
#define NPORT2250PLUS_S_EU_HWEXID	0x00050017
#define NPORT2250PLUS_S_JP_HWEXID	0x00050018
#define WE2100T_US_HWEXID	        		0x00002100
#define WE2100T_EU_HWEXID	        		0x00002101
#define WE2100T_JP_HWEXID	        		0x00002102
#define MB3170_HWEXID               			0x00003110
#define MB3170_I_HWEXID				0x00003118
#define MB3270_HWEXID					0x00003210
#define MB3270_I_HWEXID				0x00003218
#define RS232_FACE						0x00	
#define RS4852W_FACE					0x01
#define RS4854W_FACE					0x02
#define RS422_FACE						0x04
#define SERIAL_FACE_MASK  				(RS232_FACE|RS4852W_FACE|RS4854W_FACE|RS422_FACE)
#define DISABLE_FACE					0xFF


#define FILE_NUM 				2		// for firmware



/* Define For Memory Base Address */
#define SDRAM_REMAP_BASE                     		0x0      
#define SDRAM_CONSOLE_TXBUF_BASE           	(SDRAM_REMAP_BASE + BIOS_MAX_SIZE + CONFIG_STACKSIZE)  /* stack set in start.S */
#define SDRAM_CONSOLE_RXBUF_BASE           	(SDRAM_CONSOLE_TXBUF_BASE + CONSOLE_TXBUF_SIZE)
#define SDRAM_GENERAL_MEMORY_BASE           	(SDRAM_CONSOLE_RXBUF_BASE + CONSOLE_RXBUF_SIZE)
#define SDRAM_MAC_TX_DESCRIPTOR_BASE(x)   ((SDRAM_GENERAL_MEMORY_BASE + GENERAL_MEMORY_MAX_SIZE) + (x * MAC_TX_DESCRIPTOR_MAX_SIZE))   
#define SDRAM_MAC_RX_DESCRIPTOR_BASE(x)   (SDRAM_MAC_TX_DESCRIPTOR_BASE(MAC_MAX_AMOUNT) + (x * MAC_RX_DESCRIPTOR_MAX_SIZE))   
#define SDRAM_MAC_TX_BUF_BASE(x)            	(SDRAM_MAC_RX_DESCRIPTOR_BASE(MAC_MAX_AMOUNT) + (x * MAC_TXBUF_MAX_SIZE)) 
#define SDRAM_MAC_RX_BUF_BASE(x)            	(SDRAM_MAC_TX_BUF_BASE(MAC_MAX_AMOUNT) + (x * MAC_RXBUF_MAX_SIZE)) 
#define SDRAM_UART_TX_BASE(x)               		(SDRAM_MAC_RX_BUF_BASE(MAC_MAX_AMOUNT) + (x * UART_BUF_MAX_SIZE)) 
#define SDRAM_UART_RX_BASE(x)               		(SDRAM_UART_TX_BASE(UART_MAX_AMOUNT) + (x * UART_BUF_MAX_SIZE))
#define SDRAM_TEST_BASE					(SDRAM_UART_RX_BASE(UART_MAX_AMOUNT))		// the location outside of BIOS
#define SDRAM_DOWNLOAD_TEMP_BASE		SDRAM_TEST_BASE

// MAC descriptor in SRAM to speed up access rate
#define SRAM_MAC_TX_DESCRIPTOR_BASE(x)	((SRAM_REMAP_BASE) + (x * MAC_TX_DESCRIPTOR_MAX_SIZE))   
#define SRAM_MAC_RX_DESCRIPTOR_BASE(x)	(SRAM_MAC_TX_DESCRIPTOR_BASE(MAC_MAX_AMOUNT) + (x * MAC_RX_DESCRIPTOR_MAX_SIZE))   
#define MAC_TX_DESCRIPTOR_BASE(x)			SDRAM_MAC_TX_DESCRIPTOR_BASE(x)
#define MAC_RX_DESCRIPTOR_BASE(x)			SDRAM_MAC_RX_DESCRIPTOR_BASE(x)


/*------------------------------------------------------ Structure ----------------------------------*/
typedef struct _SysModelInfo_{
	unsigned char	hw_id;		/* hw id , This is for HW to distinguish the product */ //8bit size		
	unsigned long	hw_exid; 	/* hw external id , This is for SW to distinguish the product */
	unsigned short burn_id;	/* burn-in id , This is for burn-in to distinguish the product*/
	unsigned long	magic;		/* interface */	
	unsigned char	port;   		/* uart max num */
	unsigned char	lan;    		/* lan max num */
	unsigned char	wlan;		/* wlan max num */
	unsigned int    pci;    		/* is this product support pci ?*/
	unsigned int	smcflash0; 	/* SMC_0 Flash type */
	unsigned int	smcflash1; 	/* SMC_2 Flash type */
	unsigned long  serial;		/* Serial Number of Product. This can be modified in flash. */
	unsigned char	modelName[32];
}_SysModelInfo;


typedef struct _FILEDATA {
	UINT32	file_type;
	UINT32	file_length;
	UINT16	file_checksum;
}FILEDATA , *FILEDATA_t;

#define E1200_FWNO	1

typedef struct _FRM_HEADER {
	UINT32		frm_magic;
	UINT32		frm_version;
	UINT32		frm_package;
	FILEDATA	file_info[E1200_FWNO];
	UINT8		rev[10];
}FRM_HEADER;


typedef struct burnin_cmd_T burnin_cmd;

#define DACmagic	0x21434144



/*------------------------------------------------------ Extern / Function Declaration -----------------*/
/* for the following variables, see start.S */
extern ulong _armboot_start; /* code start */
extern ulong _armboot_end;   /* code end */ 
extern ulong _bss_end;       /* bss end */    
extern ulong IRQ_STACK_START; /* top of IRQ stack */
extern ulong FIQ_STACK_START; /* top of FIQ stack */
extern ulong _armboot_real_end; /* first usable RAM address */

extern char	MacSrc[MAC_MAX_AMOUNT][6];

/*
 * varibles on ip.c
 */
extern int G_LanPort;
extern ulong NicIp[MAC_MAX_AMOUNT];
extern struct addrtab ArpTable[];

extern _SysModelInfo sysModelInfo[MOXA_ioLogik_E1200_MODEL_MAX_NUM + 1];
extern int board_index;




#endif /* _GLOBAL_H */
