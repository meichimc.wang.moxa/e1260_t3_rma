/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	chipset.h

	Header file for S2E chip.

	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-14	Chin-Fu Yang		
		modified			
    
*/

#ifndef CHIPSET_H
#define CHIPSET_H


/*
 *   S2E address map (Normal mode , Remap = 1)
 *
 *    ============================================
 *     0x00000000 ~ 0x003FFFFF 	SDRAM
 *     0x03000000 ~ 0x03000FFF	SRAM
 *     0x04000000 ~ 0x0400FFFF	Memory Controller's Register
 *     0x04010000 ~ 0x04011FFF	Ethernet MAC
 *     0x04012000 ~ 0x04012FFF	AHB Controller
 *     0x05000000 ~ 0x0500FFFF	Remap
 *     0x05010000 ~ 0x0501FFFF	GPIO
 *     0x05020000 ~ 0x0502FFFF	I2C
 *     0x05030000 ~ 0x0503FFFF	General UART
 *     0x05040000 ~ 0x0504FFFF	SPI
 *     0x05050000 ~ 0x0505FFFF	Interrupt Controller
 *     0x05060000 ~ 0x0506FFFF	Timer
 *     0x05070000 ~ 0x0507FFFF	Watch Dog Timer
 *     0x05080000 ~ 0x0508FFFF	MOXA UART
 *    ============================================
*/


#define S2E_MEM_CTRL_BASE			0x04000000	/* AHB slave 2: Registers of memory controller */ 
#define S2E_MAC_BASE					0x04010000	/* AHB slave 3: Ethernet MAC */
#define S2E_AHB_CTRL_BASE			0x04012000
#define S2E_AHB2APB_BASE			0x05000000	/* AHB slave 4: AHB2APB Bridge (Remap inside) */


/************************************************************************/
/* APB Device system registers											*/
/*  Memory definitions                                                  */
/************************************************************************/

#define S2E_REMAP_BASE				0x05000000		/* APB slave 9: Remap */
#define S2E_GPIO_BASE				0x05010000		/* APB slave 2: GPIO */
#define S2E_I2C_BASE					0x05020000		/* APB slave 3: I2C */
#define S2E_DW_UART_BASE			0x05030000
#define S2E_SPI_BASE					0x05040000		/* APB slave 1: SPI */
#define S2E_INTC_BASE				0x05050000		/* APB slave 8: Interrupt Controller */
#define S2E_TIMER_BASE				0x05060000		/* APB slave 5: Timer */
#define S2E_WDT_BASE					0x05070000		/* APB slave 6: Watch Dog Timer */
#define S2E_MOXA_UART_BASE			0x05080000		/* APB slave 7: MOXA UART */ // UART 0 & UART 1(shift 8)
#define S2E_CLOCK_CONTROL_BASE	0x05090000

/************************************************************************/
/* TIMER  Control Registers	      										*/
/************************************************************************/

#define TIMER1_LOAD_COUNT		0x00
#define TIMER1_CURRENT_VALUE	0x04
#define TIMER1_CONTROL			0x08
#define TIMER1_EOI				0x0C
#define TIMER_INT_STATUS		0xA0
#define TIMER_EOI					0xA4
#define TIMER_RAW_INT_STATUS	0xA8
#define TIMER_COMPO_VERSION	0xAC

/* -------------------------------------------------------------------------------
 *  GPIO definitions
 * -------------------------------------------------------------------------------
 */

#define GPIO_DATA					0x0
#define GPIO_DIRCTION			0x4

#define GPIO_INT_ENABLE			0x30
#define GPIO_INT_MASK			0x34
#define GPIO_INT_TYPE			0x38
#define GPIO_INT_POLARITY		0x3C			// rising = 1 , falling = 0
#define GPIO_INT_STATUS			0x40
#define GPIO_INT_RAWSTATUS		0x44
#define GPIO_INT_EOI				0x4C
#define GPIO_EX					0x50
#define GPIO_LS_SYNC				0x60
#define GPIO_CONFIG_REG1		0x74
#define GPIO_CONFIG_REG2		0x70

#define GPIO_NUM					16
#define GPIO_EDGE					0
#define GPIO_LEVEL				1


/* -------------------------------------------------------------------------------
 *  Interrupt Controller - Register Offset
 * -------------------------------------------------------------------------------
 */

#define IRQ_INT_ENABLE			0x00			// default 0 = disable
#define IRQ_INT_MASK				0x08			// default 0
#define IRQ_INT_FORCE			0x10			// default 1
#define IRQ_RAWSTATUS			0x18			// default 0

#define IRQ_MASKSTATUS			0x28			// default 0
#define IRQ_FINALSTATUS			0x30			// default 0

#define FIQ_INT_ENABLE			0xC0			// default 0
#define FIQ_INT_MASK				0xC4			// default 0
#define FIQ_INT_FORCE			0xC8			// default 1
#define FIQ_RAWSTATUS			0xCC			// default 0

#define FIQ_FINALSTATUS			0xD4			// default 0

#define IRQ_PLEVEL				0xD8
#define IRQ_PR_SPI				0xE8
#define IRQ_PR_GPIO				0xEC
#define IRQ_PR_I2C				0xF0
#define IRQ_PR_UART				0xF4
#define IRQ_PR_MAC				0xF8
#define IRQ_PR_WDT				0xFC
#define IRQ_PR_MOXA_UART		0x100
#define IC_COMP_VERSION			0xE0



/*  -------------------------------------------------------------------------------
 *   I2C Controllers
 *  -------------------------------------------------------------------------------
 */

#define I2C_CONTROL				0x00
#define I2C_TAR					0x04		// target address
#define I2C_SAR					0x08		// slave address
#define I2C_HS_MADDR				0x0c		// high speed master mode address
#define I2C_DATA_CMD				0x10		// RX /TX data buffer & command
#define I2C_SS_SCL_HCNT			0x14		// Standard speed I2C clock SCL high count register
#define I2C_SS_SCL_LCNT			0x18		// Standard speed I2C clock SCL low count register
#define I2C_FS_SCL_HCNT			0x1C		// Fast speed I2C clock SCL high count register
#define I2C_FS_SCL_LCNT			0x20		// Fast speed I2C clock SCL low count register
#define I2C_HS_SCL_HCNT			0x24		// High speed I2C clock SCL high count register
#define I2C_HS_SCL_LCNT			0x28		// High speed I2C clock SCL low count register
#define I2C_INTR_STAT			0x2C		// Interrupt Status
#define I2C_INTR_MASK			0x30		// Interrupt Mask
#define I2C_RAW_INTR_STAT		0x34		// RAW Interrupt Status
#define I2C_RX_TL					0x38		// RX FIFO Threshold
#define I2C_TX_TL					0x3C		// TX FIFO Threshold
#define I2C_CLR_INTR				0x40		// clear combined and individual interrupt
#define I2C_CLR_RX_UNDER		0x44		// clear RX_UNDER interrupt
#define I2C_CLR_RX_OVER			0x48		// clear RX_OVER interrupt
#define I2C_CLR_TX_OVER			0x4C		// clear TX_OVER interrupt
#define I2C_CLR_RD_REQ			0x50		// clear RD_REQ interrupt
#define I2C_CLR_TX_ABRT			0x54		// clear TX_ABRT interrupt
#define I2C_CLR_RX_DONE			0x58		// clear RX_DONE interrupt
#define I2C_CLR_ACTIVITY		0x5C		// clear ACTIVITY interrupt
#define I2C_CLR_STOP_DET		0x60		// clear STOP_DET interrupt
#define I2C_CLR_START_DET		0x64		// clear START_DET interrupt
#define I2C_CLR_GEN_CALL		0x68		// clear GEN_CALL interrupt
#define I2C_ENABLE				0x6C		// I2C Enable
#define I2C_STATUS				0x70		// I2C Status
#define I2C_TXFLR					0x74		// I2C Transmit FIFO Level
#define I2C_RXFLR					0x78		// I2C Receive FIFO Level
#define I2C_TX_ABRT_SOURCE		0x80		// I2C Transmit Abort Source
#define I2C_SDA_SETUP			0x94		// I2C SDA Setup
#define I2C_ACK_GENERAL_CALL	0x98		// ACK General call
#define I2C_ENABLE_STATUS		0x9C		// Enable Status
#define I2C_COMP_PARAM_1		0xF4		// Component Parameter Register 1
#define I2C_COMP_VERSION		0xF8		// Component Version
#define I2C_COMP_TYPE			0xFC		// Component Type


/* -------------------------------------------------------------------------------
 *  Internal UART baudrate definitions
 * -------------------------------------------------------------------------------
 */
#define BAUDRATE_450	5208	//dll = 88 ,dlm = 20
#define BAUDRATE_600	3906	//dll = 66 ,dlm = 15
#define BAUDRATE_900	2604	//dll = 44 ,dlm = 10
#define BAUDRATE_1200	1953	//dll = 161 ,dlm = 7
#define BAUDRATE_1800	1302	//dll = 22 ,dlm = 5
#define BAUDRATE_2400	977		//dll = 209 ,dlm = 3
#define BAUDRATE_3600	651		//dll = 139 ,dlm = 2
#define BAUDRATE_4800	488		//dll = 232 ,dlm = 1
#define BAUDRATE_7200	326		//dll = 70 ,dlm = 1
#define BAUDRATE_9600	244		//dll = 244 ,dlm = 0
#define BAUDRATE_14400	163		//dll = 163 ,dlm = 0
#define BAUDRATE_19200	122		//dll = 122 ,dlm = 0
#define BAUDRATE_28800	81		//dll = 81 ,dlm = 0
#define BAUDRATE_38400	61		//dll = 61 ,dlm = 0
#define BAUDRATE_57600	41		//dll = 41 ,dlm = 0
#define BAUDRATE_115200	20		//dll = 20 ,dlm = 0
#define BAUDRATE_230400	10		//dll = 10 ,dlm = 0
#define BAUDRATE_460800	5		//dll = 5 ,dlm = 0
#define BAUDRATE_921600	3		//dll = 3 ,dlm = 0

/* -------------------------------------------------------------------------------
 *  SPI definitions
 * -------------------------------------------------------------------------------
 */
#define SPI_CONTROL0						0x00
#define SPI_CONTROL1						0x04
#define SPI_SSI_ENABLE					0x08
#define SPI_MICROWIRE_CTRL				0x0C
#define SPI_SLAVE_SELECT				0x10
#define SPI_CLOCK_DIV					0x14	/* Fsclk_out = Fssi_clk / SCKDV */
#define SPI_TX_FIFO_THRESHOLD			0x18
#define SPI_RX_FIFO_THRESHOLD			0x1C
#define SPI_TX_FIFO_LEVEL				0x20
#define SPI_RX_FIFO_LEVEL				0x24
#define SPI_STATUS_REG					0x28
#define SPI_INT_MASK						0x2C
#define SPI_INT_STATUS					0x30
#define SPI_RAW_INT_STATUS				0x34
#define SPI_TX_FIFO_OVER_INT_CLR		0x38
#define SPI_RX_FIFO_OVER_INT_CLR		0x3C
#define SPI_RX_FIFO_UNDER_INT_CLR	0x40
#define SPI_MULTI_MASTER_INT_CLR		0x44
#define SPI_INT_CLR						0x48
#define SPI_ID								0x58
#define SPI_VERSION						0x58
#define SPI_DATA							0x60	/* read: access RX FIFO, write: access TX FIFO */	// Maximum Data width = 16-bit



//\\ S2E - These MACROs have to modify when the system clock change!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define MHz					1000000L
#define SYS_CLK			(75 * MHz)			// 1 (SYS) : 1 (AHB)  : 1/2 (APB) Model
#define AHB_CLK			SYS_CLK
#define MAX_TIMER			1
#define APB_CLK			(SYS_CLK / 2)
#define DEFAULT_TICK		1

#endif
