/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	i2c.c

	Embedded I2C.
		
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified
	2009-08-03	Nolan Yu
		modified    
*/


#include "types.h"
#include "chipset.h"
#include "global.h"
#include "errno.h"
#include "i2c.h"
#include "lib.h"
#include "console.h"
#include "timer.h"

#if MODULE_AIO
char EEPROM_address = EEPROM_WRITE;
#endif

#define TX_FIFO_DEPTH		8
/**	\brief
 *
 *	Disable all interrupts
 *
 *	\retval 		error code
 */
int i2c_init(void)
{
	int retval;
	retval = i2c_disable();

	if(retval == 0){
		i2c_mask_irq(i2c_irq_all);
		i2c_clear_irq(i2c_irq_all);
	}

	return retval;
}

/**	\brief
 *
 *	Enable I2C chip.
 *
 */
void i2c_enable(void)
{
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_ENABLE);
	reg |= sBIT0;
	Outhw(S2E_I2C_BASE + I2C_ENABLE , reg);
}

/**	\brief
 *
 *	Disable I2C chip.
 *
 */
int i2c_disable(void)
{
	UINT16 reg;
	int retval;
	if( i2c_is_busy() == FALSE){
		reg = Inhw(S2E_I2C_BASE + I2C_ENABLE);
		reg &= ~sBIT0;
		Outhw(S2E_I2C_BASE + I2C_ENABLE , reg); 
		retval = 0;
	}else{
		retval = -EBUSY;
	}

	return retval;
}

/**	\brief
 *
 *	Check if the I2C is busy
 *
 *	\param[in]	value : ...
 *
 *	\retval 		TRUE : Busy
 *	\retval 		TRUE : Free
 */
BOOL i2c_is_busy(void)
{
	UINT16 reg;
	BOOL retval;
	retval = FALSE;
	reg = Inhw(S2E_I2C_BASE + I2C_STATUS);
	if(reg & sBIT0){
		retval = TRUE;
	}

	return retval;
}

/**	\brief
 *
 *	Check if TX FIFO is empty
 *
 *	\param[in]	value : ...
 *
 *	\retval 		TRUE : Busy
 *	\retval 		TRUE : Free
 */
BOOL i2c_is_tx_fifo_empty(void)
{
	UINT32 reg;
	BOOL retval;
	retval = FALSE;
	reg = Inw(S2E_I2C_BASE + I2C_STATUS);
	if(reg & BIT2){
		retval = TRUE;
	}

	return retval;
}

/**	\brief
 *
 *	Check if TX FIFO is full
 *
 *	\param[in]	value : ...
 *
 *	\retval 		TRUE : Busy
 *	\retval 		TRUE : Free
 */
BOOL i2c_is_tx_fifo_full(void)
{
	UINT32 reg;
	BOOL retval;
	retval = FALSE;
	reg = Inw(S2E_I2C_BASE + I2C_STATUS);
	if(!(reg & BIT1)){
		retval = TRUE;
	}

	return retval;
}

/**	\brief
 *
 *	Check if RX FIFO is empty
 *
 *	\param[in]	value : ...
 *
 *	\retval 		TRUE : Busy
 *	\retval 		TRUE : Free
 */
BOOL i2c_is_rx_fifo_empty(void)
{
	UINT32 reg;
	BOOL retval;
	retval = FALSE;
	reg = Inw(S2E_I2C_BASE + I2C_STATUS);
	if(!(reg & BIT3)){
		retval = TRUE;
	}

	return retval;
}

/**	\brief
 *
 *	Check if RX FIFO is full
 *
 *	\param[in]	value : ...
 *
 *	\retval 		TRUE : Busy
 *	\retval 		TRUE : Free
 */
BOOL i2c_is_rx_fifo_full(void)
{
	UINT32 reg;
	BOOL retval;
	retval = FALSE;
	reg = Inw(S2E_I2C_BASE + I2C_STATUS);
	if(reg & BIT4){
		retval = TRUE;
	}

	return retval;
}

/**	\brief
 *
 *	Gets the TX FIFO level.
 *
 *	\param[in]	value : ...
 *
 *	\retval 		level
 */
int get_tx_fifo_level(void)
{
	UINT32 reg;
	reg = Inw(S2E_I2C_BASE + I2C_TXFLR);
	reg &= 0x0000001F;		// mask bit 5~31
	
	return reg;
}

/**	\brief
 *
 *	Gets the RX FIFO level.
 *
 *	\param[in]	value : ...
 *
 *	\retval 		level
 */
int get_rx_fifo_level(void)
{
	UINT32 reg;
	reg = Inw(S2E_I2C_BASE + I2C_RXFLR);
	reg &= 0x0000001F;		// mask bit 5~31
	
	return reg;
}

/**	\brief
 *
 *	If the data in TX FIFO is less than threshold level , the interrupt of TX empty  will generate.
 *
 *	\param[in]	level : set the trigger threshold of TX empty
 */
void set_tx_threshold_level(UINT8 level)
{
	UINT16 reg;

	if( (int)level > TX_BUFFER_DEPTH){
		level = TX_BUFFER_DEPTH;
	}
	reg = Inhw(S2E_I2C_BASE + I2C_TX_TL);
	reg &= 0xff00;		// mask bit 0~7
	reg |= level;
	Outhw(S2E_I2C_BASE + I2C_TX_TL , reg);
}

/**	\brief
 *
 *	Gets the TX threshold level.
 *
 *	\retval 		level
 */
int get_tx_threshold_level(void)
{
	UINT16 reg;

	reg = Inhw(S2E_I2C_BASE + I2C_TX_TL);
	reg &= 0xff00;		// mask bit 0~7
	return (int)reg;
}

/**	\brief
 *
 *	If the data in RX FIFO is less than threshold level , the interrupt of RX empty will generate.
 *
 *	\param[in]	level : set the trigger threshold of RX empty
 */
void set_rx_threshold_level(UINT8 level)
{
	UINT16 reg;

	if( (int)level > RX_BUFFER_DEPTH){
		level = RX_BUFFER_DEPTH;
	}
	reg = Inhw(S2E_I2C_BASE + I2C_RX_TL);
	reg &= 0xff00;		// mask bit 0~7
	reg |= level;
	Outhw(S2E_I2C_BASE + I2C_RX_TL , reg);
}

/**	\brief
 *
 *	Gets the RX threshold level.
 *
 *	\retval 		level
 */
int get_rx_threshold_level(void)
{
	UINT16 reg;

	reg = Inhw(S2E_I2C_BASE + I2C_RX_TL);
	reg &= 0xff00;		// mask bit 0~7
	return (int)reg;
}

/**	\brief
 *
 *	Issue a read process.
 *
 *	\retval 		level
 */
void i2c_issue_read(void)
{
	Outhw(S2E_I2C_BASE + I2C_DATA_CMD , 0x100);		// 1 0000 0000
}

/**	\brief
 *
 *	Reads 1 byte from RX FIFO
 *
 *	\retval 		1 byte data of RX FIFO
 */
UINT8 i2c_read(void)
{
	UINT8 retval;
	UINT16 reg;

//	i2c_issue_read();
	reg = Inhw(S2E_I2C_BASE + I2C_DATA_CMD);
	retval = reg & 0xff;

	return retval;
}

/**	\brief
 *
 *	Writes 1 byte to TX FIFO
 *
 *	\param[in]	ch : data to be write into TX FIFO
 */
void i2c_write(UINT8 ch)
{
	// test + CMD1bit
	UINT16 value = 0;
	
	value |= ch;
	//---end
	Outhw(S2E_I2C_BASE + I2C_DATA_CMD , value);
}

/**	\brief
 *
 *	S2E Only Support Master 10-bit addressing ...
 *
 *	\param[in]	mode : I2C Addressing mode , 7-bit or 10-bit
 *
 *	\retval 		error code
 */
int i2c_set_master_address_mode(i2c_address_mode mode)
{
	int retval;
	UINT16 reg;
	
	if(mode == i2c_7bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg &= ~sBIT4 ;	// clear bit4
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
		retval = 0;
	}else if( mode == i2c_10bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg |= sBIT4 ;	// set bit4
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
		retval = 0;
	}
	else{
		retval = -EPERM;
	}

	return retval;
}

/**	\brief
 *
 *	S2E Only Support Master 10-bit addressing ...
 *
 *	\retval 		I2C Addressing mode , 7-bit or 10-bit
 */
i2c_address_mode i2c_get_master_address_mode(void)
{
	int retval;
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
	if( reg &= sBIT4 ){
		retval = i2c_10bit_address;
	}else{
		retval = i2c_7bit_address;
	}

	return retval;
}

/**	\brief
 *
 *	Sets the addressing mode of I2C in slave mode.
 *
 *	\param[in]	mode : I2C Addressing mode , 7-bit or 10-bit
 *
 *	\retval 		error code
 */
int i2c_set_slave_address_mode(i2c_address_mode mode)
{
	int retval;
	UINT16 reg;
	
	if(mode == i2c_7bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg &= ~sBIT3 ;	// clear bit3
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
		retval = 0;
	}else if( mode == i2c_10bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg |= sBIT3 ;	// set bit3
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
		retval = 0;
	}
	else{
		retval = -EPERM;
	}

	return retval;
}

/**	\brief
 *
 *	Gets the addressing mode of I2C in slave mode.
 *
 *	\retval 		I2C Addressing mode , 7-bit or 10-bit
 */
i2c_address_mode i2c_get_slave_address_mode(void)
{
	int retval;
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
	if( reg &= sBIT3 ){
		retval = i2c_10bit_address;
	}else{
		retval = i2c_7bit_address;
	}
	return retval;
}

/**	\brief
 *
 *	Sets the specific address to I2C in slave mode.
 *
 *	\param[in]	slave_address : a specific address
 *
 *	\retval 		error code
 */
int i2c_set_slave_address(UINT16 slave_address)		// default value = 0x55
{
	int retval;
	UINT16 reg;	
	if((slave_address & 0x03FF) == 0x00 || 0x07 || 0x78 || 0x7F){
		return -EINVAL;		// reserved address ...
	}
	if( i2c_is_enable() == FALSE){
		retval = 0;
		slave_address &= 0x03FF;	// mask
		reg = Inhw(S2E_I2C_BASE + I2C_SAR);
		reg &= ~0x03FF ;		// clear bit 0~9
		reg |= slave_address;
		Outhw(S2E_I2C_BASE + I2C_SAR , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;	
}

/**	\brief
 *
 *
 */
int i2c_set_target_addressing_mode(i2c_address_mode addressing_mode)		
{
	int retval = 0;
	UINT16 reg;	

	if(addressing_mode == i2c_10bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_TAR);		
		reg |= sBIT12;
		Outhw(S2E_I2C_BASE + I2C_TAR , reg);
	}else if(addressing_mode == i2c_7bit_address){
		reg = Inhw(S2E_I2C_BASE + I2C_TAR);		
		reg &= (~sBIT12);
		Outhw(S2E_I2C_BASE + I2C_TAR , reg);	
	}else{
		retval = -EINVAL;
	}


	return retval;	
}

/**	\brief
 *
 *	Sets the specific address into I2C_TAR register.
 *	This value will be used when I2C sent data to target.
 *
 *	\param[in]	target_address : a specific address
 *
 *	\retval 		error code
 */
int i2c_set_target_address(UINT16 target_address)		// default value = 0x55
{
	int retval;
	UINT16 reg;	
	if((target_address & 0x03FF) == 0x00 || 
		(target_address & 0x03FF) == 0x07 || 
		(target_address & 0x03FF) == 0x78 || 
		(target_address & 0x03FF) == 0x7F){
		
		return -EINVAL;		// reserved address ...
	}
	if( i2c_is_enable() == FALSE){
		retval = 0;
		target_address &= 0x03FF;	// mask
		reg = Inhw(S2E_I2C_BASE + I2C_TAR);
		reg &= ~0x03FF ;		// clear bit 0~9
		reg |= target_address;
		Outhw(S2E_I2C_BASE + I2C_TAR , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;	
}

/**	\brief
 *
 *	set the length of serial clock count 
 *
 *	\param[in]	speed_mode : standard / fast / high
 *	\param[in]	phase : SCL clock count low/high phase
 *	\param[in]	count : count value
 *
 *	\retval 		error code
 */
int i2c_set_scl_count(i2c_speed_mode speed_mode , i2c_scl_phase phase , UINT16 count)
{
	int retval = 0;

	if( i2c_is_enable() == FALSE) {
		retval = 0;
		switch (speed_mode) {
			case i2c_speed_standard:
				if( phase == i2c_scl_low){
					Outhw(S2E_I2C_BASE + I2C_SS_SCL_LCNT , count);
				}else if(phase == i2c_scl_high){
					Outhw(S2E_I2C_BASE + I2C_SS_SCL_HCNT , count);			
				}
				else{
					retval = -EINVAL;
				}
				break;
			case i2c_speed_fast:
				if( phase == i2c_scl_low){
					Outhw(S2E_I2C_BASE + I2C_FS_SCL_LCNT , count);
				}else if(phase == i2c_scl_high){
					Outhw(S2E_I2C_BASE + I2C_FS_SCL_HCNT , count);			
				}
				else{
					retval = -EINVAL;
				}
				break;
			case i2c_speed_high:
				if( phase == i2c_scl_low){
					Outhw(S2E_I2C_BASE + I2C_HS_SCL_LCNT , count);
				}else if(phase == i2c_scl_high){
					Outhw(S2E_I2C_BASE + I2C_HS_SCL_HCNT , count);			
				}
				else{
					retval = -EINVAL;
				}
				break;
			default:
				retval = -EPERM;
				break;
		}
	}

	return retval;
}

/**	\brief
 *
 *	I2C clock setting function.
 *	It will compute the count value of three speed mode and set them into I2C count register.
 *
 *	\param[in]	ic_clk : APB clock in MHz
 *
 *	\retval 		error code
 */
int i2c_clock_setup(UINT32 ic_clk )
{

	int retval;
	UINT16 ss_scl_high = 0;
	UINT16 ss_scl_low = 0;
	UINT16 fs_scl_high = 0;
	UINT16 fs_scl_low = 0 ;
	UINT16 hs_scl_high = 0;
	UINT16 hs_scl_low = 0;
//	int user_defined = 1;

    // ic_clk is the clock speed (in MHz) that is being supplied to the
    // DW_apb_i2c device.  The correct clock count values are determined
    // by using this inconjunction with the minimum high and low signal
    // hold times as per the I2C bus specification.


/*
	if( user_defined == 0){
		ss_scl_high = ((UINT16) (((SS_MIN_SCL_HIGH * ic_clk) / 100) + 1));
		ss_scl_low = ((UINT16) (((SS_MIN_SCL_LOW * ic_clk) / 100) + 1));
		fs_scl_high = ((UINT16) (((FS_MIN_SCL_HIGH * ic_clk) / 100) + 1));
		fs_scl_low = ((UINT16) (((FS_MIN_SCL_LOW * ic_clk) / 100) + 1));
		hs_scl_high = ((UINT16) (((HS_MIN_SCL_HIGH_100PF * ic_clk) / 100) + 1));
		hs_scl_low = ((UINT16) (((HS_MIN_SCL_LOW_100PF * ic_clk) / 100) + 1));
	}else if(user_defined == 1){
		ss_scl_high = ((UINT16) (((SS_MIN_SCL_HIGH * EEPROM_20K_FACTOR * ic_clk) / 1000) + 8));
		ss_scl_low = ((UINT16) (((SS_MIN_SCL_LOW * EEPROM_20K_FACTOR * ic_clk) / 1000) + 1));
		fs_scl_high = ((UINT16) (((FS_MIN_SCL_HIGH * ic_clk) / 1000) + 8));
		fs_scl_low = ((UINT16) (((FS_MIN_SCL_LOW * ic_clk) / 1000) + 1));
		hs_scl_high = ((UINT16) (((HS_MIN_SCL_HIGH_100PF * ic_clk) / 1000) + 8));
		hs_scl_low = ((UINT16) (((HS_MIN_SCL_LOW_100PF * ic_clk) / 1000) + 1));
	}
*/

	/* 10MHz APB */
	// ss_scl_high = 39*8 ;			// by Allen Lee
	// ss_scl_low = 39*8     +   1;
	/*
		EEPROM Speed = 200KHz
		APB = 37.5MHz
		High_count + Low_count = 37.5Mhz / 200KHz
		
		---->  High_count + Low_count = 96 + 97
	*/
	/* 37.5MHz APB */
	ss_scl_high = 96  ;			
	ss_scl_low = 96 + 1;		

	retval = 0;
	retval = i2c_set_scl_count(i2c_speed_standard, i2c_scl_high, ss_scl_high);
	retval = i2c_set_scl_count(i2c_speed_standard, i2c_scl_low,ss_scl_low);
	retval = i2c_set_scl_count(i2c_speed_fast, i2c_scl_high, fs_scl_high);
	retval = i2c_set_scl_count(i2c_speed_fast, i2c_scl_low, fs_scl_low);
	retval = i2c_set_scl_count(i2c_speed_high, i2c_scl_high, hs_scl_high);
	retval = i2c_set_scl_count(i2c_speed_high, i2c_scl_low, hs_scl_low);

	return retval;
}

/**	\brief
 *
 *	Check whether the I2C chip is enabled. 
 *
 *	\retval 		enable or not
 */
BOOL i2c_is_enable()
{
	BOOL retval;
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_ENABLE);
	reg &= sBIT0;		// check bit0
	if( reg == 1){		
		retval = TRUE;
	}else{
		retval = FALSE;
	}
	return retval;
}

/**	\brief
 *
 *	Set I2C speed mode (default value = high speed)
 *
 *	\param[in]	speed_mode : Standard / Fast / High
 *
 *	\retval 		error code
 */
int i2c_set_speed_mode(i2c_speed_mode speed_mode)
{
	int retval;
	UINT16 reg;


	if( i2c_is_enable() == FALSE){
		retval = 0;
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg &= ~(sBIT1 | sBIT2)	;	// clear bit1 & bit 2
		reg |= (speed_mode << 1) ;
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;
}

/**	\brief
 *
 *	Enable restart condition
 *
 *	\retval 		error code
 */
int i2c_enable_restart(void)
{
	int retval;
	UINT16 reg;
	if( i2c_is_enable() == FALSE){
		retval = 0;
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg |= (1 << 5) ;
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;
}

/**	\brief
 *
 *	Set the I2C chip to master condition.
 *	Softwareshouldensurethatifenable_masterbitiswrittenwith
 *	"1,"thenbit6shouldalsobewrittenwitha"1".
 *
 *	\retval 		error code
 */
int i2c_enable_master(void)
{
	int retval;
	UINT16 reg;
	if( i2c_is_enable() == FALSE){
		retval = 0;
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg |= sBIT0;
		// must disable slave
		reg |= sBIT6;
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;
}

/**	\brief
 *
 *	Set the I2C chip to slave condition.
 *	Softwareshouldensurethatifenable_slavebitiswrittenwith
 *	"0,"thenbit0shouldalsobewrittenwitha"0".
 *
 *	\retval 		error code
 */
int i2c_enable_slave(void)
{
	int retval;
	UINT16 reg;
	if( i2c_is_enable() == FALSE){
		retval = 0;
		reg = Inhw(S2E_I2C_BASE + I2C_CONTROL);
		reg &= ~(sBIT6);
		// must disable master
		reg &= ~(sBIT0);
		Outhw(S2E_I2C_BASE + I2C_CONTROL , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;
}

/**	\brief
 *
 *	You should set the target address before you conduct a transfer.
 *
 *	\param[in]	tx_mode : normal / general_call / START BYTE
 *
 *	\retval 		error code
 */
int i2c_set_tx_mode(i2c_tx_mode tx_mode)
{
	int retval;
	UINT16 reg;	
	if( i2c_is_enable() == FALSE){
		retval = 0;
		tx_mode &= 0x0002;		// mask
		reg = Inhw(S2E_I2C_BASE + I2C_TAR);
		reg &= ~(sBIT10 | sBIT11);
		reg |= (tx_mode<<10);
		Outhw(S2E_I2C_BASE + I2C_TAR , reg);
	}
	else{
		retval = -EPERM;
	}
	
	return retval;
}

/**	\brief
 *
 *	Thesebitsmasktheircorrespondinginterruptstatusbitsin
 *	theIC_INTR_STATregister.
 *
 *	\param[in]	interrupt : What kind of interrupt of I2C you want to mask.
 */
void i2c_mask_irq(i2c_irq interrupt)
{
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_INTR_MASK);
	reg |= interrupt;
	Outhw(S2E_I2C_BASE + I2C_INTR_MASK , reg);
	
}

/**	\brief
 *
 *	Thesebitsunmasktheircorrespondinginterruptstatusbitsin
 *	theIC_INTR_STATregister.
 *
 *	\param[in]	interrupt : What kind of interrupt of I2C you want to unmask.
 */
void i2c_unmask_irq(i2c_irq interrupt)
{
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_INTR_MASK);
	reg &= ~interrupt;
	Outhw(S2E_I2C_BASE + I2C_INTR_MASK , reg);
	
}

/**	\brief
 *
 *	Clear interrupt pending by reading it.
 *
 *	\param[in]	interrupt : What kind of interrupt pending of I2C you want to clear.
 */
void i2c_clear_irq(i2c_irq interrupt)
{
	switch (interrupt){
		
		case i2c_irq_none:
			// no interrupt
			break;
		case i2c_irq_rx_under :
			Inhw(S2E_I2C_BASE + I2C_CLR_RX_UNDER);
			break;
		case i2c_irq_rx_over :
			Inhw(S2E_I2C_BASE + I2C_CLR_RX_OVER);
			break;			
		case i2c_irq_rx_full:
			// clear by hardware
			break;
		case i2c_irq_tx_over :
			Inhw(S2E_I2C_BASE + I2C_CLR_TX_OVER);
			break;
		case i2c_irq_tx_empty:
			// clear by hardware
			break;			
		case i2c_irq_rd_req:
			Inhw(S2E_I2C_BASE + I2C_CLR_RD_REQ);
			break;
		case i2c_irq_tx_abrt :
			Inhw(S2E_I2C_BASE + I2C_CLR_TX_ABRT);
			break;			
		case i2c_irq_rx_done :
			Inhw(S2E_I2C_BASE + I2C_CLR_RX_DONE);
			break;
		case i2c_irq_activity :
			Inhw(S2E_I2C_BASE + I2C_CLR_ACTIVITY);
			break;
		case i2c_irq_stop_det :
			Inhw(S2E_I2C_BASE + I2C_CLR_STOP_DET);
			break;		
		case i2c_irq_start_det :
			Inhw(S2E_I2C_BASE + I2C_CLR_START_DET);
			break;	
		case i2c_irq_gen_call :
			Inhw(S2E_I2C_BASE + I2C_CLR_GEN_CALL);
			break;	
		case i2c_irq_all:
			Inhw(S2E_I2C_BASE + I2C_CLR_INTR);
			break;				
		default :
			break;
	}
}

/**	\brief
 *
 *	Get the interrupt pendings of I2C
 *
 *	\retval 		interrupt pending value
 */
UINT16 i2c_get_inturrpt_status(void)
{
	UINT16 reg;
	reg = Inhw(S2E_I2C_BASE + I2C_INTR_STAT);
	return reg;
}

/**	\brief
 *
 *	It is convenient to setup the I2C chip.
 *
 *	\param[in]	i2c_type : master / slave
 *	\param[in]	address_mode : 7bits / 10bits
 *	\param[in]	speed_mode : standard / fast / high
 *	\param[in]	slave_address : target address 
 *
 *	\retval 		error code
 */
#define I2C_CLOCK 12.5		// FPGA : APB Clock 20MHz
int i2c_auto_init( i2c_master_slave_type i2c_type, 
					i2c_address_mode address_mode, 
					i2c_speed_mode speed_mode,
					UINT32 slave_address)
{
	int retval;

	retval = i2c_init();
	if(retval == 0){
		retval = i2c_clock_setup(I2C_CLOCK);
		if(retval != 0){
			Printf("i2c_clock_setup error \r\n");
			goto error_handle;
		}
		retval = i2c_set_speed_mode(speed_mode);
		if(retval != 0){
			Printf("i2c_set_speed_mode error \r\n");
			goto error_handle;
		}		
		retval = i2c_set_master_address_mode(address_mode);
		if(retval != 0){
			Printf("i2c_set_master_address_mode error \r\n");
			goto error_handle;
		}
		retval = i2c_enable_restart();
		if(retval != 0){
			Printf("i2c_enable_restart error \r\n");
			goto error_handle;
		}		
		retval = i2c_enable_master();
		if(retval != 0){
			Printf("i2c_enable_master error \r\n");
			goto error_handle;
		}		
		retval = i2c_set_tx_mode(i2c_tx_target);
		if(retval != 0){
			Printf("i2c_set_tx_mode error \r\n");
			goto error_handle;
		}			
		if(slave_address == 0){
			slave_address = I2C_SLAVE_ADDRESS;
		}
		retval = i2c_set_target_address(slave_address);
		if(retval != 0){
			Printf("i2c_set_target_address error \r\n");			
			goto error_handle;
		}
		set_tx_threshold_level(0);
//		set_rx_threshold_level(4);
		i2c_enable();
	}
	else{
		retval = -EPERM;
	}

	return retval;

error_handle:

	return retval;
}

/**	\brief
 *
 *	Write date to EEPROM by I2C serial.
 *	EEPROM can support 20KHz to 400KHz.
 *
 *	\param[in]	addr : address of EEPROM
 *	\param[in]	buf : data in SDRAM to be rx
 *
 */

int i2c_read_from_eeprom(UINT32 addr , UINT8 *buf)
{	
	ulong t;
#if EEPROM_ADDRESS_2BYTES
	UINT8 add;
#if MODULE_AIO
	if(EEPROM_address == EEPROM_WRITE){
#endif
		add  = (addr>>8) & 0xff;
		i2c_write(add);	
		t = Gsys_msec;
		while( i2c_is_tx_fifo_empty == FALSE){
			if(Gsys_msec - t > 3000){
				Printf("I2C timeout !!!\r\n");
				i2c_disable();
				return -1;
			}
		}
#if MODULE_AIO
	}else{
		i2c_disable();
		i2c_set_target_address(EEPROM_address+(addr>>8));
		i2c_enable();
	}
#endif
#else
	i2c_disable();
	i2c_set_target_address(EEPROM_address+(addr>>8));
	i2c_enable();
#endif
	addr &= 0xff;		// ADDR = 8bits		mask it.
	i2c_write(addr);	
	sleep(1);
	i2c_issue_read();
	// send read command to EEPROM
	t = Gsys_msec;
	while(   i2c_is_rx_fifo_empty() == TRUE ){		// RX FIFO Empty
		if(Gsys_msec - t > 3000){
			Printf("I2C timeout !!!\r\n");
			return -1;
		}
	}
	buf[0] = i2c_read();	
	return 0;
}

int i2c_read_from_eeprom_page(UINT32 addr , UINT8 *buf, int length){
	int i;
	ulong t;
#if EEPROM_ADDRESS_2BYTES
	UINT8 add;
#if MODULE_AIO
	if(EEPROM_address == EEPROM_WRITE){
#endif
		add  = (addr>>8) & 0xff;
		i2c_write(add);	
		t = Gsys_msec;
		while( i2c_is_tx_fifo_empty == FALSE){
			if(Gsys_msec - t > 3000){
				Printf("I2C timeout !!!\r\n");
				i2c_disable();
				return -1;
			}
		}
#if MODULE_AIO
	}else{
		i2c_disable();
		i2c_set_target_address(EEPROM_address+(addr>>8));
		i2c_enable();
	}
#endif
#else
	i2c_disable();
	i2c_set_target_address(EEPROM_address+(addr>>8));
	i2c_enable();
#endif
	addr &= 0xff;		// ADDR = 8bits		mask it.
	i2c_write(addr);	
	sleep(1);
	// send read command to EEPROM
	for( i = 0 ; i < length ; i++){
		i2c_issue_read();	// send read command to EEPROM
		t = Gsys_msec;
		while(  i2c_is_rx_fifo_empty() == TRUE){		// RX FIFO Empty
			if(Gsys_msec - t > 3000){
				Printf("I2C timeout !!!\r\n");
				return -1;
			}
		}
		*buf = i2c_read();
		buf++;
		sleep(1);
	}
	return 0;
}	

UINT8 eeprom_readb(UINT8 command,UINT16 addr){
	UINT8 data;
	ulong t;
	if(command == RAND_ADDR){
#if EEPROM_ADDRESS_2BYTES
		UINT8 add;
#if MODULE_AIO
		if(EEPROM_address == EEPROM_WRITE){ 
#endif
			add  = (addr>>8) & 0xff;
			i2c_write(add);	
			t = Gsys_msec;
			while( i2c_is_tx_fifo_empty == FALSE){
				if(Gsys_msec - t > 3000){
					Printf("I2C timeout !!!\r\n");
					return -1;
				}
			}
#if MODULE_AIO
		}else{
			i2c_disable();
			i2c_set_target_address(EEPROM_address+(addr>>8));
			i2c_enable();
		}
#endif
#else
		i2c_disable();
		i2c_set_target_address(EEPROM_address+(addr>>8));
		i2c_enable();
#endif
		addr &= 0xff;		// ADDR = 8bits		mask it.
		i2c_write(addr);	
		sleep(1);
	}
	i2c_issue_read();	// send read command to EEPROM
	t = Gsys_msec;
	while(i2c_is_rx_fifo_empty() == TRUE){		// RX FIFO Empty
		if(Gsys_msec - t > 3000){
			Printf("I2C timeout !!!\r\n");
			return -1;
		}
	}
	data = i2c_read();
	return data;
}

/**	\brief
 *
 *	Write date to EEPROM by I2C serial.
 *	EEPROM can support 20KHz to 400KHz.
 *
 *	\param[in]	addr : address of EEPROM
 *	\param[in]	buf : data in SDRAM to be tx
 *
 */

int i2c_write_to_eeprom(UINT32 addr , UINT8 buf)
{
	ulong t;
#if EEPROM_ADDRESS_2BYTES
	UINT8 add;
#if MODULE_AIO
	if(EEPROM_address == EEPROM_WRITE){
#endif
		add  = (addr >> 8) & 0xff;
		i2c_write(add);	
		t = Gsys_msec;
		while( i2c_is_tx_fifo_empty == FALSE){
			if(Gsys_msec - t > 3000){
				Printf("I2C timeout !!!\r\n");
				return -1;
			}
		}
#if MODULE_AIO
	}else{
		i2c_disable();
		i2c_set_target_address(EEPROM_address+(addr>>8));
		i2c_enable();
	}
#endif
#else
	i2c_disable();
	i2c_set_target_address(EEPROM_address+(addr>>8));
	i2c_enable();
#endif
	addr &= 0xff;		// ADDR = 8bits		mask it.
	i2c_write(addr);	
	i2c_write(buf);
	sleep(5);
	return 0;
}

/**	\brief
 *
 *	Write date to EEPROM by I2C serial.
 *	Page mode of reading in I2C is 16bytes at one time.
 *	EEPROM can support 20KHz to 400KHz.
 *
 *	\param[in]	addr : address of EEPROM
 *	\param[in]	buf : data in SDRAM to be tx
 *
 */

UINT16 i2c_get_tx_fifo_level(void)
{
	return Inhw(S2E_I2C_BASE + I2C_TXFLR);
}

int eeprom_write_page(UINT16 addr , UINT8 *data, UINT16 dataSize)
{
	ulong t1,t;
#if EEPROM_ADDRESS_2BYTES
	UINT8 add;
#if MODULE_AIO
	EEPROM_address = EEPROM_WRITE;
#endif

	add  = (addr >> 8) & 0xff;
	i2c_write(add);	
	t = Gsys_msec;
	while( i2c_is_tx_fifo_empty == FALSE){
		if(Gsys_msec - t > 3000){
			Printf("I2C timeout !!!\r\n");
			return -1;
		}
	}
#endif

	addr &= 0xff;		// ADDR = 8bits	
	i2c_write(addr);
	while( i2c_is_tx_fifo_empty == FALSE);
	t1 = Gsys_msec;
	while(dataSize)
	{
		t = Gsys_msec;
		while(i2c_get_tx_fifo_level()>=TX_FIFO_DEPTH)
		{			
			if(Gsys_msec - t > 30)//timeout in 10 ms
			{	//wait longer than 10ms, just return false
				Printf("tx fifo stocked\r\n");
				return -1;
			}
		}
		i2c_write(*data);
		data++;
		dataSize--;
		if(Gsys_msec - t1 > 3000)//timeout in 1000 ms
		{
			Printf("eeprom_write_page timeout.\r\n");		
			return -1;
		}
	}
	return 0;
}




