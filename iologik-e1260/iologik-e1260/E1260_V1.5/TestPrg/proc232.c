/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	proc232.c

	MM/MP for setting serial,MAC address protocol 

	2008-06-10	Albert Yu
		new release
	2008-10-15	Chin-Fu Yang		
		modified			
	2009-08-03	Nolan Yu		
		modified			
*/

/************************************************************************/
/* Name 	: PROC232.C													*/
/* Description	: MM/MP for setting serial,MAC address protocol 		*/
/* Product	: NP5200 series 											*/
/* Module	:			          										*/
/* Author	: Tony_Li 							    					*/
/* Date 	: 8-13-2003													*/
/*		:										     					*/
/*	 length :  1	1     1   60	    1   (fixed 64 bytes)			*/
/* Frame format : SOH  MAGIC CMD parameter EOT							*/
/************************************************************************/
#include "types.h"
#include "chipset.h"
#include "global.h"
#include "lib.h"
#include "memory.h"
#include "timer.h"
#include "mac.h"
#include "moxauart.h"
#include "gpio.h"
#include "udpburn.h"
#include "spi.h"
#include "proc232.h"
#include "i2c.h"
#include "ai.h"
#include "DIO.h"


extern int FlashromProgram(UINT32 dest, UINT32 src, long len);
extern int	diag_do_set_clean_flag(int mode,long parameter);
extern int sprintf(char * buf, const char *fmt, ...);
extern void Printf(const char *fmt, ...);

static struct frame232 Frame;

char *smptr;
int smlen;
char	MAC_GEN_Mac[6] = {0x55,0x44,0x33,0x22,0x11,0x00};


/**	\brief
 *
 *	Get a specific frame from UART
 *
 *	\retval 		error code
 */
extern void half_millisec(void);

static int get_1_frame(void)
{
	uchar *ptr;		// pointer to a character
	int len;
	frame232_t  frame;	// pointer to a frame structure
	int i;
	int led = 0;
	long t;

	frame = &Frame;	
	ptr = (uchar *)&Frame;

	len = 0;
	smlen = 0;
	t = Gsys_msec;

	while(1){
		NicProcess();
		if((Gsys_msec - t) > 500){
			t = Gsys_msec;
			led++;
			ReadyLed(led%2);
		}
//		half_millisec();
//		led++;

		if(smlen != 0){
			for(i =0;i<smlen;i++,ptr++){
				*ptr = smptr[i];
			}
			len += smlen;
			smlen =0;
		}
		 if (len >= (int)(sizeof(Frame) - 2)){	// receive the remaining characters 
			break;
	    }
	}
	ptr = (uchar *)&Frame;
	if(*ptr != CMD_232_SOH)	return 0;
	ptr++;
	if(*ptr != CMD_232_MAGIC) return 0;
	if (frame->eot != CMD_232_EOT) {	// check last symbol.
	    return(0);
	}
	return 1;
}

/**	\brief
 *
 *	Utility of UART to  change HWID, MAC address or Serial number by proc232 command
 *
 *	\param[in]	method : ID Gen / MAC & Serial
 *
 */
extern int UdpSends(ulong d_ip,ushort s_port,ushort d_port,uchar *buf,int len);
extern void	ArpInit(void);
extern int poweronstatus;
extern char EEPROM_address;
void set_serial_to_EEPROM(unsigned long serial);
void	proc_232(int method)
{
	frame232_t  frame;
	vulong serial;		// for SMC_FLASH0_SERIAL in SPI Flash
	vulong swid;
	vulong hw_exid = 0;

	vuchar m[6];			// for SMC_FLASH0_MACA in SPI Flash
	vuchar mac[20];		// for SMC_FLASH0_MACA in SPI Flash
	uchar *ptr;
	int i;
	long len;

	poweronstatus = 3; // MAC GEN_STATUS

	switch( method ){
		case 0:	/* ID Gen */
			Printf("Waitting for HW ExtID...\r\n");
			break;
		case 1: /* MAC & Serial */
			Printf("Waitting for serial and MAC address...\r\n");
			break;
	}
	
	// read data from SPI Flash to SDRAM
    mx_read_data((UINT32)&serial, SMC_FLASH0_SERIAL, 4);		
    mx_read_data((UINT32)m, SMC_FLASH0_MACA, 6);	
		
	for (i=0;i<6;i++) {
		sprintf((char*)&mac[i * 2],"%02x",m[i]);
	}
	mac[12] = 0;

	Printf("Initializing network ....\r\n");
	MemInit();		/* init memory buffer   */
	ArpInit();

	G_LanPort = 1;
	Printf("MAC init ..");
	mac_open(G_LanPort,MODE_NORMAL);
//	savedNicIp = NicIp[G_LanPort];
//	mx_read_data((UINT32)&NicIp[G_LanPort], SMC_FLAGH0_LOCALIP, 4);

	// select the UART Interface by product

	frame = &Frame;
	while (1) {
	    if (!get_1_frame()){
			continue;
	    }
		frame = &Frame;

	    switch (frame->cmd){
			
			case CMD_232_ALIVE :
		    		frame->cmd |= CMD_232_ACK;
			    	break;
			case CMD_232_SET_SERIAL :
			{
				
				Printf("Set Serial\r\n");
				ptr = (uchar *)&Frame;
				serial = (ptr[3] & 0xff) | ((ptr[4] & 0xff) << 8) | ((ptr[5] & 0xff) << 16) | ((ptr[6] & 0xff) << 24);
				Printf("Serial = %d\r\n",serial);

				frame->cmd |= CMD_232_ACK;
			    	break;
			}
			case CMD_232_SET_MAC :
			{
			    	frame->data[12] = 0;
			    	if ((IsHex(frame->data))){
						Strcpy((uchar*)mac,frame->data);
						for(i=0;i<6;i++){
						    m[i] = Atob((char*)&mac[i * 2]);
						}
						frame->cmd |= CMD_232_ACK;
				}else{
						frame->cmd |= CMD_232_NAK;
			    	}
				    	break;
			}
			case CMD_232_GET_SERIAL :
				Printf("Get Serial\r\n");
				Printf("Get serial = %d\r\n",serial);
			    frame->data[0] = serial & 0xff;
				frame->data[1] = (serial >> 8) & 0xff;
				frame->data[2] = (serial >> 16) & 0xff;
				frame->data[3] = (serial >> 24) & 0xff;
			    frame->cmd |= CMD_232_ACK;
			    break;
			case CMD_232_GET_MAC :
			    frame->cmd |= CMD_232_ACK;
			    mx_read_data((UINT32)m, SMC_FLASH0_MACA, 6);	
				for (i=0;i<6;i++) {
				    sprintf((char*)&mac[i * 2],"%02x",m[i]);
				}
				mac[12] = 0;
			    
			    Strcpy(frame->data,(uchar*)mac);
		    	break;

			case CMD_232_GET_SWID :
			    mx_read_data((UINT32)&hw_exid, SMC_FLASH0_HWEXID, 4);
				frame->data[0] = hw_exid & 0xff;
				frame->data[1] = (hw_exid >> 8) & 0xff;
				frame->data[2] = (hw_exid >> 16) & 0xff;
				frame->data[3] = (hw_exid >> 24) & 0xff;
		    	frame->cmd |= CMD_232_ACK;
		    	break;
			case CMD_232_SET_SWID :
			{
				swid = ((frame->data[3] << 24) | (frame->data[2] << 16) | (frame->data[1] << 8) | (frame->data[0]) );

				len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	            mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
				*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = swid;
				Printf("HWEX_ID = 0x%08x\r\n",swid);
				if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0)
				{
					/*Printf("Write swid fail!\r\n"); 	*/
					frame->cmd |= CMD_232_NAK;
				}
				else
				{
					/*Printf("Write swid no ok.\r\n");*/
					diag_do_set_clean_flag(1,FLAG_HWEXID);

					frame->cmd |= CMD_232_ACK;
		    	}

		    	break;
		    }
			case CMD_232_SAVE :
				Printf("Save\r\n");
				len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
				mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
				*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|SERIAL_OFFSET) = serial;
				for (i=0;i<6;i++) VPchar((SDRAM_DOWNLOAD_TEMP_BASE|MACA_OFFSET) + i) = m[i];
			//	m[5]++;
//				for (i=0;i<6;i++) VPchar((SDRAM_DOWNLOAD_TEMP_BASE|MACB_OFFSET) + i) = m[i];
				if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0)
				{
		    		frame->cmd |= CMD_232_NAK;
					/*Printf("FAIL !\r\n");*/
				}
				else
				{
//					diag_do_set_clean_flag(1,FLAG_SERIAL_MAC);
		    		frame->cmd |= CMD_232_ACK;
				}
		    	break;
			case CMD_232_COPY :
				Printf("Copy\r\n");
			    break;
			case CMD_232_RESTART :
			    frame->cmd |= CMD_232_ACK;
			    break;
			default :
			    frame->cmd |= CMD_232_NAK;
			    break;
		}
		
		UdpSends(MAC_GEN_NPORTIP,MAC_GEN_UDP,MAC_GEN_UDP,(char *)&Frame,sizeof(Frame));
		if (frame->cmd == (CMD_232_RESTART | CMD_232_ACK))
		{
#if MODULE_AIO
			i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
			EEPROM_address = PARMETER_EEPROM_WRITE;
			i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
			set_serial_to_EEPROM(sysModelInfo[board_index].serial);
#endif
			sleep(100);
			diag_do_set_clean_flag(1,FLAG_SERIAL_MAC);
			ReadyLed(OFF);
			GpioSetDir(PIO_WADG ,PIO_OUT);
			while(1);
		}
	}
}

void set_serial_to_EEPROM(unsigned long serial)
{
	UINT8 high_byte, middle_byte1, middle_byte2, low_byte;

	high_byte = (UINT8)((serial & 0xff000000) >> 24);
	middle_byte1 = (UINT8)((serial & 0x00ff0000) >> 16);
	middle_byte2 = (UINT8)((serial & 0x0000ff00) >> 8);
	low_byte =  (UINT8)(serial & 0x000000ff);
//	Printf("Serial number: high byte is %d, low byte is %d\r\n", high_byte, low_byte);

	i2c_write_to_eeprom(MODULE_SERIES,high_byte);
	sleep(1);
	i2c_write_to_eeprom(MODULE_SERIES+1,middle_byte1);
	sleep(1);
	i2c_write_to_eeprom(MODULE_SERIES+2,middle_byte2);
	sleep(1);
	i2c_write_to_eeprom(MODULE_SERIES+3,low_byte);
	sleep(1);

	low_byte=eeprom_readb(RAND_ADDR,MODULE_SERIES+3);
	sleep(10);
	middle_byte2=eeprom_readb(RAND_ADDR,MODULE_SERIES+2);
	sleep(10);
	middle_byte1=eeprom_readb(RAND_ADDR,MODULE_SERIES+1);
	sleep(10);
	high_byte=eeprom_readb(RAND_ADDR,MODULE_SERIES);
	sleep(10);

	if (serial == (high_byte << 24)+(middle_byte1 << 16)+(middle_byte2 << 8)+low_byte)
		Printf("Serial number save to EEPROM OK!\r\n");
	else
	{
		Printf("Serial number save to EEPROM Error!\r\n");
	}
}
