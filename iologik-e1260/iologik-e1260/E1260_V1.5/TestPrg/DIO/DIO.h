#define DI_channel	8
#define DO_channel	4

#if E1242	
	void diag_do_DI_Read_test(void);
	void diag_do_SPI_DIO_Burn_in_test(void);
	void diag_do_SPI_DIO_Loop_Back_test(void);
	void diag_do_DO_Pulse_test (void);
//	void diag_do_DIO_EMS_test (UINT8 DO_DATA);
	void diag_do_DIO_EMS_test (void);
//	UINT16 diag_do_SPI_E1212_MP_DI_test (void);
//	void diag_do_SPI_E1212_MP_DO_test(UINT8 DO_DATA);
//	void diag_do_E1212_EMI_scan_test(void);
	void diag_do_SPI_DIO_55AA_test(void);
	void diag_do_SPI_DIO_00FF_test(void);
	void diag_do_SPI_DIO_Manual_test(void);
	void diag_do_SPI_DO_Loop_test(void);
//	void diag_do_SPI_DO_1KHz_test(void);
//	void diag_do_E1214_EMI_scan_test(void);
//	void diag_do_SPI_E1211_MP_DO_test(UINT16 DO_DATA);
//	void diag_do_SPI_DIO_Burn_in_test(void);
	UINT8 DIO_TYPE_READ (void);
	void SPI_DIO_init (void);
	void SPI_DIO_disable (void);
#endif
