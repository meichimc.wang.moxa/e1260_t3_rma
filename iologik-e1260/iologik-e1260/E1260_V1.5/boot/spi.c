/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    spi.c

    Routines for accessing SPI flash

    2008-06-10	Albert Yu
		new release
*/

/*! \file
\ingroup grpBoot
Initializes SPI controller and contains routines for accessing SPI Flash.
*/

/*! \page spi_ctrl SPI Controller Introduction
\image html SPI_block.gif

Input data is latched on the rising edge of Serial Clock (SCLK) and data shifts out
on the falling edge of SCLK. Two serial modes 0 & 3 are supported, as shown in the
following figure.

\image html serial_mode.gif

CPOL indicates clock polarity of Serial master, CPOL=0 for SCLK low while not transmitting,
CPOL=1 for SCLK high while idle. CPHA indicates clock phase, CPHA=0 means SCLK toggles
in middle of first data bit, CPHA=1 means SCLK toggles at start of first data bit.
In SPI controller, Serial mode3 is selected as default mode.

\section spi_init SPI Controller Initialization
<OL>
<LI>Disable DW_apb_ssi by writing 0 to SSI Enable Register.</LI>
<LI>Disable all interrupts by writing 0 to Interrupt Mask Register (IMR).</LI>
<LI>Program Control Register 0 to set control frame size to 8 bits (CFS=7),
transfer mode to Transmit & Receive (TMOD=0), serial clock polarity (SCPOL=1),
serial clock phase (SCPH=1), frame format to Motorola SPI (FRF=0),
and data frame size to 8 bits (DFS=7).</LI>
<LI>Set SCKDV=2 in Baud Rate Select Register. (The frequency of output bit rate clock =
The frequency of APB clock/SCKDV. The APB clock is AHB_clock/2).</LI>
</OL>

\page spi_mx_read SPI Flash Read
\image html SPI_read.gif

The read instruction is for reading data out. The address is latched on rising edge of SCLK,
and data shifts out on the falling edge of SCLK. The first address byte can be at any location.
The address is automatically increased to the next higher address after each byte data is shifted
out, so the whole memory can be read out at a single READ instruction.\n
To end READ operation, use CS# to high at any time during data out.
\section spi_mx_read_poll Polling Mode
<OL>
<LI>Program Control Register 0 to set transfer mode to Transmit & Receive (TMOD=0).</LI>
<LI>Write 1 to SSI Enable Register.</LI>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write four data frames to transmit FIFO (Data Register), 8-bit opcode (0x03) followed by 8-bit
upper address, 8-bit middle address and 8-bit lower address.</LI>
<LI>Polling Receive FIFO Level Register, which contains the number of valid data entries in the
receive FIFO. Read out the valid data entries, and store them to the desired SDRAM address.
Repeat the data polling procedure until the desired amount of data are all read out.</LI>
<LI>Write 0 to SSI Enable Register. (Disable SPI controller, also clear TX/RX FIFO)</LI>
</OL>
\section spi_mx_read_interrupt Interrupt Mode

<B>Global variables:\n</B>

RX_DONE: indicates the completion of receiving data, set by ISR.\n
RX_CNT: received data count, increased by ISR.\n
RX_LEN: required data length.\n

<B>Setup:\n</B>
<OL>
<LI>Enable the IRQ interrupt for SPI by setting the system interrupt controller, and register
the ISR for SPI. RX_DONE=1, RX_CNT=0, RX_LEN required length of data to be read.</LI>
<LI>Program Control Register 0 to set transfer mode to Transmit & Receive (TMOD=0).</LI>
<LI>Write Control Register 1 with the number of frames in the transfer minus 1;
	for example, write 65535 to this register if we want to receive 64K bytes.</LI>
<LI>Writing 15 to Receive FIFO Threshold Level Register. (16 bytes threshold for 32 entry FIFO)
<LI>Enable Receive FIFO Full Interrupt and Receive FIFO Overflow Interrupt by setting the
Interrupt Mask Register.</LI> 
<LI>Writing 1 to SSI Enable Register.</LI>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write four data frames to transmit FIFO (Data Register), 8-bit opcode (0x03) followed by
8-bit upper address, 8-bit middle address and 8-bit lower address.</LI>
<LI>Polling the RX_DONE until it is set to 1 by ISR.</LI>
<LI>Write 0 to SSI Enable Register. (Disable SPI controller, also clear TX/RX FIFO)</LI>
</OL>

<B>ISR:\n</B>
<OL>
<LI>Read Interrupt Status Register.</LI>
<LI>Read Interrupt Clear Register to clear interrupt.</LI>
<LI>If Receive FIFO Full Interrupt Status=1, read 16 bytes data from receive FIFO. Store the data to the desired SDRAM address.</LI>
<LI>RX_CNT += 16.</LI>
<LI>If (RX_CNT >= RX_LEN), disable the interrupt, and set RX_DONE=1.</LI>
</OL>

\page spi_mx_erase SPI Flash Sector (4KB) Erase
\anchor spi_mx_wren \image html SPI_wren.gif

\anchor spi_mx_erase \image html SPI_erase.gif

\anchor spi_mx_rdsr \image html SPI_rdsr.gif

<OL>
<LI>Program Control Register 0 to set transfer mode to Transmit & Receive (TMOD=0).</LI>
</OL>
<B>\ref spi_mx_wren "// Setting Write Enable Latch"</B>
<OL start=2>
<LI>Write 1 to SSI Enable Register.</LI>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x06 to transmit FIFO (Data Register).</LI>
<LI>Polling Transmit FIFO Empty bit in Status Register until it's set to 1.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_erase "// Issue Sector Erase command"</B>
<OL start=7>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write four data frames to transmit FIFO (Data Register), 8-bit opcode (0x20) followed by 8-bit upper address, 8-bit middle address and 8-bit lower address.</LI>
<LI>Polling Transmit FIFO Empty bit in Status Register until it's set to 1.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_rdsr "// Check the flash's Status Register till ready or time out"</B>
<OL start=11>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x05 to transmit FIFO (Data Register).</LI>
<LI>Polling Receive FIFO Not Empty bit in Status Register until it's set to 1.</LI>
<LI>Read out one byte flash's Status Register data in Receive FIFO.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
<LI>If WIP (write in progress bit) in flash's Status Register is 1, and not time out, go to step 11.</LI>
</OL>

\page spi_mx_program SPI Flash Page (256 bytes) Program
As shown in the following MX series SPI Flash block diagram,
there is Page Buffer where you can temporarily store the data
before they are actually wrote to the Memory Array.
The size of Page Buffer is 256 bytes, but it does not mean
the unit of each write operation is 256 bytes. You can write
any number of bytes between 1 and 256 to the Page Buffer,
and only these bytes given by you will overwrite Memory Array,
not all 256 bytes in Page Buffer.
The data stored in the Page Buffer are transfered to Memory Array
when CS# goes high.

\anchor spi_mx_block \image html mx_flash.gif

\anchor spi_mx_wren \image html SPI_wren.gif

\anchor spi_mx_pp \image html SPI_program.gif

\anchor spi_mx_rdsr \image html SPI_rdsr.gif

\section spi_mx_program_poll Polling Mode
<OL start=1>
<LI>Program Control Register 0 to set transfer mode to Transmit & Receive (TMOD=0).</LI>
</OL>
<B>\ref spi_mx_wren "// Setting Write Enable Latch"</B>
<OL start=2>
<LI>Write 1 to SSI Enable Register.</LI>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x06 to transmit FIFO (Data Register).</LI>
<LI>Polling Transmit FIFO Empty bit in Status Register until it's set to 1.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_pp "// Issue Page Program command"</B>
<OL start=7>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write four data frames to transmit FIFO (Data Register), 8-bit opcode (0x02) followed by 8-bit upper address, 8-bit middle address and 8-bit lower address.</LI>
</OL>
<B>\ref spi_mx_pp "// Send 256 bytes data (or less)"</B>
<OL start=9>
<LI>Read Transmit FIFO Level Register.</LI>
<LI>Write (32 - Tx_FIFO_Level) bytes data to transmit FIFO (Data Register).</LI>
<LI>Repeat step 9-10 until to be programmed data (256 or less) are all sent out.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_rdsr "// Check the flash's Status Register till ready or time out"</B>
<OL start=13>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x05 to transmit FIFO (Data Register).</LI>
<LI>Polling Receive FIFO Not Empty bit in Status Register until it's set to 1.</LI>
<LI>Read out one byte flash's Status Register data in Receive FIFO.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
<LI>If WIP (write in progress bit) in flash's Status Register is 1, and not time out, go to step 13.</LI>
</OL>

\section spi_mx_program_interrupt Interrupt Mode

<B>Global variables:\n</B>

RX_DONE: indicates the completion of receiving data, set by ISR.\n
RX_CNT: received data count, increased by ISR.\n
RX_LEN: required data length.\n

<B>Setup:\n</B>
<OL>
<LI>Enable the IRQ interrupt for SPI by setting the system interrupt controller, and register
	the ISR for SPI. RX_DONE=1, RX_CNT=0, RX_LEN required length of data to be read.</LI>
<LI>Program Control Register 0 to set transfer mode to Transmit & Receive (TMOD=0).</LI>
<LI>Writing 15 to Receive FIFO Threshold Level Register. (16 bytes threshold for 32 entry FIFO)
<LI>Enable Receive FIFO Full Interrupt and Receive FIFO Overflow Interrupt by setting the
	Interrupt Mask Register.</LI> 
</OL>
<B>\ref spi_mx_wren "// Setting Write Enable Latch"</B>
<OL start=5>
<LI>Write 1 to SSI Enable Register.</LI>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x06 to transmit FIFO (Data Register).</LI>
<LI>Polling Transmit FIFO Empty bit in Status Register until it's set to 1.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_pp "// Issue Page Program command"</B>
<OL start=10>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write four data frames to transmit FIFO (Data Register), 8-bit opcode (0x02) followed by 8-bit upper address, 8-bit middle address and 8-bit lower address.</LI>
</OL>
<B>\ref spi_mx_pp "// Trigger ISR"</B>
<OL start=12>
<LI>Write 32 bytes data transmit FIFO (Data Register).</LI>
<LI>Polling the TX_DONE until it is set to 1 by ISR.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
</OL>
<B>\ref spi_mx_rdsr "// Check the flash's Status Register till ready or time out"</B>
<OL start=15>
<LI>Select slave 0 (SPI flash) by writing 1 to the SER Register.</LI>
<LI>Write 0x05 to transmit FIFO (Data Register).</LI>
<LI>Polling Receive FIFO Not Empty bit in Status Register until it's set to 1.</LI>
<LI>Read out one byte flash's Status Register data in Receive FIFO.</LI>
<LI>Unselect slave 0 by writing 0 to the SER Register.</LI>
<LI>If WIP (write in progress bit) in flash's Status Register is 1, and not time out, go to step 13.</LI>
</OL>

<B>ISR:\n</B>
<OL>
<LI>Read Interrupt Status Register.</LI>
<LI>Read Interrupt Clear Register to clear interrupt.</LI>
<LI>If Transmit FIFO Empty Interrupt Status=1, write 16 (or the last remainder) bytes data to transmit FIFO (Data Register).</LI>
<LI>RX_CNT += 16 (or the last remainder).</LI>
<LI>If (TX_CNT == TX_LEN), disable the interrupt, and set TX_DONE=1.</LI>
</OL>

*/

#include	"types.h"
#include    "chipset.h"
#include	"spi_reg.h"
#include	"spi.h"

#define SPI_WAIT_MAX_CNT	10000

int spi_wait_TX_FIFO_empty(void)
{
	int i, ret = -1;

	for (i = 0; i < SPI_WAIT_MAX_CNT; i++)
	{
		if (SPI_STATUS() & SPI_STATUS_TX_FIFO_EMPTY) /* TX FIFO Empty */
		{
			ret = 0;
			break;
		}
	}

	return ret;
}

/*
 * Before you issue a new SPI command, you must wait until the previous command transfer is done,
 * or the previous command transfer will be interrupted and not be executed.
 */
void spi_wait_not_busy(void)
{
	int i;

	for (i = 0; i < SPI_WAIT_MAX_CNT; i++)
	{
		if (!SPI_IS_BUSY())
			break;
	}
}

/**	\brief	Read status register of MX SPI flash
 *
 *	Issue Read Status Register command to MX SPI flash and get result.
 *
 *	\return	value of status register
 */
int mx_read_status_reg(void)
{
	int ret = -1, i;

	SPI_DISABLE();
	SPI_SET_MODE(0x73C7);		/* EEPROM Read mode */
	SPI_SET_EEPROM_READ_CNT(0);	/* read 1 byte */
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_RDSR);	/* issue Read Status Register command */
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	
	/* get result */
	for (i = 0; i < SPI_WAIT_MAX_CNT; i++)
	{
		if (SPI_RX_FIFO_ENTRIES() >= 1)
		{
			ret = SPI_READ_DATA();
			break;
		}
	}

	SPI_DISABLE_FLASH();
	SPI_DISABLE();

	return ret;
}

/**	\brief	Write status register of MX SPI flash
 *
 *	Issue Write Status Register command to MX SPI flash.
 *
 *	\param[in]	value   Assign the value to be wrote to status register.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_write_status_reg(int value)
{
	int ret;

	SPI_ENABLE();
	SPI_WRITE_DATA(MX_WRSR);		/* issue Write Status Register command */
	SPI_WRITE_DATA(value);			/* the value to be written to Status Register */
	SPI_ENABLE_FLASH();				/* CS0# low */
	ret = spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();			/* wait until not busy */
	SPI_DISABLE_FLASH();
	SPI_DISABLE();

	return ret;
}

/**	\brief	Read data from MX SPI flash
 *
 *	Read data from MX SPI flash and write them to SDRAM.
 *
 *	\param[in]	sdramDestAddr	The SDRAM destination address to which the data is write.
 *	\param[in]	flashSrcAddr	The SPI flash source address from which the data is read.
 *	\param[in]	len             The length of data to be read.
 *
 *	\return	length of data be read
 */
int mx_read_data(UINT32 sdramDestAddr, UINT32 flashSrcAddr, int len);

void mx_write_enable(void)
{
	int status;
	
	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);		/* Transmit Only mode */
	SPI_ENABLE();
	SPI_WRITE_DATA(MX_WREN);	/* issue Write Enable command */
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */	
	spi_wait_not_busy();		/* wait until not busy */
	SPI_DISABLE_FLASH();
	SPI_DISABLE();
	
	while (1)
	{
		status = mx_read_status_reg();
		if (status & MX_STATUS_BIT_WEL)
			break;
	}
}

int mx_check_write_ok(int timeout)
{
	int i, status, ret = -1;

	i = 0;
	while (1)
	{
		status = mx_read_status_reg();
		if (status == -1)
			break;
		/* WIP may not be 1 at early beginning, so we check WEL */	
		if (!(status & MX_STATUS_BIT_WEL))	/* WEL will clear after write completion */
		{
			ret = 0;
			break;
		}
		if (timeout > 0)
		{
			if (++i > timeout)
				break;
		}
	}

	return ret;
}

static int mx_issue_erase_command(int command, UINT32 flashAddr)
{
	int ret, i;
	
	mx_write_enable();			/* Setting Write Enable Latch bit */

	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);		/* Transmit Only mode */

	SPI_ENABLE();
	SPI_WRITE_DATA(command);		/* issue Sector Erase command */
	if (command != MX_CE)
	{
		for (i = 2; i >= 0; i--)	/* write address from MSB to LSB */
			SPI_WRITE_DATA((flashAddr >> (i * 8)) & 0xFF);
	}
	SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();		/* wait until not busy */
	// spi_wait_not_busy() is a must, or command transfer will be interrupt by the following SPI instruction

	ret = mx_check_write_ok(0);

	SPI_DISABLE_FLASH();
	SPI_DISABLE();

	return ret;	
}

/**	\brief	Erase a sector
 *
 *	Erase a sector of MX SPI flash. Each secotr contains 4K bytes of data.
 *
 *	\param[in]	flashAddr	Assign the sector address to be erased.
 *                          It can be any address in the flash,
 *                          but only address bits [A23-A12] select the sector address.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_sector_erase(UINT32 flashAddr)
{
	return mx_issue_erase_command(MX_SE, flashAddr);
}

/**	\brief	Program a page
 *
 *  There is Page Buffer (256 bytes) where you can temporarily store the data before
 *	they are actually wrote to the Memory Array (see \ref spi_mx_program).
 *  You can read 256 bytes of data or less from SDRAM and write them to a page of MX SPI flash.
 *  \li \c [Note] Before you program a page, you must call mx_sector_erase() to erase the sector
 *  at which the page is located.
 *
 *	\param[in]	flashDestAddr	The SPI flash destination address to which the data is write.
 *	\param[in]	sdramSrcAddr	The SDRAM source address from which the data is read.
 *	\param[in]	len             The length of data to be programmed. <B> Range is 1 ~ 256. </B>
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_page_program(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len)
{
	int i, ret, first_fill_len, short_write;
	extern void spi_fill_tx_fifo(UINT32 sdramSrcAddr, int len);
	
	mx_write_enable();			/* Setting Write Enable Latch bit */

	SPI_DISABLE();
	SPI_SET_MODE(0x71C7);		/* Transmit Only mode */

	if (len <= (SPI_FIFO_SIZE - 4))
	{
		first_fill_len = len;
		short_write = 1;
	}
	else
	{
		first_fill_len = SPI_FIFO_SIZE - 4;
		short_write = 0;		
	}

	SPI_ENABLE();
	SPI_WRITE_DATA(MX_PP);		/* issue Page Program command */
	for (i = 2; i >= 0; i--)	/* write 3-byte address from MSB to LSB */
		SPI_WRITE_DATA(flashDestAddr >> (i * 8));
	for (i = 0; i < first_fill_len; i++)	/* write first 28 bytes data to fill the TX FIFO */
		SPI_WRITE_DATA(VPchar(sdramSrcAddr++));

	if (short_write)
	{
		SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
	}
	else
	{
		extern void printSpiAddr(void);
		len -= (SPI_FIFO_SIZE - 4);
		#ifdef TX_ISR_MODE
		gSpiSdramSrcAddr = sdramSrcAddr;
		gSpiSdramEndAddr = sdramSrcAddr + len;
		SPI_ENABLE_TX_INT();		
		SPI_ENABLE_FLASH();			/* CS0# low to start transmission */
		while (1)
		{	/* wait for completion of write */
			if (gSpiSdramSrcAddr == gSpiSdramEndAddr)
				break;
		}
		#else
		/* fill remaining data to TX FIFO must be in assembly to catch up the serial transfer */
		/* CS0# low to start transmission is set in spi_fill_tx_fifo() */
		spi_fill_tx_fifo(sdramSrcAddr, len);
		#endif
	}
	
	spi_wait_TX_FIFO_empty();	/* wait TX FIFO empty */
	spi_wait_not_busy();		/* wait until not busy */
	SPI_DISABLE_FLASH();
	SPI_DISABLE();				/* also clear TX/RX FIFO */
	#ifdef TX_ISR_MODE
	SPI_DISABLE_TX_INT();
	#endif

	ret = mx_check_write_ok(10000);

	return ret;	
}

/**	\brief	Write data to MX SPI flash
 *
 *	Read data from SDRAM and write them to MX SPI flash.
 *
 *	\param[in]	flashDestAddr	The SPI flash destination address to which the data is write.
 *	\param[in]	sdramSrcAddr	The SDRAM source address from which the data is read.
 *	\param[in]	len             The length of data to be wrote.
 *
 *	\retval 0   OK
 *  \retval -1  FAIL due to command timeout
 */
int mx_write_data(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len)
{
    int wlen, ret = 0;

    while (len > 0)
    {
    	if (len > MX_PAGE_SIZE)
			wlen = MX_PAGE_SIZE;
		else
			wlen = len;
        ret = mx_page_program(flashDestAddr, sdramSrcAddr, wlen);
        if (ret < 0)
            break;
        flashDestAddr += wlen;
        sdramSrcAddr += wlen;
        len -= wlen;
    }

    return ret;
}
