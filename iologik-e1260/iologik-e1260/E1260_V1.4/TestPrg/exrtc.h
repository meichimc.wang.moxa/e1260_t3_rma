/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	exrtc.h

	RTC HT1380/HT1381
	The header file of HT1380/HT1381 register definition.
	
	2008-06-10	Chin-Fu Yang		
		new release
 	2008-09-25	Chin-Fu Yang		
		modified	
		
*/

#ifndef	_EXRTC_H
#define _EXRTC_H


/*------------------------------------------------------ Macro / Define ----------------------------*/
#define RTC_PROTECT_W	0x8E
#define RTC_PROTECT_R	0x8F
#define RTC_YEAR_W		0x8C
#define RTC_YEAR_R		0x8D
#define RTC_DAY_W		0x8A
#define RTC_DAY_R		0x8B
#define RTC_MONTH_W	0x88
#define RTC_MONTH_R		0x89
#define RTC_DATE_W		0x86
#define RTC_DATE_R		0x87
#define RTC_HOURS_W	0x84
#define RTC_HOURS_R		0x85
#define RTC_MINUTES_W 	0x82
#define RTC_MINUTES_R 	0x83
#define RTC_SECONDS_W 	0x80
#define RTC_SECONDS_R 	0x81

#define RTC_DATA_IN		0
#define RTC_DATA_OUT	1


/*------------------------------------------------------ Structure ----------------------------------*/
/*
struct rtc_time {
	int	t_sec;		// 00 - 59
	int	t_min;		// 00 - 59
	int	t_hour; 		// 00 - 23
	int	t_mday; 		// 01 - 31
	int	t_mon;		// 01 - 12
	int	t_year; 		// 2000 - 2099
	int	t_wday; 		// 01 - 07
};
*/
/*------------------------------------------------------ Extern / Function Declaration ------------------*/
extern uchar const sys_daysofmonth[];
extern uchar *const DayString[];

/*
void RTCTransferStart(void);	//	Disable RTC
void RTCTransferStop(void);		// Disable RTC
void RTCWriteRegister(uchar data);
uchar RTCReadRegister(void);
void RTCWrite(uchar command, uchar data);
uchar RTCRead(uchar command);
void ex_rtc_Init(void);
void	ex_rtc_gettime(struct rtc_time *timep);
void	ex_rtc_settime(struct rtc_time *timep);
int	ex_rtc_get_day_of_week(struct rtc_time *timep);
*/


#endif /* _RTC_H */