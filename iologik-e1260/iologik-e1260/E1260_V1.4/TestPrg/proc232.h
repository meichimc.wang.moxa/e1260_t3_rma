/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    proc232.h

    Header file of proc232.c

    2008-10-15	Chin-Fu Yang
		new release
*/

#ifndef _PROC232_H
#define _PROC232_H





/*------------------------------------------------------ Macro / Define -----------------------------*/
#define CMD_232_SOH    	0x01
#define CMD_232_EOT    	0xFF
#define CMD_232_MAGIC	0x04
#define CMD_232_ACK     	0x80
#define CMD_232_NAK     	0x40

#define CMD_232_ALIVE	    	0x00
#define CMD_232_SET_SERIAL	0x01
#define CMD_232_SET_MAC	0x02
#define CMD_232_GET_SERIAL	0x03
#define CMD_232_GET_MAC	0x04
#define CMD_232_GET_SWID	0x05
#define CMD_232_SET_SWID	0x06
#define CMD_232_COPY	    	0x3D
#define CMD_232_SAVE	    	0x3E
#define CMD_232_RESTART	0x3F

#define Port232 1

#define MAC_GEN_IP	0x027FA8C0
#define MAC_GEN_NPORTIP	0xFE7FA8C0
#define MAC_GEN_UDP	4001

/*------------------------------------------------------ Structure ----------------------------------*/
struct frame232 {
	uchar	soh;
	uchar	magic;
	uchar	cmd;
	uchar	data[60];
	uchar	eot;
}__attribute__((packed));
typedef struct frame232 *frame232_t;



/*------------------------------------------------------ Extern / Function Declaration -----------------*/










#endif

