/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	ip.h

	Header file of ip program
			
	2008-09-25	Chin-Fu Yang		
		new release
	2008-10-15	Chin-Fu Yang		
		modified		
    
*/


#ifndef _IP_H
#define _IP_H


/*------------------------------------------------------ Macro / Define -----------------------------*/
#define DIP_ICMP	    1
#define DIP_TCP 	    6
#define DIP_UDP 	    17
#define CONSOLE_UDP	0x0404C
//#define CONSOLE_UDP	0x0405
//#define AP_UDP		0x0404
#define AP_UDP		0x0404A

#define ICMP_ECHOREPLY		0	/* Echo Reply			*/
#define ICMP_DEST_UNREACH	3	/* Destination Unreachable	*/
#define ICMP_SOURCE_QUENCH	4	/* Source Quench		*/
#define ICMP_REDIRECT		5	/* Redirect (change route)	*/
#define ICMP_ECHO		8	/* Echo Request 		*/
#define ICMP_TIME_EXCEEDED	11	/* Time Exceeded		*/
#define ICMP_PARAMETERPROB	12	/* Parameter Problem		*/
#define ICMP_TIMESTAMP		13	/* Timestamp Request		*/
#define ICMP_TIMESTAMPREPLY	14	/* Timestamp Reply		*/
#define ICMP_INFO_REQUEST	15	/* Information Request		*/
#define ICMP_INFO_REPLY 	16	/* Information Reply		*/
#define ICMP_ADDRESS		17	/* Address Mask Request 	*/
#define ICMP_ADDRESSREPLY	18	/* Address Mask Reply		*/

#define MINTCPHLEN  20

#define TCP_FLAG_URG	0x20
#define TCP_FLAG_ACK	0x10
#define TCP_FLAG_PSH	0x08
#define TCP_FLAG_RST	0x04
#define TCP_FLAG_SYN	0x02
#define TCP_FLAG_FIN	0x01

#define UDPHLEN (sizeof(udpinfo))


/*------------------------------------------------------ Structure ----------------------------------*/
struct ipinfo {
	uchar	ver;
	uchar	service;
	ushort	len;
	ushort	id;
	ushort	frag;
	uchar	ttl;
	uchar	protocol;
	ushort	sum;
	ulong	s_ip;
	ulong	d_ip;
} __attribute__((packed));
typedef struct ipinfo *ipinfo_t;

struct icmpinfo {
	uchar	type;
	uchar	code;
	ushort	sum;
};
typedef struct icmpinfo *icmpinfo_t;

struct udp_info {
	ushort	s_port;
	ushort	d_port;
	ushort	len;
	ushort	sum;
};
typedef struct udp_info udpinfo;
typedef struct udp_info *udpinfo_t;

struct tcpinfo {
	ushort	s_port;
	ushort	d_port;
	ulong	ser;
	ulong	ack;
	uchar	data;
	uchar	flag;
	ushort	win;
	ushort	sum;
	ushort	urgent;
	uchar	opt;
	uchar	opt_len;
};
typedef struct tcpinfo *tcpinfo_t;



/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void	IpReceive(msgstr_t msg);
void	IcmpReceive(msgstr_t msg);
int IcmpPing(void);
void	IcmpStartPing(ulong ip);
void	IcmpStopPing(void);
void	UdpReceive(msgstr_t msg);
int	UdpSend(ulong d_ip,ushort s_port,ushort d_port,uchar *buf,int len);
ulong  inet_addr(char *ptr);


#endif
