/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	MPtester.c

	For communicate MP tester 

	2009-05-12	Nolan Yu
		new release
*/

#include "types.h"
#include "chipset.h"
#include "global.h"
#include "lib.h"
#include "memory.h"
#include "timer.h"
#include "mac.h"
#include "moxauart.h"
#include "gpio.h"
#include "udpburn.h"
#include "spi.h"
#include "proc232.h"
#include "i2c.h"
#include "mptester.h"

extern void Printf(const char *fmt, ...);

void mptester_communication_init(void){
	UINT8 mode = RS232_MODE;

	m_sio_init();
	m_sio_open(mp_port,0);

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);

	m_sio_lctrl(mp_port,DTR_ON|RTS_ON);
	m_sio_setopmode(mp_port,mode);
}

void mptester_communication_stop(void){
	m_sio_close(mp_port);	
}

UINT8 transmit_MPtester(UINT8* transmit_data,UINT8 length, UINT8* receive_data){
	int len,i,j;
	unsigned int t;	
	UINT8 ch,tr;
	tr=1;
	j=0;

	t = Gsys_msec;
	while(1) {
		if(Gsys_msec - t > 2000){
			Printf("Time out");
			return mp_time_out;//break;
		}
		if ((len = m_sio_iqueue(mp_port)) > 0) {
			for (i=0;i<len;i++){
				m_sio_read(mp_port,&ch,1);
				receive_data[j]=ch;
//				Printf("\r\n rec = %02x",receive_data[j]);
				if(ch == 0x0a && receive_data[(j-1)] == 0x0d)
					return 0;
				j++;
			}			
		}
		if(tr){
			m_sio_write(mp_port, transmit_data, length);
			while(m_sio_oqueue(mp_port)!=0);
			tr=0;
		}
		sleep(1);
	}
}

UINT8 tr_mptester(UINT8 parameter,UINT16* channel){
	UINT8 command[9] = {0,0,0,0,0,0,0,0x0D,0x0A};
	UINT8 data[9];
	int i; 

	switch	(parameter){
	case MP_DO:	command[0] = 'D'; command[1] = 'O';	break;
	case MP_RY: command[0] = 'R'; command[1] = 'Y';	break;
	case MP_LD: command[0] = 'L'; command[1] = 'D';	break;
	case MP_GI:	command[0] = 'G'; command[1] = 'I';	break;
	case MP_GO:	command[0] = 'G'; command[1] = 'O';	break;
	default:
		Printf(command);
		return mp_command_fail;
		break;		
	}
	for (i=3;i>=0;i--){
		command[2+i] = *channel & 0xf;
		*channel >>= 4;
		if(command[2+i] > 9)	command[2+i] += 0x37;
		else	command[2+i] += 0x30;
	}
	command[6] = (command[0] ^ command[1]) ^ (command[2] ^ command[3])^ (command[4] ^ command[5]);
	if(transmit_MPtester(command,9,data))	return mp_time_out;
	if(data[6] == ((data[0] ^ data[1]) ^ (data[2] ^ data[3]) ^ (data[4] ^ data[5]))){
		switch(parameter){
		case MP_GI:	
			*channel=0;
			for(i=0;i<4;i++){
				*channel <<= 4;
				if(data[2+i] > '9')	*channel |= (data[2+i] -0x37);
				else	*channel |= (data[2+i] - 0x30);
			}
			break;
		default:	if(data[4] == 'O' && data[5] =='K'){}
			else	return mp_NG;
					break;
		}
		return mp_tran_OK;
	}
	else		
		return mp_tran_Fail;
}

UINT8 mptest_DO(UINT16 parameter){
	if(parameter > 0xf)
		return mp_command_fail;
	return tr_mptester(MP_DO,&parameter);
}

UINT8 mptest_relay(UINT16 parameter){
	if(parameter > 0xff)
		return mp_command_fail;
	return tr_mptester(MP_RY,&parameter);
}

UINT8 mptest_led(UINT16 parameter){
	if(parameter > 0xf)
		return mp_command_fail;
	return tr_mptester(MP_LD,&parameter);
}

UINT8 mptest_GPIO_input(UINT16* parameter){
	return tr_mptester(MP_GI,parameter);
}

UINT8 mptest_GPIO_output(UINT16 parameter){
	return tr_mptester(MP_GO,&parameter);
}



