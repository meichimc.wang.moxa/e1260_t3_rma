/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	tftp.c

	Routines for implementing TFTP protocl

	2008-06-10	Albert Yu
		new release
	2008-10-15	Chin-Fu Yang		
		modified		
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "timer.h"
#include "memory.h"
#include "lib.h"
#include "tftp.h"
#include "global.h"
#include "spi.h"
#include "console.h"
#include "mac.h"
#include "arp.h"
#include "ip.h"


extern UINT8 diag_check_press_ESC(void);
extern int FlashromProgram(UINT32 dest, UINT32 src, long len);


static int tftp_complete_flag = 0;
ulong tftp_file_length = 0;
ulong serverip = 0;
ulong localip = 0;


static struct {
	unsigned short 	last_good_block;
	char 			data[SEGSIZE+sizeof(struct tftphdr)];
	unsigned long 		offset;
} tftp_stream;





/**	\brief
 *
 *	Init a TFTP protocol.
 *	1. Init ARP
 *	2. Open MAC
 *	3. Init TFTP Stream parameter.
 *
 *	\param[in]	port : MAC port
 *
 *	\retval 		error code
 */
int tftp_start(void)
{
	ulong t,t1;

	/* Get  the server IP and local IP from SPI flash*/
	mx_read_data((UINT32)&serverip, SMC_FLAGH0_SERVERIP, 4);
	mx_read_data((UINT32)&localip, SMC_FLAGH0_LOCALIP, 4);
		
	tftp_file_length = 0;
	
	Printf("\r\nInitializing network ...");

	ArpInit();
	
	/* Open Ethernet Port 0 */
	if (mac_open(G_LanPort, MODE_NORMAL) == -1){
		return -1;
	}
	
//	NicIp[0] = localip;	// store the local ip get from SPI flash .
	NicIp[1] = localip;	// store the local ip get from SPI flash .
		
	Printf("OK\r\n");		// Initializing network ... OK

	t = t1 = Gsys_msec;

	ArpSearch(serverip);		// If no ARP record of server IP in ARP Table , this function will get it.
	tftp_complete_flag = 0;

	
	Printf("Search TFTP Server...");	
	for (;;){
		if((Gsys_msec - t) > 3000){	// total 3 seconds
			Printf("Fail\r\n");
			return (-1);
		}
				
		if((Gsys_msec - t1) > 500){	// 500ms to search again
			if (ArpSearch(serverip) >= 0){
				break;
			}
			t1 = Gsys_msec;
		}
	  
	  	NicProcess();	// process 100 network packets at a time
	  	TimerPoll();		// check wait queue
	  
	 	 if(diag_check_press_ESC() == DIAG_ESC ){
			Printf("Fail\r\n");
			return (-1);
		}
	}
	Printf("OK\r\n");		// Search TFTP Server...OK
	
	/* init the first TFTP stream */
	tftp_stream.offset = 0;	
	tftp_stream.last_good_block = 0;
	Printf("Using default protocol (TFTP)\r\n");
	return 0;
}







/**	\brief
 *
 *	Forward a TFTP Read Request Packet to UDP.
 *
 *	\param[in]	*file_name : MAC port
 *
 *	\retval 		error code
 */
int tftp_stream_rrq(char * file_name)
{

    	struct tftphdr *hdr = (struct tftphdr *)tftp_stream.data;		// apply a TFTP header to tftp_stream_data
    	char *cp;
	char *fp;		// file pointer / string pointer
	int i=sizeof(struct tftphdr);
		
	// Create initial request
    	hdr->th_opcode = htons(RRQ);  // Read file request
    	cp = (char *)&hdr->th_stuff;
	fp = file_name;

	/* Copy Filename to TFTP header */
    	while (*fp){ 
		*cp++ = *fp++;
		i++;
	}
	Printf("Ready to receive %s\r\n", file_name);
    	*cp++ = '\0';		// append 0 to header
    	
   	 // Since this is used for downloading data, OCTET (binary) is the
    	// only mode that makes sense.
    	fp = "OCTET";

	/* Copy Mode String to TFTP Header */
    	while (*fp){
		*cp++ = *fp++;
		i++;
	}
    	*cp++ = '\0';		

	/* Send this TFTP packet by UDP */

	
		UdpSend(serverip, GET_PORT, TFTP_PORT, (uchar *)&tftp_stream.data, i);
				
	/* Hank ?? */
	return (-1); // Couldn't read
}





/**	\brief
 *
 *	Forward a TFTP ACK Packet to UDP.
 *
 *	\param[in]	d_ip : destination ip
 *	\param[in]	udp_sport : UDP source port
 *	\param[in]	block : block number
 *
 *	\retval 		error code
 */
int tftp_stream_ack(ulong d_ip,ushort udp_sport, int block)
{
    	struct tftphdr *hdr = (struct tftphdr *)tftp_stream.data;
	
	hdr->th_opcode = htons(ACK);  // Read file
	hdr->th_block = htons(tftp_stream.last_good_block);

	UdpSend(d_ip, GET_PORT, udp_sport, (uchar *)&tftp_stream.data, 4);
	return 0; // Couldn't read
}





/**	\brief
 *
 *	Forward a TFTP ACK Packet to UDP.
 *
 *	\param[in]	err_code : error code
 *
 *	\retval 		String for print out
 */
char *tftp_error(int err_code)
{
    char *errmsg = "Unknown error\r\n";

    switch (err_code) {
		
    case TFTP_ENOTFOUND:
        return "file not found\r\n";
    case TFTP_EACCESS:
        return "access violation\r\n";
    case TFTP_ENOSPACE:
        return "disk full or allocation exceeded\r\n";
    case TFTP_EBADOP:
        return "illegal TFTP operation\r\n";
    case TFTP_EBADID:
        return "unknown transfer ID\r\n";
    case TFTP_EEXISTS:
        return "file already exists\r\n";
    case TFTP_ENOUSER:
        return "no such user\r\n";
    case TFTP_TIMEOUT:
        return "operation timed out\r\n";
    case TFTP_NETERR:
        return "some sort of network error\r\n";
    case TFTP_INVALID:
        return "invalid parameter\r\n";
    case TFTP_PROTOCOL:
        return "protocol violation\r\n";
    case TFTP_TOOLARGE:
        return "file is larger than buffer\r\n";
    }
	
    return errmsg;
}





/**	\brief
 *
 *	Receive a TFTP data packet
 *
 *	\param[in]	d_ip : destination ip
 *	\param[in]	udp_sport : UDP source port
 *	\param[in]	header : TFTP Header
 *	\param[in]	len : Packet Length
 *
 *	\retval 		error code
 */
int tftp_stream_read(ulong d_ip, ushort udp_sport, char *header,int len)
{
 	ushort ch;
	char *ptr;
	static ulong count=0;

	
	struct tftphdr *hdr = (struct tftphdr *)header;
	/* We just get the file from server ip */
	if(d_ip != serverip){
		Printf("TFTP : Check Source IP Fail\r\n");
		return 0;
	}	
		
	Runbar(count++, 100);
	
	ch = htons(hdr->th_opcode);	// reverse the opcode.
	
	switch(ch){
		case DATA:
			if(htons(hdr->th_block) == (tftp_stream.last_good_block + 1)){		// check block sequence
				tftp_stream.last_good_block = htons(hdr->th_block);			// update block sequence
				tftp_stream_ack(d_ip, udp_sport, tftp_stream.last_good_block);	
				ptr = (uchar *)(SDRAM_DOWNLOAD_TEMP_BASE) + tftp_stream.offset;	// store file data here
				Memcpy(ptr,hdr->th_data,(len - 4));		// -4 is opcode(2 bytes) + Block#(2 bytes)
				tftp_stream.offset += (len - 4);
				
				if((len - 4) < SEGSIZE){	// if data size is less than 512bytes , the total transmission is done.
					tftp_complete_flag = 1;
					tftp_file_length = tftp_stream.offset;
				}
			}else{
				// the transmission is not done , we should send an ACK packet to server.
				tftp_stream_ack(d_ip, udp_sport, tftp_stream.last_good_block);	// last_good_block indicates the block number we have received
			}
			break;
		case ERROR_TFTP:
			Printf(tftp_error(htons(hdr->th_code)));
			tftp_complete_flag = -1;
			break;
		default:
			Printf("Unknow TFTP Packet\r\n");
			break;
	}
	return 0;
}





/**	\brief
 *
 *	Start a TFTP receive process.
 *
 *	\param[in]	* file_name : File name which will be received
 *
 *	\retval 		tftp_complete_flag
 */
int tftp_protocol_recv(char *file_name)
{
	
	if(!tftp_start()){
		tftp_stream_rrq(file_name);
		for (;;){
			if (diag_check_press_ESC() == DIAG_ESC){
				break;
			}
			NicProcess();
			if (tftp_complete_flag != 0){
				break;
			}
		}
	}
	return (tftp_complete_flag);
}





/**	\brief
 *
 *	Shutdown MAC
 *
 *	\retval 	error code
 */
int tftp_close(void)
{
	NicIp[0] = 0xFE7Fa8c0;
	mac_shutdown(0);
	return 0;
}





/**	\brief
 *
 *	Sets the specific address into I2C_TAR register.
 *
 *	\param[in]	local_ip : Local IP
 *	\param[in]	server_ip : Server IP
 *
 *	\retval 		error code
 */
int tftp_set_ip(ulong local_ip, ulong server_ip)
{
	char *ptr;
	int len;
	
	/* Hank : you should check sector size */
	len = GetSectorSize(sysModelInfo[board_index].smcflash0, 0);

	/* get the config data in the SPI Flash and store in the SDRAM */
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);		

	/* Set Default TFTP Local Ip and Server Ip */
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|LOCALIP_OFFSET) = local_ip; 
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|SERVERIP_OFFSET) = server_ip; 
	/* Write back to the SPI Flash */
	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0){
		Printf("Config initial....FAIL !\r\n");
		return -1;
	}else{
		Printf("\r\nConfig initial....OK !\r\n");
	}

	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, SMC_FLAGH0_LOCALIP, 4);
	ptr = (char *)SDRAM_DOWNLOAD_TEMP_BASE;
	Printf("\r\n");
	Printf("Local IP : %d.%d.%d.%d\r\n", ptr[0], ptr[1], ptr[2], ptr[3]);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, SMC_FLAGH0_SERVERIP, 4);
	ptr = (char *)SDRAM_DOWNLOAD_TEMP_BASE;
	Printf("Server IP : %d.%d.%d.%d\r\n", ptr[0], ptr[1], ptr[2], ptr[3]);

	return 0;
}
