/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    mac.h

    DesignWare Cores Ethernet MAC Universal registers/values definition and
    Tx/Rx descriptors definition.

    2008-06-12	Will Suen
		Created
	2008-10-03	Chin-Fu Yang		
		modified			
*/

#ifndef _MAC_H
#define _MAC_H

#include "global.h"

/*------------------------------------------------------ Macro / Define -----------------------------*/
/***************************************
	Ethernet MAC Control
***************************************/
#define S2E_PHY_ADDR				1				/* PHY address */
#define NIC_IP	0x0008
#define NIC_ARP 	0x0608
#define NIC_IPX 	0x3781


/* inw/outw wrapper */
#define inw(addr)			(*((volatile ulong*)(addr)))
#define outw(addr, val)	*((volatile ulong*)(addr)) = (val)

/***************************************
	Register Offset Definition
***************************************/
/* DMA Register */
#define MAC_DMAR(r)						(0x1000 + 4 * (r))
#define MAC_DMA_BMR					MAC_DMAR(0)		/*0x1000*/	/* Bus mode register */
#define MAC_DMA_TPDR					MAC_DMAR(1)		/*0x1004*/	/* Transmit Poll Demand Register */
#define MAC_DMA_RPDR					MAC_DMAR(2)		/*0x1008*/	/* Receiv Poll Demand Register */
#define MAC_DMA_RDLAR					MAC_DMAR(3)		/*0x100C*/	/* Receive Descriptor List Address Register */
#define MAC_DMA_TDLAR					MAC_DMAR(4)		/*0x1010*/	/* Transmit Descriptor List Address Register */
#define MAC_DMA_SR						MAC_DMAR(5)		/*0x1014*/	/* Status Register */
#define MAC_DMA_OMR					MAC_DMAR(6)		/*0x1018*/	/* Operation mode Register */
#define MAC_DMA_IER					MAC_DMAR(7)		/*0x101C*/	/* Interrupt Enable register */
#define MAC_DMA_MFBOCR				MAC_DMAR(8)		/*0x1020*/	/* Missed Frame and Buffer Overflow Counter Register */
#define MAC_DMA_CHTDR					MAC_DMAR(18)	/*0x1048*/	/* Current Host Transmit Descriptor Register */
#define MAC_DMA_CHRDR					MAC_DMAR(19)	/*0x104C*/	/* Current Host Receive Descriptor Register */
#define MAC_DMA_CHTBR					MAC_DMAR(20)	/*0x1050*/	/* Current Host Transmit Buffer Register */
#define MAC_DMA_CHRBR					MAC_DMAR(21)	/*0x1054*/	/* Current Host Receive Buffer Register */
/* GMAC Register */
#define MAC_CSR(r)						(0x0000 + 4 * (r))
#define MAC_CR							MAC_CSR(0)	/*0x0000*/	/* MAC configuration Register */
#define MAC_FR							MAC_CSR(1)	/*0x0004*/	/* MAC Frame Register */
#define MAC_HTHR						MAC_CSR(2)	/*0x0008*/	/* Hash Table High Register */
#define MAC_HTLR						MAC_CSR(3)	/*0x000C*/	/* Host Table Low Register */
#define MAC_GMII_AR						MAC_CSR(4)	/*0x0010*/	/* GMII Address Register */
#define MAC_GMII_DR						MAC_CSR(5)	/*0x0014*/	/* GMII Data Register */
#define MAC_FCR							MAC_CSR(6)	/*0x0018*/	/* Flow control Register */
#define MAC_VLAN_TR						MAC_CSR(7)	/*0x001C*/	/* VLAN Tag Register */
#define MAC_VR							MAC_CSR(8)	/*0x0020*/	/* Version Register */
#define MAC_ISR							MAC_CSR(14)	/*0x0038*/	/* Interrupt status Register */
#define MAC_IMR							MAC_CSR(15)	/*0x003C*/	/* Interrupt Mask Register */
#define MAC_A0HR						MAC_CSR(16)	/*0x0040*/	/* MAC address0 High Register */
#define MAC_A0LR						MAC_CSR(17)	/*0x0044*/	/* MAC address0 Low Register */
#define MAC_AnHR(n)						MAC_CSR(16 + 2 * (n))
#define MAC_AnLR(n)						MAC_CSR(17 + 2 * (n))
#define MAC_A1HR						MAC_AnHR(1)	/*MAC_CSR(18)*/	/*0x0048*/	/* MAC address1 High Register */
#define MAC_A1LR						MAC_AnLR(1)	/*MAC_CSR(19)*/	/*0x004C*/	/* MAC address1 Low Register */
#define MAC_A2HR						MAC_AnHR(2)	/*MAC_CSR(20)*/	/*0x0050*/	/* MAC address2 High Register */
#define MAC_A2LR						MAC_AnLR(2)	/*MAC_CSR(21)*/	/*0x0054*/	/* MAC address2 Low Register */
#define MAC_A3HR						MAC_AnHR(3)	/*MAC_CSR(22)*/	/*0x0058*/	/* MAC address3 High Register */
#define MAC_A3LR						MAC_AnLR(3)	/*MAC_CSR(23)*/	/*0x005C*/	/* MAC address3 Low Register */
#define MAC_A4HR						MAC_AnHR(4)	/*MAC_CSR(24)*/	/*0x0060*/	/* MAC address4 High Register */
#define MAC_A4LR						MAC_AnLR(4)	/*MAC_CSR(25)*/	/*0x0064*/	/* MAC address4 Low Register */
#define MAC_A5HR						MAC_AnHR(5)	/*MAC_CSR(26)*/	/*0x0068*/	/* MAC address5 High Register */
#define MAC_A5LR						MAC_AnLR(5)	/*MAC_CSR(27)*/	/*0x006C*/	/* MAC address5 Low Register */
#define MAC_A6HR						MAC_AnHR(6)	/*MAC_CSR(28)*/	/*0x0070*/	/* MAC address6 High Register */
#define MAC_A6LR						MAC_AnLR(6)	/*MAC_CSR(29)*/	/*0x0074*/	/* MAC address6 Low Register */
#define MAC_A7HR						MAC_AnHR(7)	/*MAC_CSR(30)*/	/*0x0078*/	/* MAC address7 High Register */
#define MAC_A7LR						MAC_AnLR(7)	/*MAC_CSR(31)*/	/*0x007C*/	/* MAC address7 Low Register */
/* MAC Management Counter Registers */
#define MAC_MMC_CR						MAC_CSR(64)		/*0x0100*/	/* MMC Control Register */
#define MAC_MMC_RIR						MAC_CSR(65)		/*0x0104*/	/* MMC Receive Interrupt Register */
#define MAC_MMC_TIR						MAC_CSR(66)		/*0x0108*/	/* MMC Transmit Interrupt Register */
#define MAC_MMC_RIMR					MAC_CSR(67)		/*0x010C*/	/* MMC Receive Interrupt Mask Register */
#define MAC_MMC_TIMR					MAC_CSR(68)		/*0x0110*/	/* MMC Transmit Interrupt Mask Register */
#define MAC_MMC_TXFRAMECOUNT_GB			MAC_CSR(70)		/*0x0118*/	/* txframecount_gb */
#define MAC_MMC_TXUNDERFLOWERROR		MAC_CSR(82)		/*0x0148*/	/* txunderflowerror */
#define MAC_MMC_TXDEFERRED				MAC_CSR(85)		/*0x0154*/	/* txdeferred */
#define MAC_MMC_TXLATECOL				MAC_CSR(86)		/*0x0158*/	/* txlatecol */
#define MAC_MMC_TXEXESSCOL				MAC_CSR(87)		/*0x015C*/	/* txexesscol */
#define MAC_MMC_TXCARRIERERROR			MAC_CSR(88)		/*0x0160*/	/* txcarriererror */
#define MAC_MMC_TXFRAMECOUNT_G			MAC_CSR(90)		/*0x0168*/	/* txframecount_g */
#define MAC_MMC_TXEXCESSDEF				MAC_CSR(91)		/*0x016C*/	/* txexcessdef */
#define MAC_MMC_TXPAUSEFRAMES			MAC_CSR(92)		/*0x0170*/	/* txpauseframes */
#define MAC_MMC_RXFRAMECOUNT_GB			MAC_CSR(96)		/*0x0180*/	/* rxframecount_gb */
#define MAC_MMC_RXCRCERROR				MAC_CSR(101)	/*0x0194*/	/* rxcrcerror */
#define MAC_MMC_RXALIGNMENTERROR		MAC_CSR(102)	/*0x0198*/	/* rxalignmenterror */
#define MAC_MMC_RXRUNTERROR				MAC_CSR(103)	/*0x019C*/	/* rxrunterror */
#define MAC_MMC_RXJABBERERROR			MAC_CSR(104)	/*0x01A0*/	/* rxjabbererror */
#define MAC_MMC_RXUNDERSIZE_G			MAC_CSR(105)	/*0x01A4*/	/* rxundersize_g */
#define MAC_MMC_RXOVERSIZE_G			MAC_CSR(106)	/*0x01A8*/	/* rxoversize_g */
#define MAC_MMC_RXLENGTHERROR			MAC_CSR(114)	/*0x01C8*/	/* rxlengtherror */
#define MAC_MMC_RXPAUSEFRAMES			MAC_CSR(116)	/*0x01D0*/	/* rxpauseframes */
#define MAC_MMC_RXFIFOOVERFLOW			MAC_CSR(117)	/*0x01D4*/	/* rxfifooverflow */
#define MAC_MMC_RXWATCHDOGERROR			MAC_CSR(119)	/*0x01DC*/	/* rxwatchdogerror */
/* IEEE 15888 Time Stamp Control Registers */
#define MAC_TSC_TSR						MAC_CSR(448)	/*0x0700*/	/* Time Stamp Register */
#define MAC_TSC_SSIR					MAC_CSR(449)	/*0x0704*/	/* Sub-Second Increment Register */
#define MAC_TSC_TSHR					MAC_CSR(450)	/*0x0708*/	/* Time Stamp HIGH Register */
#define MAC_TSC_TSLR					MAC_CSR(451)	/*0x070C*/	/* Time Stamp LOW Register */
#define MAC_TSC_TSHUR					MAC_CSR(452)	/*0x0710*/	/* Time Stamp High Update Register */
#define MAC_TSC_TSLUR					MAC_CSR(453)	/*0x0714*/	/* Time Stamp Low Update Register */
#define MAC_TSC_TSAR					MAC_CSR(454)	/*0x0718*/	/* Time Stamp Addend Register */
#define MAC_TSC_TTHR					MAC_CSR(455)	/*0x071C*/	/* Target Time High Register */
#define MAC_TSC_TTLR					MAC_CSR(456)	/*0x0720*/	/* Target Time Low Register */

/***************************************
	Register values & masks (not all)
***************************************/
#define DWMAC_BITS(h, l)						(((1 << ( (h) - (l) + 1) ) - 1) << l)		// Set Bit From L to H
#define DWMAC_BIT(b)							(1 << (b))		// Set Bit b
#define DWMAC_BVAL(v, b)						((v) << (b))		// Set Bit b as V

/* DMA Register0: Bus mode register masks */
#define MAC_DMA_BMR_AAL					DWMAC_BIT(25)		/* Address-Aligned Beats */
#define MAC_DMA_BMR_4xPBL				DWMAC_BIT(24)		/* 4xPBL Mode */
#define MAC_DMA_BMR_USP					DWMAC_BIT(23)		/* Use Separate PBL */
#define MAC_DMA_BMR_RPBL				DWMAC_BITS(22, 17)	/* Rx Programmable Burst Length */
#define MAC_DMA_BMR_FB					DWMAC_BIT(16)		/* Fix Burst */
#define MAC_DMA_BMR_PR					DWMAC_BITS(15, 14)	/* Rx:Tx priority ratio */
#define MAC_DMA_BMR_PBL					DWMAC_BITS(13, 8)	/* Programmable Burst Length */
#define MAC_DMA_BMR_DSL					DWMAC_BITS(6, 2)	/* Descriptor Skip Length (no. of words) */
#define MAC_DMA_BMR_DA					DWMAC_BIT(1)		/* DMA Arbitration scheme */
#define MAC_DMA_BMR_SWR					DWMAC_BIT(0)		/* Software Reset */
/* values */
#define MAC_DMA_BMR_RPBL_1				DWMAC_BVAL(1, 17)	/* RxPBL = 1 */
#define MAC_DMA_BMR_RPBL_2				DWMAC_BVAL(2, 17)	/* RxPBL = 2 */
#define MAC_DMA_BMR_RPBL_4				DWMAC_BVAL(4, 17)	/* RxPBL = 4 */
#define MAC_DMA_BMR_RPBL_8				DWMAC_BVAL(8, 17)	/* RxPBL = 8 */
#define MAC_DMA_BMR_RPBL_16				DWMAC_BVAL(16, 17)	/* RxPBL = 16 */
#define MAC_DMA_BMR_RPBL_32				DWMAC_BVAL(32, 17)	/* RxPBL = 32 */
#define MAC_DMA_BMR_PR_1_1				DWMAC_BVAL(0, 14)	/* Rx:Tx PR = 1:1 */
#define MAC_DMA_BMR_PR_2_1				DWMAC_BVAL(1, 14)	/* Rx:Tx PR = 2:1 */
#define MAC_DMA_BMR_PR_3_1				DWMAC_BVAL(2, 14)	/* Rx:Tx PR = 3:1 */
#define MAC_DMA_BMR_PR_4_1				DWMAC_BVAL(3, 14)	/* Rx:Tx PR = 4:1 */
#define MAC_DMA_BMR_PBL_1				DWMAC_BVAL(1, 8)	/* TxPBL = 1 */
#define MAC_DMA_BMR_PBL_2				DWMAC_BVAL(2, 8)	/* TxPBL = 2 */
#define MAC_DMA_BMR_PBL_4				DWMAC_BVAL(4, 8)	/* TxPBL = 4 */
#define MAC_DMA_BMR_PBL_8				DWMAC_BVAL(8, 8)	/* TxPBL = 8 */
#define MAC_DMA_BMR_PBL_16				DWMAC_BVAL(16, 8)	/* TxPBL = 16 */
#define MAC_DMA_BMR_PBL_32				DWMAC_BVAL(32, 8)	/* TxPBL = 32 */
#define MAC_DMA_BMR_DSL_(n)				DWMAC_BVAL((n), 2)	/* DSL = n */
/* DMA Register6: Operation Mode Register */
#define MAC_DMA_OMR_SR					DWMAC_BIT(1)		/* Start/Stop Receive */
#define MAC_DMA_OMR_OSF					DWMAC_BIT(2)		/* Operate on Second Frame */
#define MAC_DMA_OMR_RTC					DWMAC_BITS(4, 3)	/* Receive Threshold Control */
#define MAC_DMA_OMR_FUF					DWMAC_BIT(6)		/* Forward Undersized Good Frames */
#define MAC_DMA_OMR_FEF					DWMAC_BIT(7)		/* Forward Error Frames */
#define MAC_DMA_OMR_EFC					DWMAC_BIT(8)		/* Enable HW flow control */
#define MAC_DMA_OMR_ST					DWMAC_BIT(13)		/* Start/Stop Transmission Command */
#define MAC_DMA_OMR_TTC					DWMAC_BITS(16, 14)	/* Transmit Threshold Control */
#define MAC_DMA_OMR_FTF					DWMAC_BIT(20)		/* Flush Transmit FIFO */
#define MAC_DMA_OMR_TSF					DWMAC_BIT(21)		/* Transmit Store and Forward */
#define MAC_DMA_OMR_RFD					(DWMAC_BIT(22) | DWMAC_BITS(12, 11))	/* Threshold for Deactivating Flow Control */
#define MAC_DMA_OMR_RFA					(DWMAC_BIT(23) | DWMAC_BITS(10, 9))		/* Threshold for Activating Flow Control */
#define MAC_DMA_OMR_DFF					DWMAC_BIT(24)		/* Disable Flushing of Received Frames */
#define MAC_DMA_OMR_RSF					DWMAC_BIT(25)		/* Receive Store and Forward */
#define MAC_DMA_OMR_DT					DWMAC_BIT(26)		/* Disable Dropping of TCP/IP Checksum Error Frames */
/* values */
#define MAC_DMA_OMR_RTC_64				DWMAC_BVAL(0, 3)
#define MAC_DMA_OMR_RTC_32				DWMAC_BVAL(1, 3)
#define MAC_DMA_OMR_RTC_96				DWMAC_BVAL(2, 3)
#define MAC_DMA_OMR_RTC_128				DWMAC_BVAL(3, 3)
#define MAC_DMA_OMR_RFA_1K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(0, 9))	/* Full minus 1KB */
#define MAC_DMA_OMR_RFA_2K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(1, 9))	/* Full minus 2KB */
#define MAC_DMA_OMR_RFA_3K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(2, 9))	/* Full minus 3KB */
#define MAC_DMA_OMR_RFA_4K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(3, 9))	/* Full minus 4KB */
#define MAC_DMA_OMR_RFA_5K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(0, 9))	/* Full minus 5KB */
#define MAC_DMA_OMR_RFA_6K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(1, 9))	/* Full minus 6KB */
#define MAC_DMA_OMR_RFA_7K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(2, 9))	/* Full minus 7KB */
#define MAC_DMA_OMR_RFD_1K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(0, 9))	/* Full minus 1KB */
#define MAC_DMA_OMR_RFD_2K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(1, 9))	/* Full minus 2KB */
#define MAC_DMA_OMR_RFD_3K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(2, 9))	/* Full minus 3KB */
#define MAC_DMA_OMR_RFD_4K				(DWMAC_BVAL(0, 23) | DWMAC_BVAL(3, 9))	/* Full minus 4KB */
#define MAC_DMA_OMR_RFD_5K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(0, 9))	/* Full minus 5KB */
#define MAC_DMA_OMR_RFD_6K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(1, 9))	/* Full minus 6KB */
#define MAC_DMA_OMR_RFD_7K				(DWMAC_BVAL(1, 23) | DWMAC_BVAL(2, 9))	/* Full minus 7KB */
#define MAC_DMA_OMR_TTC_64				DWMAC_BVAL(0, 14)
#define MAC_DMA_OMR_TTC_128				DWMAC_BVAL(1, 14)
#define MAC_DMA_OMR_TTC_192				DWMAC_BVAL(2, 14)
#define MAC_DMA_OMR_TTC_256				DWMAC_BVAL(3, 14)
#define MAC_DMA_OMR_TTC_40				DWMAC_BVAL(4, 14)
#define MAC_DMA_OMR_TTC_32				DWMAC_BVAL(5, 14)
#define MAC_DMA_OMR_TTC_24				DWMAC_BVAL(6, 14)
#define MAC_DMA_OMR_TTC_16				DWMAC_BVAL(7, 14)
/* DMA Register7: Interrupt Enable Register */
#define MAC_DMA_IER_TIE					DWMAC_BIT(0)		/* Transmit Interrupt Enable */
#define MAC_DMA_IER_TSE					DWMAC_BIT(1)		/* Transmit Stopped Enable */
#define MAC_DMA_IER_TUE					DWMAC_BIT(2)		/* Transmit Buffer Unavilable Enable */
#define MAC_DMA_IER_TJE					DWMAC_BIT(3)		/* Transmit Jabber Timeout Enable */
#define MAC_DMA_IER_OVE					DWMAC_BIT(4)		/* Overflow Interrupt Enable */
#define MAC_DMA_IER_UNE					DWMAC_BIT(5)		/* Underflow Interrupt Enable */
#define MAC_DMA_IER_RIE					DWMAC_BIT(6)		/* Receive Interrupt Enable */
#define MAC_DMA_IER_RUE					DWMAC_BIT(7)		/* Receive Buffer Unavilable Enable */
#define MAC_DMA_IER_RSE					DWMAC_BIT(8)		/* Receive Stopped Error */
#define MAC_DMA_IER_RWE					DWMAC_BIT(9)		/* Receive Watchdog Timeout Enable */
#define MAC_DMA_IER_ETE					DWMAC_BIT(10)		/* Early Transmit Interrupt Enable */
#define MAC_DMA_IER_FBE					DWMAC_BIT(13)		/* Fatal Bus Error Enable */
#define MAC_DMA_IER_ERE					DWMAC_BIT(14)		/* Early Receive Interrupt Enable */
#define MAC_DMA_IER_AIE					DWMAC_BIT(15)		/* Abnormal Interrupt Summary Enable */
#define MAC_DMA_IER_NIE					DWMAC_BIT(16)		/* Normal Interrupt Summary Enable */
/* GMAC Register0: MAC Configuration Register */
#define MAC_CR_RE						DWMAC_BIT(2)		/* Receiver Enable */
#define MAC_CR_TE						DWMAC_BIT(3)		/* Transmitter Enable */
#define MAC_CR_DC						DWMAC_BIT(4)		/* Deferral Check */
#define MAC_CR_BL						DWMAC_BITS(6, 5)	/* Back-Off Limit */
#define MAC_CR_ACS						DWMAC_BIT(7)		/* Automatic Pad/CRC Stripping */
#define MAC_CR_LUD						DWMAC_BIT(8)		/* Link Up/Down */
#define MAC_CR_DR						DWMAC_BIT(9)		/* Disable Retry */
#define MAC_CR_IPC						DWMAC_BIT(10)		/* Checksum Offload */
#define MAC_CR_DM						DWMAC_BIT(11)		/* Duplex Mode */
#define MAC_CR_LM						DWMAC_BIT(12)		/* Loopback Mode */
#define MAC_CR_DO						DWMAC_BIT(13)		/* Disable Receive On */
#define MAC_CR_FES						DWMAC_BIT(14)		/* Speed */
#define MAC_CR_PS						DWMAC_BIT(15)		/* Port Select */
#define MAC_CR_DCRS						DWMAC_BIT(16)		/* Disable Carrier Sense During Transmission */
#define MAC_CR_IFG						DWMAC_BITS(19, 17)	/* Inter-Frame Gap */
#define MAC_CR_JE						DWMAC_BIT(20)		/* Jumbo Frame Enable */
#define MAC_CR_BE						DWMAC_BIT(21)		/* Frame Burst Enable */
#define MAC_CR_JD						DWMAC_BIT(22)		/* Jabber Disable */
#define MAC_CR_WD						DWMAC_BIT(23)		/* Watchdog Disable */
#define MAC_CR_TC						DWMAC_BIT(24)		/* Transmit Configuration in RGMII/SGMII */
/* GMAC Register4: GMII Address Register */
#define MAC_GMII_AR_PA					DWMAC_BITS(15, 11)	/* Physical Layer Address */
#define MAC_GMII_AR_GR					DWMAC_BITS(10, 6)	/* GMII Register */
#define MAC_GMII_AR_CR					DWMAC_BITS(4, 2)	/* CSR Clock Range */
#define MAC_GMII_AR_GW					DWMAC_BIT(1)		/* GMII Write */
#define MAC_GMII_AR_GB					DWMAC_BIT(0)		/* GMII Busy */
/* values */
#define MAC_GMII_AR_PA_(n)				DWMAC_BVAL((n), 11)	/* PA = n */
#define MAC_GMII_AR_GR_(n)				DWMAC_BVAL((n), 6)	/* GR = n */
#define MAC_GMII_AR_CR_60_100			DWMAC_BVAL(0, 2)	/* 60-100 MHz */
#define MAC_GMII_AR_CR_100_150			DWMAC_BVAL(1, 2)	/* 100-150 MHz */
#define MAC_GMII_AR_CR_20_35			DWMAC_BVAL(2, 2)	/* 20-35 MHz */
#define MAC_GMII_AR_CR_35_60			DWMAC_BVAL(3, 2)	/* 35-60 MHz */
#define MAC_GMII_AR_CR_150_250			DWMAC_BVAL(4, 2)	/* 150-250 MHz */
#define MAC_GMII_AR_CR_250_300			DWMAC_BVAL(5, 2)	/* 250-300 MHz */
/* GMAC Register6: Flow Control Register */
#define MAC_FCR_PT						DWMAC_BITS(31, 16)	/* Pause Time */
#define MAC_FCR_DZPQ					DWMAC_BIT(7)		/* Disable Zero-Quenta Pause */
#define MAC_FCR_PLT						DWMAC_BITS(5, 4)	/* Pause Low Threshold */
#define MAC_FCR_UP						DWMAC_BIT(3)		/* Unicast Pause Frame Detect */
#define MAC_FCR_RFE						DWMAC_BIT(2)		/* Receive Flow Control Enable */
#define MAC_FCR_TFE						DWMAC_BIT(1)		/* Transmit Flow Control Enable */
#define MAC_FCR_FCB						DWMAC_BIT(0)		/* Flow Control Busy */
#define MAC_FCR_BPA						DWMAC_BIT(0)		/* Backpressure Activate */
/* values */
#define MAC_FCR_PT_(n)					DWMAC_BVAL((n), 16)
#define MAC_FCR_PLT_4					DWMAC_BVAL(0, 4)	/* Pause time minus 4 slot times */
#define MAC_FCR_PLT_28					DWMAC_BVAL(1, 4)	/* Pause time minus 28 slot times */
#define MAC_FCR_PLT_144					DWMAC_BVAL(2, 4)	/* Pause time minus 144 slot times */
#define MAC_FCR_PLT_256					DWMAC_BVAL(3, 4)	/* Pause time minus 256 slot times */
/* GMAC Register16,18,20,22,24,26,28,30: MAC Address High Register */
#define MAC_AHR_AE						DWMAC_BIT(31)		/* Address Enable */
#define MAC_AHR_SA						DWMAC_BIT(30)		/* Source Address */
#define MAC_AHR_HBC(n)					DWMAC_BIT(29 - (n))	/* Mask Byte Control */
#define MAC_AHR_HBC0					MAC_AHR_HBC(0)		/* Mask Byte Control: XX-__-__-__-__-__ */
#define MAC_AHR_HBC1					MAC_AHR_HBC(1)		/* Mask Byte Control: __-XX-__-__-__-__ */
#define MAC_AHR_HBC2					MAC_AHR_HBC(2)		/* Mask Byte Control: __-__-XX-__-__-__ */
#define MAC_AHR_HBC3					MAC_AHR_HBC(3)		/* Mask Byte Control: __-__-__-XX-__-__ */
#define MAC_AHR_HBC4					MAC_AHR_HBC(4)		/* Mask Byte Control: __-__-__-__-XX-__ */
#define MAC_AHR_HBC5					MAC_AHR_HBC(5)		/* Mask Byte Control: __-__-__-__-__-XX */
#define MAC_AHR_A						DWMAC_BITS(15, 0)	/* MAC Address[47:32] */
/* values */
#define MAC_AHR_A_(addr)				(DWMAC_BVAL(((unsigned char *)addr)[5], 8) | DWMAC_BVAL(((unsigned char *)addr)[4], 0))
/* GMAC Register17,19,21,23,25,27,29,31: MAC Address Low Register */
#define MAC_ALR_A						DMAC_BITS(31, 0)	/* MAC Address[31:0] */
/* values */
#define MAC_ALR_A_(addr)				(DWMAC_BVAL(((unsigned char *)addr)[3], 24) | DWMAC_BVAL(((unsigned char *)addr)[2], 16) | DWMAC_BVAL(((unsigned char *)addr)[1], 8) | DWMAC_BVAL(((unsigned char *)addr)[0], 0))








/*------------------------------------------------------ Structure ----------------------------------*/


/***************************************
	Tx/Rx Descriptors
***************************************/
typedef struct _mac_rdes {
	/* RDES0 */
	ulong	RxMACAddressPayloadChecksumError : 1;	/* [0]				*/
	ulong	CRCError : 1;								/* [1] CRC Error		*/
	ulong	DribbleBitError : 1;						/* [2] Dribble Bit Error		*/
	ulong	ReceiveError : 1;							/* [3] Receive Error		*/
	ulong	ReceiveWatchdogTimeout : 1;				/* [4] Receive Watchdog Timeout		*/
	ulong	FrameType : 1;							/* [5] Frame Type			*/
	ulong	LateCollision : 1;							/* [6] Late Collision				*/
	ulong	IPCChecksumErrorGiantFrame : 1;			/* [7]						*/
	ulong	LastDescriptor : 1;						/* [8] Last Descriptor			*/
	ulong	FirstDescriptor : 1;						/* [9] First Descriptor				*/
	ulong	VLANTag : 1;								/* [10] VLAN Tag					*/
	ulong	OverflowError : 1;						/* [11] Overflow Error			*/
	ulong	LengthError : 1;							/* [12] Length Error			*/
	ulong	SourceAddressFilterFail : 1;				/* [13] Source Address Filter Fail		*/
	ulong	DescriptorError : 1;						/* [14] Descriptor Error			*/
	ulong	ErrorSummary : 1;						/* [15] Error Summary			*/
	ulong	FrameLength : 14;						/* [16:29] Frame Length		*/
	ulong	DestinationAddressFilterFail : 1;			/* [30] Destination Address Filter Fail	*/
	ulong	OwnBit : 1;								/* [31] Own Bit, 1 => MAC, 0 => software	*/
	
	/* RDES1 */
	ulong	ReceiveBuffer1Size : 11;					/* [0:10] Receive Buffer1 Size		*/
	ulong	ReceiveBuffer2Size : 11;					/* [11:21] Receive Buffer 2 Size	*/
	ulong	Reserved_22_23 : 2;
	ulong	SecondAddressChained : 1;				/* [24] Second Address Chained		*/
	ulong	ReceiveEndOfRing : 1;					/* [25] Receive End of Ring			*/
	ulong	Reserved26_30 : 5;
	ulong	DisableInterruptOnCompletion : 1;			/* [31]								*/
	
	/* RDES2 */
	ulong	Buffer1AddressPointer;
	
	/* RDES3 */
	ulong	Buffer2AddressPointer;
} mac_rdes, *mac_rdes_p;



typedef struct _mac_tdes {
	/* TDES0 */
	ulong	DeferredBit : 1;				/* [0] Deferred Bit							*/
	ulong	UnderflowError : 1;			/* [1] Underflow Error						*/
	ulong	ExcessiveDeferral : 1;		/* [2] Excessive Deferral					*/
	ulong	CollisionCount : 4;			/* [3:6] Collision Count					*/
	ulong	VLANFrame : 1;				/* [7] VLAN Frame							*/
	ulong	ExcessiveCollision : 1;		/* [8] Excessive Collision					*/
	ulong	LateCollision : 1;				/* [9] Late Collision						*/
	ulong	NoCarrier : 1;				/* [10] No Carrier							*/
	ulong	LossOfCarrier : 1;			/* [11] Loss of Carrier						*/
	ulong	PayloadChecksumError : 1;	/* [12] Payload Checksum Error				*/
	ulong	FrameFlushed : 1;			/* [13] Frame Flushed						*/
	ulong	JabberTimeout : 1;			/* [14] Jabber Timeout						*/
	ulong	ErrorSummary : 1;			/* [15] Error Summary						*/
	ulong	IPHeaderError : 1;			/* [16] IP Header Error						*/
	ulong	TxTimeStampStatus : 1;		/* [17] Tx Time Stamp Status				*/
	ulong	Reserved_18_30 : 13;
	ulong	OwnBit : 1;					/* [32] Own Bit, 1 => MAC, 0 => software	*/
	
	/* TDES1 */
	ulong	TransmitBuffer1Size : 11;			/* [0:10] Transmit Buffer 1 Size		*/
	ulong	TransmitBuffer2Size : 11;			/* [11:21] Transmit Buffer 2 Size		*/
	ulong	TransmitTimeStampEnable : 1;	/* [22] Transmit Time Stamp Enable		*/
	ulong	DisablePadding : 1;				/* [23] Disable Padding					*/
	ulong	SecondAddressChained : 1;		/* [24] Second Address Chained			*/
	ulong	TransmitEndOfRing : 1;			/* [25] Transmit End of Ring			*/
	ulong	DisableCRC : 1;					/* [26] Disable CRC						*/
	ulong	ChecksumInsertionControl : 2;	/* [27:28] Checksum Insertion Control	*/
	ulong	FirstSegment : 1;				/* [29] First Segment					*/
	ulong	LastSegment : 1;					/* [30] Last Segment					*/
	ulong	InterruptOnCompletion : 1;		/* [31] Interrupt on Completion			*/
	
	/* TDES2 */
	ulong	Buffer1AddressPointer;
	
	/* TDES3 */
	ulong	Buffer2AddressPointer;
} mac_tdes, *mac_tdes_p;



struct  _MacInfo{
	unsigned long	base;			// base address
	int			irq;				// IRQ number
	mac_tdes_p	Tring;			// TX descriptor pointer
	mac_rdes_p	Rring;			// RX descriptor pointer
	unsigned long	ip;
	unsigned char mac_addr[6];
	unsigned long	phy_addr;
	unsigned long mode;
	unsigned long	InterruptCount;
} __attribute__((packed));
typedef struct _MacInfo MacInfo;

/*------------------------------------------------------ Extern / Function Declaration -----------------*/
extern int mac_is_open;
extern MacInfo MoxaMacInfo[MAC_MAX_AMOUNT];

ushort Lnet_chip_smi_read(int phyAddr, int regNum);
void mac_init(int port);
msgstr_t	mac_read(int port);
int mac_send(int port,msgstr_t msg);
int	mac_send_after_read(int port);
void	mac_shutdown(int port);
int mac_open(int port,int operational_mode);
void	NicProcess(void);
ushort Lnet_chip_smi_read(int phyAddr, int regNum);
int Lnet_chip_smi_write(int phyAddr, int regNum, ushort wData);
int ResetPhyChip(int phyAddr, int operational_mode);
void PrintPHY(int phyAddr);
int PowerMode(int port,UINT8 state);





#endif	/* DW_ETH_H */

