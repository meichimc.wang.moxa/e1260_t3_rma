/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	dbg_uart.h

	Debug UART handle routines

	2008-06-10	Albert Yu
		first release
	2008-10-14	Chin-Fu Yang		
		modified			
*/

#ifndef _DBG_UART_H
#define _DBG_UART_H



/*------------------------------------------------------ Macro / Define -----------------------------*/
#if 1
#define S2E_DW_UART_BASE            	0x05030000	/* APB slave 4: DesignWare UART */

#define DBG_UART_RBR	0x00
#define DBG_UART_THR	0x00
#define DBG_UART_RX		0x00	/* In:  Receive buffer (DLAB=0) */
#define DBG_UART_TX		0x00	/* Out: Transmit buffer (DLAB=0) */
#define DBG_UART_DLL	0x00	/* Out: Divisor Latch Low (DLAB=1) */
#define DBG_UART_TRG	0x00	/* (LCR=BF) FCTR bit 7 selects Rx or Tx
				 * In: Fifo count
				 * Out: Fifo custom trigger levels
				 * XR16C85x only */

#define DBG_UART_DLM	0x04	/* Out: Divisor Latch High (DLAB=1) */
#define DBG_UART_IER	0x04	/* Out: Interrupt Enable Register */
#define DBG_UART_FCTR	0x04	/* (LCR=BF) Feature Control Register
				 * XR16C85x only */

#define DBG_UART_IIR	0x08	/* In:  Interrupt ID Register */
#define DBG_UART_FCR	0x08	/* Out: FIFO Control Register */
#define DBG_UART_EFR	0x08	/* I/O: Extended Features Register */
				/* (DLAB=1, 16C660 only) */

#define DBG_UART_LCR	0x0C	/* Out: Line Control Register */
#define DBG_UART_MCR	0x10	/* Out: Modem Control Register */
#define DBG_UART_LSR	0x14	/* In:  Line Status Register */
#define DBG_UART_MSR	0x18	/* In:  Modem Status Register */
#define DBG_UART_SCR	0x1C	/* I/O: Scratch Register */
#else
#define DBG_BY_MOXA_UART

#define S2E_DW_UART_BASE            	0x05080000	/* APB slave 4: DesignWare UART */

#define DBG_UART_RBR	0x00
#define DBG_UART_THR	0x00
#define DBG_UART_RX		0x00	/* In:  Receive buffer (DLAB=0) */
#define DBG_UART_TX		0x00	/* Out: Transmit buffer (DLAB=0) */
#define DBG_UART_DLL	0x00	/* Out: Divisor Latch Low (DLAB=1) */
#define DBG_UART_TRG	0x00	/* (LCR=BF) FCTR bit 7 selects Rx or Tx
				 * In: Fifo count
				 * Out: Fifo custom trigger levels
				 * XR16C85x only */

#define DBG_UART_DLM	0x01	/* Out: Divisor Latch High (DLAB=1) */
#define DBG_UART_IER	0x01	/* Out: Interrupt Enable Register */
#define DBG_UART_FCTR	0x01	/* (LCR=BF) Feature Control Register
				 * XR16C85x only */

#define DBG_UART_IIR	0x02	/* In:  Interrupt ID Register */
#define DBG_UART_FCR	0x02	/* Out: FIFO Control Register */
#define DBG_UART_EFR	0x02	/* I/O: Extended Features Register */
				/* (DLAB=1, 16C660 only) */

#define DBG_UART_LCR	0x03	/* Out: Line Control Register */
#define DBG_UART_MCR	0x04	/* Out: Modem Control Register */
#define DBG_UART_LSR	0x05	/* In:  Line Status Register */
#define DBG_UART_MSR	0x06	/* In:  Modem Status Register */
#define DBG_UART_SCR	0x07	/* I/O: Scratch Register */
#endif

#define DBG_UART_NUM			1		// only one port

/* REMAP_CLOCK_SEL bits definition */
#define DW_UART_CLK_SRC_EXTERNAL       	0X20
#define DW_UART_CLK_ENABLE             		0X04

#define DBG_UART_BUFFER_SIZE	UART_BUF_MAX_SIZE
#define DBG_UART_BUFFER_MASK	(DBG_UART_BUFFER_SIZE - 1)

/*
 * These are the definitions for the FIFO Control Register
 * (16650 only)
 */
#define UART_MCR_AFE		    0x20
#define UART_LSR_SPECIAL	    0x1E
#define UART_FCR_ENABLE_FIFO	0x01 /* Enable the FIFO */
#define UART_FCR_CLEAR_RCVR     0x02 /* Clear the RCVR FIFO */
#define UART_FCR_CLEAR_XMIT     0x04 /* Clear the XMIT FIFO */
#define UART_FCR_DMA_SELECT     0x08 /* For DMA applications */
#define UART_FCR_TRIGGER_MASK	0xC0 /* Mask for the FIFO trigger range */
/* 16650 redefinitions */
#define UART_FCR6_R_TRIGGER_1	0x00 /* Mask for receive trigger set at 1 */
#define UART_FCR6_R_TRIGGER_2	0x40 /* Mask for receive trigger set at 2 */
#define UART_FCR6_R_TRIGGER_4   0x80 /* Mask for receive trigger set at 4 */
#define UART_FCR6_R_TRIGGER_6	0xC0 /* Mask for receive trigger set at 6 */
#define UART_FCR6_T_TRIGGER_0	0x00 /* Mask for transmit trigger set at 0 */
//#define UART_FCR6_T_TRIGGER_2	0x10 /* Mask for transmit trigger set at 2 */
#define UART_FCR6_T_TRIGGER_2   0x20 /* Mask for transmit trigger set at 2 */
#define UART_FCR6_T_TRIGGER_4   0x30 /* Mask for transmit trigger set at 4 */
/* TI 16750 definitions */
#define UART_FCR7_64BYTE	0x20 /* Go into 64 byte mode */

/*
 * These are the definitions for the Line Control Register
 * 
 * Note: if the word length is 5 bits (UART_LCR_WLEN5), then setting 
 * UART_LCR_STOP will select 1.5 stop bits, not 2 stop bits.
 */
#define UART_LCR_DLAB		0x80	/* Divisor latch access bit */
#define UART_LCR_SBC		0x40	/* Set break control */
#define UART_LCR_SPAR		0x20	/* Stick parity (?) */
#define UART_LCR_EPAR		0x10	/* Even parity select */
#define UART_LCR_PARITY	0x08	/* Parity Enable */
#define UART_LCR_STOP		0x04	/* Stop bits: 0=1 stop bit, 1= 2 stop bits */
#define UART_LCR_WLEN5  	0x00	/* Wordlength: 5 bits */
#define UART_LCR_WLEN6  	0x01	/* Wordlength: 6 bits */
#define UART_LCR_WLEN7  	0x02	/* Wordlength: 7 bits */
#define UART_LCR_WLEN8  	0x03	/* Wordlength: 8 bits */
#define UART_LCR_N81		0x03	// No parity , 8 bits word length , 1 stop bit

/*
 * These are the definitions for the Line Status Register
 */
#define UART_LSR_TEMT	0x40	/* Transmitter empty */
#define UART_LSR_THRE	0x20	/* Transmit-hold-register empty */
#define UART_LSR_BI	0x10	/* Break interrupt indicator */
#define UART_LSR_FE	0x08	/* Frame error indicator */
#define UART_LSR_PE	0x04	/* Parity error indicator */
#define UART_LSR_OE	0x02	/* Overrun error indicator */
#define UART_LSR_DR	0x01	/* Receiver data ready */

/*
 * These are the definitions for the Interrupt Identification Register
 */
#define UART_IIR_NO_INT	0x01	/* No interrupts pending */
#define UART_IIR_ID	    0x0e	/* Mask for the interrupt ID */

#define UART_IIR_MSI	0x00	/* Modem status interrupt */
#define UART_IIR_THRI	0x02	/* Transmitter holding register empty */
#define UART_IIR_RDI	0x04	/* Receiver data interrupt */
#define UART_IIR_RLSI	0x06	/* Receiver line status interrupt */
#define UART_IIR_RTO	0x0c	/* character time-out indication */

/*
 * These are the definitions for the Interrupt Enable Register
 */
#define UART_IER_PTIME  0x80    /* Programmable THRE Interrupt Mode Enable */
#define UART_IER_EDSSI	0x08	/* Enable Modem status interrupt */
#define UART_IER_ELSI	0x04	/* Enable receiver line status interrupt */
#define UART_IER_ETBEI	0x02	/* Enable Transmitter holding register empty interrupt */
#define UART_IER_ERBI	0x01	/* Enable receiver data available interrupt */
/*
 * Sleep mode for ST16650 and TI16750.
 * Note that for 16650, EFR-bit 4 must be selected as well.
 */
#define UART_IERX_SLEEP  0x10	/* Enable sleep mode */

/*
 * The Intel PXA250/210 & IXP425 chips define these bits
 */
#define UART_IER_DMAE	0x80	/* DMA Requests Enable */
#define UART_IER_UUE	0x40	/* UART Unit Enable */
#define UART_IER_NRZE	0x20	/* NRZ coding Enable */
#define UART_IER_RTOIE	0x10	/* Receiver Time Out Interrupt Enable */

/*
 * These are the definitions for the Modem Control Register
 */
#define UART_MCR_AFCE	0x20	/* Auto H/W flow control */
#define UART_MCR_LOOP	0x10	/* Enable loopback test mode */
#define UART_MCR_OUT2	0x08	/* Out2 complement */
#define UART_MCR_OUT1	0x04	/* Out1 complement */
#define UART_MCR_RTS	0x02	/* RTS complement */
#define UART_MCR_DTR	0x01	/* DTR complement */

/*
 * These are the definitions for the Modem Status Register
 */
#define UART_MSR_DCD	0x80	/* Data Carrier Detect */
#define UART_MSR_RI	    	0x40	/* Ring Indicator */
#define UART_MSR_DSR	0x20	/* Data Set Ready */
#define UART_MSR_CTS	0x10	/* Clear to Send */
#define UART_MSR_DDCD	0x08	/* Delta DCD */
#define UART_MSR_TERI	0x04	/* Trailing edge ring indicator */
#define UART_MSR_DDSR	0x02	/* Delta DSR */
#define UART_MSR_DCTS	0x01	/* Delta CTS */
#define UART_MSR_ANY_DELTA 0x0F	/* Any of the delta bits! */

/*
 * The RSA DSV/II board has two fixed clock frequencies.  One is the
 * standard rate, and the other is 8 times faster.
 */
#define OSC_CLK 14745600  
#define SERIAL_RSA_BAUD_BASE (OSC_CLK/16)
#define SERIAL_RSA_BAUD_BASE_LO (SERIAL_RSA_BAUD_BASE / 8)

// enable CTS interrupt
#define DBG_UART_IER_ECTSI		0x80
// eanble RTS interrupt
#define DBG_UART_IER_ERTSI		0x40
// enable Xon/Xoff interrupt
#define DBG_UART_IER_XINT		0x20
// enable GDA interrupt
#define DBG_UART_IER_EGDAI		0x10

#define DBG_UART_RECV_ISR		(UART_IER_ERBI | DBG_UART_IER_EGDAI)

// GDA interrupt pending
#define DBG_UART_IIR_GDA		0x1C
#define DBG_UART_IIR_RDA		0x04
#define DBG_UART_IIR_RTO		0x0C
#define DBG_UART_IIR_LSR		0x06

// recieved Xon/Xoff or specical interrupt pending
#define DBG_UART_IIR_XSC		0x10

// RTS/CTS change state interrupt pending
#define DBG_UART_IIR_RTSCTS		0x20
#define DBG_UART_IIR_MASK		0x3E				// distinguish Interrupt type

#define DBG_UART_MCR_XON_FLAG		0x40
#define DBG_UART_MCR_XON_ANY		0x80
#define DBG_UART_HARDWARE_ID		0x01
#define DBG_UART_HARDWARE_ID1		0x02

// software flow control on chip mask value
#define DBG_UART_EFR_SF_MASK		0x0F
// send Xon1/Xoff1
#define DBG_UART_EFR_SF_TX1		0x08
// send Xon2/Xoff2
#define DBG_UART_EFR_SF_TX2		0x04
// send Xon1,Xon2/Xoff1,Xoff2
#define DBG_UART_EFR_SF_TX12		0x0C
// don't send Xon/Xoff
#define DBG_UART_EFR_SF_TX_NO		0x00
// Tx software flow control mask
#define DBG_UART_EFR_SF_TX_MASK	0x0C
// don't receive Xon/Xoff
#define DBG_UART_EFR_SF_RX_NO		0x00
// receive Xon1/Xoff1
#define DBG_UART_EFR_SF_RX1		0x02
// receive Xon2/Xoff2
#define DBG_UART_EFR_SF_RX2		0x01
// receive Xon1,Xon2/Xoff1,Xoff2
#define DBG_UART_EFR_SF_RX12		0x03
// Rx software flow control mask
#define DBG_UART_EFR_SF_RX_MASK	0x03

#define DBG_UART_RX_TRIGGER		4
#define DBG_UART_TX_FIFO_SIZE	8

#define XON		0x11
#define XOFF		0x13

#define DBG_UART_OPENED					0x00000001
#define DBG_UART_HAS_HW_FLOW_CONTROL	0x00000002
#define DBG_UART_HAS_SW_FLOW_CONTROL	0x00000004
#define DBG_UART_HAS_ENHANCE_MODE		0x80000000
#define DBG_UART_FLOW_CONTROL_MASK		(DBG_UART_HAS_HW_FLOW_CONTROL | DBG_UART_HAS_SW_FLOW_CONTROL)

// following for user API used
#define DATA_BITS_8		UART_LCR_WLEN8
#define DATA_BITS_7		UART_LCR_WLEN7
#define DATA_BITS_6		UART_LCR_WLEN6
#define DATA_BITS_5		UART_LCR_WLEN5
#define PARITY_ENABLE	UART_LCR_PARITY
#define NONE_PARITY		0
#define EVEN_PARITY		(UART_LCR_PARITY|UART_LCR_EPAR)
#define ODD_PARITY		UART_LCR_PARITY
#define MARK_PARITY		(UART_LCR_PARITY|UART_LCR_SPAR)
#define SPACE_PARITY	(UART_LCR_PARITY|UART_LCR_SPAR|UART_LCR_EPAR)

#define STOP_BIT_1		0
#define STOP_BITS_2		UART_LCR_STOP
#define STOP_BITS_1_5	UART_LCR_STOP
#define INTERNAL_LOOPBACK_ON	1
#define INTERNAL_LOOPBACK_OFF	0
#define FLUSH_INPUT		1
#define FLUSH_OUTPUT		2
#define FLUSH_INOUT		3
#define DTR_ON			UART_MCR_DTR
#define RTS_ON			UART_MCR_RTS
#define DCD_ON			UART_MSR_DCD
#define RI_ON			UART_MSR_RI
#define DSR_ON			UART_MSR_DSR
#define CTS_ON			UART_MSR_CTS


/*------------------------------------------------------ Structure ----------------------------------*/


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void dbg_sio_init(void);
int dbg_sio_open(int port, int mode);
int dbg_sio_close(int port);;
int dbg_sio_iqueue(int port);
int dbg_sio_ioctl(int port, int baud, int mode);
int dbg_sio_read(int port, char *buf, int len);
int dbg_sio_write(int port, char *buf, int len);
int dbg_sio_flush(int port, int mode);
int dbg_sio_lctrl(int port, int mode);
int dbg_sio_getch(int port);
void dbg_sio_putch(int port, unsigned char ch);





#endif	// _DBG_UART_H
