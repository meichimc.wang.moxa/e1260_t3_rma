

#define scan_rate	4	//	60/(scan_rate+1)

#define differential_mode	1 //1 : diff. end module, 0 : single end module

#if E1240
#define Input_signal		1 //0 : bipolar mode, 1 : unipolar mode 
#define CHANNEL_NUMBER		8
#define CAL_AVERAGE_NUMBER	32
#define CAL_AVEAGE_SHIFT	5	//32 = 2^5
#define CAL_CONV_LSB		256
#define CAL_ERROR_LSB		3000
#endif

#if E1242
#define Input_signal		1 //0 : bipolar mode, 1 : unipolar mode 
#define CHANNEL_NUMBER		4
#define CAL_AVERAGE_NUMBER	32
#define CAL_AVERAGE_SHIFT	5	//32 = 2^5
#define CAL_CONV_LSB		256
#define CAL_ERROR_LSB		3000
#endif

#if E1260
#define Input_signal		1 //0 : bipolar mode, 1 : unipolar mode 
#define CHANNEL_NUMBER		6
#define CAL_AVERAGE_NUMBER	32
#define CAL_AVERAGE_SHIFT	5	//32 = 2^5
#define CAL_CONV_LSB		256
#define CAL_ERROR_LSB		3000
#endif

#if E1262
#define Input_signal		0 //0 : bipolar mode, 1 : unipolar mode 
#define CHANNEL_NUMBER		8
#define CAL_AVERAGE_NUMBER	32
#define CAL_AVERAGE_SHIFT	5	//32 = 2^5
#define CAL_CONV_LSB		192
#define CAL_ERROR_LSB		1500
#endif

#define CAL_TIME			0xF8
#define CAL_YEAR			0xF5
#define CAL_MONTH			0xF6
#define CAL_DAY				0xF7
#define CAL_FLAG1			0xF2
#define CAL_RFLAG			0xF1
#define CAL_TEMP			0xF3
#define CAL_HUMI			0xF4
#define MODULE_SERIES		0xFB
#define MODULE_ID			0xFF

#if E1240
#define	SINGLE_CHANNEL_EEPROM_DATA	2
#define TOTAL_CAL_SIZE		0x60
#define BACKUP_CAL_START	0x80
#define TEMP_OFFSET			0x60
#define TEMP_GAIN			0x63
#endif

#if E1242
#define	SINGLE_CHANNEL_EEPROM_DATA	2
#define TOTAL_CAL_SIZE		0x30
#define BACKUP_CAL_START	0x80
#define TEMP_OFFSET			0x30
#define TEMP_GAIN			0x33
#endif

#if E1260
#define	SINGLE_CHANNEL_EEPROM_DATA	4
#define TOTAL_CAL_SIZE		0x90
#define BACKUP_CAL_START	0x100
#define TEMP_OFFSET			0x90
#define TEMP_GAIN			0x93
#endif

#if E1262
#define	SINGLE_CHANNEL_EEPROM_DATA	3
#define TOTAL_CAL_SIZE		0x90
#define BACKUP_CAL_START	0x100
#define TEMP_OFFSET			0x90
#define TEMP_GAIN			0x93
#endif

//ADC input port
#if E1240
#define INT_VREF			2
#define	ADR03_TEMP			6
#define	AnalogGnd			7
#define AnalogCom			8	
#define	EXT_VREF			1

#if differential_mode
#define	AnalogP				3	
#define AnalogN				1	
#else
#define	AnalogP				3	
#define AnalogN				4	
#endif

#endif

#if E1242
#define INT_VREF			2
#define	ADR03_TEMP			6
#define	AnalogGnd			7
#define AnalogCom			8	
#define	EXT_VREF			1

#if differential_mode
#define	AnalogP				3	
#define AnalogN				1	
#else
#define	AnalogP				3	
#define AnalogN				4
#endif

#endif

#if E1260
#define INT_VREF			2
#define	ADR03_TEMP			6
#define	AnalogGnd			7
#define AnalogCom			8	
#define	EXT_VREF			5
#define	AnalogP				0	
#define AnalogN				7
#endif

#if E1262
#define INT_VREF			2
#define	ADR03_TEMP			0	//0
#define	AnalogGnd			7
#define AnalogCom			8	
#define	EXT_VREF			1
#define CJC_L				5
#define CJC_R				6
#define	AnalogP				3	
#define AnalogN				4	//4	
#endif

#if E1240 | E1242
#define Voltage_mode		0
#define Current_mode		1

#define CAL_point_H			0xC00000
#define CAL_point_L			0x400000

#define CAL_Vo_HH			7	//CAL voltage out 7.5V
#define	CAL_Vo_HL			5000
#define CAL_Vo_LH			2	//CAL voltage out 2.5V
#define CAL_Vo_LL			5000

#define Cal_point_IH		0xC00000 //16mA
#define Cal_point_IL		0x400000 //8mA

#define CAL_Io_HH			16	//CAL Current out 16mA
#define	CAL_Io_HL			0
#define CAL_Io_LH			8	//CAL Current out 8mA
#define CAL_Io_LL			0

#define LINEARITY_Vo_HH		9	//10 - 10*0.002 = 9.98
#define LINEARITY_Vo_HL		9800
#define LINEARITY_Vo_LH		0	//10*0.002 = 0.02
#define LINEARITY_Vo_LL		200

#define LINEARITY_Io_HH		19	//20 - (20-4) * 0.002 = 19.968
#define LINEARITY_Io_HL		968
#define LINEARITY_Io_LH		4	//(20-4) * 0.002 + 4 = 4.032
#define LINEARITY_Io_LL		32

#define LINEARITY_Vo_point	0.4167
#define LINEARITY_Vo_fpoint	4167

#define LINEARITY_Io_point	0.667
#define LINEARITY_Io_fpoint	667

#define LINEARITY_Io_LOW	4
#endif

#if E1262
#define CAL_Vo_X1H			39
#define CAL_Vo_X1L			63
#define CAL_Vo_X2H			19
#define CAL_Vo_X2L			531
#define CAL_Vo_X4H			9
#define CAL_Vo_X4L			766
#define CAL_point_H			0x400000
#define CAL_point_L			0xC00000
#define LINEARITY_VO_X1H	77
#define LINEARITY_VO_X1L	969
#define LINEARITY_VO_X2H	38
#define LINEARITY_VO_X2L	984
#define LINEARITY_VO_X4H	19
#define LINEARITY_VO_X4L	492

#define LINEARITY_VO_X1point	6.510
#define LINEARITY_VO_X1fpoint	510
#define LINEARITY_VO_X2point	3.255
#define LINEARITY_VO_X2fpoint	255
#define LINEARITY_VO_X4point	1.627
#define LINEARITY_VO_X4fpoint	627
#endif

#if E1260
#define CAL_R_X1LH			625
#define CAL_R_X1LL			000
#define CAL_R_X2LH			312
#define CAL_R_X2LL			500
#define CAL_R_X4LH			156
#define CAL_R_X4LL			250
#define CAL_R_X8LH			78
#define CAL_R_X8LL			125
#define CAL_R_X1HH			1875
#define CAL_R_X1HL			0
#define CAL_R_X2HH			937
#define CAL_R_X2HL			500
#define CAL_R_X4HH			468
#define CAL_R_X4HL			750
#define CAL_R_X8HH			234
#define CAL_R_X8HL			375
#define CAL_point_H			0xC00000
#define CAL_point_L			0x400000
//#define LINEARITY_R_X1H		2495
#define LINEARITY_R_X1H		2434
#define LINEARITY_R_X1L		0
#define LINEARITY_R_X2H		1247
#define LINEARITY_R_X2L		500
#define LINEARITY_R_X4H		623
#define LINEARITY_R_X4L		750
#define LINEARITY_R_X8H		311
#define LINEARITY_R_X8L		880

#define LINEARITY_R_X1point	100
#define LINEARITY_R_X2point	50
#define LINEARITY_R_X4point	25
#define LINEARITY_R_X8point	12
#define LINEARITY_R_X8fpoint	500
#endif



/***********ADC Define table***********/

#define ADC_PGA_GAIN_1		0
#define ADC_PGA_GAIN_2		1
#define ADC_PGA_GAIN_4		2
#define ADC_PGA_GAIN_8		3
#define ADC_PGA_GAIN_16		4
#define ADC_PGA_GAIN_32		5
#define ADC_PGA_GAIN_64		6
#define ADC_PGA_GAIN_128	7

#define ADC_FILTER_AUTO		0
#define ADC_FILTER_FAST		1
#define ADC_FILTER_SINC_2	2
#define ADC_FILTER_SINC_3	3

#define ADC_EXT_REF			0
#define ADC_INT_REF			1

#define ADC_INT_REF_2_5V	1
#define ADC_INT_REF_1_25V	0

#define ADC_M_FACTOR_128	0
#define ADC_M_FACTOR_256	1

#define ADC_SELF_CAL		0
#define ADC_SELF_CAL_OFFSET 1
#define ADC_SELF_CAL_GAIN	2
#define ADC_SYS_CAL_OFFSET	3
#define ADC_SYS_CAL_GAIN	4

#define DIAG_GET_VALUE_DEC_MODE		1

void AI_CH_Select(UINT8 channel);

void ADC_Init(void);
void ADC_PGA_Gain(UINT8 gain);
void ADC_CH_Select(UINT8 VP, UINT8 VN);
void ADC_Data_Sync(void);
void ADC_Filter_Select(UINT8 filter);
UINT32 ADC_Read(void);
void ADC_Data_Rate(UINT16 m_factor, UINT16 decimation_ratio);
void ADC_Ref_Select(UINT8 dir, UINT8 voltage);
void ADC_Calibration(UINT8 mode);
void ADC_SW_Reset(void);
void ADC_RAM_Readback(UINT8 bank, UINT8 *ram);

void copy_ADC_REG_to_RAM(UINT8 bank);
void copy_ADC_RAM_to_REG(UINT8 bank);

void cal_parameters_save(UINT8 channel, UINT8 input_type, UINT32 offset_reg, UINT32 gain_reg);
void cal_parameters_duplicate(UINT8 source_channel, UINT8 target_channel);
void cal_parameters_block_duplicate(UINT8 source_block, UINT8 target_block);
void cal_parameters_input(UINT8 channel, UINT8 gain);

int cal_parameters_load(UINT8 channel, UINT8 input_type);

UINT32 AI_data_transfer(UINT32 value, UINT8 gain);
UINT8 hex2num(char hex);
UINT8 ADC_Register_Readback(UINT16 offset);
UINT8 ADC_Data_Ready(void);
void ADC_input_type_Select(UINT8 input_type);
UINT8 AI_Auto_System_Calibration_routine(UINT8 all_cal,UINT8 input_type);
UINT32 calculate_gain(UINT32 origin_gain,UINT32 gain);
void write_parm_to_ADC(UINT32 offset_tmp, UINT32 gain_tmp);
UINT32 calculate_offset(UINT32 err_high,UINT32 err_low,UINT32 offset);
UINT32 calculate_offsetc(UINT32 err_high,UINT32 err_low,UINT32 offset);
void diag_do_AI_linearity_test(UINT8 all_cal,UINT8 raw);
void System_auto_cal(void);
int cal_parameters_uniformity_check(void);
UINT32 ADC_gain_return(void);
UINT32 ADC_offset_return(void);

/*** data compare ***/
UINT32 CompareMAX(UINT32 Number1, UINT32 Number2);
UINT32 CompareMIN(UINT32 Number1, UINT32 Number2);
UINT32 Compare_different(UINT32 Number1, UINT32 Number2);

/***********AI Test Command***********/
void module_temperature_monitor(void);
void diag_AI_CH_sel(void);
void diag_do_AI_test(void);
void diag_do_noise_loop_test(void);
void diag_do_AI_scan_test(void);
void diag_do_AI_EMI_scan_test(void);
void diag_do_AI_EMS_scan_test(void);
void AI_Auto_System_Calibration_single_ch(void);
void diag_do_AI_linearity_ch_test(void);

#if E1240 | E1242
void diag_do_AI_input_scan_test(void);
void diag_do_AI_Voltage_Calibration(void);
void diag_do_AI_Voltage_linearity_test(void);
void diag_do_AI_Current_Calibration(void);
void diag_do_AI_Current_linearity_test(void);
#endif

#if E1262
void diag_do_TC_burn_out_detection_test(void);
void diag_do_TC_full_linearity_test(void);
void diag_do_TC_Auto_System_Calibration_single_gain(void);
void diag_do_TC_gain_linearity_test(void);
void TC_Full_Auto_System_Calibration(void);
#endif

#if E1260
void diag_do_RTD_burn_out_detection_test(void);
void diag_do_RTD_full_linearity_test(void);
void diag_do_RTD_Auto_System_Calibration_single_gain(void);
void diag_do_RTD_gain_linearity_test(void);
void RTD_Full_Auto_System_Calibration(void);
#endif
/***********ADC Setting Command***********/
void diag_AI_set_ref(void);  
void diag_AI_set_ref_in_voltage(void); 
void diag_AI_ADC_CH_sel_vp(void);  
void diag_AI_ADC_CH_sel_vn(void);
void diag_AI_ADC_m_factor(void);
void diag_AI_ADC_decimation_ratio(void);
void diag_AI_ADC_PGA_gain(void);
void diag_AI_ADC_filter_sel(void);
void diag_AI_ADC_calibration_sel(void);
void diag_AI_ADC_Memory_dump(void);
void diag_AI_ADC_unipolar_set(void);
void AI_TYPE_CH_Select(UINT8 channel);
UINT8 AI_TYPE_READ(void);

/***********MP test***********/
int diag_chip_ADC(void);
int diag_chip_SPI(void);
int diag_chip_Mux(void);
int diag_chip_ADC_PGA(void);
void diag_do_AI_MP_calibration(void);
void diag_do_AI_WARM_UP(void);
#if E1240 | E1242
UINT8 diag_burn_in_AI(void);
#endif
#if E1262
UINT8 diag_burn_in_TC(UINT8 fix_gain);
#endif
#if E1260
UINT8 diag_burn_in_RTD(UINT8 fix_gain);
#endif

/***********Cal. parmeter Setting Command***********/
void cal_parameters_dump(void);
void diag_cal_parameters_input(void);
void diag_cal_parameters_duplicate(void);
void diag_cal_parameters_block_duplicate(void);

/***********Cal. fix Command***********/
void calibration_pass(void);
void calibration_fail(void);
void calibration_retry(void);
