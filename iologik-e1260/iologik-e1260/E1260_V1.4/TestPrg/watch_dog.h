/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	watch_dog.h

	Function declarations of watch dog.

	--- S2E WDT Counting Table ---
	
	Power  APB_BUS 20 25 30 35 40 45 50       
	16  3,276,800  2,621,440  2,184,533  1,872,457  1,638,400  1,456,356  1,310,720   
	17  6,553,600  5,242,880  4,369,067  3,744,914  3,276,800  2,912,711  2,621,440   
	18  13,107,200  10,485,760  8,738,133  7,489,829  6,553,600  5,825,422  5,242,880   
	19  26,214,400  20,971,520  17,476,267  14,979,657  13,107,200  11,650,844  10,485,760   
	20  52,428,800  41,943,040  34,952,533  29,959,314  26,214,400  23,301,689  20,971,520   
	21  104,857,600  83,886,080  69,905,067  59,918,629  52,428,800  46,603,378  41,943,040   
	22  209,715,200  167,772,160  139,810,133  119,837,257  104,857,600  93,206,756  83,886,080   
	23  419,430,400  335,544,320  279,620,267  239,674,514  209,715,200  186,413,511  167,772,160   
	24  838,860,800  671,088,640  559,240,533  479,349,029  419,430,400  372,827,022  335,544,320   
	25  1,677,721,600  1,342,177,280  1,118,481,067  958,698,057  838,860,800  745,654,044  671,088,640   
	26  3,355,443,200  2,684,354,560  2,236,962,133  1,917,396,114  1,677,721,600  1,491,308,089  1,342,177,280   
	27  6,710,886,400  5,368,709,120  4,473,924,267  3,834,792,229  3,355,443,200  2,982,616,178  2,684,354,560   
	28  13,421,772,800  10,737,418,240  8,947,848,533  7,669,584,457  6,710,886,400  5,965,232,356  5,368,709,120   
	29  26,843,545,600  21,474,836,480  17,895,697,067  15,339,168,914  13,421,772,800  11,930,464,711  10,737,418,240   
	30  53,687,091,200  42,949,672,960  35,791,394,133  30,678,337,829  26,843,545,600  23,860,929,422  21,474,836,480   
	31  107,374,182,400  85,899,345,920  71,582,788,267  61,356,675,657  53,687,091,200  47,721,858,844  42,949,672,960  
	
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified		
    
*/

#ifndef __WATCH_DOG_H
#define __WATCH_DOG_H



/*------------------------------------------------------ Macro / Define ----------------------------*/
// S2E WDT Register Offset
#define WDT_CONTROL			0x00			// control register
#define WDT_TORR				0x04			// timeout range register
#define WDT_CCVR				0x08			// current count values
#define WDT_CRR					0x0C			// counter restart register
#define WDT_STAT				0x10			// WDT Interrupt status
#define WDT_EOI					0x14			// WDT End Of Interrupt
#define WDT_COMP_PARAMS_5		0xE4
#define WDT_COMP_PARAMS_4		0xE8
#define WDT_COMP_PARAMS_3		0xEC
#define WDT_COMP_PARAMS_2		0xF0
#define WDT_COMP_PARAMS_1		0xF4

// S2E Control Register
#define WDT_ENABLE					BIT0
#define WDT_RESPONSE_MODE			BIT1
#define WDT_RESET_PULSE_LENGTH	0x1C
#define WDT_AUTO_RELOAD			0x76
#define CLOCK_CONTROL_BASE			0x05090000
#define WDT_PAUSE					BIT6
 

/*------------------------------------------------------ Structure ----------------------------------*/
typedef enum _watchdog_reset_mode{
	watchdog_reset_mode_immediate = 0,
	watchdog_reset_mode_interrupt = 1
 }watchdog_reset_mode;


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void WatchDog_SetIsr(void);
void WatchDog_EnableIrq(void);
void WatchDog_Enable(void);
void WatchDog_Enable_Clock(void);
void WatchDog_SetMode(watchdog_reset_mode reset_mode);
void WatchDog_ReStart(void);
void WatchDog_SetTopInit(UINT32 power);
void WatchDog_SetTop(UINT32 power);
void WatchDog_ClearStatus(void);
void WatchDog_SetResetPulseLength(UINT32 length);
UINT32 WatchDog_ReadCounter(void);
int WatchDog_IsInterruptPending(void);






#endif // __WATCH_DOG_H
