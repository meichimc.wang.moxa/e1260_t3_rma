/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	spi.h

	Routines for accessing SPI flash and GPIO expander

	2008-06-10	Albert Yu
		new release
	2008-10-17	Chin-Fu Yang		
		modified	
		
*/

#ifndef _SPI_H
#define _SPI_H




/*------------------------------------------------------ Macro / Define -----------------------------*/
/*
 * Flash type definition
 */
#define FLASH_TYPE_MX25L       	 0

//#define TX_ISR_MODE
#define SPI_WAIT_MAX_CNT		10000			// wait loop

/*
 * MX25L1635D SPI flash command definition
 */
#define MX_WREN			0x06	/* Write Enable */
#define MX_WRDI			0x04	/* Write Disable */
#define MX_RDID 			0x9F	/* Read ID */
#define MX_RDSR			0x05	/* Read Status Register */
#define MX_WRSR			0x01	/* Write Status Register */
#define MX_READ			0x03	/* Read Data */
#define MX_FAST_READ	0x0B	/* Fast Read Data */
#define MX_SE			0x20	/* Sector Erase */
#define MX_BE			0xD8	/* Block Erase */
#define MX_CE			0x60	/* Chip Erase */
#define MX_PP			0x02	/* Page Program */
#define MX_CP			0xAD	/* Continuously Program */

#define MX_STATUS_BIT_WIP	0x01	/* Write In Progress */
#define MX_STATUS_BIT_WEL	0x02	/* Write Enable Latch */

#define MX_SECTOR_SIZE  	4096	// bytes
#define MX_PAGE_SIZE    		256		// bytes

/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void spi_init(void);
int spi_wait_TX_FIFO_empty(void);
int spi_wait_not_busy(void);
void spi_show_rx_overflow(void);

UINT32 GetSectorSize(int type, int sector);
int mx_read_status_reg(void);
int mx_write_status_reg(int value);
int mx_read_data(UINT32 sdramDestAddr, UINT32 flashSrcAddr, int len);
int mx_write_data(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len);
int mx_sector_erase(UINT32 flashAddr);
int mx_block_erase(UINT32 flashAddr);
int mx_chip_erase(void);
int mx_page_program(UINT32 flashDestAddr, UINT32 sdramSrcAddr, int len);
ulong GetNetModuleID(void);
void spi_isr(void);	/* use assembly code to catch up the SPI transmission speed */

#endif
