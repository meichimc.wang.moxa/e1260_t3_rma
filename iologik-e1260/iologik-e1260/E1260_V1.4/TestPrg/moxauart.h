/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	moxauart.h

	Offset of registers of MOXA UART.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified			
    
*/


#ifndef _MOXAUART_H
#define _MOXAUART_H



/*------------------------------------------------------ Macro / Define ----------------------------*/
#define XON		0x11
#define XOFF		0x13

#define READ_ONLY	0x00000001
#define WRITE_ONLY	0x00000002
#define MOXAUART_BUFFER_SIZE	UART_BUF_MAX_SIZE
#define MOXAUART_BUFFER_MASK	(MOXAUART_BUFFER_SIZE - 1)
#define MOXAUART_CPU_MAX	 (2+1)	// 2 MOXAUART + 1 General UART
#define MOXAUART_MAX_PORT	(MOXAUART_CPU_MAX)
#define GENERAL_UART_PORT	2	// MOXAUART = 0 , 1 ; General UART = 2

#define SUPPORT_BUSES	3

#define MAX_PCI_SLOT 0			// No PCI Interface in S2E

#define MXSER_BUS_ISA	0
#define MXSER_BUS_PCI	1
#define MXSER_BUS_APB   2 // MOXA CPU embedded UART


#define MOXA_UART_INT_VCT_OFFSET			0x30
#define MOXA_UART_MODE_REG_OFFSET		0x38
#define MOXA_EMBEDDED_UART_BASE_REG		S2E_MOXA_UART_BASE
#define MOXA_EMBEDDED_UART_BASE_INT		(S2E_MOXA_UART_BASE + (MOXA_UART_INT_VCT_OFFSET * 4))
#define MOXA_EMBEDDED_UART_BASE_MODE	(S2E_MOXA_UART_BASE + (MOXA_UART_MODE_REG_OFFSET * 4))


#define UART_RBR	0
#define UART_THR	0
#define UART_RX	0	/* In:  Receive buffer (DLAB=0) */
#define UART_TX	0	/* Out: Transmit buffer (DLAB=0) */
#define UART_DLL	0	/* Out: Divisor Latch Low (DLAB=1) */
#define UART_TRG	0	/* (LCR=BF) FCTR bit 7 selects Rx or Tx
				 * In: Fifo count
				 * Out: Fifo custom trigger levels
				 * XR16C85x only */

#define UART_DLM	1	/* Out: Divisor Latch High (DLAB=1) */
#define UART_IER	1	/* Out: Interrupt Enable Register */
#define UART_FCTR	1	/* (LCR=BF) Feature Control Register
				 * XR16C85x only */

#define UART_IIR	2	/* In:  Interrupt ID Register */
#define UART_FCR	2	/* Out: FIFO Control Register */
#define UART_EFR	2	/* I/O: Extended Features Register */
				/* (DLAB=1, 16C660 only) */

#define UART_LCR	3	/* Out: Line Control Register */
#define UART_MCR	4	/* Out: Modem Control Register */
#define UART_LSR	5	/* In:  Line Status Register */
#define UART_MSR	6	/* In:  Modem Status Register */
#define UART_SCR	7	/* I/O: Scratch Register */
#define UART_EMSR	7	/* (LCR=BF) Extended Mode Select Register 
				 * FCTR bit 6 selects SCR or EMSR
				 * XR16c85x only */

/* definition of extended features of Moxa UART */
#define E_UART_EFR		0x02	// Extended Features Register
#define E_UART_GDL		0x07	// Good Data Length

#define E_UART_XON1	0x04	// Store character of XON1
#define E_UART_XON2	0x05	// Store character of XON2
#define E_UART_XOFF1	0x06	// Store character of XOFF1
#define E_UART_XOFF2	0x07	// Store character of XOFF2

#define E_UART_RBRTL	0x04	// Low water , Control RTS 
#define E_UART_RBRTH	0x05	// High water , Control RTS 
#define E_UART_RBRTI	0x06	// Intermediate water , Generating Interrupt
#define E_UART_THRTL	0x07	// Generating Interrupt

#define E_UART_ENUM	0x04	
#define E_UART_HWID	0x05	
#define E_UART_ECR		0x06	
#define E_UART_CSR		0x07	







/*
 * These are the definitions for the FIFO Control Register
 * (16650 only)
 */
#define UART_MCR_AFE			0x20
#define UART_LSR_SPECIAL		0x1E
#define UART_FCR_ENABLE_FIFO	0x01 /* Enable the FIFO */
#define UART_FCR_CLEAR_RCVR	0x02 /* Clear the RCVR FIFO */
#define UART_FCR_CLEAR_XMIT	0x04 /* Clear the XMIT FIFO */
#define UART_FCR_DMA_SELECT	0x08 /* For DMA applications */
#define UART_FCR_TRIGGER_MASK	0xC0 /* Mask for the FIFO trigger range */
#define UART_FCR_TRIGGER_1		0x00 /* Mask for trigger set at 1 */
#define UART_FCR_TRIGGER_4		0x40 /* Mask for trigger set at 4 */
#define UART_FCR_TRIGGER_8		0x80 /* Mask for trigger set at 8 */
#define UART_FCR_TRIGGER_14	0xC0 /* Mask for trigger set at 14 */
/* 16650 redefinitions */
#define UART_FCR6_R_TRIGGER_8		0x00 /* Mask for receive trigger set at 1 */
#define UART_FCR6_R_TRIGGER_16	0x40 /* Mask for receive trigger set at 4 */
#define UART_FCR6_R_TRIGGER_24  	0x80 /* Mask for receive trigger set at 8 */
#define UART_FCR6_R_TRIGGER_28	0xC0 /* Mask for receive trigger set at 14 */
#define UART_FCR6_T_TRIGGER_16	0x00 /* Mask for transmit trigger set at 16 */
#define UART_FCR6_T_TRIGGER_8		0x10 /* Mask for transmit trigger set at 8 */
#define UART_FCR6_T_TRIGGER_24  	0x20 /* Mask for transmit trigger set at 24 */
#define UART_FCR6_T_TRIGGER_30	0x30 /* Mask for transmit trigger set at 30 */
/* TI 16750 definitions */
#define UART_FCR7_64BYTE			0x20 /* Go into 64 byte mode */

/*
 * These are the definitions for the Line Control Register
 * 
 * Note: if the word length is 5 bits (UART_LCR_WLEN5), then setting 
 * UART_LCR_STOP will select 1.5 stop bits, not 2 stop bits.
 */
#define UART_LCR_DLAB	0x80	/* Divisor latch access bit */
#define UART_LCR_SBC	0x40	/* Set break control */
#define UART_LCR_SPAR	0x20	/* Stick parity (?) */
#define UART_LCR_EPAR	0x10	/* Even parity select */
#define UART_LCR_PARITY	0x08	/* Parity Enable */
#define UART_LCR_STOP	0x04	/* Stop bits: 0=1 stop bit, 1= 2 stop bits */
#define UART_LCR_WLEN5  0x00	/* Wordlength: 5 bits */
#define UART_LCR_WLEN6  0x01	/* Wordlength: 6 bits */
#define UART_LCR_WLEN7  0x02	/* Wordlength: 7 bits */
#define UART_LCR_WLEN8  0x03	/* Wordlength: 8 bits */
#define UART_LCR_N81		0x03	// No parity , 8 bits word length , 1 stop bit


/*
 * These are the definitions for the Line Status Register
 */
#define UART_LSR_TEMT	0x40	/* Transmitter empty */
#define UART_LSR_THRE	0x20	/* Transmit-hold-register empty */
#define UART_LSR_BI	0x10	/* Break interrupt indicator */
#define UART_LSR_FE	0x08	/* Frame error indicator */
#define UART_LSR_PE	0x04	/* Parity error indicator */
#define UART_LSR_OE	0x02	/* Overrun error indicator */
#define UART_LSR_DR	0x01	/* Receiver data ready */

/*
 * These are the definitions for the Interrupt Identification Register
 */
#define UART_IIR_NO_INT	0x01	/* No interrupts pending */
#define UART_IIR_ID	0x0e	/* Mask for the interrupt ID */

#define UART_IIR_MSI	0x00	/* Modem status interrupt */
#define UART_IIR_THRI	0x02	/* Transmitter holding register empty */
#define UART_IIR_RDI	0x04	/* Receiver data interrupt */
#define UART_IIR_RLSI	0x06	/* Receiver line status interrupt */
#define UART_IIR_RTO	0x0c	/* character time-out indication */
#define UART_IIR_XCH	0x20	/* XON/XOFF Interrupt */
#define UART_IIR_RCH    0x40	/* RTS/CTS change Interrupt */

/*
 * These are the definitions for the Interrupt Enable Register
 */
#define UART_IER_EDSSI	0x08	/* Enable Modem status interrupt */
#define UART_IER_ELSI	0x04	/* Enable receiver line status interrupt */
#define UART_IER_ETBEI	0x02	/* Enable Transmitter holding register empty interrupt */
#define UART_IER_ERBI	0x01	/* Enable receiver data available interrupt */
/*
 * Sleep mode for ST16650 and TI16750.
 * Note that for 16650, EFR-bit 4 must be selected as well.
 */
#define UART_IERX_SLEEP  0x10	/* Enable sleep mode */

/*
 * The Intel PXA250/210 & IXP425 chips define these bits
 */
#define UART_IER_DMAE	0x80	/* DMA Requests Enable */
#define UART_IER_UUE	0x40	/* UART Unit Enable */
#define UART_IER_NRZE	0x20	/* NRZ coding Enable */
#define UART_IER_RTOIE	0x10	/* Receiver Time Out Interrupt Enable */

/*
 * These are the definitions for the Modem Control Register
 */
#define UART_MCR_LOOP	0x10	/* Enable loopback test mode */
#define UART_MCR_OUT2	0x08	/* Out2 complement */
#define UART_MCR_OUT1	0x04	/* Out1 complement */
#define UART_MCR_RTS	0x02	/* RTS complement */
#define UART_MCR_DTR	0x01	/* DTR complement */

/*
 * These are the definitions for the Modem Status Register
 */
#define UART_MSR_DCD	0x80	/* Data Carrier Detect */
#define UART_MSR_RI	    0x40	/* Ring Indicator */
#define UART_MSR_DSR	0x20	/* Data Set Ready */
#define UART_MSR_CTS	0x10	/* Clear to Send */
#define UART_MSR_DDCD	0x08	/* Delta DCD */
#define UART_MSR_TERI	0x04	/* Trailing edge ring indicator */
#define UART_MSR_DDSR	0x02	/* Delta DSR */
#define UART_MSR_DCTS	0x01	/* Delta CTS */
#define UART_MSR_ANY_DELTA 0x0F	/* Any of the delta bits! */

/*
 * These are the definitions for the Extended Features Register
 * (StarTech 16C660 only, when DLAB=1)
 */
#define UART_EFR_CTS	0x80	/* CTS flow control */
#define UART_EFR_RTS	0x40	/* RTS flow control */
#define UART_EFR_SCD	0x20	/* Special character detect */
#define UART_EFR_ECB	0x10	/* Enhanced control bit */
/*
 * the low four bits control software flow control
 */

/*
 * These register definitions are for the 16C950
 */
#define UART_ASR	0x01	/* Additional Status Register */
#define UART_RFL	0x03	/* Receiver FIFO level */
#define UART_TFL 	0x04	/* Transmitter FIFO level */
#define UART_ICR	0x05	/* Index Control Register */

/* The 16950 ICR registers */
#define UART_ACR	0x00	/* Additional Control Register */
#define UART_CPR	0x01	/* Clock Prescalar Register */
#define UART_TCR	0x02	/* Times Clock Register */
#define UART_CKS	0x03	/* Clock Select Register */
#define UART_TTL	0x04	/* Transmitter Interrupt Trigger Level */
#define UART_RTL	0x05	/* Receiver Interrupt Trigger Level */
#define UART_FCL	0x06	/* Flow Control Level Lower */
#define UART_FCH	0x07	/* Flow Control Level Higher */
#define UART_ID1	0x08	/* ID #1 */
#define UART_ID2	0x09	/* ID #2 */
#define UART_ID3	0x0A	/* ID #3 */
#define UART_REV	0x0B	/* Revision */
#define UART_CSR	0x0C	/* Channel Software Reset */
#define UART_NMR	0x0D	/* Nine-bit Mode Register */
#define UART_CTR	0xFF

/*
 * The 16C950 Additional Control Reigster
 */
#define UART_ACR_RXDIS	0x01	/* Receiver disable */
#define UART_ACR_TXDIS	0x02	/* Receiver disable */
#define UART_ACR_DSRFC	0x04	/* DSR Flow Control */
#define UART_ACR_TLENB	0x20	/* 950 trigger levels enable */
#define UART_ACR_ICRRD	0x40	/* ICR Read enable */
#define UART_ACR_ASREN	0x80	/* Additional status enable */

/*
 * These are the definitions for the Feature Control Register
 * (XR16C85x only, when LCR=bf; doubles with the Interrupt Enable
 * Register, UART register #1)
 */
#define UART_FCTR_RTS_NODELAY	0x00  /* RTS flow control delay */
#define UART_FCTR_RTS_4DELAY	0x01
#define UART_FCTR_RTS_6DELAY	0x02
#define UART_FCTR_RTS_8DELAY	0x03
#define UART_FCTR_IRDA		0x04  /* IrDa data encode select */
#define UART_FCTR_TX_INT	0x08  /* Tx interrupt type select */
#define UART_FCTR_TRGA		0x00  /* Tx/Rx 550 trigger table select */
#define UART_FCTR_TRGB		0x10  /* Tx/Rx 650 trigger table select */
#define UART_FCTR_TRGC		0x20  /* Tx/Rx 654 trigger table select */
#define UART_FCTR_TRGD		0x30  /* Tx/Rx 850 programmable trigger select */
#define UART_FCTR_SCR_SWAP	0x40  /* Scratch pad register swap */
#define UART_FCTR_RX		0x00  /* Programmable trigger mode select */
#define UART_FCTR_TX		0x80  /* Programmable trigger mode select */

/*
 * These are the definitions for the Enhanced Mode Select Register
 * (XR16C85x only, when LCR=bf and FCTR bit 6=1; doubles with the
 * Scratch register, UART register #7)
 */
#define UART_EMSR_FIFO_COUNT	0x01  /* Rx/Tx select */
#define UART_EMSR_ALT_COUNT	0x02  /* Alternating count select */

/*
 * These are the definitions for the Programmable Trigger
 * Register (XR16C85x only, when LCR=bf; doubles with the UART RX/TX
 * register, UART register #0)
 */
#define UART_TRG_1		0x01
#define UART_TRG_4		0x04
#define UART_TRG_8		0x08
#define UART_TRG_16		0x10
#define UART_TRG_32		0x20
#define UART_TRG_64		0x40
#define UART_TRG_96		0x60
#define UART_TRG_120	0x78
#define UART_TRG_128	0x80

/*
 * These definitions are for the RSA-DV II/S card, from
 *
 * Kiyokazu SUTO <suto@ks-and-ks.ne.jp>
 */

#define UART_RSA_BASE (-8)

#define UART_RSA_MSR ((UART_RSA_BASE) + 0) /* I/O: Mode Select Register */

#define UART_RSA_MSR_SWAP (1 << 0) /* Swap low/high 8 bytes in I/O port addr */
#define UART_RSA_MSR_FIFO (1 << 2) /* Enable the external FIFO */
#define UART_RSA_MSR_FLOW (1 << 3) /* Enable the auto RTS/CTS flow control */
#define UART_RSA_MSR_ITYP (1 << 4) /* Level (1) / Edge triger (0) */

#define UART_RSA_IER ((UART_RSA_BASE) + 1) /* I/O: Interrupt Enable Register */

#define UART_RSA_IER_Rx_FIFO_H (1 << 0) /* Enable Rx FIFO half full int. */
#define UART_RSA_IER_Tx_FIFO_H (1 << 1) /* Enable Tx FIFO half full int. */
#define UART_RSA_IER_Tx_FIFO_E (1 << 2) /* Enable Tx FIFO empty int. */
#define UART_RSA_IER_Rx_TOUT 	(1 << 3) /* Enable char receive timeout int */
#define UART_RSA_IER_TIMER 	(1 << 4) /* Enable timer interrupt */

#define UART_RSA_SRR ((UART_RSA_BASE) + 2) /* IN: Status Read Register */

#define UART_RSA_SRR_Tx_FIFO_NEMP (1 << 0) /* Tx FIFO is not empty (1) */
#define UART_RSA_SRR_Tx_FIFO_NHFL (1 << 1) /* Tx FIFO is not half full (1) */
#define UART_RSA_SRR_Tx_FIFO_NFUL (1 << 2) /* Tx FIFO is not full (1) */
#define UART_RSA_SRR_Rx_FIFO_NEMP (1 << 3) /* Rx FIFO is not empty (1) */
#define UART_RSA_SRR_Rx_FIFO_NHFL (1 << 4) /* Rx FIFO is not half full (1) */
#define UART_RSA_SRR_Rx_FIFO_NFUL (1 << 5) /* Rx FIFO is not full (1) */
#define UART_RSA_SRR_Rx_TOUT (1 << 6) /* Character reception timeout occurred (1) */
#define UART_RSA_SRR_TIMER (1 << 7) /* Timer interrupt occurred */

#define UART_RSA_FRR ((UART_RSA_BASE) + 2) /* OUT: FIFO Reset Register */

#define UART_RSA_TIVSR ((UART_RSA_BASE) + 3) /* I/O: Timer Interval Value Set Register */

#define UART_RSA_TCR ((UART_RSA_BASE) + 4) /* OUT: Timer Control Register */

#define UART_RSA_TCR_SWITCH (1 << 0) /* Timer on */

/*
 * The RSA DSV/II board has two fixed clock frequencies.  One is the
 * standard rate, and the other is 8 times faster.
 */
#define OSC_CLK 14745600  
#define SERIAL_RSA_BAUD_BASE (OSC_CLK/16)
#define SERIAL_RSA_BAUD_BASE_LO (SERIAL_RSA_BAUD_BASE / 8)

//
// follow just for Moxa Must chip define.
//
// when LCR register (offset 0x03) write following value,
// the Must chip will enter enchance mode. And write value
// on EFR (offset 0x02) bit 6,7 to change bank.
#define MOXA_MUST_ENTER_ENCHANCE	0xBF

// when enhance mode enable, access on general bank register
//#define MOXA_MUST_GDL_REGISTER		0x07
#define MOXA_MUST_GDL_MASK			0x7F
#define MOXA_MUST_GDL_HAS_BAD_DATA	0x80

// enchance register bank select and enchance mode setting register
// when LCR register equal to 0xBF
//#define MOXA_MUST_EFR_REGISTER		0x02
// enchance mode enable
#define MOXA_MUST_EFR_EFRB_ENABLE	0x10
// enchance register bank set 0, 1, 2
#define MOXA_MUST_EFR_BANK0			0x00
#define MOXA_MUST_EFR_BANK1			0x40
#define MOXA_MUST_EFR_BANK2			0x80
#define MOXA_MUST_EFR_BANK3			0xC0
#define MOXA_MUST_EFR_BANK_MASK		0xC0

// good data mode enable
#define MOXA_MUST_FCR_GDA_MODE_ENABLE	0x20
// only good data put into RxFIFO
#define MOXA_MUST_FCR_GDA_ONLY_ENABLE	0x10

// enable CTS interrupt
#define MOXA_MUST_IER_ECTSI		0x80
// eanble RTS interrupt
#define MOXA_MUST_IER_ERTSI		0x40
// enable Xon/Xoff interrupt
#define MOXA_MUST_IER_XINT		0x20
// enable GDA interrupt
#define MOXA_MUST_IER_EGDAI		0x10

#define MOXA_MUST_RECV_ISR		(UART_IER_ERBI | MOXA_MUST_IER_EGDAI)

// GDA interrupt pending
#define MOXA_MUST_IIR_GDA		0x1C		// 
#define MOXA_MUST_IIR_RDA		0x04		// Received data available
#define MOXA_MUST_IIR_RTO		0x0C		// Character Timeout indication
#define MOXA_MUST_IIR_LSR		0x06

// recieved Xon/Xoff or specical interrupt pending
#define MOXA_MUST_IIR_XSC		0x10

// RTS/CTS change state interrupt pending
#define MOXA_MUST_IIR_RTSCTS		0x20
#define MOXA_MUST_IIR_MASK			0x3E				// distinguish Interrupt type

#define MOXA_MUST_MCR_XON_FLAG		0x40
#define MOXA_MUST_MCR_XON_ANY		0x80
#define MOXA_MUST_HARDWARE_ID		0x01
#define MOXA_MUST_HARDWARE_ID1		0x02

// software flow control on chip mask value
#define MOXA_MUST_EFR_SF_MASK		0x0F
// send Xon1/Xoff1
#define MOXA_MUST_EFR_SF_TX1			0x08
// send Xon2/Xoff2
#define MOXA_MUST_EFR_SF_TX2			0x04
// send Xon1,Xon2/Xoff1,Xoff2
#define MOXA_MUST_EFR_SF_TX12			0x0C
// don't send Xon/Xoff
#define MOXA_MUST_EFR_SF_TX_NO		0x00
// Tx software flow control mask
#define MOXA_MUST_EFR_SF_TX_MASK		0x0C
// don't receive Xon/Xoff
#define MOXA_MUST_EFR_SF_RX_NO		0x00
// receive Xon1/Xoff1
#define MOXA_MUST_EFR_SF_RX1			0x02
// receive Xon2/Xoff2
#define MOXA_MUST_EFR_SF_RX2			0x01
// receive Xon1,Xon2/Xoff1,Xoff2
#define MOXA_MUST_EFR_SF_RX12			0x03
// Rx software flow control mask
#define MOXA_MUST_EFR_SF_RX_MASK		0x03

#define MOXA_MUST_MIN_XOFFLIMIT		66
#define MOXA_MUST_MIN_XONLIMIT		20

#define MOXA_MUST_DEFAULT_RBRL_VALUE    	4
#define MOXA_MUST_DEFAULT_RBRH_VALUE	96
#define MOXA_MUST_TX_FIFO_SIZE			128

#define GENERAL_UART_TX_FIFO_SIZE		8

// _Moxa_UART_Struct->flag
// operational_mode
#define MOXAUART_NORMAL_MODE				0x00000000
#define MOXAUART_OPENED					0x00000001
#define MOXAUART_HAS_HW_FLOW_CONTROL	0x00000002
#define MOXAUART_HAS_SW_FLOW_CONTROL	0x00000004
#define MOXAUART_FIFO_ENABLED				0x00000008
#define MOXAUART_TX_POLLING_MODE			0x00000010
#define MOXAUART_HAS_ENHANCE_MODE		0x80000000
#define MOXAUART_FLOW_CONTROL_MASK		(MOXAUART_HAS_HW_FLOW_CONTROL | MOXAUART_HAS_SW_FLOW_CONTROL)

// following for user API used
#define DATA_BITS_8		UART_LCR_WLEN8
#define DATA_BITS_7		UART_LCR_WLEN7
#define DATA_BITS_6		UART_LCR_WLEN6
#define DATA_BITS_5		UART_LCR_WLEN5
#define PARITY_ENABLE	UART_LCR_PARITY
#define NONE_PARITY		0
#define EVEN_PARITY		(UART_LCR_PARITY|UART_LCR_EPAR)
#define ODD_PARITY		UART_LCR_PARITY
#define MARK_PARITY		(UART_LCR_PARITY|UART_LCR_SPAR)
#define SPACE_PARITY	(UART_LCR_PARITY|UART_LCR_SPAR|UART_LCR_EPAR)

#define STOP_BIT_1		0
#define STOP_BITS_2		UART_LCR_STOP
#define STOP_BITS_1_5	UART_LCR_STOP
#define INTERNAL_LOOPBACK_ON	1
#define INTERNAL_LOOPBACK_OFF	0
#define FLUSH_INPUT			1
#define FLUSH_OUTPUT		2
#define FLUSH_INOUT			3
#define DTR_ON			UART_MCR_DTR
#define RTS_ON			UART_MCR_RTS
#define DTR_OFF			0x40
#define RTS_OFF			0x80
#define DCD_ON			UART_MSR_DCD
#define RI_ON			UART_MSR_RI
#define DSR_ON			UART_MSR_DSR
#define CTS_ON			UART_MSR_CTS

// Interface Mode
#define RS232_MODE			0
#define RS485_2WIRE_MODE	1
#define RS422_MODE			2
#define RS485_4WIRE_MODE	3
#define INTERFACE_MODE_MASK	(RS232_MODE | RS485_2WIRE_MODE | RS422_MODE | RS485_4WIRE_MODE)


// error code define for MOXAUART
#define MOXAUART_OK							0
#define MOXAUART_ERROR_NOT_FOUND			(-1)
#define MOXAUART_ERROR_NOT_OPENED			(-2)
#define MOXAUART_ERROR_NO_SUCH_PORT			(-3)
#define MOXAUART_ERROR_PARAMETER				(-4)
// error code define for General UART (Debug UART)
#define GENERAL_UART_OK						0
#define GENERAL_UART_ERROR_NOT_FOUND		(-1)
#define GENERAL_UART_ERROR_NOT_OPENED		(-2)
#define GENERAL_UART_ERROR_NO_SUCH_PORT		(-3)
#define GENERAL_UART_ERROR_PARAMETER		(-4)




// macro define //
#define CheckMoxaUARTFound()	{	\
	if ( !moxauart_found_flag )	\
		return MOXAUART_ERROR_NOT_FOUND;	\
}
#define CheckPortNumber(_p_)	{	\
	if ( (_p_) < 0 || (_p_) >= MoxauartFoundPortNo )	\
		return MOXAUART_ERROR_NO_SUCH_PORT;	\
}
#define CheckOpened(_p_)	{	\
	if ( !(moxauart_info[(_p_)].flag & MOXAUART_OPENED) )	\
		return MOXAUART_ERROR_NOT_OPENED;	\
}
#define BasicCheck1(_p_)	{	\
	CheckMoxaUARTFound();	\
	CheckPortNumber(_p_);	\
}
#define BasicCheck2(_p_)	{	\
	BasicCheck1(_p_);	\
	CheckOpened(_p_);	\
}

/*------------------------------------------------------ Structure ----------------------------------*/
struct _Moxa_UART_Struct {
	UINT32	base;
	UINT8	vector_mask;			// this mask can distinguish which UART generates the interrupt
	UINT8	IER;
	UINT8	FCR;
	UINT8	LCR;
	UINT8   	DLL;
	UINT8	DLM;
	UINT8	MCR;
	UINT8	EFR;
	UINT8	XON1;				// X Character
	UINT8	XON2;
	UINT8	XOFF1;								
	UINT8	XOFF2;
	UINT8	RBRTL;				// Setting water level of Enhanced Mode
	UINT8	RBRTH;									
	UINT8	RBRTI;
	UINT8	THRTL;
	UINT8	ENUM;				// Setting Any Baudrate 
	UINT32	txrptr, txwptr;
	UINT32	rxrptr, rxwptr;
	unsigned char	*txbuf;
	unsigned char   *rxbuf;
	UINT32	flag;					// Is UART in Enhanced Mode or not
	UINT32	bus_type;				//  APB , PCI , ISA 
	UINT32	mode_reg0; //Tony Li.	
	UINT32	mode_reg1; //Tony Li.	
};//__attribute__((packed)); 
typedef struct _Moxa_UART_Struct MoxaUartStruct;
typedef struct _Moxa_UART_Struct *pMoxaUartStruct;


/*------------------------------------------------------ Extern / Function Declaration -----------------*/

extern UINT8 moxa_shadow_dll;		// ZarZar
extern UINT8 moxa_shadow_dlm;		// ZarZar

extern int MoxauartFoundPortNo;
extern int MX_UART_RX[SUPPORT_BUSES];
extern int MX_UART_TX[SUPPORT_BUSES];
extern int MX_UART_DLL[SUPPORT_BUSES];
extern int MX_UART_TRG[SUPPORT_BUSES];

extern int MX_UART_DLM[SUPPORT_BUSES];
extern int MX_UART_IER[SUPPORT_BUSES];
extern int MX_UART_FCTR[SUPPORT_BUSES];

extern int MX_UART_IIR[SUPPORT_BUSES];
extern int MX_UART_FCR[SUPPORT_BUSES];
extern int MX_UART_EFR[SUPPORT_BUSES];

extern int MX_UART_LCR[SUPPORT_BUSES];
extern int MX_UART_MCR[SUPPORT_BUSES];
extern int MX_UART_LSR[SUPPORT_BUSES];
extern int MX_UART_MSR[SUPPORT_BUSES];
extern int MX_UART_SCR[SUPPORT_BUSES];

extern int MOXA_MUST_GDL_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_EFR_REGISTER[SUPPORT_BUSES];

extern int MOXA_MUST_XON1_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_XON2_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_XOFF1_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_XOFF2_REGISTER[SUPPORT_BUSES];

extern int MOXA_MUST_RBRTL_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_RBRTH_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_RBRTI_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_THRTL_REGISTER[SUPPORT_BUSES];

extern int MOXA_MUST_ENUM_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_HWID_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_ECR_REGISTER[SUPPORT_BUSES];
extern int MOXA_MUST_CSR_REGISTER[SUPPORT_BUSES];

extern MoxaUartStruct   moxauart_info[MOXAUART_MAX_PORT];

extern int  isr_total_count;
extern int	 isr_count[MOXAUART_MAX_PORT];
extern int  isr_rx[MOXAUART_MAX_PORT];
extern int  isr_tx[MOXAUART_MAX_PORT];
extern int  isr_ls[MOXAUART_MAX_PORT];
extern int  isr_ms[MOXAUART_MAX_PORT];
extern int  lsr_bi[MOXAUART_MAX_PORT];
extern int  lsr_fe[MOXAUART_MAX_PORT];
extern int  lsr_pe[MOXAUART_MAX_PORT];
extern int  lsr_oe[MOXAUART_MAX_PORT];
extern int  isr_other[MOXAUART_MAX_PORT];
extern int  int_tx[MOXAUART_MAX_PORT];
extern int  isr_Xchange[MOXAUART_MAX_PORT];
extern int  isr_RCchange[MOXAUART_MAX_PORT];



void m_sio_init(void);
int m_sio_open(int port,int operational_mode);
int m_sio_close(int port);
int m_sio_ifree(int port);
int m_sio_ofree(int port);
int m_sio_iqueue(int port);
int m_sio_oqueue(int port);
int m_sio_ioctl(int port, int speed, int reg_lcr);
int m_sio_ioctl_dll_dlm(int port, UINT8 dll , UINT8 dlm, int reg_lcr);
int m_sio_software_flowctrl(int port, UINT8 XON1_s,UINT8 XOFF1_s,UINT8 XON2_s,UINT8 XOFF2_s,int Con);
int m_sio_hardware_flowctrl(int port, int mode);
int m_sio_fifoctrl(int port, int enable, int mode);
int m_sio_enhance_fifoctrl(int port, int RBRTL, int RBRTH, int RBRTI, int THRTL);
int m_sio_read(int port, char *buf, int len);
int m_sio_write(int port, char *buf, int len);
int m_sio_write_register(int port, int reg, UINT8 value);
int m_sio_read_register(int port, int reg, UINT8 *value);
int m_sio_internal_loopback(int port, int mode);
int m_sio_flush(int port, int mode);
int m_sio_lctrl(int port, int mode);
int m_sio_lstatus(int port);
int m_sio_setopmode(int port, UINT8 mode);
int m_sio_getch(int port);
void	m_sio_putch(int port,unsigned char ch);
void	m_sio_get_uart_usage(void);
int GetMoxaUartCnt(void);



// Enhanced Mode //
void ENABLE_MOXA_MUST_ENCHANCE_MODE(unsigned int baseio, int bus_type);
void DISABLE_MOXA_MUST_ENCHANCE_MODE(unsigned int baseio, int bus_type) ;
void SET_MOXA_MUST_XON1_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_XON2_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_XOFF1_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_XOFF2_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_RBRTL_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_RBRTH_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_RBRTI_VALUE(unsigned int baseio, int bus_type, int Value);
void SET_MOXA_MUST_THRTL_VALUE(unsigned int baseio, int bus_type, int Value) ;
void SET_MOXA_MUST_DEFAULT_FIFO_VALUE(pMoxaUartStruct info);
void SET_MOXA_MUST_ENUM_VALUE(unsigned int baseio, int bus_type, int Value) ;
void GET_MOXA_MUST_HARDWARE_ID(unsigned int baseio, int bus_type, UCHAR *pId) ;
void SET_MOXA_MUST_SOFTWARE_FLOW_CONTROL_CONFIGURATION(unsigned int base_address, int bus_type, unsigned char configuration) ;
void ENABLE_MOXA_MUST_TX1_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_TX2_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void DISABLE_MOXA_MUST_TX1_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void DISABLE_MOXA_MUST_TX2_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_RX1_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void DISABLE_MOXA_MUST_RX1_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_RX2_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void DISABLE_MOXA_MUST_RX2_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_TX1_RX1_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_TX2_RX2_SOFTWARE_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void ENABLE_MOXA_MUST_XON_ANY_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void DISABLE_MOXA_MUST_XON_ANY_FLOW_CONTROL(unsigned int baseio, int bus_type) ;
void GET_MOXA_MUST_GDL(unsigned int base_address, int bus_type , unsigned char *reg_gdl);






#endif	// _MOXAUART_H
