/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	main.c

	bios_main()
	S2E initialization and test functions.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-20	Chin-Fu Yang		
		modified
	2009-08-27	Nolan Yu
		modified
    
*/

/*! \mainpage Manufacture/Test Program

\section intro_sec Introduction
The Manufacture/Test program is loaded from SPI Flash to SDRAM address 0x00000000 by boot-loader.
The boot-loader then jumps to SDRAM address 0x00000000 to execute the Manufacture/Test program.
The boot code is start.S, after boot procedure bios_main() in main.c is called to execute the main program.
The main program handles the test of each peripheral components, setting of MAC address and serinal number,
and doing of burn-in test. When each test is passed it will set the corresponding flag stored in SPI Flash.
When all flags are set, which indicates the manufacture/test procedures are done,
firmware will be loaded from SPI Flash to SDRAM, the execution is then transfered to firmware.
       
\section bios_boot_sec start.S (boot code)
<OL start=1>
<LI>Set stack for FIQ mode, IRQ mode, Abort mode, Undefined mode, Supervisor mode. (starting from BSS end, each mode has 4K bytes stack)</LI>
<LI>Enter System mode by setting CPSR with FIQ, IRQ enabledD</LI>
<LI>Set stack for System mode. (16K bytes next to Supervisor mode's stack)</LI>
<LI>init BSS segment</LI>
<LI>branch to bios_main (entry of C code, the main program)</LI>
</OL>
\image html bios_stack.gif

\section bios_upgrade_bios_sec BIOS upgrade
The Manufacture/Test config data in SPI Flash can not be overwrote during BIOS upgrade,
so the write of BIOS file to SPI Flash is divided into 2 stages.
<OL>
<LI>Download BIOS file to SDRAM download area via X-modem protocol.</LI>
<LI>Copy 4K bytes <B>armboot.bin</B> in SDRAM download area to SPI Flash address 0x0000_0000.</LI>
<LI>Copy <B>testprg.bin</B> in SDRAM download area to SPI Flash address 0x0000_2000.</LI>
</OL>
\image html bios_file.gif

\section bios_run_firm_sec	Run firmware
Built-in 4KB SRAM, which contains the boot-loader code, is located at address 0x0300_0000~0x0300_1000, 
The 'firmware loader' is contained in boot-loader.
<OL start=1>
<LI>Disable all interrupts</LI>
<LI>Disable Watch Dog Timer</LI>
<LI>Branch to 0x0300_0020, which jumps to 'firmware loader' immediately.</LI>
<LI>'firmware loader' reads 4 bytes LENGTH field in header from SPI Flash firmware area, then copy LENGTH bytes data from SPI Flash address 0x05_0020 to SDRAM address 0x0000_0000.</LI>
<LI>Branch to SDRAM address 0x0000_0000.</LI>
</OL>

*/
/*
	main.c

 	2009-08-03	Nolan Yu		
		New release		
   
*/
#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "global.h"
#include "string.h"
#include "memory.h"
#include "timer.h"
#include "mac.h"
#include "moxauart.h"
#include "exrtc.h"
#include "gpio.h"
#include "watch_dog.h"
#include "udpburn.h"
//#include "fw_unpack.h"
#include "i2c.h"
#include "main.h"
#include "lib.h"
#include "exspi.h"
#include "spi.h"
#include "spi_reg.h"
#include "dbg_uart.h"
#include "console.h"
#include "arp.h"
#include "ip.h"
#include "tftp.h"
#include "mii.h"
#include "ai.h"
#include "Calibration.h"
#include "MPtester.h"
#include "dio.h"

extern long XmodemRecv(int port,uchar *buf);

#if MODULE_AIO
extern char EEPROM_address;
#endif


int  ConsolePort = 2;		// MOXAUART = 0 , 1 ; General UART = 2
int  ConsoleBaudrate = 115200;

int  board_index=0;
int  poweronstatus = MM_STATUS;
char ErrMsg[256];			// for buffer , not specific
char Current_AI_Channel;
char Current_ADC_Calibration_Mode;

int major_ver,minor_ver,test_ver;		// version of this BIOS

static char const AbortString[20] = {"Abort this test!\r\n"};
static char const ESCSTRING[48] = {"Please press 'ESC' to Quit this testing ...\r\n"};
static char const RUNBAR[4][8] = {"\\\r","| \r","/ \r","- \r"};		// runbar symbols
static UINT8 const UARTPattern[]={0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x12, 0x20, 0x40, 0x80, 0x55, 0xAA, 0x55, 0xAA, 0x00, 0xFF};
static Packet_Info PacketInfo;	// store data pattern 

ulong NicIp[MAC_MAX_AMOUNT] = {0x570aabc0		/* 192.171.10.87 */
								,0x580aabc0		/* 192.171.10.88 */
								,0x580aabc0		/* 192.171.10.88 */
								,0x580aabc0		/* 192.171.10.88 */
								,0x580aabc0};	/* 192.171.10.88 */

char	MacSrc[MAC_MAX_AMOUNT][6];		// allocate the space but not assign the value. the value will burn in the Flash.


//**************************************************************************************************************
//		HW_ID			: This is for HW to distinguish the product	--> 8bit size
//		HW_EXID		: This is for SW to distinguish the product
//		Burn_ID		: This is for burn-in to distinguish the product
//		Magic			: interface
//		Port			: Uart max num
//		LAN			: Lan max num
//		WLAN			: wlan max num
//		PCI			: is this product support pci ?
//		SMCFlash0	: SMC_0 Flash type
//		SMCFlash1	: SMC_2 Flash type
//		Serial No.	: Serial Number of Product. This can be modified in flash.
//**************************************************************************************************************
//																																					*
//	HW_ID,  HW_EXID,  Burn_ID,  Magic,  Port,  LAN,  WLAN,  PCI,  SMCFlash0, SMCFlash1, Serial No., Model Name	*
//																																					*
//**************************************************************************************************************

//_SysModelInfo sysModelInfo[MOXA_ioLogik_E1200_MODEL_MAX_NUM]=	/* Define the parameter of product. */
_SysModelInfo sysModelInfo[MOXA_ioLogik_E1200_MODEL_MAX_NUM + 1]=	/* Define the parameter of product. */
{
#if E1210	
	{0x70, 0x00000070, 0x70, RS232_FACE, 2,2,0,0,0,0,0x70,"ioLogik E1210"},	//	Jerry Wu
	{0x80, 0x00000080, 0x80, RS232_FACE, 2,2,0,0,0,0,0x80,"ioLogik E1210-T"},
#endif

#if E1211	
	{0x71, 0x00000071, 0x71, RS232_FACE, 2,2,0,0,0,0,0x71,"ioLogik E1211"},
	{0x81, 0x00000081, 0x81, RS232_FACE, 2,2,0,0,0,0,0x81,"ioLogik E1211-T"},
#endif

#if E1212
	{0x72, 0x00000072, 0x72, RS232_FACE, 2,2,0,0,0,0,0x72,"ioLogik E1212"},
	{0x82, 0x00000082, 0x82, RS232_FACE, 2,2,0,0,0,0,0x82,"ioLogik E1212-T"},
#endif

#if E1214
	{0x73, 0x00000073, 0x73, RS232_FACE, 2,2,0,0,0,0,0x73,"ioLogik E1214"},
	{0x83, 0x00000083, 0x83, RS232_FACE, 2,2,0,0,0,0,0x83,"ioLogik E1214-T"},
#endif

#if E1240
	{0x74, 0x00000074, 0x74, RS232_FACE, 2,2,0,0,0,0,0x74,"ioLogik E1240"},
	{0x84, 0x00000084, 0x84, RS232_FACE, 2,2,0,0,0,0,0x84,"ioLogik E1240-T"},
#endif

#if E1242
	{0x75, 0x00000075, 0x75, RS232_FACE, 2,2,0,0,0,0,0x75,"ioLogik E1242"},
	{0x85, 0x00000085, 0x85, RS232_FACE, 2,2,0,0,0,0,0x85,"ioLogik E1242-T"},
#endif

#if E1241
	{0x76, 0x00000076, 0x76, RS232_FACE, 2,2,0,0,0,0,0x76,"ioLogik E1241"},
	{0x86, 0x00000086, 0x86, RS232_FACE, 2,2,0,0,0,0,0x86,"ioLogik E1241-T"},
#endif

#if E1260
	{0x77, 0x00000077, 0x77, RS232_FACE, 2,2,0,0,0,0,0x77,"ioLogik E1260"},
	{0x87, 0x00000087, 0x87, RS232_FACE, 2,2,0,0,0,0,0x87,"ioLogik E1260-T"},
#endif

#if E1262
	{0x78, 0x00000078, 0x78, RS232_FACE, 2,2,0,0,0,0,0x78,"ioLogik E1262"},
	{0x88, 0x00000088, 0x88, RS232_FACE, 2,2,0,0,0,0,0x88,"ioLogik E1262-T"},
#endif
  {0x0D, 0xff,0xff,0xff,0xff,0xff,0xff, 0, 0, 0, 0, "???"}
};


burnin_cmd Datapattern_vlaue[] = {
	{ 1,"Pattern 0x5AA55AA5             ",NULL, 1}, 
	{ 2,"Pattern 0xA55AA55A             ",NULL, 1},  
	{ 3,"Pattern 0x5A5A5A5A             ",NULL, 1},      
	{ 4,"Pattern 0xA5A5A5A5             ",NULL, 1},   	
	{ 5,"Pattern 0x00000000             ",NULL, 1},
	{ 6,"Pattern 0xFFFFFFFF             ",NULL, 1},
	{ 0,"",0,0}         
};  

burnin_cmd Datapattern_value1[] = {
    { 1,"Pattern 0x55                     ",NULL, 1},  
    { 2,"Pattern 0x55 -loop               ",NULL, 1},  
    { 3,"Pattern 0xAA                     ",NULL, 1},      
    { 4,"Pattern 0xAA -loop               ",NULL, 1},      
    { 5,"Pattern 0x55 0xAA 0x00 0xFF      ",NULL, 1},      
    { 6,"Pattern 0x55 0xAA 0x00 0xFF -loop",NULL, 1},      
    { 0,"",0,0}         
};   

burnin_cmd burnin_cmd_value_E1200[] = {
	{ 1,"SDRAM Test                     ",diag_do_SDRAM_func, 1},
	{ 2,"SPI Flash Test\r\n"             ,diag_do_SPI_Flash_func , 1},
	{ 3,"SRAM Test                      ",diag_do_SRAM_test_func , 1},
	{ 4,"EEPROM Test\r\n"                ,diag_do_EEPROM_test_func,1},
	{ 5,"UART Test                      ",diag_do_MOXAUART_func, 1},
	{ 6,"Ethernet Test\r\n"              ,diag_do_ETH_func, 1},
	{ 7,"Jumper & Switch Test           ",diag_do_JP_SW_func,1},
	{ 8,"Watch-Dog Test\r\n"             ,diag_do_WDT_func, 1},
	{ 9,"I/O Test                       ",diag_do_IO_func , 1},
	{10,"LED Test\r\n----------------------------------------------------------------------------\r\n"
			                					 ,diag_do_LED_func , 1},
	{11,"Manual MP-Test                 ",diag_do_MPTEST_func, 1},
	{12,"Burn-in Test\r\n----------------------------------------------------------------------------\r\n"
	                                     ,diag_do_BURNIN_func, 1},
	{13,"Set/Clear flag                 ",diag_do_FLAG_func, 1},
	{14,"Set Serial No. & MAC Addr.\r\n" ,diag_do_SERIAL_MAC_func, 1},
	{15,"Product Info                   ",diag_do_PRODUCTINFO_func, 1},
	{16,"Set HW External ID\r\n----------------------------------------------------------------------------\r\n"
	  												 ,diag_do_HWEXID_func, 1},
	{17,"BIOS Upgrade                   ",diag_do_BIOS_upgrade, 1},
	{18,"System reset\r\n"               ,diag_do_SYSRESET_func,1},
	{19,"Download User file             ",diag_do_DOWNLOAD_func, 1},
	{20,"Jump to firmware\r\n"           ,diag_do_JUMPTOGO_func, 1},
	{21,"Change module name    "         ,diag_do_Change_module_name, 1},
	{22,"Debug\r\n"                      ,diag_do_DEBUG_func,0},
	{ 0,"",0,0}
};

//\\ -----------------------------------Test Functions--------------------------------------------

/****************************/
/*  1. SDRAM Test					  */
/****************************/
burnin_cmd sdram_cmd_vlaue[] = {
	{ 1,"SDRAM view register            ",diag_do_get_SDRAM_setting, 1}, 	// non-linear register map
	{ 2,"SDRAM BYTE Test				",diag_do_SDRAM_byte, 1},
	{ 3,"SDRAM HALFWORD Test            ",diag_do_SDRAM_short, 1},
	{ 4,"SDRAM WORD Test				",diag_do_SDRAM_word, 1},
//	{ 5,"SDRAM Usage Status				",diag_do_get_SDRAM_usage, 1},
	{ 0,"",0,0}         
};     


/**	\brief
 *
 *	SDRAM test main function.
 *	It will call the sub menu.
 *
 */
void diag_do_SDRAM_func(void)
{       
   burnin_cmd *burnin_temp=sdram_cmd_vlaue;
   ManualTesting(burnin_temp);
}   


void diag_do_get_SDRAM_setting(void)
{
    int i;
    
    for(i=0;i<0x3C;i+=4)
        Printf("\r\nSDRAM Controller Registers offset 0x%02x = 0x%08x",i,*((UINT32*) (S2E_MEM_CTRL_BASE+ i)));
}

void diag_do_SDRAM_byte(void)
{
   	UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	sleep(1);
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sleep(1);
	sdram_byte_mptest(1, i);		// (mm , pattern)
}    

void diag_do_SDRAM_short(void)
{
    UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sdram_short_mptest(1, i);
}    

void diag_do_SDRAM_word(void)
{
    UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sdram_word_mptest(1, i);
}

static int sdram_byte_mptest(int mm, int pattern)
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	UINT8 *ptr;  
	vuchar *addr_ptr;
	
	Content = diag_get_data_pattern(pattern);
	
	start_addr = SDRAM_TEST_BASE;		// start from BIOS END
	end_addr = S2E_SDRAM_BANK_SIZE;

	ptr = (UINT8*) &Content;
	if( mm == 1 ){
		PrintStr("\r\nBegin SDRAM Byte access test from 0x");
		PrintHex(start_addr);
		PrintStr(" to 0x");
		PrintHex(end_addr);
		PrintStr("\r\n");
		sleep(1);
	}
	
	for(i = start_addr , next_start_addr = start_addr , k=0  ;  i<end_addr  ;  i+=4){

		addr_ptr = (vuchar*)i;
		
		*addr_ptr = ptr[0];
		addr_ptr++;
		*addr_ptr = ptr[1];     
		addr_ptr++;
		*addr_ptr = ptr[2];      
		addr_ptr++;
		*addr_ptr = ptr[3]; 
		addr_ptr++;
 
		CompareContent = VPlong(i);       
		if(CompareContent != Content){
			PrintStr("error address:");
			PrintHex(i);
			PrintStr(" error data:");
			PrintHex(CompareContent);
			PrintStr(", correct data:");
			PrintHex(Content);
			PrintStr("\r\n");
			return(-1);
		} 

		if((i & 0x003FFFFF) == 0){		// 0x003FFFFF = 4MB
			if( mm == 1 ){
				PrintStr("SDRAM test from 0x");
				PrintHex(next_start_addr);
				PrintStr(" to 0x");
				PrintHex(i);
				PrintStr(" ...OK!\r\n");
			}
			next_start_addr = i;
		}

		if((i & 0x00003FFF) == 0){
			PrintStr((char*)RUNBAR[(k++)%4]);
		} 

		if(mm & (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}

	}
	if( mm == 1 ){
		PrintStr("SDRAM test from 0x");
		PrintHex(next_start_addr);
		PrintStr(" to 0x");
		PrintHex(i);
		PrintStr(" ...OK!\r\n");
	}
    return(0);
}

static int sdram_short_mptest(int mm, int pattern)		// short = 2bytes
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	UINT16  * ptr;  
	vushort *addr_ptr;
	
	Content = diag_get_data_pattern(pattern);
	
	start_addr = SDRAM_TEST_BASE;		// start from BIOS END
	end_addr = S2E_SDRAM_BANK_SIZE;
    
	ptr = (UINT16*) &Content;
	if( mm == 1 ){
		Printf("\r\nBegin SDRAM Half Word access test from 0x%.8x to 0x%.8x ...\r\n ",start_addr,end_addr);
	}
	
	for(i=start_addr,next_start_addr=start_addr,k=0;i<end_addr;i+=4){

		addr_ptr = (vushort*)i;
		
		*addr_ptr = ptr[0];
		addr_ptr++;
		*addr_ptr = ptr[1];     
		addr_ptr++;

		CompareContent = VPlong(i);                                  
		if(CompareContent != Content){
			Printf("error address:%x, error data:%x, correct data:%x\r\n ", i, CompareContent,Content );                  
			return(-1);
		} 
		
		if((i & 0x003FFFFF) == 0){
			if( mm == 1 ){
				Printf("SDRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,i);            
			}
			next_start_addr = i;
		}
		
		if((i & 0x00003FFF) == 0){
			Printf("%s",RUNBAR[(k++)%4]);
		} 
		
		if( mm && (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}
	}
	
	if( mm == 1 ){
		Printf("SDRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,end_addr);            
	}
	return(0);
}

static int sdram_word_mptest(int mm, int pattern)		// word = 4bytes
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	UINT32  * ptr;  
	
	Content = diag_get_data_pattern(pattern);
	
	start_addr = SDRAM_TEST_BASE;
	end_addr = S2E_SDRAM_BANK_SIZE;

	ptr = (UINT32*) &Content;
	if( mm == 1 ){
		Printf("\r\nBegin SDRAM Word access test from 0x%.8x to 0x%.8x ...\r\n ",start_addr,end_addr);
	}
	
	for(i=start_addr,next_start_addr=start_addr,k=0;i<end_addr;i+=4){
		VPlong(i) = ptr[0];            
		CompareContent = VPlong(i);                                  
		if(CompareContent != Content){
			Printf("error address:%x, error data:%x, correct data:%x\r\n ", i, CompareContent,Content );                  
			return(-1);
		} 
		if((i & 0x003FFFFF) == 0){
			if( mm == 1 ){
				Printf("SDRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,i);            
			}
			next_start_addr = i;
		}
		if((i & 0x00003FFF) == 0){
			Printf("%s",RUNBAR[(k++)%4]);
		} 
		if(mm && (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}
	}   
	
	if( mm == 1 ){
		Printf("SDRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,end_addr);            
	}
	return(0);
}
/*
void diag_do_get_SDRAM_usage(void)
{
    int i;
    
    Printf("\r\narmboot_end = 0x%08x bss_end : 0x%08x stack end = 0x%08x\r\n",
       (int)_armboot_end,(int)_bss_end,(int)(_bss_end + CONFIG_STACKSIZE));
    Printf("Download Ptr = 0x%08x  Console Tptr = 0x%08x  Console Rptr = 0x%08x\r\n",
        (int)SDRAM_DOWNLOAD_TEMP_BASE,(int)SDRAM_CONSOLE_TXBUF_BASE,(int)SDRAM_CONSOLE_RXBUF_BASE);
	Printf("General buf Base 0x%08x\r\n",SDRAM_GENERAL_MEMORY_BASE);
    for(i=0;i<MAC_MAX_AMOUNT;i++){
        Printf("MAC%d TXDES = 0x%08x RXDES = 0x%08x MACTB = 0x%08x MACRB = 0x%08x\r\n",i,
               (int)MAC_TX_DESCRIPTOR_BASE(i), (int)MAC_RX_DESCRIPTOR_BASE(i),
               (int)SDRAM_MAC_TX_BUF_BASE(i),(int)SDRAM_MAC_RX_BUF_BASE(i));
    }           
    for(i=0;i<UART_MAX_AMOUNT;i++){
        Printf("UART%02d Txbuf = 0x%08x Rxbuf = 0x%08x\r\n",i,(int)SDRAM_UART_TX_BASE(i),(int)SDRAM_UART_RX_BASE(i));
    }
	Printf("\r\n");
}
*/
/****************************/
/*  3. SRAM Test		    */
/****************************/
burnin_cmd sram_cmd_vlaue[] = {
//    { 1,"SRAM BYTE Test                  ",diag_do_SRAM_byte, 1},  
    { 1,"SRAM HALFWORD Test              ",diag_do_SRAM_short, 1},      
    { 2,"SRAM WORD Test                  ",diag_do_SRAM_word, 1}, 
    { 0,"",0,0}         
};    

/**	\brief
 *
 *	SRAM test main function.
 *	It will call the sub menu.
 *
 */
void diag_do_SRAM_test_func(void)
{
	burnin_cmd *burnin_temp=sram_cmd_vlaue;
	ManualTesting(burnin_temp);
}
/*
void diag_do_SRAM_byte(void)
{
   	UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sram_byte_mptest(1, i);		// (mm , pattern)
}
*/
void diag_do_SRAM_short(void)
{
   	UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sram_short_mptest(1, i);
}

void diag_do_SRAM_word(void)
{
   	UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	Printf("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("TEST DATA_PATTERN >>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sram_word_mptest(1, i);
}
/*
static int sram_byte_mptest(int mm, int pattern)
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	vuchar *ptr;  
	UINT8 *from;
	UINT8 *to;
	vuchar *addr_ptr; 
	
	Content = diag_get_data_pattern(pattern);
	
   	start_addr = SRAM_REMAP_BASE;
	end_addr = SRAM_END;
	
	// copy data from SRAM to SDRAM for backup 
	from = (UINT8*)SRAM_REMAP_BASE;
	to = (UINT8*)SDRAM_TEST_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);

	ptr = (vuchar*) &Content;
	if( mm == 1 ){
		Printf("\r\nBegin SRAM Byte access test from 0x%.8x to 0x%.8x ...\r\n ",start_addr,end_addr);
	}
	for( i = start_addr , next_start_addr = start_addr , k=0 ; i < end_addr ; i += 4){

		addr_ptr = (vuchar*)i;
		
		*addr_ptr = ptr[0];
		addr_ptr++;
		*addr_ptr = ptr[1];     
		addr_ptr++;
		*addr_ptr = ptr[2];      
		addr_ptr++;
		*addr_ptr = ptr[3]; 
		addr_ptr++;
		CompareContent = VPlong(i);                                  
		if(CompareContent != Content){
			Printf("error address:%x, error data:%x, correct data:%x\r\n ", i, CompareContent,Content );                  
			return(-1);
		} 
		if((i & 0x003FFFFF) == 0){
			if( mm == 1 ){
				Printf("SRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,i);            
			}
			next_start_addr = i;
		}
		if((i & 0x00003FFF) == 0){
			Printf("%s",RUNBAR[(k++)%4]);
		} 
		if(mm & (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}
	}
	
	if( mm == 1 ){
		Printf("SRAM test from 0x%08x to 0x%.8x ...OK!\r\n ",next_start_addr,end_addr);            
	}
	
	// restore data from SDRAM to SRAM
	from = (UINT8*)SDRAM_TEST_BASE;
	to = (UINT8*)SRAM_REMAP_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);
	return(0);
}
*/
static int sram_short_mptest(int mm, int pattern)		// short = 2 bytes
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	UINT16  * ptr;  
	UINT8 *from;
	UINT8 *to;
	vushort *addr_ptr ;
	
	Content = diag_get_data_pattern(pattern);
	
   	start_addr = SRAM_REMAP_BASE;
	end_addr = SRAM_END;
	
	// copy data from SRAM to SDRAM for backup 
	from = (UINT8*)SRAM_REMAP_BASE;
	to = (UINT8*)SDRAM_TEST_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);

	ptr = (UINT16*) &Content;
	if( mm == 1 ){
		Printf("\r\nBegin SRAM Half-Word access test from 0x%.8x to 0x%.8x ...\r\n ",start_addr,end_addr);
	}
	
	for( i = start_addr , next_start_addr = start_addr , k=0 ; i < end_addr ; i += 4){
		addr_ptr = (vushort*)i;
		
		*addr_ptr = ptr[0];
		addr_ptr++;
		*addr_ptr = ptr[1];     
		addr_ptr++;
		                
		CompareContent = VPlong(i);                                  
		if(CompareContent != Content){
			Printf("error address:%x, error data:%x, correct data:%x\r\n ", i, CompareContent,Content );                  
			return(-1);
		} 
		if((i & 0x003FFFFF) == 0){
/*			if( mm == 1 ){
				Printf("SRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,i);            
			}
*/			next_start_addr = i;
		}
		if((i & 0x00003FFF) == 0){
			Printf("%s",RUNBAR[(k++)%4]);
		} 
		if(mm & (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}
	}
	if( mm == 1 ){
		Printf("SRAM test from 0x%08x to 0x%08x ...OK!\r\n ",next_start_addr,end_addr);            
	}

	// restore data from SDRAM to SRAM
	from = (UINT8*)SDRAM_TEST_BASE;
	to = (UINT8*)SRAM_REMAP_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);
	
	return(0);
}

static int sram_word_mptest(int mm, int pattern)		// word = 4bytes
{
	UINT32 start_addr,end_addr,next_start_addr;
	UINT32 i,k;
	UINT32 CompareContent,Content; 
	UINT32  * ptr;  
	UINT8 *from;
	UINT8 *to;
	
	Content = diag_get_data_pattern(pattern);
	
   	start_addr = SRAM_REMAP_BASE;
	end_addr = SRAM_END;
	
	// copy data from SRAM to SDRAM for backup 
	from = (UINT8*)SRAM_REMAP_BASE;
	to = (UINT8*)SDRAM_TEST_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);

	ptr = (UINT32*)&Content;
	if( mm == 1 ){
		Printf("\r\nBegin SRAM Word access test from 0x%.8x to 0x%.8x ...\r\n ",start_addr,end_addr);
	}
	
	for( i = start_addr , next_start_addr = start_addr , k=0 ; i < end_addr ; i += 4){
		VPlong(i+0) = ptr[0];                                         
		CompareContent = VPlong(i);                                  
		if(CompareContent != Content){
			Printf("error address:%x, error data:%x, correct data:%x\r\n ", i, CompareContent,Content );                  
			return(-1);
		} 
		if((i & 0x003FFFFF) == 0){
/*			if( mm == 1 ){
				Printf("SRAM test from 0x%.8x to 0x%.8x ...OK!\r\n ",next_start_addr,i);            
			}
*/			next_start_addr = i;
		}
		if((i & 0x00003FFF) == 0){
			Printf("%s",RUNBAR[(k++)%4]);
		} 
		if(mm & (diag_check_press_ESC() == DIAG_ESC)){
			return(-2);
		}
	}
	
	if( mm == 1 ){
		Printf("SRAM test from 0x%08x to 0x%08x ...OK!\r\n ",next_start_addr,end_addr);            
	}

	// restore data from SDRAM to SRAM
	from = (UINT8*)SDRAM_TEST_BASE;
	to = (UINT8*)SRAM_REMAP_BASE;
	memcpy(to , from , SRAM_MAX_SIZE);
	
    return(0);
}

/****************************/
/*  4 EEPROM Test		    */
/****************************/
void diag_test_EEPROM_MP(void);
void diag_EEPROM_dump(void);
void diag_clear_EEPROM(void);
void diag_EEPROM_MP(void);
void diag_manual_EEPROM(void);
UINT8 diag_EEPROM_protect(void);
#if	MODULE_AIO
void diag_Set_EEPROM_ID(void);
#endif

burnin_cmd eeprom_cmd_vlaue[] = {
	{ 1,"EEPROM Diagnostics              ",/*(void*)*/diag_EEPROM_MP, 1},
	{ 2,"EEPROM Dump                     ",diag_EEPROM_dump, 1},
	{ 3,"Manual Data input               ",diag_manual_EEPROM, 1},
	{ 4,"EEPROM Clear                    ",diag_clear_EEPROM, 1},
	{ 5,"EEPROM Protected Test           ",(void*)diag_EEPROM_protect, 1},
#if MODULE_AIO
	{ 6,"EEPROM ID/SER NO. Set & Check   ",diag_Set_EEPROM_ID, 1},
#endif
	{ 0,"",0,0}         
};

//--------	EEPROM Test Func		-----------
void diag_do_EEPROM_test_func(void)
{	
	burnin_cmd *burnin_temp=eeprom_cmd_vlaue;
	GpioWriteData(PIO_EEPROM_PROTECT,0);
	ManualTesting(burnin_temp);
	GpioWriteData(PIO_EEPROM_PROTECT,1);
}

void diag_EEPROM_dump(void)
{	
	int i,j,k;
	int length;
	UINT8 buffer[256];
	int page_size;
	UINT16 loop;
	int eeprom_page;

#if MODULE_AIO
	int value;
	if(diag_get_value("\r\nDump EEPROM (0)Config data (1)Cal parameter", &value, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
	{return;}

	if(value){
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
		length = Cal_EEPROM_MAX_SIZE;
		page_size = Cal_EEPROM_PAGE_SIZE;
		EEPROM_address = PARMETER_EEPROM_WRITE;
	}
	else{
#endif
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
		length = EEPROM_MAX_SIZE;
		page_size = EEPROM_PAGE_SIZE;
#if MODULE_AIO
		EEPROM_address = EEPROM_WRITE;
	}
#endif
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing

#if MODULE_AIO
	eeprom_page = 0;
	if(!value){
#endif
		if(diag_get_value("Dump EEPROM ALL(0)/Single page(1) ...", &eeprom_page, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
			i2c_disable();
			return;
		}
		
		if(eeprom_page){
			if(diag_get_value("Dump EEPROM Page ...", &eeprom_page, 0, (EEPROM_MAX_SIZE >> 8)-1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
				i2c_disable();
				return;
			}
			eeprom_page <<= 8;
			length = eeprom_page + 256;
		}
#if MODULE_AIO
	}
#endif

	for(k=eeprom_page;k<length;k=k+256){
		if(EEPROM_MAX_SIZE == 128){
			Printf("<< EEPROM Address form 0x0000 ~ 0x008f >>\r\n\n");
			loop = 128;
		}
		else{
			Printf("<< EEPROM Address form 0x%02x00 ~ 0x%02xff >>\r\n\n",(k>>8),(k>>8));
			loop = 256;
		}
		Printf("               |");

		for(i=0;i<16;i++)
			Printf("%02X.",i);
		Printf("\r\n--------------------------------------------------------------\r\n");

		for(i=0;i<loop;i+=page_size){	//Read data
			if(i2c_read_from_eeprom_page(i+k,&buffer[i],page_size) == -1){
				i2c_disable();
				return;
			}
		}

		for(i=0;i<(loop>>4);i++){	//Printf data
			Printf("ADD %04X ~ %04X|", i*16+k, i*16+15+k);
			for(j=0;j<16;j++)
				Printf("%02X.",buffer[i*16+j]);
			Printf("\r\n");
		}

		if(k+256 < length){
			Printf("\r\nPress any key to continue! \r\n");
			if(Getch() == 27){
				i2c_disable();
				return;
			}
		}
	}
	i2c_disable();
}

void diag_manual_EEPROM(void){

	UINT32 add,data;

 	if(diag_get_value("\r\nAddress is : ", &add, 0, EEPROM_MAX_SIZE-1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
	{return;}

 	if(diag_get_value("Data is : ", &data, 0, 255, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
	{return;}

	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
#if MODULE_AIO
	EEPROM_address = EEPROM_WRITE;
#endif
	if(i2c_write_to_eeprom(add,(UINT8)data) == -1)
		return;

	Printf("\r\nADD 0x%03X ~ 0x%03X|", add, add+15);
	for(data=0;data<16;data++)
		Printf("%02X.",eeprom_readb(RAND_ADDR,add+data));
	i2c_disable();
}

void diag_clear_EEPROM_BR(void){
	int k,i;
	UINT8 ptr[EEPROM_PAGE_SIZE],buf[EEPROM_PAGE_SIZE];
	int length;
	int page_size;

	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
	length = EEPROM_MAX_SIZE;
	page_size = EEPROM_PAGE_SIZE;

	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing

	for(k=0;k<EEPROM_PAGE_SIZE;k++){
		ptr[k] = 0xff;
	}

	for(k=0;k<length;k+=page_size){
		if(k%256 == 0)
			Printf("Clear EEPROM 0x%02X00 ~ 0x%02XFF..........",(k>>8),(k>>8));

			if(eeprom_write_page(k,ptr,page_size) == -1){
				i2c_disable();
				return;
			}
		sleep(10);
		if(i2c_read_from_eeprom_page(k,buf, page_size) == -1){
			i2c_disable();
			return;
		}
		for(i=0;i<page_size;i++){
			if(buf[i] != 0xff){
				Printf (" Addr[%d] = %x != 0xff\r\n",i+k,buf[i]);
				Printf("FAIL!!\r\n");
				i2c_disable();
				return;
			}
		}
		if((k%256 + page_size) == 256)
			Printf("OK!!\r\n");
	}
	i2c_disable();
}

void diag_clear_EEPROM(void){
	int k,i;
	UINT8 ptr[EEPROM_PAGE_SIZE],buf[EEPROM_PAGE_SIZE];
	int length;
	int page_size,eeprom_page;

#if MODULE_AIO
	int value;
	if(diag_get_value("\r\nClear EEPROM (0)Config data (1)Cal parameter", &value, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
		return;	

	if(value){
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
		length = Cal_EEPROM_MAX_SIZE;
		EEPROM_address = PARMETER_EEPROM_WRITE;
		page_size = Cal_EEPROM_PAGE_SIZE;
	}
	else{
#endif
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
		length = EEPROM_MAX_SIZE;
		page_size = EEPROM_PAGE_SIZE;
#if MODULE_AIO
		EEPROM_address = EEPROM_WRITE;
	}
#endif
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing

#if MODULE_AIO
	eeprom_page = 0;
	if(!value){
#endif
		if(diag_get_value("Clear EEPROM ALL(0)/Single page(1) ...", &eeprom_page, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
			i2c_disable();
			return;
		}
		
		if(eeprom_page){
			if(diag_get_value("Clear EEPROM Page ...", &eeprom_page, 0, (EEPROM_MAX_SIZE >> 8)-1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
				i2c_disable();
				return;
			}
			eeprom_page <<= 8;
			length = eeprom_page + 256;
		}
#if MODULE_AIO
	}
#endif

	for(k=0;k<EEPROM_PAGE_SIZE;k++)
		ptr[k] = 0xff;

	for(k=eeprom_page;k<length;k+=page_size){
		if(k%256 == 0)
			Printf("Clear EEPROM 0x%02X00 ~ 0x%02XFF..........",(k>>8),(k>>8));
#if MODULE_AIO
		if(EEPROM_address == EEPROM_WRITE){
#endif
			if(eeprom_write_page(k,ptr,page_size) == -1){
				i2c_disable();
				return;
			}
#if MODULE_AIO
		}
		else{
			for(i=0;i<page_size;i++){
				if(i2c_write_to_eeprom(k+i,ptr[0]) == -1){
					i2c_disable();
					return;
				}
			}
		}
#endif
		sleep(10);
		if(i2c_read_from_eeprom_page(k,buf, page_size) == -1){
			i2c_disable();
			return;
		}
		for(i=0;i<page_size;i++){
			if(buf[i] != 0xff){
				Printf (" Addr[%d] = %x != 0xff\r\n",i+k,buf[i]);
				Printf("FAIL!!\r\n");
				i2c_disable();
				return;
			}
		}
		if((k%256 + page_size) == 256)
			Printf("OK!!\r\n");
	}
	i2c_disable();
}

UINT8 diag_EEPROM_protect_test(int mm){

	UINT8 data,data1;

	if(mm == 1){
#if MODULE_AIO
		int value;
		if(diag_get_value("\r\nEEPROM Protected function (0)Config data (1)Cal parameter", &value, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
			return 0;	

		if(value){
			i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
			EEPROM_address = PARMETER_EEPROM_WRITE;
		}
		else{
#endif
			i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
#if MODULE_AIO
			EEPROM_address = EEPROM_WRITE;
#endif
		}
		i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	}
	data = eeprom_readb(RAND_ADDR,0);
	GpioWriteData(PIO_EEPROM_PROTECT,1);
	
	if(i2c_write_to_eeprom(0,data+1) == -1){ 		
		GpioWriteData(PIO_EEPROM_PROTECT,0);
		Printf("EEPROM Protected function test OK!!\r\n");
		return 0;
	}

	GpioWriteData(PIO_EEPROM_PROTECT,0);
	if(i2c_read_from_eeprom(1,&data1) == -1){
		Printf("EEPROM Protected function test OK!!\r\n");
		return 0;
	}else{
		if(data1 != data){
			i2c_write_to_eeprom(0,data);
			Printf("data error");
			return 1;
		}
		else {
			Printf("EEPROM Protected function test OK!!\r\n");
			return 0;
		}
	}
}

UINT8 diag_EEPROM_protect(void){
	return diag_EEPROM_protect_test(1);
}

#if	MODULE_AIO

extern void set_serial_to_EEPROM(unsigned long serial);
void diag_Set_EEPROM_ID(void)
{
	UINT8 model_id;

	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;
#if E1240
	if(i2c_write_to_eeprom(MODULE_ID,E1240) == -1){
		i2c_disable();
		return;
	}
#endif
#if E1242
	if(i2c_write_to_eeprom(MODULE_ID,E1242) == -1){
		i2c_disable();
		return;
	}
#endif
#if E1262
	if(i2c_write_to_eeprom(MODULE_ID,E1262) == -1){
		i2c_disable();
		return;
	}
#endif
#if E1260
	if(i2c_write_to_eeprom(MODULE_ID,E1260) == -1){
		i2c_disable();
		return;
	}
#endif
#if E1241
	if(i2c_write_to_eeprom(MODULE_ID,E1241) == -1){
		i2c_disable();
		return;
	}
#endif
	
	if(i2c_read_from_eeprom(MODULE_ID,&model_id) == -1){
		i2c_disable();
		return;
	}

	set_serial_to_EEPROM(sysModelInfo[board_index].serial);
	sleep(10);
#if E1240
	if (model_id==E1240)
		Printf("\r\nE1240 ID Set & Check OK!\r\n");
	else
		Printf("\r\nE1240 ID Set & Check Error!\r\n");
	i2c_disable();
#endif
#if E1242
	if (model_id==E1242)
		Printf("\r\nE1242 ID Set & Check OK!\r\n");
	else
		Printf("\r\nE1242 ID Set & Check Error!\r\n");
	i2c_disable();
#endif
#if E1260
	if (model_id==E1260)
		Printf("\r\nE1260 ID Set & Check OK!\r\n");
	else
		Printf("\r\nE1260 ID Set & Check Error!\r\n");
	i2c_disable();
#endif
#if E1262
	if (model_id==E1262)
		Printf("\r\nE1262 ID Set & Check OK!\r\n");
	else
		Printf("\r\nE1262 ID Set & Check Error!\r\n");
	i2c_disable();
#endif
#if E1241
	if (model_id==E1241)
		Printf("\r\nE1241 ID Set & Check OK!\r\n");
	else
		Printf("\r\nE1241 ID Set & Check Error!\r\n");
	i2c_disable();
#endif
}
#endif

int diag_EEPROM_MP_page(UINT16 addr,int length)
{
	int i,j;
	UINT8 ptr[EEPROM_PAGE_SIZE],buf[EEPROM_PAGE_SIZE];
	UINT8 buffer[EEPROM_PAGE_SIZE];

//	Printf("Save EEPROM to SDRAM......");
	
	if(i2c_read_from_eeprom_page(addr,buffer, length) == -1){// save original content to SDRAM
		i2c_disable();
		return -1;
	}
		
//	Printf("Done.\r\n");

	for(i=0;i<2;i++)
	{
		for(j=0;j<EEPROM_PAGE_SIZE;j++){
			if(i == 0)
				ptr[j]=0x55;
			else
				ptr[j]=j;
		}
//		data=0;
#if MODULE_AIO
		if(EEPROM_address == EEPROM_WRITE){
#endif
			if(eeprom_write_page(addr,ptr,length) == -1){
				i2c_disable();
				return -1;
			}
#if MODULE_AIO
		}
		else{
			for(j=0;j<length;j++){
				if(i2c_write_to_eeprom(addr+j,ptr[j]) == -1){
					i2c_disable();
					return -1;
				}
			}
		}
#endif
		sleep(10);
		i2c_read_from_eeprom_page(addr,buf,length);
		for(j=0;j<length;j++){
			if (buf[j] != ptr[j]){
				Printf ("\r\ndata [%x] = %x\r\n",addr+j,buf[j]);
				Printf ("ptr [%x] = %x\r\n",addr+j,ptr[j]);
				Printf(".....error\r\n"); 
				i2c_disable();
				return -1;
			}
		}
	}

//	Printf("Save SDRAM to EEPROM......\r\n");
#if MODULE_AIO
	if(EEPROM_address == EEPROM_WRITE){
#endif
		if(eeprom_write_page(addr,buffer,length) == -1){
			i2c_disable();
			return -1;
		}
#if MODULE_AIO
	}
	else{
		for(j=0;j<length;j++){
			if(i2c_write_to_eeprom(addr+j,buffer[j]) == -1){
				i2c_disable();
				return -1;
			}
		}
	}
#endif
	sleep(10);
	return 0;
}

void diag_EEPROM_MP(void){
	int k;
	int length;
	int page_size;
	int eeprom_page;

#if MODULE_AIO
	int value;
	if(diag_get_value("\r\nEEPROM test (0)Config data (1)Cal parameter", &value, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
		return;	

	if(!value){ //config data eeprom setting
#endif
		if(diag_get_value("Test EEPROM ALL(0)/Single page(1) ...", &eeprom_page, 0, 1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
			i2c_disable();
			return;
		}
		length = EEPROM_MAX_SIZE;
		if(eeprom_page){
			if(diag_get_value("Test EEPROM Page ...", &eeprom_page, 0, (EEPROM_MAX_SIZE >> 8)-1, DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
				i2c_disable();
				return;
			}
			eeprom_page <<= 8;
			length = eeprom_page + 256;
		}
		
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
		page_size = EEPROM_PAGE_SIZE;
#if MODULE_AIO
		EEPROM_address = EEPROM_WRITE;
	}
	else{		
		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
		length = Cal_EEPROM_MAX_SIZE;
		EEPROM_address = PARMETER_EEPROM_WRITE;
		page_size = Cal_EEPROM_PAGE_SIZE;
		eeprom_page = 0;
	}
#endif
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	
	for(k=eeprom_page;k<length;k+=page_size){
		if(k%256 == 0)
			Printf("EEPROM test from 0x%04x to 0x%04x and Length = %d bytes..",k,k+255,256);
		if(diag_EEPROM_MP_page(k,page_size) == -1){
			Printf("FAIL!!\r\n");
			return;
		}
		if((k%256+page_size) == 256)
			Printf("OK!!\r\n");
	}
	i2c_disable();
}

/****************************/
/*  5 MOXA UART 					  */
/****************************/
burnin_cmd moxa_uart_cmd_vlaue[] = {
	{ 1,"UART Register Test                ",diag_do_MOXAUART_register_test, 1},  	// show reg
	{ 2,"UART Internal Test                ",diag_do_MOXAUART_Internal_test, 1},      	// loopback
	{ 3,"UART Terminal Test                ",diag_do_MOXAUART_Terminal_test, 1},  	// DTR/RTS
	{ 4,"UART Usage Status                 ",diag_do_MOXAUART_Usage_test, 1},	
	{ 0,"",0,0}         
};    

/**	\brief
 *
 *	MOXAUART test main function.
 *	It will call the sub menu.
 *
 */
void diag_do_MOXAUART_func(void)
{
	burnin_cmd *burnin_temp=moxa_uart_cmd_vlaue;
	ManualTesting(burnin_temp);
}

int moxa_uart_test(UINT8 mode, int parameter)
{
	int 	i,p=0,err=0,k=0,count=0,loop;
    unsigned int t,t1,t2;	
    UINT32	wlen=0,rlen=0,len=0,baudrate;
	pMoxaUartStruct info;
	
	m_sio_init();
	loop = PacketInit(0);
	if(loop == DIAG_ESC){return DIAG_ESC;}
	if(poweronstatus == MM_STATUS){
		if(diag_get_value("Please key in baudrate X bps (0) 9.6K (1) 19.2K (2) 57.6K (3) 115.2K",&baudrate,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
		{return 0;}
	}
	else
		baudrate=3;

	m_sio_open(p,0);
	switch(baudrate){
	case 0:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_9600 , 0, 0x3);	break;  
	case 1:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_19200 , 0, 0x3);	break;  
	case 2:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_57600 , 0, 0x3);	break;  
	case 3:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_115200 , 0, 0x3);break;  
	default:
		return 0;  
	}
	m_sio_lctrl(p,DTR_ON|RTS_ON);
	if((parameter & MODE_LOOPBACK) == MODE_LOOPBACK)
		m_sio_internal_loopback(p,INTERNAL_LOOPBACK_ON);
	else
		m_sio_setopmode(p,mode);

	m_sio_flush(p,FLUSH_INOUT);	// Flush TX/RX Buffer

	sleep(500);
	t = t1 = t2 = 0;
	t =	Gsys_msec;	
	if(poweronstatus == MM_STATUS){
		Printf("==============>>>Setting information<<<=============\r\n");
		Printf("Mode                  :Internal test\r\n");
		Printf("Packet length         :%d\r\n",PacketInfo.length);
		Printf("Pattern               :0x%02x%02x%02x%02x\r\n",PacketInfo.wbuf[3],PacketInfo.wbuf[2],PacketInfo.wbuf[1],PacketInfo.wbuf[0]);
		Printf("Baudrate              :%d\r\n",baudrate);
		Printf("====================================================\r\n");
	}
	while(1){
		wlen = m_sio_write(p, PacketInfo.wbuf, PacketInfo.length);
		if(mode == RS232_MODE){
			for(;;){
				if(m_sio_oqueue(p) == 0)
					break;
			}
		}

		while(1){
			if(m_sio_iqueue(p) >= PacketInfo.length){
				break;
			}

			if (diag_check_press_ESC() == DIAG_ESC){

				info = &moxauart_info[p];
				if((len=m_sio_iqueue(p)) < PacketInfo.length){
					m_sio_read(p,PacketInfo.rbuf,len);
					Printf("port%d receive length = %d Data :",p,len);
					for(k=0;k<len;k++){
						if((k%16) == 0)
							Printf("\r\n");
						Printf("%02x ",PacketInfo.rbuf[k]);
					}
					Printf("\r\n");
	 				Printf("Port%d len : %d isr = %d tx = %d rx = %d ls = %d ms = %d xch = %d rch = %d other = %d\r\n",p,m_sio_iqueue(p),isr_count[p],isr_tx[p],isr_rx[p],isr_ls[p],isr_ms[p],isr_Xchange[p],isr_RCchange[p],isr_other[p]);
	  				Printf("BI = %d FE = %d PE = %d OE = %d\r\n",lsr_bi[p],lsr_fe[p],lsr_pe[p],lsr_oe[p]);			  		
					Printf("tw = %d tr = %d rw = %d rr = %d IIR = %x LSR = %x\r\n",info->txwptr,info->txrptr,info->rxwptr,info->rxrptr,Inb(info->base+MX_UART_IIR[info->bus_type]),Inb(info->base+MX_UART_LSR[info->bus_type]));		
					err = DIAG_FAIL;
				}else{
					Printf("port%d receive OK !\r\n",p);
				}
				m_sio_close(p);	
				return (err);
			}
		}				
		if((len = m_sio_read(p,PacketInfo.rbuf,PacketInfo.length)) != 0){
			rlen += len;
			for(i=0,err=0;i<PacketInfo.length;i++){
				if(PacketInfo.rbuf[i] != PacketInfo.wbuf[i]){
					Printf("Port %d Data error Write 0x%x Read 0x%x\r\n",p,PacketInfo.wbuf[i],PacketInfo.rbuf[i]);
 			  		Printf("Port%d len : %d isr = %d tx = %d rx = %d ls = %d ms = %d xch = %d rch = %d other = %d\r\n",p,m_sio_iqueue(p),isr_count[p],isr_tx[p],isr_rx[p],isr_ls[p],isr_ms[p],isr_Xchange[p],isr_RCchange[p],isr_other[p]);
 					for(k=0;k<len;k++){
						if((k%16) == 0)
							Printf("\r\n");
						Printf("%02x ",PacketInfo.rbuf[k]);
					}						
					Printf("\r\n");
					m_sio_flush(p,FLUSH_INPUT | FLUSH_OUTPUT);
					err = DIAG_FAIL;
					break;
				}
			}	
			if(err){
				break;
			}
		}	

		if(err)	{
			m_sio_close(p);	
			return (err);
		}
		t1 = Gsys_msec;	
		if((t1 - t) >= 1000){
			rlen = 0;
			t = Gsys_msec;	
		}
		if (diag_check_press_ESC() == DIAG_ESC){
			break;
	    } 
		isr_count[p] = isr_rx[p] = isr_tx[p] = isr_ls[p] = isr_ms[p] = isr_other[p] = int_tx[p] = 0;
		Printf("%s",RUNBAR[(count++)%4]);
		if(poweronstatus == MM_STATUS){
			if(loop == 1000){
				if(count > 100)
					break;
			}
		}
		else	break;
	}		
	m_sio_close(p);	
	return (err);
}

void diag_do_MOXAUART_register_test(void)
{
	int i;
	int p;
	UINT8 v;
	int ret = MOXAUART_OK;
	pMoxaUartStruct info;	


	m_sio_init();		// init MOXA UART software data structure

		
	for ( p = 0 ; p < MoxauartFoundPortNo ; p++ ) {
		
		if(p == ConsolePort){
			Printf("Testing UART register port %d PASS.\r\n",ConsolePort); 	// console port can't do register testing.
			continue;
		}
		Printf("Testing UART register port %d \r\n", p);
   		info = &moxauart_info[p];		
		m_sio_write_register(p, MX_UART_LCR[info->bus_type], 0x80);
		m_sio_read_register(p, MX_UART_LCR[info->bus_type], &v);
		if ( v != 0x80 ) {
			ret = (-1);	// LCR error
			Printf("LCR register 0x%08x fail ! [0x80 -- 0x%02X]\r\n",Inw(info->base+MX_UART_LCR[info->bus_type]), v);		
			continue;
		}
		m_sio_write_register(p, MX_UART_DLL[info->bus_type], 0x01);
		m_sio_read_register(p, MX_UART_DLL[info->bus_type], &v);
		if ( v != 0x01 ) {
			ret = (-2);	// DLL error
			Printf("DLL register 0x%08x fail ! [0x01 -- 0x%02X]\r\n",Inw(info->base+MX_UART_DLL[info->bus_type]), v);
			continue;
		}
		m_sio_write_register(p, MX_UART_LCR[info->bus_type], 0);
		m_sio_read_register(p, MX_UART_LCR[info->bus_type], &v);
		if ( v == 0x01 ) {
			ret = (-3);	// LCR error
			Printf("LCR register fail ! [0x00 -- 0x01]\r\n");
			continue;
		}
		for ( i = 0 ; i < (int)UART_PATTERN_SIZE ; i++ ) {
			m_sio_write_register(p, MX_UART_SCR[info->bus_type], UARTPattern[i]);
			m_sio_read_register(p, MX_UART_SCR[info->bus_type], &v);
			if ( v != UARTPattern[i] ) {
				ret = (-4);	// SCR error
				Printf("SCR register 0x%08x fail ! [0x%02X -- 0x%02X]\r\n",info->base+MX_UART_SCR[info->bus_type], UARTPattern[i], v);
				break;
			}
		}
		if ( i == UART_PATTERN_SIZE ){
			Printf("OK.\r\n");
		}
	}
}


void diag_do_MOXAUART_Internal_test(void)	// Internal = Loopback
{
//	Printf("UART Internal (Loopback) test ..\r\n");
	if(moxa_uart_test(RS232_MODE,MODE_LOOPBACK) == DIAG_OK){
		Printf("UART Internal test ... OK !\r\n");
	}
	else{
		Printf("UART Internal test ... FAIL !\r\n");
	}
}

void diag_do_MOXAUART_Terminal_test(void){
	int len,i,interface_mode=RS232_MODE;
	char ch,chr;
	int p=0;
	pMoxaUartStruct info;	
	char	STATUS[2][8] ={"HIGH","LOW"};
	UINT8 val;
	UINT32	baudrate=0;
	int lsr_count=0,bi_count=0,fe_count=0,pe_count=0,oe_count=0;

    m_sio_init();
	if(diag_get_value("Please key in baudrate X bps (0) 9.6K (1) 19.2K (2) 57.6K (3) 115.2K",&baudrate,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC)
	{return;}
	m_sio_open(p,0);
	switch(baudrate){
	case 0:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_9600 , 0, 0x3);	break;  
	case 1:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_19200 , 0, 0x3);	break;  
	case 2:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_57600 , 0, 0x3);	break;  
	case 3:
		m_sio_ioctl_dll_dlm(cal_port, BAUDRATE_115200 , 0, 0x3);	break;  
	default:
		return;  
	}
	m_sio_lctrl(p,DTR_ON|RTS_ON);
    m_sio_setopmode(p,interface_mode);
	sleep(300);
	info = &moxauart_info[p];		
	Printf("\r\nTerminal Mode ->"); 
	m_sio_read_register(p, MX_UART_MSR[info->bus_type], &val);
	Printf("start test : ");
	for(;;){
		if(lsr_count != isr_ls[p]){
			lsr_count = isr_ls[p];
			Printf("\r\nData Format Error :"); 
			if(bi_count != lsr_bi[p]){ bi_count = lsr_bi[p]; Printf(" Break !"); }
			if(fe_count != lsr_fe[p]){ fe_count = lsr_fe[p]; Printf(" Frame !"); }
			if(pe_count != lsr_pe[p]){ pe_count = lsr_pe[p]; Printf(" PARITY !");}			
			if(oe_count != lsr_oe[p]){ oe_count = lsr_oe[p]; Printf(" Over !"); }	
			Printf("\r\n");
		} 
		if((len=m_sio_iqueue(p)) > 0){
			for(i=0;i<len;i++){
				m_sio_read(p,&ch,1);
				Printf("%c",ch); 
			}	
		}
		m_sio_read_register(p, MX_UART_MSR[info->bus_type], &val);
		if(val & (UART_MSR_DCTS|UART_MSR_DDSR|UART_MSR_TERI|UART_MSR_DDCD)){
			Printf("\r\nModem Status change ->");
			if(val & UART_MSR_DDCD){
				Printf("[DCD] = %s ",STATUS[(val>> 7) & 0x1]);
			}
			if(val & UART_MSR_TERI){
				Printf("[RI]  = %s ",STATUS[(val>> 6) & 0x1]);
			}				
			if(val & UART_MSR_DDSR){
				Printf("[DSR] = %s ",STATUS[(val>> 5) & 0x1]);
			}					
			if(val & UART_MSR_DCTS){
				Printf("[CTS] = %s ",STATUS[(val>> 4) & 0x1]);
			}				
			Printf("\r\n");
		}	
		chr = diag_check_press_ESC();
		if(chr == DIAG_ESC){
			m_sio_close(p);	
			return;
		}else if(chr == DIAG_OK){
			continue;
		}else{
			m_sio_write(p,&chr,1);
			while(m_sio_oqueue(p)!=0);
		}
	}	
	return; 		
} 

void diag_do_MOXAUART_Usage_test(void)
{
	m_sio_init();
	m_sio_get_uart_usage();
}

//  Ethernet Test (MAC&PHY)
burnin_cmd eth_cmd_vlaue[] = {
	{ 1,"Ethernet Internal Test             ",diag_do_ETH_internal_test, 1},      
	{ 2,"Ethernet 10MHZ-Full External Test  ",diag_do_ETH_external_10MF_test, 1}, 
	{ 3,"Ethernet 100MHZ-Full External Test ",diag_do_ETH_external_100MF_test, 1}, 
	{ 4,"Ethernet wait for ping             ",diag_do_ETH_ping_test, 1},
	{ 5,"88E6060 Write Command test         ",diag_do_ETH_88E6060_write_Command_test, 1},
	{ 6,"88E6060 Read Command test          ",diag_do_ETH_88E6060_read_Command_test, 1},
	{ 0,"",0,0}         
};     

static void printEthTestResult(int ret)
{
	PrintStr(" ...");
	switch(ret){
		case  (DIAG_OK) : 	PrintStr("OK !"); break;
		case (-1) :			PrintStr("FAIL [length]\r\n"); break;
		case (-2) :			PrintStr("FAIL [Dest Addr.]\r\n"); break;
		case (-3) :			PrintStr("FAIL [Source Addr.]\r\n"); break;
		case (-4) :			PrintStr("FAIL [Data]\r\n"); break;
		case (-5) : 		PrintStr("FAIL [Loss]\r\n"); break;
		case (-6) : 		PrintStr("FAIL [Link]\r\n"); break;
		case (-7) : 		PrintStr("FAIL [PHY]\r\n"); break;
		default : 			PrintStr("Unknow !\r\n"); break;
	} 
}

void diag_do_ETH_func(void)
{
   burnin_cmd *burnin_temp=eth_cmd_vlaue;       
   ManualTesting(burnin_temp);
}   

void diag_do_ETH_88E6060_write_Command_test(void){	// Jerry test for 88E6060 command
	int phyAddr=0,regNum=0,wData=0;
//	Lnet_chip_smi_write(int phyAddr, int regNum, ushort wData)	//
	Printf("\r\n **** Ethernet 88E6060 Write Command test ****\r\n");
	
	if(diag_get_value("Enter PHY Address:",&phyAddr,0,65535,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	
	if(diag_get_value("Enter Register Number:",&regNum,0,65535,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	
	if(diag_get_value("Enter Command data:",&wData,0,65535,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	
	Lnet_chip_smi_write(phyAddr,regNum,wData);
}

void diag_do_ETH_88E6060_read_Command_test(void){
	int phyAddr=0,regNum=0;
	ulong ETH_Read_Data=0;
	
	Printf("\r\n **** Ethernet 88E6060 Read Command test ****\r\n");
	
	if(diag_get_value("Enter PHY Address:",&phyAddr,0,65535,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	
	if(diag_get_value("Enter Register Number:",&regNum,0,65535,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}

	ETH_Read_Data = Lnet_chip_smi_read(phyAddr,regNum);
	Printf ("\r\n88E6060 Read data = 0x%x",ETH_Read_Data);
}

void diag_do_ETH_internal_test(void)		// internal = loopback
{
	int ret;

	ret = diag_do_ETH_test(1,0,MODE_LOOPBACK);		// (mm , port , mode)
	PrintStr("Ethernet Internal test");
	printEthTestResult(ret);
}   

void diag_do_ETH_external_10MF_test(void)
{
	int ret;
	int eth_mode;

	eth_mode = MODE_LAN10M |MODE_FDUP;	

	ret = diag_do_ETH_test(1, 0, eth_mode);
	PrintStr("Ethernet External test");
	printEthTestResult(ret);
}

void diag_do_ETH_external_100MF_test(void)
{
	int ret;
	int eth_mode;

	eth_mode = MODE_LAN100M|MODE_FDUP;	
	
	ret = diag_do_ETH_test(1, 0, eth_mode);
	Printf("Ethernet External test");
	printEthTestResult(ret);
}

int diag_do_ETH_test(int mm, int ethport, int mode)  // mm = warning MSG 
{
 char  *p;
 msgstr_t msgt,msgr;
 int i,j,port,err=0,count=0,loop;
 int rx,tx;
 ulong t,t1,tstart;
 
 if( mm == 1 /*&& mode != MODE_LOOPBACK */){
  if(diag_get_value("\r\nSelect Ethernet Port", &port, 1, 2, DIAG_GET_VALUE_DEC_MODE))
   return DIAG_ESC;
 }else
  port = ethport;
 
 loop = PacketInit(0);
 loop = 1000;
 if(loop == DIAG_ESC){return DIAG_ESC;}
 MemInit();
 if((msgt=MemAlloc(PacketInfo.length)) == NULL){ 
  PrintStr("*** MemAlloc fail!\r\n"); 
  return DIAG_FAIL;
 }
 
 if(mm == 1){
  Printf("==============>>>Setting information<<<=============\r\n");
//  if(mode != MODE_LOOPBACK)
   Printf("Ethernet Port         :%d\r\n",port);
  switch(mode)
  {
   case (MODE_LAN100M)    :Printf("Mode                  :100MHZ-Half\r\n"); break;
   case (MODE_LAN100M|MODE_FDUP) :Printf("Mode                  :100MHZ-Full\r\n"); break;
   case (MODE_LAN10M)    :Printf("Mode                  :10MHZ-Half\r\n"); break;
   case (MODE_LAN10M|MODE_FDUP) :Printf("Mode                  :10MHZ-Full\r\n"); break;
   case (MODE_LOOPBACK)   :Printf("Mode                  :LOOPBACK\r\n");
   default       :break;
  }
  Printf("Packet length         :%d\r\n",PacketInfo.length);
  Printf("Pattern               :0x%02x%02x%02x%02x\r\n",PacketInfo.wbuf[3],PacketInfo.wbuf[2],PacketInfo.wbuf[1],PacketInfo.wbuf[0]);
  Printf("====================================================\r\n");
 }
 
 if((msgt=MemAlloc(PacketInfo.length)) == NULL){ Printf("MemAlloc fail!\r\n"); return DIAG_FAIL;}
 
 /* Allocate MAC RX Buffer */
 for (i=0;i<6;i++) msgt->base[msgt->wndx++] = 0xff;
 for (i=6;i<12;i++) msgt->base[msgt->wndx++] = MacSrc[port][i-6];
 for (i=0;i<(PacketInfo.length-12);i++)
  msgt->base[msgt->wndx++] = PacketInfo.wbuf[i];
 msgt->wndx = PacketInfo.length;
 if(mm != 2){
 Printf("MAC init ..");
 
 err = mac_open(port,mode);
 if(err == DIAG_OK) Printf("OK!\r\n");
 else{
  Printf("FAIL!\r\n");
  return (err);
 }
 }
 /* Allocate MAC TX Buffer */
 if(mm == 1){
  Printf(ESCSTRING);
 }
 
 if(port == 1)
  PowerMode(2,1); //port II power down
 else
  PowerMode(1,1); //port I power down
 
 if(mac_send(port,msgt) == 0)
  MemFree(msgt);
 
 t = t1 = tstart = Gsys_msec;    // for timeout
 for (rx=0,msgr=NULL,tx=1,j=0;j<loop;) {
  if ((long)(Gsys_msec - t) >= 10000){
   Printf("Test TimeOut\r\n");
   break;
  }
  if( mm == 2){
   if ((long)(Gsys_msec - t) >= 1000){
    Printf("Test TimeOut\r\n");
    break;
   }
  }
  msgr = mac_read(port);
  if (msgr != NULL){
   rx++;
   p = msgr->base;
   if(msgr->wndx != PacketInfo.length){ err = (-1); break;}
   for (i=0;i<6;i++) {
    if (msgr->base[i] != 0xff){ err = (-2); break;}
   }
   for (i=6;i<12;i++) {
    if (msgr->base[i] != MacSrc[port][i-6]){ err = (-3); break;}
   }     
   for (i=12;i<(PacketInfo.length);i++) {
    if (msgr->base[i] != PacketInfo.wbuf[i-12]){ err = (-4); break;}
   }     
   if(mm == 1){
    if (diag_check_press_ESC() == DIAG_ESC)
     break;
   }else{
    if ((long)(Gsys_msec - t1) >= 3000){break;}  // timeout 
   }
   if(mm == 1){
    if(mac_send(port,msgr) != DIAG_OK){
     MemFree(msgr);
    }else{
     t = Gsys_msec;
     Printf("%s",RUNBAR[(count++)%4]);
     if(loop == 1000)
      j++;
     if(j < loop)
      tx++;     
    }
   }
   else if(mm == 0){
    if(mac_send(port,msgr) != DIAG_OK){
     MemFree(msgr);
    }else{
     t = Gsys_msec;
     Printf("%s",RUNBAR[(count++)%4]);
     if(loop == 1000)
      j++;
     if(j < loop)
      tx++;     
    }
   }  }
 }
 if(tx != rx)
  err = (-5);
 if(mm != 2){
  mac_shutdown(port);
  if(port == 1)
   PowerMode(2,0); //port II power on
  else
   PowerMode(1,0); //port I power on
 }
 MemFree(msgr);
 if(mm == 1)
  Printf("Mac test stop!\r\n");
 return (err);
}

long loopback_tx = 0;
long different_tx_rx =0;
int tx_old=0,rx_old=0;
int diag_do_ETH_burn_test(void)		// mm = warning MSG 
{
	int err=0;
	int rx,tx;

	PacketInit(0);	
	Lnet_chip_smi_write(0x1D, 0x06, 0x1D);
	sleep(10);

	UdpSend(0xffffffC0,0xAAAA,0xAAAB,PacketInfo.wbuf,64);

	Lnet_chip_smi_write(0x1D, 0x06, 0x1F);
	sleep(10);
	rx = Lnet_chip_smi_read(0x1A,0x10);
	tx = Lnet_chip_smi_read(0x1A,0x11);
	loopback_tx++;
	tx += (tx_old << 16);
	rx += (rx_old << 16);
		
//	Printf("RX = %d, TX = %d, loop = %d, diff =%d",rx,tx,loopback_tx,different_tx_rx);
	if(tx < loopback_tx){
		loopback_tx = tx;
		return -1;
	}else
		loopback_tx = tx;
	if(tx != (rx + different_tx_rx)){
		err = (-1);
		different_tx_rx = tx - rx;
	}
	if((tx&0xFFFF) == 65535) tx_old++;
	if((rx&0xFFFF) == 65535) rx_old++;
	return (err);
}

/*
 *	subroutine wait_ping
 *	function :
 *	    wait remote to ping us
 *	input :
 *	    none
 *	return :
 *	    none
 */
void diag_do_ETH_ping_test(void)
{
	int port;
	ulong savedNicIp, t;	

	Printf("Initializing network ....\r\n");
	MemInit();		/* init memory buffer   */
	ArpInit();
	if(diag_get_value("\r\nSelect Ethernet Port", &port, 1, 2, DIAG_GET_VALUE_DEC_MODE)){return;}

	G_LanPort = port;
	Printf("MAC init ..");
	mac_open(G_LanPort,MODE_NORMAL);
	savedNicIp = NicIp[G_LanPort];
	mx_read_data((UINT32)&NicIp[G_LanPort], SMC_FLAGH0_LOCALIP, 4);
	Printf("Wait ping start (use TFTP Local IP: %d.%d.%d.%d)\r\n",
			    NicIp[G_LanPort] & 0xff,
			    (NicIp[G_LanPort] >> 8) & 0xff,
			    (NicIp[G_LanPort] >> 16) & 0xff,
			    (NicIp[G_LanPort] >> 24) & 0xff);
	Printf("Initializing network ok !\r\n");
	Printf(ESCSTRING);
	t = Gsys_msec;
	for (;;) {
		if (diag_check_press_ESC() == DIAG_ESC){
			break;
		}
		NicProcess();
		if (Gsys_msec - t >= 1000)
		{
			t = Gsys_msec;
		}
	}
	mac_shutdown(port);
	Printf("Wait ping stop\r\n");

	NicIp[G_LanPort] = savedNicIp;
}

// 8. Watch Dog Timer
burnin_cmd wdt_cmd_vlaue[] = {
	{ 1,"Reboot Test (5 secs)                ",diag_do_WDT_reboot_test, 1},
	{ 0,"",0,0}         
};    
void diag_do_WDT_func(void)
{
	burnin_cmd *burnin_temp=wdt_cmd_vlaue;
	ManualTesting(burnin_temp);
}   

void diag_do_WDT_reboot_test(void)
{
	UINT32 i;		// power of 2
	ulong t = 0;

	if(diag_get_value("Reboot time (power of 2)>>",&i,0,15,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sleep(10);

	WatchDog_SetTopInit(i);		// 2^27  APB = 20MHz
	WatchDog_SetTop(i);
	WatchDog_SetResetPulseLength(0x1);		// 0b111
	WatchDog_SetIsr();
	WatchDog_EnableIrq();
	WatchDog_SetMode(watchdog_reset_mode_interrupt);
	WatchDog_Enable();

	
	for(i=1;;){
		PrintStr(".");
		if((Gsys_msec - t) > 100){
			Printf("%d",i++);
			t = Gsys_msec;
		}
	}
}

void diag_do_SYSRESET_func(void)
{
	/* enable WDT to reset system */
	Outw(S2E_WDT_BASE + WDT_TORR, 0x00);
	Outw(S2E_WDT_BASE + WDT_CONTROL, 0x1D);
//	GpioSetDir(PIO_WADG ,PIO_OUT);
	while(1);
}

/*
 * Jumper & Dip-Switch Test Item
 */
void diag_do_jp_test(void);
void diag_do_swbutton_test(void);
void diag_do_jp2_test(void);
burnin_cmd jump_cmd_vlaue[] = {
    { 1,"JP1 test                        ",diag_do_jp_test, 1},  
    { 2,"SW button test                  ",diag_do_swbutton_test, 1},
#if E1242
    { 3,"2*3 Jumpers test                 ",diag_do_jp2_test, 1},
#endif
    { 0,"",0,0}         
};     

void diag_do_jp_test(void)
{
	int jp1old=0;

	Printf(ESCSTRING);
	Printf("Now JP1 status following: \r\n");	 
	if (GetJpStatus(PIO_JP1) == 1){Printf("JP1-[SHORT]   ");	jp1old = 1;}
	else{Printf("JP1-[OPEN]   "); }	
	Printf("Please change Jumper status\r\n");
	while(1){
		if(diag_check_press_ESC() == DIAG_ESC)
			break;
		if(GetJpStatus(PIO_JP1) != jp1old){
			sleep(100);
			if(GetJpStatus(PIO_JP1) != jp1old){
				jp1old = GetJpStatus(PIO_JP1);
				if(jp1old == 1)
					Printf("JP1 changed status-[SHORT]   \r\n");
				else
					Printf("JP1 changed status-[OPEN]    \r\n");
				sleep(50);
			}
		}
	}
}

void diag_do_swbutton_test(void)
{
	int swbuttonold=0;

	Printf(ESCSTRING);
	Printf("Now SW button status following: \r\n");	 
	swbuttonold = GetJpStatus(PIO_SW_BUTTON);
	if(swbuttonold){
		Printf("SWBT-[Press] ");
	}else{
		Printf("SWBT-[Free] ");
	}
	Printf("\r\n");
	Printf("Please change SW button status\r\n");
	while(1){
		if(diag_check_press_ESC() == DIAG_ESC)
			break;
		if(swbuttonold != GetJpStatus(PIO_SW_BUTTON)){
			swbuttonold ^= 0x1;
			Printf("SW button change status.\r\n");
			for(;;){
				if(diag_check_press_ESC() == DIAG_ESC)	return;
				if(swbuttonold != GetJpStatus(PIO_SW_BUTTON)) break;
			}
		}
	}
}

void diag_do_jp2_test(void){

	Printf("\r\nPlease change 2*3 Jumpers status...\r\n");
	Printf(ESCSTRING);

#if E1240
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();

	diag_do_AI_input_scan_test();
#endif
}

UINT8 diag_do_jp2mp_test(void){
	UINT8 jumper = 0;
	#if E1240
	int j = 0;
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();

	do{
		AI_TYPE_CH_Select(j);
		AI_CH_Select(j);
		sleep(10);
		if(AI_TYPE_READ() == Voltage_mode){
//			jumper |= 1 << j;
		}
		else{
			jumper |= 1 << j;
		}
		sleep(70);
		if (++j == CHANNEL_NUMBER){
			break;
		}
	}while(1);
#endif
	return jumper;
}

void diag_do_JP_SW_func(void)
{
   burnin_cmd *burnin_temp=jump_cmd_vlaue;
   ManualTesting(burnin_temp);
}

/*
 * LED Test Item
 */
void diag_do_READYLED_test(void);
burnin_cmd led_cmd_vlaue[] = {
    { 1,"Ready LED On/Off test       ",diag_do_READYLED_test, 1},  
    { 0,"",0,0}         
};     

void diag_do_READYLED_test(void)
{
	int i;

    Printf("Ready LED Test !\r\n");
	Printf(ESCSTRING);
	while(1){
		if(diag_get_value("Off(0)/On(1)>>",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
		ReadyLed(i);
	}
}    

// Jerry Wu 1/9,09'
void diag_do_LED_func(void)
{
   burnin_cmd *burnin_temp=led_cmd_vlaue;
   ManualTesting(burnin_temp);
}   


#if Analog_Input
void diag_do_ADC_setup(void);
#endif
void diag_do_IO_cal_para_setup(void);

#if E1240 | E1242
burnin_cmd AI_cmd_value[] = {
	{  1,"ADC Parameters Setting           ",diag_do_ADC_setup, 1}, 
	{  2,"Module Temp. Monitoring\r\n----------------------------------------------------------------------------"
                                          ,module_temperature_monitor,1},
  {  3,"AI Channel Select                ",diag_AI_CH_sel, 1},
	{  4,"AI Cal. Parameters               ",diag_do_IO_cal_para_setup, 1},
  {  5,"AI Sample Test                   ",diag_do_AI_test, 1},  
  {  6,"Noise Loop Test                  ",diag_do_noise_loop_test, 1},  
  {  7,"AI Channel Scan Test             ",diag_do_AI_scan_test, 1},
  {  8,"AI Input State Test              ",diag_do_AI_input_scan_test, 1}, 
  {  9,"AI EMI Scan Test                 ",diag_do_AI_EMI_scan_test, 1}, 
  { 10,"AI EMS Scan Test                 ",diag_do_AI_EMS_scan_test, 1}, 
	{ 11,"AI Single Channel Cal.           ",AI_Auto_System_Calibration_single_ch, 1},
	{ 12,"AI Single Channel Linearity      ",diag_do_AI_linearity_ch_test, 1}, 
	{ 13,"AI Volatge Type Cal.             ",diag_do_AI_Voltage_Calibration, 1},
	{ 14,"AI Volatge Type Linearity        ",diag_do_AI_Voltage_linearity_test, 1}, 
	{ 15,"AI Current Type Cal.             ",diag_do_AI_Current_Calibration,1},
	{ 16,"AI Current Type Linearity\r\n----------------------------------------------------------------------------"                                 ,diag_do_AI_Current_linearity_test,1},
	{ 17,"Calibrator select                ",calibrator_select, 0},
	
#if E1242
	{ 18,"DI Read Test                     ",diag_do_DI_Read_test, 1},
	{ 19,"DO Test                          ",diag_do_SPI_DO_test, 1},
	{ 20,"DO Manual Test                   ",diag_do_SPI_DO_Manual_test, 1},
	{ 21,"DO Pulse test                    ",diag_do_DO_Pulse_test, 1},
	{ 22,"DIO Loop Back Test               ",diag_do_SPI_DIO_Loop_Back_test, 1},
	{ 23,"DIO Burn in test                 ",diag_do_SPI_DIO_Burn_in_test, 1},
/*	{ 18,"DI Read Test                     ",diag_do_DI_Read_test, 1},
	{ 19,"DO 55AA Test                     ",diag_do_SPI_DIO_55AA_test, 1},
	{ 20,"DO 00FF Test                     ",diag_do_SPI_DIO_00FF_test, 1},
	{ 21,"DO Manual Test(P8~P15)           ",diag_do_SPI_DIO_Manual_test, 1},
	{ 22,"DO Loop Test(P8~P15)             ",diag_do_SPI_DO_Loop_test, 1},
	{ 23,"DIO Loop Back Test(P0~P15)       ",diag_do_SPI_DIO_Loop_Back_test, 1},
	{ 24,"diag_do_DIO_EMS_test             ",diag_do_DIO_EMS_test, 1},
	{ 25,"DO Pulse test (500Hz)            ",diag_do_DO_Pulse_test, 1},
*/
//	{ 25,"1KHz Plus Output                   ",diag_do_SPI_DO_1KHz_test, 1},
//	{	26,"EMS/EMI Test												",diag_do_E1212_EMI_scan_test, 1},
#endif	
//	{ 18,"test                ",diag_do_AI_MP_calibration, 1},
    { 0,"",0,0} 
};
#endif

#if E1262
burnin_cmd TC_cmd_value[] = {
	{  1,"ADC Parameters Setting           ",diag_do_ADC_setup, 1}, 
	{  2,"Module Temp. Monitoring
----------------------------------------------------------------------------"
                                            ,module_temperature_monitor,1},
    {  3,"TC Channel Select                ",diag_AI_CH_sel, 1},
	{  4,"TC Cal. Parameters               ",diag_do_IO_cal_para_setup, 1},
    {  5,"TC Sample Test                   ",diag_do_AI_test, 1},  
    {  6,"Noise Loop Test                  ",diag_do_noise_loop_test, 1},  
    {  7,"TC Channel Scan Test             ",diag_do_AI_scan_test, 1},
    {  8,"TC Burn-out Det. Test            ",diag_do_TC_burn_out_detection_test, 1}, 
    {  9,"TC EMI Scan Test                 ",diag_do_AI_EMI_scan_test, 1}, 
    { 10,"TC EMS Scan Test                 ",diag_do_AI_EMS_scan_test, 1}, 
	{ 11,"TC Single Channel Cal.           ",AI_Auto_System_Calibration_single_ch, 1},
	{ 12,"TC Single Channel Linearity      ",diag_do_AI_linearity_ch_test, 1}, 
	{ 13,"TC Single Gain Cal.              ",diag_do_TC_Auto_System_Calibration_single_gain, 1},
	{ 14,"TC Single Gain Linearity         ",diag_do_TC_gain_linearity_test, 1}, 
	{ 15,"TC Full System Cal.              ",TC_Full_Auto_System_Calibration,1},
	{ 16,"TC Full System Linearity         ",diag_do_TC_full_linearity_test,1},
//----------------------------------------------------------------------------"
	{ 17,"Calibrator select                ",calibrator_select, 0},
    { 0,"",0,0} 
};
#endif

#if E1260
burnin_cmd RTD_cmd_value[] = {
	{  1,"ADC Parameters Setting           ",diag_do_ADC_setup, 1}, 
	{  2,"Module Temp. Monitoring\r\n----------------------------------------------------------------------------"
                                            ,module_temperature_monitor,1},
    {  3,"RTD Channel Select               ",diag_AI_CH_sel, 1},
	{  4,"RTD Cal. Parameters              ",diag_do_IO_cal_para_setup, 1},
    {  5,"RTD Sample Test                  ",diag_do_AI_test, 1},  
    {  6,"Noise Loop Test                  ",diag_do_noise_loop_test, 1},  
    {  7,"RTD Channel Scan Test            ",diag_do_AI_scan_test, 1},
    {  8,"RTD Burn-out Det. Test           ",diag_do_RTD_burn_out_detection_test, 1}, 
    {  9,"RTD EMI Scan Test                ",diag_do_AI_EMI_scan_test, 1}, 
    { 10,"RTD EMS Scan Test                ",diag_do_AI_EMS_scan_test, 1}, 
	{ 11,"RTD Single Channel Cal.          ",AI_Auto_System_Calibration_single_ch, 1},
	{ 12,"RTD Single Channel Linearity     ",diag_do_AI_linearity_ch_test, 1}, 
	{ 13,"RTD Single Gain Cal.             ",diag_do_RTD_Auto_System_Calibration_single_gain, 1},
	{ 14,"RTD Single Gain Linearity        ",diag_do_RTD_gain_linearity_test, 1}, 
	{ 15,"RTD Full System Cal.             ",RTD_Full_Auto_System_Calibration,1},
	{ 16,"RTD Full System Linearity        ",diag_do_RTD_full_linearity_test,1},
	{ 18,"RTD Burn-in test                 ",diag_burn_in_RTD,1},
//----------------------------------------------------------------------------"
	{ 17,"Calibrator select                ",calibrator_select, 0},
    { 0,"",0,0} 
};
#endif

#if E1241
burnin_cmd AO_cmd_value[] = {
/*	{  1,"ADC Parameters Setting           ",diag_do_ADC_setup, 1}, 
	{  2,"Module Temp. Monitoring
----------------------------------------------------------------------------"
                                            ,module_temperature_monitor,1},
    {  3,"RTD Channel Select               ",diag_AI_CH_sel, 1},
	{  4,"RTD Cal. Parameters              ",diag_do_IO_cal_para_setup, 1},
    {  5,"RTD Sample Test                  ",diag_do_AI_test, 1},  
    {  6,"Noise Loop Test                  ",diag_do_noise_loop_test, 1},  
    {  7,"RTD Channel Scan Test            ",diag_do_AI_scan_test, 1},
    {  8,"RTD Burn-out Det. Test           ",diag_do_RTD_burn_out_detection_test, 1}, 
    {  9,"RTD EMI Scan Test                ",diag_do_AI_EMI_scan_test, 1}, 
    { 10,"RTD EMS Scan Test                ",diag_do_AI_EMS_scan_test, 1}, 
	{ 11,"RTD Single Channel Cal.          ",AI_Auto_System_Calibration_single_ch, 1},
	{ 12,"RTD Single Channel Linearity     ",diag_do_AI_linearity_ch_test, 1}, 
	{ 13,"RTD Single Gain Cal.             ",diag_do_RTD_Auto_System_Calibration_single_gain, 1},
	{ 14,"RTD Single Gain Linearity        ",diag_do_RTD_gain_linearity_test, 1}, 
	{ 15,"RTD Full System Cal.             ",RTD_Full_Auto_System_Calibration,1},
	{ 16,"RTD Full System Linearity        ",diag_do_RTD_full_linearity_test,1},
*///----------------------------------------------------------------------------"
	{ 17,"Calibrator select                ",calibrator_select, 0},
    { 0,"",0,0} 
};
#endif

burnin_cmd ADC_cmd_value[] = {
    {  1,"ADC Reference                    ",diag_AI_set_ref, 1},  
	{  2,"ADC Internal Reference           ",diag_AI_set_ref_in_voltage, 1},  
	{  3,"ADC Channel Select VIN+          ",diag_AI_ADC_CH_sel_vp, 1},  
	{  4,"ADC Channel Select VIN-          ",diag_AI_ADC_CH_sel_vn, 1},  
	{  5,"ADC Modulator Factor             ",diag_AI_ADC_m_factor, 1},
	{  6,"ADC Decimation Ratio             ",diag_AI_ADC_decimation_ratio, 1},  
	{  7,"ADC PGA Gain                     ",diag_AI_ADC_PGA_gain, 1},  
	{  8,"ADC Digital Filter               ",diag_AI_ADC_filter_sel, 1},
	{  9,"ADC Chip Calibration             ",diag_AI_ADC_calibration_sel, 1},
	{ 10,"ADC Memory dump                  ",diag_AI_ADC_Memory_dump, 1},
	{ 11,"ADC Data Format                  ",diag_AI_ADC_unipolar_set, 1},
    {  0,"",0,0}         
};
//#endif

burnin_cmd IO_cal_para_cmd_value[] = {
	{  1,"IO Cal.Para. Dump                ",cal_parameters_dump, 1},
	{  2,"IO Cal.Para. Input               ",diag_cal_parameters_input, 1},
	{  3,"IO Cal.Para. Channel Duplicate   ",diag_cal_parameters_duplicate, 1},
	{  4,"IO Cal.Para. Block Duplicate     ",diag_cal_parameters_block_duplicate,1},
    {  0,"",0,0}         
};

void diag_do_ADC_setup(void){
	burnin_cmd *burnin_temp=ADC_cmd_value;       
	ManualTesting(burnin_temp);
}

void diag_do_IO_cal_para_setup(void){
	burnin_cmd *burnin_temp=IO_cal_para_cmd_value;       
	ManualTesting(burnin_temp);
}

void diag_do_IO_func(void)
{
	UINT32 flag;
#if E1240 | E1242
	burnin_cmd *burnin_temp=AI_cmd_value;       
#endif
#if	E1262
	burnin_cmd *burnin_temp=TC_cmd_value;       
#endif
#if	E1241
	burnin_cmd *burnin_temp=AO_cmd_value;       
#endif
#if E1260
	burnin_cmd *burnin_temp=RTD_cmd_value;       
#endif

	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;
#if	Analog_Input
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();

	ADC_PGA_Gain(0);		// load offset/gain parameters from EEPROM
#endif
#if E1240 | E1242 | E1262 | E1241
	Set_Calibrator(calibrator_ca150);
#endif
#if E1260
	Set_Calibrator(calibrator_fluke);
#endif
	Calibrator_communication_init();

	GpioWriteData(PIO_EEPROM_PROTECT,0);
	ManualTesting(burnin_temp);
	GpioWriteData(PIO_EEPROM_PROTECT,1);

	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();
	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 0){
		mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);
		if(flag & FLAG_CAL)
			diag_do_set_clean_flag(0,FLAG_CAL);
	}
}

// 10. SPI Flash
void diag_do_SPI_Flash_protectd(void);
burnin_cmd spi_flash_cmd_vlaue[] = {
	{ 1,"SPI Flash Byte Test            ",diag_do_SPI_Flash_byte, 1},
	{ 2,"SPI Flash Short Test           ",diag_do_SPI_Flash_short, 1},
	{ 3,"SPI Flash Long Test            ",diag_do_SPI_Flash_long, 1},
	{ 4,"SPI Flash Protected Test       ",(void *)diag_do_SPI_Flash_protect, 1},
	{ 5,"SPI Flash Protected Disable    ",diag_do_SPI_Flash_protectd, 1},
	{ 0,"",0,0}         
};    


/**	\brief
 *
 *	SPI Flash test main function.
 *	It will call the sub menu.
 *
 */
void diag_do_SPI_Flash_func(void)
{
	burnin_cmd *burnin_temp=spi_flash_cmd_vlaue;
	GpioWriteData(PIO_FLASH_PROTECT,0);
	ManualTesting(burnin_temp);
	GpioWriteData(PIO_FLASH_PROTECT,1);
}

void diag_do_SPI_Flash_byte(void)
{
    UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	PrintStr("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("Data Pattern 1~6>>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	spi_byte_mptest(1, i);		// (mm , pattern)
}

void diag_do_SPI_Flash_short(void)
{
    UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	PrintStr("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("Data Pattern 1~6>>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	spi_short_mptest(1, i);
}

void diag_do_SPI_Flash_long(void)
{
    UINT32 i;
	burnin_cmd *burnin_temp=Datapattern_vlaue;
    
	PrintItemMsg(burnin_temp);	
	PrintStr("---------------------------------------------------------------------------\r\n");
	if(diag_get_value("Data Pattern 1~6>>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	spi_long_mptest(1, i);
}

static int spi_rw_test(int mm , int pattern, int size_test)
{
	UINT32 start_addr,end_addr;
	UINT32 i;
	static UINT32 Content; 
	char *ptr;
	void *ptr_data_place;
	UINT8 wdata8, rdata8;
	UINT16 wdata16, rdata16;
	UINT32 wdata32, rdata32;
	int ok;
	int read_length = 0;

	
	Content = diag_get_data_pattern(pattern);
	

	// 1. Write data
	ptr = (char*)&Content;
	start_addr = FIRMWARE_FLASH_BASE;
	end_addr = FLASH_MAX_SIZE;
	if( mm == 1 ){
		Printf("\r\nBegin SPI FLASH %x bytes access test from 0x%08x to 0x%08x ...\r\n",size_test,start_addr,end_addr);
	}
#define DISPLAY_MASK	0x3FFF	/* display every 16K bytes */
	for (i = start_addr ; i < end_addr; i += size_test){
		if (!(i & DISPLAY_MASK))
			Printf("program 0x%08x-0x%08x ...", i, i + DISPLAY_MASK);
		if(mx_write_data(i, (UINT32)&(ptr[i%(4/size_test)]), size_test) != 0){
			Printf("SPI Flash Write Error\r\n");
			return (-1);
		}
		if (!(i & DISPLAY_MASK))
			Printf("OK\r\n");
	}

	// 2. Read data and compare
	Printf("Comparing...\r\n");
	ptr_data_place = (char*)SDRAM_TEST_BASE;
	for (i = start_addr ; i < end_addr; i += size_test){
		if (!(i & DISPLAY_MASK))
			Printf("compare 0x%08x-0x%08x ...", i, i + DISPLAY_MASK);
		read_length = mx_read_data((UINT32)ptr_data_place , i , size_test);
		if(read_length != size_test){
			Printf("SPI Flash Read Data Error\r\n");
			return (-1);
		}
		if (size_test == 1)
		{
			rdata8 = *(UINT8*)ptr_data_place;
			wdata8 = (UINT8)ptr[i%4];
			ok = (rdata8 == wdata8);
			if (!ok)
				Printf("\r\nWrite = %x , Read = %x\r\n" , wdata8, rdata8);
		}
		else if (size_test == 2)
		{
			rdata16 = *(UINT16*)ptr_data_place;
			wdata16 = *(UINT16*)&(ptr[i%2]);
			ok = (rdata16 == wdata16);
			if (!ok)
				Printf("\r\nWrite = %x , Read = %x\r\n" , wdata16, rdata16);
		}
		else
		{
			rdata32 = *(UINT32*)ptr_data_place;
			wdata32 = *(UINT32*)ptr;
			ok = (rdata32 == wdata32);
			if (!ok)
				Printf("\r\nWrite = %x , Read = %x\r\n" , wdata32, rdata32);
		}
		if( !ok ){
			Printf("SPI Flash Compare Data Error\r\n");
			return (-1);
		}
		if (!(i & DISPLAY_MASK))
			Printf("OK\r\n");
	}

	Printf("SPI Flash Byte Test OK\r\n");
	return 0;
}

extern void mx_write_enable(void);
void flash_write_protect(UINT8 enable){
	GpioWriteData(PIO_FLASH_PROTECT,1);//disable HW
	mx_write_enable();			// Setting Write Enable Latch bit 
	if(enable)	//write protect
		mx_write_status_reg(0xbc);
	else
		mx_write_status_reg(0);
	sleep(100);
	GpioWriteData(PIO_FLASH_PROTECT,0);
}

void diag_do_SPI_Flash_protectd(void){
	flash_write_protect(0);
}

UINT8 diag_do_SPI_Flash_protect(void){
	UINT32 sector_addr;
	int ret;

	// 1. Erase a whole flash using sector erase
	flash_write_protect(1);
	Printf("SPI Flash Protected test ");
	sector_addr = FIRMWARE_FLASH_BASE;
	ret = mx_page_program(sector_addr, SDRAM_DOWNLOAD_TEMP_BASE, 4);
	if( ret != 0){
		Printf("PASS!!\r\n");
		flash_write_protect(0);
		return 0;
	}
	else
		PrintStr("NG!!\r\n");
	
	flash_write_protect(0);
	return 1;
}

static int spi_byte_mptest(int mm , int pattern)
{
	int ret;

	// Erase a whole flash using chip erase
	PrintStr("Erasing a whole flash, please wait about 10 seconds...");
	ret = mx_chip_erase();
	if( ret != 0){
		Printf("SPI Flash Erase Error\r\n");
		return -1;
	}
	else
		PrintStr("OK\r\n");

	ret = spi_rw_test(mm, pattern, 1);
	if (ret == 0)
		Printf("SPI Flash Byte Test OK\r\n");
	else
		Printf("..............SPI Flash Byte Test Error ...............\r\n");

	return ret;
}


static int spi_short_mptest(int mm , int pattern)
{
	UINT32 block_addr;
	int ret;

	// 1. Erase a whole flash using block erase
	PrintStr("Erasing a whole flash\r\n");
	block_addr = FIRMWARE_FLASH_BASE;
	while (block_addr < FLASH_MAX_SIZE) {
		//Printf("erase 0x%08x-0x%08x ...", block_addr, block_addr + FLASH_BLOCK_SIZE - 1);
		PrintStr("block erase 0x");
		PrintHex(block_addr);
		PrintStr("-0x");
		PrintHex(block_addr + FLASH_BLOCK_SIZE - 1);
		PrintStr("...");
		ret = mx_block_erase(block_addr);
		if( ret != 0){
			Printf("SPI Flash Erase Error\r\n");
			return -1;
		}
		else
			PrintStr("OK\r\n");
		block_addr += FLASH_BLOCK_SIZE;
	}

	ret = spi_rw_test(mm, pattern, 2);	// short = 2 bytes
	if (ret == 0)
		Printf("SPI Flash Short Test OK\r\n");
	else
		Printf("..............SPI Flash Short Test Error ...............\r\n");

	return ret;
}



static int spi_long_mptest(int mm , int pattern)
{
	UINT32 sector_addr;
	int ret;

	// 1. Erase a whole flash using sector erase
	PrintStr("Erasing a whole flash\r\n");
	sector_addr = FIRMWARE_FLASH_BASE;
	while (sector_addr < FLASH_MAX_SIZE) {
		//Printf("erase 0x%08x-0x%08x ...", sector_addr, sector_addr + FLASH_SECTOR_SIZE - 1);
		PrintStr("sector erase 0x");
		PrintHex(sector_addr);
		PrintStr("-0x");
		PrintHex(sector_addr + FLASH_SECTOR_SIZE - 1);
		PrintStr("...");
		ret = mx_sector_erase(sector_addr);
		if( ret != 0){
			Printf("SPI Flash Erase Error\r\n");
			return -1;
		}
		else
			PrintStr("OK\r\n");
		sector_addr += FLASH_SECTOR_SIZE;
	}
	
	ret = spi_rw_test(mm, pattern, 4);	// long = 4 bytes
	if (ret == 0)
		Printf("SPI Flash Long Test OK\r\n");
	else
		Printf("..............SPI Flash Long Test Error ...............\r\n");

	return ret;
}

// FlashProgram
int FlashromProgram(UINT32 dest, UINT32 src, long len)
{
    long wlen, sector_size;

    sector_size = GetSectorSize(sysModelInfo[board_index].smcflash0, 0);
	
	while (len > 0)
	{
		PrintStr("Programming flashrom ");
		PrintHex(dest);
		PrintStr("-");
		if (len >= sector_size)
			wlen = sector_size;
		else
			wlen = len;
		PrintHex(dest + wlen);
		PrintStr(" .. erase ..");
		if(mx_sector_erase(dest) != 0)
			break;
		PrintStr("write..");
   		if (mx_write_data(dest, src, wlen)) 
   			break;
		PrintStr("OK\r\n");
		dest += wlen;
		src += wlen;
		len -= wlen;
	}

	return (len);
}

// BIOS Upgrade
void diag_do_BIOS_upgrade(void)
{
	char	*ptr;
    long	len, lenArray[2] = {4096, 0};
	UINT32  addr, addrArray[2] = {BOOTLOADER_FLASH_BASE, MANUFACTURE_PROGRAM_FLASH_BASE};
	int i;
 
    PrintStr("\r\n");
    ptr = (char*)SDRAM_DOWNLOAD_TEMP_BASE;
    len = XmodemRecv(ConsolePort,ptr);
	if (len <= 0) {
    	PrintStr("Xmodem download Fail.\r\n");
	}else{
		if (len >= BIOS_MAX_SIZE) {
			PrintStr("bios too big!\r\n");
		}else{
			PrintStr("Xmodem download ok.\r\n");
			lenArray[1] = len - MANUFACTURE_PROGRAM_FLASH_BASE;  /* skip Manufacutre/Test config data area */
			for (i = 0; i < 2; i++)
			{
			    ptr = (char*)(SDRAM_DOWNLOAD_TEMP_BASE + addrArray[i]);
			    addr = addrArray[i];    /* skip Manufacutre/Test config data area */
			    len = lenArray[i];
    			if(FlashromProgram(addr, (UINT32)ptr, len) == 0)
    			{
    				//Printf("Update BIOS 0x%08x-0x%08x and Length = %d bytes .. OK!\r\n", addr, (addr+len), len);
    				PrintStr("Update BIOS 0x");
    				PrintHex(addr);
    				PrintStr("-");
    				PrintHex(addr+len);
    				PrintStr(" .. OK!\r\n");
    			}
    			else
    			{
    				//Printf("Update BIOS 0x%08x-0x%08x and Length = %d bytes .. FAIL!\r\n", addr, (addr+len), len);
    				PrintStr("Update BIOS 0x");
    				PrintHex(addr);
    				PrintStr("-");
    				PrintHex(addr+len);
    				PrintStr(" .. OK!\r\n");    				
    			}
			}
		}
	}    
}


// Firmware Lan
extern ulong tftp_file_length;
extern ulong serverip, localip;
void diag_do_FIRM_Lan(void)
{
	char 	*firm_name = "E12xx.1kp";
	char	*ptr;
	UINT32  addr;
	int		i;
	
	addr = FIRMWARE_FLASH_BASE;		
	Printf("\r\nPlease key in file name (E12xx.fs) - :");
	if ((i = ConGetString(ErrMsg, sizeof(ErrMsg))) == -1)
  	return;
	else if (i!=0)
	{
		firm_name = ErrMsg;
	}
	
	Printf("\r\n\r\nFile Name : %s\r\n", firm_name);
		
	ptr = (uchar *)SDRAM_DOWNLOAD_TEMP_BASE;
	
	G_LanPort = 1;
		
	if (tftp_protocol_recv(firm_name) != 1)
	{
		tftp_close();
		Printf("TFTP Receive Fail\r\n");
		return;
	}
	tftp_close();
	Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes\r\n", addr, (addr+tftp_file_length), tftp_file_length);	
	if (FlashromProgram(addr, (UINT32)ptr, tftp_file_length) == 0)
		Printf("\r\nUpdate File 0x%08x-0x%08x and Length = %d bytes .. OK!\r\n", addr, (addr+tftp_file_length), tftp_file_length);
	else
	{
		Printf("Update File 0x%08x-0x%08x and Length = %d bytes .. FAIL!\r\n", addr, (addr+tftp_file_length), tftp_file_length);
		return;
	}
	return;	
}

void diag_do_FIRM_Uart(void)
{
	char	*ptr;
    long	len;
	UINT32  addr;
	addr = FIRMWARE_FLASH_BASE;

    ptr = (uchar *)SDRAM_DOWNLOAD_TEMP_BASE;
    len = XmodemRecv(ConsolePort,ptr);
	if (len <= 0) {
    	Printf("Xmodem download Fail.\r\n");
		return;
	}
	Printf("Xmodem download ok.\r\n");
	Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes\r\n", addr, (addr+len), len);
	if(FlashromProgram(addr, (UINT32)ptr,len) == 0)
		Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes .. OK!\r\n", addr, (addr+len), len);
	else
		Printf("Programming flashrom 0x%08x-0x%08x and Length = %d bytes .. FAIL!\r\n", addr, (addr+len), len);
}

int diag_do_tftp_set_ip(void);
void diag_do_EraseFlash_func(void);

burnin_cmd firm_cmd_vlaue[] = {
    { 1,"Console Download FS file         ",diag_do_FIRM_Uart, 1},  
    { 2,"TFTP Download FS file            ",diag_do_FIRM_Lan, 1},  
	{ 3,"TFTP User Configure              ",(void*)diag_do_tftp_set_ip, 1},
	{ 4,"Erase Flash                      ",diag_do_EraseFlash_func, 1},
//	{ 5,"Erase FAT                        ",st_flashrom_erase_FAT, 1},
    { 0,"",0,0}         
};

void diag_do_EraseFlash_func(void){
    long wlen, sector_size;
	UINT32 	dest = FIRMWARE_FLASH_BASE;
	UINT32  len = 0x1B0000 ;//0x200000-0x4000
	sector_size = GetSectorSize(sysModelInfo[board_index].smcflash0, 0);
	while (len > 0)
	{
		PrintStr("Erasing flashrom ");
		PrintHex(dest);
		PrintStr("-");
		if (len >= sector_size)
			wlen = sector_size;
		else
			wlen = len;
		PrintHex(dest + wlen);
		PrintStr(" .. erase ..");
		if(mx_sector_erase(dest) != 0)
			break;
		PrintStr("OK\r\n");
		dest += wlen;
		len -= wlen;
	}
}

void diag_do_DOWNLOAD_func(void)
{
	burnin_cmd *burnin_temp=firm_cmd_vlaue;
    ManualTesting(burnin_temp);	
}

void diag_do_JUMPTOGO_func(void)
{
	int len;

	Printf("Set Jump to Firmware flag...\r\n");
	len = 64;
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
	// set run firmware flag for next boot
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|JUMP_TO_FIRM_OFFSET) = 1;
	FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len);
	// reboot
	Printf("Reboot to load firmware...\r\n");
	// watch dog reset
	Outw(S2E_WDT_BASE + WDT_TORR, 0x00);
	Outw(S2E_WDT_BASE + WDT_CONTROL, 0x1D);
	while (1);

}

int diag_do_tftp_set_ip(void)
{
	ulong local=0, server=0;
	
	while(1)
	{
		Printf("\r\nPlease key in Local IP (XXX.XXX.XXX.XXX) - :");
		if (ConGetString(ErrMsg, sizeof(ErrMsg)) == -1)
       	return -1;
    
    if ((local = inet_addr(ErrMsg)) == INADDR_NONE)
    {
    	Printf("IP Value Error!");
    	continue;
    }
    else
    {
    	Printf("\r\n");
    	break;
    }
  }
  
  while(1)
	{
		Printf("\r\nPlease key in Server IP (XXX.XXX.XXX.XXX) - :");
		if (ConGetString(ErrMsg, sizeof(ErrMsg)) == -1)
       	return -1;
    
    if ((server = inet_addr(ErrMsg)) == INADDR_NONE)
    {
    	Printf("IP Value Error!");
    	continue;
    }
    else
    {
    	Printf("\r\n");
    	break;
    }
  }    
	
	if (tftp_set_ip(local, server))
	{
		Printf("\r\nSet to TFTP Default Fail!!\r\n");
		return -1;
	}
	
	Initboardinfo(board_index);
	return 0;
}

// Set MAC address & Serial Number
burnin_cmd SerMac_cmd_vlaue[] = {
    { 1,"Set serial number           ",diag_do_SETSERIAL_test, 1},  
	{ 2,"Set MAC address             ",diag_do_SETMAC_test, 1},	
    { 0,"",0,0}         
};    

void diag_do_SETSERIAL_test(void)
{
	int serial;
	long len;

	if(diag_get_value("Set Serial Number:",&serial,0,99999,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|SERIAL_OFFSET) = serial;

	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0){
		Printf("FAIL !\r\n");
		return;
	}
	Initboardinfo(board_index);
}

void diag_do_SETMAC_test(void)
{
	UINT32 i;
//	UINT8  mac[2][6];
	UINT8  mac[6];
	ulong temp=0;
	long len;

	Printf("\r\nPlease key in MAC address (001122334455) - ");
	if(ConGetString(ErrMsg,sizeof(ErrMsg)) == -1){ Printf(AbortString); return;}
	if(StrLength(ErrMsg) != 12){
		Printf("\r\nInvalid number !");
		return;
	}
	if(!IsHex(ErrMsg)){
		Printf("\r\nInvalid number !");
		return;
	}
	for(i=0;i<6;i++){
		mac[i] = Atob(&ErrMsg[i * 2]);
	}
	temp = (((mac[2] & 0xff) << 24) | 
		    ((mac[3] & 0xff) << 16) | 
			((mac[4] & 0xff) <<  8) | 
			(mac[5] & 0xff));
	temp++;
	len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
//	for (i=0;i<6;i++) VPchar((SDRAM_DOWNLOAD_TEMP_BASE|MACA_OFFSET) + i) = mac[0][i];
	for (i=0;i<6;i++) VPchar((SDRAM_DOWNLOAD_TEMP_BASE|MACA_OFFSET) + i) = mac[i];
//	for (i=0;i<6;i++) VPchar((SDRAM_DOWNLOAD_TEMP_BASE|MACB_OFFSET) + i) = mac[1][i];
	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0){
		Printf("FAIL !\r\n");
		return;
	}
	Initboardinfo(board_index);

}



/*
 * Set Serial number and MAC Address Item
 */
void diag_do_SERIAL_MAC_func(void)
{
   burnin_cmd *burnin_temp=SerMac_cmd_vlaue;
   ManualTesting(burnin_temp);
}    

// HWEXID
void diag_do_HWEXID_func(void)
{
	int i;
	UINT32 j;
	long len;


	Printf("\r\n");
	for(i=0;i<MOXA_ioLogik_E1200_MODEL_MAX_NUM;i++){
		Printf("Item = %02d HWEID --> 0x%08x [%s]\r\n",i,sysModelInfo[i].hw_exid,sysModelInfo[i].modelName);
	}
	if(diag_get_value("Set Device Model :",&j,0, (MOXA_ioLogik_E1200_MODEL_MAX_NUM),DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}

	len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[j].hw_exid;

	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0){
		Printf("FAIL !\r\n");
		return;
	}
	Initboardinfo(0);
}

/*
 * fFlag Setting Item
 */
void diag_do_CALFLAG_test(void);
void diag_do_REPFLAG_test(void);
burnin_cmd flag_cmd_vlaue[] = {
    { 1,"Set/Clean MP flag                ",diag_do_MPFLAG_test, 1}, 
    { 2,"Set/Clean Serial No. & MAC flag  ",diag_do_SMFLAG_test, 1}, 
    { 3,"Set/Clean BR flag                ",diag_do_BRFLAG_test, 1}, 
    { 4,"Set/Clean WARM UP flag           ",diag_do_WARMUPFLAG_test, 1}, 
    { 5,"Clean Calibration flag           ",diag_do_CALFLAG_test, 1}, 
    { 6,"Clean Report flag                ",diag_do_REPFLAG_test, 1}, 
    { 7,"Set/Clean All flag               ",diag_do_ALLFLAG_test, 1}, 	
    { 8,"View now all flag setting        ",diag_do_VIEWFLAG_test, 1}, 	
    { 9,"Set/Clean EOT flag               ",diag_do_EOTFLAG_test, 1},
    { 0,"",0,0}         
};   
  
void diag_do_CALFLAG_test(void)
{
	ulong flag;

#if MODULE_AIO
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
#endif
	GpioWriteData(PIO_EEPROM_PROTECT,0);	// enable calibration EERPOM write
	
	flag = FLAG_CAL;	
	diag_do_set_clean_flag(0,flag);
	i2c_write_to_eeprom(CAL_FLAG1,0);
	diag_do_view_now_flag(flag);
GpioWriteData(PIO_EEPROM_PROTECT,1);	// disable calibration EERPOM write
}

void diag_do_REPFLAG_test(void)
{
	ulong flag;

#if MODULE_AIO
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
#endif

	GpioWriteData(PIO_EEPROM_PROTECT,0);	// enable calibration EERPOM write

	flag = FLAG_REPORT;
	diag_do_set_clean_flag(0,flag);
	i2c_write_to_eeprom(CAL_RFLAG,0);
	diag_do_view_now_flag(flag);
	
	GpioWriteData(PIO_EEPROM_PROTECT,1);	// disable calibration EERPOM write
}


void diag_do_MPFLAG_test(void)
{
	int i;
	ulong flag;

	flag = FLAG_MP;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}

void diag_do_SMFLAG_test(void)
{
	int i;
	ulong flag;

	flag = FLAG_SERIAL_MAC;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}

void diag_do_WARMUPFLAG_test(void)
{
	int i;
	ulong flag;
	
	flag = FLAG_WARMUP;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}

void diag_do_EOTFLAG_test(void)
{
	int i;
	ulong flag;

	flag = FLAG_EOT;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}

void diag_do_BRFLAG_test(void)
{
	int i;
	ulong flag;

	flag = FLAG_BR;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}
/*
void diag_do_HWEXIDFLAG_test(void)
{
	int i;
	ulong flag;

	flag = FLAG_HWEXID;	
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}
*/
void diag_do_ALLFLAG_test(void)
{
	int i;
	ulong flag;
#if MODULE_AIO
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_WARMUP|FLAG_EOT);	
#else
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_POWER_FAIL|FLAG_EOT);	
#endif
	if(diag_get_value("0 - clean , 1 - set",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	diag_do_set_clean_flag(i,flag);
	diag_do_view_now_flag(flag);
}

void diag_do_VIEWFLAG_test(void)
{
	ulong flag;
#if MODULE_AIO
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_WARMUP|FLAG_CAL|FLAG_REPORT|FLAG_EOT);	
#else
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_POWER_FAIL|FLAG_EOT);	
#endif
	diag_do_view_now_flag(flag);
}

int	diag_do_set_clean_flag(int mode,long parameter)
{
	ulong len,base;

	len = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	base = (SDRAM_DOWNLOAD_TEMP_BASE|FLAG_OFFSET);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, len);
	if(mode == 0){
		VPlong(base) &= ~(parameter);
	}else if(mode == 1){
		VPlong(base) |= (parameter);
	}
	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, len) != 0){
		Printf("FAIL !\r\n");
		return DIAG_FAIL;
	}
	return DIAG_OK;
}

void diag_do_view_now_flag(long parameter)
{
	static ulong flag;

#if MODULE_AIO
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
#endif

	mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);
	if((parameter & FLAG_MP) == FLAG_MP){
		Printf("Now MP flag is ...................");
		if(flag & FLAG_MP)
			Printf(" Set.\r\n");
		else 
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_SERIAL_MAC) == FLAG_SERIAL_MAC){
		Printf("Now Serial No. & MAC flag is .....");
		if((flag & FLAG_SERIAL_MAC) == FLAG_SERIAL_MAC)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_EOT) == FLAG_EOT){
		Printf("Now EOT flag is ..................");
		if((flag & FLAG_EOT) == FLAG_EOT)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_BR) == FLAG_BR){
		Printf("Now BR flag is ...................");
		if((flag & FLAG_BR) == FLAG_BR)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_HWEXID) == FLAG_HWEXID){
		Printf("Now HWEXID flag is ...............");
		if((flag & FLAG_HWEXID) == FLAG_HWEXID)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_POWER_FAIL) == FLAG_POWER_FAIL){
		Printf("Now PFCS flag is .................");
		if((flag & FLAG_POWER_FAIL) == FLAG_POWER_FAIL)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_WARMUP) == FLAG_WARMUP){
		Printf("Now Warm Up flag is ..............");
		if((flag & FLAG_WARMUP) == FLAG_WARMUP)
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
	if((parameter & FLAG_CAL) == FLAG_CAL){   
		Printf("Now Calibration flag is ..........");
		if((eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1) ||((flag & FLAG_CAL)==FLAG_CAL))
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}	
	if((parameter & FLAG_REPORT) == FLAG_REPORT){
		Printf("Now Report flag is ...............");
		if((eeprom_readb(RAND_ADDR,CAL_RFLAG) == 1)||((flag & FLAG_REPORT)==FLAG_REPORT))
			Printf(" Set.\r\n");
		else
			Printf(" Clean.\r\n");
	}
}

void diag_do_FLAG_func(void)
{
   burnin_cmd *burnin_temp=flag_cmd_vlaue;
   ManualTesting(burnin_temp);
}   

void diag_do_Change_module_name(void){
	static ulong flag;
	int i;
	ulong ret;
/*
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
*/
//	if(diag_get_value("0 - clean",&i,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	sleep(100);
	diag_do_set_clean_flag(0,FLAG_BR |FLAG_WARMUP|FLAG_CAL|FLAG_REPORT|FLAG_EOT);
//	if(i)
//		return;
	
	/*Set Module ID*/
	ret = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	ret = 64;
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, ret);
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[0].hw_exid;
	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, ret) != 0){
		Printf("FAIL !\r\n");
		return;
	}
	Initboardinfo(0);
	sleep(100);
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	
	GpioWriteData(PIO_EEPROM_PROTECT,0);	// enable calibration EERPOM write
	i2c_write_to_eeprom(CAL_FLAG1,0);
	i2c_write_to_eeprom(CAL_RFLAG,0);
	GpioWriteData(PIO_EEPROM_PROTECT,1);	// disable calibration EERPOM write
}

extern int EE_Address;

void diag_do_BURNIN_func(void)
{
	int port=1;

	Printf("Initializing network ....\r\n");
	/*Init EEPROM setting*/
#if MODULE_AIO
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	EEPROM_address = PARMETER_EEPROM_WRITE;
#else
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
#endif
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EE_Address = 0; //init EEPROM test
	GpioWriteData(PIO_EEPROM_PROTECT,0);

	MemInit();		/* init memory buffer		    */
	ArpInit();

	mac_open(2,MODE_NORMAL); // for link
	PowerMode(2,1);	//port II power down
	sleep(10);
	poweronstatus = BR_STATUS;
	G_LanPort = port;
	Printf("MAC init ..");
	mac_open(G_LanPort,MODE_NORMAL);

	/*For VLAN TUNNEL*/
	Lnet_chip_smi_write(0x19, 0x04, 0x83);	//port I
	Lnet_chip_smi_write(0x1A, 0x04, 0x83);	//port II
	Lnet_chip_smi_write(0x1D, 0x04, 0x83);	//MII port
	Lnet_chip_smi_write(0x19, 0x06, 0x39);
	Lnet_chip_smi_write(0x1A, 0x06, 0x19);
	Lnet_chip_smi_write(0x1D, 0x06, 0x1F);

	Printf("Device IP : (%d.%d.%d.%d)\r\n",
	    NicIp[G_LanPort] & 0xff,
	    (NicIp[G_LanPort] >> 8) & 0xff,
	    (NicIp[G_LanPort] >> 16) & 0xff,
	    (NicIp[G_LanPort] >> 24) & 0xff);
	Printf("Initializing network ok !\r\n");
	Printf(ESCSTRING);
	sleep(10);
	ConUdpInit(0,921600);
	sleep(10);
#if E1240
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
#endif
	tx_old=0,rx_old=0;
	sleep(10);
	for (;;) {
		if (diag_check_press_ESC() == DIAG_ESC)
			break;
	    NicProcess();
		ConUdpBurn();
	    TimerPoll();
	}
	mac_shutdown(G_LanPort);
	/*Disable VLAN TUNNEL*/
	Lnet_chip_smi_write(0x19, 0x04, 0x03);
	Lnet_chip_smi_write(0x1A, 0x04, 0x03);
	/*Disable loopback port*/
	mac_shutdown(2);

	Printf("Wait ping stop\r\n");
	poweronstatus = MM_STATUS;
}

/*
void diag_do_PRODUCTINFO_func(void)
{
	static ulong hwid, hwexid;

	mx_read_data((UINT32)&hwid, SMC_FLASH0_HWID, 4);
	mx_read_data((UINT32)&hwexid, SMC_FLASH0_HWEXID, 4);

	Printf("HWID : [0x%02x] HWEXID = [0x%08x] BURNID = [0x%02x]\r\n",
			hwid, hwexid, sysModelInfo[board_index].burn_id);
	Printf("Module Name : %s\r\n",sysModelInfo[board_index].modelName);
	Printf("Interface : RS232 / RS422 / RS485 \r\n");
	Printf("Find UART : %d \r\n",sysModelInfo[board_index].port);
	Printf("Find MAC : %d \r\n",sysModelInfo[board_index].lan);
}
*/

void diag_do_PRODUCTINFO_func(void)
{
	ulong flag;

#if MODULE_AIO
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_WARMUP|FLAG_CAL|FLAG_REPORT|FLAG_EOT);	
#else
	flag = (FLAG_MP|FLAG_SERIAL_MAC|FLAG_BR|FLAG_POWER_FAIL|FLAG_EOT);	
#endif

	sysModelInfo[board_index].port = GetMoxaUartCnt();
	Printf("\r\n=========================<< PRODUCT  INFORMATION >>=========================\r\n");
	Printf("HWID ............................. [0x%02x]\r\n",sysModelInfo[board_index].hw_id);
//	Printf("HWEXID ........................... [0x%08x]\r\n",sysModelInfo[board_index].hw_exid);
	Printf("BURNID ........................... [0x%02x]\r\n",sysModelInfo[board_index].burn_id);
	Printf("Module Name ...................... %s.\r\n",sysModelInfo[board_index].modelName);
//	Printf("Find UART ........................ %d. \r\n",sysModelInfo[board_index].port);
	Printf("Find LAN ......................... %d. \r\n",sysModelInfo[board_index].lan);
	diag_do_view_now_flag(flag);
	Printf("============================================================================\r\n");
	Printf("Press any key to continue!\r\n");
	Getch();
}

//\\ -----------------------------------Test Functions End----------------------------------------


//------------------------------------------- Library !!!-----------------------------------------------
/**	\brief
 *
 *	Parse the string to get a integer for index.
 *
 *	\param[in]	*Buffer : A string to parse
 *
 *	\retval 		error code
 */
int ParseCmd(char *Buffer)
{
	char seps[]  = " ";
	char *token;
	char Cmd[256];
	UINT32 i;

	token = strtok( Buffer, seps );
	if( token == NULL ){
		return 0;		// no command
	}
	strcpy(Cmd, token);
	if((strcmp(Cmd,"q") == 0) ||(strcmp(Cmd,"Q") == 0)){	// press Q to exit
		return (-1);		// quit command
	}
	i = atoi( Cmd );		// convert string to a integer
	return i;
}

/**	\brief
 *
 *	Print the BIOS information and the test items.
 *
 *	\param[in]	*cmd_list : command items
 *
 */
static void PrintItemMsg(burnin_cmd *cmd_list)
{
	int i=0;
	burnin_cmd *burnin_temp = cmd_list;
	UINT8 gain,cal_flag = 0,vn, vp,reg_read_back,filter_mode,data_format;
	UINT16 decimation_ratio;
	UINT8 dio_type_setting;

	// Show the BIOS information
	Printf("\r\n============================================================================");
	Printf("\r\n  Moxa ioLogik Active Ethernet I/O Server Test Program Version %d.%d.%d",major_ver,minor_ver,test_ver);
	Printf("\r\n  Module Name	  : %s ",sysModelInfo[board_index].modelName);
	Printf("\r\n  Serial No.	  : %d",sysModelInfo[board_index].serial);
	Printf("\r\n  MAC Address	  : %02x:%02x:%02x:%02x:%02x:%02x",MacSrc[0][0],MacSrc[0][1],MacSrc[0][2],MacSrc[0][3],MacSrc[0][4],MacSrc[0][5]);

	if(burnin_temp == burnin_cmd_value_E1200)
	{
		Printf("\r\n  Build date	  : " __DATE__ " - " __TIME__ "");
		Printf("\r\n  Updated by	  : Jerry Wu");
	}
#if E1240 | E1242
	else if (burnin_temp == AI_cmd_value){
		gain = ADC_Register_Readback(2) & 0x07;
		cal_flag = eeprom_readb(RAND_ADDR,CAL_FLAG1);

		Printf("\r\n  AI Channel      : CH-%d",Current_AI_Channel);
		AI_TYPE_CH_Select(Current_AI_Channel);
		sleep(10);
		if(AI_TYPE_READ() == Voltage_mode)
			Printf("\r\n  AI Input type   : Voltage mode");
		else
			Printf("\r\n  AI Input type   : Current mode");
		if (cal_flag == 1)
			Printf("\r\n  Calibration     : Done");
		else
			Printf("\r\n  Calibration     : Pending");
	
#if E1242
		dio_type_setting = DIO_TYPE_READ();
		Printf ("\r\n  DIO Channel     :");
		for (i=0;i<4;i++){
			Printf ("  DIO-%d   ",i);
		}
		Printf ("\r\n  DIO Select Type :");
		for (i=0;i<4;i++){
			if ((dio_type_setting & (0x01<<i)) == (0x01<<i)){
				Printf (" DI mode  ");
			}
			else{
				Printf (" DO mode  ");
			}
		}
#endif
	}
#endif


#if E1262
	else if (burnin_temp == TC_cmd_value){
		gain = ADC_Register_Readback(2) & 0x07;
		cal_flag = eeprom_readb(RAND_ADDR,CAL_FLAG1);

		Printf("\r\n  TC Channel      : CH-%d",Current_AI_Channel);

		sleep(10);
		Printf("\r\n  TC Gain         : X%d", 0x1 << gain );
		if (cal_flag == 1)
			Printf("\r\n  Calibration     : Done");
		else
			Printf("\r\n  Calibration     : Pending");
	}
#endif
#if E1260
	else if (burnin_temp == RTD_cmd_value){
		gain = ADC_Register_Readback(2) & 0x07;
		cal_flag = eeprom_readb(RAND_ADDR,CAL_FLAG1);

		Printf("\r\n  RTD Channel     : CH-%d",Current_AI_Channel);

		sleep(10);
		Printf("\r\n  RTD Gain        : X%d", 0x1 << gain );
		if (cal_flag == 1)
			Printf("\r\n  Calibration     : Done");
		else
			Printf("\r\n  Calibration     : Pending");
	}
#endif
	else if (burnin_temp == ADC_cmd_value){
		reg_read_back = ADC_Register_Readback(1);
		vn = reg_read_back & 0x0f;
		vp = (reg_read_back & 0xf0) >> 4;

		Printf("\r\n  ADC Vin+ Ch.    : CH-%d",vp);
		Printf("\r\n  ADC Vin- Ch.    : CH-%d",vn);
		Printf("\r\n  ADC Int. ref	  : ");
			
		reg_read_back = ADC_Register_Readback(0);
		reg_read_back &= 0x08;
		if (reg_read_back != ADC_EXT_REF){
			Printf("Enabled, ");
			reg_read_back = ADC_Register_Readback(0);
			reg_read_back &= 0x04;
			if (reg_read_back == ADC_INT_REF_1_25V)
				Printf("1.25V");
			else
				Printf("2.5V");
		}
		else
			Printf("Disabled ");
			
		Printf("\r\n  ADC Mod. Factor : ");
		reg_read_back = ADC_Register_Readback(0);
		reg_read_back &= 0x10;
		if (reg_read_back == ADC_M_FACTOR_128)
			Printf("128");
		else
			Printf("256");

		decimation_ratio = ((UINT16)(ADC_Register_Readback(9) & 0x07)) << 8;
		decimation_ratio |= ADC_Register_Readback(8);
		Printf("\r\n  ADC Dec. Ratio  : %d",decimation_ratio);
			
		Printf("\r\n  ADC Dig. Filter : ");
		filter_mode = (ADC_Register_Readback(9) & 0x30) >> 4;
		switch(filter_mode){
			case ADC_FILTER_AUTO:	Printf("Auto");	break;
			case ADC_FILTER_FAST:	Printf("Fast");	break;
			case ADC_FILTER_SINC_2:	Printf("Sinc-2");	break;
			case ADC_FILTER_SINC_3:	Printf("Sinc-3");	break;
			default:break;
		}

		Printf("\r\n  ADC Data Format : ");
		data_format = (ADC_Register_Readback(9) & 0x40) >> 6;
		switch(data_format){
			case 0:	Printf("Bipolar");	break;
			case 1:	Printf("Unipolar");	break;
			default:break;
		}

		gain = ADC_Register_Readback(2) & 0x07;
		Printf("\r\n  ADC PGA  Gain   : X%d",(0x01 << gain));
	}

	if(burnin_temp == burnin_cmd_value_E1200){
		switch(sysModelInfo[board_index].hw_exid){
#if E1241
			case 0x76:
				Printf("\r\n====================<< ioLogik E1241 MM-TEST Main Menu >>===================\r\n");
				break;
				
			case 0x86:
				Printf("\r\n===================<< ioLogik E1241-T MM-TEST Main Menu >>==================\r\n");
				break;
#endif

#if E1242			
			case 0x75:
				Printf("\r\n====================<< ioLogik E1242 MM-TEST Main Menu >>===================\r\n");
				break;
				
			case 0x85:
				Printf("\r\n===================<< ioLogik E1242-T MM-TEST Main Menu >>==================\r\n");
				break;
#endif

#if E1260				
			case 0x77:
				Printf("\r\n====================<< ioLogik E1260 MM-TEST Main Menu >>===================\r\n");
				break;
				
			case 0x87:
				Printf("\r\n===================<< ioLogik E1260-T MM-TEST Main Menu >>==================\r\n");
				break;
#endif

#if E1262
			case 0x78:
				Printf("\r\n====================<< ioLogik E1262 MM-TEST Main Menu >>===================\r\n");
				break;
				
			case 0x88:
				Printf("\r\n===================<< ioLogik E1262-T MM-TEST Main Menu >>==================\r\n");
				break;
#endif			
			default:
				break;
		}
	}
	else
		Printf("\r\n============================================================================\r\n");
	if(burnin_temp == burnin_cmd_value_E1200)
	{
		while(burnin_temp->index)
		{
			if(burnin_temp->exist)
			{
				Printf("(%2d)",burnin_temp->index);
				Printf(" %s",burnin_temp->string);
				i++;
			}
				burnin_temp++;
		}
		if((i%2)!=0)
			Printf("\r\n");
	}
	else
	{	
		while(burnin_temp->index)
		{
			if(burnin_temp->exist)
			{
				Printf("(%2d)",burnin_temp->index);
				Printf(" %s",burnin_temp->string);
				i++;
				if((i%2)==0)
					Printf("\r\n");
			}
			burnin_temp++;
		}
		if((i%2)!=0)
			Printf("\r\n");
	}
}

/**	\brief
 *
 *	Parse the command and call the corresponding function.
 *
 *	\param[in]	*cmd_list : command items
 *
 *	\retval 		command index
 */
static int ManualTesting(burnin_cmd *cmd_list)
{
	UINT32 command_index=0;

	while(1){
		burnin_cmd *burnin_temp = cmd_list;
		PrintItemMsg(burnin_temp);

		PrintStr("----------------------------------------------------------------------------\r\n");
		PrintStr("Command>>");
		ConGetString(ErrMsg,sizeof(ErrMsg));
		command_index = ParseCmd(ErrMsg);

		if(command_index==0){			// NULL command
			continue;
		}else if(command_index == (-1)){	// QUIT command
			PrintStr("\r\n");
			break;
		}        

		while(command_index>=burnin_temp->index){
			if((command_index==burnin_temp->index) && (burnin_temp->exist)){
				(*burnin_temp->burnin_routine)();		// call function
				break;
			}
			burnin_temp++;
		}
		if(command_index == 25) diag_do_DEBUG_func();
	}        
	return command_index;
}

/**	\brief
 *
 *	Get the character that types from user.
 *
 *	\retval 		a character which user key in.
 *
 */
UINT8 diag_check_press_ESC(void)
{
	UINT8 ch;

	if (Kbhit()) {
		ch = Getch();
		if (ch == 27){				// ESC
			return DIAG_ESC;
		}else{
			return ch;			// else return original character
		}
	}
	return DIAG_OK;
}

/**	\brief
 *
 *	Parse the number in string and store the value to "val".
 *
 *	\param[in]	*s : string pointer
 *	\param[in]	*val : store the value
 *	\param[in]	**es : ???
 *	\param[in]	*delim : delimiter
 *
 *	\retval 		error code
 */
int parse_num(char *s, UINT32 *val, char **es, char *delim)
{
	int first = 1;
	int radix = 10;		// 10-base
	char ch;
	unsigned long result = 0;
	int digit;

	while(*s == ' '){
		s++;			// pointer jump to non zero position.
	}	
	
	while (*s) {
		
		if (first && (s[0] == '0') && (_tolower(s[1]) == 'x')) {	// parse if string is a 16-base digit
			radix = 16;
			s += 2;
		}
		
		ch = *s++;
		if (_is_hex(ch) && ((digit = _from_hex(ch)) < radix)) {
			
			result = (result * radix) + digit;		// store the result in variable which user passes in.
			
		} else {
		
			if (delim != (char *)0) {
				// See if this character is one of the delimiters
				char *dp = delim;
				while (*dp && (ch != *dp)){
					dp++;
				}
				if (*dp){
					break;  // Found a good delimiter
				}
			}
			return DIAG_FAIL;  // Malformatted number
			
		}
	}

	*val = result;
	if (es != (char **)0) {
		*es = s;
	}
	return DIAG_OK;	
}

/**	\brief
 *
 *	Sets the specific address into I2C_TAR register.
 *
 *	\param[in]	*title : a string to print out.
 *	\param[in]	*val : value to store into
 *	\param[in]	min : minimum value
 *	\param[in]	max : maximun value
 *	\param[in]	radix_mode : the radix of input string.
 *
 *	\retval 		error code
 */
int diag_get_value(char *title, UINT32 *val, UINT32 min, UINT32 max, int radix_mode)
{
	while ( 1 ) {
		Printf("\r\n%s (", title);
		if ( radix_mode == DIAG_GET_VALUE_HEX_MODE ){
			Printf("0x%lX-0x%lx",  (ulong)min, (ulong)max);
		}else{
			Printf("%lu-%lu", (ulong)min, (ulong)max);		// DIAG_GET_VALUE_DEC_MODE
		}
		
		if ( radix_mode & DIAG_GET_VALUE_NO_ABORT ){
			PrintStr("): ");
		}else{
			PrintStr(",enter for abort): ");
		}
		
		if(ConGetString(ErrMsg,sizeof(ErrMsg)) == -1){
			PrintStr((char*)AbortString); return DIAG_ESC;
		}
		
		if(ErrMsg[0] == 0){
		    return DIAG_FAIL;
		}
		
		if ( (parse_num(ErrMsg, val, 0, 0) || *val < min || *val > max ) == DIAG_FAIL) {
			if((parse_num(ErrMsg, val, 0, 0))){
				PrintStr("*** parse_num() error  ***");
			}else if(*val < min){
				PrintStr("*** value is too small ***");
			}else if(*val > max){
				PrintStr("*** value is too big ***");
			}
			PrintStr("\r\nInvalid number !");
			continue;
		}
		break;		    
	}		        
	PrintStr("\r\n");
	return DIAG_OK;
}

/**	\brief
 *
 *	get the data pattern which switch by user in 32 bits.
 *
 *	\param[in]	pattern_index : pattern index 
 *
 *	\retval 		data pattern of user selected
 */
UINT32 diag_get_data_pattern(int pattern_index)
{
	UINT32 Content = 0;

	switch(pattern_index){
		case (1):  Content = DATA_PATTERN1; break;
		case (2):  Content = DATA_PATTERN2; break;
		case (3):  Content = DATA_PATTERN3; break;
		case (4):  Content = DATA_PATTERN4; break;
		case (5):  Content = DATA_PATTERN5; break;
		case (6):  Content = DATA_PATTERN6; break;
		case (7):  Content = DATA_PATTERN7; break;
		
		default :  Content = DATA_PATTERN6; break;			// 0xFFFFFFFF
	}
	return Content;
}

/**	\brief
 *
 *	
 *
 *	\param[in]	packet_length : packet length. 0 means that input by user.
 *
 *	\retval 		error code
 */
static int PacketInit(int packet_length)
{
	int i,loop;
	int length;		// packet length
//	burnin_cmd *burnin_temp=Datapattern_vlaue;
	burnin_cmd *burnin_temp=Datapattern_value1;
	UINT32 Content; 		// data pattern
    UINT8 *ptr;  

	Memset(&PacketInfo,0,sizeof(Packet_Info));		// clear packet
	if( poweronstatus == MM_STATUS ){
		PrintItemMsg(burnin_temp);				// datapattern
 	   	Printf("---------------------------------------------------------------------------\r\n");
		if(diag_get_value("Command>>",&i,1,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
			return DIAG_ESC;
		}
	}else{
		i = 5;	// do 0x55AA00FF
	}

	switch(i){
		case (1):	Content = 0x55555555;	loop = 1000; break;
		case (2):	Content = 0x55555555;	loop = 10000; break; 
		case (3):	Content = 0xAAAAAAAA;	loop = 1000; break;
		case (4):	Content = 0xAAAAAAAA;	loop = 10000; break;
		case (5):	Content = 0x55AA00FF;	loop = 1000; break;
		case (6):	Content = 0x55AA00FF;	loop = 10000; break;
		default :	Content = DATA_PATTERN6; loop = 1000;	break;			// 0xFFFFFFFF
	}
		
	if( poweronstatus == MM_STATUS ){
		length = packet_length;
		if (packet_length == 0) {		// 0 means that input by user
			// max length = max Ethernet length (1518) - MAC append CRC (4)		
			if(diag_get_value("Please key in Packet length",&length,(UINT32)64,(UINT32)1514,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){
				return DIAG_ESC;
			}
		}
	}else{
		length = 128;		// default value = 128
	}
	
	PacketInfo.length = length;

	/* copy data to Packet */
	ptr = (UINT8*) &Content;
	for(i=0;i<PacketInfo.length;i+=4){
		PacketInfo.wbuf[i+0] = ptr[0];            
		PacketInfo.wbuf[i+1] = ptr[1];                    
		PacketInfo.wbuf[i+2] = ptr[2];
		PacketInfo.wbuf[i+3] = ptr[3];
	}
	return loop;
}




#define d	0x00000001
#define e8	0x00000002
#define e16 	0x00000004
#define e32 	0x00000008
/**	\brief
 *
 *	Dump the value of specific address.
 *
 *	\param[in]	*Buffer : a temp buffer for process the data.
 *
 *	\retval 		error code
 */
int DebugCmd(char *Buffer)
{
	char seps[]  = " ";
	char *token;
	int i;
	char Temp[256];
	UINT32 addr,data,cmd;
	
	cmd = addr = data = 0x0;
	i = 0;
	token = strtok( Buffer, seps );
	while( token != NULL ){
		strcpy(Temp, token);
		if(i==0){
			if(strcmp(Temp,"d") == 0){ cmd = d;}
			else if(strcmp(Temp,"e8") == 0){ cmd = e8;}
			else if(strcmp(Temp,"e16") == 0){ cmd = e16;}
			else if(strcmp(Temp,"e32") == 0){ cmd = e32;}
		}else if(i==1){
			if ( (parse_num(token, &addr, 0, 0) || addr < 0 || addr > 0xffffffff ) == DIAG_FAIL) {
				Printf("\r\nInvalid number !");
				return DIAG_FAIL;
			}
		}else if(i==2){
			if ( (parse_num(token, &data, 0, 0) || data < 0 || data > 0xffffffff ) == DIAG_FAIL) {
				Printf("\r\nInvalid number !");
				return DIAG_FAIL;
			}
		}
		i++;
		token = strtok( NULL, seps );
	}
	if(i > 3) return DIAG_FAIL;
	if((cmd == d) && (data == 0))
		data = 64;
	switch(cmd){
	case d:
		for(i=0;i<data;i++){
			if(i%16 ==0) Printf("[0x%08x]   ",addr);
			Printf("%02x ",VPchar(addr));
			addr+=1;
			if(i%16 == 15) Printf("\r\n");
		}
		break;
	case e8:
		VPchar(addr) = (char)data;
		for(i=0;i<64;i++){
			if(i%16 ==0) Printf("[0x%08x]   ",addr);
			Printf("%02x ",VPchar(addr));
			addr+=1;
			if(i%16 == 15) Printf("\r\n");
		}
		break;
	case e16:
		VPshort(addr) = (short)data;
		for(i=0;i<64;i++){
			if(i%16 ==0) Printf("[0x%08x]   ",addr);
			Printf("%02x ",VPchar(addr));
			addr+=1;
			if(i%16 == 15) Printf("\r\n");
		}
		break;
	case e32:
		VPlong(addr) = (long)data;
		for(i=0;i<64;i++){
			if(i%16 ==0) Printf("[0x%08x]   ",addr);
			Printf("%02x ",VPchar(addr));
			addr+=1;
			if(i%16 == 15) Printf("\r\n");
		}
		break;
	default: break;
	}
	return DIAG_OK;
}

void diag_do_DEBUG_func(void)
{
	for(;;){
		Printf("\r\n");
		Printf("\r\n");
		Printf("=======================<< ioLogik E1K Debug Utility >>======================\r\n");
		Printf("d   : Dump memory Ex:[d start_address length]    \r\n");
		Printf("e8  : Edit memory byte Ex:[e8 address data]\r\n");
		Printf("e16 : Edit memory half-word Ex:[e16 address data]\r\n");
		Printf("e32 : Edit memory word Ex:[e32 address data]\r\n");
		Printf("=============================================================================\r\n");
		Printf("Command>>");
		if(ConGetString(ErrMsg,sizeof(ErrMsg)) == DIAG_FAIL)
			break;
		Printf("\r\n");
		DebugCmd(ErrMsg);
	}
}

/**	\brief
 *
 *	Get the board information that had programmed in flash already.
 *
 *	\param[in]	i : MAC port
 *
 */
void Initboardinfo(int i)
{
	ulong hw_exid;

	hw_exid = GetNetModuleID();
	for(i=0;i<MOXA_ioLogik_E1200_MODEL_MAX_NUM;i++){
		if(hw_exid == sysModelInfo[i].hw_exid){
			break;
		}
	}
	board_index = i;
	mx_read_data((UINT32)&(sysModelInfo[board_index].serial), SMC_FLASH0_SERIAL, 4);
	mx_read_data((UINT32)MacSrc[0], SMC_FLASH0_MACA, 6);
//	mx_read_data((UINT32)MacSrc[1], SMC_FLASH0_MACB, 6);
	if (MAC_MAX_AMOUNT > 2){
		int i;
		for (i = 1; i < MAC_MAX_AMOUNT; i++){
			memcpy(MacSrc[i], MacSrc[0], 6);		// get MacSrc From Flash
		}
	}
	mx_read_data((UINT32)&localip, SMC_FLAGH0_LOCALIP, 4);
	mx_read_data((UINT32)&serverip , SMC_FLAGH0_SERVERIP, 4);
}

/**	\brief
 *
 *	Twinkle the LED.
 *	By input the LED number , we can define some debugging message to help our development.
 *
 *	\param[in]	led : LED number
 *
 */
void HaltLED(void)
{
	int i=0;
	ulong t =0;

	t = Gsys_msec;
	while(1){
		if((Gsys_msec - t) > 500){
			i = !i;
			t = Gsys_msec;
		}
		if(i == 0){
			mptest_DO(LED_RED|BUZZER);
		}else{
			mptest_DO(BUZZER);
		}
	}
}

UINT8 ShowMPLED(UINT16 num){
	return mptest_led(num);
}

/**	\brief
 *
 *	Diagnostic the board by default parameters. (Automatically)
 *
 *	\param[in]	port : MAC port
 *
 *	\retval 		error code
 */
UINT8 mptester_exist = 0;
void diag_do_MPTEST_func(void)
{
	int ErrFlag = 0;
	int i=0,ret;
	ulong t;	UINT32 cmd_set_reg;


	poweronstatus = MP_STATUS;
	ReadyLed(0);

	/*100M Ethernet test ShowLED 1*/
	mptester_communication_init();

	mptester_exist = ShowMPLED(1);
	
	ErrFlag = 0;
	Printf("MAC 100MHZ External testing ...\r\n");
	for(i=1; i <= 2; i++){
		if( (ret = diag_do_ETH_test(0, i, MODE_LAN100M|MODE_FDUP)) != DIAG_OK ){
			Printf("MAC%d ", i);
			switch(ret){
				case (-1) :	Printf(" ...FAIL [length]\r\n"); break;
				case (-2) :	Printf(" ...FAIL [Dest Addr.]\r\n"); break;
				case (-3) :	Printf(" ...FAIL [Source Addr.]\r\n"); break;
				case (-4) :	Printf(" ...FAIL [Data]\r\n"); break;
				case (-5) : Printf(" ...FAIL [Loss]\r\n"); break;
				default : Printf("...Unknow !\r\n"); break;
			}
			ErrFlag++;
		}
	}
	if( ErrFlag ){
		Printf("MAC 100MHz External testing ** FAIL ** \r\n");
		HaltLED();
	}else{
		Printf("MAC 100MHZ External testing ** OK ** \r\n");
	}

	/*10M Ethernet test ShowLED 2*/
	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(2);
	ErrFlag = 0;
	Printf("MAC 10MHZ External testing ...\r\n");
	for(i=1; i <= 2; i++){
		if( (ret = diag_do_ETH_test(0, i, MODE_LAN10M|MODE_FDUP)) != DIAG_OK ){
			Printf("MAC%d ", i);
			switch(ret){
				case (-1) :	Printf(" ...FAIL [length]\r\n"); break;
				case (-2) :	Printf(" ...FAIL [Dest Addr.]\r\n"); break;
				case (-3) :	Printf(" ...FAIL [Source Addr.]\r\n"); break;
				case (-4) :	Printf(" ...FAIL [Data]\r\n"); break;
				case (-5) : Printf(" ...FAIL [Loss]\r\n"); break;
				default : Printf("...Unknow !\r\n"); break;
			}
			ErrFlag++;
		}
	}
	if( ErrFlag ){
		Printf("MAC 10MHz External testing ** FAIL ** \r\n");
		HaltLED();
	}else{
		Printf("MAC 10MHZ External testing ** OK ** \r\n");
	}

	/*EEPROM test ShowLED 3*/
	Printf("EEPROM testing ...\r\n");
	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(3);
	GpioWriteData(PIO_EEPROM_PROTECT,0);
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	ErrFlag = 0;
#if MODULE_AIO
	EEPROM_address = EEPROM_WRITE;
#endif
	Printf("Configuration EEPROM testing ...");
	if(diag_EEPROM_MP_page(0,128) == -1){
		Printf("FAIL!!\r\n");
		ErrFlag = 1;
	}
	if(diag_EEPROM_MP_page(EEPROM_MAX_SIZE-256,128) == -1){
		Printf("FAIL!!\r\n");
		ErrFlag = 1;
	}
	else
		Printf("OK!!\r\n");
#if MODULE_AIO
	i2c_disable();
	i2c_set_target_address(PARMETER_EEPROM_WRITE);
	i2c_enable();
	EEPROM_address = PARMETER_EEPROM_WRITE;

	Printf("Parameter EEPROM testing ...");
	
	for(i=0;i<Cal_EEPROM_MAX_SIZE;i+=Cal_EEPROM_PAGE_SIZE){
		if(diag_EEPROM_MP_page(i,Cal_EEPROM_PAGE_SIZE) == -1){
			Printf("FAIL!!\r\n");
			break;
			ErrFlag = 2;
		}
	}
	if(ErrFlag !=2) Printf("OK!!\r\n");
	diag_Set_EEPROM_ID();
	i2c_disable();
	i2c_set_target_address(EEPROM_WRITE);
	i2c_enable();
	EEPROM_address = EEPROM_WRITE;

#endif
	if( ErrFlag ){
		Printf("EEPROM writing testing ** FAIL ** \r\n");
		HaltLED();
	}else{
		Printf("EEPROM writing testing ** OK ** \r\n");
	}
	
	ErrFlag = diag_EEPROM_protect_test(0);
	if( ErrFlag ){
		Printf("EEPROM protecting testing ** FAIL ** \r\n");
		HaltLED();
	}else{
		Printf("EEPROM protecting testing ** OK ** \r\n");
	}	

	/*Flash protected test ShowLED 4*/
	Printf("Flash protected testing ...\r\n");
	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(4);
	ErrFlag = diag_do_SPI_Flash_protect();
	if(ErrFlag){
		Printf("Flash protected Test *** FAIL ***\r\n");
		HaltLED();
	}else{
		Printf("Flash protected Test *** OK ***\r\n");
	}

	/*SDRAM test ShowLED 5*/
	Printf("SDRAM testing ...\r\n");
	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(5);

	if( sdram_word_mptest(0, 6) != 0) ErrFlag++;
	if(ErrFlag){
		Printf("SDRAM Test *** FAIL ***\r\n");
		HaltLED();
	}else{
		Printf("SDRAM Test *** OK ***\r\n");
	}

	/*I/O test ShowLED 6*/
	Printf("I/O testing ...\r\n");
	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(6);
#if E1240
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
	if (diag_chip_SPI()==-1) 
	{
		HaltLED();
	}
	
	ErrFlag = diag_chip_ADC();

	if (ErrFlag==-1) 
	{
		HaltLED();
	}
	if (ErrFlag==-2) 
	{
		HaltLED();
	}

	if(ErrFlag==-3){
		HaltLED();
	}

	if (diag_chip_Mux()==-1) 
	{
		HaltLED();
	}
/*
	if (diag_chip_ADC_PGA()==-1) 
	{
		HaltLED();
	}
*/
	if(mptest_GPIO_output(0x0000) == mp_time_out){
		Printf("Press stir the channel 1~4 OFF\r\n");
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
#endif

#if E1260
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
	if (diag_chip_SPI()==-1) 
	{
		HaltLED();
	}
	
	ErrFlag = diag_chip_ADC();

	if (ErrFlag==-1) 
	{
		HaltLED();
	}
	if (ErrFlag==-2) 
	{
		HaltLED();
	}

	if(ErrFlag==-3){
		HaltLED();
	}

	if (diag_chip_Mux()==-1) 
	{
		HaltLED();
	}
/*
	if (diag_chip_ADC_PGA()==-1) 
	{
		HaltLED();
	}
*/
	
/*	
	if(mptest_GPIO_output(0x000F) == mp_time_out){
		Printf("Press stir the channel 1~4 OFF\r\n");
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
*/
#endif

	/*Jumper test ShowLED 7*/
#if E1240
	Printf("Jumper testing ...\r\n");
	cmd_set_reg = 0x520000 | (UINT32)(ADC_Register_Readback(2) | 0);	// gain setting
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	if(mptester_exist != mp_time_out)
		mptester_exist = ShowMPLED(7);

	if(mptester_exist == mp_time_out){

		Printf("Press set ALL AI input states are voltage mode\r\n");

		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
	else	mptest_relay(0xff);

	ErrFlag = diag_do_jp2mp_test();
	if(ErrFlag != 0){
		Printf("Error\r\n");
		HaltLED();
	}
	if(mptester_exist == mp_time_out){
#if E1240
		Printf("Press set ALL AI input states are current mode\r\n");
#endif
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
	else	mptest_relay(0x00);
	ErrFlag = diag_do_jp2mp_test();
	if(ErrFlag != 0xff){
		Printf("Error\r\n");
		HaltLED();
	}
	if(mptester_exist != mp_time_out)
		mptest_relay(0xff);
	Printf("2*3 Jumpers test ** OK ** \r\n");
#endif
	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();

	/*SW push button test ShowLED 8*/
	if(mptester_exist != mp_time_out)
		ShowMPLED(8);
	Printf("Please [Press] SW Push Button...\r\n"); 
	t = Gsys_msec;
	i=0;
	do{
		if(GetJpStatus(PIO_SW_BUTTON))
			break;
		if((Gsys_msec - t) > 500){
			if(i){	
				i=0; 
				if(mptester_exist != mp_time_out)
					mptest_DO(LED_GREEN|BUZZER);
			}
			else{
				i=1; 
				if(mptester_exist != mp_time_out)
					mptest_DO(BUZZER);
			}
			ReadyLed(i);
			t = Gsys_msec; 
		}
	}while(1);
//	mptest_DO(0x0);
	mptest_DO(BUZZER);
	Printf("SW Push Button Test OK!! \r\n");

	poweronstatus = MP_STATUS;

	/*Set Module ID*/
	ret = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
	mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, ret);
	*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[0].hw_exid;

	if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, ret) != 0){
		Printf("FAIL !\r\n");
		return;
	}
	Initboardinfo(0);

	Printf("Set MP-Flag...  ");
	if( diag_do_set_clean_flag(1,FLAG_MP) == DIAG_OK){		// Set MP flag
		Printf("MP-Test Complete...O.K.\r\n");
		ReadyLed(ON);
	}else{
		Printf("MP-TEST FAIL !  ");
		HaltLED();
	}
	ShowMPLED(0);
}

//#define SWITCH_CLOCK
//-------------------------------- Library end-----------------------------------------------
/**	\brief
 *
 *	This is the BIOS entry point. It will do some initailizations and print out the command items for user.
 *	bios_main() is called by start.S
 *
 */
//#define EMS_TEST
//#define Burn_in_TEST

extern void	proc_232(int method);
void bios_main(void)
{
	burnin_cmd *burnin_temp=burnin_cmd_value_E1200;		// command items
	static ulong flag;
	ulong tmp_flag;
	int ret;


#ifdef SWITCH_CLOCK
	int i = 0;
	UINT32 reg = 0;
	Outw(S2E_CLOCK_CONTROL_BASE , 0x33);
	Outw(S2E_CLOCK_CONTROL_BASE , 0x03);
	Outw(S2E_CLOCK_CONTROL_BASE , 0x0F);

	reg = Inw(S2E_CLOCK_CONTROL_BASE);

#endif
    	
	isr_init_isr();		// disable all interrupts ; Init ISR table
	enable_interrupts();	// enable all interrupts
	
	ConInit(ConsolePort , ConsoleBaudrate);		// init Console ; init m_sio_init()

	timer_init();			// init timer
	spi_init();			// init SPI
	MemInit();			// init Memory management

	GpioInit();			// init GPIO
	Initboardinfo(0);		// init board information by getting data that had programmed already from SPI Flash
	
		// Check EOT flag for HWID...Start			
	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();

	mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);	// Modify bu Jerry Wu[12/23,10'] for E1242-T
   
	if((flag & FLAG_EOT) == FLAG_EOT){
		sysModelInfo[board_index].port = GetMoxaUartCnt();
		if((sysModelInfo[board_index].hw_id)!= 0x87){
//	if (flag == 0x4F){
			Printf("Write HW ID for ...... E1260-T\r\n");
			sleep(10);
			ret = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
			sleep(10);
//			ret = 64;
			mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, ret);
			sleep(10);
			*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[1].hw_exid;
			sleep(10);
			if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, ret) != 0){
				Printf("FAIL !\r\n");
				return;
			}
			Initboardinfo(0);
			sleep(10);
		}
	}
	else{
		if((sysModelInfo[board_index].hw_id)!= 0x77){
			Printf("Write HW ID for ...... E1260\r\n");
			sleep(10);
			ret = GetSectorSize(sysModelInfo[board_index].smcflash0,0);
//			ret = 64;
			sleep(10);
			mx_read_data(SDRAM_DOWNLOAD_TEMP_BASE, MANUFACTURE_CONFIG_FLASH_BASE, ret);
			sleep(10);
			*(ulong *)(SDRAM_DOWNLOAD_TEMP_BASE|HWEXID_OFFSET) = sysModelInfo[0].hw_exid;
			sleep(10);

			if(FlashromProgram(MANUFACTURE_CONFIG_FLASH_BASE, SDRAM_DOWNLOAD_TEMP_BASE, ret) != 0){
				Printf("FAIL !\r\n");
				return;
			}
			Initboardinfo(0);
			sleep(10);
		}
	}
	// Check EOT flag for HWID...end
	
	if (GetJpStatus(PIO_JP1) == 1){ 	//(GetJpStatus(PIO_JP1) == 1)
		UINT32 version;
	
		major_ver = minor_ver = test_ver = 0;
	
		mx_read_data((UINT32)&version, SMC_FLASH0_VERIOSN, 4);
		major_ver = (version >> 24) & 0xff; 
		minor_ver = (version >> 16) & 0xff;
		test_ver = (version >> 8) & 0xff;
		while(1){
			ManualTesting(burnin_temp);
		}
	}else{
#ifdef	EMS_TEST
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		while(1){
			diag_do_AI_EMS_scan_test();
		}
#endif

#ifdef	Burn_in_TEST
		diag_do_BURNIN_func();
#endif
		// get status
		// 1. check ID --> ID Gen
		// 2. check MP flag --> MP Test
		// 3. check serial no & MAC addr --> MAC gen
		// 4. check burnin flag --> burnin
		// 5. load firmware
		do{
			/* Read manufacture flag from SPI flash */
		    mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);		// read the flag to decide to do something
		    	
			if( (flag & FLAG_MP) == 0 ){
				// do MP Test
				diag_do_MPTEST_func();	// auto testing
			}else if( (flag & FLAG_SERIAL_MAC) == 0 ){
				// do MAC Gen
				proc_232(1);
			}else if( (flag & FLAG_BR) == 0 ){
				// do burnin
				diag_do_BURNIN_func();
			}else if( (flag & FLAG_WARMUP) == 0 ){
				// do warm up
				diag_do_AI_WARM_UP();
			}else if( (flag & FLAG_CAL) == 0 ){
				// do calibrate
				diag_do_AI_MP_calibration();
			}
			tmp_flag = flag & (FLAG_MP | FLAG_SERIAL_MAC | FLAG_BR | FLAG_WARMUP | FLAG_CAL|FLAG_EOT);
		}while( tmp_flag != (FLAG_MP | FLAG_SERIAL_MAC | FLAG_BR | FLAG_WARMUP | FLAG_CAL|FLAG_EOT) );
//			tmp_flag = flag & (FLAG_MP | FLAG_SERIAL_MAC | FLAG_BR);
//		}while( tmp_flag != (FLAG_MP | FLAG_SERIAL_MAC | FLAG_BR) );

		// load firmware
		diag_do_JUMPTOGO_func();
	}
}
