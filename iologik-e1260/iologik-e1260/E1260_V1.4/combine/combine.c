/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    combine.c

    Combine armboot.bin and testprg.bin to form s2e.bos

    2008-06-10	Albert Yu
		first release
		2009-08-03	Nolan Yu
		modified
*/

/*
   The content of s2e.bos is :

   0x00000 ~ 0x00FFF (4K)   : armboot.bin (bootloader)
   0x01000 ~ 0x01FFF (4K)   : all 0x00 (Manufacture/Test config data area)
   0x02000 ~ 0x3FFFF (248K) : Manufacture/Test program (header + code, see following)

*/
/*
 * The format of Manufacture/Test program file is :
 *
 * ---------   +---------------+---------------+---------------+---------------+
 * Header      |     magic     |     version   |    length     | program start |
 *   32 bytes  +---------------+---------------+---------------+---------------+
 *	       	   |    checksum   |          reserved = 0(12 bytes)		       |
 * ---------   +---------------+---------------+-------------------------------+
 * CODE + DATA |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 * ---------   +---------------------------------------------------------------+
 *
 * magic   : 4 bytes
 * version : 4 bytes
 * length  : 4 bytes, length of program (CODE + DATA)
 * prg star: 4 bytes, program start address
 * checksum: 4 bytes, the check sum of CODE + DATA
 * reserve : 12 bytes
 */

#include    <stdio.h>
#include	<string.h>
#include    <io.h>
#include    <dos.h>
#include    <fcntl.h>
#include    <memory.h>

#define		E1240	0
#define		E1262	0
#define		E1242	0
#define		E1260	1
#define		E1241	0

#define     MAGIC404            0x33336464	    /* d3 */
#define		PACK_NO				1
#define		PRG_TYPE			1

#define		BOOT_BASE           0x0000L
#define		BOOT_CODE_SIZE      4*1024
#define     TESTCFG_BASE        0x1000L
#define		TSETPRG_HEADER_BASE 0x2000L
#define		TSETPRG_CODE_BASE   0x2020L

typedef unsigned char	u_char;
typedef unsigned short	u_short;
typedef unsigned long	u_long;


// E1260 V1.3: Add EOT flag for E1260-T
// E1260 V1.4: Fix change HWID for E1260-T

#if	E1240
u_long     VERSION             =0x01020000;	    /* 00 */
#endif
#if	E1262
u_long     VERSION             =0x01000000;	    /* 00 */
#endif
#if	E1260
u_long     VERSION             =0x01040000;	    /* 00 */
#endif
#if	E1242
u_long     VERSION             =0x01000000;	    /* 00 */
#endif
#if	E1241
u_long     VERSION             =0x01000000;	    /* 00 */
#endif

typedef struct head_isd {
	u_long magic;
	u_long version;
	u_long package_number;
	u_long prg_type;
	u_long length;
	u_short checksum;
	u_char reserve[10];
}	HEAD_ISD;

/*
 * The format of Manufacture/Test program file is :
 *
 * ---------   +---------------+---------------+---------------+---------------+
 * Header      |     magic     |     version   |    Package    |  Program type |
 *   32 bytes  +---------------+---------+-----+---------------+---------------+
 *	       	   |    Length     | checksum|     reserved = 0(10 bytes)	       |
 * ---------   +---------------+---------+-----+-------------------------------+
 * CODE + DATA |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 *	           |							                                   |
 * ---------   +---------------------------------------------------------------+
 *
 * magic   : 4 bytes
 * version : 4 bytes
 * length  : 4 bytes, length of program (CODE + DATA)
 * Package : 4 bytes
 * Program type : 4 bytes 
 * checksum: 2 bytes, the check sum of CODE + DATA
 * reserve : 12 bytes
 */
/*typedef struct head_isd {
	u_long magic;
	u_long version;
	u_long length;
	u_long prg_start;
	u_long sum;
	u_char reserve[12];
}	HEAD_ISD;
*/
HEAD_ISD	header;

u_long  totalLen, testPrgLen, bootLen, sum;

void show_result(void);

int	main(int argc,char *argv[])
{
	int	    i,rlen,wlen,slen;
	FILE	*mdfp,*mdfp1,*mdfp2;
	char	*bootname="../boot/armboot.bin";
	char	*testprgname="../TestPrg/testprg.bin";
#if	E1240
	char	*Biosname="../binary/E1240.bos";
#endif
#if	E1242
	char	*Biosname="../binary/E1242.bos";
#endif
#if	E1260
	char	*Biosname="../binary/E1260.bos";
#endif
#if	E1262
	char	*Biosname="../binary/E1262.bos";
#endif
#if	E1241
	char	*Biosname="../binary/E1241.bos";
#endif
	char 	buf[256];
	u_long	j=0;

    if((mdfp1=fopen(bootname,"r+b")) == NULL){
    	printf("open file %s error\n",bootname);
		return(-1);
	}
    if((mdfp2=fopen(testprgname,"r+b")) == NULL){
    	printf("open file %s error\n",testprgname);
		return(-4);
	}
    if((mdfp=fopen(Biosname,"w+b")) == NULL){	
    	printf("open file %s error\n",Biosname);
		return(-5);
	}    	
	if(fseek(mdfp,0,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-6);
	}

	/* Write bootloader to file */
	if(fseek(mdfp1,0L,SEEK_SET)){
    	printf("open file %s error\n",bootname);
		return(-7);
	}
	totalLen = 0;
	bootLen = 0;
	for(;;){	
		rlen = fread(buf, sizeof( char ), 256, mdfp1 );		
		if(rlen < 0){
			printf("read file %s error\n",bootname);
			return(-8);
		}
		if(!rlen) break;
		bootLen += rlen;
		wlen = fwrite( buf, sizeof( char ), rlen, mdfp);
		if (wlen != rlen){
			printf("write file %s error\n",Biosname);
			return(-9);
		}			
	}
	totalLen += bootLen;
	if(totalLen > BOOT_CODE_SIZE){ printf("Boot file len error\r\n"); return(-10);}

	/* Clear Manufacture/Test config data area */
	memset(buf,0,256);
	for(;;){
		if(totalLen > TSETPRG_HEADER_BASE)
			break;
		else{
			fwrite( buf, sizeof( char ), 256, mdfp);
			totalLen += 256;
		}
	}

#if 1
	/* Fill Manufacture/Test config data area */
	{
	char mac1[6] = {0x00, 0x90, 0xe8, 0x66, 0x32, 0x17};
	char mac2[6] = {0x00, 0x90, 0xe8, 0x66, 0x32, 0x18};
	char ip1[4] = {192, 168, 32, 39};
	char ip2[4] = {192, 168, 32, 35};
	unsigned long hw_exid = 0x20;

	if(fseek(mdfp,TESTCFG_BASE+0x08,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-17);
	}
	fwrite(&hw_exid, sizeof( char ), 4, mdfp);

	if(fseek(mdfp,TESTCFG_BASE+0x10,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-17);
	}
	fwrite(mac1, sizeof( char ), 6, mdfp);
	fwrite(mac2, sizeof( char ), 6, mdfp);
	if(fseek(mdfp,TESTCFG_BASE+0x20,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-17);
	}
	fwrite(ip1, sizeof( char ), 4, mdfp);
	fwrite(ip2, sizeof( char ), 4, mdfp);
	}
#endif

	totalLen = TSETPRG_CODE_BASE;

	/* write Manufacture/Test program code: testprg.bin */
	if(fseek(mdfp,TSETPRG_CODE_BASE,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-17);
	}
	testPrgLen = 0;
	sum = 0;
	for(;;){
		rlen = fread(buf, sizeof( char ), 256, mdfp2);		
		if(rlen < 0){
			printf("read file %s error\n",testprgname);
			return(-18);
		}
		if(!rlen)
		    break;
		testPrgLen += rlen;

		slen = rlen&(~(int)0x3);	/* align to word boundary */
		for(i=0;i<slen;i+=4)
  			sum += *(unsigned long*)&(buf[i]);

		wlen = fwrite( buf, sizeof( char ), rlen, mdfp);
		if (wlen != rlen){
			printf("write file %s error\n",Biosname);
			return(-19);
		}			
	}

    totalLen += testPrgLen;

    /* write Manufacture/Test program header */
	if(fseek(mdfp,TSETPRG_HEADER_BASE,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-23);
	}
	memset(&header, 0x00, sizeof(header));
	header.magic = MAGIC404;
	header.version = VERSION;
	header.length = testPrgLen;
	header.package_number = PACK_NO;
	header.prg_type = PRG_TYPE;
	header.checksum = (u_short)sum;
	memcpy(buf,&header,32);
	if(fwrite(buf, sizeof( char ), 32, mdfp) != 32){
    	printf("Write file %s error\n",Biosname);
		return(-24);
	}

#if 0 /* Add firmware */
{
	FILE *firmFP;
	//char *firmName="../firmware/s2e_firmware.rom";
	char *firmName="e:/np_2g/npia-5250/firmware/np51x0.rom";
	u_long firmLen;

    if((firmFP=fopen(firmName,"r+b")) == NULL){	
    	printf("open file %s error\n",firmName);
		return(-5);
	}

	if(fseek(mdfp,totalLen,SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-23);
	}

	/* Clear Firmware config data area */
#define FIRMWARE_START_ADDR		0x050000
	memset(buf,0,256);
	for(;;){
		if(totalLen > FIRMWARE_START_ADDR)
			break;
		else{
			fwrite( buf, sizeof( char ), 256, mdfp);
			totalLen += 256;
		}
	}

	totalLen = FIRMWARE_START_ADDR;

	/* write Manufacture/Test program code: testprg.bin */
	if(fseek(mdfp, FIRMWARE_START_ADDR, SEEK_SET)){
    	printf("open file %s error\n",Biosname);
		return(-17);
	}
	firmLen = 0;
	for(;;){
		rlen = fread(buf, sizeof( char ), 256, firmFP);		
		if(rlen < 0){
			printf("read file %s error\n",firmName);
			return(-18);
		}
		if(!rlen)
		    break;
		firmLen += rlen;

		wlen = fwrite( buf, sizeof( char ), rlen, mdfp);
		if (wlen != rlen){
			printf("write file %s error\n",Biosname);
			return(-19);
		}			
	}

    totalLen += firmLen;
}
#endif

	fclose(mdfp);
	fclose(mdfp1);
	fclose(mdfp2);
	show_result();
	return(0);
}

void show_result(void)
{
	printf("-------------------------------------------\n");
	printf("INPUT  armboot.bin: base=0x%06x, len=%ld\r\n", BOOT_BASE, bootLen);
	printf("INPUT  testprg.bin: base=0x%06x, len=%ld (0x%lX), sum=0x%08lX\r\n", TSETPRG_CODE_BASE, testPrgLen, testPrgLen, sum);
#if	E1240
	printf("OUTPUT E1240.bos  : len=%d\r\n", totalLen);
#endif
#if	E1242
	printf("OUTPUT E1242.bos  : len=%d\r\n", totalLen);
#endif
#if	E1260
	printf("OUTPUT E1260.bos  : len=%d\r\n", totalLen);
#endif
#if	E1262
	printf("OUTPUT E1262.bos  : len=%d\r\n", totalLen);
#endif
#if	E1241
	printf("OUTPUT E1241.bos  : len=%d\r\n", totalLen);
#endif
	printf("-------------------------------------------\n");
}