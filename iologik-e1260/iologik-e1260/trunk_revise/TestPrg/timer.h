/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	timer.h

	Function declarations
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified		
		
*/

#ifndef _TIMER_H
#define _TIMER_H


/*------------------------------------------------------ Macro / Define ----------------------------*/
// S2E is down-counted timer , so that MOXA Art Overflow Enable = S2E INT UNMASK
// S2E Control Register
#define TIMER1_DISABLE						(0 << 0)			// default value
#define TIMER1_ENABLE						(1 << 0)		
#define TIMER1_FREERUN_MODE				(0 << 1)			// default value (set LOAD to all 1 first )
#define TIMER1_USER_DEFINE_MODE			(1 << 1)
#define TIMER1_INT_UNMASK					(0 << 2)			// default value
#define TIMER1_INT_MASK					(1 << 2)


#define DSYS_MAXTWAIT		12




/*------------------------------------------------------ Structure ----------------------------------*/
typedef struct taskwait {		// for timeout , timepoll
	struct taskwait *	next;
	int	(*func)(void);
	ulong	time;
}taskw , *taskw_t;









/*------------------------------------------------------ Extern / Function Declaration ----------------*/
extern unsigned long SysTimerTicks ;	/* Time Unit: hundredths of a second */
extern unsigned long Gsys_msec ;

void	timer_init(void);
void timer_enable(void);
void timer_disable(void);
void timer_set_reload_value(unsigned long reload_value);
int TimerOut(int (*func)(void), int ms);
int	TimerPoll(void);
void	sleep(int ms);

#endif