/*  	Copyright (C) MOXA Inc. All rights reserved.

    	This software is distributed under the terms of the
    	MOXA License.  See the file COPYING-MOXA for details.
*/

/*
    	mii.h

    	Standard MII definition
    	Refer to http://en.wikipedia.org/wiki/Media_Independent_Interface

    	2008-06-11	Will Suen
		Created
	2008-10-14	Chin-Fu Yang		
		modified		
		
*/
#ifndef MII_H
#define MII_H

/*------------------------------------------------------ Macro / Define -----------------------------*/

#define MII_BITS(h, l)				(((1 << ((h) - (l) + 1)) - 1) << l)		// Set to 1 from Bit L to Bit H
#define MII_BIT(b)				(1 << (b))							// Set Bit b to 1
#define MII_BVAL(v, b)			((v) << (b))							// Set Value to bit position from bit b

/*
 * Standard PHY registers. Control it by SMI.
 */
#define REG_MII_BMCR	0	/* Basic Mode Configuration		*/
#define REG_MII_SWR		1	/* Status Word Register			*/
#define REG_MII_PHY1	2	/* PHY Identification 1			*/
#define REG_MII_PHY2	3	/* PHY Identification 2			*/
#define REG_MII_AAR		4	/* Ability Advertisement		*/
#define REG_MII_LPAR	5	/* Link Partner Ability			*/
#define REG_MII_ANER	6	/* Auto Negotiation Expansion	*/

/*
 * Basic Mode Configuration bits
 */
#define MII_BMCR_RST			MII_BIT(15)	/* Reset					*/
#define MII_BMCR_LPBK			MII_BIT(14)	/* Loopback select			*/
#define MII_BMCR_SPEED			MII_BIT(13)	/* Speed select				*/
#define MII_BMCR_ANEN			MII_BIT(12)	/* Auto-negotiation enable	*/
#define MII_BMCR_PWDN			MII_BIT(11)	/* Power down enable		*/
#define MII_BMCR_ISO			MII_BIT(10)	/* MII isolation			*/
#define MII_BMCR_RESTART_AN	MII_BIT(9)	/* Restart auto-negotiation	*/
#define MII_BMCR_DUPLEX		MII_BIT(8)	/* Duplex mode select		*/
#define MII_BMCR_COLTST		MII_BIT(7)	/* Collision test enable	*/

/*
 * Status Word bits
 */
#define MII_SWR_T4		MII_BIT(15)	/* Not capable of T4 operation						*/
#define MII_SWR_TXFD	MII_BIT(14)	/* Capable of 100-TX full duplex operation			*/
#define MII_SWR_TXHD	MII_BIT(13)	/* Capable of 100-TX half duplex operation			*/
#define MII_SWR_TPFD	MII_BIT(12)	/* Capable of 10-TP full duplex operation			*/
#define MII_SWR_TPHD	MII_BIT(11)	/* Capable of 10-TP half duplex operation			*/
#define MII_SWR_SPREM	MII_BIT(6)	/* Accepting MII frames with preamble suppressed	*/
#define MII_SWR_ANC		MII_BIT(5)	/* auto-negotiation complete						*/
#define MII_SWR_RF		MII_BIT(4)	/* remote fault detected							*/
#define MII_SWR_AN		MII_BIT(3)	/* capable of auto-negotiation operation			*/
#define MII_SWR_LINK	MII_BIT(2)	/* link established									*/
#define MII_SWR_JAB		MII_BIT(1)	/* jabber detected									*/
#define MII_SWR_EXT		MII_BIT(0)	/* extended register exist							*/

/*
 * Ability Advertisement bits
 */
#define MII_AAR_NP		MII_BIT(15)		/* Next page								*/
#define MII_AAR_ACK		MII_BIT(14)		/* Acknowledgement							*/
#define MII_AAR_RFDET	MII_BIT(13)		/* Remote fault detected					*/
#define MII_AAR_T4		MII_BIT(9)		/* capable of T4 operation					*/
#define MII_AAR_TXFD	MII_BIT(8)		/* capable of 100-TX full duplex operation	*/
#define MII_AAR_TXHD	MII_BIT(7)		/* capable of 100-TX half duplex operation	*/
#define MII_AAR_TPFD	MII_BIT(6)		/* capable of 10-TP full duplex operation	*/
#define MII_AAR_TPHD	MII_BIT(5)		/* capable of 10-TP half duplex operation	*/
#define MII_AAR_SELECT	MII_BITS(4,0)	/* Selector field							*/

/*
 * Link Partner Ability bits
 */
#define MII_LPAR_NP		MII_BIT(15)		/* Next page								*/
#define MII_LPAR_ACK	MII_BIT(14)		/* Acknowledgement							*/
#define MII_LPAR_RFDET	MII_BIT(13)		/* Remote fault detected					*/
#define MII_LPAR_T4		MII_BIT(9)		/* capable of T4 operation					*/
#define MII_LPAR_TXFD	MII_BIT(8)		/* capable of 100-TX full duplex operation	*/
#define MII_LPAR_TXHD	MII_BIT(7)		/* capable of 100-TX half duplex operation	*/
#define MII_LPAR_TPFD	MII_BIT(6)		/* capable of 10-TP full duplex operation	*/
#define MII_LPAR_TPHD	MII_BIT(5)		/* capable of 10-TP half duplex operation	*/
#define MII_LPAR_SELECT	MII_BITS(4,0)	/* Selector field							*/

#endif	/* MII_H */
