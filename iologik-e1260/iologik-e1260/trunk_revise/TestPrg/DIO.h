#define DI_channel	8
#define DO_channel	4
#define PWR_Fail_Back_Size	DI_channel*4
#define EEPROM_PWR_Fail_add		0x4000

#if E1242	
	void diag_do_DI_Read_test(void);
	UINT8 diag_do_SPI_DIO_Burn_in_test(void);
	void diag_do_SPI_DIO_Loop_Back_test(void);
	void diag_do_DO_Pulse_test (void);
//	void diag_do_DIO_EMS_test (UINT8 DO_DATA);
	void diag_do_DIO_EMS_test (void);
	void diag_do_SPI_DO_test(void);
	void diag_do_SPI_DO_Manual_test(void);
//	void diag_do_DIO_AI_noise_test (UINT8 AI_Noice_test);
	void diag_do_DIO_AI_noise_test (void);
	void SPI_DIO_init (void);
	void SPI_DIO_disable (void);
	UINT8 DIO_TYPE_READ (void);
#endif