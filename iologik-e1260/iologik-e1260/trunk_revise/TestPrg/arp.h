/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	arp.h

	2008-09-25	Chin-Fu Yang		
		new release

	2008-10-15	Chin-Fu Yang		
		modified    
*/


#ifndef _ARP_H
#define _ARP_H




/*------------------------------------------------------ Macro / Define -----------------------------*/
#define DARP_HRD_ETHER		0x0100	/* ethernet hardware address */
#define DARP_PRO_IP		0x0008	/* IP protocol */
#define DARP_HLN		6	/* ethernet hardware address length */
#define DARP_PLN		4	/* internet protocol address length */
#define DARP_OP_REQUEST 	0x0100	/* request to resolve address */
#define DARP_OP_REPLY		0x0200	/* response to previous request */
#define DARP_MAX_TABLE		64	/* maximum 64 arp table 	*/
#define ARP_TABLE_ADJUST_TIME	5000	/* 5 seconds to adjust arptable */
#define ARP_TABLE_TIMEOUT	300000	/* 5 minutes to move out	*/

/*------------------------------------------------------ Structure ----------------------------------*/
struct addrtab {		/* Internet to ethernet address table */
	ulong	ip;		/* internet address */
	uchar	netaddr[6];	/* ethernet address */
	ulong	timeout;	/* time to remove from this table	*/
};



struct arpinfo {
	ushort	hrd;	    /* format of hardware address */
	ushort	pro;	    /* format of protocol address */
	uchar	hln;	    /* length of hardware address */
	uchar	pln;	    /* length of protocol address */
	ushort	op;	    /* ARP operation: request or reply */
	uchar	sha[6];     /* sender hardware address */
	ulong	spa;	    /* sender protocol address */
	uchar	tha[6];     /* target hardware address */
	ulong	tpa;	    /* target protocol address */
}__attribute__((packed));
typedef struct arpinfo *arpinfo_t;




/*------------------------------------------------------ Extern / Function Declaration -----------------*/
int ArpTimeOut(void);
void	ArpInit(void);
int	ArpAdd(ulong ip,uchar *addr);
void	ArpReceive(msgstr_t msg);
int	ArpSearch(ulong ip);






#endif