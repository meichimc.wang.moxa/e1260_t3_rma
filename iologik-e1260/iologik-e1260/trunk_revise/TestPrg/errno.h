/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	errno.h

	Define error numbers for return value of functions.

	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified	
		
*/

#ifndef _ERRNO_H_
#define _ERRNO_H_

/*------------------------------------------------------ Macro / Define ----------------------------*/
#define EPERM            	1   // operation not permitted
#define EIO              	5   // I/O error
#define ENXIO            	6   // no such device or address
#define ENOMEM         	12  // out of memory
#define EACCES          	13  // permission denied
#define EBUSY            	16  // device or resource busy
#define ENODEV          	19  // no such device
#define EINVAL           	22  // invalid argument
#define ENOSPC          	28  // no space left on device
#define ENOSYS          	38  // function not implemented/supported
#define ECHRNG          	44  // channel number out of range
#define ENODATA       	61  // no data available
#define ETIME            	62  // timer expired
#define EPROTO          	71  // protocol error


// error code define
#define DBG_UART_OK							0
#define DBG_UART_ERROR_NOT_FOUND			-1
#define DBG_UART_ERROR_NOT_OPENED			-2
#define DBG_UART_ERROR_NO_SUCH_PORT		-3
#define DBG_UART_ERROR_PARAMETER			-4


#endif
