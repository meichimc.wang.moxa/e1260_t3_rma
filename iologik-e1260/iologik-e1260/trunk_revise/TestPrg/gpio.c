/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	gpio.c

	16pins GPIO.
	S2E only supports single-edge trigger
	
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified				

    
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "lib.h"
#include "global.h"
#include "timer.h"
#include "gpio.h"


extern void Printf(const char *fmt, ...);


static UINT32 gpio_last_interrupt_time;



/**	\brief
 *
 *	Set the GPIO direction for each pin.
 *
 */
void GpioInit()
{	
	GpioWriteData(PIO_DIO_EN,1);
	GpioWriteData(PIO_DIO_SEL,1);
	GpioWriteData(PIO_SW_READY,1);
	GpioWriteData(PIO_ADC_CS,1);
	GpioWriteData(PIO_EEPROM_PROTECT,1);
	GpioWriteData(PIO_FLASH_PROTECT,0);

	GpioSetDir(PIO_PWR_FAIL,PIO_IN);					// Power Fail trigger
	GpioSetDir(PIO_SW_READY ,PIO_OUT);  			// ReadyLED is Output mode
	GpioSetDir(PIO_SW_BUTTON ,PIO_IN);				// SW Reset button Iutput mode

	GpioSetDir(PIO_WADG ,PIO_IN);							// SW Reset button Iutput mode
	GpioSetDir(PIO_JP1 ,PIO_IN); 							// Jumper 1

	GpioSetDir(PIO_ADC_CS ,PIO_OUT); 					// ADC select is Output mode
	GpioSetDir(PIO_DIO_SEL ,PIO_OUT);  				// DIO select is Output mode
	GpioSetDir(PIO_DIO_EN ,PIO_OUT);  				// DIO enable is Output mode
	GpioSetDir(PIO_DATA_READY ,PIO_IN); 			// ADC data ready is Input mode

	GpioSetDir(PIO_EEPROM_PROTECT,PIO_OUT);		// LAN Relay Control
	GpioSetDir(PIO_FLASH_PROTECT,PIO_OUT);		// LAN Relay Control
	GpioSetDir(PIO_SPI_0 ,PIO_OUT); 					// DIO DI Control

	Outw(S2E_GPIO_BASE + GPIO_LS_SYNC , 1);			// sync
	
	//\\ Initial value may set to 0 
}

/**	\brief
 *
 *	Read the value of all GPIO pins.
 *
 *	\retval 		GPIO value of all pins
 */
UINT32 GpioReadOutputReg(void)
{
	return Inw(S2E_GPIO_BASE + GPIO_DATA);
}                         



/**	\brief
 *
 *	Read the value of all GPIO pins in Iuput Register.
 *	Inculding Input and Output Pins.
 *
 *	\retval 		GPIO value of all pins
 */
UINT32 GpioReadInputReg(void)
{
	return Inw(S2E_GPIO_BASE + GPIO_EX);
}



/**	\brief
 *
 *	Writing 16pins at a time.
 *
 *	\param[in]	val : value to be written
 *
 */
void GpioWrite16Pins(UINT16 val)
{
	UINT32 reg;
	reg = Inw(S2E_GPIO_BASE + GPIO_DATA);
	reg &= 0xffff0000;			// mask 16 bits
	reg |= val;	
	Outw(S2E_GPIO_BASE + GPIO_DATA , reg);
	
}



/**	\brief
 *
 *	Writing 8pins at a time.
 *	This function is only for loopback test.
 *
 *	\param[in]	val : value to be written
 *	\param[in]	byte_type : High Byte or Low Byte to be written.
 *
 */
void GpioWrite8Pins(UINT16 val , gpio_byte byte_type)
{
	UINT32 reg;
	reg = GpioReadInputReg();
	val = val & 0x00ff;
	
	if(byte_type == gpio_low_byte){
		reg &= 0xffffff00;			// mask	8bits
		reg |= val ;		// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);
	}
	else if(byte_type == gpio_high_byte){
		reg &= 0xffff00ff;			// mask	
		reg |= (val << 8);	// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);		
	}

}


/**	\brief
 *
 *	Writing 4pins at a time.
 *	This function is only for loopback test.
 *
 *	\param[in]	val : value to be written
 *	\param[in]	byte_type : which group of pins to be written.
 *
 */
void GpioWrite4Pins(UINT16 val , gpio_4pins pins)
{
	UINT32 reg;
	reg = GpioReadInputReg();
	val = val & 0x000f;
	
	if(pins == gpio_4pins_0){
		reg &= 0xfffffff0;			// mask	8bits
		reg |= val ;		// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);
	}
	else if(pins == gpio_4pins_1){
		reg &= 0xffffff0f;			// mask	
		reg |= (val << 4);	// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);		
	}else if(pins == gpio_4pins_2){
		reg &= 0xfffff0ff;			// mask	
		reg |= (val << 8);	// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);		
	}else if(pins == gpio_4pins_3){
		reg &= 0xffff0fff;			// mask	
		reg |= (val << 12);	// mask
		Outw(S2E_GPIO_BASE + GPIO_DATA , reg);		
	}

}

















/**	\brief
 *
 *	Write 1 pin at a time
 *
 *	\param[in]	pin : Which pin you want to write
 *	\param[in]	val : value to be written
 *
 */
void GpioWriteData(UINT32 pin,UINT32 val)
{

	UINT32 reg;
	
	reg = Inw(S2E_GPIO_BASE + GPIO_DATA);
		
	if(val){
		reg |= (0x1 << pin);
	}
	else {
		reg &= ~(0x1 << pin);
	}
	
	Outw(S2E_GPIO_BASE + GPIO_DATA , reg);
}      



/**	\brief
 *
 *	Sets the direction of a GPIO pin.
 *
 *	\param[in]	pin : Which pin.
 *	\param[in]	dir : direction
 *
 */
void GpioSetDir(UINT32 pin,UINT32 dir)
{
	UINT32 tmp;
 	tmp = Inw(S2E_GPIO_BASE + GPIO_DIRCTION);
 	if(dir)
 		tmp |= 1<<pin; 		//output 
 	else
		tmp &= ~(1<<pin);		//input 
    	Outw(S2E_GPIO_BASE + GPIO_DIRCTION , tmp); 
}            



/**	\brief
 *
 *	Sets a pin from normal mode to interrupt mode. 
 *
 *	\param[in]	pin : Which pin you want to set.
 *
 */
void  GpioSetIntEnable(UINT32 pin)
{
	UINT32 tmp;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_ENABLE);
	tmp |= 1<<pin;
  	Outw(S2E_GPIO_BASE + GPIO_INT_ENABLE,tmp);
}                        



/**	\brief
 *
 *	Sets a pin from interrupt mode to normal mode
 *
 *	\param[in]	pin : Which pin you want to set.
 *
 */
void  GpioSetIntDisable(UINT32 pin)
{
	UINT32 tmp;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_ENABLE);
	tmp &= ~(1<<pin);
  	Outw(S2E_GPIO_BASE + GPIO_INT_ENABLE , tmp);
}  



/**	\brief
 *
 *	Gets the raw interrupt status ( non-masked ).
 *
 *	\param[in]	pin : Which pin you want to check.
 *
 *	\retval TRUE		Interrupt pending
 *  	\retval FALSE		No interrupt pending
 */
BOOL  GpioIsRawInt(UINT32 pin)
{
	UINT32 tmp;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_RAWSTATUS);
  	if((tmp & (1<<pin)) != 0)
  		return TRUE;
  	else
  		return FALSE;
}   




/**	\brief
 *
 *	Masks one pin to disable interrupt generation from state change.
 *
 *	\param[in]	pin : Which pin you want to mask.
 *
 */
void  GpioSetIntMask(UINT32 pin)
{
	UINT32 tmp;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_MASK);
	tmp |= 1<<pin;
  	Outw(S2E_GPIO_BASE + GPIO_INT_MASK,tmp);
}                        



/**	\brief
 *
 *	Unmask one pin to enable interrupt generation from state change.
 *
 *	\param[in]	pin : Which pin you want to disable the mask.
 *
 */
void  GpioSetIntUnMask(UINT32 pin)
{
	UINT32 tmp = 0;

	// mask all for test
	Outw(S2E_GPIO_BASE + GPIO_INT_MASK,0xffff);

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_MASK);
	tmp &= ~(1<<pin);
  	Outw(S2E_GPIO_BASE + GPIO_INT_MASK,tmp);
	
	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_MASK);
	Printf("GPIO_INT_MASK  Reg = 0x%.8x \r\n" , tmp);
}  




/**	\brief
 *
 *	Gets the masked interrupt status.
 *
 *	\retval 		The value of which pin has interrupt pendded.
 */
UINT32 GpioGetIntMaskStatus(void)
{
 	return Inw(S2E_GPIO_BASE + GPIO_INT_STATUS);	
}	




/**	\brief
 *
 *	Clear interrupt pending by user , hardware will not clear it automatically !
 *	Clear interrupt pending by writing value 1.
 *
 *	\param[in]	data : Write 1 to clear interrupt pending.
 *
 */
void GpioClearInt(UINT32 data)
{
  	Outw(S2E_GPIO_BASE + GPIO_INT_EOI,data);
}



/**	\brief
 *
 *	Setting the trigger mode (Level (0)or Edge (1)) from pin number.
 *
 *	\param[in]	pin : Which pin you want to set.
 *	\param[in]	trigger : Level (0)or Edge (1)
 *
 */
void GpioSetTrigger(UINT32 pin,UINT32 trigger)
{
    	UINT32 tmp = 0;
	UINT32  tmp1 = 0;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_TYPE);

  	if(trigger == gpio_edge_trigger){		// Edge
  		tmp |= 1<<pin;
  		Outw(S2E_GPIO_BASE + GPIO_INT_TYPE , tmp);
  	}else{							// Level
  	    	tmp1 |= 1<<pin;
  	    	tmp &= (~tmp1);
  		Outw(S2E_GPIO_BASE + GPIO_INT_TYPE , tmp);  	    
  	}
}



/**	\brief
 *
 *	Setting the trigger mode ( Rising-trigger (1) or Falling-trigger (0) ) from pin number
 *
 *	\param[in]	pin : Which pin you want to set.
 *	\param[in]	Active : Rising-trigger(1)/High or Falling-trigger(0)/Low
 *
 */
void GpioSetActiveMode(UINT32 pin,UINT32 Active)
{
    	UINT32 tmp = 0;
	UINT32 tmp1 = 0;

 	tmp = Inw(S2E_GPIO_BASE + GPIO_INT_POLARITY);

  	if(Active == gpio_polarity_high){		// high
  		tmp |= 1<<pin;
  		Outw(S2E_GPIO_BASE + GPIO_INT_POLARITY,tmp);
  	}else{							// low
  	    tmp1 = 1<<pin;
  	    tmp &= ~tmp1;
  		Outw(S2E_GPIO_BASE + GPIO_INT_POLARITY,tmp);  	    
  	}
}




/**	\brief
 *
 *	Setting this pin to interrupt mode.
 *
 *	\param[in]	pin : Which pin you want to set.
 *	\param[in]	trigger : Level (0)or Edge (1)
 *	\param[in]	active : Rising-trigger (1) or Falling-trigger (0)
 *
 */
void GpioEnableInt(UINT32 pin,UINT32 trigger,UINT32 active)
{
 	GpioSetTrigger(pin,trigger);	
 	GpioSetActiveMode(pin,active);	
 	GpioSetIntUnMask(pin);	
	GpioClearInt(0xFFFF);		//clear IRQ first
	GpioSetIntEnable(pin);	
}




/**	\brief
 *
 *	Setting this pin to normal operation mode.
 *
 *	\param[in]	pin : Which pin you want to mask.
 *
 */
void GpioDisableInt(UINT32 pin)
{
 	GpioSetIntDisable(pin);
 	GpioSetIntMask(pin); 	
}



/**	\brief
 *
 *	An interrupt service routine of GPIO. It just counts the occurences of interrupt generated.
 *	We could clear EOI here.
 *
 */
static void GpioIsr(void)
{
		
	Printf("time = %.8d\r\n" , Gsys_msec);
	
	if( (Gsys_msec - gpio_last_interrupt_time) > GPIO_DEBOUNCE){
		irq_occurs[IRQ_GPIO]++;
	}

	gpio_last_interrupt_time = Gsys_msec;
	DISABLE_IRQ(IRQ_GPIO);
	GpioClearInt(0xFFFF);		// clear EOI of GPIO
}




/**	\brief
 *
 *	Sets the GPIO ISR to ISR array.
 *
 */
void GpioSetIsr(void)
{
	isr_set_isr(IRQ_GPIO, GpioIsr);		// setting ISR
}



/*----------------------------------------------- application -----------------------------------*/

/**	\brief
 *
 *	Control the ready_led
 *
 *	\param[in]	mode : 0:ON , 1:OFF
 *
 */
void ReadyLed(int mode)
{
	if(mode){
		GpioWriteData(PIO_SW_READY,0);
	}
	else{
		GpioWriteData(PIO_SW_READY,1);
	}
}

/**	\brief
 *
 *	Gets the status of jumper.
 *
 *	\retval 		Jumper Status, 0:shorted ; 1:not shorted
 */
int GetJpStatus(int num)
{
	if(GpioReadInputReg() & (0x1 << num)){
		return 0;
	}
	else{
		return 1;
	}
}


/**	\brief
 *
 *	Sets the status of beeper
 *
 *	\param[in]	val : binary status of beeper (0/1)
 *
 */
/*		// Jerry Wu
void SetBeeperStatus(int val)
{
	GpioWriteData(PIO_BEEPER,val);
}
*/

/**	\brief
 *
 *	Turn on/off the individual LED.
 *
 *	\param[in]	num : the number of debug LED
 *	\param[in]	mode : 1: turn on ; 0: turn off
 *
 */
/*void (int num, int mode)
{
	if(mode){
		GpioWriteData(PIO(0 + num),0);
	}
	else{
		GpioWriteData(PIO(0 + num),1);
	}
}
*/