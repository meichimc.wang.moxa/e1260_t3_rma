/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	lib.h

	Commom declaration of functions.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified		
    
*/

#ifndef _LIB_H
#define _LIB_H

#include   <stdarg.h>

/*------------------------------------------------------ Macro / Define ----------------------------*/
#define Inw(addr)			(*((volatile unsigned long*)(addr)))
#define Outw(addr, val)		(*((volatile unsigned long*)(addr))) = (val)
#define Inhw(addr)			(*((volatile unsigned short*)(addr)))
#define Outhw(addr, val)		(*((volatile unsigned short*)(addr))) = (val)
#define Inb(addr)				(*((volatile unsigned char*)(addr)))
#define Outb(addr, val)		(*((volatile unsigned char*)(addr))) = (val)

#define INADDR_NONE 		0xFFFFFFFF	// for TFTP
#define CYCLES_PER_MS		10000		// configure by USER 
#define MAX_CMD_LEN   		80			// for debug


/*------------------------------------------------------ Structure ----------------------------------*/
typedef struct debugarg {
	unsigned char	*arg[4];
} DebugArg;



/*------------------------------------------------------ Extern / Function Declaration -----------------*/
int skip_atoi(const char **s);
void	Strcpy(uchar *dbuf,uchar *sbuf);
int StrLength(const char *ptr);
unsigned long simple_strtoul(const char *cp,char **endp,unsigned int base);
long simple_strtol(const char *cp,char **endp,unsigned int base);
int atoi(const char *str);
uchar Atob(char *buf);
ushort Atoh(char *buf);
ulong Atow(char *buf);
ushort Atos(char *buf);
ulong Atol(char *buf);
int _is_hex(char c);
int _from_hex(char c);
char _tolower(char c);
int IsDigit(char *buf);
int IsHex(char *buf);
ushort Htons(ushort val);
ulong Htonl(ulong val);
int HtoS(uchar *buf, long val,int len);
int DtoS(uchar *buf, long val,int len);
int  get_digit(char *buf,int *len);
void delay_ms(int ms );
ushort htons(ushort val);



#endif
