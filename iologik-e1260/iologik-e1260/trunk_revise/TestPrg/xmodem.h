/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	xmodem.h

	header file of xmodem
		
	2008-09-25	Chin-Fu Yang		
		new release
    
*/


#ifndef _XMODEM_H
#define _XMODEM_H


/*------------------------------------------------------ Macro / Define -----------------------------*/
#define XMODEMTIMEOUT	3000		  /* 3 seconds		  */

#define SOH		0x01
#define EOT		0x04
#define CAN		0x18
#define NAK		0x15
#define ACK		0x06

#define XFRAMESIZE	(sizeof(struct xmodem_frame))
/*------------------------------------------------------ Structure ----------------------------------*/
// Xmodem Frame Structure
typedef struct xmodem_frame {
	uchar	ser;
	uchar	chk;
	uchar	buf[128];
	ushort	crc;
} XmodemFrame;

/*------------------------------------------------------ Extern / Function Declaration -----------------*/
long XmodemRecv(int port,uchar *buf);



#endif