/*  	Copyright (C) MOXA Inc. All rights reserved.

	This software is distributed under the terms of the
	MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	dbg_uart.h

	DesignWare UART handle routines

	2008-06-10	Albert Yu
		first release
	2008-10-14	Chin-Fu Yang		
		modified			
*/

#include "chipset.h"
#include "types.h"
#include "lib.h"
#include "isr.h"
#include "global.h"
#include "dbg_uart.h"
#include "moxauart.h"
#include "string.h"
#include "errno.h"

// counting the occurrence times of different interrupts
static int dbg_isr_ls;
static int dbg_lsr_bi;
static int dbg_lsr_fe;
static int dbg_lsr_pe;
static int dbg_lsr_oe;

static  UINT8   IER;
static  UINT32  flag;
static  UINT32	txrptr, txwptr;
static  UINT32	rxrptr, rxwptr;
static  unsigned char *txbuf;
static  unsigned char *rxbuf;

volatile static BOOL dbg_uart_init = FALSE;		// initiallized or not
static char digits[]="0123456789ABCDEF";
#define UART_ISR_MODE		// Enable the ISR mode of UART



/**	\brief	
 *
 *	Debug UART Interrupt Service Routine
 */
void dbg_uart_isr(void)
{
	UINT8 iir; 
	UINT8 lsr;
	UINT8 ch;
	UINT8 fcr;
	int wptr;	
	int rptr;
	int i;
	
	iir = Inw(S2E_DW_UART_BASE + DBG_UART_IIR);		// get IIR to decide which kind of UART Interrupt occurs

	// 1. check the UART Interrupt Pending
	if ( iir & UART_IIR_NO_INT ) {
		// If IIR has no Interrupt pending, BIT0 is set to 1, otherwise BIT0 is set to 0.
		return;
	}

	// 2. which kind of interrupt occurs
	iir &= DBG_UART_IIR_MASK;

	// 3. Handle RDA ( Received data available ) , RTO ( Character timeout indication ) first.
	if (iir == DBG_UART_IIR_RDA ||
		iir == DBG_UART_IIR_RTO ) {
		wptr = rxwptr;
		rptr = (rxrptr - 1) & DBG_UART_BUFFER_MASK;

		while(1) {
			lsr = Inw(S2E_DW_UART_BASE + DBG_UART_LSR);		
			if(lsr & UART_LSR_DR){		// check whether data ready
		
		    		ch = Inw(S2E_DW_UART_BASE + DBG_UART_RX);
		    		if ( rptr != wptr ) {
		    			rxbuf[wptr++] = ch;	// write to rx buffer which is in SDRAM
		    			wptr &= DBG_UART_BUFFER_MASK;
		    		}
					
			}else{
				break;
			}
		}
		rxwptr = wptr;

	} else if ( iir == UART_IIR_THRI ) {
	// 4. Handle THRE Interrupt. When this interrupt occurs, it says the FIFO is empty, so we can input our data to FIFO.
		rptr = txrptr;
		wptr = txwptr;
		for ( i=0; i<DBG_UART_TX_FIFO_SIZE; i++ ) {
			if ( rptr == wptr ) {
				// Turn off THRI, it will be turn on again in dbg_sio_write()
				IER &= ~UART_IER_ETBEI;
				Outw(S2E_DW_UART_BASE + DBG_UART_IER, IER);
				break;
			}
			if (Inw(S2E_DW_UART_BASE + 0x7C/* UART Status Register */) & 0x02/*TX FIFO not full*/){
				Outw((S2E_DW_UART_BASE + DBG_UART_TX), txbuf[rptr++]);
				rptr &= DBG_UART_BUFFER_MASK;
			}
		}
		txrptr = rptr;

	} else if ( iir == UART_IIR_MSI ) {
    // 5. Handle Modem Status Interrupt, It can detect signal change of DCD, DSR and CTS.
		// clear by reading MSR register
		Inw(S2E_DW_UART_BASE + DBG_UART_MSR);	// clear modem change interrupt

	} else if ( iir == DBG_UART_IIR_LSR ) {
	// 6. Handle the change of Receive Line Status (Overrun Error, Parity Error, Frame Error, Break Interrupt)
		// clear by reading LSR register
		lsr = Inw(S2E_DW_UART_BASE + DBG_UART_LSR);
		if ( lsr & UART_LSR_SPECIAL ) {
			// LSR has many types of situation, we perform different handlers for different situation
			ch = Inw(S2E_DW_UART_BASE + DBG_UART_RX);
			if(lsr&UART_LSR_BI)
				dbg_lsr_bi++;
			if(lsr&UART_LSR_FE)
				dbg_lsr_fe++;	
			if(lsr&UART_LSR_PE)
				dbg_lsr_pe++;					
			if(lsr&UART_LSR_OE)
				dbg_lsr_oe++;					
			if ( (lsr&UART_LSR_OE) && !(lsr&UART_LSR_DR) )
			{
			    fcr = Inw(S2E_DW_UART_BASE + DBG_UART_FCR);
				Outw(S2E_DW_UART_BASE + DBG_UART_FCR, fcr | UART_FCR_CLEAR_RCVR);
			}
		}
		dbg_isr_ls++;
	}
}





/**	\brief	Debug SIO init
 *
 *	1. Set UART clock source
 *  	2. Set ISR
 *  	3. Set TX/RX memory space
 */
void dbg_sio_init(void)
{
    #define DBG_UART_PORT_NUM   0   /* 1 ~ 2 for MOXA UART */

 	if(dbg_uart_init == TRUE){		// If we have set it already , skip it.
	    return;
	}

    /* Select external U_CLK (14.7456 MHz) as UART clock and Eanble UART clock */
    //VPlong(S2E_REMAP_BASE + REMAP_CLOCK_SEL) |= (DW_UART_CLK_SRC_EXTERNAL | DW_UART_CLK_ENABLE);
	//VPlong(S2E_REMAP_BASE + REMAP_CLOCK_SEL) |= 0x18;

#ifdef DBG_BY_MOXA_UART
	isr_set_isr(IRQ_MOXA_UART, dbg_uart_isr);		
	ENABLE_IRQ(IRQ_MOXA_UART);	
#else
#ifdef UART_ISR_MODE
    /* Set ISR */
	isr_set_isr(IRQ_GENERAL_UART, dbg_uart_isr);		
	ENABLE_IRQ(IRQ_GENERAL_UART);
#endif
#endif

    /* Set TX/RX memory space */
	txbuf = (uchar *)(SDRAM_UART_TX_BASE(DBG_UART_PORT_NUM));
	rxbuf = (uchar *)(SDRAM_UART_RX_BASE(DBG_UART_PORT_NUM));
	txrptr = txwptr = 0;
	rxrptr = rxwptr = 0;

	IER = 0;
	flag = 0;

	dbg_uart_init = TRUE;
}





/**	\brief	Debug SIO init
 *
 *	\retval DBG_UART_OK
 */
int dbg_sio_open(int port, int mode)
{
	if ( flag & DBG_UART_OPENED ){
		return DBG_UART_OK;		// If it opened before , then return
	}

	// 1. Disable UART Interrupt
    Outw(S2E_DW_UART_BASE + DBG_UART_IER, 0);

    // 2. Clear TX/RX FIFO
    Outw(S2E_DW_UART_BASE + DBG_UART_FCR, UART_FCR_CLEAR_RCVR | UART_FCR_CLEAR_XMIT);
	
    /*
     * 3. Clear interrupt registers.
     */
     // clear these registers by reading them
    (void)Inw((S2E_DW_UART_BASE) + DBG_UART_LSR);
    (void)Inw((S2E_DW_UART_BASE) + DBG_UART_RX);
    (void)Inw((S2E_DW_UART_BASE) + DBG_UART_IIR);
    (void)Inw((S2E_DW_UART_BASE) + DBG_UART_MSR);
	
    /*
     * Now, initialize the UART
     */
    // 4. Set N81, RTS, DTR
    Outw(S2E_DW_UART_BASE + DBG_UART_LCR, UART_LCR_WLEN8);    /* reset DLAB */	// N81
	// DTR and RTS on
    Outw(S2E_DW_UART_BASE + DBG_UART_MCR, UART_MCR_DTR | UART_MCR_RTS);
	
    /*
     * And clear the interrupt registers again for luck.
     */
    // 5. clear these 4 Registers again
    (void)Inw(S2E_DW_UART_BASE + DBG_UART_LSR);
    (void)Inw(S2E_DW_UART_BASE + DBG_UART_RX);
    (void)Inw(S2E_DW_UART_BASE + DBG_UART_IIR);
    (void)Inw(S2E_DW_UART_BASE + DBG_UART_MSR);

    // 6. Set FIFO(8bytes) enable, RCVR Trigger = 1 character in the FIFO, TX Emptry Trigger = FIFO emtpy
    Outw(S2E_DW_UART_BASE + DBG_UART_FCR, UART_FCR6_R_TRIGGER_1 | UART_FCR6_T_TRIGGER_0 | UART_FCR_ENABLE_FIFO);

    flag |= DBG_UART_OPENED;    // setting complete!!

    /*
     * Finally, enable interrupts
     */
#ifdef UART_ISR_MODE
    IER = /*UART_IER_ELSI |*/ UART_IER_ERBI;
#else
	IER = 0;
#endif
    Outw(S2E_DW_UART_BASE + DBG_UART_IER, IER);

	return DBG_UART_OK;
}





/**	\brief	Debug SIO close
 *
 *  1. Reset TX/RX buffer
 *  2. Disable interrupts of UART
 *  3. Pull-down DTR/RTS
 *
 *	\retval DBG_UART_OK
 */
int dbg_sio_close(int port)
{
	// If UART was not opened before, then return
    if ( !(flag & DBG_UART_OPENED) ){
    	return DBG_UART_OK;
    }	

    flag = 0;
    
    // Reset TX/RX buffer
    txrptr = txwptr = rxrptr = rxwptr = 0;

	// Disable interrupts of UART
    IER = 0;
    Outw(S2E_DW_UART_BASE + DBG_UART_IER, IER);

	// Pull-down DTR/RTS
    Outw(S2E_DW_UART_BASE + DBG_UART_MCR, 0);

    return DBG_UART_OK;
}





/**	\brief	Debug SIO IO control
 *
 *  1. Change baudrate
 *  2. Set LCR to switch mode, for example N81
 *
 *	\param[in]	baud    Assign baudrate: 50 ~ 921600
 *  	\param[in]  mode    Assign parity, stop bits length, data length
 *                      \li Bits 1:0    Data Length 00=5bits, 01=6bits, 10=7bits, 11=8bits
 *                      \li Bits 2      Stop Bits   0=1 stop bit, 1=1.5 or 2 stop bit
 *                      \li Bits 3      Parity      0=disabled, 1=enabled
 *                      \li Bits 4      Even parity 0=Odd parity, 1=Even parity
 *
 *  \retval DBG_UART_ERROR_PARAMETER    baud rate out of range
 *	\retval DBG_UART_OK
 */
int dbg_sio_ioctl(int port, int baud, int mode)
{
	UINT32  div;
	UINT8   dll, dlm;
	
    if ( baud < 50 || baud > SERIAL_RSA_BAUD_BASE )
	    return DBG_UART_ERROR_PARAMETER;

    div = SERIAL_RSA_BAUD_BASE / baud;
    Outw(S2E_DW_UART_BASE + DBG_UART_LCR, mode | UART_LCR_DLAB);
    dll = div & 0xff;
    dlm = div >> 8;
    Outw(S2E_DW_UART_BASE + DBG_UART_DLL, dll);  	/* LS of divisor */
    Outw(S2E_DW_UART_BASE + DBG_UART_DLM, dlm);	  	/* MS of divisor */
    Outw(S2E_DW_UART_BASE + DBG_UART_LCR, mode);    /* reset DLAB */
     
    return DBG_UART_OK;
}



/**	\brief
 *
 *	1. flush RX / TX Buffer
 *
 *	\param[in]	port : from 0 , ID of UART
 *	\param[in]	mode : FLUSH_INOUT or not
 *
 *	\retval 		error code
 */
int dbg_sio_flush(int port, int mode)
{
	if ( !(mode & FLUSH_INOUT) ){
		return DBG_UART_OK;
	}
	
	if ( mode & FLUSH_INPUT ){
		rxrptr = rxwptr;
	}
	
	if ( mode & FLUSH_OUTPUT ){
		txwptr = txrptr;
	}

	return DBG_UART_OK;
}




/**	\brief	Debug SIO line control
 *
 *  1. Set MCR to control line signal
 *
 *  \param[in]  mode    Set DTR, RTS on/off
 *                      \li Bits 0  DTR     0=off, 1=on
 *                      \li Bits 1  RTS     0=off, 1=on
 *
 *	\retval DBG_UART_OK
 */
int dbg_sio_lctrl(int port, int mode)
{
    Outw(S2E_DW_UART_BASE + DBG_UART_MCR, mode);
    
    return DBG_UART_OK;
}




/**	\brief	Debug SIO input queue length
 *
 *	\return Remaining bytes in RX buffer
 *
 */
int dbg_sio_iqueue(int port)
{
    return (rxwptr - rxrptr) & DBG_UART_BUFFER_MASK;
}




/**	\brief	Debug SIO read data
 *
 *  \param[in]  buf     Buffer pointer to which you want the data to be stored
 *  \param[in]  len     Assign the number of bytes you want to read
 *
 *	\return Number of bytes be read
 */
int dbg_sio_read(int port, char *buf, int len)
{
    int i, j;

    if ( buf == NULL || len <= 0 ){
    	return 0;
    }		
    if ( rxrptr == rxwptr )
        return 0;
    j = (rxwptr - rxrptr) & DBG_UART_BUFFER_MASK;
    if ( j <= len )
    	len = j;
    for ( i=0, j=rxrptr; i<len; j&=DBG_UART_BUFFER_MASK, i++ )
        *buf++ = rxbuf[j++];
    rxrptr = j;

    return len;
}




/**	\brief	Debug SIO write data
 *
 *  \param[in]  buf     Buffer pointer which contains the data you want to write to TX buffer
 *  \param[in]  len     Assign the number of bytes you want to write
 *
 *	\return Number of bytes be wrote
 */
int dbg_sio_write(int port, char *buf, int len)
{
#if 1
	int i;

	i = 0;
	while (i < len)
	{
		//if (Inw(S2E_DW_UART_BASE + DBG_UART_LSR) & UART_LSR_THRE)
		//if (Inw(S2E_DW_UART_BASE + 0x80) < 8)	/* TX FIFO Level Register */
		if (Inw(S2E_DW_UART_BASE + 0x7C/* UART Status Register */) & 0x02/*TX FIFO not full*/)
			Outw((S2E_DW_UART_BASE + DBG_UART_TX), buf[i++]);
	}
#else	/* Interrupt mode will interrupt SPI transmission and cause SPI RX overflow */
    int i, j;

    if ( buf == NULL || len <= 0 ){
    	return 0;
    }
    j = (txrptr - txwptr - 1) & DBG_UART_BUFFER_MASK;
    if ( j <= len )
	    len = j;
    for ( i=0, j=txwptr; i<len; j&=DBG_UART_BUFFER_MASK, i++ )
    	txbuf[j++] = *buf++;

    txwptr = j;

	// If THRI Bit was disable, we enable it, so that UART could wait for next interrupt of THRE.
    if ( !(IER & UART_IER_ETBEI) ) {
    	IER |= UART_IER_ETBEI;
    	Outw(S2E_DW_UART_BASE + DBG_UART_IER, IER);
    }
#endif
    return len;
}




/**	\brief	Debug SIO get character
 *
 *  	Read 1 character of data from RX buffer. If there is no data in RX buffer,
 *  	it will wait until at least one character appears.
 *
 *	\return One character in head of RX buffer
 */
int	dbg_sio_getch(int port)
{
	int ret;
	unsigned char ch;

    for(;;){	
		if (dbg_sio_iqueue(port) > 0){
        	break;  // wait until RX buffer has data 
		}
	}
	dbg_sio_read(port, &ch, 1);
	ret = ch & 0xff;

	return(ret);
}




 /**	\brief
 *
 *	Write 1 char of data from rx buffer
 *
 *	\param[in]	port : UART port number
 *	\param[in]	ch : data to be written
 *
 */
void dbg_sio_putch(int port, unsigned char ch)
{
	dbg_sio_write(port,&ch,1);
}



/**	\brief
 *
 *	Print the content of buffer to console
 *
 *	\param[in]	*buf : buffer to print out.
 *
 */
void dbgPrintf(char *buf)
{
	int i;

	i = 0;
	while (i < strlen(buf)){
		if (Inw((S2E_MOXA_UART_BASE + UART_LSR) & UART_LSR_THRE)){
			Outw((S2E_MOXA_UART_BASE + UART_TX), buf[i++]);
		}
	}
}



/**	\brief
 *
 *	Print the content of buffer to console in HEX format
 *
 *	\param[in]	value : buffer to print out.
 *
 */
void dbgPrintShortHex(ushort value)
{
	int i, idx;

	for (i = 3; i >= 0; i--)
	{
		idx = value >> (i*4);
		while (1)
		{
			if (Inw((S2E_MOXA_UART_BASE + UART_LSR) & UART_LSR_THRE))
			{
				Outw((S2E_MOXA_UART_BASE + UART_TX), digits[idx&0xf]);
				break;
			}
		}
	}
}

