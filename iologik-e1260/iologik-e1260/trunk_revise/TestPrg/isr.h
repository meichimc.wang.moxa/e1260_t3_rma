/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	isr.h

	Function declaration and IRQ numbers.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-02	Chin-Fu Yang		
		modified		
    
*/


#ifndef _ISR_H
#define _ISR_H

/*------------------------------------------------------ Macro / Define -----------------------------*/
// S2E's  FIQ / IRQ Number , 0 : The lowest priority
#define FIQ_TIMER			0
#define IRQ_SPI				0
#define IRQ_GPIO				1
#define IRQ_I2C				2
#define IRQ_GENERAL_UART	3
#define IRQ_MAC				4
#define IRQ_WDT				5
#define IRQ_MOXA_UART		6

#define MAXHNDLRS		7		// S2E Max IRQ Handlers
#define MAXHNDLRS_FAST	1		// S2E (Timer)


#define ENABLE_IRQ(n)		VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) |= ((unsigned long)1<<(n))
#define ENABLE_FIQ(n)		VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) |= ((unsigned long)1<<(n))

#define DISABLE_IRQ(n)		VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) &= (~((unsigned long)1<<(n)))
#define DISABLE_FIQ(n)		VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) &= (~((unsigned long)1<<(n)))

#define ENABLE_INT(n)   		ENABLE_IRQ(n)
#define DISABLE_INT(n)  		DISABLE_IRQ(n)


/*------------------------------------------------------ Structure ----------------------------------*/
typedef enum _irq_trigger_type{
	level_trigger = 0,
	edge_trigger = 1
}irq_trigger_type;

typedef enum _irq_high_low{
	sensitive_low = 0,
	sensitive_high = 1
}irq_high_low;


/*------------------------------------------------------ Extern / Function Declaration -----------------*/
extern UINT32 irq_occurs[MAXHNDLRS];			// number of IRQ occurence
extern UINT32 fiq_occurs[MAXHNDLRS_FAST];
extern int	 isrRegVal[17];	/* filled by start.S, r0~r14, cpsr, spsr */

void enable_interrupts (void);
int disable_interrupts (void);
void	isr_set_isr(volatile unsigned long vector, void (*handler)(void));
void	(*get_isr(volatile unsigned long vector))(void);
void	isr_set_isr_fast(volatile unsigned long vector, void (*handler)(void));
void	(*get_isr_fast(volatile unsigned long vector))(void);
void	isr_init_isr(void);
void	Ssys_isrUndefHandler(unsigned long address);
void	Ssys_isrPrefetchHandler(unsigned long address);
void	Ssys_isrAbortHandler(unsigned long address);
void	Ssys_isrSwiHandler(unsigned long number, unsigned long addr);
void	Ssys_isrIrqHandler(void);
void	Ssys_isrFiqHandler(void);
void FIQ_SetIntTrig(int intNum,int intMode,int intLevel);
void IRQ_SetIntTrig(int intNum,int intMode,int intLevel);



#endif /* _ISR_H */
