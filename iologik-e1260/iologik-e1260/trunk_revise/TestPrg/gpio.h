/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	gpio.h

	16pins GPIO.
	Define the purpose of each pin.
	
	2008-06-10	Chin-Fu Yang		
		new release
	2008-09-25	Chin-Fu Yang		
		modified				
 	2009-02-09	Nolan Yu		
		modified   
*/

#ifndef _GPIO_H
#define _GPIO_H


/*------------------------------------------------------ Macro / Define ----------------------------*/
#define GPIO_DEBOUNCE	2		// 2ms
// S2E's usages of GPIO.

#define PIO(ch)						ch

#define PIO_PWR_FAIL				PIO(0)
#define PIO_SW_BUTTON				PIO(1)
#define PIO_JP1							PIO(2)
#define PIO_SW_READY				PIO(3)
#define PIO_WADG						PIO(4)
/* ADC */
#define PIO_DATA_READY			PIO(5)
#define PIO_ADC_CS					PIO(6)
#define PIO_EEPROM_PROTECT	PIO(7)
#define PIO_DIO_EN					PIO(9)		// DIO Enable GPIO
#define PIO_DIO_SEL					PIO(10)		// DIO select GPIO
#define PIO_FLASH_PROTECT		PIO(11)
#define PIO_SPI_0						PIO(12)
#define PIO_SPI_1						PIO(13)
#define PIO_SPI_2						PIO(14)
#define PIO_SPI_3						PIO(15)

#define PIO_IN				0
#define PIO_OUT				1


/*------------------------------------------------------ Structure ----------------------------------*/
typedef enum _gpio_byte{
	gpio_low_byte = 0,			// pin0~7
	gpio_high_byte = 1			// pin8~15
}gpio_byte;

typedef enum _gpio_trigger_type{
	gpio_level_trigger = 0,			
	gpio_edge_trigger = 1			
}gpio_trigger_type;

typedef enum _gpio_trigger_polarity{
	gpio_polarity_low = 0,			
	gpio_polarity_high = 1			
}gpio_trigger_polarity;

typedef enum _gpio_4pins{
	gpio_4pins_0 = 0,			
	gpio_4pins_1 = 1	,
	gpio_4pins_2 = 2,			
	gpio_4pins_3 = 3		
}gpio_4pins;

/*------------------------------------------------------ Extern / Function Declaration -----------------*/
void GpioInit(void);
UINT32 GpioReadOutputReg(void);
UINT32 GpioReadInputReg(void);
void	GpioWriteData(UINT32 pin,UINT32 val);
void	GpioSetDir(UINT32 pin,UINT32 dir);
void	GpioSetIntEnable(UINT32 pin);
void	fLib_SetGpioIntDisable(UINT32 pin);
BOOL	GpioIsRawInt(UINT32 pin);
void	GpioSetIntMask(UINT32 pin);
void	GpioSetIntUnMask(UINT32 pin);
UINT32 GpioGetIntMaskStatus(void);
void	GpioClearInt(UINT32 data);
void	GpioSetTrigger(UINT32 pin,UINT32 trigger);
void	GpioSetActiveMode(UINT32 pin,UINT32 Active);
void	GpioEnableInt(UINT32 pin,UINT32 trigger,UINT32 active);
void	GpioDisableInt(UINT32 pin);
void	GpioWrite16Pins(UINT16 val);
void	GpioWrite8Pins(UINT16 val , gpio_byte byte_type);
void	GpioWrite4Pins(UINT16 val , gpio_4pins pins);
void	GpioSetIsr(void);

void	ReadyLed(int mode);
int	GetJpStatus(int num);
//void	SetBeeperStatus(int val);
//void	ShowIndividualLED(int num, int mode);


#endif /*_GPIO_H */
