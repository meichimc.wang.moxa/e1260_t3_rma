/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	lib.c

	Commom functions.
			
	2008-06-10	Chin-Fu Yang		
		new release
 	2008-09-25	Chin-Fu Yang		
		modified	
		
*/


#include  "types.h"
#include	"chipset.h"
#include  "ctype.h"
#include	"lib.h"
#include	"global.h"





int skip_atoi(const char **s)
{
	int i=0;

	while (is_digit(**s)){
		i = i*10 + *((*s)++) - '0';
	}
	return i;
}



/**	\brief
 *
 *	Gets the length of string
 *
 *	\param[in]	*ptr : string
 *
 *	\retval 		length of string
 */
int StrLength(const char *ptr)
{
	int	i=0;

	while (*ptr++ != 0){
		i++;
	}
	return(i);
}


/**	\brief
 *
 *	Copy a string to a specific buffer
 *
 *	\param[in]	*dbuf : destination buffer
 *	\param[in]	*sbuf : source buffer (string)
 *
 */
void	Strcpy(uchar *dbuf,uchar *sbuf)
{
	int	i = 0;

	while (sbuf[i] != 0) {
	    dbuf[i] = sbuf[i];
	    i++;
	}
	dbuf[i] = 0;
}


unsigned long simple_strtoul(const char *cp,char **endp,unsigned int base)
{
	unsigned long result = 0,value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}
		if (!base) {
			base = 8;
		}
	}
	if (!base) {
		base = 10;
	}
	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}
	if (endp)
		*endp = (char *)cp;
	return result;
}



long simple_strtol(const char *cp,char **endp,unsigned int base)
{
	if(*cp=='-')
		return -simple_strtoul(cp+1,endp,base);
	return simple_strtoul(cp,endp,base);
}



/**	\brief
 *
 *	Converts string to integer
 *
 *	\param[in]	*str : the string to convert
 *
 *	\retval 		integer
 */
int atoi(const char *str) 
{
    return ( (int)( simple_strtoul (str, (char **) NULL, 10) ) );
}



/**	\brief
 *
 *	Converts ASCII string to byte binary value  (unsigned char) (1 byte)
 *
 *	\param[in]	*buf : the string to convert
 *
 *	\retval 		byte binary value
 */
uchar Atob(char *buf)
{
	uchar	value = 0;
	char	ch;
	int	i=0;

	for (;;) {
	    ch = *buf++;
	    if ((ch >= '0') && (ch <= '9'))
		value = (value << 4) | (ch - '0');
	    else if ((ch >= 'a') && (ch <= 'f'))
		value = (value << 4) | (ch - 'a' + 10);
	    else if ((ch >= 'A') && (ch <= 'F'))
		value = (value << 4) | (ch - 'A' + 10);
	    else
		break;
	    i++;
	    if (i >= 2)
		break;
	}
	return(value);
}



 /**	\brief
 *
 *	Converts ASCII string to Half-word binary value  (unsigned short) (2 bytes)
 *
 *	\param[in]	*buf : the string to convert
 *
 *	\retval 		unsigned short binary value
 */
ushort Atoh(char *buf)
{
	ushort	value = 0;
	char	ch;
	int	i=0;


	for (;;) {
	    ch = *buf++;
	    if ((ch >= '0') && (ch <= '9'))
		value = (value << 4) | (ch - '0');
	    else if ((ch >= 'a') && (ch <= 'f'))
		value = (value << 4) | (ch - 'a' + 10);
	    else if ((ch >= 'A') && (ch <= 'F'))
		value = (value << 4) | (ch - 'A' + 10);
	    else
		break;
	    i++;
	    if (i >= 4)
		break;
	}
	return(value);
}



 /**	\brief
 *
 *	Converts ASCII string to word binary value  (unsigned long) (4 bytes)
 *
 *	\param[in]	*buf : the string to convert
 *
 *	\retval 		unsigned long binary value
 */
ulong Atow(char *buf)
{
	ulong	value = 0;
	char	ch;
	int	i=0;


	for (;;) {
	    ch = *buf++;
	    if ((ch >= '0') && (ch <= '9'))
		value = (value << 4) | (ch - '0');
	    else if ((ch >= 'a') && (ch <= 'f'))
		value = (value << 4) | (ch - 'a' + 10);
	    else if ((ch >= 'A') && (ch <= 'F'))
		value = (value << 4) | (ch - 'A' + 10);
	    else
		break;
	    i++;
	    if (i >= 8)
		break;
	}
	return(value);
}


 
 /**	\brief
 *
 *	Converts ASCII string to unsigned short
 *
 *	\param[in]	*buf : the string to convert
 *
 *	\retval 		integer
 */
ushort Atos(char *buf)
{
	ushort	value = 0;
	char	ch;

	for (;;) {
	    ch = *buf++;
	    if ((ch >= '0') && (ch <= '9'))
		value = value * 10 + ch - '0';
	    else
		break;
	}
	return(value);
}




 /**	\brief
 *
 *	Converts ASCII string to unsigned long	
 *
 *	\param[in]	*buf : the string to convert
 *
 *	\retval 		integer
 */
ulong Atol(char *buf)
{
	ulong	value = 0;
	char	ch;

	for (;;) {
	    ch = *buf++;
	    if ((ch >= '0') && (ch <= '9'))
		value = value * 10 + ch - '0';
	    else
		break;
	}
	return(value);
}



/**	\brief
 *
 *	Check if the input is a hex value
*
 *	\param[in]	c : character to process
 *
 *	\retval 		yes or no
 */
int _is_hex(char c)
{
    return (((c >= '0') && (c <= '9')) ||
            ((c >= 'A') && (c <= 'F')) ||            
            ((c >= 'a') && (c <= 'f')));
}


/**	\brief
 *
 *	Get the value of the character.
 *
 *	\param[in]	c : character to process
 *
 *	\retval 		value of the character
 */
int _from_hex(char c) 
{
    int ret = 0;

    if ((c >= '0') && (c <= '9')) {
        ret = (c - '0');
    } else if ((c >= 'a') && (c <= 'f')) {
        ret = (c - 'a' + 0x0a);
    } else if ((c >= 'A') && (c <= 'F')) {
        ret = (c - 'A' + 0x0A);
    }
    return ret;
}



/**	\brief
 *
 *	Converts the character to lower character
 *
 *	\param[in]	c : a character
 *
 *	\retval 		lower character
 */
char _tolower(char c)
{
    if ((c >= 'A') && (c <= 'Z')) {
        c = (c - 'A') + 'a';
    }
    return c;
}



/**	\brief
 *
 *	Check if the string is a digit
 *
 *	\param[in]	*buf : the string
 *
 *	\retval 		yes or no
 */
int IsDigit(char *buf)
{
	char	ch;

	for (;;) {
	    ch = *buf++;
	    if (ch == 0)
		break;
	    if ((ch >= '0') && (ch <= '9'))
		continue;
	    return(0);
	}
	return(1);
}



/**	\brief
 *
 *	Check if the string is a HEX
 *
 *	\param[in]	*buf : the string
 *
 *	\retval 		yes or no
 */
int IsHex(char *buf)
{
	char	ch;

	for (;;) {
	    ch = *buf++;
	    if (ch == 0)
		break;
	    if(_is_hex(ch) == 0)
      	    return(0);
	}
	return(1);
}



ushort Htons(ushort val)
{
	static uchar   ch[2],ch1[2];
	static ushort  ret;

	*(ushort *)ch = val;
	ch1[0] = ch[1];
	ch1[1] = ch[0];
	ret = *(ushort *)ch1;
	return(ret);
}



ulong Htonl(ulong val)
{
	static uchar	ch[4];
	static uchar	ch1[4];
	static ulong	ret;

	*(ulong *)ch = val;
	ch1[0] = ch[3];
	ch1[1] = ch[2];
	ch1[2] = ch[1];
	ch1[3] = ch[0];
	ret = *(ulong *)ch1;
	return(ret);
}



 
 /**	\brief
 *
 *	Convert hex value to string
 *
 *	\param[in]	*buf : a string you want to convert
 *	\param[in]	val : hex value to be convert
 *	\param[in]	len : val length
 *
 *	\retval 		string length of val
 */
int HtoS(uchar *buf, long val,int len)
{
	static char	tbuf[80];
	int		index = 0,j,k;
	long		i;

	for (k=0;k<8;k++) {
	    i = val & 0x0000000f;
	    if (i > 9)
		tbuf[index++] = i - 10 + 'a';
	    else
		tbuf[index++] = i + '0';
	    val >>= 4;
	    if (val == 0)
		break;
	}
	while (index < len)
	    tbuf[index++] = '0';
	for (j=0;j<index;j++)
	    buf[j] = tbuf[index-j-1];
	return(index);
}


 
 /**	\brief
 *
 *	Convert decimal value to string
 *
 *	\param[in]	*buf : a string you want to convert
 *	\param[in]	val : decimal value to be convert
 *	\param[in]	len : val length
 *
 *	\retval 		string length of val
 */
int DtoS(uchar *buf, long val,int len)
{
	static char	tbuf[80];
	int		index = 0;
	int		j,base;
	long		i;

	base = 0;
	if (val < 0) {
	    buf[base++] = '-';
	    val = val * -1;
	}

	if (val == 0)
	    tbuf[index++] = '0';

	while (val > 0) {
	    i = val % 10;
		tbuf[index++] = i + '0';
	    val = val / 10;
	}
	while (index < len)
	    tbuf[index++] = '0';
	for (j=0;j<index;j++)
	    buf[j+base] = tbuf[index-j-1];
	return(index+base);
}



/**	\brief
 *
 *	Gets number of digit on a string
 *
 *	\param[in]	*buf : string
 *	\param[in]	*len : string length
 *
 *	\retval 		number of digit on this string
 */
int  get_digit(char *buf,int *len)
{
	int	i=0;
	char	ch;

	*len = 0;
	for (;;) {
	    	ch = *buf++;
	    	if ((ch < '0') || (ch >'9')){
			break;
		}
	    	*len = *len * 10 + ch - '0';
	    	i++;
	}
	return(i);
}





/**	\brief
 *
 *	Delay in milliseconds.
 *	Implement by busy-waiting. We must configure CYCLES_PER_MS in "lib.h" first.
 *
 *	\param[in]	ms : milliseconds
 *
 */
void delay_ms(int ms )
{
	vlong cycles = (ms * CYCLES_PER_MS);

	while(cycles != 0){
		cycles--;
	}
}


 
 /**	\brief
 *
 *	Swap high,low bytes of ushort data
 *
 *	\param[in]	val : value to swap
 *
 *	\retval 		swapped value
 */
ushort htons(ushort val)
{
	uchar	ch[2],ch1[2];
	ushort	ret;

	*(ushort *)ch = val;
	ch1[0] = ch[1];
	ch1[1] = ch[0];
	ret = *(ushort *)ch1;
	return(ret);
}

