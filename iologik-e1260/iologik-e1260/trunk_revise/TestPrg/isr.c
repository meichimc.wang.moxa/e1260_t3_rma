/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	isr.c

	Interrupt Service Routine.
	Fast Interrupt Service Routine.
			
	2008-06-10	Chin-Fu Yang		
		new release
	2008-10-02	Chin-Fu Yang		
		modified
    
*/

#include "types.h"
#include "chipset.h"
#include "isr.h"
#include "lib.h"
#include "gpio.h"		// show LED
#include "moxauart.h"	// for debug ISR 

UINT32 irq_occurs[MAXHNDLRS];			// number of IRQ occurence
UINT32 fiq_occurs[MAXHNDLRS_FAST];

// Call Interrupt Service Routine
static void (*interrupt_handlers[MAXHNDLRS])(void);
static void (*fast_interrupt_handlers[MAXHNDLRS_FAST])(void);		// FIQ Handler Array



/**	\brief
 *
 *	Dummy ISR for each interrupts.
 *
 */
static void isr_dummy_isr(void)
{

}





/**	\brief
 *
 *	Disable all interrupt.
 *
 */
static void isr_disable_all_interrupt(void)
{
	VPlong(S2E_INTC_BASE + IRQ_INT_ENABLE) =  0x00000000;		/* 0 : Disable the interrupt source */	
	VPlong(S2E_INTC_BASE + FIQ_INT_ENABLE) =  0x00000000;		/* 0 : Disable the fast interrupt source */	
}


/**	\brief
 *
 *	Initial ISR array and assign the dummy ISR to each interrupt.
 *
 */
static void isr_init_isr_table(void)
{
	int i;
    	
	for ( i=0; i<MAXHNDLRS; i++ ){
		interrupt_handlers[i] = isr_dummy_isr;
		irq_occurs[i] = 0;
	}
	// S2E FIQ Handler Initial
	for ( i = 0 ; i < MAXHNDLRS_FAST ; i++){
		fast_interrupt_handlers[i] = isr_dummy_isr;
		fiq_occurs[i] = 0;
	}
}



/**	\brief
 *
 *	Init ISR array and assign the dummy ISR to each interrupt.
 *
 */
void	isr_init_isr(void)
{
	isr_disable_all_interrupt();		// disable all interrupt source
	isr_init_isr_table();		
}


/**	\brief
 *
 *	Sets the ISR to ISR array(Interrupt Handler Vector Table).
 *
 *	\param[in]	vector : vector number
 *	\param[in]	handler : a function pointer to real interrupt service routine.
 *
 */
void	isr_set_isr(volatile unsigned long vector, void (*handler)(void))
{
	interrupt_handlers[vector] = handler;
}


/**	\brief
 *
 *	Get corresponding ISR from ISR table by vector number.
 *
 */
void	(*get_isr(volatile unsigned long vector))(void)
{
	return(interrupt_handlers[vector]);
}


/**	\brief
 *
 *	Sets the ISR to ISR array of FIQ.
 *
 *	\param[in]	vector : vector number of FIQ.
 *	\param[in]	handler : a function pointer to real interrupt service routine.
 *
 */
void	isr_set_isr_fast(volatile unsigned long vector, void (*handler)(void))
{
	fast_interrupt_handlers[vector] = handler;
}



/**	\brief
 *
 *	Get corresponding ISR from ISR table of FIQ by vector number.
 *
 */
void	(*get_isr_fast(volatile unsigned long vector))(void)
{
	return(fast_interrupt_handlers[vector]);
}



/**	\brief
 *
 *	Print string in character format
 *
 *	\param[in]	*str : string to print out
 *	\param[in]	len : string length
 *
 */
static void dbg_PrintStr(char *str, int len)
{
	int i = 0;

	while (i < len){
		if (Inw(S2E_DW_UART_BASE + UART_LSR*4) & UART_LSR_THRE){
			Outw((S2E_DW_UART_BASE + UART_TX*4), str[i++]);
		}
	}
}



/**	\brief
 *
 *	Print a 32-bit word in HEX format.
 *
 *	\param[in]	value : the 
 *
 */
static void dbg_PrintHex(ulong value)
{
	static char digits[]="0123456789ABCDEF";
	int i, idx;

	for (i = 7; i >= 0; i--){
		idx = value >> (i*4);
		while (1){
			if (Inw(S2E_DW_UART_BASE + UART_LSR*4) & UART_LSR_THRE){
				Outw((S2E_DW_UART_BASE + UART_TX*4), digits[idx&0xf]);
				break;
			}
		}
	}
}



/**	\brief
 *
 *	Print all ARM registers.
 *
 */
static void dbg_PrintReg(void)
{
	static char regStr[17][5] = {"r0  ","r1  ","r2  ","r3  ","r4  ","r5  ","r6  ","r7  ","r8  ","r9  ",
							     "r10 ","r11 ","r12 ","r13 ","r14 ","CPSR","SPSR"};
	int i;

	for (i = 0; i < 17; i++){
		dbg_PrintStr(regStr[i], 4);
		dbg_PrintStr("= ", 2);
		dbg_PrintHex(isrRegVal[i]);
		dbg_PrintStr("\r\n", 2);
	}
}





//=================================================================
//  Exception Handler Function
//=================================================================

/**	\brief
 *
 *	Undefine Instruction ISR
 *
 *	\param[in]	address : address of Undefine Instruction
 *
 */
void	Ssys_isrUndefHandler(ulong address)
{
//	Printf("\r\n * Exception -> Undefined Abort : [0x%lx] : 0x%lx  !!!", (ulong)address, *(ulong*)address);
	dbg_PrintStr("Undefined Abort : [0x", 21);
	dbg_PrintHex(address);
	dbg_PrintStr("] : ", 4);
	dbg_PrintHex(*(ulong*)address);
	dbg_PrintStr("!!!\r\n", 5);
}




/**	\brief
 *
 *	Prefetch Abort ISR
 *
 *	\param[in]	address : address of Prefetch Abort
 *
 */
void	Ssys_isrPrefetchHandler(ulong address)
{
//	Printf("\r\n * Exception -> Prefetch  Abort : [0x%lx] : 0x%lx  !!!", (ulong)address, *(ulong*)address);
	dbg_PrintStr("Prefetch Abort : [0x", 20);
	dbg_PrintHex(address);
	dbg_PrintStr("] : ", 4);
	dbg_PrintHex(*(ulong*)address);
	dbg_PrintStr("!!!\r\n", 5);
	dbg_PrintReg();
}


/**	\brief
 *
 *	Data Abort ISR
 *
 *	\param[in]	address : address of Data Abort
 *
 */
void	Ssys_isrAbortHandler(ulong address)
{
	dbg_PrintStr("Data Abort : [0x", 16);
	dbg_PrintHex(address);
	dbg_PrintStr("] : ", 4);
	dbg_PrintHex(*(ulong*)address);
	dbg_PrintStr("!!!\r\n", 5);
	dbg_PrintReg();
	//Printf("\r\nData Abort:[0x%lx]:0x%lx !!!", address, *(ulong*)address);
	//Printf("\r\n * Exception -> Data Abort : [0x%lx] : 0x%lx  !!!", (ulong)address, *(ulong*)address);
}


/**	\brief
 *
 *	Software Interrupt ISR
 *
 *	\param[in]	number : vector of Software Interrupt
 *	\param[in]	address : address of Software Interrupt
 *
 */
void	Ssys_isrSwiHandler(ulong number, ulong address)
{
	dbg_PrintStr("Software Interrupt Num=", 23);
	dbg_PrintHex(number);
	dbg_PrintStr(", Addr=", 7);
	dbg_PrintHex(*(ulong*)address);
	dbg_PrintStr("\r\n", 2);
	dbg_PrintReg();	
	//Printf("\r\n * Exception -> Software Interrupt Number %ld!!! ", number);
	//Printf("\r\n             -> Return Address : 0x%lX !!! ", (ulong)addr);
}




/**	\brief
 *
 *	ISR dispatcher. Check if the interrupt was be masked first.
 *	Find the number of IRQ and call the corresponding ISR.
 *
 */
void	Ssys_isrIrqHandler(void)
{
	unsigned long IntOffSet,tmp;
	int i,j;

	for(;;){
		tmp = IntOffSet = VPlong(S2E_INTC_BASE + IRQ_MASKSTATUS);
		if( IntOffSet == 0 ){		// if masked , return
			return;
		}
		for(j=0,i=0; tmp > 0L;i++){
			if(tmp & 0x01){
				(*interrupt_handlers[i])();
				if(i != IRQ_GPIO){
					irq_occurs[i]++;
				}	
			}
			tmp = tmp >> 1;		
			j++;
		}	
	}	
}




/**	\brief
 *
 *	ISR dispatcher. Check if the interrupt was be masked first.
 *	Find the number of FIQ and call the corresponding ISR.
 *
 */
void	Ssys_isrFiqHandler(void)
{
	unsigned long IntOffSet,tmp;
	int i,j;

    for(;;){
    	tmp = IntOffSet = VPlong(S2E_INTC_BASE + FIQ_FINALSTATUS);
		
    	if( IntOffSet == 0 )
    		return;
		
    	for(j=0,i=0; tmp > 0L;i++){
    	    	if(tmp & 0x01){
        		(*fast_interrupt_handlers[i])();
			fiq_occurs[i]++;;
        	}
        	tmp = tmp >> 1;		
        	j++;
        }	
    }	

}





/**	\brief
 *
 *	Enable IRQ interrupts.
 *
 */
void enable_interrupts (void)
{
    unsigned long temp;
    __asm__ __volatile__("mrs %0, cpsr\n"
			 "bic %0, %0, #0x80\n"
			 "msr cpsr_c, %0"
			 : "=r" (temp)
			 :
			 : "memory");
}



/**	\brief
 *
 *	Disable IRQ/FIQ interrupts.
 *	Returns true if interrupts had been enabled before we disabled them.
 *
 */
int disable_interrupts (void)
{
    unsigned long old,temp;
    __asm__ __volatile__("mrs %0, cpsr\n"
			 "orr %1, %0, #0xc0\n"
			 "msr cpsr_c, %1"
			 : "=r" (old), "=r" (temp)
			 :
			 : "memory");
	isr_disable_all_interrupt();
    return (old & 0x80) == 0;
}




