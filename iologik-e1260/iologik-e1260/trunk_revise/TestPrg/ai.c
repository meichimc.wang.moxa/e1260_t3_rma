/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	ai.c

	2009-08-03	Nolan Yu
		new release
	2009-08-28	Nolan Yu
		modified
*/
#include "types.h"
#include "chipset.h"
#include "spi_reg.h"
#include "spi.h"
#include "exspi.h"
#include "isr.h"
#include "lib.h"
#include "global.h"
#include "console.h"
#include "gpio.h"
#include "ai.h"
#include "timer.h"
#include "i2c.h"
#include "memory.h"
#include "arp.h"
#include "mac.h"
#include "udpburn.h"
#include "ip.h"
#include "Calibration.h"
#include "mptester.h"
#include "moxauart.h"
#include "DIO.h"

extern char Current_AI_Channel;
extern char Current_ADC_Calibration_Mode;
extern char ErrMsg[256];
extern int diag_get_value(char *title, UINT32 *val, UINT32 min, UINT32 max, int mode);
extern UINT8 diag_check_press_ESC(void);
extern int sprintf(char * buf, const char *fmt, ...);

void ADC_Init(void)
{
	UINT32 tmp;
//	SPI_Tx(PIO_ADC_CS,0x50010434,32);
	tmp = 0x50010400 | AnalogN | (AnalogP << 4);
	SPI_Tx(PIO_ADC_CS,tmp,32);
	// modulator speed OSC/128 = 4.9152MHz/128 = 38.4KHz
	// internal reference disable
	// buffer disable
	// Internal MUX V+ = AnalogP, V- = AnalogN
	tmp = 0x58018032 | (Input_signal << 6);
	SPI_Tx(PIO_ADC_CS,tmp,32);
	// modulator decimation rate = 38.4K/640 = 60Hz
	// unipolar or bipolar data format
	// digital filter mode : sinc-3
#if E1240 | E1242
	SPI_Tx(PIO_ADC_CS,0x56010281,32);
	// external MUX channel 0
	// Read Channel 1 state
	// GPIO0 : Determine Voltage or Current state
	// GPIO0,7 : input type, others : output type
#endif

#if E1260
	SPI_Tx(PIO_ADC_CS,0x5601028F,32);
	// external MUX channel 0
	// GPIO4,5,6 : output type, others : input type
#endif

#if E1262
	SPI_Tx(PIO_ADC_CS,0x5601028D,32);
	// external MUX channel 0
	// Read Channel 2 burn out
	// GPIO0 : Determine Voltage or Current state
	// GPIO0,2,3,7 : input type, others : output type
#endif
}

/******************************************************/
/*    Set the data rate of ADS1216                    */
/******************************************************/
void ADC_Data_Rate(UINT16 m_factor, UINT16 decimation_ratio)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(0);
	// read back setup register
	reg_read_back &= 0xef;
	// clear speed bit
	reg_read_back |= (UINT8)(m_factor << 4);
	// set speed bit
	cmd_set_reg = 0x500000 | (UINT32)reg_read_back;
	// command to set register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	reg_read_back = ADC_Register_Readback(9);
	// read back mode & decimation register
	cmd_set_reg = 0x58010000 | (UINT32)(reg_read_back & 0xf0);
	cmd_set_reg |= (UINT32)((decimation_ratio & 0x00ff) << 8);
	cmd_set_reg |= (UINT32)((decimation_ratio & 0x0700) >> 8);
	// set decimation ratio
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,32);
}

/*** Set ADC PGA gain ***/
void ADC_PGA_Gain(UINT8 gain)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(2);
	sleep(10);
	// read back analog control register
	reg_read_back &= 0xf8;
	// clear PGA gain bits
	cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | gain);
	// command to set ADS1216 MUX control register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
	// load calibration paramters
#if E1240 | E1242
	if(gain)
		cal_parameters_load(Current_AI_Channel, Current_mode);
	else
		cal_parameters_load(Current_AI_Channel, Voltage_mode);
#endif
#if E1262 | E1260
	cal_parameters_load(Current_AI_Channel, gain);
#endif

}

void diag_AI_ADC_PGA_gain(void)
{
	UINT8 gain;
    UINT32 ret=0;
	
	gain = ADC_Register_Readback(2) & 0x07;
	Printf("\r\nCurrent ADC PGA Gain is <X%d>\r\n", (UINT8)(1 << gain)); 
 	if(diag_get_value("ADC PGA Gain is ... (0:X1 / 1:X2 / 2:X4 / 3:X8) => ",&ret,0,7,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain((UINT8)ret);
}

/*** Channel Select ***/

void ADC_CH_Select(UINT8 VP, UINT8 VN)
{
	UINT32 cmd_set_reg;

	cmd_set_reg = 0x510000 | (UINT32)(VP << 4);
	cmd_set_reg |= (UINT32)VN;
	// command to set ADS1216 MUX control register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

void diag_AI_ADC_CH_sel_vp(void)
{
	UINT8 vn, vp;
	UINT8 reg_read_back;
    UINT32 ret=0;

	reg_read_back = ADC_Register_Readback(1);
	vn = reg_read_back & 0x0f;
	vp = (reg_read_back & 0xf0) >> 4;
	Printf("\r\nCurrent ADC V+ Channel is <CH-%d>\r\n", vp);  
	if(diag_get_value("ADC V+ Channel Select ... => ",&ret,0,8,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_CH_Select((UINT8)ret, vn);
}

void diag_AI_ADC_CH_sel_vn(void)
{
	UINT8 vn, vp;
	UINT8 reg_read_back;
    UINT32 ret=0;

	reg_read_back = ADC_Register_Readback(1);
	vp = (reg_read_back & 0xf0) >> 4;
	vn = reg_read_back & 0x0f;
	Printf("\r\nCurrent ADC V- Channel is <CH-%d>\r\n", vn);  
	if(diag_get_value("ADC V- Channel Select ... => ",&ret,0,8,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
 	ADC_CH_Select(vp, (UINT8)ret);
}

void AI_CH_Select(UINT8 channel)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;
//	UINT8 rchannel;

	reg_read_back = ADC_Register_Readback(6);
	// read back analog control register
	reg_read_back &= 0x0f;
	// preserve the digital I/O state
	Current_AI_Channel = channel;
//	rchannel = channel << 1;
	
//	rchannel |= ((channel & 0x4) >> 2);
	cmd_set_reg = 0x560000 | (UINT32)(channel << 4) | reg_read_back;
	// command to set ADS1216 DIO control register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

#if E1240 | E1242
void AI_TYPE_CH_Select(UINT8 channel)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(6);
	// read back analog control register
	reg_read_back &= 0xf1;
	// preserve the digital I/O state
	cmd_set_reg = 0x560000 | (UINT32)(channel << 1) | reg_read_back;
	// command to set ADS1216 DIO control register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

UINT8 AI_TYPE_READ(void){
	return (ADC_Register_Readback(6) & 0x1);
}
#endif

void diag_AI_CH_sel(void)
{
    UINT32 ret=0;
#if E1240 | E1242
	Printf("\r\nCurrent AI Channel is <%d>\r\n", Current_AI_Channel); 
  	if(diag_get_value("AI Channel Select is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1262
	Printf("\r\nCurrent TC Channel is <%d>\r\n", Current_AI_Channel); 
  	if(diag_get_value("TC Channel Select is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif

#if E1260
	Printf("\r\nCurrent RTD Channel is <%d>\r\n", Current_AI_Channel); 
  	if(diag_get_value("RTD Channel Select is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif

	AI_CH_Select((UINT8)ret);
}

/*** ADC filter setting ***/
void ADC_Filter_Select(UINT8 filter)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(9);
	// read back mode & decimation register
	reg_read_back &= 0xcf;
	// clear filter mode bits
	reg_read_back |= filter << 4;
	// set filter mode
	cmd_set_reg = 0x590000 | reg_read_back;
	// command to set mode & decimation register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

void diag_AI_ADC_filter_sel(void)
{
	UINT8 filter_mode;
    UINT32 ret=0;
	
	filter_mode = (ADC_Register_Readback(9) & 0x30) >> 4;
	Printf("\r\nCurrent ADC Digital Filter is ");
	switch(filter_mode)
	{
		case 0:	Printf("<Auto>\r\n");
				break;
		case 1:	Printf("<Fast>\r\n");
				break;
		case 2:	Printf("<Sinc-2>\r\n");
				break;
		case 3:	Printf("<Sinc-3>\r\n");
				break;
		default:break;
	}
  	if(diag_get_value("ADC Digital Filter Select is.. (0:auto / 1:fast / 2:sinc-2 / 3:sinc-3) => ",&ret,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_Filter_Select((UINT8)ret);
}

/*** ADC data type setting ***/
void diag_AI_ADC_unipolar_set(void)
{
	UINT8 input_mode;
    UINT32 ret=0;
	
	input_mode = (ADC_Register_Readback(9) & 0x40) >> 6;
	Printf("\r\nCurrent ADC Data Format is ");
	switch(input_mode)
	{
		case 0:	Printf("<Bipolar>\r\n");
				break;
		case 1:	Printf("<Unipolar>\r\n");
				break;
		default:break;
	}
  	if(diag_get_value("ADC Data Format Select is.. (0:Bipolar / 1:Unipolar) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_input_type_Select((UINT8)ret);
}

void ADC_input_type_Select(UINT8 input_type)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(9);
	// read back mode & decimation register
	reg_read_back &= 0xbf;
	// clear input mode bits
	reg_read_back |= input_type << 6;
	// set filter mode
	cmd_set_reg = 0x590000 | reg_read_back;
	// command to set mode & decimation register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

/*** ADC decimation ratio setting ***/
void diag_AI_ADC_decimation_ratio(void)
{
	UINT8 reg_read_back, m_factor;
	UINT16 decimation_ratio;
    UINT32 ret=0;
	
	decimation_ratio = ((UINT16)(ADC_Register_Readback(9) & 0x07)) << 8;
	decimation_ratio |= ADC_Register_Readback(8);

	reg_read_back = ADC_Register_Readback(0);
	m_factor = (reg_read_back & 0x10) > 4;

	Printf("\r\nCurrent ADC Decimation Ratio is <%d>\r\n", decimation_ratio); 
	if(diag_get_value("ADC Decimation Ratio ... => ",&ret,0,2047,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_Data_Rate(m_factor, (UINT16)ret);
}

/*** ADC m_factor setting ***/
void diag_AI_ADC_m_factor(void)
{
	UINT8 reg_read_back;
	UINT16 decimation_ratio;
    UINT32 ret=0;
	
	decimation_ratio = (UINT16)(ADC_Register_Readback(9) & 0x07) << 8;
	decimation_ratio |= ADC_Register_Readback(8);

	reg_read_back = ADC_Register_Readback(0);
	Printf("\r\nCurrent ADC Modulator Factor is ");  
	if ((reg_read_back & 0x10) == 0)
	       Printf("<128>\r\n");
	else
	       Printf("<256>\r\n");

	if(diag_get_value("ADC Modulator Factor Select ... (0:128 / 1:256) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	switch(ret){
		case  0: ADC_Data_Rate(ADC_M_FACTOR_128, decimation_ratio);
				 break;
		case  1: ADC_Data_Rate(ADC_M_FACTOR_256, decimation_ratio);
				 break;
		default: break;
	}
}

/*** ADC reference setting ***/
void ADC_Ref_Select(UINT8 dir, UINT8 voltage)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(0);
	// read back setup register
	reg_read_back &= 0xf3;
	// clear reference & voltage bits
	reg_read_back |= dir << 3;
	// set reference direction
	reg_read_back |= voltage << 2;
	// set internal reference voltage
	cmd_set_reg = 0x500000 | reg_read_back;
	// command to set setup register
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

void diag_AI_set_ref(void)
{
	UINT8 int_ref_v;
	UINT8 reg_read_back;
    UINT32 ret=0;

	reg_read_back = ADC_Register_Readback(0);
	// read back setup register
	int_ref_v = (reg_read_back & 0x04) >> 2;
	reg_read_back &= 0x08;
	// get reference direction bit
	Printf("\r\nCurrent Internal Reference is ");  
	if (reg_read_back == 0)
	       Printf("<Disabled>\r\n");
	else
	       Printf("<Enabled>\r\n");
	if(diag_get_value("ADC Reference Select ... (0:Disable / 1:Enable) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
 	ADC_Ref_Select((UINT8)ret, int_ref_v);
}

void diag_AI_set_ref_in_voltage(void)
{
	UINT8 ref_dir;
	UINT8 reg_read_back;
    UINT32 ret=0;

	reg_read_back = ADC_Register_Readback(0);
	// read back setup register
	ref_dir = (reg_read_back & 0x08) >> 3;
	reg_read_back &= 0x04;
	// get reference direction bit
	Printf("\r\nCurrent Internal Reference is ");  
	if (reg_read_back == 0)
	       Printf("<1.25V>\r\n");
	else
	       Printf("<2.5V>\r\n");
	if(diag_get_value("ADC Internal Reference Voltage Select ... (0:1.25V / 1:2.5V) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_Ref_Select(ref_dir, (UINT8)ret);
}

#if E1262
void Burnout_enable(void)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;

	reg_read_back = ADC_Register_Readback(6);
	// read back analog control register
	reg_read_back &= 0xf0;
	// preserve the digital I/O state
	cmd_set_reg = 0x560004 | reg_read_back;

	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);
}

UINT8 Burnout_disable(void)
{
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;
	UINT8 burn_out;

	reg_read_back = ADC_Register_Readback(6);
	burn_out = (reg_read_back >> 2) & 0x01;
	// read back analog control register
	reg_read_back &= 0xf0;
	// preserve the digital I/O state
	cmd_set_reg = 0x560006 | reg_read_back;
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	return (burn_out);
}
#endif

/*** ADC SW reset ***/
void ADC_SW_Reset(void)
{
	// command to reset ADS1216
	SPI_Tx(PIO_ADC_CS,0xfe,8);
	sleep(1);
}

/*** ADC data ready ***/
UINT8 ADC_Data_Ready(void)
{
	if ((GpioReadInputReg() & (0x1 << PIO_DATA_READY)) != 0)
		return FALSE;
	else
		return TRUE;
}

/*** ADC data sync ***/
void ADC_Data_Sync(void)
{
	SPI_Tx(PIO_ADC_CS,0xfcff,16);
}

/******************************************************/
/*    Read a single register byte from ADC ADS1216    */
/******************************************************/
UINT8 ADC_Register_Readback(UINT16 offset)
{
	UINT32 value;
	UINT16 cmd_read_reg = 0x1000;

	cmd_read_reg |= (offset << 8); 
	
	SetSPI_Len(15);
	GpioWriteData(PIO_ADC_CS,0);
	sleep(1);
	SPI_WRITE_DATA(cmd_read_reg);
	while(SPI_BUSY());

	SetSPI_Len(7);
	SPI_WRITE_DATA(0xff);
	while(SPI_BUSY());
	GpioWriteData(PIO_ADC_CS,1);
	value = SPI_READ_DATA();
	return (UINT8)value;
}

UINT32 ADC_Read(void)
{
	UINT32 value;
	GpioWriteData(PIO_ADC_CS,0);
	sleep(1);
	SetSPI_Len(7);
	SPI_WRITE_DATA(0x01);
	while(SPI_BUSY());

	SPI_Delay_10us();
	SPI_READ_DATA();
	SPI_WRITE_DATA(0xff);
	while(SPI_BUSY());
	value = SPI_READ_DATA() << 16;
	SPI_WRITE_DATA(0xff);
	while(SPI_BUSY());
	value |= (SPI_READ_DATA() << 8);
	SPI_WRITE_DATA(0xff);
	while(SPI_BUSY());
	value |= SPI_READ_DATA();
	GpioWriteData(PIO_ADC_CS,1);
	sleep(1);
	return value;
}

UINT32 ADC_read_a_value(UINT8 read_count)
{
	UINT8 i=0;
	UINT32 value;

	ADC_Data_Sync();
	sleep(5);
	while(!ADC_Data_Ready() && i<10) i++;
	if (i==10)
	{
		Printf("ADC no response!\r\n");
		return 1;
	}
	i=0;
	do{
		while(!ADC_Data_Ready());
		value = ADC_Read() & 0xFFFFFF;		
		i++;
	}while(i<read_count);
	return value;
}

/******************************************************/
/*              ADS1216 CHIP calibration              */
/******************************************************/

void ADC_Calibration(UINT8 mode)
{
	UINT32 cmd_set_reg;
	int i=0;

	cmd_set_reg = 0xf0 | mode;
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,8);
	sleep(1);
	while(!ADC_Data_Ready()){
		i++;
		if (i>100000)
		{
			Printf("\r\nADC no response!\r\n");
			break;
		}
	}
	Printf("\r\nCalibration over, wait iteration = %d!\r\n",i);
}

void diag_AI_ADC_calibration_sel(void)
{
    UINT32 ret=0;
	
	Printf("\r\nCurrent ADC Calibration Mode is ");
	switch(Current_ADC_Calibration_Mode)
	{
		case 0:	Printf("<Self Offset/Gain>\r\n");
				break;
		case 1:	Printf("<Self Offset>\r\n");
				break;
		case 2:	Printf("<Self Gain>\r\n");
				break;
		case 3:	Printf("<System Offset>\r\n");
				break;
		case 4:	Printf("<System Gain>\r\n");
				break;
		case 5:	Printf("<System Offset/Gain>\r\n");
				break;
		case 6:	Printf("<None>\r\n");
				break;
		default:break;
	}
  	if(diag_get_value("Mode Select (0:Self_O/G / 1:Self_O / 2:Self_G / 3:Sys_O / 4:Sys_G / 5:Sys_O/G / 6:No) => ",&ret,0,6,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	Current_ADC_Calibration_Mode = ret;
	if(ret < 5){
 		ADC_Calibration((UINT8)ret);
	}
	else if(ret == 5){
		System_auto_cal();
	}
	Printf("\r\nADC calibration on-going ........ \r\n");
}

/*** ADC system calibration method by Nolan ***/
void System_auto_cal(void){
	UINT32 offset_reg, gain_reg;
	UINT32 offset_tmp=0x00000000, gain_tmp=0x500000;
	UINT32 ADC_value;
	int i;
	UINT32 Value[37];
	UINT32 ave_high,ave_low;
	INT32 err_high=0,err_low=0;
	int cal_pass=0x1;
	UINT8 iter_no=0;

	ADC_CH_Select(EXT_VREF, AnalogCom);
	ADC_Filter_Select(ADC_FILTER_SINC_3);
	ADC_PGA_Gain(ADC_PGA_GAIN_1);
	
	Printf("\r\n--------------------------------------------------------------------\r\n");

	sleep(20);

#if Input_signal
	offset_reg = 0xfff000;
	gain_reg = 0x500000;
#else
	offset_reg = 600;
	gain_reg = 0x500000;
#endif

	write_parm_to_ADC(offset_reg,gain_reg);
	Printf(" Before calibration : Offset reg = 0x%06x , Gain reg = %06x\r\n", offset_reg, gain_reg);

	Printf("--------------------------------------------------------------------\r\n");

	do {
		Printf("\r\nIteration count .................................... <%d>\r\n", iter_no);

		ADC_CH_Select(EXT_VREF, AnalogCom);
		sleep(1);
		ADC_Data_Sync();
		while(!ADC_Data_Ready());
		ADC_Read();
		i=0;
		do{			
			while(!ADC_Data_Ready());
			ADC_value = ADC_Read() & 0xFFFFFF;
			Value[i]=ADC_value;
			i++;
		}while(i<36);
		ave_high = 0;

		for (i=4;i<36;i++) ave_high +=Value[i];
		ave_high >>=  5;

		ADC_CH_Select(AnalogGnd, AnalogCom);
		sleep(1);
		ADC_Data_Sync();
		while(!ADC_Data_Ready());
		ADC_Read();
		i=0;
		do{
			while(!ADC_Data_Ready());
			ADC_value = ADC_Read() & 0xffffff;
#if (Input_signal == 0)
			if(ADC_value & 0x800000)
				ADC_value |= 0xff000000;
#endif
			Value[i]=ADC_value;
			i++;
		}while(i<36);

		ave_low = 0;

		for (i=4;i<36;i++) ave_low += Value[i];	
		ave_low >>= 5;
#if Input_signal
		err_high = 0xFFFFFF - ave_high;
		if(err_high & 0xffff80)
			gain_tmp = gain_reg + (err_high >> 2);
		else
			gain_tmp = gain_reg + (err_high / 5);

		err_low = ave_low;
		if(offset_reg & 0x800000){
			offset_reg |= 0xff000000;
			offset_tmp = offset_reg + (err_low >> 2);
		}
		else
			offset_tmp = offset_reg + (err_low >> 2) ;
#else
		if(ave_high > 0x7fffff){
			err_high = ave_high - 0x7fffff;
			if(err_high & 0xffff80 )
				gain_tmp = gain_reg - (err_high >> 1);
			else
				gain_tmp = gain_reg - (err_high / 5);
		}
		else{
			err_high = 0x7fffff - ave_high;
			if(err_high & 0xffff80)
				gain_tmp = gain_reg + (err_high >> 1);
			else
				gain_tmp = gain_reg + (err_high / 5);
		}

		if(ave_low & 0x800000){
			ave_low |= 0xff000000;
			err_low = ave_low;
			if(offset_reg & 0x800000){
				offset_reg |= 0xff000000;
				offset_tmp = err_low - offset_reg;
			}
			else
				offset_tmp = err_low + offset_reg;
		}
		else{
			err_low = ave_low;
			if(offset_reg & 0x800000){
				offset_reg |= 0xff000000;
				offset_tmp = offset_reg + err_low;
			}
			else
				offset_tmp = offset_reg + err_low ;
		}
#endif		
		
		Printf("Devi_high = %d,Devi_low = %d\r\n",err_high,err_low);

		cal_pass = 1;
		if((err_high < 4) && (err_low < 4))
			cal_pass = 0;
		else{
			offset_tmp &= 0xffffff;
			gain_tmp &= 0xffffff;
			write_parm_to_ADC(offset_tmp,gain_tmp);
			sleep(100);
			offset_reg = offset_tmp;
			gain_reg = gain_tmp;
		}
		sleep(1);
		iter_no++;
		if(diag_check_press_ESC() == DIAG_ESC) return;
	}while(cal_pass);

	Printf(" After calibration : Offset reg = 0x%06x , Gain reg = %06x\r\n", offset_reg, gain_reg);

	iter_no = offset_reg & 0x000000ff;
	i2c_write_to_eeprom(TEMP_OFFSET , iter_no);
	iter_no = (offset_reg & 0x0000ff00)>>8;
	i2c_write_to_eeprom(TEMP_OFFSET+1 , iter_no);
	iter_no = (offset_reg & 0x00ff0000)>>16;
	i2c_write_to_eeprom(TEMP_OFFSET+2 , iter_no);

	iter_no = gain_reg & 0x000000ff;
	i2c_write_to_eeprom(TEMP_GAIN , iter_no);
	iter_no = (gain_reg & 0x0000ff00)>>8;
	i2c_write_to_eeprom(TEMP_GAIN+1 , iter_no);
	iter_no = (gain_reg & 0x00ff0000)>>16;
	i2c_write_to_eeprom(TEMP_GAIN+2 , iter_no);

	ADC_CH_Select(AnalogP, AnalogN); //AI input Channel
}

/******************************************************/
/*                      Parameter                     */
/******************************************************/
extern char EEPROM_address;
void cal_parameters_save(UINT8 channel, UINT8 input_type, UINT32 offset_reg, UINT32 gain_reg)
{
	UINT8 offset_ee[3], gain_ee[3];
	UINT16 add;

	offset_ee[0] = offset_reg & 0x000000ff;
	offset_ee[1] = (offset_reg & 0x0000ff00)>>8;
	offset_ee[2] = (offset_reg & 0x00ff0000)>>16;

	gain_ee[0] = gain_reg & 0x000000ff;
	gain_ee[1] = (gain_reg & 0x0000ff00)>>8;
	gain_ee[2] = (gain_reg & 0x00ff0000)>>16;

	add = channel*SINGLE_CHANNEL_EEPROM_DATA*6 + input_type * 6;

	i2c_write_to_eeprom(add,offset_ee[0]);
	i2c_write_to_eeprom(add+1,offset_ee[1]);
	i2c_write_to_eeprom(add+2,offset_ee[2]);
	i2c_write_to_eeprom(add+3,gain_ee[0]);
	i2c_write_to_eeprom(add+4,gain_ee[1]);
	i2c_write_to_eeprom(add+5,gain_ee[2]);
}

int cal_parameters_uniformity_check(void)
{
	int channel, input_type, ret=0;
	UINT32 max, min;
//	UINT8 offset_ee[3], gain_ee[3];
	UINT8 offset_ee[4], gain_ee[4];
	UINT32 offset_val, gain_val;
	UINT16 add;

	Printf("---------------------------------------------------------------------------\r\n");

	for(input_type=0;input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++) //檔位
	{
#if Input_signal
		max=0x0;
		min=0xFFFFFF;
#else
		max=0x00800001;
		min=0x007fffff;
#endif
		for(channel=0;channel<CHANNEL_NUMBER;channel++) //channel
		{
			add = channel*SINGLE_CHANNEL_EEPROM_DATA*6 + input_type * 6;
			
			offset_ee[0]=eeprom_readb(RAND_ADDR,add);
			offset_ee[1]=eeprom_readb(CURR_ADDR,0);
			offset_ee[2]=eeprom_readb(CURR_ADDR,0);
			offset_val=(offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];

			max = CompareMAX(max, offset_val);
			min = CompareMIN(min, offset_val);
		}
		max = Compare_different(max,min);
#if E1240 | E1242
		switch(input_type){
		case Voltage_mode:
			Printf("Voltage mode offset cal. parameters' max difference = 0x%x", max); 
			break;
		case Current_mode:
			Printf("Current mode offset cal. parameters' max difference = 0x%x", max); 
			break;
		default:	break;
		}
#endif
#if E1262 | E1260
		Printf("Gain X%d offset cal. parameters' max difference = 0x%x\r\n", (1 << input_type), max); 
#endif
		if(max > (150 << input_type))
		{
			Printf("** Non-uniform **\r\n");
			ret=0;
		}
		else
		{
			ret++;
		}
	}
		
	for(input_type=0;input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++)
	{

#if Input_signal
		max=0x0;
		min=0x00FFFFFF;
#else
		max=0x00800001;
		min=0x007fffff;
#endif
		for(channel=0;channel<CHANNEL_NUMBER;channel++)
		{
			add = channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6+3;
//			add = channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6;

			gain_ee[0]=eeprom_readb(RAND_ADDR,add);
			gain_ee[1]=eeprom_readb(CURR_ADDR,0);
			gain_ee[2]=eeprom_readb(CURR_ADDR,0);
			gain_val=(gain_ee[2]<<16)+(gain_ee[1]<<8)+gain_ee[0];

			max = CompareMAX(max, gain_val);
			min = CompareMIN(min, gain_val);
		}
		max = Compare_different(max,min);
#if E1240 | E1242
		switch(input_type){
		case Voltage_mode:
			Printf("Voltage mode gain   cal. parameters' max difference = 0x%x", max); 
			break;
		case Current_mode:
			Printf("Current mode gain   cal. parameters' max difference = 0x%x", max); 
			break;
		default:	break;
		}
#endif
#if E1262 | E1260
		Printf("Gain X%d gain cal. parameters' max difference = 0x%x",1<<input_type, max); 
#endif
#if E1240 | E1242
		switch(input_type){
		case Voltage_mode:
			gain_val = 0x1000;	break;
		case Current_mode:
			gain_val = 0x8000;	break;
		default:	gain_val = 0;	break;
		}
#endif
#if E1262 | E1260
//		gain_val = 0x1000;
		gain_val = 2300;
#endif
		if (max>gain_val){
			Printf(" ** Non-uniform **\r\n");
			ret=0;
		}
		else{
			Printf("\r\n");
			ret++;
		}
	}
	return ret;
}

int cal_parameters_load(UINT8 channel, UINT8 input_type)
{
	UINT8 offset_ee[3], gain_ee[3];
	UINT16 add;
	UINT32 offset_val, gain_val;
	int ret=0;

	add = channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6;

	offset_ee[0]=eeprom_readb(RAND_ADDR,add);
	offset_ee[1]=eeprom_readb(CURR_ADDR,0);
	offset_ee[2]=eeprom_readb(CURR_ADDR,0);
	gain_ee[0]=eeprom_readb(CURR_ADDR,0);
	gain_ee[1]=eeprom_readb(CURR_ADDR,0);
	gain_ee[2]=eeprom_readb(CURR_ADDR,0);
	
	offset_val=(offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];
	gain_val  =  (gain_ee[2]<<16)+  (gain_ee[1]<<8)+  gain_ee[0];

	if (offset_val == gain_val)		// new EEPROM, set initial values
		{
			ret=1;
#if E1240 | E1242
			switch(input_type)
			{
				case Voltage_mode:	
						offset_val	= 0x3500;
						gain_val	= 0x58df00;
						break;
				case Current_mode:
						offset_val	= 0x129900;
						gain_val	= 0x73c700;
						break;
				default:break;
			}
#endif
#if E1262
			switch(input_type)
			{
				case 0:	offset_val	= 0x151;
						gain_val	= 0x57CC0C;
						break;
				case 1:	offset_val	= 0x309;
						gain_val	= 0x57E831;
						break;
				case 2:	offset_val	= 0x6A3;
						gain_val	= 0x582308;
						break;
				default:break;
			}
#endif
#if E1260
			switch(input_type)
			{
				case 0:	offset_val	= 8758;
						gain_val	= 5746788;
						break;
				case 1:	offset_val	= 37848;
						gain_val	= 5775410;
						break;
				case 2:	offset_val	= 151005;
						gain_val	= 5829699;
						break;
				case 3:	offset_val	= 590291;
						gain_val	= 5946706;
						break;
				default:	break;
			}
#endif
	}
	write_parm_to_ADC(offset_val, gain_val);
	sleep(10);
	return ret;
}

extern int	diag_do_set_clean_flag(int mode,long parameter);
void cal_parameters_dump(void)
{
	UINT8 offset_ee[3], gain_ee[3];
	UINT32 offset_val, gain_val;
	UINT8 channel, input_type, digit,bank;
	UINT16 add;
	int ret;


  	if(diag_get_value("Show by Hexadecimal(0) or Decimal(1) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	digit=ret;

  	if(diag_get_value("Select parameter bank (0/1) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	bank=ret;

	switch (bank)
	{
		case 0:	Printf("\r\n\nDump Block 0: 0x000 ~ 0x%03X\r\n",TOTAL_CAL_SIZE-1);
				break;
		case 1: Printf("\r\n\nDump Block 1: 0x%03X ~ 0x%03X\r\n",BACKUP_CAL_START,BACKUP_CAL_START+TOTAL_CAL_SIZE-1);
				break;
		default:break;
	}	
#if E1240 | E1242
	Printf("AI Calibration Parameters:\r\nVolatge(Offset, Gain), Current(Offset, Gain)\r\n");
#endif
#if E1260 | E1262
	Printf("AI Calibration Parameters:\r\n");
	for(ret=0;ret < SINGLE_CHANNEL_EEPROM_DATA; ret++)
		Printf("X%d(Offset, Gain)",1<<ret);
	Printf("\r\n");
#endif
	Printf("---------------------------------------------------------------------------\r\n");

	for(channel=0;channel<CHANNEL_NUMBER;channel++)
	{
#if E1240 | E1242
		Printf("AI CH-%d: ", channel);
#endif
#if E1262
		Printf("TC CH-%d: ", channel);
#endif
#if E1260
		Printf("RTD CH-%d: ", channel);
#endif
		for(input_type=0;input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++)
		{
			add = channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6+bank*BACKUP_CAL_START;

			offset_ee[0]=eeprom_readb(RAND_ADDR,add);
			offset_ee[1]=eeprom_readb(CURR_ADDR,0);
			offset_ee[2]=eeprom_readb(CURR_ADDR,0);
			gain_ee[0]=eeprom_readb(CURR_ADDR,0);
			gain_ee[1]=eeprom_readb(CURR_ADDR,0);
			gain_ee[2]=eeprom_readb(CURR_ADDR,0);
			offset_val=(offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];
			gain_val  =  (gain_ee[2]<<16)+  (gain_ee[1]<<8)+  gain_ee[0];

			if (digit == 1)
			{
				Printf("%d, ",offset_val);
				Printf("%d, ",gain_val);
			}
			else
			{
				Printf("%06X, ",offset_val);
				Printf("%06X, ",gain_val);
			}
		}
		Printf("\r\n");
	}

	offset_ee[0]=eeprom_readb(RAND_ADDR,TEMP_OFFSET);
	offset_ee[1]=eeprom_readb(CURR_ADDR,0);
	offset_ee[2]=eeprom_readb(CURR_ADDR,0);
	gain_ee[0]=eeprom_readb(RAND_ADDR,TEMP_GAIN);
	gain_ee[1]=eeprom_readb(CURR_ADDR,0);
	gain_ee[2]=eeprom_readb(CURR_ADDR,0);
	offset_val=(offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];
	gain_val  =  (gain_ee[2]<<16)+  (gain_ee[1]<<8)+  gain_ee[0];

	Printf("---------------------------------------------------------------------------\r\n");
	if (digit == 1){
		Printf("Temperature Offset Parameter...%d\r\n", offset_val);
		Printf("Temperature Gain Parameter.....%d\r\n", gain_val);
	}
	else{
		Printf("Temperature Offset Parameter...%06X\r\n", offset_val);
		Printf("Temperature Gain Parameter.....%06X\r\n", gain_val);
	}

	Printf("Calibration Flag ..............%d\r\n", eeprom_readb(RAND_ADDR,CAL_FLAG1));
	Printf("Calibration Report Flag .......%d\r\n", eeprom_readb(RAND_ADDR,CAL_RFLAG));
	Printf("Calibration Temperature........%d\r\n", eeprom_readb(RAND_ADDR,CAL_TEMP));
	Printf("Calibration Humidity...........%d\r\n", eeprom_readb(RAND_ADDR,CAL_HUMI));
	Printf("Calibration Date...............%d-%d-%02d\r\n", eeprom_readb(RAND_ADDR,CAL_MONTH), eeprom_readb(RAND_ADDR,CAL_DAY), eeprom_readb(RAND_ADDR,CAL_YEAR));
	Printf("Calibration Time...............%d:%d:%d\r\n", eeprom_readb(RAND_ADDR,CAL_TIME), eeprom_readb(RAND_ADDR,CAL_TIME+1), eeprom_readb(RAND_ADDR,CAL_TIME+2));
	Printf("Press any key to continue\r\n");
	while(!Kbhit());
	bank=Getch();
	if(bank == 'K'){
	
		SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
		SPI_ENABLE_SLAVE0();

		if(diag_get_value("Cal flag set - 1 or clean - 0",&gain_val,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
		diag_do_set_clean_flag(gain_val,FLAG_CAL);
		i2c_write_to_eeprom(CAL_FLAG1,gain_val);

		EXSPI_INIT();	//SPI interface set to ADC
	}
	else if(bank == 'R'){

		SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
		SPI_ENABLE_SLAVE0();

		if(diag_get_value("Report flag set - 1 or clean - 0",&gain_val,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
		diag_do_set_clean_flag(gain_val,FLAG_REPORT);
		i2c_write_to_eeprom(CAL_RFLAG,gain_val);

		EXSPI_INIT();	//SPI interface set to ADC
	}		
}

void diag_cal_parameters_input(void)
{
	UINT8 channel, input_type;
	UINT32 ret=8;

  	if(diag_get_value("Select channel => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	channel=ret;

#if E1240 | E1242
	if(diag_get_value("Select Volatge(0), Current(1) ) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1262
	if(diag_get_value("Select Gain (0) X1, (1) X2, (2) X4 ) => ",&ret,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1260
	if(diag_get_value("Select Gain (0) X1, (1) X2, (2) X4 (3) X8 ) => ",&ret,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
	input_type=ret;

	cal_parameters_input(channel, input_type);
}

void diag_cal_parameters_duplicate(void)
{
	UINT8 source_channel, target_channel;
	UINT32 ret;

  	if(diag_get_value("Select channel => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	source_channel=ret;

  	if(diag_get_value("Select target channel => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	target_channel=ret;

	cal_parameters_duplicate(source_channel, target_channel);
}

void diag_cal_parameters_block_duplicate(void)
{
	UINT8 source_block, target_block;
	UINT32 ret;

  	if(diag_get_value("Select source block (0 ~ 1) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	source_block=ret;

  	if(diag_get_value("Select target block (0 ~ 1) => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
   	target_block=ret;

	cal_parameters_block_duplicate(source_block, target_block);
}

/*
 *	subroutine cal_parameters_duplicate
 *	function :
 *	    duplicate the calibration parameters from source channel to target channel
 *	input :
 *	    source_channel --> source channel with calibration parameters
 *	    target_channel --> target channel to duplicate calibration parameters
 *	output :
 *	    none
 */

void cal_parameters_duplicate(UINT8 source_channel, UINT8 target_channel)
{
	UINT8 offset_ee[3], gain_ee[3];
	UINT16 add;
	int input_type;

	for (input_type=0;input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++)
	{
		add = source_channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6;
		offset_ee[0]=eeprom_readb(RAND_ADDR,add);
		offset_ee[1]=eeprom_readb(CURR_ADDR,0);
		offset_ee[2]=eeprom_readb(CURR_ADDR,0);
		gain_ee[0]=eeprom_readb(CURR_ADDR,0);
		gain_ee[1]=eeprom_readb(CURR_ADDR,0);
		gain_ee[2]=eeprom_readb(CURR_ADDR,0);

		add = target_channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6;
		i2c_write_to_eeprom(add,offset_ee[0]);
		i2c_write_to_eeprom(add+1,offset_ee[1]);
		i2c_write_to_eeprom(add+2,offset_ee[2]);
		i2c_write_to_eeprom(add+3,gain_ee[0]);
		i2c_write_to_eeprom(add+4,gain_ee[1]);
		i2c_write_to_eeprom(add+5,gain_ee[2]);
	}
}

UINT8 hex2num(char hex)
{
	UINT8 num;

	if (hex>=48 && hex<=57)
		num=hex-48;
	else if (hex>=65 && hex<=70)
		num=hex-55;
	else if (hex>=97 && hex<=102)
		num=hex-87;
	else
		num=16;

	return num;
}

extern int ParseCmd(char* Buffer);

void cal_parameters_input(UINT8 channel, UINT8 input_type)
{
	UINT8 add;
	int i,j,k, ret;
	UINT32 value=0, offset_reg, gain_reg;

	add = channel*SINGLE_CHANNEL_EEPROM_DATA*6+input_type*6;

	Printf("\r\nNew Offset register value (HEX 24-bit/6-digit) is : 0x"); 
    while(1){
		ConGetString(ErrMsg,sizeof(ErrMsg));
		ret = ParseCmd(ErrMsg);
		for(i=0;i<6;i++)
		{
			k=hex2num(ErrMsg[i]);
			for (j=0;j<(5-i);j++)
				k*=16;
			value+=k;
		}
		offset_reg=value; 
		Printf("\r\nOffset value=%d", value);
		if (ret == -1)
			Printf("\r\n");
		break;
	}   

	value=0;
	Printf("\r\nNew Gain register value (HEX 24-bit/6-digit) is : 0x"); 
    while(1){
		ConGetString(ErrMsg,sizeof(ErrMsg));
		ret = ParseCmd(ErrMsg);
		for(i=0;i<6;i++)
		{
			k=hex2num(ErrMsg[i]);
			for (j=0;j<(5-i);j++)
				k*=16;
			value+=k;
		}
		gain_reg=value; 
		Printf("\r\nGain value=%d", value);
		if (ret == -1)
			Printf("\r\n");
		break;
	}   
	cal_parameters_save(channel, input_type, offset_reg, gain_reg);
}

void cal_parameters_block_duplicate(UINT8 source_block, UINT8 target_block)
{
	UINT8 para;
	UINT16 add;
	int i;

	Printf("\r\n");
	for (i=0;i<TOTAL_CAL_SIZE+6;i++)	// CAL_SIZE+6 ->6 is temperature sensor cal parm.
	{
		Printf("Process : %d%\r", i*100/TOTAL_CAL_SIZE);
		add = source_block*BACKUP_CAL_START+i;
		para=eeprom_readb(RAND_ADDR,add);

		add = target_block*BACKUP_CAL_START+i;
		i2c_write_to_eeprom(add,para);
	}
	Printf("Process : 100%\r\n");
}

void write_parm_to_ADC(UINT32 offset_tmp, UINT32 gain_tmp){
	UINT32 command;

	command = 0x005a0000 | (offset_tmp & 0x000000ff);
	SPI_Tx(PIO_ADC_CS,command,24);
	command = 0x005b0000 | ((offset_tmp & 0x0000ff00)>>8);
	SPI_Tx(PIO_ADC_CS,command,24);
	command = 0x005c0000 | ((offset_tmp & 0x00ff0000)>>16);
	SPI_Tx(PIO_ADC_CS,command,24);
	command = 0x005d0000 | (gain_tmp & 0x000000ff);
	SPI_Tx(PIO_ADC_CS,command,24);
	command = 0x005e0000 | ((gain_tmp & 0x0000ff00)>>8);
	SPI_Tx(PIO_ADC_CS,command,24);
	command = 0x005f0000 | ((gain_tmp & 0x00ff0000)>>16);
	SPI_Tx(PIO_ADC_CS,command,24);
}
/******************************************************/
/*                 Temperature                        */
/******************************************************/
#if E1262
void CJC_temperature_print(UINT32 value){
	UINT32 temperature,temperature_floating;

	temperature = (value - 1275068)/21642.6086;	//1275068 = -20'C
	if(value < 1686278){
		temperature_floating = ((temperature+1) * 100) -(value - 1275068)/216.426086;
		Printf("%d.%02d'C",temperature-19 ,temperature_floating );
	}else if(value < 1707921){
		temperature_floating = ((temperature+1) * 100) -(value - 1275068)/216.426086;
		Printf("-%d.%02d'C",temperature-19 ,temperature_floating );
	}else{
		temperature_floating = (value - 1275068)/216.426086 - (temperature * 100);
		Printf("%d.%02d'C",temperature-20 ,temperature_floating );
	}
}
#endif

void module_temperature_monitor(void)
{
	UINT32 ADR03_value = 0;
	UINT32 ADR03_sum = 0;
	int ADR03_loop=0;
#if E1262
	UINT32 Left_value=0,Right_value=0;
	UINT32 Left_sum =0,Right_sum=0;
	int Left_loop=0,Right_loop=0;
	UINT8 temp = 0;
#endif
	UINT8 offset_ee[3],gain_ee[3];
	UINT32 offset_val,gain_val;
	UINT8 sample=0;
	int i;
	UINT32 temperature,temperature_floating;

	Printf("\r\n<<< Temperature Monitoring.... >>>\r\n");

	ADC_CH_Select(ADR03_TEMP, AnalogCom);					// temperature input from ADR03
	ADC_Filter_Select(ADC_FILTER_SINC_3);
	ADC_PGA_Gain(ADC_PGA_GAIN_1);

	//Read temperature sensor gain offset
	offset_ee[0]=eeprom_readb(RAND_ADDR,TEMP_OFFSET);	
	offset_ee[1]=eeprom_readb(CURR_ADDR,0);
	offset_ee[2]=eeprom_readb(CURR_ADDR,0);
	gain_ee[0]=eeprom_readb(RAND_ADDR,TEMP_GAIN);
	gain_ee[1]=eeprom_readb(CURR_ADDR,0);
	gain_ee[2]=eeprom_readb(CURR_ADDR,0);

	offset_val=(offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];
	gain_val  =  (gain_ee[2]<<16)+  (gain_ee[1]<<8)+  gain_ee[0];
	sleep(10);
	if(offset_val != gain_val){	//New EERPOM
		write_parm_to_ADC(offset_val, gain_val);
		sleep(10);
	}

	ADC_Data_Sync();
	i=0;
	while(!ADC_Data_Ready());
	ADC_Read();

	i=0;
	do{
		do{
			while(!ADC_Data_Ready());
#if E1262
			switch(temp){
			case 0:
#endif
				ADR03_value = ADC_Read() & 0xffffff;	
#if E1262
				break;
			case 1:
				Left_value = ADC_Read() & 0xffffff;	break;
			case 2:
				Right_value = ADC_Read() & 0xffffff;	break;
			default:	break;
			}
#endif
			i++;
		}while(i<=4);
#if E1262
		switch(temp){
		case 0:
#endif
			ADR03_sum += ADR03_value;
			ADR03_loop++;
#if E1262
			ADC_CH_Select(CJC_L, AnalogCom);					// temperature input from Left
			temp =1;
			break;
		case 1:
			Left_sum += Left_value;
			Left_loop++;
			ADC_CH_Select(CJC_R, AnalogCom);					// temperature input from Right
			temp = 2;
			break;
		case 2:
			Right_sum += Right_value;
			Right_loop++;
			ADC_CH_Select(ADR03_TEMP, AnalogCom);					// temperature input from Right
			temp = 0;
			break;
		default:	break;
		}
#endif
		sample = 64;
#if E1262
		sample = 16;
#endif
		Printf("\r %d-sampling board temperature >>> ",sample);
		if (ADR03_loop >= sample)
		{
			ADR03_value = ADR03_sum/ADR03_loop;
#if Input_signal			
			temperature = (ADR03_value - 3099086)/13153.3374;	//1549543 = -20'C unipolar
#else
			temperature = (ADR03_value - 1549543)/6576.6687;	//1549543 = -20'C bipolar
#endif
#if Input_signal
			if(ADR03_value < 3349000){ // unipolar
				temperature_floating = ((temperature+1) * 100) - (ADR03_value - 3099086)/131.533374; // unipolar
				Printf("%d.%02d'C\r\n",temperature-19 ,temperature_floating );
			}else if(ADR03_value < 3362154){ // unipolar
				temperature_floating = ((temperature+1) * 100) - (ADR03_value - 3099086)/131.533374; // unipolar
				Printf("-%d.%02d'C\r\n",temperature-19 ,temperature_floating );
			}
			else{
				temperature_floating = (ADR03_value - 3099086)/131.533374 - (temperature * 100); // unipolar
				Printf("%d.%02d'C\r\n",temperature-20 ,temperature_floating );
			}
#else
			if(ADR03_value < 1674500){ // bipolar
				temperature_floating = ((temperature+1) * 100) - (ADR03_value - 1549543)/65.766687;	// bipolar
				Printf("%d.%02d'C\r\n",temperature-19 ,temperature_floating );
			}else if(ADR03_value < 1681077){ // bipolar
				temperature_floating = ((temperature+1) * 100) - (ADR03_value - 1549543)/65.766687; // bipolar
				temperature_floating = (ADR03_value - 1549543)/65.766687 - (temperature * 100); // bipolar
			}else{
				temperature_floating = (ADR03_value - 1549543)/65.766687 - (temperature * 100); // bipolar
				Printf("%d.%02d'C\r\n",temperature-20 ,temperature_floating );
			}
#endif
#if E1262
			CJC_temperature_print(Right_value);
			Printf(",");
			CJC_temperature_print(Left_value);
			Printf("\r\n");
			Left_loop=0; Right_loop=0;
			Left_sum=0;Right_sum=0;
#endif
			ADR03_loop=0;
			ADR03_sum=0;
			i=0;
		}
		sleep(30);
	}while(!Kbhit());
	Getch();
	ADC_CH_Select(AnalogP,AnalogN);
}

/******************************************************/
/*    Read 16 bytes from RAM of ADC ADS1216           */
/******************************************************/
void ADC_RAM_Readback(UINT8 bank, UINT8 *ram)
{
	UINT32 i;
	UINT16 cmd_read_reg = 0x200f;

	cmd_read_reg |= (bank << 8); 
	
	SetSPI_Len(15);
	GpioWriteData(PIO_ADC_CS,0);
	sleep(1);
	SPI_WRITE_DATA(cmd_read_reg);
	while(SPI_BUSY());

	SetSPI_Len(7);
	for(i=0;i<16;i++)
	{
		SPI_WRITE_DATA(0xff);
		while(SPI_BUSY());
		ram[i] = SPI_ReadFIFO();
	}
	GpioWriteData(PIO_ADC_CS,1);
	sleep(1);
}

void diag_AI_ADC_Memory_dump(void)
{
	UINT8 ram[16];
	int i, j;

	Printf("\r\nDump Registers  16 bytes >> ");
	for(i=0; i<16; i++)					
		Printf("%02X ", ADC_Register_Readback(i));
	Printf("\r\n");

	for(i=0; i<8; i++)
	{
		ADC_RAM_Readback(i, ram);
		Printf("Dump RAM bank-%d 16 bytes >> ",i);
		for(j=0; j<16; j++)					
			Printf("%02X ", ram[j]);
		Printf("\r\n");
	}
}

void copy_ADC_REG_to_RAM(UINT8 bank)
{
	UINT32 cmd_set_reg;

	cmd_set_reg = 0x40 | (UINT32)bank;
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,8);
	sleep(1);							// at least 45us delay 
}

void copy_ADC_RAM_to_REG(UINT8 bank)
{
	UINT32 cmd_set_reg;

	cmd_set_reg = 0xc0 | (UINT32)bank;
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,8);
	sleep(1);							// at least 45us delay 
}

void ADC_RAM_Write_Bank(UINT8 bank, UINT32 offset, UINT32 gain, UINT8 PGA){
	UINT32 i;
	UINT8 ram[16];
	UINT16 cmd_read_reg = 0x600f;
	cmd_read_reg |= (bank << 8); 
	ADC_RAM_Readback(bank, ram);

	ram[2] = PGA;	
	ram[12] = offset >> 16;
	ram[11] = ( offset >> 8 ) & 0xff;
	ram[10] = offset & 0xff;
	ram[15] = gain >> 16;
	ram[14] = ( gain >> 8 ) & 0xff;
	ram[13] = gain & 0xff;

	SetSPI_Len(15);
	GpioWriteData(PIO_ADC_CS,0);
	sleep(1);
	SPI_WRITE_DATA(cmd_read_reg);
	while(SPI_BUSY());

	SetSPI_Len(7);
	for(i=0;i<16;i++)
	{
		SPI_WRITE_DATA(ram[i]);
		while(SPI_BUSY());
	}
	GpioWriteData(PIO_ADC_CS,1);
	sleep(1);
}


/******************************************************/
/*                      AI test                       */
/******************************************************/

/*** 30-Samples test ***/
void diag_do_AI_test(void)
{
	UINT32 value = 0, raw_value=0;
	UINT8  gain;
	UINT32 integer=0, floating=0;
	uchar key;
	int i;
#if E1240 | E1242
	Printf("\r\n<<< AI Sampling Test >>>\r\n");
	
	if(diag_get_value("Input type: (0) Voltage (1) Current : ",&value,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	switch(value){
	case Voltage_mode:
		gain = 0;	break;
	case Current_mode:
		gain = 2;	break;
	default:
		gain = 0;	break;
	}
#endif
#if E1262
	Printf("\r\n<<< TC Sampling Test >>>\r\n");
	
	if(diag_get_value("Gain : (0) X1 (1) X2 (2) X4 : ",&value,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	gain = value;
#endif
#if E1260
	Printf("\r\n<<< RTD Sampling Test >>>\r\n");
	
	if(diag_get_value("Gain : (0) X1 (1) X2 (2) X4 (3) X8: ",&value,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	gain = value;
#endif
	ADC_PGA_Gain(gain);

	do{
		ADC_Data_Sync();
		i=0;
		while(!ADC_Data_Ready());
		do{
			value = ADC_Read() & 0xFFFFFF;
			raw_value = value;
#if (Input_signal == 0)
			if(value & 0x800000)
				value = 0x1000000 - value;
#endif

			if (i < 3)
				Printf("Data ignored %d... : Raw data : %06X\r\n",i+1, value);
			else
			{
				value = AI_data_transfer(value, gain);
				integer = value/1000;
				floating = value%1000;

#if E1240 | E1242
				if(gain == 0)
					Printf("Data valid   %d... : Raw data : %d, (V) = %d.%03d\r\n",i-2, raw_value, integer, floating);
				else
					Printf("Data valid   %d... : Raw data : %d, (mA) = %d.%03d\r\n",i-2, raw_value, integer, floating);
#endif
#if E1262
				if(raw_value & 0x800000){
					raw_value = 0xff000000 | raw_value;
					Printf("Data valid   %d... : Raw data : %d, (mV) = -%d.%03d\r\n",i-2, raw_value, integer, floating);
				}
				else
					Printf("Data valid   %d... : Raw data : %d, (mV) = %d.%03d\r\n",i-2, raw_value, integer, floating);
#endif
#if E1260
				Printf("Data valid   %d... : Raw data : %d, (Ohm) = %d.%03d\r\n",i-2, raw_value, integer, floating);
#endif
			}
			while(!ADC_Data_Ready());
			i++;
		}while(i<23);
		Printf("Press 'q' to stop, other key to repeat!");
		key = Getch();
		Printf("\r\n");
	}while((key != 'q') && (key != 'Q'));
}

/*** 128 sample noise test ***/
/*** Kbhit = '1' by data average ***/

void diag_do_noise_loop_test(void)
{
	UINT32 value = 0;
	UINT32 RAW_data,sum=0;
	UINT32 pvalue = 0,pvar = 0;
	UINT32 max=0x0, min=0xFFFFFF, var=0;
	int i,j=0;
	UINT8 select_gain=0,ch=0;
	int dio_ai_noise_test=0;
	
#if E1240 | E1242
	Printf("\r\n<<< 128-samples AI Loop Test >>>\r\n");
#endif
#if E1262
	Printf("\r\n<<< 128-samples TC Loop Test >>>\r\n");
#endif
#if E1260
	Printf("\r\n<<< 128-samples RTD Loop Test >>>\r\n");
#endif

	if(diag_get_value("Data Format: (0) Raw data (1) Data transfer : ",&RAW_data,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	
#if E1240 | E1242
	if(diag_get_value("Input Type: (0) Voltage (1) Current : ",&value,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1242
	if(diag_get_value("DIO V.S AI noise test: (0) Disable (1) Enable : ",&dio_ai_noise_test,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1262
	if(diag_get_value("Gain : (0) X1 (1) X2 (2) X4 : ",&value,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1260
	if(diag_get_value("Gain : (0) X1 (1) X2 (2) X4 (3) X8 : ",&value,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
	ADC_Data_Sync();

	i=0;
	while(!ADC_Data_Ready());
#if E1240 | E1242
	if(value == Voltage_mode)
		select_gain = 0;
	else
		select_gain = 2;
#endif
#if E1262 | E1260
	select_gain = value;
#endif

#if Input_signal 
	max = 0;
	min = 0xffffff;
#else
	max = 0x800000;
	min = 0x7fffff;
#endif
	ADC_PGA_Gain(select_gain);
	sleep(100);
	do{
		if (dio_ai_noise_test == 1){	// test DIO v.s AI noise
//			diag_do_DIO_AI_noise_test();
		}
		
		value = ADC_Read() & 0xFFFFFF;
		if (i == 4){
			sum += value;
#if E1240 | E1242
			switch(select_gain){
			case 0:
				Printf("Voltage mode, ");	break;
			case 2:
				Printf("Current mode, ");	break;
			default:	break;
			}

#endif
#if E1262 | E1260
			Printf("Gain = %d, ", 0x1 << select_gain);
#endif
			if(j == 127)
				Printf("(average,max,min,var) = (");
			else
				Printf("(value,max,min,var) = (");
			max = CompareMAX(max,value);
			min = CompareMIN(min,value);

			if(j == 127)
				value = sum >> 7 ;

			if(RAW_data){
#if (Input_signal == 0)
				if(value & 0x800000){
					pvalue = AI_data_transfer((0x1000000-value),select_gain);
					Printf("-%02d.%03d,",pvalue/1000,pvalue%1000);
				}
				else{
#endif
					pvalue = AI_data_transfer(value,select_gain);
					Printf(" %02d.%03d,",pvalue/1000,pvalue%1000);
#if (Input_signal == 0)
				}
#endif
			}
			else{
#if (Input_signal == 0)
				if(value & 0x800000)
					pvalue = value | 0xff000000;
				else
#endif	
					pvalue = value;
				Printf("%d,",pvalue);
			}

			if(RAW_data){
#if (Input_signal == 0)
				if(value & 0x800000){
					pvalue = AI_data_transfer((0x1000000-max),select_gain);
					Printf("-%02d.%03d,",pvalue/1000,pvalue%1000);
				}
				else{
#endif
					pvalue = AI_data_transfer(max,select_gain);
					Printf(" %02d.%03d,",pvalue/1000,pvalue%1000);
#if (Input_signal == 0)
				}
#endif
				pvar = pvalue;
			}
			else{
#if (Input_signal == 0)
				if(max & 0x800000)
					pvalue = max | 0xff000000;
				else
#endif	
					pvalue = max;

				Printf("%d,",pvalue);
			}
#if (Input_signal == 0)
			if(min & 0x800000)
					pvalue = min | 0xff000000;
				else
#endif	
			pvalue = min;
			if(RAW_data){
#if (Input_signal == 0)
				if(value & 0x800000){
					pvalue = AI_data_transfer((0x1000000-min),select_gain);
					Printf("-%02d.%03d,",pvalue/1000,pvalue%1000);
				}
				else{
#endif
					pvalue = AI_data_transfer(min,select_gain);
					Printf(" %02d.%03d, ",pvalue/1000,pvalue%1000);
#if (Input_signal == 0)
				}
#endif
				if(pvar > pvalue)
					pvar = pvar - pvalue;
				else
					pvar = pvalue - pvar;
			}
			else
				Printf("%d, ",pvalue);

			if(j == 127){
				if(RAW_data)
					Printf("%d.%03d)\r\n",pvar/1000,pvar%1000);
				else
					Printf("%d)\r\n",var);
				j=0; sum =0; var = 0; 
#if Input_signal
				max=0; min=0xFFFFFF; 
#else
				max=0x800001; min=0x7FFFFF; 
#endif
			}
			else
				j++;
			
			if(Compare_different(max,min) > var){
				var = Compare_different(max,min);
				if(RAW_data)
					Printf("%d.%03d) \r",pvar/1000,pvar%1000);
				else
					Printf("%d) \r",var);
			}
			else
				Printf("\r");
		}
		while(!ADC_Data_Ready());
		if (i < 4) i++;

		if(Kbhit())
		ch = Getch();
	}while(ch != 27);
#if E1240 | E1242
	switch(select_gain){
	case 0:
		Printf("Voltage mode, ");	break;
	case 2:
		Printf("Current mode, ");	break;
	default:	break;
	}
#endif
#if E1262 | E1260
	Printf("Gain = %d, ", 0x1 << select_gain);
#endif

	Printf("(value,max,min,var) = (");

#if (Input_signal == 0)
	if(value & 0x800000)
		pvalue = value | 0xff000000;
	else
#endif	
		pvalue = value;

	if(RAW_data){
#if (Input_signal == 0)
		if(value & 0x800000){
			pvalue = AI_data_transfer((0x1000000-value),select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);
		}
		else{
#endif
			pvalue = AI_data_transfer(value,select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);
#if (Input_signal == 0)
		}
#endif
	}
	else
		Printf("%d,",pvalue);

#if (Input_signal == 0)
	if(max & 0x800000)
		pvalue = max | 0xff000000;
	else
#endif	
		pvalue = max;

	if(RAW_data){
#if (Input_signal == 0)
		if(max & 0x800000){
			pvalue = AI_data_transfer((0x1000000-max),select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);
		}
		else{
#endif
			pvalue = AI_data_transfer(max,select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);

#if (Input_signal == 0)
		}
#endif
		pvar = pvalue;
	}
	else
		Printf("%d,",pvalue);

#if (Input_signal == 0)
	if(min & 0x800000)
		pvalue = min | 0xff000000;
	else
#endif
		pvalue = min;

	if(RAW_data){
#if (Input_signal == 0)
		if(min & 0x800000){
			pvalue = AI_data_transfer((0x1000000-min),select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);
		}
		else{
#endif
			pvalue = AI_data_transfer(min,select_gain);
			Printf("%d.%03d,",pvalue/1000,pvalue%1000);
#if (Input_signal == 0)
		}
#endif
		if(pvar > pvalue)
			pvar = pvar - pvalue;
		else
			pvar = pvalue - pvar;
	}
	else
		Printf("%d,",pvalue);
	var = Compare_different(max,min);
	if(RAW_data)
		Printf("%d.%03d)\r\n",pvar/1000,pvar%1000);
	else
		Printf("%d)\r\n",var);
}

/*** 1-mins all channel scan test ***/
void diag_do_AI_scan_test(void){
	UINT32 value = 0,pvalue;
	int dio_ai_noise_test=0;
	
#if E1240 | E1242
	UINT8 DO_DATA=0x55;
		int DI_Read_data[DI_channel],t=0;
	UINT8	DI_Read=0,DI_Read_temp=0;


	UINT32 max=0x0, min=0x00ffffff, var=0;
	UINT8 Input_det[CHANNEL_NUMBER],Input;
	UINT32 gain[CHANNEL_NUMBER][SINGLE_CHANNEL_EEPROM_DATA],offset[CHANNEL_NUMBER][SINGLE_CHANNEL_EEPROM_DATA];
	UINT8 temp_ee[3],add;
#endif
#if E1262
	UINT32 max=0x800000, min=0x007fffff, var=0,BO;
	UINT8 Burn_det[CHANNEL_NUMBER];
#endif
#if E1260
	UINT32 max=0x0, min=0x00ffffff, var=0;
#endif
	UINT8 ch=0,channel=0,c=0;
	UINT32 value_ch[CHANNEL_NUMBER];	
	UINT8 Gain_array[CHANNEL_NUMBER];
	int i,j,count[CHANNEL_NUMBER];
#if E1240 | E1242
	Printf("\r\n<<< AI Scan Test >>>\r\n");
#endif

#if E1242
	if(diag_get_value("DIO V.S AI noise test: (0) Disable (1) Enable : ",&dio_ai_noise_test,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1262
	Printf("\r\n<<< TC Scan Test >>>\r\n");
#endif
#if E1260
	Printf("\r\n<<< RTD Scan Test >>>\r\n");
#endif

	for(i=0 ; i<CHANNEL_NUMBER ; i++){
		value_ch[i]=0;
		Gain_array[i]=0;
	}
#if E1262
	for(i=0; i<CHANNEL_NUMBER; i++)
	{
		Printf("CH-%d ",i);
		if(diag_get_value("Gain set to .. (0:X1 / 1:X2 / 2:X4) => ",&value,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
		Gain_array[i] = (UINT8)value;
	}
#endif
#if E1260
	for(i=0; i<CHANNEL_NUMBER; i++)
	{
		Printf("CH-%d ",i);
		if(diag_get_value("Gain set to .. (0:X1 / 1:X2 / 2:X4 / 3:X8) => ",&value,0,3,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
		Gain_array[i] = (UINT8)value;
//		Printf ("\r\nGain_array[%d] = %d\r\n",Gain_array[i]);
	}
#endif
#if E1262
	Burnout_enable(); //enable
#endif

#if E1240 | E1242
	Printf("Please Check Channels input type\r\n");
	Printf("Press any key to continue! \r\n");
	while(!Kbhit());
	Getch();
	for(i=0 ; i < CHANNEL_NUMBER ; i++){
		AI_TYPE_CH_Select(i);
		sleep(1);
		if(AI_TYPE_READ() == Voltage_mode){
			Printf("CH-%d => Voltage mode\r\n",i);
			Gain_array[i] = 0;
			Input_det[i] = Voltage_mode;
		}
		else{
			Printf("CH-%d => Current mode\r\n",i);
			Gain_array[i] = 2;
			Input_det[i] = Current_mode;
		}
	}
#endif
	sleep(1);
	for(i=0; i<CHANNEL_NUMBER ; i++)			// build-up the channel parameters' table in RAM
	{
#if	E1240 | E1242
		add = i*SINGLE_CHANNEL_EEPROM_DATA*6;
		temp_ee[0] = eeprom_readb(RAND_ADDR,add);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		offset[i][0] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];

		temp_ee[0] = eeprom_readb(CURR_ADDR,0);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		gain[i][0] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];

		add = i*SINGLE_CHANNEL_EEPROM_DATA*6+6;
		temp_ee[0] = eeprom_readb(RAND_ADDR,add);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		offset[i][1] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];

		temp_ee[0] = eeprom_readb(CURR_ADDR,0);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		gain[i][1] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];
#endif
		AI_CH_Select(i);
#if E1240 | E1242
		if(i==(CHANNEL_NUMBER-1))
			AI_TYPE_CH_Select(0);
		else
			AI_TYPE_CH_Select(i+1);
#endif

		ADC_PGA_Gain(Gain_array[i]);
		copy_ADC_REG_to_RAM(i);
	}
	sleep(1);
	copy_ADC_RAM_to_REG(0); //start from RTD AI channel0
	sleep(1);
	ADC_Data_Sync();
	while(!ADC_Data_Ready());

	sleep(100);
	j=0;
	for(i=0; i<CHANNEL_NUMBER; i++){
		count[i]=0;
		value_ch[i]=0;
#if E1262
		Burn_det[i]=0;
#endif
	}
#if E1260
	for(i=0;i<CHANNEL_NUMBER;i++) Printf("**********");
	Printf("\r");
	for(i=0;i<CHANNEL_NUMBER;i++) Printf("[  CH-%d  ]",i);
	Printf("\r\n");
#else
	for(i=0;i<CHANNEL_NUMBER;i++) Printf("*********");
	Printf("\r");
	for(i=0;i<CHANNEL_NUMBER;i++) Printf("[  CH%d  ]",i);
	Printf("\r\n");
#endif
	i=0;

	do{

//		if( count[CHANNEL_NUMBER-1] > 127 ){
		if( j > 127 ){
			for(i=0;i<CHANNEL_NUMBER;i++){
#if E1262 | E1260
#if E1262
				if(Burn_det[i])
					Printf("[Burnout]");
#else
				if(value == 0xFFFFFF)
					Printf("[Burn-out]");
#endif
				else{
#endif
					value = value_ch[i]/count[i];
#if (Input_signal == 0)
					if(value & 0x800000){
						value = 0xffffffff - value_ch[i] + 1;
						value /= count[i];
						pvalue = AI_data_transfer(value,Gain_array[i]);
						Printf("[-%02d.%03d]",pvalue/1000,pvalue%1000);
					}else{
#endif
						pvalue = AI_data_transfer(value,Gain_array[i]);
#if E1260
						Printf("[ %02d.%03d]",pvalue/1000,pvalue%1000);
#else
						Printf("[%04d.%03d]",pvalue/1000,pvalue%1000);
					
#endif
#if (Input_signal == 0)
					}
#endif
#if E1262 | E1260
				}
#endif
			}
			Printf("\r\n");

			Printf("CH-%d, Count = %d\r\n",channel,count[channel]);
			Printf("(Average,Max,Min,Var) = (");
			value = value_ch[channel]/count[channel];
#if Input_signal
			Printf("%d,%d,%d,%d)\r\n",value,max,min,var);
#else
			if(value & 0x800000){
				value = 0xffffffff - value_ch[channel] + 1;
				value /= count[channel];
				Printf("-%d,",value);
			}else
				Printf("%d,",value);

			if(max & 0x800000)
				value = max | 0xff000000;
			else	value = max;
			Printf("%d,",value);

			if(min & 0x800000)
				value = min | 0xff000000;
			else	value = min;
			Printf("%d,%d)\r\n",value,var);
#endif
#if E1260
			for(i=0;i<CHANNEL_NUMBER;i++) Printf("**********");
			Printf("\r");
			for(i=0;i<CHANNEL_NUMBER;i++) Printf("[  CH-%d  ]",i);
			Printf("\r\n");
#else
			for(i=0;i<CHANNEL_NUMBER;i++) Printf("*********");
			Printf("\r");
			for(i=0;i<CHANNEL_NUMBER;i++) Printf("[  CH%d  ]",i);
			Printf("\r\n");
#endif

#if Input_signal
			max=0x0; min=0xFFFFFF; 
#else
			max=0x800001; min=0x7FFFFF; 
#endif
			var=0; j=0; 
			for(i=0;i<CHANNEL_NUMBER;i++){
				count[i]=0;
				value_ch[i]=0;
			}
			i=0;
			ADC_Data_Sync();
			GpioWriteData(PIO_SW_READY,1);
		}
		else{		// AI scan less than 127 times
			
#if E1242
			if (dio_ai_noise_test == 1){
				diag_do_DIO_AI_noise_test();
			}
#endif
			value = ADC_Read() & 0xFFFFFF;
			if (i == scan_rate)
			{
#if E1262 | E1260
#if E1262
				if(Burn_det[ch])
					Printf("[Burnout]");
#else
				if(value == 0xFFFFFF)
					Printf("[Burn-out]");
#endif					
				else{				
#endif
#if (Input_signal == 0)
					if(value & 0x800000){
						pvalue = AI_data_transfer((0x1000000-value),Gain_array[ch]);
						Printf("[-%02d.%03d]",pvalue/1000,pvalue%1000);
						pvalue = value | 0xff000000;
						value_ch[ch] += pvalue;
					}
					else{
#endif
						pvalue = AI_data_transfer(value,Gain_array[ch]);
#if E1260
						Printf("[%04d.%03d]",pvalue/1000,pvalue%1000);
#else
						Printf("[ %02d.%03d]",pvalue/1000,pvalue%1000);
#endif
						value_ch[ch] += value;
#if (Input_signal == 0)
					}
#endif
					count[ch]++;
					if(ch == channel){

						max=CompareMAX(value,max);
						min=CompareMIN(value,min);
						pvalue = Compare_different(max,min);
						if(pvalue > var)	var = pvalue;
					}
#if E1262 | E1260
				}
#endif
			}

			while(!ADC_Data_Ready());

			if (i <= (scan_rate-1)) i++;
			else {
				if ( ch == (CHANNEL_NUMBER - 1)) j++;
				i = 0;
				if(ch < (CHANNEL_NUMBER-1))
					ch ++;
				else{ 
					ch = 0;
					Printf("\r");
				}
				copy_ADC_RAM_to_REG(ch);
				
#if E1262
				sleep(10);
				BO = Burnout_disable();
				if (BO)
					Burn_det[ch]=1;
				else
					Burn_det[ch]=0;
#endif
#if E1240 | E1242
	//			sleep(1);	// for test
				Input = AI_TYPE_READ(); 
				if(ch == (CHANNEL_NUMBER-1)){
					if(Input != Input_det[0]){
						Input_det[0] = Input;
						if(Input == Voltage_mode)
							Gain_array[0] = ADC_PGA_GAIN_1;
						else
							Gain_array[0] = ADC_PGA_GAIN_4;
						ADC_RAM_Write_Bank(0, offset[0][Input],gain[0][Input],Gain_array[0]);
					}
				}
				else{
					if(Input != Input_det[ch+1]){
						Input_det[ch+1] = Input;
						if(Input == Voltage_mode)
							Gain_array[ch+1] = ADC_PGA_GAIN_1;
						else
							Gain_array[ch+1] = ADC_PGA_GAIN_4;
						ADC_RAM_Write_Bank(ch+1, offset[ch+1][Input],gain[ch+1][Input],Gain_array[ch+1]);
					}
				}
#endif
			}
		}
		if(Kbhit()){
			c = Getch();
			if(c > 0x29 && c < 0x38){
				channel = c - 0x30;
#if Input_signal
				max=0x0; min=0xFFFFFF; var=0;
#else
				max=0x800001; min=0x7FFFFF; var=0;
#endif
			}
		}
//	}while(!Kbhit());
	}while(c != 27);
	Current_AI_Channel = (ADC_Register_Readback(6) & 0xf0) >> 4;
	return;
}

/*** Input type scan test ***/
#if E1240 | E1242
void diag_do_AI_input_scan_test(void){
	int j;
	UINT8 ch;

	for(j=0;j<CHANNEL_NUMBER;j++)	Printf("[  CH%d  ]",j);
	Printf("\r\n");
	j=0;
	do{
		AI_TYPE_CH_Select(j);
		AI_CH_Select(j);
		sleep(10);
		if(AI_TYPE_READ() == Voltage_mode){
			Printf("[Voltage]");
		}
		else{
			Printf("[Current]");
		}
		sleep(70);
		if (++j == CHANNEL_NUMBER){
			j=0;
			Printf("\r");
		}
		ch = 0;
		if(Kbhit())
			ch = Getch();
	}while(ch !=27);
}
#endif
#if E1262
void diag_do_TC_burn_out_detection_test(void){
	int j;
	UINT8 ch;

	for(j=0;j<CHANNEL_NUMBER;j++)	Printf("[  CH%d  ]",j);
	Printf("\r\n");
	j=0;
	do{
		AI_CH_Select(j);
		Burnout_enable(); //enable
		sleep(10);
		if (Burnout_disable())
			Printf("[Burnout]");
		else
			Printf("[       ]");

		sleep(70);
		if (++j == 8){
			j=0;
			Printf("\r");
		}
		ch = 0;
		if(Kbhit())
			ch = Getch();
	}while(ch !=27);
	Burnout_disable();
}
#endif

#if E1260
void diag_do_RTD_burn_out_detection_test(void){
	int j;
	UINT8 ch;
	UINT32 value;

	for(j=0;j<CHANNEL_NUMBER;j++)	Printf("[  CH%d  ]",j);
	Printf("\r\n");
	j=0;
	do{
		AI_CH_Select(j);
		sleep(15);
		value = ADC_read_a_value(4);
		if (value == 0xFFFFFF)
			Printf("[Burnout]");
		else
			Printf("[       ]");

		if (++j == CHANNEL_NUMBER){
			j=0;
			Printf("\r");
		}
		ch = 0;
		if(Kbhit())
			ch = Getch();
	}while(ch !=27);
}
#endif

UINT32 CompareMAX(UINT32 Number1, UINT32 Number2){
#if Input_signal
	if(Number1 > Number2)
		return Number1;
	else return Number2;
#else
	if(Number1 & 0x800000){
		if(Number2 & 0x800000){
			if(Number1 > Number2)
				return Number1;
			else
				return Number2;
		}
		else
			return Number2;
	}
	else{
		if(Number2 & 0x800000)
			return Number1;
		else{
			if(Number1 > Number2)
				return Number1;
			else 
				return Number2;
		}
	}
#endif
}

UINT32 CompareMIN(UINT32 Number1, UINT32 Number2){
#if Input_signal
	if(Number1 < Number2)
		return Number1;
	else
		return Number2;
#else
	if(Number1 & 0x800000){
		if(Number2 & 0x800000){
			if(Number1 > Number2)
				return Number2;
			else
				return Number1;
		}
		else
			return Number1;
	}
	else{
		if(Number2 & 0x800000)
			return Number2;
		else{
			if(Number1 > Number2)
				return Number2;
			else 
				return Number1;
		}
	}
#endif
}

UINT32 Compare_different(UINT32 Number1, UINT32 Number2){	//must Number1 > NUmber2
#if Input_signal
	if(Number1 > Number2)
		return (Number1 - Number2);
	else
		return 0;
#else
	if(Number1 < 0x800000){
		if(Number2 < 0x800000)
			return (Number1-Number2);
		else
			return (0x1000000 + Number1 - Number2);
	}
	else{
		return (Number1 - Number2);
	}
#endif
}

/*** data transfer ***/
UINT32 AI_data_transfer(UINT32 value, UINT8 gain)
{
#if E1240 | E1242
	switch(gain){
		case 0: value /= 1677.7215;		// 0 V~10 V 
				break;
		case 2: value /= 1048.576;		// 4 mA ~ 20mA
				value += (LINEARITY_Io_LOW * 1000);
				break;
		default:break;
	}
#endif
#if E1262
	switch(gain){
		case 0: value /= 107.37280802;		// -78.126~78.126 
				break;
		case 1: value /= 214.75111361;		// -39.062~39.062
				break;
		case 2: value /= 429.48018636;		// -19.532~19.532
				break;
		default:break;
	}
#endif

#if E1260
	switch(gain){
			case 0: value /= 6.710886;		// 0 ~ 25000 > 0 ~ 2500.0
					break;
			case 1: value /= 13.421772;		// 0 ~ 12500 > 0 ~ 1250.0
					break;
			case 2: value /= 26.843544;		// 0 ~ 62500 > 0 ~ 625.00
					break;
			case 3: value /= 53.687088;		// 0 ~ 31250 > 0 ~ 312.50
					break;
			default:break;
	}
#endif
	return value;
}

/******************************************************/
/*           Calibration and linearity test           */
/******************************************************/

#define recal

/*** signal cal ***/
UINT8 AI_Auto_System_Calibration_routine(UINT8 all_cal,UINT8 input_type) 
{
	UINT32 offset_reg[CHANNEL_NUMBER], gain_reg[CHANNEL_NUMBER];
	UINT32 offset_tmp=0x00000000, gain_tmp=0x00000000;
	UINT32 value;
	int i,j;
	UINT32 Value[CAL_AVERAGE_NUMBER+4][CHANNEL_NUMBER];
	UINT32 ave_high[CHANNEL_NUMBER],ave_low[CHANNEL_NUMBER];
	INT32 err_high[CHANNEL_NUMBER],err_low[CHANNEL_NUMBER];
	UINT32 val_max=0,val_min=0;
	UINT32 val[CHANNEL_NUMBER];
	int cal_pass=0;
	UINT8 iter_no=0,err_times;
	UINT8 data[CAL_AVERAGE_NUMBER];

	Printf("\r\n--------------------------------------------------------------------\r\n");
#if E1240 | E1242
	switch(input_type){
	case Current_mode:
		Printf(" Current Calibration\r\n");	break;
	case Voltage_mode:
		Printf(" Voltage Calibration\r\n");	break;
	default:	break;
	}
#endif
#if E1262 | E1260
	Printf(" Calibration gian = %d\r\n", 1 << input_type);
#endif

	for(j=0;j<CHANNEL_NUMBER;j++){
		err_high[j] = 0xffffff;
		err_low[j] = 0xffffff;
	}

	if(all_cal){
		for( j = 0 ;j<CHANNEL_NUMBER;j++){
#if E1240 | E1242
			if(input_type)
				cal_parameters_load(j, Current_mode);
			else
				cal_parameters_load(j, Voltage_mode);
#endif
#if E1262 | E1260
			cal_parameters_load(j,input_type);
#endif
			sleep(1);
			offset_reg[j] = (UINT32)ADC_Register_Readback(10);
			offset_reg[j] |= (UINT32)ADC_Register_Readback(11) << 8;
			offset_reg[j] |= (UINT32)ADC_Register_Readback(12) << 16;
			gain_reg[j] = (UINT32)ADC_Register_Readback(13);
			gain_reg[j] |= (UINT32)ADC_Register_Readback(14) << 8;
			gain_reg[j] |= (UINT32)ADC_Register_Readback(15) << 16;
			Printf(" Before calibration CH%d : Offset reg = 0x%06x , Gain reg = %06x\r\n", j, offset_reg[j], gain_reg[j]);
		}
		for (j=CHANNEL_NUMBER ; j<8 ; j++) cal_pass |= 1 << j; //meichi note: 11000000
	}else{
#if E1240 | E1242
		if(input_type)
			cal_parameters_load(Current_AI_Channel, Current_mode);
		else
			cal_parameters_load(Current_AI_Channel, Voltage_mode);
#endif
#if E1262 | E1260
		cal_parameters_load(Current_AI_Channel, input_type);
#endif
		sleep(1);
		offset_reg[CHANNEL_NUMBER-1] = (UINT32)ADC_Register_Readback(10);
		offset_reg[CHANNEL_NUMBER-1] |= (UINT32)ADC_Register_Readback(11) << 8;
		offset_reg[CHANNEL_NUMBER-1] |= (UINT32)ADC_Register_Readback(12) << 16;
		gain_reg[CHANNEL_NUMBER-1] = (UINT32)ADC_Register_Readback(13);
		gain_reg[CHANNEL_NUMBER-1] |= (UINT32)ADC_Register_Readback(14) << 8;
		gain_reg[CHANNEL_NUMBER-1] |= (UINT32)ADC_Register_Readback(15) << 16;
		Printf(" Before calibration: Offset reg = 0x%06x , Gain reg = 0x%06x\r\n", offset_reg[CHANNEL_NUMBER-1], gain_reg[CHANNEL_NUMBER-1]);
//		cal_pass = 0xff;
		cal_pass = 0x7f;
		cal_pass &= ~(1 << (CHANNEL_NUMBER - 1));
	}
	Printf("--------------------------------------------------------------------\r\n");
	do {

		Printf("\r\nIteration count .................................... <%d>\r\n", iter_no);
#if E1240 | E1242
		switch(input_type)
		{
			case Voltage_mode:
					sprintf(data,"%d.%d",CAL_Vo_HH,CAL_Vo_HL);
					Calibrator_Set_Output_value(data);
					break;
			case Current_mode:
					sprintf(data,"%d.%d",CAL_Io_HH,CAL_Io_HL);
					Calibrator_Set_Output_value(data);
					sleep(5000);
					break;
			default:break;
		}
#endif
#if E1262
		switch(input_type)
		{
			case 0: sprintf(data,"%d.%03d",CAL_Vo_X1H,CAL_Vo_X1L);
					break;
			case 1: sprintf(data,"%d.%03d",CAL_Vo_X2H,CAL_Vo_X2L);
					break;
			case 2: sprintf(data,"%d.%03d",CAL_Vo_X4H,CAL_Vo_X4L);
					break;
			default:break;
		}
		Calibrator_Set_Output_value(data);
#endif
#if E1260
		switch(input_type)
		{
			case 0: sprintf(data,"%d.%03d",CAL_R_X1HH,CAL_R_X1HL);
					break;
			case 1: sprintf(data,"%d.%03d",CAL_R_X2HH,CAL_R_X2HL);
					break;
			case 2: sprintf(data,"%d.%03d",CAL_R_X4HH,CAL_R_X4HL);
					break;
			case 3: sprintf(data,"%d.%03d",CAL_R_X8HH,CAL_R_X8HL);
					break;
			default:break;
		}
		Calibrator_Set_Output_value(data);
		Calibrator_out();
//		Calibrator_out();
#endif
//		sleep(2000);

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1; //meichi note: why not current_channel?
		do{
#ifdef recal
			if((cal_pass & (1 << j)) == 0){
#endif
				if(all_cal){
					AI_CH_Select(j);
					sleep(1);
					write_parm_to_ADC(offset_reg[j], gain_reg[j]);
					sleep(1);
				}
				ADC_Data_Sync();
				while(!ADC_Data_Ready());
				i=0;
#if Input_signal
				val_max = 0x0;
				val_min = 0xFFFFFF;
#else
				val_max = 0x800001;
				val_min = 0x7FFFFF;
#endif
				err_times = 0;
				do{
					value = ADC_Read() & 0xFFFFFF;
					Value[i][j]=value;
					if(i>3){
						val_max=CompareMAX(Value[i][j],val_max);
						val_min=CompareMIN(Value[i][j],val_min);

						val[j] = val_max - val_min;
						if(val[j] > CAL_ERROR_LSB){
							if(all_cal)
								Printf("\r\nNoise > %d LSBs ................................... CH-%d, Fail!\r\n",val[j], j);
							else
								Printf("\r\nNoise > %d LSBs ................................... CH-%d, Fail!\r\n",val[j], Current_AI_Channel);
#if Input_signal
							val_max = 0x0;
							val_min = 0xFFFFFF;
#else
							val_max = 0x800001;
							val_min = 0x7FFFFF;
#endif
							i=3;
							if(err_times > 5){
								return 2;
							}
							else
								err_times++;						
							sleep(2000);
						}
					}
					i++;
					while(!ADC_Data_Ready());
				}while(i<(CAL_AVERAGE_NUMBER+4));
#ifdef recal
			}
#endif
			j++;
		}while(j<CHANNEL_NUMBER);
		
		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;
		for( i =j ; i < CHANNEL_NUMBER ; i++)
			ave_high[i] = 0;

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;

		do{
#ifdef recal
			if((cal_pass & (1 << j)) == 0){
#endif
				for (i=4;i<(CAL_AVERAGE_NUMBER+4);i++)	ave_high[j]+=Value[i][j];
				ave_high[j] = ave_high[j] >> CAL_AVERAGE_SHIFT;
#ifdef recal
			}
			else
				ave_high[j] = CAL_point_H;
#endif
			j++;
		}while(j<CHANNEL_NUMBER);
#if E1240 | E1242
		switch(input_type)
		{
			case Voltage_mode:
					sprintf(data,"%d.%d",CAL_Vo_LH,CAL_Vo_LL);
					Calibrator_Set_Output_value(data);
					break;
			case Current_mode:
					sprintf(data,"%d.%d",CAL_Io_LH,CAL_Io_LL);
					Calibrator_Set_Output_value(data);
					sleep(5000);
					break;
			default:break;
		}
#endif
#if E1262
		switch(input_type)
		{
			case 0: sprintf(data,"-%d.%03d",CAL_Vo_X1H,CAL_Vo_X1L);
					break;
			case 1: sprintf(data,"-%d.%03d",CAL_Vo_X2H,CAL_Vo_X2L);
					break;
			case 2: sprintf(data,"-%d.%03d",CAL_Vo_X4H,CAL_Vo_X4L);
					break;
			default:break;
		}
		Calibrator_Set_Output_value(data);
#endif
#if E1260
		switch(input_type)
		{
			case 0: sprintf(data,"%d.%03d",CAL_R_X1LH,CAL_R_X1LL);
					break;
			case 1: sprintf(data,"%d.%03d",CAL_R_X2LH,CAL_R_X2LL);
					break;
			case 2: sprintf(data,"%d.%03d",CAL_R_X4LH,CAL_R_X4LL);
					break;
			case 3: sprintf(data,"%d.%03d",CAL_R_X8LH,CAL_R_X8LL);
					break;
			default:break;
		}
		Calibrator_Set_Output_value(data);
		Calibrator_out();
//		Calibrator_out();
#endif
//		sleep(2000);

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;
		do{
#ifdef recal
			if((cal_pass & (1 << j)) == 0){
#endif
				if(all_cal){
					AI_CH_Select(j);
					sleep(1);
					write_parm_to_ADC(offset_reg[j], gain_reg[j]);
					sleep(1);
				}
				ADC_Data_Sync();
				while(!ADC_Data_Ready());
#if Input_signal
				val_max = 0x0;
				val_min = 0xffffff;
#else
				val_max = 0x800001;
				val_min = 0x7fffff;
#endif
				err_times =0;
				i=0;
				do{
					value = ADC_Read() & 0xFFFFFF;
					Value[i][j]=value;
					if(i>3){

						val_max=CompareMAX(Value[i][j],val_max);
						val_min=CompareMIN(Value[i][j],val_min);

						val[j] = val_max - val_min;
						if(val[j] > CAL_ERROR_LSB){
							if(all_cal)
								Printf("\r\nNoise > %d LSBs ................................... CH-%d, Fail!\r\n",val[j], j);
							else
								Printf("\r\nNoise > %d LSBs ................................... CH-%d, Fail!\r\n",val[j], Current_AI_Channel);
							i=3;
#if Input_signal
							val_max = 0x0;
							val_min = 0xffffff;
#else
							val_max = 0x800001;
							val_min = 0x7fffff;
#endif
							if(err_times > 5){
								return 2;
							}
							else
								err_times++;
							sleep(2000);
						}
					}
					i++;
					while(!ADC_Data_Ready());
				}while(i<(CAL_AVERAGE_NUMBER+4));
#ifdef recal
			}
#endif
			j++;
		}while(j<CHANNEL_NUMBER);

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;
		for(i=j;i<CHANNEL_NUMBER;i++)	ave_low[i] = 0;

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;

		do{
#ifdef recal
			if((cal_pass & (1 << j)) == 0){
#endif
				for (i=4;i<(CAL_AVERAGE_NUMBER+4);i++)	ave_low[j]+=Value[i][j];
				ave_low[j] = ave_low[j] >> CAL_AVERAGE_SHIFT;
#ifdef recal
			}
			else 
				ave_low[j] = CAL_point_L;
#endif
			j++;
		}while(j<CHANNEL_NUMBER);

		if(all_cal)	j=0; 
		else	j=CHANNEL_NUMBER-1;
		do{
			if(all_cal)
				Printf(" CH%d ",j);
			else
				Printf(" CH%d ",Current_AI_Channel);
			if( ave_high[j] > CAL_point_H ){
				value = ave_high[j] - CAL_point_H;
				err_high[j] = value;
				Printf("Devi_high = %d,Devi_low = ",err_high[j]);
			}
			else{
				value = CAL_point_H-ave_high[j];
				err_high[j] = value;
				Printf("Devi_high = -%d,Devi_low = ",err_high[j]);
			}
			
			if( ave_low[j] > CAL_point_L ){
				value = ave_low[j] - CAL_point_L;
				err_low[j] = value;
				Printf("%d",err_low[j]);
			}
			else{
				value = CAL_point_L-ave_low[j];
				err_low[j] = value;
				Printf("-%d",err_low[j]);
			}
			Printf(",Noise = %d\r\n",val[j]);

			cal_pass &= ~(1 << j);
			if((err_high[j] < CAL_CONV_LSB) && (err_low[j] < CAL_CONV_LSB))
				cal_pass |= 1 << j;
			else{
//				offset_tmp = calculate_offset(ave_high[j],ave_low[j],offset_reg[j]);
//				offset_tmp = offset_reg[j];
#if E1240 | E1242
				err_high[j] = ave_high[j] - ave_low[j];
				gain_tmp = calculate_gain(gain_reg[j],err_high[j]);
				switch(input_type)
				{
					case Voltage_mode:
						offset_tmp = calculate_offset(ave_high[j],ave_low[j],offset_reg[j]);
					break;
					case Current_mode:
						offset_tmp = calculate_offsetc(ave_high[j],ave_low[j],offset_reg[j]);
					break;
					default:break;
				} 
#endif
#if E1262
				ave_low[j] = 0x1000000 - ave_low[j]; 
				err_high[j] = ave_high[j] + ave_low[j];
				gain_tmp = calculate_gain(gain_reg[j],err_high[j]);
				offset_tmp = calculate_offset(ave_high[j],ave_low[j],offset_reg[j]);
#endif
#if E1260
				err_high[j] = ave_high[j] - ave_low[j];
//				gain_tmp = calculate_gain(gain_reg[j],err_high[j]);
				gain_tmp = calculate_gain(gain_reg[j],CAL_point_H-CAL_point_L,err_high[j]);
//				offset_tmp = calculate_offset(ave_high[j],ave_low[j],offset_reg[j]);
				offset_tmp = calculate_offset(ave_low[j],CAL_point_L,2097152,offset_reg[j],gain_reg[j],gain_tmp);
#endif
				write_parm_to_ADC(offset_tmp,gain_tmp);
				sleep(1);
				offset_reg[j] = offset_tmp;
				gain_reg[j] = gain_tmp;
			}
			sleep(1);
			j++;
		}while(j<CHANNEL_NUMBER);
		if(iter_no > 15){
			if(cal_pass !=0xff){
			if(all_cal){
				for(j=0;j<CHANNEL_NUMBER;j++){
					if(cal_pass & (0x1 << j)){
#if E1240 | E1242
						if(input_type == Current_mode)
							cal_parameters_save(j, Current_mode, offset_reg[j],gain_reg[j]);
						else
							cal_parameters_save(j, Voltage_mode, offset_reg[j],gain_reg[j]);
#endif
#if E1262 | E1260
						cal_parameters_save(j, input_type, offset_reg[j], gain_reg[j]);
#endif
					}
				}
			}
			return 1;
			}
		}
		iter_no++;
		if(diag_check_press_ESC() == DIAG_ESC) return 0;
	}while(cal_pass != 0xff);
	if(all_cal){
		Printf("--------------------------------------------------------------------\r\n");
		for(j=0;j<CHANNEL_NUMBER;j++){
			Printf(" After calibration CH%d: Offset reg = 0x%06x , Gain reg = %06x\r\n",j, offset_reg[j], gain_reg[j]);
#if E1240 | E1242
			if(input_type == Current_mode)
				cal_parameters_save(j, Current_mode, offset_reg[j],gain_reg[j]);
			else
				cal_parameters_save(j, Voltage_mode, offset_reg[j],gain_reg[j]);
#endif
#if E1262 | E1260
			cal_parameters_save(j, input_type, offset_reg[j], gain_reg[j]);
#endif
		}
	}
	else{
#if E1240 | E1242
		if(input_type == Current_mode)
			cal_parameters_save(Current_AI_Channel, Current_mode, offset_reg[CHANNEL_NUMBER-1], gain_reg[CHANNEL_NUMBER-1]);
		else
			cal_parameters_save(Current_AI_Channel, Voltage_mode, offset_reg[CHANNEL_NUMBER-1], gain_reg[CHANNEL_NUMBER-1]);
#endif
#if E1262 | E1260
			cal_parameters_save(Current_AI_Channel, input_type, offset_reg[CHANNEL_NUMBER-1], gain_reg[CHANNEL_NUMBER-1]);
#endif
		Printf("--------------------------------------------------------------------\r\n");
		Printf(" After calibration CH%d: Offset reg = 0x%06x , Gain reg = %06x\r\n",Current_AI_Channel, offset_reg[CHANNEL_NUMBER-1], gain_reg[CHANNEL_NUMBER-1]);
	}
	return 0;
}

void AI_Auto_System_Calibration_single_ch(void)
{
	UINT8 input_type;
    UINT32 ret=0;

#if E1240 | E1242
  	if(diag_get_value("Calibration AI Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	AI_TYPE_CH_Select((UINT8)ret);
#endif
#if E1262
  	if(diag_get_value("Calibration TC Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1260
	UINT8 command[4];
  	if(diag_get_value("Calibration RTD Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
	AI_CH_Select((UINT8)ret);

#if E1240 | E1242
	for (input_type=0; input_type<SINGLE_CHANNEL_EEPROM_DATA; input_type++){
		switch(input_type){
		case Voltage_mode:
			if(AI_TYPE_READ() != Voltage_mode){ /*Current mode is high Voltage mode is low*/
				Printf("\r\nPlease Check Channel %d is Voltage mode\r\n",ret);
				Printf("Press any key to continue! \r\n");
				while(!Kbhit());
				if(Getch() == 27) return;
			}
			Calibrator_Set_State(DCV);
			CA150_Set_Source_Range_func(DCV_10V);
			Calibrator_out();
			ADC_PGA_Gain(ADC_PGA_GAIN_1);	break;
		case Current_mode:
			if(AI_TYPE_READ() != Current_mode){ /*Current mode is high Voltage mode is low*/
				Printf("\r\nPlease Check Channel %d is Current mode\r\n",ret);
				Printf("Press any key to continue! \r\n");
				while(!Kbhit());
				if(Getch() == 27) return;
			}
			Calibrator_Set_State(DCA);
			CA150_Set_Source_Range_func(DCA_4_20mA);
			Calibrator_out();
			ADC_PGA_Gain(ADC_PGA_GAIN_4);	break;
		default:	break;
		}
		AI_Auto_System_Calibration_routine(0,input_type);
	}
#endif

#if E1262
	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();
	for (input_type=0; input_type<SINGLE_CHANNEL_EEPROM_DATA; input_type++){
		ADC_PGA_Gain(input_type);
		AI_Auto_System_Calibration_routine(0,input_type);
	}
#endif

#if E1260
	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();
	for (input_type=0; input_type<SINGLE_CHANNEL_EEPROM_DATA; input_type++){
		ADC_PGA_Gain(input_type);
		AI_Auto_System_Calibration_routine(0,input_type);
	}
#endif

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}

#if E1240 | E1242
void diag_do_AI_Voltage_Calibration(void){
	UINT8 i;

	ADC_PGA_Gain(ADC_PGA_GAIN_1);
	for(i=0; i<CHANNEL_NUMBER;i++){
		AI_TYPE_CH_Select(i);
		sleep(1);
		if(AI_TYPE_READ() != Voltage_mode){
			Printf("Please Check Channel %d is Voltage mode\r",i);
			i--;
			while(!Kbhit());
			if(Getch() == 27) return;
		}			
	}
	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_10V);
	Calibrator_out();
	AI_Auto_System_Calibration_routine(1,Voltage_mode);

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}

void diag_do_AI_Current_Calibration(void) 
{
	UINT8 i;
	UINT32 ret=0;

  	if(diag_get_value("Calibration method => (0)Single channel / (1)All Channels",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}

	ADC_PGA_Gain(ADC_PGA_GAIN_4);
	Calibrator_Set_State(DCA);
	CA150_Set_Source_Range_func(DCA_4_20mA);
	if(ret){
		for (i=0; i<CHANNEL_NUMBER; i++){
			sleep(1);
			if(AI_TYPE_READ() != Current_mode){
				Printf("Please Check Channel %d is Current mode\r",i);
				i--;
				while(!Kbhit());
				if(Getch() == 27) return;
			}	
		}
		Calibrator_out();
		AI_Auto_System_Calibration_routine(1,Current_mode);
	}
	else{
		for (i=0; i<CHANNEL_NUMBER; i++){
			AI_CH_Select(i);
			AI_TYPE_CH_Select(i);
			sleep(1);
			if(AI_TYPE_READ() != Current_mode){
				Printf("Please Check Channel %d is Current mode\r",i);
				i--;
				while(!Kbhit());
				if(Getch() == 27) return;
			}	
			else{
				Calibrator_out();
				AI_Auto_System_Calibration_routine(0,Current_mode);
			}
		}
	}

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}
#endif

#if E1262
void diag_do_TC_Auto_System_Calibration_single_gain(void){
    UINT32 ret=0;

   	if(diag_get_value("Calibration TC Gain is ... (0:X1 / 1:X2 / 2:X4) => ",&ret,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain((UINT8)ret);

	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();
	AI_Auto_System_Calibration_routine(1,(UINT8)ret);

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}

void TC_Full_Auto_System_Calibration(void){

	UINT8 gain;

	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();
	for (gain=0; gain<SINGLE_CHANNEL_EEPROM_DATA; gain++){
		ADC_PGA_Gain(gain);
		AI_Auto_System_Calibration_routine(1,gain);
	}

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}
#endif

#if E1260
void diag_do_RTD_Auto_System_Calibration_single_gain(void){
    UINT32 ret=0;
	UINT8 command[4];

   	if(diag_get_value("Calibration RTD Gain is ... (0:X1 / 1:X2 / 2:X4 / 3:X8) => ",&ret,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain((UINT8)ret);

	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();
	AI_Auto_System_Calibration_routine(1,(UINT8)ret);

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}

void RTD_Full_Auto_System_Calibration(void){
	UINT8 gain;
	UINT8 command[4];

	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();
	for (gain=0; gain<SINGLE_CHANNEL_EEPROM_DATA; gain++){
		ADC_PGA_Gain(gain);
		AI_Auto_System_Calibration_routine(1,gain);
	}

	if(eeprom_readb(RAND_ADDR,CAL_FLAG1) == 1)
		i2c_write_to_eeprom(CAL_FLAG1,0);
}
#endif

extern char EEPROM_address; 
#if E1240 | E1242
void diag_do_scan_init(void){
	int i;
	UINT16 add;
	UINT8 temp_ee[3];
	UINT32 offset[CHANNEL_NUMBER],gain[CHANNEL_NUMBER];

	for(i=0; i<CHANNEL_NUMBER ; i++)			// build-up the channel parameters' table in RAM
	{
		add = i*12;
		temp_ee[0] = eeprom_readb(RAND_ADDR,add);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		offset[i] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];

		temp_ee[0] = eeprom_readb(CURR_ADDR,0);
		temp_ee[1] = eeprom_readb(CURR_ADDR,0);
		temp_ee[2] = eeprom_readb(CURR_ADDR,0);
		gain[i] = (temp_ee[2]<<16)+(temp_ee[1]<<8)+temp_ee[0];

		AI_CH_Select(i);
		if(i==(CHANNEL_NUMBER-1))
			AI_TYPE_CH_Select(0);
		else
			AI_TYPE_CH_Select(i+1);
		ADC_PGA_Gain(ADC_PGA_GAIN_1);
		copy_ADC_REG_to_RAM(i);
	}
	sleep(1);
	copy_ADC_RAM_to_REG(0);
	sleep(14);
}

void diag_do_scan_ai(UINT8 scan){
	UINT32 value,pvalue;
	UINT8 ch=0;
	UINT16 state=0;
	int i=0;
	ulong t;
	UINT8 data=0;

	t = Gsys_msec;

	do{
		value = ADC_Read() & 0xFFFFFF;
		if (i == scan_rate){
			pvalue = AI_data_transfer(value,Voltage_mode);
			Printf("[ %02d.%03d]",pvalue/1000,pvalue%1000);
			if(scan == 0){
				if(pvalue < 20){
					copy_ADC_RAM_to_REG(0);
					return;
				}
			}else if(scan == 1){
				if(pvalue > 400){
					copy_ADC_RAM_to_REG(0);
					return;
				}
			}
			else if(scan == 2){
				if((pvalue > 100) && (pvalue < 150))
					data |= (0x1 << ch); 
			}
			else if(scan == 3){
				if(pvalue > 50)
					data |= (0x1 << ch);
			}
		}

		while(!ADC_Data_Ready());
		if (i <= (scan_rate-1)) i++;
		else {
			i = 0;
			if(ch < (CHANNEL_NUMBER-1))
				ch ++;
			else{ 
				ch = 0;
				Printf("\r");
			}
			copy_ADC_RAM_to_REG(ch);
			sleep(1);
		}	
		if(scan == 0){
			if((Gsys_msec - t) > 500){
				if(state == BUZZER)
					state = 0;
				else
					state = BUZZER;
				mptest_DO(state);
				t = Gsys_msec;
			}
		}
	}while(data!=0xff);
}

void diag_do_AI_MP_calibration(void){
	ulong t,t1;
	char data[5];

	/*init EEPROM*/
	Printf("\r\nCalibration start\r\n");
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;

	i2c_write_to_eeprom(CAL_FLAG1,0);	//clear cal. flag
	i2c_write_to_eeprom(CAL_RFLAG,0);	//clear report flag
	/*init MPtester communicate*/
	mptester_communication_init();

	/*init SPI & ADC*/
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
	ADC_PGA_Gain(ADC_PGA_GAIN_1);

	/* Voltage Calibration */
	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);
	if(mptest_GPIO_output(0xFFFF) != mp_tran_OK){
		sleep(100);
		if(mptest_GPIO_output(0xFFFF) != mp_tran_OK){
			Printf("Calibration Fix Fail\r\n");
			calibration_fail();
			return;
		}
	}

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_9600 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);

	Calibrator_Set_State(DCV);
	sleep(100);
	if(CA150_Set_Source_func(QUERY) != DCV){
		Calibrator_Set_State(DCV);
		if(CA150_Set_Source_func(QUERY) != DCV){
			calibration_fail();
			return;
		}
	}

//	sleep(1000);
	CA150_Set_Source_Range_func(DCV_10V);
	sleep(100);
	if(CA150_Set_Source_Range_func(QUERY) != DCV_10V){
		CA150_Set_Source_Range_func(DCV_10V);
		if(CA150_Set_Source_Range_func(QUERY) != DCV_10V){
			calibration_fail();
			return;
		}
	}

	Calibrator_out();
	t = Gsys_msec;
	if(AI_Auto_System_Calibration_routine(1,Voltage_mode) != 0){
		CA150_Set_Source_State_func(TURN_OFF);
		calibration_fail();
		return;
	}

	sprintf(data,"1.0");
	Calibrator_Set_Output_value(data);

	t1 = Gsys_msec - t;

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);
	if(mptest_GPIO_output(0xFFFE) != mp_tran_OK){
		sleep(100);
		if(mptest_GPIO_output(0xFFFE) != mp_tran_OK){
			Printf("Calibration Fix Fail\r\n");
			calibration_fail();
		}
	}
	sleep(1000);	//wait relay OK
	diag_do_scan_init();
	diag_do_scan_ai(3);
	PrintStr("\r\n");
	diag_do_scan_ai(0);
	PrintStr("\r\n");
	mptest_DO(0x0);
	diag_do_scan_ai(1);
	PrintStr("\r\n");
	diag_do_scan_ai(2);
	PrintStr("\r\n");

	/* Current Calibration */
	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_9600 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);

	Calibrator_Set_State(DCA);
	sleep(100);
	if(CA150_Set_Source_func(QUERY) != DCA){
		Calibrator_Set_State(DCA);
		if(CA150_Set_Source_func(QUERY) != DCA){
			calibration_fail();
			return;
		}
	}
	CA150_Set_Source_Range_func(DCA_4_20mA);
	sleep(100);
	if(CA150_Set_Source_Range_func(QUERY) != DCA_4_20mA){
		CA150_Set_Source_Range_func(DCA_4_20mA);
		if(CA150_Set_Source_Range_func(QUERY) != DCA_4_20mA){
			calibration_fail();
			return;
		}
	}
	CA150_Set_Source_State_func(TURN_ON);
	sleep(100);	//wait to get states
	if(CA150_Set_Source_State_func(QUERY) != TURN_ON){
		CA150_Set_Source_State_func(TURN_ON);
		if(CA150_Set_Source_State_func(QUERY) != TURN_ON){
			calibration_fail();
			return;
		}
	}
	t = Gsys_msec;
	ADC_PGA_Gain(ADC_PGA_GAIN_4);
	if(AI_Auto_System_Calibration_routine(1,Current_mode) != 0){
		CA150_Set_Source_State_func(TURN_OFF);
		calibration_fail();
		return;
	}

//	CA150_Set_Source_State_func(TURN_OFF);
	t =  (Gsys_msec - t + t1)/1000;
	Printf("\r\nCalibration time = %02d:%02d\r\n",t/60,t%60);

	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();

	diag_do_set_clean_flag(1,FLAG_CAL);
	i2c_write_to_eeprom(CAL_FLAG1,1);	//clear cal. flag
//	cal_para_ok = cal_parameters_uniformity_check(0);

	i2c_write_to_eeprom(CAL_TIME,0);
	i2c_write_to_eeprom(CAL_TIME+1,(t/60));
	i2c_write_to_eeprom(CAL_TIME+2,(t%60));

	if(cal_parameters_uniformity_check() == 4)
		calibration_pass();				
	else
		calibration_retry();
}
#endif
#if E1262
void diag_do_AI_MP_calibration(void){
	ulong t;
	UINT8 gain;

	/*init EEPROM*/
	Printf("\r\nCalibration start\r\n");
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;

	i2c_write_to_eeprom(CAL_FLAG1,0);	//clear cal. flag
	i2c_write_to_eeprom(CAL_RFLAG,0);	//clear report flag
	/*init MPtester communicate*/
	mptester_communication_init();

	/*init SPI & ADC*/
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_9600 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);

	Calibrator_Set_State(DCV);
	sleep(100);
	if(CA150_Set_Source_func(QUERY) != DCV){
		Calibrator_Set_State(DCV);
		if(CA150_Set_Source_func(QUERY) != DCV){
			calibration_fail();
			return;
		}
	}

	CA150_Set_Source_Range_func(DCV_100mV);
	sleep(100);
	if(CA150_Set_Source_Range_func(QUERY) != DCV_100mV){
		CA150_Set_Source_Range_func(DCV_100mV);
		if(CA150_Set_Source_Range_func(QUERY) != DCV_100mV){
			calibration_fail();
			return;
		}
	}

	Calibrator_out();
	t = Gsys_msec;
	for(gain=0;gain < SINGLE_CHANNEL_EEPROM_DATA ; gain++){
		ADC_PGA_Gain(gain);
		if(AI_Auto_System_Calibration_routine(1,gain) != 0){
			CA150_Set_Source_State_func(TURN_OFF);
			calibration_fail();
			return;
		}
	}

	t =  (Gsys_msec - t)/1000;
	Printf("\r\nCalibration time = %02d:%02d\r\n",t/60,t%60);

	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();

	diag_do_set_clean_flag(1,FLAG_CAL);
	i2c_write_to_eeprom(CAL_FLAG1,1);	//clear cal. flag
//	cal_para_ok = cal_parameters_uniformity_check(0);

	i2c_write_to_eeprom(CAL_TIME,0);
	i2c_write_to_eeprom(CAL_TIME+1,(t/60));
	i2c_write_to_eeprom(CAL_TIME+2,(t%60));

	if(cal_parameters_uniformity_check() == 6)
		calibration_pass();				
	else
		calibration_retry();
}
#endif

#if E1260
//白箱、黑箱測試
/**	\brief
 *
 *	Check RTD channels before starting calibration.
 *	\retval return 0: All channels PASS \r\n
 *	\retval return -1: FAIL
 */
UINT8 do_AI_channels_RTD_check_before_K(void){
	UINT8 input_type, input_type_reg;
	UINT32 gain_default, offset_default;
	UINT32 gain_reg, offset_reg;
	UINT32 Value_arr[CHANNEL_NUMBER];
	UINT8 i, j;
	UINT8 channel_check_pass_flag = 0x0; //Pass: 0011 1111
	UINT8 data[32];
	Printf("\r\nCheck RTD channels before starting calibration\r\n");
	input_type = 0;
	/*set ADC PGA register*/
	ADC_PGA_Gain(input_type);
	/*read back PGA register*/
	input_type_reg = ADC_Register_Readback(2) & 0x07;

	/*clear adc raw data arrays*/
	//memset(Value_arr, 0, sizeof(Value_arr));
	for (j=0; j<CHANNEL_NUMBER; j++){
		if (input_type == 0){
			/*set default value for gain and offset*/
			gain_default = 5746788;
			offset_default = 8758;
			/*write gain and offset to ADC register*/
			write_parm_to_ADC(offset_default, gain_default);
			sleep(10);
		}
		sleep(1);
		/*read back gain and offset from ADC register*/
		offset_reg = (UINT32)ADC_Register_Readback(10);
		offset_reg |= (UINT32)ADC_Register_Readback(11) << 8;
		offset_reg |= (UINT32)ADC_Register_Readback(12) << 16;
		gain_reg = (UINT32)ADC_Register_Readback(13);
		gain_reg |= (UINT32)ADC_Register_Readback(14) << 8;
		gain_reg |= (UINT32)ADC_Register_Readback(15) << 16;
		Printf(" Current calibration value for CH%d: Input type reg = x%d, Offset reg = 0x%06x , Gain reg = %06x\r\n", j, (UINT8)(1 << input_type_reg), offset_reg, gain_reg);
		
		/*Set data value for the calibrator 3/4*/
		switch(input_type)
		{
			case 0: sprintf(data,"%d.%03d",CAL_R_X1HH,CAL_R_X1HL);
					break;
			case 1: sprintf(data,"%d.%03d",CAL_R_X2HH,CAL_R_X2HL);
					break;
			case 2: sprintf(data,"%d.%03d",CAL_R_X4HH,CAL_R_X4HL);
					break;
			case 3: sprintf(data,"%d.%03d",CAL_R_X8HH,CAL_R_X8HL);
					break;
			default:break;
		}
		Calibrator_Set_Output_value(data);
		Calibrator_out();

		ADC_Data_Sync();
		for (i=0; i<(CAL_AVERAGE_NUMBER+3); i++){
			while(!ADC_Data_Ready());
			if (i > 2) {
				Value_arr[j] += ADC_Read() & 0xFFFFFF;
			}
		}
	}

	for (j=0; j<CHANNEL_NUMBER; j++) {
		Value_arr[j] = Value_arr[j] >> 5;
		/*range check*/
		if ((Value_arr[j]<(CAL_point_H-1500)) || (Value_arr[j]>(CAL_point_H+1500))){
			channel_check_pass_flag &= ~(1 << j);
			Printf("FAIL: channel %d raw data is 0x%06x\r\n", j, Value_arr[j]);
		}
		else {
			channel_check_pass_flag |= (1 << j);
			Printf("PASS: channel %d raw data is 0x%06x\r\n", j, Value_arr[j]);
		}
	}
	if (channel_check_pass_flag != 0x3f) {
		return -1;
	}

	return 0;
}

void diag_do_AI_MP_calibration(void){
	ulong t;
	UINT8 gain,test;
	UINT8 command[4];
	char data[5];
	/*init EEPROM*/
	Printf("\r\nCalibration start\r\n");
	
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;
	
	GpioWriteData(PIO_EEPROM_PROTECT,0);	// enable calibration EERPOM write
	
	i2c_write_to_eeprom(CAL_FLAG1,0);	//clear cal. flag
	i2c_write_to_eeprom(CAL_RFLAG,0);	//clear report flag

	
	/*init MPtester communicate*/
	sleep(10);
	mptester_communication_init();
	sleep(10);
	/*init SPI & ADC*/
	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
	
	sleep(10);
	Set_Calibrator(calibrator_fluke);
//	sleep(10);
//	Calibrator_communication_init();
//	sleep(100);
	
	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_9600 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(2000);
	
/*	Fluke_5500A_Reset();
	sleep(10);
	Fluke_5500A_Reset();
	sleep(10);*/
	Fluke_525B_Reset();

	
/*	sprintf(command,"0");
	sleep(10);
	Calibrator_Set_Output_value(command);

	Calibrator_out();*/

/*	
	Fluke_5500A_Set_Source_Range_func(OHM_0);
		if(Fluke_5500A_Set_Source_Range_func(QUERY) != OHM_0){
		Fluke_5500A_Set_Source_Range_func(OHM_0);
		if(Fluke_5500A_Set_Source_Range_func(QUERY) != OHM_0){
			calibration_fail();
			return;
		}
	}
*/
	sleep(2000);

	if (do_AI_channels_RTD_check_before_K() != 0){
		return;
	}
	
/*	sprintf(command,"0");
	sleep(10);
	Calibrator_Set_Output_value(command);

	Calibrator_out();*/
	t = Gsys_msec;

	for(gain=0;gain < SINGLE_CHANNEL_EEPROM_DATA ; gain++){
//			Calibrator_out();
		ADC_PGA_Gain(gain);
	//	AI_Auto_System_Calibration_routine(1,gain);
		if(AI_Auto_System_Calibration_routine(1,gain) != 0){
//			CA150_Set_Source_State_func(TURN_OFF);
//			calibration_fail();
//			Fluke_5500A_Reset();
//			Calibrator_communication_stop();
			return;
		}
	}
/*
	sprintf(data,"1.0");
	Calibrator_Set_Output_value(data);
*/	
	t =  (Gsys_msec - t)/1000;
	Printf("\r\nCalibration time = %02d:%02d\r\n",t/60,t%60);
/*
	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();

	diag_do_set_clean_flag(1,FLAG_CAL);
	i2c_write_to_eeprom(CAL_FLAG1,1);	//clear cal. flag
*/

//	cal_para_ok = cal_parameters_uniformity_check(0);
	i2c_write_to_eeprom(CAL_TIME,0);
	i2c_write_to_eeprom(CAL_TIME+1,(t/60));
	i2c_write_to_eeprom(CAL_TIME+2,(t%60));
	
//	sleep(5000);
	mptester_communication_stop();
	sleep(10);
	mptester_communication_init();
	sleep(10);
	if(cal_parameters_uniformity_check() == 8)
	{
		SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
		SPI_ENABLE_SLAVE0();

		diag_do_set_clean_flag(1,FLAG_CAL);
		diag_do_set_clean_flag(0,FLAG_WARMUP);
		i2c_write_to_eeprom(CAL_FLAG1,1);	//set cal. flag
	
		Printf("\r\nPass....\r\n");
		calibration_pass();
	}
	else{
//		calibration_fail();
		SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
		SPI_ENABLE_SLAVE0();

		diag_do_set_clean_flag(0,FLAG_CAL);
		i2c_write_to_eeprom(CAL_FLAG1,0);	//clear cal. flag
		
		calibration_retry();
	}
	
	GpioWriteData(PIO_EEPROM_PROTECT,1);	// disable calibration EERPOM write
}
#endif

void calculate_mult(UINT32 ret1,UINT32 ret2,UINT32 *tmp1,UINT32 *tmp2){
	UINT32 old_tmp2;
	UINT32 data1,data2;
	UINT8 data=1;
	*tmp1=0;
	*tmp2=0;
	do
	{
	 	if(ret2 & 0x1)
		{
			data1 = (ret1 << (data-1));
			data2 = (ret1 >> (33 - data));
			*tmp1 += data2;
			old_tmp2 = *tmp2;
			*tmp2 += data1;
			if(*tmp2 < old_tmp2)
			(*tmp1)++;
		}
		ret2 = ret2 >> 1;
		data++;
		if(ret2 == 0) break;
	}while(1);
}

UINT32 calculate_data(UINT32 origin_gain,UINT32 difference, UINT32 gain){
	UINT32 temp_high,temp_low;
	UINT32 temp,temp2,temp1,temp_h,temp_hi,temp_lo;
 
	calculate_mult(origin_gain,difference,&temp_high,&temp_low);

	temp_h = temp_high << 16;
	temp1 = gain >> 8;
	temp1++;
	temp = temp_h / temp1;
	temp = temp << 8;
	calculate_mult(gain,temp,&temp_hi,&temp_lo);
	if(temp_lo > temp_low)
	{
		temp_lo = 0xFFFFFFFF - temp_lo;
		temp_lo = temp_lo + temp_low + 1;
		temp1 = temp_high - 1;
	}
	else
	{
		temp_lo = temp_low - temp_lo;
		temp1 = temp_high;
	}
	temp_hi = temp1 - temp_hi;
	do
	{
 		if(temp_lo < gain)
		{
			temp2 = 0xFFFFFFFF - gain;
			temp_lo = temp_lo + temp2 + 1;
			temp_hi--;
			temp++;
		}
		else
		{
			temp_lo -= gain;
			temp++;
		}
		if(temp_hi == 0)
		{
			if(temp_lo < gain)
			break;
		}
	}while(1);
	return temp;
}

/*** calculate parameter ***/
/*
UINT32 calculate_gain(UINT32 origin_gain,UINT32 gain){
	UINT32 temp_high,temp_low;
	UINT32 temp,temp2,temp3;

	temp_low = origin_gain << 23;
	temp_high = origin_gain >> 9;
	temp = temp_low / gain;
	temp2 = temp;
	temp = (temp_high << 16)/ (gain >> 8);
	temp = (temp << 8) + temp2;
	temp2 = temp * gain;
	temp3 = origin_gain << 23;
	if(temp2 > temp3){
		temp2 = temp2 - temp3;
		temp2 /= gain;
		temp -= temp2;
	}
	else{
		temp2 = temp3 - temp2;
		temp2 /= gain;
		temp += temp2;
	}
	return temp;
}*/
UINT32 calculate_gain(UINT32 origin_gain,UINT32 difference,UINT32 gain){
	return calculate_data(origin_gain,difference,gain);
}
#if E1262
UINT32 calculate_offset(UINT32 err_high,UINT32 err_low,UINT32 offset){
	UINT32 offset_tmp;
	UINT32 err;
	if(err_high > err_low){
		err = err_high - err_low;
		if(err > 2048){
			offset_tmp = offset + (err >> 1) - (err >> 2) + (err >> 4);
		}
		else
			offset_tmp = offset + (err >> 1) - (err >> 3);
	}
	else{
		err = err_low - err_high;
		if(err > 2048){
			offset_tmp = offset - (err >> 1) + (err >> 2) - (err >> 4);
		}
		else
			offset_tmp = offset - (err >> 1) + (err >> 3);
	}
	return offset_tmp;
}
#endif

#if E1260
/*
UINT32 calculate_offset(UINT32 err_high,UINT32 err_low,UINT32 offset){
	UINT32 offset_tmp;
	UINT32 err;
	err_high = err_high - CAL_point_H;
	err_low = err_low - CAL_point_L;
	err = (err_high + err_low) >> 1;

	if(err & 0x80000000){
		err = 0xffffffff - err;
		if(err > 2048)
			offset_tmp = offset - (err >> 2) - (err >> 3) + (err >> 5);
		else
			offset_tmp = offset - (err >> 2) ;
	}
	else{
		if(err > 2048)
			offset_tmp = offset + (err >> 2) + (err >> 3) - (err >> 5);
		else
			offset_tmp = offset + (err >> 2)  ;
	}
	return offset_tmp;
}*/
UINT32 calculate_offset(UINT32 origin_data,UINT32 point_data,UINT32 parameter,UINT32 origin_offset,UINT32 origin_gain,UINT32 new_gain){
	UINT32 temp,offset_tmp;
	temp = calculate_data(origin_data,parameter,origin_gain);
	if(origin_offset & 0x800000)
		origin_offset = origin_offset | 0xFF000000;
	temp += origin_offset;
	offset_tmp = calculate_data(point_data,parameter,new_gain);
	if(temp & 0x80000000)
	{
		offset_tmp = temp - offset_tmp;
		offset_tmp &= 0xFFFFFF;
	}
	else
 	{
		if(temp > offset_tmp)
			offset_tmp = temp - offset_tmp;
		else
		{
			offset_tmp = offset_tmp - temp;
			offset_tmp = 0x1000000 - offset_tmp;
 		}
	}
	return (offset_tmp & 0xFFFFFF);
}
#endif

#if E1240 | E1242
UINT32 calculate_offset(UINT32 err_high,UINT32 err_low,UINT32 offset){
	UINT32 offset_tmp;
	UINT32 err;
	err_high = err_high - CAL_point_H;
	err_low = err_low - CAL_point_L;
	err = (err_high + err_low) >> 1;

	if(err & 0x80000000){
		err = 0xffffffff - err;
		if(err > 2048)
			offset_tmp = offset - (err >> 2) - (err >> 3) + (err >> 5);
		else
			offset_tmp = offset - (err >> 2) ;
	}
	else{
		if(err > 2048)
			offset_tmp = offset + (err >> 2) + (err >> 3) - (err >> 5);
		else
			offset_tmp = offset + (err >> 2)  ;
	}
	return offset_tmp;
}

UINT32 calculate_offsetc(UINT32 err_high,UINT32 err_low,UINT32 offset){
	UINT32 offset_tmp;
	UINT32 err;
	err_high = err_high - CAL_point_H;
	err_low = err_low - CAL_point_L;
	err = (err_high + err_low) >> 1;

	if(err & 0x80000000){
		err = 0xffffffff - err;
		if(err > 2048)
			offset_tmp = offset - (err >> 2) - (err >> 3) + (err >> 4) + (err >> 5) + (err >> 7);
		else
			offset_tmp = offset - (err >> 2) + (err >> 4);
	}
	else{
		if(err > 2048)
			offset_tmp = offset + (err >> 2) + (err >> 3) - (err >> 4) - (err >> 5) - (err >> 7);
		else
			offset_tmp = offset + (err >> 2) - (err >> 4) ;
	}
	return offset_tmp;
}
#endif

/*** linearity test ***/

void diag_do_AI_linearity_test(UINT8 all_cal,UINT8 raw)
{
	UINT32 value = 0, sum = 0;
	UINT8 gain;
#if Input_signal
	UINT32 max=0x00000000, min=0x00ffffff;
#else
	UINT32 max=0x800001, min=0x7fffff;
#endif
	UINT8 command[32];
	int temp_high=0,temp_low=0;
	int i,j,ch;
	UINT8 input_type;

#if E1240 | E1242
	if(ADC_Register_Readback(2) & 0x07){
		input_type = Current_mode;
		gain = ADC_PGA_GAIN_4;
	}else {
		input_type = Voltage_mode;
		gain = ADC_PGA_GAIN_1;
	}
#endif
#if E1262 | E1260
	input_type = ADC_Register_Readback(2) & 0x07;
	gain = input_type;
#endif

#if Input_signal
	for (j=0;j<25;j++)
#else
	for (j=-12;j<13;j++)
#endif
	{
#if E1240 | E1242
		if(j == 0){
			switch(input_type){
			case Voltage_mode:
				sprintf(command,"%d.%04d",LINEARITY_Vo_LH,LINEARITY_Vo_LL);
				break;
			case Current_mode:
				sprintf(command,"%d.%03d",LINEARITY_Io_LH,LINEARITY_Io_LL);
				break;
			default:break;
			}
		}
		else if(j == 24){
			switch(input_type){
			case Voltage_mode:
				sprintf(command,"%d.%04d",LINEARITY_Vo_HH,LINEARITY_Vo_HL);
				break;
			case Current_mode:
				sprintf(command,"%d.%03d",LINEARITY_Io_HH,LINEARITY_Io_HL);
				break;
			default:break;
			}
		}else{
			switch(input_type){
			case Voltage_mode:
				temp_high = j*LINEARITY_Vo_point;
				temp_low = (j*LINEARITY_Vo_fpoint)%10000;
				sprintf(command,"%d.%04d",temp_high,temp_low);
				break;
			case Current_mode:
				temp_high = j*LINEARITY_Io_point + LINEARITY_Io_LOW;
				temp_low = (j*LINEARITY_Io_fpoint) + (LINEARITY_Io_LOW * 1000);
				temp_low %= 1000;
				sprintf(command,"%d.%03d",temp_high,temp_low);
				break;
			default:break;
			}
		}
#endif
#if E1262
		if(j == -12){
			switch(input_type){
			case 0:
				sprintf(command,"-%d.%03d",LINEARITY_VO_X1H,LINEARITY_VO_X1L);
				break;
			case 1:
				sprintf(command,"-%d.%03d",LINEARITY_VO_X2H,LINEARITY_VO_X2L);
				break;
			case 2:
				sprintf(command,"-%d.%03d",LINEARITY_VO_X4H,LINEARITY_VO_X4L);
				break;
			default:break;
			}
		}
		else if(j == 12){
			switch(input_type){
			case 0:
				sprintf(command,"%d.%03d",LINEARITY_VO_X1H,LINEARITY_VO_X1L);
				break;
			case 1:
				sprintf(command,"%d.%03d",LINEARITY_VO_X2H,LINEARITY_VO_X2L);
				break;
			case 2:
				sprintf(command,"%d.%03d",LINEARITY_VO_X4H,LINEARITY_VO_X4L);
				break;
			default:break;
			}
		}
		else if(j < 0){
			switch(input_type){
			case 0:
				temp_high = j*LINEARITY_VO_X1point;
				temp_low = -(j*LINEARITY_VO_X1fpoint)%1000;
				break;
			case 1:
				temp_high = j*LINEARITY_VO_X2point;
				temp_low = -(j*LINEARITY_VO_X2fpoint)%1000;
				break;
			case 2:
				temp_high = j*LINEARITY_VO_X4point;
				temp_low = -(j*LINEARITY_VO_X4fpoint)%1000;
				break;
			default:break;
			}
			sprintf(command,"-%d.%03d",(-temp_high),temp_low);
		}else if( j > 0){
			switch(input_type){
			case 0:
				temp_high = j*LINEARITY_VO_X1point;
				temp_low = (j*LINEARITY_VO_X1fpoint)%1000;
				break;
			case 1:
				temp_high = j*LINEARITY_VO_X2point;
				temp_low = (j*LINEARITY_VO_X2fpoint)%1000;
				break;
			case 2:
				temp_high = j*LINEARITY_VO_X4point;
				temp_low = (j*LINEARITY_VO_X4fpoint)%1000;
				break;
			default:break;
			}
			sprintf(command,"%d.%03d",temp_high,temp_low);
		}
		else
			sprintf(command,"0");
#endif
#if E1260
		if(j == 0){
			sprintf(command,"1");
		}
		else if(j == 24){
			switch(input_type){
			case 0:
				sprintf(command,"%d.%03d",LINEARITY_R_X1H,LINEARITY_R_X1L);
				break;
			case 1:
				sprintf(command,"%d.%03d",LINEARITY_R_X2H,LINEARITY_R_X2L);
				break;
			case 2:
				sprintf(command,"%d.%03d",LINEARITY_R_X4H,LINEARITY_R_X4L);
				break;
			case 3:
				sprintf(command,"%d.%03d",LINEARITY_R_X8H,LINEARITY_R_X8L);
				break;
			default:break;
			}
		}else{
			switch(input_type){
			case 0:
				temp_high = j*LINEARITY_R_X1point;
				sprintf(command,"%d",temp_high);
				break;
			case 1:
				temp_high = j*LINEARITY_R_X2point;
				sprintf(command,"%d",temp_high);
				break;
			case 2:
				temp_high = j*LINEARITY_R_X4point;
				sprintf(command,"%d",temp_high);
				break;
			case 3:
				temp_high = j*LINEARITY_R_X8point;
				temp_low = (j*LINEARITY_R_X8fpoint)%1000;
				sprintf(command,"%d.%03d",temp_high,temp_low);
				break;
			default:break;
			}
		}
#endif
		Calibrator_Set_Output_value(command);
		Calibrator_out();
		sleep(2000);
		if(all_cal)	ch = 0;
		else ch =CHANNEL_NUMBER-1;
		do{
#if E1240 | E1242
			if(all_cal)
				Printf(" %d , %d, %s, ",ch, input_type,command);
			else
				Printf(" %d , %d, %s, ",Current_AI_Channel,input_type,command);
#endif
#if E1262 | E1260
			if(all_cal)
				Printf(" %d , %d, %s, ",ch, 1 << input_type,command);
			else
				Printf(" %d , %d, %s, ",Current_AI_Channel,1 << input_type,command);
#endif
			if(all_cal){
				AI_CH_Select(ch);
#if E1240 | E1242
				if(input_type == Current_mode)
					cal_parameters_load(ch, Current_mode);
				else
					cal_parameters_load(ch, Voltage_mode);
#endif
#if E1262 | E1260
				cal_parameters_load(ch, input_type);
#endif
			}
			else{
#if E1240 | E1242
				if(input_type == Current_mode)
					cal_parameters_load(Current_AI_Channel, Current_mode);
				else
					cal_parameters_load(Current_AI_Channel, Voltage_mode);
#endif
#if E1262 | E1260
				cal_parameters_load(Current_AI_Channel, input_type);
#endif
			}
			sleep(20);
			ADC_Data_Sync();
			i=0;
#if Input_signal
			max=0x0;
			min=0xffffffff;
#else
			max=0x800001;
			min=0x7fffff;
#endif
			sum=0;
			while(!ADC_Data_Ready());
			do{
				value = ADC_Read() & 0xFFFFFF;
				if (i>3){
					max=CompareMAX(value,max);
					min=CompareMIN(value,min);
#if (Input_signal == 0)
					if(value > 0x800000)
						value |= 0xff000000;
#endif
					sum += value;
				}
				while(!ADC_Data_Ready());
				i++;
			}while(i<20);

			if(raw){
				value = sum >> 4;
#if (Input_signal == 0)
				if(value & 0x800000){
					sum = 0xffffffff - sum + 1;
					value = sum >> 4;
					sum = AI_data_transfer(value,gain);
					Printf("-%d.%03d, ",sum/1000,sum%1000);
				}
				else{
#endif
					sum = AI_data_transfer(value,gain);
					Printf("%d.%03d, ",sum/1000,sum%1000);
#if (Input_signal == 0)
				}
#endif	
				value=Compare_different(max,min);
#if (Input_signal == 0)
				if(max & 0x800000){
					max = AI_data_transfer((0x1000000-max),gain);
					Printf("-%d.%03d, ",max/1000,max%1000);
				}
				else{
#endif
					max = AI_data_transfer(max,gain);
					Printf("%d.%03d, ",max/1000,max%1000);
#if (Input_signal == 0)
				}

				if(min & 0x800000){
					min = AI_data_transfer((0x1000000-min),gain);
					Printf("-%d.%03d, ",min/1000,min%1000);
				}
				else{
#endif
					min = AI_data_transfer(min,gain);
					Printf("%d.%03d, ",min/1000,min%1000);
#if (Input_signal == 0)
				}
#endif
				Printf("%d\r\n",value);
			}
			else{
#if (Input_signal == 0)
				if(max & 0x800000)
					max |= 0xff000000;
				if(min & 0x800000)
					min |= 0xff000000;
#endif
				sum = sum >> 4;
#if (Input_signal == 0)
				if(sum & 0x800000)
					sum |= 0xff000000;
#endif
				Printf("%d, %d, %d, %d\r\n", sum, max, min, max-min);
			}
			sleep(10);
			ch++;
		}while(ch < CHANNEL_NUMBER);
		if(diag_check_press_ESC() == DIAG_ESC) return;
	}
}

void diag_do_AI_linearity_ch_test(void){
	UINT8 raw=0,ch=0,input_type;
    UINT32 ret=0;
#if E1260
	UINT8 command[32];
#endif

#if E1240 | E1242
	if(diag_get_value("Linearity test AI Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	AI_TYPE_CH_Select((UINT8)ret);
#endif
#if E1262
	if(diag_get_value("Linearity test TC Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
#if E1260
	if(diag_get_value("Linearity test RTD Channel is ... => ",&ret,0,CHANNEL_NUMBER-1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
#endif
	AI_CH_Select((UINT8)ret);
	ch = (UINT8)ret;
  	if(diag_get_value("Data Format: (0) Raw data (1) Data Transfer: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	raw = (UINT8)ret;
#if E1240 | E1242
	for(input_type = 0; input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++){
		switch(input_type){
		case Voltage_mode:
			if(AI_TYPE_READ() != Voltage_mode){ /*Current mode is high Voltage mode is low*/
				Printf("\r\nPlease Check Channel %d is Voltage mode\r\n",ch);
				Printf("Press any key to continue! \r\n");
				while(!Kbhit());
				if(Getch() == 27) return;
			}
			Calibrator_Set_State(DCV);
			CA150_Set_Source_Range_func(DCV_10V);
			Calibrator_out();
			ADC_PGA_Gain(ADC_PGA_GAIN_1);	break;
		case Current_mode:
			if(AI_TYPE_READ() != Current_mode){ /*Current mode is high Voltage mode is low*/
				Printf("\r\nPlease Check Channel %d is Current mode\r\n",ch);
				Printf("Press any key to continue! \r\n");
				while(!Kbhit());
				if(Getch() == 27) return;
			}
			Calibrator_Set_State(DCA);
			CA150_Set_Source_Range_func(DCA_4_20mA);
			Calibrator_out();
			ADC_PGA_Gain(ADC_PGA_GAIN_4);	break;
		default:	break;
		}
		Printf(" Channel, Input_type, Point, Average, Max, Min ,Var\r\n");
		Printf("--------------------------------------------------\r\n");
		diag_do_AI_linearity_test(0,raw);
	}
#endif
#if E1262
	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");

	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();

	for(input_type = 0; input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++){
		ADC_PGA_Gain(input_type);
		diag_do_AI_linearity_test(0,raw);
	}
#endif
#if E1260
	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");

	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();

	for(input_type = 0; input_type<SINGLE_CHANNEL_EEPROM_DATA;input_type++){
		ADC_PGA_Gain(input_type);
		diag_do_AI_linearity_test(0,raw);
	}
#endif
}

#if E1240 | E1242
void diag_do_AI_Voltage_linearity_test(void){
    UINT32 ret=0;
	UINT8 i;

  	if(diag_get_value("Data Format: (0) Raw data (1) Voltage: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain(ADC_PGA_GAIN_1);
	for(i=0; i<CHANNEL_NUMBER;i++){
		AI_TYPE_CH_Select(i);
		sleep(1);
		if(AI_TYPE_READ() != Voltage_mode){
			Printf("Please Check Channel %d is Voltage mode\r",i);
			i--;
			while(!Kbhit());
			if(Getch() == 27) return;
		}			
	}
	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_10V);
	Calibrator_out();
	Printf("\r\n Channel, Input_type, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	diag_do_AI_linearity_test(1,(UINT8)ret);
}

void diag_do_AI_Current_linearity_test(void)
{
	UINT8 i;
	UINT32 ret=0,channel=0;
 	if(diag_get_value("Linearity Method is => (0)Single channel (1) All channels",&channel,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}

  	if(diag_get_value("Data Format: (0) Raw data (1) Milliamp: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain(ADC_PGA_GAIN_4);
	Calibrator_Set_State(DCA);
	CA150_Set_Source_Range_func(DCA_4_20mA);

	Printf(" Channel, Input_type, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	if(channel){
		for(i=0; i<CHANNEL_NUMBER;i++){
			AI_CH_Select(i);
			AI_TYPE_CH_Select(i);
			sleep(1);
			if(AI_TYPE_READ() != Current_mode){
				Printf("Please Check Channel %d is Current mode\r",i);
				i--;
				while(!Kbhit());
				if(Getch() == 27) return;
			}			
		}
		Calibrator_out();
		diag_do_AI_linearity_test(1,(UINT8)ret);
	}
	else{
		for(i=0; i<CHANNEL_NUMBER;i++){
			AI_CH_Select(i);
			AI_TYPE_CH_Select(i);
			sleep(1);
			if(AI_TYPE_READ() != Current_mode){
				Printf("Please Check Channel %d is Current mode\r",i);
				i--;
				while(!Kbhit());
				if(Getch() == 27) return;
			}			
			else{
				Calibrator_out();
				diag_do_AI_linearity_test(0,(UINT8)ret);
			}
		}
	}
}
#endif

#if E1262
void diag_do_TC_full_linearity_test(void){
	UINT8 gain,raw=0;
	UINT32 ret=0;

  	if(diag_get_value("Data Format: (0) Raw data (1) Millivolt: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	raw = (UINT8)ret;

	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();

	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	for (gain=0; gain<3; gain++){
		ADC_PGA_Gain(gain);
		diag_do_AI_linearity_test(1,raw);
	}
}

void diag_do_TC_gain_linearity_test(void){
	UINT32 ret=0;
	UINT8 raw=0;

  	if(diag_get_value("Linearity test TC Gain is ... (0:X1 / 1:X2 / 2:X4) => ",&ret,0,2,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain((UINT8)ret);
  	if(diag_get_value("Data Format: (0) Raw data (1) Millivolt: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	raw = (UINT8)ret;
	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	Calibrator_Set_State(DCV);
	CA150_Set_Source_Range_func(DCV_100mV);
	Calibrator_out();
	diag_do_AI_linearity_test(1,raw);
}
#endif

#if E1260
void diag_do_RTD_full_linearity_test(void){
	UINT8 gain,raw=0;
	UINT32 ret=0;
	UINT8 command[8];

  	if(diag_get_value("Data Format: (0) Raw data (1) Ohm : ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	raw = (UINT8)ret;

	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();

	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	for (gain=0; gain<SINGLE_CHANNEL_EEPROM_DATA; gain++){
		ADC_PGA_Gain(gain);
		diag_do_AI_linearity_test(1,raw);
	}
}

void diag_do_RTD_gain_linearity_test(void){
	UINT32 ret=0;
	UINT8 raw=0;
	UINT8 command[8];

  	if(diag_get_value("Linearity test RTD Gain is ... (0:X1 / 1:X2 / 2:X4 / 3:X8) => ",&ret,0,SINGLE_CHANNEL_EEPROM_DATA,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	ADC_PGA_Gain((UINT8)ret);
  	if(diag_get_value("Data Format: (0) Raw data (1) Millivolt: ... => ",&ret,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
	raw = (UINT8)ret;
	Printf(" Channel, Gain, Point, Average, Max, Min ,Var\r\n");
	Printf("--------------------------------------------------\r\n");
	sprintf(command,"0");
	Calibrator_Set_Output_value(command);
	Calibrator_out();
	diag_do_AI_linearity_test(1,raw);
}
#endif

/******************************************************/
/*                   EMI and EMS test                 */
/******************************************************/

void diag_do_AI_EMI_scan_test(void)
{
	msgstr_t msgt;	// tx memory
	int i,j=0,k=0;
	UINT32 value = 0;
	UINT32 Value[CHANNEL_NUMBER][4];
	UINT32 ave[CHANNEL_NUMBER];
	uchar buf[8]="55AA00FF";

	MemInit();							// initialize Ethernet UDP 
	ArpInit();
	G_LanPort = 1;
	mac_open(G_LanPort,MODE_NORMAL);
	Printf("\r\nEthernet Initialized!\r\n");
	ConUdpInit(0,921600);

	EXSPI_INIT();
	sleep(10);
	ADC_SW_Reset();
	sleep(10);
	ADC_Init();
#if E1240 | E1242
	Printf("\r\n<<< AI EMI Test >>>\r\n");
#endif
#if E1262
	Printf("\r\n<<< TC EMI Test >>>\r\n");
#endif
#if E1260
	Printf("\r\n<<< RTD EMI Test >>>\r\n");
#endif

	for(i=0;i<CHANNEL_NUMBER;i++)
	{
		ave[i]=0;
		for(j=0;j<4;j++)
			Value[i][j]=0;
	}

	j=0;
	sleep(20);
	do{
		
//		diag_do_DIO_EMS_test(0x55);
//		diag_do_DIO_EMS_test(0xAA);
		ADC_Data_Sync();
		for(i=0;i<6;i++)
		{
			while(!ADC_Data_Ready());
			value = ADC_Read() & 0xFFFFFF;
		}
		Value[j][k]=value;
		ave[j]=0;
		for(i=0;i<4;i++)
			ave[j]+=Value[j][i];
		ave[j] >>= 2;
		
		j++;
		if (j == CHANNEL_NUMBER){
			j=0;
			k++;
			if (k == 4) k=0;
		}

		AI_CH_Select(j);
		for(i=0;i<CHANNEL_NUMBER;i++) Printf("[%06x] ",ave[i]);

		Printf("\r");
//		Printf("[%06x] [%06x] [%06x] [%06x] [%06x] [%06x] [%06x] [%06x]\r",ave[0],ave[1],ave[2],ave[3],ave[4],ave[5],ave[6],ave[7]);
		sleep(15);
		msgt=MemAlloc(128); 
		
		/* Allocate MAC TX Buffer */
		for (i=0;i<6;i++)	msgt->base[msgt->wndx++] = 0xff;				// destination address
		for (i=6;i<12;i++)	msgt->base[msgt->wndx++] = MacSrc[1][i-6];	// source address
		for (i=0;i<(116);i++){
			msgt->base[msgt->wndx++] = buf[i%8];				// fill with Packet
		}

		mac_send(1,msgt);
		MemFree(msgt);
		sleep(100);
	}while(!Kbhit());
	Getch();
}


extern char EEPROM_address;
void diag_do_AI_EMS_scan_test(void)
{
	msgstr_t msgt;	// tx memory
	int i,j=0,k=0,m=0;
	UINT32 value = 0;
	UINT32 Value[CHANNEL_NUMBER][4];
	UINT32 ave[CHANNEL_NUMBER];
	UINT32 time_out = 0;
	UINT8 fault	= 0;
	uchar buf[8]="55AA00FF";
	UINT32 limit_high[CHANNEL_NUMBER];
	UINT32 limit_low[CHANNEL_NUMBER];

#if E1240
	limit_high[0]=0x400000;limit_high[1]=0x600000;limit_high[2]=0x400000;limit_high[3]=0x900000;
	limit_high[4]=0x400000;limit_high[5]=0x600000;limit_high[6]=0x400000;limit_high[7]=0x900000;
	limit_low[0]=0x100000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;
	limit_low[4]=0x100000;limit_low[5]=0x300000;limit_low[6]=0x100000;limit_low[7]=0x600000;
#endif
#if E1262
	limit_high[0]=0x400000;limit_high[1]=0x600000;limit_high[2]=0x400000;limit_high[3]=0x900000;
	limit_high[4]=0x400000;limit_high[5]=0x600000;limit_high[6]=0x400000;limit_high[7]=0x900000;
	limit_low[0]=0x100000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;
	limit_low[4]=0x100000;limit_low[5]=0x300000;limit_low[6]=0x100000;limit_low[7]=0x600000;
#endif
#if E1260
/*	limit_high[0]=0x400000;limit_high[1]=0x600000;limit_high[2]=0x400000;limit_high[3]=0x900000;
	limit_high[4]=0x400000;limit_high[5]=0x600000;
	limit_low[0]=0x100000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;
	limit_low[4]=0x100000;limit_low[5]=0x300000;
*/
	// RTD:330 ohm
/*	limit_high[0]=0x250000;limit_high[1]=0x250000;limit_high[2]=0x250000;limit_high[3]=0x250000;
	limit_high[4]=0x250000;limit_high[5]=0x250000;
	limit_low[0]=0x150000;limit_low[1]=0x150000;limit_low[2]=0x150000;limit_low[3]=0x150000;
	limit_low[4]=0x150000;limit_low[5]=0x150000;
*/
		// RTD: CH0/3 -> 1K; CH1/4 -> 500 ohm; CH2/5 -> 250 ohm
	limit_high[0]=0x750000;limit_high[1]=0x400000;limit_high[2]=0x200000;limit_high[3]=0x750000;
	limit_high[4]=0x400000;limit_high[5]=0x200000;
	limit_low[0]=0x600000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;
	limit_low[4]=0x300000;limit_low[5]=0x100000;

#endif
#if E1242
//	limit_high[0]=0x400000;limit_high[1]=0x600000;limit_high[2]=0x400000;limit_high[3]=0x900000;
//	limit_low[0]=0x100000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;

	limit_high[0]=0x400000;limit_high[1]=0x600000;limit_high[2]=0x400000;limit_high[3]=0x900000;
	limit_low[0]=0x100000;limit_low[1]=0x300000;limit_low[2]=0x100000;limit_low[3]=0x600000;
#endif


	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);

	MemInit();							// initialize Ethernet UDP 
	ArpInit();
	G_LanPort = 1;
	GpioSetDir(PIO_WADG ,PIO_IN);
//	GpioSetDir(PIO_WADG ,PIO_OUT);
	mac_open(G_LanPort,MODE_NORMAL);
	Printf("\r\nEthernet Initialized!\r\n");
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	ConUdpInit(0,921600);

	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
	i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
	EEPROM_address = PARMETER_EEPROM_WRITE;

	EXSPI_INIT();
	sleep(10);
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	ADC_SW_Reset();
	sleep(10);
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	ADC_Init();

	ADC_PGA_Gain(0);		// load offset/gain parameters from EEPROM
#if E1240 | E1242
	Printf("\r\n<<< AI EMC Test >>>\r\n");
#endif
#if E1262
	Printf("\r\n<<< TC EMC Test >>>\r\n");
#endif
#if E1262
	Printf("\r\n<<< RTD EMC Test >>>\r\n");
#endif
	GpioWriteData(PIO_SW_READY,0);
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	for(i=0;i<CHANNEL_NUMBER;i++)
	{
		ave[i]=0;
		for(j=0;j<4;j++)
			Value[i][j]=0;
	}
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);

	j=0;
	AI_CH_Select(j);
	GpioSetDir(PIO_WADG ,PIO_IN);
	GpioSetDir(PIO_WADG ,PIO_OUT);
	sleep(20);
	do{
//		diag_do_DIO_EMS_test(0x55);
//		diag_do_DIO_EMS_test();
		ADC_Data_Sync();
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		for(i=0;i<scan_rate;i++){
			time_out=0;
			while(!ADC_Data_Ready() && time_out<10000)
			{
				time_out++;
				if (time_out == 10000)
					Printf("\r\nADC Data Ready Time Out.....");
			}
			value = ADC_Read() & 0xFFFFFF;
			GpioSetDir(PIO_WADG ,PIO_IN);
			GpioSetDir(PIO_WADG ,PIO_OUT);
		}
		Value[j][k]=value;

		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);

		ave[j]=0;
		for(i=0;i<4;i++)
			ave[j]+=Value[j][i];
		ave[j] >>= 2;

		if (m > 29) {			
			if((ave[j] > limit_high[j]) || (ave[j] < limit_low[j]))
				fault |= 0x1 << j;
			else
				fault &= ~(0x1 << j);
			for(i=0;i<CHANNEL_NUMBER;i++)
				Printf("[%06x] ",ave[i]);
			Printf("\r");

			if(fault)	GpioWriteData(PIO_SW_READY,1);
			else	GpioWriteData(PIO_SW_READY,0);
		}
		else {
			m++;
			GpioWriteData(PIO_SW_READY,0);
		}
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		sleep(50);
		GpioWriteData(PIO_SW_READY,1);
		j++;

//		if (j == 8) 
		if (j == CHANNEL_NUMBER) 
		{
			j=0;
			k++;
			if (k == 4) k=0;
		}

		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		AI_CH_Select(j);
		sleep(15);
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		msgt=MemAlloc(128); 
		
		/* Allocate MAC TX Buffer */
		for (i=0;i<6;i++)	msgt->base[msgt->wndx++] = 0xff;				// destination address
		for (i=6;i<12;i++)	msgt->base[msgt->wndx++] = MacSrc[1][i-6];	// source address
		for (i=0;i<(116);i++){
			msgt->base[msgt->wndx++] = buf[i%8];				// fill with Packet
		}
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);

		mac_send(1,msgt);
		GpioSetDir(PIO_WADG ,PIO_IN);
		GpioSetDir(PIO_WADG ,PIO_OUT);
		MemFree(msgt);
		
//		diag_do_DIO_EMS_test(0x55);
//		sleep(5);
#if E1242
		diag_do_DIO_EMS_test();
		sleep(5);
#endif
						
	}while(!Kbhit());
	GpioSetDir(PIO_WADG ,PIO_IN);
	Getch();
	GpioWriteData(PIO_SW_READY,1);
}

/******************************************************/
/*                    Burn-in test                    */
/******************************************************/

UINT8 ratio_data(UINT32 data1,UINT32 data2){
	if(data1 > data2)
		return (data1/data2);
	else
		return (data2/data1);
}

UINT32 ADC_offset_return(void){
	UINT8 offset_ee[3];
	UINT32 offset;

	offset_ee[0] = ADC_Register_Readback(0xA);
	offset_ee[1] = ADC_Register_Readback(0xB);
	offset_ee[2] = ADC_Register_Readback(0xC);
	offset = (offset_ee[2]<<16)+(offset_ee[1]<<8)+offset_ee[0];
	return offset;
}

UINT32 ADC_gain_return(void){
	UINT8 gain_ee[3];
	UINT32 gain;

	gain_ee[0] = ADC_Register_Readback(0xD);
	gain_ee[1] = ADC_Register_Readback(0xE);
	gain_ee[2] = ADC_Register_Readback(0xF);
	gain = (gain_ee[2]<<16)+(gain_ee[1]<<8)+gain_ee[0];
	return gain;
}

#if E1240 | E1242
UINT8 diag_burn_in_AI(void)
{
	int i,j;
	UINT32 Value[CHANNEL_NUMBER];
	UINT8 error=0,ratio;
	UINT32 time_out = 0;

	for(i=0;i<CHANNEL_NUMBER;i++){
		AI_CH_Select(i);
		ADC_Data_Sync();
		sleep(14);
		for(j=0;j<6;j++){
			time_out=0;
			while(!ADC_Data_Ready() && time_out<10000)
			{
				time_out++;
				if (time_out == 10000)
					Printf("\r\nADC Data Ready Time Out.....");
			}
			Value[i] = ADC_Read() & 0xFFFFFF;
		}

		Value[i] = AI_data_transfer(Value[i],Voltage_mode);
		Printf("[%02d.%03d]", Value[i]/1000,Value[i]%1000);
	}

	if ((Value[0] < 500) || (Value[0] > 9500)){
		error |= (0x01 << 0);
	}
	ratio = ratio_data(Value[1],Value[0]);
	if (ratio > 3 || ratio < 1 ){
		error |= (0x01 << 1);
	}
	else if(Value[1] > 9500){
		error |= (0x01 << 1);
	}

	ratio = ratio_data(Value[2],Value[0]);
	if (ratio > 2){
		error |= (0x01 << 2);
	}
	ratio = ratio_data(Value[3],Value[0]);
	if (ratio > 4 || ratio < 2){
		error |= (0x01 << 3);
	}
	i=0;
#if E1240
	do{
		if (Compare_different(Value[i],Value[i+4]) > 500){
			error |= (0x10<< i);
		}
		i++;
	}while(i<4);
#endif
	
	Printf("\r\n");
	return error;
}
#endif

#if E1262
UINT8 diag_burn_in_TC(UINT8 fix_gain)
{
	int i,j;

	UINT8 gain[CHANNEL_NUMBER]={2,1,0,0,2,1,0,0};
	UINT32 Value[CHANNEL_NUMBER];
	UINT8 error=0,ratio;
	UINT32 time_out = 0;

	if(fix_gain){
		Printf("Fix Gain = 2^0          : ");
		gain[0]=0;gain[1]=0;gain[4]=0;gain[5]=0;
	}
	else
		Printf("Change Gain 2^2,2^1,2^0 : ");


	for(i=0;i<CHANNEL_NUMBER;i++){
		if(fix_gain)
			AI_CH_Select(i);
		else
			copy_ADC_RAM_to_REG(i);
		ADC_Data_Sync();
		sleep(14);
		for(j=0;j<6;j++){
			time_out=0;
			while(!ADC_Data_Ready() && time_out<10000)
			{
				time_out++;
				if (time_out == 10000)
					Printf("\r\nADC Data Ready Time Out.....");
			}
			Value[i] = ADC_Read() & 0xFFFFFF;
		}
		if(Value[i] & 0x800000){
			Value[i] = AI_data_transfer((0x1000000-Value[i]),gain[i])/1000;
			Value[i] = 0xFFFFFFFF - Value[i] + 1;
		}
		else
			Value[i] = AI_data_transfer(Value[i],gain[i])/1000;
		Printf("[%d]", Value[i]);
	}

	if ((Value[0] < 1) || (Value[0] > 30)){
		error |= (0x01 << 0);
	}
	ratio = ratio_data(Value[1],Value[0]);
	if (ratio > 2 || ratio < 1 ){
		error |= (0x01 << 1);
	}
	else if(Value[1] > 39){
		error |= (0x01 << 1);
	}

	ratio = ratio_data(Value[2],Value[0]);
	if (ratio > 5 || ratio < 3){
		error |= (0x01 << 2);
	}
	if ((Value[3] < 0xff00000)){
		error |= (0x01 << 3);
	}
	i=0;
	do{
		if (Compare_different(Value[i],Value[i+4]) > 3){
			error |= (0x10<< i);
		}
		if(i==3){
			if(Value[7] < 79){
				error |= (0x10<< i);
			}
		}
		else if(Value[i+4] > 77){
			error |= (0x10<< i);
		}
		i++;
	}while(i<4);
	
	Printf("\r\n");
	return error;
}
#endif

#if E1260
UINT8 diag_burn_in_RTD(UINT8 fix_gain)
{
	int i,j;
	int ch;
	UINT8 gain[CHANNEL_NUMBER]={1,1,1,1,1,1};
//	UINT8 gain[CHANNEL_NUMBER]={0,1,2,0,1,2};
	UINT32 Value[CHANNEL_NUMBER];
	UINT8 error=0,ratio;
	UINT32 time_out = 0;
/*
	if(diag_get_value("fix_gain: (0) non-fix (1) fix : ",&fix_gain,0,1,DIAG_GET_VALUE_DEC_MODE) == DIAG_ESC){return;}
*/
	if(fix_gain){
		Printf("Fix Gain = 2^0          : ");
		gain[0]=0;gain[1]=0;gain[2]=0;gain[3]=0;gain[4]=0;gain[5]=0;
//			ADC_PGA_Gain(ADC_PGA_GAIN_4);
	}
	else{
		Printf("Change Gain 2^1,2^1,2^1 : ");
//		Printf("Change Gain             : ");
		gain[0]=1;gain[1]=1;gain[2]=1;gain[3]=1;gain[4]=1;gain[5]=1;
	}

	Printf("\r\n");
//	do{
		sleep(500);
	for(i=0;i<CHANNEL_NUMBER;i++){
		if(fix_gain){
			AI_CH_Select(i);
		}
		else{
//					ADC_PGA_Gain(gain[i]);
//		copy_ADC_REG_to_RAM(i);
//			copy_ADC_RAM_to_REG(i);
				AI_CH_Select(i);
		}
		ADC_Data_Sync();
		sleep(14);
		for(j=0;j<6;j++){
			time_out=0;
			while(!ADC_Data_Ready() && time_out<10000)
			{
				time_out++;
				if (time_out == 10000)
					Printf("\r\nADC Data Ready Time Out.....");
			}
			Value[i] = ADC_Read() & 0xFFFFFF;
		}
		if (Value[i] == 0xFFFFFF){
			Printf("[Burn-Out ]");
		}
		else{
			Value[i] = AI_data_transfer(Value[i],gain[i]);
			Printf("[%02d.%03d]", Value[i]/1000,Value[i]%1000);
		}
	}

//----------------------------------------------------------------------
//		RTD Burn-in tool PCB Resistor value:
//
//		Ch0, Ch3: 1Kohm		Ch1, Ch4: 500 ohm		Ch2, Ch5: 250 ohm
//
//----------------------------------------------------------------------
//	if(fix_gain){
		if ((Value[0]/1000 < 100) || (Value[0]/1000 >2100)){
			error |= (0x01 << 0);
			Printf ("\r\n RTD Channel - 01 error = %x",error);
		}
		ratio = ratio_data(Value[1],Value[0]);
		if (ratio > 3 || ratio < 1 ){
			error |= (0x01 << 1);
			Printf ("\r\n RTD Channel - 01 or 02 error = %x",error);
		}
/*	else if((Value[1]/1000 > 700) || (Value[1]/1000 < 200)){
		error |= (0x01 << 1);
		Printf ("\r\n RTD Channel - 1 error = %x",error);
	}
*/
		ratio = ratio_data(Value[2],Value[0]);
		if ((ratio > 5) || (ratio <3)){
			error |= (0x01 << 2);
			Printf ("\r\n RTD Channel - 01 or 03 error = %x",error);
		}
/*	}
	else{
		if ((Value[0]/1000 < 100) || (Value[0]/1000 >2100)){
			error |= (0x01 << 0);
			Printf ("\r\n Gain RTD Channel - 01 error = %x",error);
		}
		ratio = ratio_data(Value[1],Value[0]);
		if (ratio > 1){
			error |= (0x01 << 1);
			Printf ("\r\n Gain RTD Channel - 01 or 02 error = %x",error);
		}

		ratio = ratio_data(Value[2],Value[0]);
		if ((ratio > 3) || (ratio <1)){
			error |= (0x01 << 2);
			Printf ("\r\nGain RTD Channel - 01 or 03 error = %x",error);
		}
	}*/
	i=0;
#if E1260
	do{
//		Printf ("\r\nCompare_different Value[%d],Value[%d]= %d",i,i+3,Compare_different(Value[i],Value[i+3])/1000);
//			Printf ("\r\nValue[%d] = %d",i,Value[i]);
//		if (Compare_different(Value[i],Value[i+3])/1000 > 2){
			ratio = ratio_data(Value[i],Value[i+3]);
			Printf ("\r\nratio:Value[%d], Value[%d] = %d",i,i+3,ratio);
			if ((ratio > 2)){
			error |= (0x01<< (i+3));
			Printf ("\r\nerror %d",i+3);
		}
		i++;
	}while(i<3);
#endif


/*
	if ((Value[0] < 500) || (Value[0] > 9500)){
		error |= (0x01 << 0);
	}
	ratio = ratio_data(Value[1],Value[0]);
	if (ratio > 3 || ratio < 1 ){
		error |= (0x01 << 1);
	}
	else if(Value[1] > 9500){
		error |= (0x01 << 1);
	}

	ratio = ratio_data(Value[2],Value[0]);
	if (ratio > 2){
		error |= (0x01 << 2);
	}
	ratio = ratio_data(Value[3],Value[0]);
	if (ratio > 4 || ratio < 2){
		error |= (0x01 << 3);
	}
*/
	i=0;
	
	Printf("\r\n");
/*	
		if(Kbhit())
		ch = Getch();
	}while(ch != 27);
*/
	return error;
}
#endif

/******************************************************/
/*                         MP test                    */
/******************************************************/

int diag_chip_ADC(void)
{
	UINT32 value = 0;
	UINT8 ref_err = 0, adc_err=0, noise_err=0;
	UINT32 max=0,min=0xffffff;
	int i=0;
	UINT32 var;

	ADC_Calibration(0);									// self calibration
	sleep(1);
	ADC_Ref_Select(ADC_INT_REF, ADC_INT_REF_1_25V);		// internal reference 2.5V
	sleep(1);
	ADC_input_type_Select(1);	//unipolar
	Printf("\r\n");

	ADC_CH_Select(AnalogGnd,AnalogCom);									// 0V read back
	sleep(10);
	value = ADC_read_a_value(5);
	Printf("0V reading ........... %d\r\n", value);
	if (value>10000)
	{
		Printf("ADC 0V check error!\r\n");
		adc_err++;
	}

	ADC_CH_Select(EXT_VREF,AnalogCom);									// Full scale read back
	sleep(10);
	value = ADC_read_a_value(5);
	Printf("Internal ref. full scale reading ... %d\r\n", value);
	if (value==0)
		ref_err = 1;
	else if ((0xffffff-value)>10000)
	{
		Printf("ADC full-scale check error!\r\n");
		adc_err++;
	}

	ADC_CH_Select(INT_VREF,AnalogCom);									// half scale read back
	sleep(10);
	value = ADC_read_a_value(60);
	Printf("Half scale reading ... %d\r\n", value);
	if ((value>9500000) || (value<7000000))
	{
		Printf("ADC half-scale check error!\r\n");
		adc_err++;
	}

	ADC_CH_Select(ADR03_TEMP,AnalogCom);									// ADR03 temperature read back
	sleep(10);
	value = ADC_read_a_value(10);
	Printf("Temp sensor reading .. %d\r\n", value);
	if (value==0)
		ref_err = 1;
	else if ((value>4500000) || (value<3500000))
	{
		Printf("Temp sensor check error!\r\n");
		adc_err++;
	}

	ADC_Ref_Select(ADC_EXT_REF, ADC_INT_REF_1_25V);		// external reference 2.5V
	ADC_CH_Select(EXT_VREF,AnalogCom);									// Full scale read back
	sleep(10);
	value = ADC_read_a_value(5);
	Printf("External ref. full scale reading ... %d\r\n", value);
	if (value< 0x800000)
		ref_err = 1;

	ADC_CH_Select(AnalogGnd,AnalogCom);
	value = ADC_offset_return();
	value -= 0x100;
	max = 0x005a0000 | (value & 0x000000ff);
	SPI_Tx(PIO_ADC_CS,max,24);
	max = 0x005b0000 | ((value & 0x0000ff00)>>8);
	SPI_Tx(PIO_ADC_CS,max,24);
	max = 0x005c0000 | ((value & 0x00ff0000)>>16);
	SPI_Tx(PIO_ADC_CS,max,24);

	max=0;
	sleep(20);
	ADC_Data_Sync();
	do{
		while(!ADC_Data_Ready());
		value = ADC_Read() & 0xFFFFFF;
		if(i > 4){
			if(value > max)	max = value;
			if(value < min) min = value;
		}
		i++;
	}while(i<36);

	var = max - min;
	Printf("Max = %d, Min = %d, Noise = %d\r\n", max,min,var);
	if(var > 512)
		noise_err++;

	ADC_Calibration(0);									// self calibration
	sleep(1);
	ADC_CH_Select(AnalogP, AnalogN);
	if (ref_err)
	{
		Printf("Function error! ......................................... ADR03\r\n");
		return -1;
	}
	else if (adc_err)
	{
		Printf("Function error! ......................................... ADS1216\r\n");
		return -2;
	}
	else if(noise_err)
	{
		Printf("Function error! ......................................... Ground noise\r\n");
		return -3;
	}
	else
	{
		Printf("ADS1216 chip function ................................... Pass !\r\n");
		Printf("ADR03   chip function ................................... Pass !\r\n");
		Printf("Ground noise function ................................... Pass !\r\n");
		return 0;
	}
}

int diag_chip_SPI(void)
{
	if (ADC_Register_Readback(0) == 0x84)
	{
		Printf("SPI function ............................................ Pass !\r\n");
		return 0;
	}
	else
	{
		Printf("Function error! ......................................... SPI Interface!\r\n"); 
		return -1;
	}
}

extern UINT8 mptester_exist;
int diag_chip_Mux(void)
{					
	UINT32 value[CHANNEL_NUMBER];
	UINT8 i, err=0;
#if E1240 | E1242
	UINT32 current_value[CHANNEL_NUMBER];
	UINT8 relay;
#endif
#if E1262 | E1260
	UINT32 cmd_set_reg=0;
	UINT8 reg_read_back;
	UINT8 TEST_STEP;
#endif

#if	E1240 | E1242
	Printf("AI voltage mode test.........\r\n");
#endif
#if E1262
	ADC_input_type_Select(0);	//unipolar
	Printf("TC input test.........\r\n");
#endif
#if E1260
	Printf("RTD input test.........\r\n");
#endif
	if(mptester_exist == mp_time_out){
#if	E1240 | E1242		
		Printf("\r\nPress stir the channel 1~4 ON\r\n");
		Printf("All AI input states are voltage mode\r\n");
#endif

#if E1260
		Printf("\r\nPress stir the channel 1~4 ON\r\n");
		Printf("All RTD input channels read test\r\n");
#endif
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}

#if E1260
	else	mptest_GPIO_output(0x0000);
#else
	else	mptest_GPIO_output(0x000F);
#endif
					
#if E1240 | E1242
	if(mptester_exist != mp_time_out)
		mptest_relay(0xff);
#endif
	for(i=0;i<CHANNEL_NUMBER;i++)
	{
		AI_CH_Select(i);
		sleep(10);
		value[i] = ADC_read_a_value(10);
#if E1240 | E1242
		Printf("AI CH-%d : value = %d\r\n", i, value[i]);
#endif
#if E1262
		Printf("TC CH-%d : value = %d\r\n", i, value[i]);
#endif
#if E1260
		Printf("RTD CH-%d : value = %d\r\n", i, value[i]);
#endif
	}
	
#if E1240 | E1242

#if E1240
	if ((value[0]/value[4]) > 1) err++;
	if ((value[1]/value[5]) > 1) err++;
	if ((value[2]/value[6]) > 1) err++;
	if ((value[3]/value[7]) > 1) err++;
#endif

	if ((value[1]/value[0]) > 2 || (value[1]/value[0]) < 1) err++;
	if ((value[2]/value[0]) > 1 || (value[2]/value[0]) < 0) err++;
	if ((value[3]/value[0]) > 3 || (value[3]/value[0]) < 2) err++;

#if E1240
	if ((value[5]/value[4]) > 2 || (value[5]/value[4]) < 1) err++;
	if ((value[6]/value[4]) > 1 || (value[6]/value[4]) < 0) err++;
	if ((value[7]/value[4]) > 3 || (value[7]/value[4]) < 2) err++;
#endif

#endif

#if E1260
	if ((value[0]/value[3]) > 1) err++;
	if ((value[1]/value[4]) > 1) err++;
	if ((value[2]/value[5]) > 1) err++;

	if ((value[0]/value[1]) > 2 || (value[0]/value[1]) < 1){
		err++;
	}
	if ((value[0]/value[2]) > 4 || (value[0]/value[2]) < 3){
		err++;
	}
#endif

#if E1262
	if ((value[0]/value[4]) > 1) err++;
	if ((value[1]/value[5]) > 1) err++;
	if ((value[2]/value[6]) > 1) err++;
	if ((value[3]/value[7]) > 1) err++;

	if ((value[1]/value[0]) > 2 || (value[1]/value[0]) < 1) err++;
	if ((value[2]/value[0]) > 4 || (value[2]/value[0]) < 3) err++;
	if (value[3] < 0x800000) err++;
	if ((value[5]/value[4]) > 2 || (value[5]/value[4]) < 1) err++;
	if ((value[6]/value[4]) > 4 || (value[6]/value[4]) < 3) err++;
	if (value[7] < 0x800000) err++;
#endif

	if (err)
	{
		Printf("Function error! ......................................... Multiplexer !\r\n"); 
		return -1;
	}
	
#if E1262
	Printf("ADC's PGA function test.........\r\n");

	reg_read_back = ADC_Register_Readback(2);
	// read back analog control register
	reg_read_back &= 0xf8;
	// clear PGA gain bits

	for(i=0;i<3;i++)
	{
		AI_CH_Select(i);
		sleep(10);
		switch(i){
		case 0:
			cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 2);	// gain setting
			break;
		case 1:
			cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 1);	// gain setting
			break;
		case 2:
			cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 0);	// gain setting
			break;
		}
		SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

		sleep(10);
		value[i] = ADC_read_a_value(10);
		Printf("Gain 2^%d : value = %d\r\n", i, value[i]);
	}
	cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 0);	// gain setting
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	if ((value[0]/value[1]) > 1) err++;
	if ((value[1]/value[2]) > 1) err++;
	if (err)
	{
		Printf("Function error! ......................................... ADC PGA !\r\n"); 
		return -1;
	}

#endif



#if E1240 | E1242
	Printf("AI current mode test.........\r\n");

	for(i=0;i<4;i++)
	{	
		if(mptester_exist != mp_time_out){
			mptest_GPIO_output(0x1<<i);
		}
		else{
			Printf("Press stir the channel %d ON, Other channels OFF\r\n",(i+1));
			Printf("Press any key to continue\r\n");
			while(!Kbhit());
			Getch();
		}
		if(mptester_exist == mp_time_out){
			Printf("Press set AI channel %d Current mode, Other channels Voltage mode\r\n",i);
			Printf("Press any key to continue\r\n");
			while(!Kbhit());
			Getch();
		}
		else{
			relay = 0x1 << i;
			relay = ~(relay) & 0xff;
			mptest_relay(relay);
		}
		

		AI_CH_Select(i);
		sleep(10);
		current_value[i] = ADC_read_a_value(10);
		Printf("AI CH-%d : value = %d\r\n", i, current_value[i]);

		if(value[i] > current_value[i]){
			if(value[i] - current_value[i] < 2000)
				err++;
		}
		else{
			if(current_value[i] - value[i] < 2000)
				err++;
		}

		if (err)
		{
			Printf("Function error! ......................................... Current Mode !\r\n"); 
			return -1;
		}
		
		if(mptester_exist == mp_time_out){
			Printf("Press set AI channel %d Current mode, Other channels Voltage mode\r\n",i+4);
			Printf("Press any key to continue\r\n");
			while(!Kbhit());
			Getch();
		}
		else{
			relay = 0x1 << (i+4);
			relay = ~(relay) & 0xff;
			if(mptester_exist != mp_time_out)
				mptest_relay(relay);
		}

		AI_CH_Select(i+4);
		sleep(10);
		current_value[i+4] = ADC_read_a_value(10);
		Printf("AI CH-%d : value = %d\r\n", i+4, current_value[i+4]);
		
		if(value[i+4] > current_value[i+4]){
			if(value[i+4] - current_value[i+4] < 2000)
				err++;
		}
		else{
			if(current_value[i+4] - value[i+4] < 2000)
				err++;
		}
		if (err)
		{
			Printf("Function error! ......................................... Current Mode !\r\n"); 
			return -1;
		}
	}	
#endif
#if E1262
	Printf("TC burn-out test.........\r\n");
	if(mptester_exist == mp_time_out){
		Printf("\r\nPress stir the channel 1~4 OFF\r\n");
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
	else	mptest_GPIO_output(0x0000);

	for(i=0;i<CHANNEL_NUMBER;i++)
	{
		AI_CH_Select(i);
		Burnout_enable(); //enable
		sleep(10);
		if (Burnout_disable()){}
		else
			err++;
	}
	if (err)
	{
		Printf("Function error! ......................................... Burn Out test !\r\n"); 
		return -1;
	}
#endif
	Printf("Multiplexer function .................................... Pass !\r\n");

#if E1260
	Printf("ADC's PGA function test.........\r\n");

	reg_read_back = ADC_Register_Readback(2);
	// read back analog control register
	reg_read_back &= 0xf8;
	// clear PGA gain bits

	for(i=0;i<3;i++)
	{
		AI_CH_Select(i);
		sleep(10);
		switch(i){
			case 0:
				cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 0);	// gain setting
				break;
			case 1:
				cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 1);	// gain setting
				break;
			case 2:
				cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 2);	// gain setting
				break;
		}
		SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

		sleep(10);
		value[i] = ADC_read_a_value(10);
		Printf("Gain 2^%d : value = %d\r\n", i, value[i]);
	}
	cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 0);	// gain setting
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	if ((value[0]/value[1]) > 1) err++;
	if ((value[1]/value[2]) > 1) err++;
	if (err)
	{
		Printf("Function error! ......................................... ADC PGA !\r\n"); 
		Printf ("Value[0] = %d\r\n",value[0]);
		Printf ("Value[1] = %d\r\n",value[1]);
		Printf ("Value[2] = %d\r\n",value[2]);
		return -1;
	}
	else{
		Printf("ADC's PGA function .................................... Pass !\r\n");
	}
#endif

#if E1260
	Printf("\r\nRTD burn-out test.........");
	for(TEST_STEP=0;TEST_STEP<4;TEST_STEP++)
	{
		if(mptester_exist == mp_time_out){
			if (TEST_STEP==0){
				Printf("\r\nPress stir all channels OFF\r\n");
			}
			else{		
				Printf("\r\nPress stir the channel %d OFF, Others ON\r\n",TEST_STEP+1);
			}

			Printf("Press any key to continue\r\n");
			while(!Kbhit());
			Getch();
		}
		else{
			if (TEST_STEP==0){
				mptest_GPIO_output(0x000F);
//				Printf ("\r\nTEST_Step -00");
			}
			else{
				mptest_GPIO_output(0x0001<<TEST_STEP);
//				Printf ("\r\nTEST_Step -%2d",TEST_STEP);
			}
		}
	
		for(i=0;i<CHANNEL_NUMBER;i++)
		{
			AI_CH_Select(i);
			sleep(15);
			value[i] = ADC_read_a_value(4);
			
			Printf ("\r\nvalue[%d] = %x",i,value[i]);
		}
		
		for (i=0;i<CHANNEL_NUMBER;i++){
			switch(TEST_STEP){
				case 0:
					if (value[i] == 0xFFFFFF){
//						Printf ("\r\nTEST_Step -00 ... Pass");
					}
					else{
						err++;
//						Printf ("\r\nFaul-00");
					}
					break;
					
				case 1:
					if ((value[0] == 0xFFFFFF)&&(value[3] == 0xFFFFFF)){
//						Printf ("\r\nTEST_Step -01 ... Pass");
					}
					else{
						if (i==1){
							err++;
//							Printf ("\r\nFail-01");
						}
/*						else{
							Printf ("CH%d = %x\r\n",i,value[i]);
						}*/
					}
					break;
					
				case 2:
					if ((value[1] == 0xFFFFFF)&&(value[4] == 0xFFFFFF)){
//						Printf ("\r\nTEST_Step -02 ... Pass");
					}
					else{
						if (i==2){
							err++;
//							Printf ("\r\nFail-02");
						}
/*						else{
							Printf ("CH%d = %x\r\n",i,value[i]);
						}*/
					}
					break;
					
				case 3:
					if ((value[2] == 0xFFFFFF)&&(value[5] == 0xFFFFFF)){
//						Printf ("\r\nTEST_Step -03 ... Pass");
					}
					else{
						if (i==3){
							err++;
//							Printf ("\r\nFail-03");
						}
/*						else{
							Printf ("CH%d = %x\r\n",i,value[i]);
						}*/
					}
					break;
					
				default:
					break;
				}
			}
		}

/*
	if(mptester_exist == mp_time_out){
		Printf("\r\nPress stir the channel 1~4 OFF\r\n");
		Printf("Press any key to continue\r\n");
		while(!Kbhit());
		Getch();
	}
	else{
		mptest_GPIO_output(0x000F);
	}
	
	for(i=0;i<CHANNEL_NUMBER;i++)
	{
		AI_CH_Select(i);
		sleep(15);
		value[i] = ADC_read_a_value(4);
		if ((value[i] == 0xFFFFFF)||(value[i] < 0x2000)){
			Printf ("CH%d = Burn-Out\r\n",i);
		}
		else{
			err++;
			Printf ("CH%d = %x\r\n",i,value[i]);
		}
	}
*/
	if (err)
	{
		Printf("Function error! ......................................... Burn Out test !\r\n"); 
		return -1;
	}
	else{
		Printf ("RTD Burn-Out function ...................................... Pass !\r\n");
	}
#endif
	return 0;
}

/*
int diag_chip_ADC_PGA(void)
{
	UINT32 value[3];
	UINT8 i, err=0;
	UINT32 cmd_set_reg;
	UINT8 reg_read_back;
	if(mptester_exist == mp_time_out){
		Printf("\r\nPress stir all channels is ON\r\n");
		while(!Kbhit());
		Getch();
	}else
		mptest_GPIO_output(0x000F);

	AI_CH_Select(0);

	reg_read_back = ADC_Register_Readback(2);
	// read back analog control register
	reg_read_back &= 0xf8;
	// clear PGA gain bits

	for(i=0;i<3;i++)
	{
		cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | i);	// gain setting
		SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

		sleep(10);
		value[i] = ADC_read_a_value(10);
		Printf("Gain 2^%d : value = %d\r\n", i, value[i]);
	}

	cmd_set_reg = 0x520000 | (UINT32)(reg_read_back | 0);	// gain setting
	SPI_Tx(PIO_ADC_CS,cmd_set_reg,24);

	if ((value[1]/value[0]) > 3 || (value[1]/value[0]) < 1) err++;
	if ((value[2]/value[1]) > 3 || (value[2]/value[1]) < 1) err++;

	if (err)
	{
		Printf("Function error! ......................................... ADC's PGA !\r\n"); 
		return -1;
	}
	else
	{
		Printf("ADC's PGA function ...................................... Pass !\r\n");
		return 0;
	}
}
*/

void half_millisec(void){
	ulong t;
	t = Gsys_msec;
	while(1){
		if((Gsys_msec - t) > 500){
			break;
		}
	}
}

void diag_do_AI_WARM_UP(void){
	long t=0,j;
	UINT32 gain,offset,flag;

	Printf("\r\nWarm up start\r\n");
	ReadyLed(t%2);
	do{
		j = t >> 1;
		Printf("Warm up time = %02d:%02d\r",j/60,j%60);
		half_millisec();
		t++;
//		if(t > 12) //30*120 = 3600 sec
		if(t > 3600) //30*120 = 3600 sec
			break;
		if(t%20 > 15){
			ReadyLed(OFF);
		}
		else{
			ReadyLed(ON);
		}
/*
		if(t%10 > 4)	ReadyLed(ON);
		else	ReadyLed(OFF);
*/
		if(Kbhit()){
			if(Getch() == 'W')
				break;
		}
	}while(1);

	SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
	SPI_ENABLE_SLAVE0();
	mx_read_data((UINT32)&flag, SMC_FLASH0_FLAG, 4);

	if((flag & FLAG_CAL) == 0)
	{
		EXSPI_INIT();
		sleep(10);
		ADC_SW_Reset();
		sleep(10);
		ADC_Init();

		i2c_auto_init( i2c_master, i2c_10bit_address , i2c_speed_standard , PARMETER_EEPROM_WRITE);
		i2c_set_target_addressing_mode(i2c_7bit_address);		// EEPROM Spec. only support 7-bit addressing
		EEPROM_address = PARMETER_EEPROM_WRITE;
		GpioWriteData(PIO_EEPROM_PROTECT,0);	// enable calibration EERPOM write

		System_auto_cal();
		for(t = 0; t < SINGLE_CHANNEL_EEPROM_DATA ; t++){
			ADC_PGA_Gain(t);
			ADC_Calibration(0);
			gain = ADC_gain_return();
			offset = ADC_offset_return();
			
			Printf("offset = %06x, gain = %06x\r\n",offset,gain);
			for(j=0;j<CHANNEL_NUMBER;j++)
#if E1240 | E1242
				if(t == 0)
					cal_parameters_save(j,Voltage_mode,offset,gain);
				else if ( t == 2)
					cal_parameters_save(j,Current_mode,offset,gain);
#endif
#if E1260 | E1262
				cal_parameters_save(j,t,offset,gain);
#endif
		}
		GpioWriteData(PIO_EEPROM_PROTECT,1);	// disable calibration EERPOM write
		sleep(20);
		SPI_SET_CLK_DIV(CLOCK_DIV);	//SPI interface set to flash
		SPI_ENABLE_SLAVE0();

		diag_do_set_clean_flag(1,FLAG_WARMUP);
		diag_do_set_clean_flag(0,FLAG_CAL);
		i2c_write_to_eeprom(CAL_FLAG1,0);	//clear cal. flag
	}
	else
	{
		diag_do_set_clean_flag(1,FLAG_WARMUP);
	}

	while(1){
		half_millisec();
		t++;
		if(t%20 > 15){
			ReadyLed(ON);
		}
		else{
			ReadyLed(OFF);
		}
/*
		if(t%20 > 9)	ReadyLed(ON);
		else	ReadyLed(OFF);
*/
	}
}

/******************************************************/
/*                 Calibration Fix setting            */
/******************************************************/


void calibration_pass(void){
	UINT8 i=0;

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);
	
	sleep(1000);
	mptester_communication_init();
	sleep(100);
	
	ReadyLed(ON);
	
/*
	if (mptester_exist != mp_tran_OK){
		Printf("MP_CPU Board connectation fail...\r\n");
		while(!Kbhit());
	}
*/
	Printf("\r\n MP_Cal_Pass-02\r\n");
	
//	mptest_DO(LED_GREEN|BUZZER);
	if(mptest_DO(LED_GREEN|BUZZER)!= mp_time_out){
		if(mptest_DO(LED_GREEN|BUZZER)!= mp_time_out){
			Printf("MP_CPU Board connectation fail...\r\n");
		}
	}
	Printf("\r\n MP_Cal_Pass-03\r\n");
	
	sleep(500);
	
//	mptest_DO(0);
	if(mptest_DO(0) != mp_time_out){
		if(mptest_DO(0) != mp_time_out){
			Printf("MP_CPU Board connectation fail...\r\n");
		}
	}
	
	sleep(100);
	ReadyLed(OFF);
	do{
//		mptest_DO(LED_GREEN|BUZZER);
 		if(mptest_DO(LED_GREEN|BUZZER)!= mp_tran_OK){
 			if(mptest_DO(LED_GREEN|BUZZER)!= mp_tran_OK){
 				Printf("MP_CPU Board connectation fail...\r\n");
 			}
 		}

		sleep(100);
//		mptest_DO(BUZZER);
		if(mptest_DO(BUZZER) !=mp_tran_OK){
			if(mptest_DO(BUZZER) !=mp_tran_OK){
				Printf("MP_CPU Board connectation fail...\r\n");
			}
		}
		
		sleep(100);
	}while(++ i<5);
//	mptest_DO(LED_GREEN);
	if(mptest_DO(LED_GREEN)!=mp_tran_OK){
		if(mptest_DO(LED_GREEN)!=mp_tran_OK){
			Printf("MP_CPU Board connectation fail...\r\n");
		}
	}
	sleep(1000);
//	mptest_DO(0);
	if(mptest_DO(0)!=mp_tran_OK){
		if(mptest_DO(0)!=mp_tran_OK){
			Printf("MP_CPU Board connectation fail...\r\n");
		}
	}
}


void calibration_retry(void){

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);
	Printf("\r\nRe-calibration is recommended ...........\r\n ");
	while(!Kbhit()){
		mptest_DO(LED_GREEN|BUZZER);
		sleep(500);
		ReadyLed(ON);
		mptest_DO(LED_GREEN|BUZZER);
		sleep(100);
		mptest_DO(BUZZER);
		sleep(100);
		mptest_DO(LED_GREEN|BUZZER);
		sleep(100);
		mptest_DO(BUZZER);
		sleep(100);
		mptest_DO(LED_GREEN|BUZZER);
		sleep(100);
		mptest_DO(BUZZER);
		sleep(500);
		ReadyLed(OFF);
	}
}

void calibration_fail(void){

	int i;
	ulong t;
	int led=0;

	m_sio_ioctl_dll_dlm(mp_port, BAUDRATE_115200 , 0, DATA_BITS_8|UART_LCR_STOP);
	sleep(10);
//	Printf("Fail123");
	t = Gsys_msec;
	do{										// red lamp on, of calibration black box
		mptester_exist = mptest_DO(LED_RED|BUZZER);
		for(i=0;i<400;i++)
		{
			if(mptester_exist != mp_time_out){
				mptest_DO(LED_RED|BUZZER);
				sleep(1);
				mptest_DO(BUZZER);
				sleep(1);
			}
		}
		for(i=0;i<200;i++)
		{
			if(mptester_exist != mp_time_out){
				mptest_DO(LED_RED|BUZZER);
				sleep(2);
				mptest_DO(BUZZER);
				sleep(2);
			}
		}
		if((Gsys_msec - t) > 5000){
			ReadyLed(led%2);
			led++;
			t = Gsys_msec;
		}
	}while(!Kbhit());
}