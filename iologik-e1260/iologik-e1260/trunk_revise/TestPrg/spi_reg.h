/*  Copyright (C) MOXA Inc. All rights reserved.

    This software is distributed under the terms of the
    MOXA License.  See the file COPYING-MOXA for details.
*/

/*
	spi_reg.h

	SPI controller register definition

	2008-06-10	Albert Yu
		new release
	2008-10-17	Chin-Fu Yang		
		modified		
*/

#ifndef _SPI_REG_H
#define _SPI_REG_H

/*------------------------------------------------------ Macro / Define -----------------------------*/
/*
 * Variable paramerter settings
 */
#define TX_FIFO_THRESHOLD	16
#define RX_FIFO_THRESHOLD	16	/* must be at least 4 for SDRAM word access in spi_isr() */
#define CLOCK_DIV			8	/* SPI Baudrate = APB clock / CLOCK_DIVs */
#define SPI_BLOCK_SIZE      	(64*1024)


/*
 * Fixed paramerter
 */
#define SPI_FIFO_SIZE		32


/*
 * SPI controller definition
 */
#define SPI_DISABLE()					VPlong(S2E_SPI_BASE + SPI_SSI_ENABLE) = (ulong)0
#define SPI_ENABLE()					VPlong(S2E_SPI_BASE + SPI_SSI_ENABLE) = (ulong)1
#define SPI_DISABLE_SLAVE()				VPlong(S2E_SPI_BASE + SPI_SLAVE_SELECT) = (ulong)0
#define SPI_ENABLE_SLAVE0()				VPlong(S2E_SPI_BASE + SPI_SLAVE_SELECT) = (ulong)1
#define SPI_ENABLE_SLAVE1()				VPlong(S2E_SPI_BASE + SPI_SLAVE_SELECT) = (ulong)2
#define SPI_ENABLE_FLASH				SPI_ENABLE_SLAVE0		// 0 is flash
#define SPI_DISABLE_FLASH				SPI_DISABLE_SLAVE
#define SPI_ENABLE_ADS1216				SPI_ENABLE_SLAVE1
#define SPI_DISABLE_ADS1216				SPI_DISABLE_SLAVE
#define SPI_WRITE_DATA(x)				VPlong(S2E_SPI_BASE + SPI_DATA) = (ulong)(x)
#define SPI_READ_DATA()					VPlong(S2E_SPI_BASE + SPI_DATA)
#define SPI_SET_MODE(x)					VPlong(S2E_SPI_BASE + SPI_CONTROL0) = (x)
#define SPI_SET_EEPROM_READ_CNT(x)		VPlong(S2E_SPI_BASE + SPI_CONTROL1) = (ulong)((x) - 1)
#define SPI_SET_CLK_DIV(x)				VPlong(S2E_SPI_BASE + SPI_CLOCK_DIV) = (x)
#define SPI_RX_FIFO_ENTRIES()			(VPlong(S2E_SPI_BASE + SPI_RX_FIFO_LEVEL))
#define SPI_TX_FIFO_ENTRIES()			(VPlong(S2E_SPI_BASE + SPI_TX_FIFO_LEVEL))
#define SPI_STATUS()					(VPlong(S2E_SPI_BASE + SPI_STATUS_REG))

#define SPI_EEPROM_READ_MODE()			SPI_SET_MODE(0x73C7)
#define SPI_TRANSMIT_ONLY_MODE()		SPI_SET_MODE(0x71C7)


/* Interrupt Mask/Status Register Bits Definition */
#define SPI_INT_RX_FIFO_FULL       		0x10
#define SPI_INT_RX_FIFO_OVERFLOW	  	0x08
#define SPI_INT_RX_FIFO_UDNERFLOW  		0x04
#define SPI_INT_TX_FIFO_OVERFLOW   		0x02
#define SPI_INT_TX_FIFO_EMPTY      		0x01


/* Enable/Disable RX Interrupt: RX_FIFO_FULL & RX_FIFO_OVERFLOW */
#define SPI_ENABLE_RX_INT()     		VPlong(S2E_SPI_BASE + SPI_INT_MASK) |= (SPI_INT_RX_FIFO_FULL | SPI_INT_RX_FIFO_OVERFLOW)
#define SPI_DISABLE_RX_INT()    		VPlong(S2E_SPI_BASE + SPI_INT_MASK) &= ~(ulong)(SPI_INT_RX_FIFO_FULL | SPI_INT_RX_FIFO_OVERFLOW)

/* Enable RX Interrupt: TX_FIFO_EMPTY */
#define SPI_ENABLE_TX_INT()    	 		VPlong(S2E_SPI_BASE + SPI_INT_MASK) |= (SPI_INT_TX_FIFO_EMPTY)
#define SPI_DISABLE_TX_INT()    		VPlong(S2E_SPI_BASE + SPI_INT_MASK) &= ~(ulong)(SPI_INT_TX_FIFO_EMPTY)

/* Enable RX Interrupt: RX_FIFO_FULL & RX_FIFO_OVERFLOW & TX_FIFO_EMPTY */
#define SPI_ENABLE_TXRX_INT()   		VPlong(S2E_SPI_BASE + SPI_INT_MASK) = SPI_INT_RX_FIFO_FULL | SPI_INT_RX_FIFO_OVERFLOW | SPI_INT_TX_FIFO_EMPTY

/* Disable all interrupts */
#define SPI_DISABLE_INT()				VPlong(S2E_SPI_BASE + SPI_INT_MASK) = 0;		// 0 is mask

#define SPI_STATUS_BUSY					0x01	/* busy transferring data */
#define SPI_STATUS_TX_FIFO_NOT_FULL		0x02	/* TX FIFO Not Full */
#define SPI_STATUS_TX_FIFO_EMPTY		0x04	/* TX FIFO Empty */
#define SPI_STATUS_RX_FIFO_NOT_EMPTY	0x08	/* RX FIFO Not Empty */
#define SPI_STATUS_RX_FIFO_FULL			0x10	/* RX FIFO Full */

#define SPI_IS_BUSY()					(SPI_STATUS() & SPI_STATUS_BUSY)



#endif
