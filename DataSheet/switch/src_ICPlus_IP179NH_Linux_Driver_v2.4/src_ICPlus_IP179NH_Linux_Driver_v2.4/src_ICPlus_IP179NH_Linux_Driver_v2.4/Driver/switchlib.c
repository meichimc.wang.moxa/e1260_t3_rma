/******************************************************************************
 *
 *   Name:           switchlib.c
 *
 *   Description:
 *
 *   Copyright:      (c) 2009-2050    IC Plus Corp.
 *                   All rights reserved.
 *
 *****************************************************************************/

#include "switchlib.h"
#include "switchmodule.h"



#ifdef USER_API
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <linux/types.h>
#else

#endif



const char *dev_switch="/dev/dev_switch";
#define read_reg switch_read_reg
#define write_reg switch_write_reg


#ifdef USER_API                           
unsigned short switch_read_reg(unsigned char phyad, unsigned char regad)
{
  int fd,retval;
  struct GeneralSetting gs;
  memset(&gs,0x0,sizeof(struct GeneralSetting));
  gs.phy=phyad;
  gs.reg=regad;
  
  if((fd=open(dev_switch,O_RDWR))<0)
    printf("Err: open %s failed!\n", dev_switch);        
  if(ioctl(fd,SWITCH_READ,&gs)<0)
    printf("Err: ioctl %s failed!\n", dev_switch);
#ifdef USER_API_DEBUG
printf("lib:switch_read_reg addr=%02x.%02x data=%04x\n",gs.phy,gs.reg,gs.data);
#endif   
	retval = close(fd);
	if (retval < 0)
		printf("Err: close %s failed!\n", fd);          
  return gs.data;
}
void switch_write_reg(unsigned char phyad, unsigned char regad, unsigned short Regdata)
{
  int fd,retval;
  struct GeneralSetting gs;
#ifdef USER_API_DEBUG
printf("lib:switch_write_reg addr=%02x.%02x data=%04x\n",phyad,regad,Regdata);
#endif
  memset(&gs,0x0,sizeof(struct GeneralSetting));
  gs.phy=phyad;
  gs.reg=regad;
  gs.data=Regdata;
  if((fd=open(dev_switch,O_RDWR))<0)
    printf("Err: open %s failed!\n", dev_switch);        
  if(ioctl(fd,SWITCH_WRITE,&gs)<0)
    printf("Err: ioctl %s failed!\n", dev_switch);
	retval = close(fd);
	if (retval < 0)
		printf("Err: close %s failed!\n", fd);            
} 

#else   
unsigned short switch_read_reg(unsigned char phyad, unsigned char regad)
{
  return switch_mdio_rd(phyad, regad);
}
void switch_write_reg(unsigned char phyad, unsigned char regad, unsigned short Regdata)
{
  switch_mdio_wr(phyad, regad, Regdata);  
}
#endif
//========================================================================

// LUT ====================================================

/**********************************************************
 *  Function: LUT_set_mode
 *
 *  Description:
 *    Set LUT structure to support 2K unicast or
 *    1K unicast/1K multicast.
 *  
 *  Parameters:
 *    mode  �V LUT_MODE_2K / LUT_MODE_1K1K 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_set_mode(unsigned char mode)
{
  unsigned short tmp;
  
  tmp = read_reg(PHY20, LEARN_FORWARD_ENABLE);
  tmp &= ~0x4;
  tmp |= (unsigned short)(mode<<2);
  write_reg(PHY20, LEARN_FORWARD_ENABLE, tmp);
}
/**********************************************************
 *  Function: LUT_get_mode
 *
 *  Description:
 *    Get LUT structure to support whether 2K unicast or
 *    1K unicast/1K multicast.
 *  
 *  Parameters:
 *    None 
 *
 *  Return:
 *    0   �V LUT_MODE_2K
 *    1   �V LUT_MODE_1K1K
 *    
 *********************************************************/
unsigned char LUT_get_mode(void)
{
  return (unsigned char)((read_reg(PHY20, LEARN_FORWARD_ENABLE)>>2) & 0x1);
}
/**********************************************************
 *  Function: LUT_get_2k_hash_index
 *
 *  Description:
 *    Calculate a hash index of 2K unicast table.
 *  
 *  Parameters:
 *    fid         �V FID
 *    mac         - MAC address
 *    entry       - entry no. of the same bucket
 *
 *  Return:
 *    0 ~ 0xFFF   �V a 11-bit hash index
 *    
 *********************************************************/
unsigned short LUT_get_2k_hash_index(unsigned char fid, unsigned char *mac, unsigned char entry)
{
      unsigned short tmp;
      tmp = ((unsigned short)fid<<3|mac[0]>>5)^
                      ((mac[0]&0x1f)<<4|mac[1]>>4)^
                      ((mac[1]&0x0f)<<5|mac[2]>>3)^
                      ((mac[2]&0x07)<<6|mac[3]>>2)^
                      ((mac[3]&0x03)<<7|mac[4]>>1)^
                      ((mac[4]&0x01)<<8|mac[5]);
      tmp = tmp << 2 | ((unsigned short)entry&0x3);
      return tmp;
}
/**********************************************************
 *  Function: LUT_get_1k1k_hash_index
 *
 *  Description:
 *    Calculate a hash index of 1K unicast table and
 *    1K multicast table.
 *  
 *  Parameters:
 *    type        - LUT_MAC_UNICAST / LUT_MAC_IGMP
 *    fid         �V FID
 *    mac         - MAC address
 *    entry       - entry no. of the same bucket
 *
 *  Return:
 *    0 ~ 0xFFF   �V a 11-bit hash index
 *    
 *********************************************************/
unsigned short LUT_get_1k1k_hash_index(unsigned char type, unsigned char fid, unsigned char *mac, unsigned char entry)
{
	unsigned short tmp;
	tmp =	fid^mac[0]^mac[1]^mac[2]^mac[3]^mac[4]^mac[5];//0x5F = 0x5E^0x00^0x01
	tmp =  (tmp<<2) | (entry&0x3);
	if (type==LUT_MAC_UNICAST){
		tmp |= (0x0<<10);
	}
	else{
		tmp |= (0x1<<10);
	}
	return tmp;
}

void LUT_fill_in_entry(unsigned short idx, unsigned short *ldata, struct LUT_entry *lutset)
{
  int i;
  unsigned char LutMode;
  
  //valid
  lutset->valid = (unsigned char)((ldata[4]>>15) & 0x1);

  //type  
  LutMode = LUT_get_mode();
  if (LutMode==LUT_MODE_1K1K && idx>=(MAX_LUT_ENTRY/2))
    lutset->type = LUT_MAC_IGMP;
  else
    lutset->type = LUT_MAC_UNICAST;
    
  //entry & fid  
  lutset->entry = (unsigned char)(idx & 0x3);
  lutset->fid = (unsigned char)((ldata[3]>>12) & 0xF);
  
  //port_map        
  lutset->port_map = (ldata[3] & 0x01FF);

  //MAC & aged
  if (lutset->type == LUT_MAC_UNICAST)
  {
    for (i=0; i < 6; i+=2)
    {
      lutset->MAC[i] = (unsigned char)(ldata[2-i/2] >> 8);
      lutset->MAC[i+1] = (unsigned char)(ldata[2-i/2] & 0xFF);
    }  
    lutset->aged = (ldata[4] & 0x1);
  }
  else
  {
    for (i=0; i < 3; i++)
      lutset->MAC[i] = 0x00;
//    lutset->MAC[0] = 0x01;
//    lutset->MAC[1] = 0x00;
//    lutset->MAC[2] = 0x5E;
    lutset->MAC[3] = (unsigned char)(ldata[1] & 0x7F);
    lutset->MAC[4] = (unsigned char)(ldata[0] >> 8);
    lutset->MAC[5] = (unsigned char)(ldata[0] & 0xFF);    
    lutset->aged = (ldata[4] & 0x1FF);
  }
}
/**********************************************************
 *  Function: LUT_get_next_valid_entry
 *
 *  Description:
 *    Get next valid entry from input index.
 *  
 *  Parameters:
 *    idx         - scan LUT from the entry of this index
 *    lutset      �V a pointer to a buffer that will hold
 *                  the data of next valid entry
 *
 *  Return:
 *    -1          - fail
 *    0 ~ 0x7FF   �V the index of next valid entry
 *    
 *********************************************************/
int LUT_get_next_valid_entry(int idx, struct LUT_entry *lutset)
{
	int i, j;
  unsigned char LutMode;
	unsigned short Reg2108=0;
	unsigned short LUT_data[5];

	if (idx < 0 || idx >= MAX_LUT_ENTRY)	return C_FAIL;

  LutMode = LUT_get_mode();
  
  for (i=idx; i < MAX_LUT_ENTRY; i++)
  {
    Reg2108 = (i | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
      Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
    
    for (j=0; j < 5; j++)
			LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);

    if (LutMode==LUT_MODE_1K1K && i>=(MAX_LUT_ENTRY/2))   //IGMP
    {
      if (!(LUT_data[4] & 0x8000))
        continue;
    }
    else  //UCST
    {
      if (!(LUT_data[4] & 0x8000) && (LUT_data[4] & 0x1))   //dynamic and aged
        continue;
    }
    
    LUT_fill_in_entry((unsigned short)i, LUT_data, lutset);
    return i;
  }
  
  return C_FAIL;
}
/**********************************************************
 *  Function: LUT_set_entry
 *
 *  Description:
 *    Set a LUT entry.
 *  
 *  Parameters:
 *    lutset  �V the data of the entry to be set
 *
 *  Return:
 *    -1      - fail
 *    0       �V success
 *    
 *********************************************************/
int LUT_set_entry(struct LUT_entry lutset)
{
  int i;
  unsigned char LutMode;
  unsigned short rdata=0;
  
  LutMode = LUT_get_mode();
  if (lutset.type==LUT_MAC_IGMP && LutMode==LUT_MODE_2K)
    return -1;

  //LUT_DATA0 ~ LUT_DATA4  
  if (lutset.type == LUT_MAC_UNICAST)
  {
    //MAC
    for (i=0; i < 3; i++)
    {
      rdata = ((unsigned short)lutset.MAC[(2-i)*2]) << 8;
      rdata |= (unsigned short)lutset.MAC[(2-i)*2+1];
      write_reg(PHY21, LUT_DATA0+i, rdata);
    }
    
    //fid & port_map
    rdata = ((unsigned short)(lutset.fid & 0xF) << 12) | (lutset.port_map & 0x1FF);
    write_reg(PHY21, LUT_DATA3, rdata);
    
    //valid & aged
    rdata = ((unsigned short)(lutset.valid & 0x1) << 15) | (lutset.aged & 0x1);
    write_reg(PHY21, LUT_DATA4, rdata);
  }
  else if (lutset.type == LUT_MAC_IGMP)
  {
    //MAC
    rdata = ((unsigned short)lutset.MAC[4] << 8) | ((unsigned short)lutset.MAC[5]);
    write_reg(PHY21, LUT_DATA0, rdata);
    rdata = (unsigned short)(lutset.MAC[3] & 0x7F);
    write_reg(PHY21, LUT_DATA1, rdata);
    
    //fid & port_map
    rdata = ((unsigned short)(lutset.fid & 0xF) << 12) | (lutset.port_map & 0x1FF);
    write_reg(PHY21, LUT_DATA3, rdata);
    
    //valid & aged
    rdata = ((unsigned short)(lutset.valid & 0x1) << 15) | (lutset.aged & 0x1FF);
    rdata |= 0x4000;
    write_reg(PHY21, LUT_DATA4, rdata);
  }
  else
    return C_FAIL;
    
  //LUT_COMMAND
  if (LutMode == LUT_MODE_2K)
    rdata = LUT_get_2k_hash_index(lutset.fid, lutset.MAC, lutset.entry);
  else
    rdata = LUT_get_1k1k_hash_index(lutset.type, lutset.fid, lutset.MAC, lutset.entry);
    
  rdata |= 0xC000;
  write_reg(PHY21, LUT_COMMAND, rdata);
  do {
    rdata = read_reg(PHY21, LUT_COMMAND);
  } while (rdata & 0x8000);
  
  return C_SUCCESS;
}
/**********************************************************
 *  Function: LUT_get_entry
 *
 *  Description:
 *    Get the LUT entry of input index.
 *  
 *  Parameters:
 *    idx     - the index of which the LUT entry is queried
 *    lutset  �V a pointer to a buffer that will hold the
 *              data of the entry associated with the input
 *              index
 *
 *  Return:
 *    -1      - fail
 *    0       �V success
 *    
 *********************************************************/
int LUT_get_entry(unsigned short idx, struct LUT_entry *lutset)
{
  int i;
	unsigned short Reg2108=0;
  unsigned short LUT_data[5];

	Reg2108 = (idx | 0x8000);
  write_reg(PHY21, LUT_COMMAND, Reg2108);
  do {
      Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);

	for (i=0; i < 5; i++)
			LUT_data[i] = read_reg(PHY21, LUT_DATA0+i);

  LUT_fill_in_entry(idx, LUT_data, lutset);

  //if the entry is invalid, return C_FAIL  
  if (lutset->type == LUT_MAC_UNICAST)
  {    
    if (lutset->valid==0 && lutset->aged)
      return C_FAIL;
    else
      return C_SUCCESS;
  }
  else
  {
    if (lutset->valid == 0)
      return C_FAIL;
    else
      return C_SUCCESS;
  }
}
/**********************************************************
 *  Function: LUT_get_port_by_mac   (UCST table only)
 *
 *  Description:
 *    Get destination ports of input MAC address.
 *  
 *  Parameters:
 *    fid     - FID
 *    mac     - input MAC address 
 *
 *  Return:
 *    0       - can't find a match entry that is valid
 *    1 ~ 9   - port to which a packet with a DMAC equals
 *              to the input MAC will be sent
 *    0xF000  - input MAC address is not a unicast address
 *    
 *********************************************************/
unsigned short LUT_get_port_by_mac(unsigned char fid, unsigned char *mac)
{
  unsigned char i, j, LutMode;
  unsigned short idx, Reg2108=0, pmap=0, port=0;
  unsigned short LUT_data[5];

  //UCST only  
  if (*mac & 0x1)
    return 0xF000;

  LutMode = LUT_get_mode();
  if (LutMode == LUT_MODE_2K)
  { idx = LUT_get_2k_hash_index(fid, mac, 0); }
  else
  { idx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, fid, mac, 0); }
    
  for (i=0; i < 4; i++)
  {
    Reg2108 = ((idx+i) | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
        
    for (j=0; j < 5; j++)
      LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);

    //if this entry is invalid         
    if (!(LUT_data[4] & 0x8000) && (LUT_data[4] & 0x1))   //dynamic and aged
        continue;
      
    //check if this MAC equals to the input MAC
    if ((LUT_data[0] & 0xFF)==*(mac+5) && (LUT_data[0] >> 8)==*(mac+4) &&
        (LUT_data[1] & 0xFF)==*(mac+3) && (LUT_data[1] >> 8)==*(mac+2) &&
        (LUT_data[2] & 0xFF)==*(mac+1) && (LUT_data[2] >> 8)==*(mac+0))
    {      
      pmap = (LUT_data[3] & 0x1FF);
        
      for (j=0; j < 9; j++)
      {
        if (pmap & ((unsigned short)0x1 << j))
        {
          port = (unsigned short)(j+1);
          break;
        }
      }
      break;
    }
  }
  
  return port;
}
/**********************************************************
 *  Function: LUT_flush_table
 *
 *  Description:
 *    Clear all entries of LUT.
 *  
 *  Parameters:
 *    None 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_table(void)
{
  unsigned short i, Reg2108=0;

  for (i=0; i < 5; i++)
  {
    if (i==4)
      write_reg(PHY21, LUT_DATA0+i, (unsigned short)0x1);
    else
      write_reg(PHY21, LUT_DATA0+i, (unsigned short)0x0);
  }

  for (i=0; i < MAX_LUT_ENTRY; i++)
  {    
    Reg2108 = (i | 0xC000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
  }
}
/**********************************************************
 *  Function: LUT_flush_port    (UCST table only)
 *
 *  Description:
 *    Clear LUT entries associated with the input port.
 *  
 *  Parameters:
 *    port �V all LUT entries associated with this port is 
 *           going to be cleared out 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_port(int port)
{
  unsigned char LutMode;
  unsigned short i, j, MaxEntry, Reg2108=0, pmap;
  unsigned short LUT_data[5];

  LutMode = LUT_get_mode();
  if (LutMode == LUT_MODE_2K)
  { MaxEntry = (unsigned short)MAX_LUT_ENTRY; }
  else
  { MaxEntry = (unsigned short)(MAX_LUT_ENTRY/2); }
  
  for (i=0; i < MaxEntry; i++)
  {    
    Reg2108 = (i | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
    
    for (j=0; j < 5; j++)
      LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);
    
    pmap = (LUT_data[3] & 0x1FF);  
    if ((pmap >> (port-1)) & 0x1)
    {
      for (j=0; j < 5; j++)
      {
        if (j==4)
          write_reg(PHY21, LUT_DATA0+j, (unsigned short)0x1);
        else
          write_reg(PHY21, LUT_DATA0+j, (unsigned short)0x0);
      }
        
      Reg2108 = (i | 0xC000);
      write_reg(PHY21, LUT_COMMAND, Reg2108);
      do {
         Reg2108 = read_reg(PHY21, LUT_COMMAND);
      } while (Reg2108 & 0x8000);
    }
  }
}
/**********************************************************
 *  Function: LUT_flush_entry     (UCST table only)
 *
 *  Description:
 *    Clear the LUT entry of specified FID and MAC.
 *  
 *  Parameters:
 *    fid     - FID
 *    mac     - input MAC address 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_entry(unsigned char fid, unsigned char *mac)
{
  unsigned char i, j, LutMode;
  unsigned short idx, Reg2108=0;
  unsigned short LUT_data[5];

  //UCST only
  if (*(mac+0)!=0x01 && *(mac+1)!=0x00 && *(mac+2)!=0x5E)
  {
    LutMode = LUT_get_mode();
    if (LutMode == LUT_MODE_2K)
    { idx = LUT_get_2k_hash_index(fid, mac, 0); }
    else
    { idx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, fid, mac, 0);  }
    
    for (i=0; i < 4; i++)
    {
      Reg2108 = ((idx+i) | 0x8000);
      write_reg(PHY21, LUT_COMMAND, Reg2108);
      do {
         Reg2108 = read_reg(PHY21, LUT_COMMAND);
      } while (Reg2108 & 0x8000);
      
      for (j=0; j < 5; j++)
        LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);
  
      //check if this MAC equals to the input MAC
      if ((LUT_data[0] & 0xFF)==*(mac+5) && (LUT_data[0] >> 8)==*(mac+4) &&
          (LUT_data[1] & 0xFF)==*(mac+3) && (LUT_data[1] >> 8)==*(mac+2) &&
          (LUT_data[2] & 0xFF)==*(mac+1) && (LUT_data[2] >> 8)==*(mac+0))
      {
          write_reg(PHY21, LUT_DATA0, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA1, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA2, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA3, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA4, (unsigned short)0x1);
     
          Reg2108 = ((idx+i) | 0xC000);
          write_reg(PHY21, LUT_COMMAND, Reg2108);
          do {
             Reg2108 = read_reg(PHY21, LUT_COMMAND);
          } while (Reg2108 & 0x8000);
            
          break;
      }
    }
  }
}

// VLAN ===================================================

/**********************************************************
 *  Function: VLAN_set_mode
 *
 *  Description:
 *    Set VLAN mode.
 *  
 *  Parameters:
 *    port    - port
 *    mode    - VLAN_MODE_PORT_BASED (0) or
 *              VLAN_MODE_TAG_BASED (1) 
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_set_mode(unsigned char port, unsigned char mode)
{
  unsigned short VLANMode=0;
  
  if (port==0 || port > MAX_PORT_NUM || mode > VLAN_MODE_TAG_BASED)
    return C_FAIL;
  
  VLANMode = read_reg(PHY22, VLAN_MODE);
  VLANMode &= ~((unsigned short)0x1 << (port-1));
  VLANMode |= ((unsigned short)mode << (port-1));
  write_reg(PHY22, VLAN_MODE, VLANMode);
  
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_mode
 *
 *  Description:
 *    Get VLAN mode.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - VLAN_MODE_PORT_BASED
 *    1       - VLAN_MODE_TAG_BASED
 *    
 *********************************************************/
int VLAN_get_mode(unsigned char port)
{
  if (port==0 || port > MAX_PORT_NUM)
    return C_FAIL;
  else
    return (unsigned char)((read_reg(PHY22, VLAN_MODE) >> (port-1)) & 0x1);
}
/**********************************************************
 *  Function: VLAN_PB_set_member
 *
 *  Description:
 *    Set member ports of a port which is port-based VLAN.
 *  
 *  Parameters:
 *    port    - port number
 *    member  - member ports which the ingress port can
 *              transmits packets to
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_member(unsigned char port, unsigned short member)
{
  if (port==0 || port > MAX_PORT_NUM)
    return C_FAIL;

  write_reg(PHY22, PB_VLAN_MEMBER_SET_0+(port-1), (member & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_member
 *
 *  Description:
 *    Get member ports of a port which is port-based VLAN.
 *  
 *  Parameters:
 *    port        - port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - member ports of the input port
 *    
 *********************************************************/
int VLAN_PB_get_member(unsigned char port)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
  
  return (int)(read_reg(PHY22, PB_VLAN_MEMBER_SET_0+(port-1))); 
}
/**********************************************************
 *  Function: VLAN_PB_set_PVID
 *
 *  Description:
 *    Set PVID of a port.
 *  
 *  Parameters:
 *    port    - port
 *    pvid    - PVID
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_PVID(unsigned char port, unsigned short pvid)
{
  if (port==0 || port > MAX_PORT_NUM)
    return C_FAIL;
    
  write_reg(PHY22, VLAN_PVID0+(port-1), pvid);
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_PVID
 *
 *  Description:
 *    Get PVID of a port.
 *  
 *  Parameters:
 *    port        - port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0xFFFF  - PVID of the input port
 *    
 *********************************************************/
int VLAN_PB_get_PVID(unsigned char port)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
  
  return (int)(read_reg(PHY22, VLAN_PVID0+(port-1))); 
}
/**********************************************************
 *  Function: VLAN_PB_set_add_tag
 *
 *  Description:
 *    Set ports to be added VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port    - the ingress port
 *    addtag  - ports to be added VLAN tag
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_add_tag(unsigned char port, unsigned short addtag)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
    
  write_reg(PHY23, PB_VLAN_ADD_TAG_MASK_0+(port-1), (addtag & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_add_tag
 *
 *  Description:
 *    Get ports to be added VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port        - the ingress port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - ports to be added VLAN tag
 *    
 *********************************************************/
int VLAN_PB_get_add_tag(unsigned char port)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
  
  return (int)(read_reg(PHY23, PB_VLAN_ADD_TAG_MASK_0+(port-1))); 
}
/**********************************************************
 *  Function: VLAN_PB_set_remove_tag
 *
 *  Description:
 *    Set ports to be removed VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port    - the ingress port
 *    rmvtag  - ports to be removed VLAN tag
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_remove_tag(unsigned char port, unsigned short rmvtag)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
    
  write_reg(PHY23, PB_VLAN_RMV_TAG_MASK_0+(port-1), (rmvtag & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_remove_tag
 *
 *  Description:
 *    Get ports to be removed VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port        - the ingress port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - ports to be removed VLAN tag
 *    
 *********************************************************/
int VLAN_PB_get_remove_tag(unsigned char port)
{
  if (port <= 0 || port > MAX_PORT_NUM)
    return C_FAIL;
  
  return (int)(read_reg(PHY23, PB_VLAN_RMV_TAG_MASK_0+(port-1))); 
}
/**********************************************************
 *  Function: VLAN_TB_set_entry
 *
 *  Description:
 *    Set a VLAN entry.
 *  
 *  Parameters:
 *    idx        - the index of the entry to be set
 *    vlanset    - the data of the entry to be set
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_TB_set_entry(unsigned char idx, struct VLAN_entry *vlanset)
{
  unsigned short u16dat, tmp;
  
  if (idx <= 0 || idx > VLAN_MAX_ENTRY)
    return C_FAIL;
    
  if (vlanset->VID >= 4096 || vlanset->FID >= 0x10)
    return C_FAIL;

  //VID & FID
  u16dat = ((unsigned short)(vlanset->FID & 0xF) << 12) | (vlanset->VID & 0xFFF);
  write_reg(PHY24, TB_VLAN_FID_VID_0+(idx-1), u16dat);
  
  //member, add tag, and remove tag
  write_reg(PHY25, TB_VLAN_MEMBER_0+(idx-1), (vlanset->member & 0x1FF));
  write_reg(PHY24, TB_VLAN_ADD_TAG_0+(idx-1), (vlanset->AddTag & 0x1FF));
  write_reg(PHY25, TB_VLAN_REMOVE_TAG_0+(idx-1), (vlanset->RmvTag & 0x1FF));

  //valid
  u16dat = read_reg(PHY22, VLAN_VALID_ENTRY);
  u16dat &= ~((unsigned short)0x1 << (idx-1));
  tmp = (unsigned short)((vlanset->valid) ? 0x1 : 0x0);
  u16dat |= (tmp << (idx-1));
  write_reg(PHY22, VLAN_VALID_ENTRY, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_TB_get_entry
 *
 *  Description:
 *    Get a VLAN entry.
 *  
 *  Parameters:
 *    idx        - the index of the entry to be get
 *    vlanset    - a pointer to a buffer that will hold the
 *                 data of the entry associated with the
 *                 input index
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_TB_get_entry(unsigned char idx, struct VLAN_entry *vlanset)
{
  unsigned short u16dat;
  
  if (idx <= 0 || idx > VLAN_MAX_ENTRY)
    return C_FAIL;
    
  vlanset->valid = (unsigned char)((read_reg(PHY22, VLAN_VALID_ENTRY) >> (idx-1)) & 0x1);
  
  u16dat = read_reg(PHY24, TB_VLAN_FID_VID_0+(idx-1));
  vlanset->VID = (u16dat & 0xFFF);
  vlanset->FID = (unsigned char)((u16dat >> 12) & 0xF);
  
  vlanset->member = read_reg(PHY25, TB_VLAN_MEMBER_0+(idx-1));
  vlanset->AddTag = read_reg(PHY24, TB_VLAN_ADD_TAG_0+(idx-1));
  vlanset->RmvTag = read_reg(PHY25, TB_VLAN_REMOVE_TAG_0+(idx-1));

  return C_SUCCESS;  
}
/**********************************************************
 *  Function: VLAN_set_ingress_type
 *
 *  Description:
 *    Set acceptable VLAN frame type in tag-based VLAN.
 *  
 *  Parameters:
 *    intype      - VLAN_ALL_FRAME / VLAN_TAGGED_FRAME /
 *                  VLAN_UNTAGGED_FRAME
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_set_ingress_type(unsigned char intype)
{
  unsigned short u16dat;
  
  if (intype > VLAN_UNTAGGED_FRAME)
    return C_FAIL;
    
  u16dat = read_reg(PHY22, VLAN_INGRESS_RULE);
  u16dat &= (unsigned short)0xCFFF;
  u16dat |= (unsigned short)intype << 12;
  write_reg(PHY22, VLAN_INGRESS_RULE, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_ingress_type
 *
 *  Description:
 *    Get acceptable VLAN frame type in tag-based VLAN.
 *  
 *  Parameters:
 *    None
 *    
 *  Return:
 *    0   - VLAN_ALL_FRAME
 *    1   - VLAN_TAGGED_FRAME
 *    2   - VLAN_UNTAGGED_FRAME
 *    
 *********************************************************/
unsigned char VLAN_get_ingress_type(void)
{
  return (unsigned char)((read_reg(PHY22, VLAN_INGRESS_RULE) >> 12) & 0x3);
}
/**********************************************************
 *  Function: VLAN_set_ingress_filter
 *
 *  Description:
 *    In tag-based VALN, if a port corresponding bit is
 *    set, frame shall discard on that port if the member
 *    set of the VLAN entry does not include that port.
 *  
 *  Parameters:
 *    port  - port number of which a port is enable or
 *            disable ingress filter
 *    en    - 1: enable, 0: disable
 *    
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_set_ingress_filter(unsigned char port, unsigned char en)
{
  unsigned short u16dat;
  
  if (port==0 || port > MAX_PORT_NUM || en > ENABLE)
    return C_FAIL;
  
  u16dat = read_reg(PHY22, VLAN_INGRESS_RULE);
  u16dat &= ~((unsigned short)0x1 << (port-1));
  u16dat |= ((unsigned short)en << (port-1));
  write_reg(PHY22, VLAN_INGRESS_RULE, u16dat);
    
  return C_SUCCESS;  
}
/**********************************************************
 *  Function: VLAN_get_ingress_filter
 *
 *  Description:
 *    Get the status of the VLAN ingress filter of an input
 *    port.
 *  
 *  Parameters:
 *    port    - port number
 *    
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int VLAN_get_ingress_filter(unsigned char port)
{
  if (port==0 || port > MAX_PORT_NUM)
    return C_FAIL;
    
  return (int)((read_reg(PHY22, VLAN_INGRESS_RULE) >> (port-1)) & 0x1);
}
/**********************************************************
 *  Function: VLAN_set_unknown_vid_mode
 *
 *  Description:
 *    Set the way to process a frame of which VID is not
 *    in any entry of VLAN table in tag-based VLAN.
 *  
 *  Parameters:
 *    mode      - VLAN_UNVID_DISCARD / VLAN_UNVID_TOCPU /
 *                VLAN_UNVID_FLOOD
 *    
 *  Return:
 *    0         - success
 *    -1        - failed
 *    
 *********************************************************/
int VLAN_set_unknown_vid_mode(unsigned char mode)
{
  unsigned short u16dat;
  
  if (mode > VLAN_UNVID_FLOOD)
    return C_FAIL;
  
  u16dat = read_reg(PHY22, VLAN_MODE);
  u16dat &= (unsigned short)0xCFFF;
  u16dat |= (unsigned short)mode << 12;
  write_reg(PHY22, VLAN_MODE, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_unknown_vid_mode
 *
 *  Description:
 *    Get the way to process a frame of which VID is not
 *    in any entry of VLAN table.
 *  
 *  Parameters:
 *    None
 *    
 *  Return:
 *    0   - VLAN_UNVID_DISCARD
 *    1   - VLAN_UNVID_TOCPU
 *    2   - VLAN_UNVID_FLOOD
 *    
 *********************************************************/
unsigned char VLAN_get_unknown_vid_mode(void)
{
  return (unsigned char)((read_reg(PHY22, VLAN_MODE) >> 12) & 0x3);
}

