#ifndef SWITCH_DRIVER_H
#define SWITCH_DRIVER_H
//#define SWITCHDEBUG

#define SWITCH_IOC_TYPE		0xE0
#define SWITCH_READ   _IOC(_IOC_READ, SWITCH_IOC_TYPE, 0, 4)
#define SWITCH_WRITE  _IOC(_IOC_WRITE, SWITCH_IOC_TYPE, 0, 4)

extern void switch_mdio_wr(unsigned short pa, unsigned short ra, unsigned short va);
extern unsigned short switch_mdio_rd(unsigned short pa, unsigned short ra);

#endif		
