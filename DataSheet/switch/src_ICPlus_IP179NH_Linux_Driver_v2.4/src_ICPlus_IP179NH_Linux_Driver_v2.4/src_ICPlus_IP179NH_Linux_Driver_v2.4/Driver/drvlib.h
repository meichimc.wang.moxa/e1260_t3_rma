/******************************************************************************
 *
 *   Name:           drvlib.h
 *
 *   Description:    Driver Lib Header File
 *
 *   Copyright:      (c) 2009-2050    IC Plus Corp.
 *                   All rights reserved.
 *
 *******************************************************************************/
#ifndef _DRVLIB_H_
#define _DRVLIB_H_

struct GeneralSetting
{
  unsigned char phy;
	unsigned char reg;
	unsigned short data;
};

//=========================================================
#define C_SUCCESS   0
#define C_FAIL		  -1

#define ENABLE      1
#define DISABLE     0

#define MAX_PORT_NUM    9

// Registers ==============================================
#define PHY_PORT0	8
  #define CONTROL_REG				0
  #define AN_REG					4
  #define	SPECIAL_STATUS_REG		18

#define PHY20   20
  #define LEARN_FORWARD_ENABLE		7
  #define QOS_MODE					8
  #define QOS_PORT_EN				9
  #define QOS_PORT_PRI				10
  #define QOS_VLAN_WRR				11
  #define QOS_DSCP2Q0				12
  
#define PHY21   21
  #define IGMP_BASE_CONTROL       0
  #define IGMP_ROUTER_PORT        1
  #define IGMP_ROUTER_TIMEOUT     2
  #define IGMP_TIMEOUT            3
  #define LUT_COMMAND             8
  #define LUT_DATA0               9
  #define LUT_DATA1               10
  #define LUT_DATA2               11
  #define LUT_DATA3               12
  #define LUT_DATA4               13
  #define ADDR_TABLE_COMMAND_REG		8
  #define ADDR_TABLE_MAC_0				9
  #define ADDR_TABLE_MAC_1				10
  #define ADDR_TABLE_MAC_2				11
  #define ADDR_TABLE_IP_0				12
  #define ADDR_TABLE_IP_1				13
  #define MIR_MODE						24
  #define MIR_RX						25
  #define MIR_TX						26
  #define BROADCAST_STORM_PROTECTION	28
  
#define PHY22   22
  #define VLAN_MODE               0
  #define VLAN_INGRESS_RULE       2
  #define VLAN_PVID0              6
  #define VLAN_VALID_ENTRY        15
  #define PB_VLAN_MEMBER_SET_0    16
  
#define PHY23   23
  #define PB_VLAN_ADD_TAG_MASK_0  0
  #define PB_VLAN_RMV_TAG_MASK_0  16
  
#define PHY24   24
  #define TB_VLAN_FID_VID_0       0
  #define TB_VLAN_ADD_TAG_0       16
  
#define PHY25   25
  #define TB_VLAN_REMOVE_TAG_0    0
  #define TB_VLAN_MEMBER_0        16

// LUT ====================================================
#define MAX_LUT_ENTRY   2048

#define LUT_MODE_2K     0
#define LUT_MODE_1K1K   1

#define LUT_MAC_UNICAST     0x0
#define LUT_MAC_IGMP	      0x1

struct LUT_entry
{
	unsigned char valid;    // in unicast: set 1 for static entry, in mcst & igmp: set 1 for valid.
	unsigned char type;     // 0:unicast 1:IGMP
	unsigned char entry;    // bit 0~1: entry num(0~3)
	unsigned char fid;      // 0~15
	unsigned char MAC[6];
	unsigned short aged;    // in unicast: 0 or 1, in mcst & igmp: 1-corresponding port is aged out
	unsigned short port_map;
};

void LUT_set_mode(unsigned char mode);
unsigned char LUT_get_mode(void);
unsigned short LUT_get_2k_hash_index(unsigned char fid, unsigned char *mac, unsigned char entry);
unsigned short LUT_get_1k1k_hash_index(unsigned char type, unsigned char fid, unsigned char *mac, unsigned char entry);
void LUT_fill_in_entry(unsigned short idx, unsigned short *ldata, struct LUT_entry *lutset);
int LUT_get_next_valid_entry(int idx, struct LUT_entry *lutset);
int LUT_set_entry(struct LUT_entry lutset);
int LUT_get_entry(unsigned short idx, struct LUT_entry *lutset);
unsigned short LUT_get_port_by_mac(unsigned char fid, unsigned char *mac);
void LUT_flush_table(void);
void LUT_flush_port(int port);
void LUT_flush_entry(unsigned char fid, unsigned char *mac);

// VLAN ===================================================
#define MAX_VLAN_ENTRY   16

#define VLAN_MODE_PORT_BASED    0
#define VLAN_MODE_TAG_BASED     1

#define VLAN_MAX_ENTRY          16

#define VLAN_ALL_FRAME          0
#define VLAN_TAGGED_FRAME       1
#define VLAN_UNTAGGED_FRAME     2

#define VLAN_UNVID_DISCARD      0
#define VLAN_UNVID_TOCPU        1
#define VLAN_UNVID_FLOOD        2

struct VLAN_entry
{
  unsigned char	valid;     // 0->disable  !0->Enable
  unsigned short	VID;     // 0~4095
  unsigned char	FID;       // 0~15
  unsigned short	member;  // bit[8:0] -> P8~P0
  unsigned short	AddTag;  // bit[8:0] -> P8~P0
  unsigned short	RmvTag;  // bit[8:0] -> P8~P0
};

int VLAN_set_mode(unsigned char port, unsigned char mode);
int VLAN_get_mode(unsigned char port);
int VLAN_PB_set_member(unsigned char port, unsigned short member);
int VLAN_PB_get_member(unsigned char port);
int VLAN_PB_set_PVID(unsigned char port, unsigned short pvid);
int VLAN_PB_get_PVID(unsigned char port);
int VLAN_PB_set_add_tag(unsigned char port, unsigned short addtag);
int VLAN_PB_get_add_tag(unsigned char port);
int VLAN_PB_set_remove_tag(unsigned char port, unsigned short rmvtag);
int VLAN_PB_get_remove_tag(unsigned char port);
int VLAN_TB_set_entry(unsigned char idx, struct VLAN_entry *vlanset);
int VLAN_TB_get_entry(unsigned char idx, struct VLAN_entry *vlanset);
int VLAN_set_ingress_type(unsigned char intype);
unsigned char VLAN_get_ingress_type(void);
int VLAN_set_ingress_filter(unsigned char port, unsigned char en);
int VLAN_get_ingress_filter(unsigned char port);
int VLAN_set_unknown_vid_mode(unsigned char mode);
unsigned char VLAN_get_unknown_vid_mode(void);

// Port ====================================================
int port_set_enable(unsigned char port,unsigned char enabled);
int port_get_enable(unsigned char port);
int port_set_flow_control(unsigned char port,unsigned char enabled);
int port_get_flow_control(unsigned char port);
int port_set_extend_mode(unsigned char port,unsigned char enabled);
int port_get_speed(unsigned char port);
int	port_get_status(unsigned char port);

// broadcast strom =========================================
int set_broadcast_storm_threshold(int value);
int get_broadcast_storm_threshold(void);

// IPC =====================================================
struct _ipc_ip_mac_entry{
	unsigned char ip_addr[4];
	unsigned char mac_addr[6];
};
struct _ipc_ip_mac_entry get_ipc_entry(unsigned char port);
int	port_get_ipc_mac(unsigned char port, unsigned char* mac);

//mirror
#define mirror_RX			0
#define mirror_TX			1
#define mirror_RX_and_TX	2
#define mirror_RX_or_TX		3
int mirror_set_enable(unsigned char enabled);
int mirror_get_enable(void);
int mirror_set_only_mir_pkt(unsigned char enabled);
int mirror_get_only_mir_pkt(void);
int mirror_set_mode(unsigned char mode);
int mirror_get_mode(void);
int mirror_set_port_enable(unsigned char port,unsigned char enabled);
int mirror_get_port_enable(unsigned char port);
int mirror_set_port_RX_enable(unsigned char port,unsigned char enabled);
int mirror_get_port_RX_enable(unsigned char port);
int mirror_set_port_TX_enable(unsigned char port,unsigned char enabled);
int mirror_get_port_TX_enable(unsigned char port);

//IGMP
#define discard					0
#define forward_to_CPU			1
#define flood_packet			2
#define forward_to_router_port	3
int igmp_set_fast_leave(unsigned char enabled);
int igmp_get_fast_leave(void);
int igmp_set_MG_INCLUDE_RP(unsigned char enabled);
int igmp_get_MG_INCLUDE_RP(void);
int igmp_set_FLOOD_UNIGMP(unsigned char enabled);
int igmp_get_FLOOD_UNIGMP(void);
int igmp_set_FLOOD_IPM_CTRL(unsigned char enabled);
int igmp_get_FLOOD_IPM_CTRL(void);
int igmp_set_UNIPM_MODE(unsigned char mode);
int igmp_get_UNIPM_MODE(void);
int igmp_set_DISCARD_LEAVE(unsigned char enabled);
int igmp_get_DISCARD_LEAVE(void);
int igmp_set_FLOOD_RPT(unsigned char enabled);
int igmp_get_FLOOD_RPT(void);
int igmp_set_LRP_NULL_SIP(unsigned char enabled);
int igmp_get_LRP_NULL_SIP(void);
int igmp_set_LEARN_RP(unsigned char enabled);
int igmp_get_LEARN_RP(void);
int igmp_set_HW_IGMP_EN(unsigned char enabled);
int igmp_get_HW_IGMP_EN(void);
int igmp_set_DEFAULT_ROUTER_PORT(unsigned char port, unsigned char RP);
int igmp_get_DEFAULT_ROUTER_PORT(unsigned char port);
int igmp_set_ROUTER_TIMEOUT_VLE(unsigned char value);
int igmp_get_ROUTER_TIMEOUT_VLE(void);
int igmp_set_ROUTER_TIMEOUT_UNIT(unsigned char unit);
int igmp_get_ROUTER_TIMEOUT_UNIT(void);
int igmp_set_IGMP_TIMEOUT_VLE(unsigned char value);
int igmp_get_IGMP_TIMEOUT_VLE(void);
int igmp_set_IGMP_TIMEOUT_UNIT(unsigned char unit);
int igmp_get_IGMP_TIMEOUT_UNIT(void);

//QOS
int QOS_set_TOS_OVER_VLAN(unsigned char enabled);
int QOS_get_TOS_OVER_VLAN(void);
int QOS_set_SP_EN(unsigned char enabled);
int QOS_get_SP_EN(void);
int QOS_set_COS_EN(unsigned char port,unsigned char enabled);
int QOS_get_COS_EN(unsigned char port);
int QOS_set_PORT_PRI_EN(unsigned char port,unsigned char enabled);
int QOS_get_PORT_PRI_EN(unsigned char port);
int QOS_set_PORT_PRI(unsigned char port,unsigned char priority);
int QOS_get_PORT_PRI(unsigned char port);
int QOS_set_VLAN_PRI2Q(unsigned char vlan_pri,unsigned char priority);
int QOS_get_VLAN_PRI2Q(unsigned char vlan_pri);
int QOS_set_WRR_Q0WEIGHT(unsigned char weight);
int QOS_set_WRR_Q1WEIGHT(unsigned char weight);
int QOS_get_WRR_Q0WEIGHT(void);
int QOS_get_WRR_Q1WEIGHT(void);
int QOS_set_DSCP2Q(unsigned char DSCP,unsigned char priority);
int QOS_get_DSCP2Q(unsigned char DSCP);

//=========================================================
struct ip179nh_priv {
  void (*LUT_set_mode)(unsigned char mode);
  unsigned char (*LUT_get_mode)(void);
  unsigned short (*LUT_get_2k_hash_index)(unsigned char fid, unsigned char *mac, unsigned char entry);
  unsigned short (*LUT_get_1k1k_hash_index)(unsigned char type, unsigned char fid, unsigned char *mac, unsigned char entry);
  int (*LUT_get_next_valid_entry)(int idx, struct LUT_entry *lutset);
  int (*LUT_set_entry)(struct LUT_entry lutset);
  int (*LUT_get_entry)(unsigned short idx, struct LUT_entry *lutset);
  unsigned short (*LUT_get_port_by_mac)(unsigned char fid, unsigned char *mac);
  void (*LUT_flush_table)(void);
  void (*LUT_flush_port)(int port);
  void (*LUT_flush_entry)(unsigned char fid, unsigned char *mac);
  int (*VLAN_set_mode)(unsigned char port, unsigned char mode);
  int (*VLAN_get_mode)(unsigned char port);
  int (*VLAN_PB_set_member)(unsigned char port, unsigned short member);
  int (*VLAN_PB_get_member)(unsigned char port);
  int (*VLAN_PB_set_PVID)(unsigned char port, unsigned short pvid);
  int (*VLAN_PB_get_PVID)(unsigned char port);
  int (*VLAN_PB_set_add_tag)(unsigned char port, unsigned short addtag);
  int (*VLAN_PB_get_add_tag)(unsigned char port);
  int (*VLAN_PB_set_remove_tag)(unsigned char port, unsigned short rmvtag);
  int (*VLAN_PB_get_remove_tag)(unsigned char port);
  int (*VLAN_TB_set_entry)(unsigned char idx, struct VLAN_entry *vlanset);
  int (*VLAN_TB_get_entry)(unsigned char idx, struct VLAN_entry *vlanset);
  int (*VLAN_set_ingress_type)(unsigned char intype);
  unsigned char (*VLAN_get_ingress_type)(void);
  int (*VLAN_set_ingress_filter)(unsigned char port, unsigned char en);
  int (*VLAN_get_ingress_filter)(unsigned char port);
  int (*VLAN_set_unknown_vid_mode)(unsigned char mode);
  unsigned char (*VLAN_get_unknown_vid_mode)(void);
  int (*port_set_enable)(unsigned char port,unsigned char enabled);
  int (*port_get_enable)(unsigned char port);
  int (*port_set_flow_control)(unsigned char port,unsigned char enabled);
  int (*port_get_flow_control)(unsigned char port);
  int (*port_set_extend_mode)(unsigned char port,unsigned char enabled);
  int (*port_get_speed)(unsigned char port);
  int (*port_get_status)(unsigned char port);
  int (*set_broadcast_storm_threshold)(int value);
  unsigned char (*get_broadcast_storm_threshold)(void);
  struct _ipc_ip_mac_entry (*get_ipc_entry)(unsigned char port);
  int (*port_get_ipc_mac)(unsigned char port, unsigned char* mac);
  int (*mirror_set_enable)(unsigned char enabled);
  int (*mirror_get_enable)(void);
  int (*mirror_set_only_mir_pkt)(unsigned char enabled);
  int (*mirror_get_only_mir_pkt)(void);
  int (*mirror_set_mode)(unsigned char mode);
  int (*mirror_get_mode)(void);
  int (*mirror_set_port_enable)(unsigned char port,unsigned char enabled);
  int (*mirror_get_port_enable)(unsigned char port);
  int (*mirror_set_port_RX_enable)(unsigned char port,unsigned char enabled);
  int (*mirror_get_port_RX_enable)(unsigned char port);
  int (*mirror_set_port_TX_enable)(unsigned char port,unsigned char enabled);
  int (*mirror_get_port_TX_enable)(unsigned char port);
  int (*igmp_set_fast_leave)(unsigned char enabled);
  int (*igmp_get_fast_leave)(void);
  int (*igmp_set_MG_INCLUDE_RP)(unsigned char enabled);
  int (*igmp_get_MG_INCLUDE_RP)(void);
  int (*igmp_set_FLOOD_UNIGMP)(unsigned char enabled);
  int (*igmp_get_FLOOD_UNIGMP)(void);
  int (*igmp_set_FLOOD_IPM_CTRL)(unsigned char enabled);
  int (*igmp_get_FLOOD_IPM_CTRL)(void);
  int (*igmp_set_UNIPM_MODE)(unsigned char mode);
  int (*igmp_get_UNIPM_MODE)(void);
  int (*igmp_set_DISCARD_LEAVE)(unsigned char enabled);
  int (*igmp_get_DISCARD_LEAVE)(void);
  int (*igmp_set_FLOOD_RPT)(unsigned char enabled);
  int (*igmp_get_FLOOD_RPT)(void);
  int (*igmp_set_LRP_NULL_SIP)(unsigned char enabled);
  int (*igmp_get_LRP_NULL_SIP)(void);
  int (*igmp_set_LEARN_RP)(unsigned char enabled);
  int (*igmp_get_LEARN_RP)(void);
  int (*igmp_set_HW_IGMP_EN)(unsigned char enabled);
  int (*igmp_get_HW_IGMP_EN)(void);
  int (*igmp_set_DEFAULT_ROUTER_PORT)(unsigned char port, unsigned char RP);
  int (*igmp_get_DEFAULT_ROUTER_PORT)(unsigned char port);
  int (*igmp_set_ROUTER_TIMEOUT_VLE)(unsigned char value);
  int (*igmp_get_ROUTER_TIMEOUT_VLE)(void);
  int (*igmp_set_ROUTER_TIMEOUT_UNIT)(unsigned char unit);
  int (*igmp_get_ROUTER_TIMEOUT_UNIT)(void);
  int (*igmp_set_IGMP_TIMEOUT_VLE)(unsigned char value);
  int (*igmp_get_IGMP_TIMEOUT_VLE)(void);
  int (*igmp_set_IGMP_TIMEOUT_UNIT)(unsigned char unit);
  int (*igmp_get_IGMP_TIMEOUT_UNIT)(void);
  int (*QOS_set_TOS_OVER_VLAN)(unsigned char enabled);
  int (*QOS_get_TOS_OVER_VLAN)(void);
  int (*QOS_set_SP_EN)(unsigned char enabled);
  int (*QOS_get_SP_EN)(void);
  int (*QOS_set_COS_EN)(unsigned char port,unsigned char enabled);
  int (*QOS_get_COS_EN)(unsigned char port);
  int (*QOS_set_PORT_PRI_EN)(unsigned char port,unsigned char enabled);
  int (*QOS_get_PORT_PRI_EN)(unsigned char port);
  int (*QOS_set_PORT_PRI)(unsigned char port,unsigned char priority);
  int (*QOS_get_PORT_PRI)(unsigned char port);
  int (*QOS_set_VLAN_PRI2Q)(unsigned char vlan_pri,unsigned char priority);
  int (*QOS_get_VLAN_PRI2Q)(unsigned char vlan_pri);
  int (*QOS_set_WRR_Q0WEIGHT)(unsigned char weight);
  int (*QOS_set_WRR_Q1WEIGHT)(unsigned char weight);
  int (*QOS_get_WRR_Q0WEIGHT)(void);
  int (*QOS_get_WRR_Q1WEIGHT)(void);
  int (*QOS_set_DSCP2Q)(unsigned char DSCP,unsigned char priority);
  int (*QOS_get_DSCP2Q)(unsigned char DSCP);
};

#endif
