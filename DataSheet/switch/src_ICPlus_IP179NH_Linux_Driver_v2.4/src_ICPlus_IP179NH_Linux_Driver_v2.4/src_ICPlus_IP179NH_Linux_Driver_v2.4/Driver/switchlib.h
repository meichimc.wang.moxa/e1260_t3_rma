/******************************************************************************
 *
 *   Name:           switchlib.h
 *
 *   Description:    switch Driver Header File
 *
 *   Copyright:      (c) 2009-2050    IC Plus Corp.
 *                   All rights reserved.
 *
 *******************************************************************************/
#ifndef _SWITCH_H_
#define _SWITCH_H_

/*struct GeneralSetting
{
  unsigned char phy;
	unsigned char reg;
	unsigned short data;
};*/

//=========================================================
#define C_SUCCESS   0
#define C_FAIL		  -1

#define ENABLE      1
#define DISABLE     0

#define MAX_PORT_NUM    9

// Registers ==============================================
#define PHY20   20
  #define LEARN_FORWARD_ENABLE    7
  
#define PHY21   21
  #define LUT_COMMAND             8
  #define LUT_DATA0               9
  #define LUT_DATA1               10
  #define LUT_DATA2               11
  #define LUT_DATA3               12
  #define LUT_DATA4               13
  
#define PHY22   22
  #define VLAN_MODE               0
  #define VLAN_INGRESS_RULE       2
  #define VLAN_PVID0              6
  #define VLAN_VALID_ENTRY        15
  #define PB_VLAN_MEMBER_SET_0    16
  
#define PHY23   23
  #define PB_VLAN_ADD_TAG_MASK_0  0
  #define PB_VLAN_RMV_TAG_MASK_0  16
  
#define PHY24   24
  #define TB_VLAN_FID_VID_0       0
  #define TB_VLAN_ADD_TAG_0       16
  
#define PHY25   25
  #define TB_VLAN_REMOVE_TAG_0    0
  #define TB_VLAN_MEMBER_0        16

// LUT ====================================================
#define MAX_LUT_ENTRY   2048

#define LUT_MODE_2K     0
#define LUT_MODE_1K1K   1

#define LUT_MAC_UNICAST     0x0
#define LUT_MAC_IGMP	      0x1

/*struct LUT_entry
{
	unsigned char valid;    // in unicast: set 1 for static entry, in mcst & igmp: set 1 for valid.
	unsigned char type;     // 0:unicast 1:IGMP
	unsigned char entry;    // bit 0~1: entry num(0~3)
	unsigned char fid;      // 0~15
	unsigned char MAC[6];
	unsigned short aged;    // in unicast: 0 or 1, in mcst & igmp: 1-corresponding port is aged out
	unsigned short port_map;
};*/

void LUT_set_mode(unsigned char mode);
unsigned char LUT_get_mode(void);
unsigned short LUT_get_2k_hash_index(unsigned char fid, unsigned char *mac, unsigned char entry);
unsigned short LUT_get_1k1k_hash_index(unsigned char type, unsigned char fid, unsigned char *mac, unsigned char entry);
void LUT_fill_in_entry(unsigned short idx, unsigned short *ldata, struct LUT_entry *lutset);
int LUT_get_next_valid_entry(int idx, struct LUT_entry *lutset);
int LUT_set_entry(struct LUT_entry lutset);
int LUT_get_entry(unsigned short idx, struct LUT_entry *lutset);
unsigned short LUT_get_port_by_mac(unsigned char fid, unsigned char *mac);
void LUT_flush_table(void);
void LUT_flush_port(int port);
void LUT_flush_entry(unsigned char fid, unsigned char *mac);

// VLAN ===================================================
#define MAX_VLAN_ENTRY   16

#define VLAN_MODE_PORT_BASED    0
#define VLAN_MODE_TAG_BASED     1

#define VLAN_MAX_ENTRY          16

#define VLAN_ALL_FRAME          0
#define VLAN_TAGGED_FRAME       1
#define VLAN_UNTAGGED_FRAME     2

#define VLAN_UNVID_DISCARD      0
#define VLAN_UNVID_TOCPU        1
#define VLAN_UNVID_FLOOD        2

/*struct VLAN_entry
{
  unsigned char	valid;   // 0->disable  !0->Enable
  unsigned short	VID;     // 0~4095
  unsigned char	FID;     // 0~15
  unsigned short	member;  // bit[8:0] -> P8~P0
  unsigned short	AddTag;  // bit[8:0] -> P8~P0
  unsigned short	RmvTag;  // bit[8:0] -> P8~P0
};*/

int VLAN_set_mode(unsigned char port, unsigned char mode);
int VLAN_get_mode(unsigned char port);
int VLAN_PB_set_member(unsigned char port, unsigned short member);
int VLAN_PB_get_member(unsigned char port);
int VLAN_PB_set_PVID(unsigned char port, unsigned short pvid);
int VLAN_PB_get_PVID(unsigned char port);
int VLAN_PB_set_add_tag(unsigned char port, unsigned short addtag);
int VLAN_PB_get_add_tag(unsigned char port);
int VLAN_PB_set_remove_tag(unsigned char port, unsigned short rmvtag);
int VLAN_PB_get_remove_tag(unsigned char port);
int VLAN_TB_set_entry(unsigned char idx, struct VLAN_entry *vlanset);
int VLAN_TB_get_entry(unsigned char idx, struct VLAN_entry *vlanset);
int VLAN_set_ingress_type(unsigned char intype);
unsigned char VLAN_get_ingress_type(void);
int VLAN_set_ingress_filter(unsigned char port, unsigned char en);
int VLAN_get_ingress_filter(unsigned char port);
int VLAN_set_unknown_vid_mode(unsigned char mode);
unsigned char VLAN_get_unknown_vid_mode(void);

#endif
