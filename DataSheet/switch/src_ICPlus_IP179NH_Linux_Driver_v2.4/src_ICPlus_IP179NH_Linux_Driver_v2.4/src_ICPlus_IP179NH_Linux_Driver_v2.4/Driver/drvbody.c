#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/unistd.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/mii.h>
#include <linux/ethtool.h>
#include <linux/phy.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/mutex.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

//*****************************************************************************

#include "drvbody.h"
#include "drvlib.h"

#define DRV_NAME	"ip179"
#define DRV_VERSION	"2.2.0"
#define DRV_RELDATE	"2020-10-05"
#define KERNEL_V4_14_138


MODULE_VERSION(DRV_VERSION);
MODULE_LICENSE("Dual BSD/GPL");

static DEFINE_MUTEX(rw_mutex);
struct mii_bus *mbus = NULL;
static dev_t devNumber;

//*****************************************************************************
// MDIO
//*****************************************************************************
#define SWITCH_GPIO_MDIO


#ifdef SWITCH_GPIO_MDIO
#define MDIO_DELAY	500
#define MDIO_RD		0
#define MDIO_WR		1


#define SWITCH_GPIO_FUNC
#ifdef SWITCH_GPIO_FUNC
//--------------   example : switch GPIO function ---------------------
#define GPIO_MDIO_OFFSET 0xB
#define GPIO_MDC_OFFSET 0xA

static DEFINE_MUTEX(switch_gpio_lock);
#define SWITCH_GPIO_BASE      0xBD700000
#define SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET	0x003C
#define SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET	0x0040 
#define SWITCH_GPIO_DIRECTION_0_OFFSET		0x0008 
#define SWITCH_GPIO_INPUT_VALUE_0_OFFSET	0x0038
#define SWITCH_GPIO_SET_VALUE_1 0x1
#define SWITCH_GPIO_SET_VALUE_0 0x0
static int switch_gpio_get(unsigned offset)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;
	
	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_INPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = ((val & BIT(offset))>>offset);

	mutex_unlock(&switch_gpio_lock);

	return val;
}
static void switch_gpio_set(unsigned offset,int value)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = (value == SWITCH_GPIO_SET_VALUE_1) ? (val | BIT(offset)) : (val & ~((BIT(offset))));
	writel(val, base);

	mutex_unlock(&switch_gpio_lock);

	return;
}
static int switch_gpio_direction_out(unsigned offset,int value)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

/*	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_DIRECTION_0_OFFSET;
	val = readl(base);
	val |= BIT(offset);
	writel(val, base);*/

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = (value == SWITCH_GPIO_SET_VALUE_1) ? (val | BIT(offset)) : (val & ~((BIT(offset))));
	writel(val, base);

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET;
	val = readl(base);
	val |= BIT(offset);
	writel(val, base);

	mutex_unlock(&switch_gpio_lock);

	return 0;
}
static int switch_gpio_direction_in( unsigned offset)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET;
	val = readl(base);
	val &= ~(BIT(offset));
	writel(val, base);

/*	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_DIRECTION_0_OFFSET;
	val = readl(base);
	val &= ~(BIT(offset));
	writel(val, base);*/

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_INPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = ((val & BIT(offset))>>offset);

	mutex_unlock(&switch_gpio_lock);

	return val;
}

#endif
//-------------------------------------------------------------------------------------

void ic_mdio_init(void)
{
#ifdef SWITCH_GPIO_FUNC
	switch_gpio_direction_out(GPIO_MDC_OFFSET,0);
	switch_gpio_direction_out(GPIO_MDIO_OFFSET,0);
#endif  
}
void mdio_set_MDC_MDIO_direction(unsigned char mdc, unsigned char mdio)//0:input, 1:output for mdc/mdio values
{
#ifdef SWITCH_GPIO_FUNC
    if(mdc) ;
    if(mdio)
      switch_gpio_direction_out(GPIO_MDIO_OFFSET,0);
    else
      switch_gpio_direction_in(GPIO_MDIO_OFFSET);
#endif         
}

void mdio_set_MDC_1(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDC_OFFSET,1);
#endif      
}
void mdio_set_MDC_0(void)
{ 
#ifdef SWITCH_GPIO_FUNC 
      switch_gpio_set(GPIO_MDC_OFFSET,0);
#endif      
}
void mdio_set_MDIO_1(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDIO_OFFSET,1);
#endif      
}
void mdio_set_MDIO_0(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDIO_OFFSET,0);
#endif      
}
unsigned int mdio_get_MDIO_value(void)
{
#ifdef SWITCH_GPIO_FUNC
  return switch_gpio_get(GPIO_MDIO_OFFSET);
#endif  
}

void mdio_1(void){
	int i=0;

//set MDIO to 1
//set MDC to 0
        mdio_set_MDIO_1();
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

//set MDIO to 1
//set MDC to 1
        mdio_set_MDIO_1();
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

}

void mdio_0(void){
	int i=0;

//set MDIO to 0
//set MDC to 0
        mdio_set_MDIO_0();
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

//set MDIO to 0
//set MDC to 1
        mdio_set_MDIO_0();
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

}

void mdio_z0(void){
	int i=0;

//set MDC to 0
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 1
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 0
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 1
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
}

void mdio_start(void){
	mdio_0();
	mdio_1();
}

void mdio_rw(int rw){
	if(rw==MDIO_RD){
		mdio_1();
		mdio_0();
	}else{
		mdio_0();
		mdio_1();
	}
}

void ic_mdio_wr(unsigned short pa, unsigned short ra, unsigned short va){
	int i=0;
	unsigned short data=0;

//set MDC/MDIO pins to GPIO mode
	ic_mdio_init();

//set MDC direction to output
//set MDIO direction to output
	mdio_set_MDC_MDIO_direction(1,1);

	for(i=0;i<32;i++)
		mdio_1();
	mdio_start();
	mdio_rw(MDIO_WR);
	for(i=0;i<5;i++){
		if((pa>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	for(i=0;i<5;i++){
		if((ra>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	mdio_1();
	mdio_0();
	for(i=0;i<16;i++){
		data=va<<i;
		data=data>>15;
		if(data==1)
			mdio_1();
		else
			mdio_0();
	}
}

unsigned short ic_mdio_rd(unsigned short pa, unsigned short ra){
	int i=0,j=0;
	unsigned short data=0;
	int regBit;
	unsigned char debug[16];

//set MDC/MDIO pins to GPIO mode
	ic_mdio_init();

//set MDC/MDIO PIN direction
//mdio_set_MDC_MDIO_dir();
//MDC direction set to output
//MDIO direction set to output
	mdio_set_MDC_MDIO_direction(1,1);

	for(i=0;i<32;i++)
		mdio_1();
	mdio_start();
	mdio_rw(MDIO_RD);
	for(i=0;i<5;i++){
		if((pa>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	for(i=0;i<5;i++){
		if((ra>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
  
//set MDC/MDIO PIN direction
//mdio_set_MDC_MDIO_dir();
//MDIO DIR set to input
	mdio_set_MDC_MDIO_direction(1,0);
        
	mdio_z0();
	for(j=0;j<16;j++){
		//regBit=mdio_readbit();
//MDC set to 0
		mdio_set_MDC_0();

		for(i=0;i<MDIO_DELAY;i++);
		for(i=0;i<MDIO_DELAY;i++);

//get MDIO value
		regBit=mdio_get_MDIO_value();

		if(regBit==0)
		{
			data|=0;
			debug[15-j]=0;
		}
		else
		{
			data|=1;
            debug[15-j]=1;
		}
		if(j<15)
			data=data<<1;

//MDC set to 1
		mdio_set_MDC_1();

		for(i=0;i<MDIO_DELAY;i++);
		for(i=0;i<MDIO_DELAY;i++);
	}
//MDC set to 0
	mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//	reg=GPREG(GPVAL);
//MDC set to 1
	mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

	return data;
}
#endif

//*****************************************************************************

unsigned short switch_mdio_rd(unsigned short pa, unsigned short ra)
{
	unsigned short val=0;
  
 	mutex_lock(&rw_mutex);
#ifdef SWITCH_GPIO_MDIO
	val = ic_mdio_rd(pa,ra);
#else
	if (mbus == NULL)
	{
		// add code
	}
	else
	{
		val = mdiobus_read(mbus, pa, ra);
	}	
#endif  
	mutex_unlock(&rw_mutex);
  
	return val;
}
void switch_mdio_wr(unsigned short pa, unsigned short ra, unsigned short va)
{
 	mutex_lock(&rw_mutex);
#ifdef SWITCH_GPIO_MDIO
	ic_mdio_wr(pa,ra,va);
#else
	if (mbus == NULL)
	{
		// add code
	}
	else
	{
		mdiobus_write(mbus, pa, ra, va);
	}
 
#endif
	mutex_unlock(&rw_mutex); 
}
//*****************************************************************************
static int ip179nh_probe(struct phy_device *phydev)
{
#ifdef SWITCHDEBUG
	printk("IP179 Switch: %s\n",__func__);
#endif
#ifdef KERNEL_V4_14_138
	mbus = phydev->mdio.bus;
#else
	mbus = phydev->bus;
#endif	
	return 0;
}

static int ip179nh_config_init(struct phy_device *phydev)
{
	int err, i, tmp;
	static int full_reset_performed;
	struct ip179nh_priv *ops;
	struct mii_bus *mb;

	printk("IP179 Switch: %s\n",__func__);

#ifdef KERNEL_V4_14_138
	mb=phydev->mdio.bus;	
#else
	mb=phydev->bus;
#endif
	if (full_reset_performed == 0) {
    	err = mdiobus_write(mb, 20, 0, 0x55AA);
		if (err < 0)
			return err;

		err = mdiobus_read(mb, 20, 0);
		mdelay(2);

		for (i = 8; i < 16; i++) {
			err = mdiobus_write(mb, i, MII_BMCR,
								(BMCR_RESET | BMCR_SPEED100 | BMCR_ANENABLE | BMCR_FULLDPLX));
			if (err < 0)
				return err;
		}

		for (i = 8; i < 16; i++)
		{
			err = mdiobus_read(mb, i, MII_BMCR);
		}	

		mdelay(2);
    	mbus = mb;
		full_reset_performed = 1;
	}
	tmp = mdiobus_read(mb, 23, 29);      

	if ((tmp & 0x3) == 0x3)
    	phydev->speed = SPEED_1000;
	else
		phydev->speed = SPEED_100;

	phydev->state = PHY_NOLINK;
	phydev->duplex = DUPLEX_FULL;
	phydev->link = 1;

	ops = kmalloc(sizeof(struct ip179nh_priv), GFP_KERNEL);
	ops->LUT_set_mode				= &LUT_set_mode;
	ops->LUT_get_mode				= &LUT_get_mode;
	ops->LUT_get_2k_hash_index		= &LUT_get_2k_hash_index;
	ops->LUT_get_1k1k_hash_index	= &LUT_get_1k1k_hash_index;
	ops->LUT_get_next_valid_entry	= &LUT_get_next_valid_entry;
	ops->LUT_set_entry				= &LUT_set_entry;
	ops->LUT_get_entry				= &LUT_get_entry;
	ops->LUT_get_port_by_mac		= &LUT_get_port_by_mac;
	ops->LUT_flush_table			= &LUT_flush_table;
	ops->LUT_flush_port				= &LUT_flush_port;
	ops->LUT_flush_entry			= &LUT_flush_entry;
	ops->VLAN_set_mode				= &VLAN_set_mode;
	ops->VLAN_get_mode				= &VLAN_get_mode;
	ops->VLAN_PB_set_member			= &VLAN_PB_set_member;
	ops->VLAN_PB_get_member			= &VLAN_PB_get_member;
	ops->VLAN_PB_set_PVID			= &VLAN_PB_set_PVID;
	ops->VLAN_PB_get_PVID			= &VLAN_PB_get_PVID;
	ops->VLAN_PB_set_add_tag		= &VLAN_PB_set_add_tag;
	ops->VLAN_PB_get_add_tag		= &VLAN_PB_get_add_tag;
	ops->VLAN_PB_set_remove_tag		= &VLAN_PB_set_remove_tag;
	ops->VLAN_PB_get_remove_tag		= &VLAN_PB_get_remove_tag;
	ops->VLAN_TB_set_entry			= &VLAN_TB_set_entry;
	ops->VLAN_TB_get_entry			= &VLAN_TB_get_entry;
	ops->VLAN_set_ingress_type		= &VLAN_set_ingress_type;
	ops->VLAN_get_ingress_type		= &VLAN_get_ingress_type;
	ops->VLAN_set_ingress_filter	= &VLAN_set_ingress_filter;
	ops->VLAN_get_ingress_filter	= &VLAN_get_ingress_filter;
	ops->VLAN_set_unknown_vid_mode	= &VLAN_set_unknown_vid_mode;
	ops->VLAN_get_unknown_vid_mode	= &VLAN_get_unknown_vid_mode;
	phydev->priv = ops;
    
	netif_carrier_on(phydev->attached_dev);
	return 0;
}


static int ip179nh_read_status(struct phy_device *phydev)
{
	int tmp;
#ifdef KERNEL_V4_14_138  
	tmp = mdiobus_read(phydev->mdio.bus, 23, 29);      
#else
	tmp = mdiobus_read(phydev->bus, 23, 29);    
#endif
	if ((tmp & 0x3) == 0x3)
    	phydev->speed = SPEED_1000;
	else
    	phydev->speed = SPEED_100;

	phydev->state = PHY_RUNNING;
	phydev->duplex = DUPLEX_FULL;
	phydev->link = 1;

	return 0;
}

static int ip179nh_config_aneg(struct phy_device *phydev)
{
#ifdef SWITCHDEBUG
	printk("IP179 Switch: %s\n",__func__);
#endif
	return 0;
}

static struct phy_driver ip179nh_driver = {
	.phy_id		= 0x02430C10,
	.name		= "ICPlus IP179N",
	.phy_id_mask= 0x0ffffff0,
	.features	= PHY_BASIC_FEATURES,
	.probe 		= &ip179nh_probe,
	.config_init	= &ip179nh_config_init,
	.config_aneg	= &ip179nh_config_aneg,
	.read_status	= &ip179nh_read_status,
	.suspend	= genphy_suspend,
	.resume		= genphy_resume,
#ifndef KERNEL_V4_14_138
	.driver		= { .owner = THIS_MODULE,},
#endif
};

static struct phy_driver ip1790_driver = {
	.phy_id		= 0x0,
	.name		= "ICPlus IP179N_1",
	.phy_id_mask	= 0x0ffffff0,
	.features	= PHY_BASIC_FEATURES,
	.probe 		= &ip179nh_probe,
	.config_init	= &ip179nh_config_init,
	.config_aneg	= &ip179nh_config_aneg,
	.read_status	= &ip179nh_read_status,
	.suspend	= genphy_suspend,
	.resume		= genphy_resume,
#ifndef KERNEL_V4_14_138
	.driver		= { .owner = THIS_MODULE,},
#endif
};

//*****************************************************************************


static struct cdev switch_cdev;
static int switch_major = 246;
static DEFINE_MUTEX(switch_mutex);

//#define SWITCH_NAME	"switch_cdev"
#define SWITCH_NAME	"dev_switch"

static int switch_open(struct inode *inode, struct file *fs)
{
#ifdef SWITCHDEBUG
	printk("IP179 Switch: open...\n"); 
#endif
	try_module_get(THIS_MODULE);

	return 0;
}

static int switch_release(struct inode *inode, struct file *file)
{
	module_put(THIS_MODULE);
#ifdef SWITCHDEBUG
	printk("IP179 Switch: release!\n");
#endif
	return 0;
}

static ssize_t switch_read(struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
#ifdef SWITCHDEBUG
	printk("IP179 Switch: read ...\n"); 
#endif
	return 0;
}

static ssize_t switch_write(struct file *filp, const char __user *buff, size_t len, loff_t *off)
{
#ifdef SWITCHDEBUG
	printk("IP179 Switch: write ...\n"); 
#endif
	return 0;
}

static int switch_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
	unsigned long len, rwcmd;
	unsigned char phyaddr,regaddr;
	unsigned short regdata;
	void *cptr;
	char *cdata;
	int ret=0x0;

#ifdef SWITCHDEBUG
	printk(KERN_ALERT "IP179 Switch: +ioctl...\n");
#endif
	len = (int)(_IOC_SIZE(cmd));
	rwcmd = (int)(_IOC_DIR(cmd));
	cptr = (void *)arg;
	
	cdata = kmalloc(len, GFP_KERNEL);
	if (!cdata)
	{
		ret = -ENOMEM;
		goto out_switch_ioctl;
	}

	if (copy_from_user(cdata, cptr, len))
	{
		ret = -EFAULT;
		goto out_switch_ioctl;
	}
	phyaddr = *((unsigned char *)(cdata));
    regaddr = *((unsigned char *)(cdata+1));
	regdata = *((unsigned short *)(cdata+2));

#ifdef SWITCHDEBUG
	printk(KERN_ALERT "phyaddr=0x%02X  regaddr=0x%02X  regdata=0x%04X\n",
    		(unsigned char)phyaddr, (unsigned char)regaddr, (unsigned short)regdata);
#endif
	if(regaddr>0xffff)
	{
		ret = -EINVAL;
		goto out_switch_ioctl;
	}
	switch(cmd)
	{
		case SWITCH_READ:
#ifdef SWITCHDEBUG
			printk(KERN_ALERT "SWITCH_READ phyaddr=0x%02X  regaddr=0x%02X  regdata=0x%04X\n",
					(unsigned char)phyaddr, (unsigned char)regaddr,(unsigned short)regdata);
#endif        
			*((unsigned short *)(cdata+2)) = switch_mdio_rd(phyaddr, regaddr);
			break;
		case SWITCH_WRITE:
#ifdef SWITCHDEBUG
			printk(KERN_ALERT "SWITCH_WRITE phyaddr=0x%02X  regaddr=0x%04X  regdata=0x%04X\n",
						(unsigned char)phyaddr, (unsigned char)regaddr,(unsigned short)regdata);  
#endif 
			switch_mdio_wr(phyaddr, regaddr, regdata);         
		break;
    }
		if (copy_to_user(cptr, cdata, len))
		{
			ret = -EFAULT;
			goto out_switch_ioctl;
		}
		cptr= (void *)*((unsigned long *)cdata);
		len = *((unsigned long *)(cdata+4));

		kfree(cdata);
		cdata = NULL;
		
out_switch_ioctl:
	if(cdata)
	{
		//memset(cdata, 0x0, len);
		kfree(cdata);
	}
#ifdef SWITCHDEBUG
	printk(KERN_ALERT "IP179 Switch: -ioctl...\n");
#endif
	return (ret < 0) ? ret : 0;
}

static long switch_unlocked_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
	int ret;

	mutex_lock(&switch_mutex);
	ret = switch_ioctl(filep, cmd, arg);
	mutex_unlock(&switch_mutex);

	return ret;
}

static struct file_operations switch_fops = {
	.owner			= THIS_MODULE,
	.read			= switch_read, 
	.write			= switch_write,
	.unlocked_ioctl	= switch_unlocked_ioctl,
	.open			= switch_open,
	.release		= switch_release
};

static int __init switch_init(void)
{
	int result;

#ifdef KERNEL_V4_14_138 
	result = phy_driver_register(&ip179nh_driver, THIS_MODULE);
#else
	result = phy_driver_register(&ip179nh_driver);
#endif
	if (result < 0)
	{
		printk(KERN_WARNING "IP179nh Switch: driver register failed!\n");
		return -ENODEV;
	}

#ifdef KERNEL_V4_14_138  
	result = phy_driver_register(&ip1790_driver, THIS_MODULE);
#else
	result = phy_driver_register(&ip1790_driver);
#endif	
	if (result < 0)
	{
		printk(KERN_WARNING "IP1790 Switch: driver register failed! (%d)\n", result);
		phy_driver_unregister(&ip179nh_driver);
		return result;
	}
   	devNumber=MKDEV(switch_major, 0);
   	printk("IP179 Switch: register major %d\n", switch_major);
	result = register_chrdev_region(devNumber, 1, SWITCH_NAME);
	if (result < 0)
	{
		printk(KERN_WARNING "IP179 Switch: can't get major %d\n", switch_major);
		phy_driver_unregister(&ip1790_driver);
		phy_driver_unregister(&ip179nh_driver);
		return result;
	}
	cdev_init(&switch_cdev, &switch_fops);
	switch_cdev.owner = THIS_MODULE;
	result = cdev_add(&switch_cdev, devNumber, 1);
	if (result)
	{
		printk(KERN_WARNING "IP179 Switch: error %d adding driver\n", result);
		phy_driver_unregister(&ip1790_driver);
		phy_driver_unregister(&ip179nh_driver);
		return result;
	}
	else
	{
		printk("IP179 Switch: driver loaded! \n");
	}
	return 0;
}

static void __exit switch_exit(void)
{
	cdev_del(&switch_cdev);
	unregister_chrdev_region(devNumber, 1);
	printk("IP179 Switch: driver unloaded!\n");
 
	phy_driver_unregister(&ip1790_driver);
	phy_driver_unregister(&ip179nh_driver);
 
}  

module_init(switch_init);
module_exit(switch_exit);

