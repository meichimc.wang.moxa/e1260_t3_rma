#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>
//**************************************

#include "switchdriver.h"
#include "switchlib.h"

MODULE_LICENSE("Dual BSD/GPL");

//***************************************************************************
// MDIO
//***************************************************************************
#define SWITCH_GPIO_MDIO


#ifdef SWITCH_GPIO_MDIO
#define MDIO_DELAY	500
#define MDIO_RD		0
#define MDIO_WR		1


#define SWITCH_GPIO_FUNC
#ifdef SWITCH_GPIO_FUNC
//--------------   example : switch GPIO function ---------------------
#define GPIO_MDIO_OFFSET 0xA
#define GPIO_MDC_OFFSET 0xB
#include <asm/io.h>
static DEFINE_MUTEX(switch_gpio_lock);
#define SWITCH_GPIO_BASE      0xBD700000
#define SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET	0x003C
#define SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET	0x0040 
#define SWITCH_GPIO_DIRECTION_0_OFFSET		0x0008 
#define SWITCH_GPIO_INPUT_VALUE_0_OFFSET	0x0038
#define SWITCH_GPIO_SET_VALUE_1 0x1
#define SWITCH_GPIO_SET_VALUE_0 0x0
static int switch_gpio_get(unsigned offset)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;
	
	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_INPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = ((val & BIT(offset))>>offset);

	mutex_unlock(&switch_gpio_lock);

	return val;
}
static void switch_gpio_set(unsigned offset,int value)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = (value == SWITCH_GPIO_SET_VALUE_1) ? (val | BIT(offset)) : (val & ~((BIT(offset))));
	writel(val, base);

	mutex_unlock(&switch_gpio_lock);

	return;
}
static int switch_gpio_direction_out(unsigned offset,int value)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

/*	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_DIRECTION_0_OFFSET;
	val = readl(base);
	val |= BIT(offset);
	writel(val, base);*/

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = (value == SWITCH_GPIO_SET_VALUE_1) ? (val | BIT(offset)) : (val & ~((BIT(offset))));
	writel(val, base);

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET;
	val = readl(base);
	val |= BIT(offset);
	writel(val, base);

	mutex_unlock(&switch_gpio_lock);

	return 0;
}
static int switch_gpio_direction_in( unsigned offset)
{
	void __iomem				*base;
	void __iomem				*base_offset = 0x0000;
	u32					val;

	mutex_lock(&switch_gpio_lock);

	if((0<=offset)&&(offset<=31))
		base_offset = 0x0000;
	else if((32<=offset)&&(offset<=47)) {
		base_offset = (void __iomem *)0x0004;
		offset -= 32;
	}

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_OUTPUT_ENABLE_0_OFFSET;
	val = readl(base);
	val &= ~(BIT(offset));
	writel(val, base);

/*	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_DIRECTION_0_OFFSET;
	val = readl(base);
	val &= ~(BIT(offset));
	writel(val, base);*/

	base = SWITCH_GPIO_BASE + base_offset + SWITCH_GPIO_INPUT_VALUE_0_OFFSET;
	val = readl(base);
	val = ((val & BIT(offset))>>offset);

	mutex_unlock(&switch_gpio_lock);

	return val;
}

#endif
//-------------------------------------------------------------------------------------

void ic_mdio_init(void)
{
#ifdef SWITCH_GPIO_FUNC
  switch_gpio_direction_out(GPIO_MDC_OFFSET,0);
  switch_gpio_direction_out(GPIO_MDIO_OFFSET,0);
#endif  
}
void mdio_set_MDC_MDIO_direction(unsigned char mdc, unsigned char mdio)//0:input, 1:output for mdc/mdio values
{
#ifdef SWITCH_GPIO_FUNC
    if(mdc) ;
    if(mdio)
      switch_gpio_direction_out(GPIO_MDIO_OFFSET,0);
    else
      switch_gpio_direction_in(GPIO_MDIO_OFFSET);
#endif         
}

void mdio_set_MDC_1(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDC_OFFSET,1);
#endif      
}
void mdio_set_MDC_0(void)
{ 
#ifdef SWITCH_GPIO_FUNC 
      switch_gpio_set(GPIO_MDC_OFFSET,0);
#endif      
}
void mdio_set_MDIO_1(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDIO_OFFSET,1);
#endif      
}
void mdio_set_MDIO_0(void)
{
#ifdef SWITCH_GPIO_FUNC
      switch_gpio_set(GPIO_MDIO_OFFSET,0);
#endif      
}
unsigned int mdio_get_MDIO_value(void)
{
#ifdef SWITCH_GPIO_FUNC
  return switch_gpio_get(GPIO_MDIO_OFFSET);
#endif  
}

void mdio_1(void){
	int i=0;

//set MDIO to 1
//set MDC to 0
        mdio_set_MDIO_1();
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

//set MDIO to 1
//set MDC to 1
        mdio_set_MDIO_1();
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

}

void mdio_0(void){
	int i=0;

//set MDIO to 0
//set MDC to 0
        mdio_set_MDIO_0();
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

//set MDIO to 0
//set MDC to 1
        mdio_set_MDIO_0();
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

}

void mdio_z0(void){
	int i=0;

//set MDC to 0
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 1
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 0
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//set MDC to 1
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
}

void mdio_start(void){
	mdio_0();
	mdio_1();
}

void mdio_rw(int rw){
	if(rw==MDIO_RD){
		mdio_1();
		mdio_0();
	}else{
		mdio_0();
		mdio_1();
	}
}

void ic_mdio_wr(unsigned short pa, unsigned short ra, unsigned short va){
	int i=0;
	unsigned short data=0;

//set MDC/MDIO pins to GPIO mode
	ic_mdio_init();

//set MDC direction to output
//set MDIO direction to output
        mdio_set_MDC_MDIO_direction(1,1);

	for(i=0;i<32;i++)
		mdio_1();
	mdio_start();
	mdio_rw(MDIO_WR);
	for(i=0;i<5;i++){
		if((pa>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	for(i=0;i<5;i++){
		if((ra>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	mdio_1();
	mdio_0();
	for(i=0;i<16;i++){
		data=va<<i;
		data=data>>15;
		if(data==1)
			mdio_1();
		else
			mdio_0();
	}
}

unsigned short ic_mdio_rd(unsigned short pa, unsigned short ra){
	int i=0,j=0;
	unsigned short data=0;
        int regBit;
        unsigned char debug[16];

//set MDC/MDIO pins to GPIO mode
	ic_mdio_init();

//set MDC/MDIO PIN direction
//mdio_set_MDC_MDIO_dir();
//MDC direction set to output
//MDIO direction set to output
        mdio_set_MDC_MDIO_direction(1,1);

	for(i=0;i<32;i++)
		mdio_1();
	mdio_start();
	mdio_rw(MDIO_RD);
	for(i=0;i<5;i++){
		if((pa>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
	for(i=0;i<5;i++){
		if((ra>>(5-1-i))%2)
			mdio_1();
		else
			mdio_0();
	}
  
//set MDC/MDIO PIN direction
//mdio_set_MDC_MDIO_dir();
//MDIO DIR set to input
        mdio_set_MDC_MDIO_direction(1,0);
        
	mdio_z0();


	for(j=0;j<16;j++){
		//regBit=mdio_readbit();
//MDC set to 0
                mdio_set_MDC_0();

		for(i=0;i<MDIO_DELAY;i++);
		for(i=0;i<MDIO_DELAY;i++);

//get MDIO value
                regBit=mdio_get_MDIO_value();

		if(regBit==0)
                {
			data|=0;
                debug[15-j]=0;
                }
		else
                {
			data|=1;
                debug[15-j]=1;
                }
		if(j<15)
			data=data<<1;

//MDC set to 1
                mdio_set_MDC_1();

		for(i=0;i<MDIO_DELAY;i++);
		for(i=0;i<MDIO_DELAY;i++);
	}
//MDC set to 0
        mdio_set_MDC_0();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);
//	reg=GPREG(GPVAL);
//MDC set to 1
        mdio_set_MDC_1();

	for(i=0;i<MDIO_DELAY;i++);
	for(i=0;i<MDIO_DELAY;i++);

	return data;
}
#endif
//***************************************************************************
unsigned short switch_mdio_rd(unsigned short pa, unsigned short ra)
{
#ifdef SWITCH_GPIO_MDIO
  return ic_mdio_rd(pa,ra);
#else
#endif  
}
void switch_mdio_wr(unsigned short pa, unsigned short ra, unsigned short va)
{
#ifdef SWITCH_GPIO_MDIO
  ic_mdio_wr(pa,ra,va);
#else
#endif 
}



static struct cdev switch_cdev;
static int switch_major = 247;
static DEFINE_MUTEX(switch_mutex);

#define SWITCH_NAME	"switch_cdev"


static int switch_open(struct inode *inode, struct file *fs)
{
#ifdef SWITCHDEBUG
	printk("switch: open...\n");
#endif
	try_module_get(THIS_MODULE);

	return 0;
}

static int switch_release(struct inode *inode, struct file *file)
{
	module_put(THIS_MODULE);
#ifdef SWITCHDEBUG
	printk("switch: release!\n");
#endif
	return 0;
}

static ssize_t switch_read(struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
	return 0;
}

static ssize_t switch_write(struct file *filp, const char __user *buff, size_t len, loff_t *off)
{
	return 0;
}

static int switch_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
	unsigned long len, rwcmd;
	unsigned char phyaddr,regaddr;
  unsigned short regdata;
	void *cptr;
	char *cdata;
	int ret=0x0;

#ifdef SWITCHDEBUG
	printk(KERN_ALERT "switch: +ioctl...\n");
#endif
	len = (int)(_IOC_SIZE(cmd));
	rwcmd = (int)(_IOC_DIR(cmd));
	cptr = (void *)arg;
  

		cdata = kmalloc(len, GFP_KERNEL);
		if (!cdata)
		{
			ret = -ENOMEM;
			goto out_switch_ioctl;
		}

		if (copy_from_user(cdata, cptr, len))
		{
			ret = -EFAULT;
			goto out_switch_ioctl;
		}
		phyaddr = *((unsigned char *)(cdata));
    regaddr = *((unsigned char *)(cdata+1));
		regdata = *((unsigned short *)(cdata+2));

#ifdef SWITCHDEBUG
		printk(KERN_ALERT "phyaddr=0x%02X  regaddr=0x%02X  regdata=0x%04X\n",
      (unsigned char)phyaddr, (unsigned char)regaddr, (unsigned short)regdata);
#endif
    if(regaddr>0xffff)
    {
      ret = -EINVAL;
      goto out_switch_ioctl;
    }
    switch(cmd)
    {
        case SWITCH_READ:
#ifdef SWITCHDEBUG
		printk(KERN_ALERT "SWITCH_READ phyaddr=0x%02X  regaddr=0x%02X  regdata=0x%04X\n",
      (unsigned char)phyaddr, (unsigned char)regaddr,(unsigned short)regdata);
#endif        
          *((unsigned short *)(cdata+2)) = switch_mdio_rd(phyaddr, regaddr);
          break;
        case SWITCH_WRITE:
#ifdef SWITCHDEBUG
    printk(KERN_ALERT "SWITCH_WRITE phyaddr=0x%02X  regaddr=0x%04X  regdata=0x%04X\n",
      (unsigned char)phyaddr, (unsigned char)regaddr,(unsigned short)regdata);  
#endif 
          switch_mdio_wr(phyaddr, regaddr, regdata);         
          break;
    }
		if (copy_to_user(cptr, cdata, len))
		{
			ret = -EFAULT;
			goto out_switch_ioctl;
		}
		cptr= (void *)*((unsigned long *)cdata);
		len = *((unsigned long *)(cdata+4));

		kfree(cdata);
		cdata = NULL;
		
out_switch_ioctl:
	if(cdata)
	{
		//memset(cdata, 0x0, len);
		kfree(cdata);
	}
#ifdef SWITCHDEBUG
	printk(KERN_ALERT "switch: -ioctl...\n");
#endif
	return (ret < 0) ? ret : 0;
}

static long switch_unlocked_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
	int ret;

	mutex_lock(&switch_mutex);
	ret = switch_ioctl(filep, cmd, arg);
	mutex_unlock(&switch_mutex);

	return ret;
}

static struct file_operations switch_fops = {
	.owner			= THIS_MODULE,
	.read			= switch_read, 
	.write			= switch_write,
	.unlocked_ioctl	= switch_unlocked_ioctl,
	.open			= switch_open,
	.release		= switch_release
};

static int __init switch_init(void)
{
	int result;
  
  unsigned char LutMode;
  int i, j, ret, idx;
  unsigned short gidx;
  struct LUT_entry lentry, getentry;

  //lut_set_hash_algorithm(LUT_HASH_DIRECT);
  //lut_set_hash_algorithm(LUT_HASH_CRC);
	result = register_chrdev_region(MKDEV(switch_major, 0), 1, SWITCH_NAME);
	if (result < 0)
	{
    printk(KERN_WARNING "switch: can't get major %d\n", switch_major);
    return result;
	}

	cdev_init(&switch_cdev, &switch_fops);
	switch_cdev.owner = THIS_MODULE;
	result = cdev_add(&switch_cdev, MKDEV(switch_major, 0), 1);
	if (result)
	{
		printk(KERN_WARNING "switch: error %d adding driver\n", result);
		return result;
	}
	else
	{
		printk("switch: driver loaded!\n");
    
  LUT_set_mode(LUT_MODE_1K1K);
  LutMode = LUT_get_mode();
  printk("LUT Mode=%d\n\n", LutMode);

  for (i=0; i < 16; i++)
  {  
    //UCST
    lentry.valid = 0x1;
    lentry.type = LUT_MAC_UNICAST;
    lentry.entry = (u8)(i%4);
    lentry.fid = (u8)i;
    lentry.MAC[0] = 0x08;
    lentry.MAC[1] = 0x99;
    lentry.MAC[2] = 0x32;
    lentry.MAC[3] = 0x1F;
    lentry.MAC[4] = 0x5B;
    lentry.MAC[5] = (0xA7+i*5);
    lentry.aged = 0x0;
    lentry.port_map = (u16)(0x1 << (i%9));  
    ret = LUT_set_entry(lentry);
  }

  i = idx = 0;
  printk("Valid entry:\n");
  do {
    idx = LUT_get_next_valid_entry(i, &lentry);
    if (idx != C_FAIL)
    {
      printk("idx=0x%03X  MAC=", idx);
      for (j=0; j < 6; j++)
        printk("%02X:", lentry.MAC[j]);
      printk("  FID=0x%X\n", lentry.fid);
    }
    i = idx + 1;
  } while (idx != C_FAIL);
  printk("\n");
  
  LUT_flush_entry(0xE, lentry.MAC);
  printk("Flush FID=0xE!\n\n");

  lentry.MAC[5] = (0xA7 + 0xE*5);
  gidx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, 0xE, lentry.MAC, (0xE%4));
  printk("idx=0x%03X:\n", gidx);
  
  ret = LUT_get_entry(gidx, &getentry);
  if (idx == C_FAIL)
    printk("LUT_get_entry failed... The entry is invalid!\n");
  else
  {
    printk("MAC=");
    for (i=0; i < 6; i++)
      printk("%02X:", getentry.MAC[i]);
    printk("\nFID=0x%X\n", getentry.fid);
    printk("port_map=0x%03X\n", getentry.port_map);
  }

 		return 0;
	}
}

static void __exit switch_exit(void)
{
	cdev_del(&switch_cdev);
	unregister_chrdev_region(MKDEV(switch_major, 0), 1);
	printk("switch: driver unloaded!\n");
}  

module_init(switch_init);
module_exit(switch_exit);
