/******************************************************************************
 *
 *   Name:           drvlib.c
 *
 *   Description:
 *
 *   Copyright:      (c) 2009-2050    IC Plus Corp.
 *                   All rights reserved.
 *
 *****************************************************************************/

#include "drvlib.h"
#include "drvbody.h"
#ifndef USER_API
#include <linux/module.h>
#endif

#ifdef USER_API
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <linux/types.h>
#else

#endif

//=============================================================================

const char *dev_switch="/dev/dev_switch";
#define read_reg switch_read_reg
#define write_reg switch_write_reg


#ifdef USER_API                           
unsigned short switch_read_reg(unsigned char phyad, unsigned char regad)
{
  int fd,retval;
  struct GeneralSetting gs;
  memset(&gs,0x0,sizeof(struct GeneralSetting));
  gs.phy=phyad;
  gs.reg=regad;
  
  if((fd=open(dev_switch,O_RDWR))<0)
  {
    printf("Err: open %s failed!\n", dev_switch);   
    perror("open: ");     
    return 0;
  }
  if(ioctl(fd,SWITCH_READ,&gs)<0)
    printf("Err: ioctl %s failed!\n", dev_switch);
#ifdef USER_API_DEBUG
printf("lib:switch_read_reg addr=%02x.%02x data=%04x\n",gs.phy,gs.reg,gs.data);
#endif   
	retval = close(fd);
	if (retval < 0)
		printf("Err: close %s failed!\n", fd);          
  return gs.data;
}
void switch_write_reg(unsigned char phyad, unsigned char regad, unsigned short Regdata)
{
  int fd,retval;
  struct GeneralSetting gs;
#ifdef USER_API_DEBUG
printf("lib:switch_write_reg addr=%02x.%02x data=%04x\n",phyad,regad,Regdata);
#endif
  memset(&gs,0x0,sizeof(struct GeneralSetting));
  gs.phy=phyad;
  gs.reg=regad;
  gs.data=Regdata;
  if((fd=open(dev_switch,O_RDWR))<0)
  {
    printf("Err: open %s failed!\n", dev_switch); 
    perror("open: ");     
    return ;
  }       
  if(ioctl(fd,SWITCH_WRITE,&gs)<0)
    printf("Err: ioctl %s failed!\n", dev_switch);
	retval = close(fd);
	if (retval < 0)
		printf("Err: close %s failed!\n", fd);            
}
 
#else   
unsigned short switch_read_reg(unsigned char phyad, unsigned char regad)
{
  return switch_mdio_rd(phyad, regad);
}
void switch_write_reg(unsigned char phyad, unsigned char regad, unsigned short Regdata)
{
  switch_mdio_wr(phyad, regad, Regdata);  
}
#endif

//=============================================================================
// LUT ====================================================

/**********************************************************
 *  Function: LUT_set_mode
 *
 *  Description:
 *    Set LUT structure to support 2K unicast or
 *    1K unicast/1K multicast.
 *  
 *  Parameters:
 *    mode  �V LUT_MODE_2K / LUT_MODE_1K1K 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_set_mode(unsigned char mode)
{
  unsigned short tmp;
  
  tmp = read_reg(PHY20, LEARN_FORWARD_ENABLE);
  tmp &= ~0x4;
  tmp |= (unsigned short)(mode<<2);
  write_reg(PHY20, LEARN_FORWARD_ENABLE, tmp);
}
/**********************************************************
 *  Function: LUT_get_mode
 *
 *  Description:
 *    Get LUT structure to support whether 2K unicast or
 *    1K unicast/1K multicast.
 *  
 *  Parameters:
 *    None 
 *
 *  Return:
 *    0   �V LUT_MODE_2K
 *    1   �V LUT_MODE_1K1K
 *    
 *********************************************************/
unsigned char LUT_get_mode(void)
{
  return (unsigned char)((read_reg(PHY20, LEARN_FORWARD_ENABLE)>>2) & 0x1);
}
/**********************************************************
 *  Function: LUT_get_2k_hash_index
 *
 *  Description:
 *    Calculate a hash index of 2K unicast table.
 *  
 *  Parameters:
 *    fid         �V FID
 *    mac         - MAC address
 *    entry       - entry no. of the same bucket
 *
 *  Return:
 *    0 ~ 0xFFF   �V a 11-bit hash index
 *    
 *********************************************************/
unsigned short LUT_get_2k_hash_index(unsigned char fid, unsigned char *mac, unsigned char entry)
{
      unsigned short tmp;
      tmp = ((unsigned short)fid<<3|mac[0]>>5)^
                      ((mac[0]&0x1f)<<4|mac[1]>>4)^
                      ((mac[1]&0x0f)<<5|mac[2]>>3)^
                      ((mac[2]&0x07)<<6|mac[3]>>2)^
                      ((mac[3]&0x03)<<7|mac[4]>>1)^
                      ((mac[4]&0x01)<<8|mac[5]);
      tmp = tmp << 2 | ((unsigned short)entry&0x3);
      return tmp;
}
/**********************************************************
 *  Function: LUT_get_1k1k_hash_index
 *
 *  Description:
 *    Calculate a hash index of 1K unicast table and
 *    1K multicast table.
 *  
 *  Parameters:
 *    type        - LUT_MAC_UNICAST / LUT_MAC_IGMP
 *    fid         �V FID
 *    mac         - MAC address
 *    entry       - entry no. of the same bucket
 *
 *  Return:
 *    0 ~ 0xFFF   �V a 11-bit hash index
 *    
 *********************************************************/
unsigned short LUT_get_1k1k_hash_index(unsigned char type, unsigned char fid, unsigned char *mac, unsigned char entry)
{
	unsigned short tmp;
	tmp =	fid^mac[0]^mac[1]^mac[2]^mac[3]^mac[4]^mac[5];//0x5F = 0x5E^0x00^0x01
	tmp =  (tmp<<2) | (entry&0x3);
	if (type==LUT_MAC_UNICAST){
		tmp |= (0x0<<10);
	}
	else{
		tmp |= (0x1<<10);
	}
	return tmp;
}

void LUT_fill_in_entry(unsigned short idx, unsigned short *ldata, struct LUT_entry *lutset)
{
  int i;
  unsigned char LutMode;
  
  //valid
  lutset->valid = (unsigned char)((ldata[4]>>15) & 0x1);

  //type  
  LutMode = LUT_get_mode();
  if (LutMode==LUT_MODE_1K1K && idx>=(MAX_LUT_ENTRY/2))
    lutset->type = LUT_MAC_IGMP;
  else
    lutset->type = LUT_MAC_UNICAST;
    
  //entry & fid  
  lutset->entry = (unsigned char)(idx & 0x3);
  lutset->fid = (unsigned char)((ldata[3]>>12) & 0xF);
  
  //port_map        
  lutset->port_map = (ldata[3] & 0x01FF);

  //MAC & aged
  if (lutset->type == LUT_MAC_UNICAST)
  {
    for (i=0; i < 6; i+=2)
    {
      lutset->MAC[i] = (unsigned char)(ldata[2-i/2] >> 8);
      lutset->MAC[i+1] = (unsigned char)(ldata[2-i/2] & 0xFF);
    }  
    lutset->aged = (ldata[4] & 0x1);
  }
  else
  {
    for (i=0; i < 3; i++)
      lutset->MAC[i] = 0x00;
//    lutset->MAC[0] = 0x01;
//    lutset->MAC[1] = 0x00;
//    lutset->MAC[2] = 0x5E;
    lutset->MAC[3] = (unsigned char)(ldata[1] & 0x7F);
    lutset->MAC[4] = (unsigned char)(ldata[0] >> 8);
    lutset->MAC[5] = (unsigned char)(ldata[0] & 0xFF);    
    lutset->aged = (ldata[4] & 0x1FF);
  }
}
/**********************************************************
 *  Function: LUT_get_next_valid_entry
 *
 *  Description:
 *    Get next valid entry from input index.
 *  
 *  Parameters:
 *    idx         - scan LUT from the entry of this index
 *    lutset      �V a pointer to a buffer that will hold
 *                  the data of next valid entry
 *
 *  Return:
 *    -1          - fail
 *    0 ~ 0x7FF   �V the index of next valid entry
 *    
 *********************************************************/
int LUT_get_next_valid_entry(int idx, struct LUT_entry *lutset)
{
	int i, j;
  unsigned char LutMode;
	unsigned short Reg2108=0;
	unsigned short LUT_data[5];

	if (idx < 0 || idx >= MAX_LUT_ENTRY)	return C_FAIL;

  LutMode = LUT_get_mode();
  
  for (i=idx; i < MAX_LUT_ENTRY; i++)
  {
    Reg2108 = (i | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
      Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
    
    for (j=0; j < 5; j++)
			LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);

    if (LutMode==LUT_MODE_1K1K && i>=(MAX_LUT_ENTRY/2))   //IGMP
    {
      if (!(LUT_data[4] & 0x8000))
        continue;
    }
    else  //UCST
    {
      if (!(LUT_data[4] & 0x8000) && (LUT_data[4] & 0x1))   //dynamic and aged
        continue;
    }
    
    LUT_fill_in_entry((unsigned short)i, LUT_data, lutset);
    return i;
  }
  
  return C_FAIL;
}
/**********************************************************
 *  Function: LUT_set_entry
 *
 *  Description:
 *    Set a LUT entry.
 *  
 *  Parameters:
 *    lutset  �V the data of the entry to be set
 *
 *  Return:
 *    -1      - fail
 *    0       �V success
 *    
 *********************************************************/
int LUT_set_entry(struct LUT_entry lutset)
{
  int i;
  unsigned char LutMode;
  unsigned short rdata=0;
  
  LutMode = LUT_get_mode();
  if (lutset.type==LUT_MAC_IGMP && LutMode==LUT_MODE_2K)
    return -1;

  //LUT_DATA0 ~ LUT_DATA4  
  if (lutset.type == LUT_MAC_UNICAST)
  {
    //MAC
    for (i=0; i < 3; i++)
    {
      rdata = ((unsigned short)lutset.MAC[(2-i)*2]) << 8;
      rdata |= (unsigned short)lutset.MAC[(2-i)*2+1];
      write_reg(PHY21, LUT_DATA0+i, rdata);
    }
    
    //fid & port_map
    rdata = ((unsigned short)(lutset.fid & 0xF) << 12) | (lutset.port_map & 0x1FF);
    write_reg(PHY21, LUT_DATA3, rdata);
    
    //valid & aged
    rdata = ((unsigned short)(lutset.valid & 0x1) << 15) | (lutset.aged & 0x1);
    write_reg(PHY21, LUT_DATA4, rdata);
  }
  else if (lutset.type == LUT_MAC_IGMP)
  {
    //MAC
    rdata = ((unsigned short)lutset.MAC[4] << 8) | ((unsigned short)lutset.MAC[5]);
    write_reg(PHY21, LUT_DATA0, rdata);
    rdata = (unsigned short)(lutset.MAC[3] & 0x7F);
    write_reg(PHY21, LUT_DATA1, rdata);
    
    //fid & port_map
    rdata = ((unsigned short)(lutset.fid & 0xF) << 12) | (lutset.port_map & 0x1FF);
    write_reg(PHY21, LUT_DATA3, rdata);
    
    //valid & aged
    rdata = ((unsigned short)(lutset.valid & 0x1) << 15) | (lutset.aged & 0x1FF);
    rdata |= 0x4000;
    write_reg(PHY21, LUT_DATA4, rdata);
  }
  else
    return C_FAIL;
    
  //LUT_COMMAND
  if (LutMode == LUT_MODE_2K)
    rdata = LUT_get_2k_hash_index(lutset.fid, lutset.MAC, lutset.entry);
  else
    rdata = LUT_get_1k1k_hash_index(lutset.type, lutset.fid, lutset.MAC, lutset.entry);
    
  rdata |= 0xC000;
  write_reg(PHY21, LUT_COMMAND, rdata);
  do {
    rdata = read_reg(PHY21, LUT_COMMAND);
  } while (rdata & 0x8000);
  
  return C_SUCCESS;
}
/**********************************************************
 *  Function: LUT_get_entry
 *
 *  Description:
 *    Get the LUT entry of input index.
 *  
 *  Parameters:
 *    idx     - the index of which the LUT entry is queried
 *    lutset  �V a pointer to a buffer that will hold the
 *              data of the entry associated with the input
 *              index
 *
 *  Return:
 *    -1      - fail
 *    0       �V success
 *    
 *********************************************************/
int LUT_get_entry(unsigned short idx, struct LUT_entry *lutset)
{
  int i;
	unsigned short Reg2108=0;
  unsigned short LUT_data[5];

	Reg2108 = (idx | 0x8000);
  write_reg(PHY21, LUT_COMMAND, Reg2108);
  do {
      Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);

	for (i=0; i < 5; i++)
			LUT_data[i] = read_reg(PHY21, LUT_DATA0+i);

  LUT_fill_in_entry(idx, LUT_data, lutset);

  //if the entry is invalid, return C_FAIL  
  if (lutset->type == LUT_MAC_UNICAST)
  {    
    if (lutset->valid==0 && lutset->aged)
      return C_FAIL;
    else
      return C_SUCCESS;
  }
  else
  {
    if (lutset->valid == 0)
      return C_FAIL;
    else
      return C_SUCCESS;
  }
}
/**********************************************************
 *  Function: LUT_get_port_by_mac   (UCST table only)
 *
 *  Description:
 *    Get destination ports of input MAC address.
 *  
 *  Parameters:
 *    fid     - FID
 *    mac     - input MAC address 
 *
 *  Return:
 *    0       - can't find a match entry that is valid
 *    1 ~ 9   - port to which a packet with a DMAC equals
 *              to the input MAC will be sent
 *    0xF000  - input MAC address is not a unicast address
 *    
 *********************************************************/
unsigned short LUT_get_port_by_mac(unsigned char fid, unsigned char *mac)
{
  unsigned char i, j, LutMode;
  unsigned short idx, Reg2108=0, pmap=0, port=0;
  unsigned short LUT_data[5];

  //UCST only  
  if (*mac & 0x1)
    return 0xF000;

  LutMode = LUT_get_mode();
  if (LutMode == LUT_MODE_2K)
  { idx = LUT_get_2k_hash_index(fid, mac, 0); }
  else
  { idx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, fid, mac, 0); }
    
  for (i=0; i < 4; i++)
  {
    Reg2108 = ((idx+i) | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
        
    for (j=0; j < 5; j++)
      LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);

    //if this entry is invalid         
    if (!(LUT_data[4] & 0x8000) && (LUT_data[4] & 0x1))   //dynamic and aged
        continue;
      
    //check if this MAC equals to the input MAC
    if ((LUT_data[0] & 0xFF)==*(mac+5) && (LUT_data[0] >> 8)==*(mac+4) &&
        (LUT_data[1] & 0xFF)==*(mac+3) && (LUT_data[1] >> 8)==*(mac+2) &&
        (LUT_data[2] & 0xFF)==*(mac+1) && (LUT_data[2] >> 8)==*(mac+0))
    {      
      pmap = (LUT_data[3] & 0x1FF);
        
      for (j=0; j < 9; j++)
      {
        if (pmap & ((unsigned short)0x1 << j))
        {
          port = (unsigned short)(j+1);
          break;
        }
      }
      break;
    }
  }
  
  return port;
}
/**********************************************************
 *  Function: LUT_flush_table
 *
 *  Description:
 *    Clear all entries of LUT.
 *  
 *  Parameters:
 *    None 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_table(void)
{
  unsigned short i, Reg2108=0;

  for (i=0; i < 5; i++)
  {
    if (i==4)
      write_reg(PHY21, LUT_DATA0+i, (unsigned short)0x1);
    else
      write_reg(PHY21, LUT_DATA0+i, (unsigned short)0x0);
  }

  for (i=0; i < MAX_LUT_ENTRY; i++)
  {    
    Reg2108 = (i | 0xC000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
  }
}
/**********************************************************
 *  Function: LUT_flush_port    (UCST table only)
 *
 *  Description:
 *    Clear LUT entries associated with the input port.
 *  
 *  Parameters:
 *    port �V all LUT entries associated with this port is 
 *           going to be cleared out 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_port(int port)
{
  unsigned char LutMode;
  unsigned short i, j, MaxEntry, Reg2108=0, pmap;
  unsigned short LUT_data[5];

  LutMode = LUT_get_mode();
  if (LutMode == LUT_MODE_2K)
  { MaxEntry = (unsigned short)MAX_LUT_ENTRY; }
  else
  { MaxEntry = (unsigned short)(MAX_LUT_ENTRY/2); }
  
  for (i=0; i < MaxEntry; i++)
  {    
    Reg2108 = (i | 0x8000);
    write_reg(PHY21, LUT_COMMAND, Reg2108);
    do {
       Reg2108 = read_reg(PHY21, LUT_COMMAND);
    } while (Reg2108 & 0x8000);
    
    for (j=0; j < 5; j++)
      LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);
    
    pmap = (LUT_data[3] & 0x1FF);  
    if ((pmap >> (port-1)) & 0x1)
    {
      for (j=0; j < 5; j++)
      {
        if (j==4)
          write_reg(PHY21, LUT_DATA0+j, (unsigned short)0x1);
        else
          write_reg(PHY21, LUT_DATA0+j, (unsigned short)0x0);
      }
        
      Reg2108 = (i | 0xC000);
      write_reg(PHY21, LUT_COMMAND, Reg2108);
      do {
         Reg2108 = read_reg(PHY21, LUT_COMMAND);
      } while (Reg2108 & 0x8000);
    }
  }
}
/**********************************************************
 *  Function: LUT_flush_entry     (UCST table only)
 *
 *  Description:
 *    Clear the LUT entry of specified FID and MAC.
 *  
 *  Parameters:
 *    fid     - FID
 *    mac     - input MAC address 
 *
 *  Return:
 *    None
 *    
 *********************************************************/
void LUT_flush_entry(unsigned char fid, unsigned char *mac)
{
  unsigned char i, j, LutMode;
  unsigned short idx, Reg2108=0;
  unsigned short LUT_data[5];

  //UCST only
  if (*(mac+0)!=0x01 && *(mac+1)!=0x00 && *(mac+2)!=0x5E)
  {
    LutMode = LUT_get_mode();
    if (LutMode == LUT_MODE_2K)
    { idx = LUT_get_2k_hash_index(fid, mac, 0); }
    else
    { idx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, fid, mac, 0);  }
    
    for (i=0; i < 4; i++)
    {
      Reg2108 = ((idx+i) | 0x8000);
      write_reg(PHY21, LUT_COMMAND, Reg2108);
      do {
         Reg2108 = read_reg(PHY21, LUT_COMMAND);
      } while (Reg2108 & 0x8000);
      
      for (j=0; j < 5; j++)
        LUT_data[j] = read_reg(PHY21, LUT_DATA0+j);
  
      //check if this MAC equals to the input MAC
      if ((LUT_data[0] & 0xFF)==*(mac+5) && (LUT_data[0] >> 8)==*(mac+4) &&
          (LUT_data[1] & 0xFF)==*(mac+3) && (LUT_data[1] >> 8)==*(mac+2) &&
          (LUT_data[2] & 0xFF)==*(mac+1) && (LUT_data[2] >> 8)==*(mac+0))
      {
          write_reg(PHY21, LUT_DATA0, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA1, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA2, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA3, (unsigned short)0x0);
          write_reg(PHY21, LUT_DATA4, (unsigned short)0x1);
     
          Reg2108 = ((idx+i) | 0xC000);
          write_reg(PHY21, LUT_COMMAND, Reg2108);
          do {
             Reg2108 = read_reg(PHY21, LUT_COMMAND);
          } while (Reg2108 & 0x8000);
            
          break;
      }
    }
  }
}

// VLAN ===================================================

/**********************************************************
 *  Function: VLAN_set_mode
 *
 *  Description:
 *    Set VLAN mode.
 *  
 *  Parameters:
 *    port    - port
 *    mode    - VLAN_MODE_PORT_BASED (0) or
 *              VLAN_MODE_TAG_BASED (1) 
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_set_mode(unsigned char port, unsigned char mode)
{
  unsigned short VLANMode=0;
  
  if (port>MAX_PORT_NUM-2 || mode>VLAN_MODE_TAG_BASED)
    return C_FAIL;
  
  VLANMode = read_reg(PHY22, VLAN_MODE);
  VLANMode &= ~((unsigned short)0x1 << port);
  VLANMode |= ((unsigned short)mode << port);
  write_reg(PHY22, VLAN_MODE, VLANMode);
  
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_mode
 *
 *  Description:
 *    Get VLAN mode.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - VLAN_MODE_PORT_BASED
 *    1       - VLAN_MODE_TAG_BASED
 *    
 *********************************************************/
int VLAN_get_mode(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  else
    return (unsigned char)((read_reg(PHY22, VLAN_MODE) >> port) & 0x1);
}
/**********************************************************
 *  Function: VLAN_PB_set_member
 *
 *  Description:
 *    Set member ports of a port which is port-based VLAN.
 *  
 *  Parameters:
 *    port    - port number
 *    member  - member ports which the ingress port can
 *              transmits packets to
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_member(unsigned char port, unsigned short member)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;

  write_reg(PHY22, PB_VLAN_MEMBER_SET_0+port, (member & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_member
 *
 *  Description:
 *    Get member ports of a port which is port-based VLAN.
 *  
 *  Parameters:
 *    port        - port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - member ports of the input port
 *    
 *********************************************************/
int VLAN_PB_get_member(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  
  return (int)(read_reg(PHY22, PB_VLAN_MEMBER_SET_0+port)); 
}
/**********************************************************
 *  Function: VLAN_PB_set_PVID
 *
 *  Description:
 *    Set PVID of a port.
 *  
 *  Parameters:
 *    port    - port
 *    pvid    - PVID
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_PVID(unsigned char port, unsigned short pvid)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
    
  write_reg(PHY22, VLAN_PVID0+port, pvid);
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_PVID
 *
 *  Description:
 *    Get PVID of a port.
 *  
 *  Parameters:
 *    port        - port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0xFFFF  - PVID of the input port
 *    
 *********************************************************/
int VLAN_PB_get_PVID(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  
  return (int)(read_reg(PHY22, VLAN_PVID0+port));
}
/**********************************************************
 *  Function: VLAN_PB_set_add_tag
 *
 *  Description:
 *    Set ports to be added VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port    - the ingress port
 *    addtag  - ports to be added VLAN tag
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_add_tag(unsigned char port, unsigned short addtag)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
    
  write_reg(PHY23, PB_VLAN_ADD_TAG_MASK_0+port, (addtag & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_add_tag
 *
 *  Description:
 *    Get ports to be added VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port        - the ingress port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - ports to be added VLAN tag
 *    
 *********************************************************/
int VLAN_PB_get_add_tag(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  
  return (int)(read_reg(PHY23, PB_VLAN_ADD_TAG_MASK_0+port)); 
}
/**********************************************************
 *  Function: VLAN_PB_set_remove_tag
 *
 *  Description:
 *    Set ports to be removed VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port    - the ingress port
 *    rmvtag  - ports to be removed VLAN tag
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_PB_set_remove_tag(unsigned char port, unsigned short rmvtag)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
    
  write_reg(PHY23, PB_VLAN_RMV_TAG_MASK_0+port, (rmvtag & 0x1FF));
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_PB_get_remove_tag
 *
 *  Description:
 *    Get ports to be removed VLAN tag when the ingress
 *    port is port-based VLAN.
 *  
 *  Parameters:
 *    port        - the ingress port
 *
 *  Return:
 *    -1          - failed
 *    0 ~ 0x1FF   - ports to be removed VLAN tag
 *    
 *********************************************************/
int VLAN_PB_get_remove_tag(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  
  return (int)(read_reg(PHY23, PB_VLAN_RMV_TAG_MASK_0+port)); 
}
/**********************************************************
 *  Function: VLAN_TB_set_entry
 *
 *  Description:
 *    Set a VLAN entry.
 *  
 *  Parameters:
 *    idx        - the index of the entry to be set
 *    vlanset    - the data of the entry to be set
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_TB_set_entry(unsigned char idx, struct VLAN_entry *vlanset)
{
  unsigned short u16dat, tmp;
  
  if (idx>VLAN_MAX_ENTRY-1)
    return C_FAIL;
    
  if (vlanset->VID >= 4096 || vlanset->FID >= 0x10)
    return C_FAIL;

  //VID & FID
  u16dat = ((unsigned short)(vlanset->FID & 0xF) << 12) | (vlanset->VID & 0xFFF);
  write_reg(PHY24, TB_VLAN_FID_VID_0+idx, u16dat);
  
  //member, add tag, and remove tag
  write_reg(PHY25, TB_VLAN_MEMBER_0+idx, (vlanset->member & 0x1FF));
  write_reg(PHY24, TB_VLAN_ADD_TAG_0+idx, (vlanset->AddTag & 0x1FF));
  write_reg(PHY25, TB_VLAN_REMOVE_TAG_0+idx, (vlanset->RmvTag & 0x1FF));

  //valid
  u16dat = read_reg(PHY22, VLAN_VALID_ENTRY);
  u16dat &= ~((unsigned short)0x1 << idx);
  tmp = (unsigned short)((vlanset->valid) ? 0x1 : 0x0);
  u16dat |= (tmp << idx);
  write_reg(PHY22, VLAN_VALID_ENTRY, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_TB_get_entry
 *
 *  Description:
 *    Get a VLAN entry.
 *  
 *  Parameters:
 *    idx        - the index of the entry to be get
 *    vlanset    - a pointer to a buffer that will hold the
 *                 data of the entry associated with the
 *                 input index
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_TB_get_entry(unsigned char idx, struct VLAN_entry *vlanset)
{
  unsigned short u16dat;
  
  if (idx>VLAN_MAX_ENTRY-1)
    return C_FAIL;
    
  vlanset->valid = (unsigned char)((read_reg(PHY22, VLAN_VALID_ENTRY) >> idx) & 0x1);
  
  u16dat = read_reg(PHY24, TB_VLAN_FID_VID_0+idx);
  vlanset->VID = (u16dat & 0xFFF);
  vlanset->FID = (unsigned char)((u16dat >> 12) & 0xF);
  
  vlanset->member = read_reg(PHY25, TB_VLAN_MEMBER_0+idx);
  vlanset->AddTag = read_reg(PHY24, TB_VLAN_ADD_TAG_0+idx);
  vlanset->RmvTag = read_reg(PHY25, TB_VLAN_REMOVE_TAG_0+idx);

  return C_SUCCESS;  
}
/**********************************************************
 *  Function: VLAN_set_ingress_type
 *
 *  Description:
 *    Set acceptable VLAN frame type in tag-based VLAN.
 *  
 *  Parameters:
 *    intype      - VLAN_ALL_FRAME / VLAN_TAGGED_FRAME /
 *                  VLAN_UNTAGGED_FRAME
 *    
 *  Return:
 *    0           - success
 *    -1          - failed
 *    
 *********************************************************/
int VLAN_set_ingress_type(unsigned char intype)
{
  unsigned short u16dat;
  
  if (intype > VLAN_UNTAGGED_FRAME)
    return C_FAIL;
    
  u16dat = read_reg(PHY22, VLAN_INGRESS_RULE);
  u16dat &= (unsigned short)0xCFFF;
  u16dat |= (unsigned short)intype << 12;
  write_reg(PHY22, VLAN_INGRESS_RULE, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_ingress_type
 *
 *  Description:
 *    Get acceptable VLAN frame type in tag-based VLAN.
 *  
 *  Parameters:
 *    None
 *    
 *  Return:
 *    0   - VLAN_ALL_FRAME
 *    1   - VLAN_TAGGED_FRAME
 *    2   - VLAN_UNTAGGED_FRAME
 *    
 *********************************************************/
unsigned char VLAN_get_ingress_type(void)
{
  return (unsigned char)((read_reg(PHY22, VLAN_INGRESS_RULE) >> 12) & 0x3);
}
/**********************************************************
 *  Function: VLAN_set_ingress_filter
 *
 *  Description:
 *    In tag-based VALN, if a port corresponding bit is
 *    set, frame shall discard on that port if the member
 *    set of the VLAN entry does not include that port.
 *  
 *  Parameters:
 *    port  - port number of which a port is enable or
 *            disable ingress filter
 *    en    - 1: enable, 0: disable
 *    
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int VLAN_set_ingress_filter(unsigned char port, unsigned char en)
{
  unsigned short u16dat;
  
  if (port>MAX_PORT_NUM-2 || en>ENABLE)
    return C_FAIL;
  
  u16dat = read_reg(PHY22, VLAN_INGRESS_RULE);
  u16dat &= ~((unsigned short)0x1 << port);
  u16dat |= ((unsigned short)en << port);
  write_reg(PHY22, VLAN_INGRESS_RULE, u16dat);
    
  return C_SUCCESS;  
}
/**********************************************************
 *  Function: VLAN_get_ingress_filter
 *
 *  Description:
 *    Get the status of the VLAN ingress filter of an input
 *    port.
 *  
 *  Parameters:
 *    port    - port number
 *    
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int VLAN_get_ingress_filter(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
    
  return (int)((read_reg(PHY22, VLAN_INGRESS_RULE) >> port) & 0x1);
}
/**********************************************************
 *  Function: VLAN_set_unknown_vid_mode
 *
 *  Description:
 *    Set the way to process a frame of which VID is not
 *    in any entry of VLAN table in tag-based VLAN.
 *  
 *  Parameters:
 *    mode      - VLAN_UNVID_DISCARD / VLAN_UNVID_TOCPU /
 *                VLAN_UNVID_FLOOD
 *    
 *  Return:
 *    0         - success
 *    -1        - failed
 *    
 *********************************************************/
int VLAN_set_unknown_vid_mode(unsigned char mode)
{
  unsigned short u16dat;
  
  if (mode > VLAN_UNVID_FLOOD)
    return C_FAIL;
  
  u16dat = read_reg(PHY22, VLAN_MODE);
  u16dat &= (unsigned short)0xCFFF;
  u16dat |= (unsigned short)mode << 12;
  write_reg(PHY22, VLAN_MODE, u16dat);
    
  return C_SUCCESS;
}
/**********************************************************
 *  Function: VLAN_get_unknown_vid_mode
 *
 *  Description:
 *    Get the way to process a frame of which VID is not
 *    in any entry of VLAN table.
 *  
 *  Parameters:
 *    None
 *    
 *  Return:
 *    0   - VLAN_UNVID_DISCARD
 *    1   - VLAN_UNVID_TOCPU
 *    2   - VLAN_UNVID_FLOOD
 *    
 *********************************************************/
unsigned char VLAN_get_unknown_vid_mode(void)
{
  return (unsigned char)((read_reg(PHY22, VLAN_MODE) >> 12) & 0x3);
}
/**********************************************************
 *  Function: port_set_enable
 *
 *  Description:
 *    Set port enable/disable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int port_set_enable(unsigned char port,unsigned char enabled)
{
	unsigned short portmode=0;
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	portmode = read_reg(PHY_PORT0+port, CONTROL_REG);
	portmode &= ~((unsigned short)0x1 << 11);
	portmode |= ((unsigned short)enabled << 11);
	write_reg(PHY_PORT0+port, CONTROL_REG, portmode);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: port_get_enable
 *
 *  Description:
 *    Get port enable/disable.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int port_get_enable(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY_PORT0+port, CONTROL_REG) >> 11) & 0x1);
}
/**********************************************************
 *  Function: port_set_flow_control
 *
 *  Description:
 *    Set port flow control enable/disable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int port_set_flow_control(unsigned char port,unsigned char enabled)
{
	unsigned short portmode=0;
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	portmode = read_reg(PHY_PORT0+port, AN_REG);
	portmode &= ~((unsigned short)0x1 << 11);
	portmode &= ~((unsigned short)0x1 << 10);
	portmode |= ((unsigned short)enabled << 11);
	portmode |= ((unsigned short)enabled << 10);
	write_reg(PHY_PORT0+port, AN_REG, portmode);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: port_get_flow_control
 *
 *  Description:
 *    Get port flow control enable/disable.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int port_get_flow_control(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY_PORT0+port, AN_REG) >> 10) & 0x1);
}
/**********************************************************
 *  Function: port_set_extend_mode
 *
 *  Description:
 *    Set port extend mode enable/disable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int port_set_extend_mode(unsigned char port,unsigned char enabled)
{
	unsigned short portmode=0;
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
		
	if(enabled)
		write_reg(PHY_PORT0+port, AN_REG, 0x0460);
	else
		write_reg(PHY_PORT0+port, AN_REG, 0x0DE1);

	write_reg(PHY_PORT0+port, CONTROL_REG, 0x0330);
	
	return C_SUCCESS;
}
/**********************************************************
 *  Function: set_broadcast_storm_threshold
 *
 *  Description:
 *    Set broadcast storm threshold
 *  
 *  Parameters:
 *    value       - threshold (0~2047)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int set_broadcast_storm_threshold(int value)
{
	int reg_val;
	int tmp =  0x7FF;

	if (value < 0 || value > 2047)
		return C_FAIL;
		
	reg_val = read_reg(PHY21, BROADCAST_STORM_PROTECTION);
	reg_val &= ~tmp;
	reg_val |= value;
	write_reg(PHY21, BROADCAST_STORM_PROTECTION, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: get_broadcast_storm_threshold
 *
 *  Description:
 *    Get broadcast storm threshold
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    value       - threshold
 *    
 *********************************************************/
int get_broadcast_storm_threshold(void)
{
	int reg_val;
	reg_val = read_reg(PHY21, BROADCAST_STORM_PROTECTION);
	return (int)reg_val & 0x7FF;
}
/**********************************************************
 *  Function: port_get_speed
 *
 *  Description:
 *    Get port speed.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    0     - 10 Mbps
 *    1     - 100 Mbps
 *    -1    - failed
 *    
 *********************************************************/
int port_get_speed(unsigned char port)
{
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY_PORT0+port, SPECIAL_STATUS_REG) >> 11) & 0x1);
}
/**********************************************************
 *  Function: port_get_status
 *
 *  Description:
 *    Get port status.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    0     - unlink
 *    1     - linkup
 *    -1    - failed
 *    
 *********************************************************/
int	port_get_status(unsigned char port)
{
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY_PORT0+port, SPECIAL_STATUS_REG) >> 14) & 0x1);
}
/**********************************************************
 *  Function: get_ipc_entry
 *
 *  Description:
 *    Get IPC entry.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    IPC_entry
 *    
 *********************************************************/
struct _ipc_ip_mac_entry get_ipc_entry(unsigned char port)
{
	struct _ipc_ip_mac_entry ipc_display;
	int val,num=0;
	write_reg(PHY21, ADDR_TABLE_COMMAND_REG, (0x2000 | port));
	while(1)
	{
		num++;
		do {
			val = read_reg(PHY21, ADDR_TABLE_COMMAND_REG);
		} while (val & 0x8000);
		
		if (((val >> 11) & 0x3) == 0x3)
			break;
		if (num > 50){
			ipc_display.mac_addr[5] = 0;
			ipc_display.mac_addr[4] = 0;
			ipc_display.mac_addr[3] = 0;
			ipc_display.mac_addr[2] = 0;
			ipc_display.mac_addr[1] = 0;
			ipc_display.mac_addr[0] = 0;
			ipc_display.ip_addr[3] = 0;
			ipc_display.ip_addr[2] = 0;
			ipc_display.ip_addr[1] = 0;
			ipc_display.ip_addr[0] = 0;
			return ipc_display;
		}
	}
	
	val = read_reg(PHY21, ADDR_TABLE_MAC_0);
	ipc_display.mac_addr[5] = (val & 0xFF);
	ipc_display.mac_addr[4] = ((val >> 8) & 0xFF);
	val = read_reg(PHY21, ADDR_TABLE_MAC_1);
	ipc_display.mac_addr[3] = (val & 0xFF);
	ipc_display.mac_addr[2] = ((val >> 8) & 0xFF);
	val = read_reg(PHY21, ADDR_TABLE_MAC_2);
	ipc_display.mac_addr[1] = (val & 0xFF);
	ipc_display.mac_addr[0] = ((val >> 8) & 0xFF);
	val = read_reg(PHY21, ADDR_TABLE_IP_0);
	ipc_display.ip_addr[3] = (val & 0xFF);
	ipc_display.ip_addr[2] = ((val >> 8) & 0xFF);
	val = read_reg(PHY21, ADDR_TABLE_IP_1);
	ipc_display.ip_addr[1] = (val & 0xFF);
	ipc_display.ip_addr[0] = ((val >> 8) & 0xFF);
	return ipc_display;
}
/**********************************************************
 *  Function: port_get_ipc_mac
 *
 *  Description:
 *    Get port IPC mac address.
 *  
 *  Parameters:
 *    port       - port
 *    mac        - mac address buf
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int	port_get_ipc_mac(unsigned char port, unsigned char* mac)
{
	struct _ipc_ip_mac_entry get_port_ipc;
	unsigned char macid[6]={0};
	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	if (port_get_status(port))
		get_port_ipc = get_ipc_entry(port);
	sprintf (macid,"%02X:%02X:%02X:%02X:%02X:%02X", 
			get_port_ipc.mac_addr[0],get_port_ipc.mac_addr[1],get_port_ipc.mac_addr[2],
			get_port_ipc.mac_addr[3],get_port_ipc.mac_addr[4],get_port_ipc.mac_addr[5]);
	memcpy(mac,macid,6);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_set_enable
 *
 *  Description:
 *    Set mirror enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int mirror_set_enable(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, MIR_MODE);
	reg_val &= ~((unsigned short)0x1 << 15);
	reg_val |= ((unsigned short)enabled << 15);
	write_reg(PHY21, MIR_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_enable
 *
 *  Description:
 *    Get mirror enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int mirror_get_enable(void)
{	
	return (int)((read_reg(PHY21, MIR_MODE) >> 15) & 0x1);
}
/**********************************************************
 *  Function: mirror_set_only_mir_pkt
 *
 *  Description:
 *    Set mirror port only transmit the mirrored packets.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int mirror_set_only_mir_pkt(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, MIR_MODE);
	reg_val &= ~((unsigned short)0x1 << 14);
	reg_val |= ((unsigned short)enabled << 14);
	write_reg(PHY21, MIR_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_only_mir_pkt
 *
 *  Description:
 *    Get mirror port only transmit the mirrored packets.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int mirror_get_only_mir_pkt(void)
{	
	return (int)((read_reg(PHY21, MIR_MODE) >> 14) & 0x1);
}
/**********************************************************
 *  Function: mirror_set_mode
 *
 *  Description:
 *    Set mirror captured condition.
 *  
 *  Parameters:
 *    mode    - mirror_RX (0) or
 *              mirror_TX (1) or
 *              mirror_RX_and_TX (2) or
 *              mirror_RX_or_TX (3)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int mirror_set_mode(unsigned char mode)
{
	int reg_val;
	unsigned char tmp =  0x3;
	
	if (mode > mirror_RX_or_TX)
		return C_FAIL;
	
	reg_val = read_reg(PHY21, MIR_MODE);
	reg_val &= ~(tmp << 12);
	reg_val |= (mode << 12);
	write_reg(PHY21, MIR_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_mode
 *
 *  Description:
 *    Get mirror captured condition.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - mirror_RX
 *    1       - mirror_TX
 *    2       - mirror_RX_and_TX
 *    3       - mirror_RX_or_TX
 *    
 *********************************************************/
int mirror_get_mode(void)
{	
	return (int)((read_reg(PHY21, MIR_MODE) >> 12) & 0x3);
}
/**********************************************************
 *  Function: mirror_set_port_enable
 *
 *  Description:
 *    Set mirror port enable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int mirror_set_port_enable(unsigned char port,unsigned char enabled)
{
	int reg_val=0;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	reg_val = read_reg(PHY21, MIR_MODE);
	reg_val &= ~((unsigned short)0x1 << port);
	reg_val |= ((unsigned short)enabled << port);
	write_reg(PHY21, MIR_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_port_enable
 *
 *  Description:
 *    Get mirror port enable.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int mirror_get_port_enable(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY21, MIR_MODE) >> port) & 0x1);
}
/**********************************************************
 *  Function: mirror_set_port_RX_enable
 *
 *  Description:
 *    Set mirror port RX enable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int mirror_set_port_RX_enable(unsigned char port,unsigned char enabled)
{
	int reg_val=0;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	reg_val = read_reg(PHY21, MIR_RX);
	reg_val &= ~((unsigned short)0x1 << port);
	reg_val |= ((unsigned short)enabled << port);
	write_reg(PHY21, MIR_RX, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_port_RX_enable
 *
 *  Description:
 *    Get mirror port RX enable.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int mirror_get_port_RX_enable(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY21, MIR_RX) >> port) & 0x1);
}
/**********************************************************
 *  Function: mirror_set_port_TX_enable
 *
 *  Description:
 *    Set mirror port TX enable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int mirror_set_port_TX_enable(unsigned char port,unsigned char enabled)
{
	int reg_val=0;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	reg_val = read_reg(PHY21, MIR_TX);
	reg_val &= ~((unsigned short)0x1 << port);
	reg_val |= ((unsigned short)enabled << port);
	write_reg(PHY21, MIR_TX, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: mirror_get_port_TX_enable
 *
 *  Description:
 *    Get mirror port TX enable.
 *  
 *  Parameters:
 *    port       - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int mirror_get_port_TX_enable(unsigned char port)
{
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY21, MIR_TX) >> port) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_fast_leave
 *
 *  Description:
 *    Set IGMP fast leave enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_fast_leave(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 10);
	reg_val |= ((unsigned short)enabled << 10);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_fast_leave
 *
 *  Description:
 *    Get IGMP fast leave enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_fast_leave(void)
{
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 10) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_MG_INCLUDE_RP
 *
 *  Description:
 *    Set IGMP multicast group include router port enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_MG_INCLUDE_RP(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 9);
	reg_val |= ((unsigned short)enabled << 9);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_MG_INCLUDE_RP
 *
 *  Description:
 *    Get IGMP multicast group include router port enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_MG_INCLUDE_RP(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 9) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_FLOOD_UNIGMP
 *
 *  Description:
 *    Set IGMP Flood Unknown IGMP enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Note �V Unknown IGMP is not one of following:
 *    1. General Query
 *    2. Group-Specific Query
 *    3. IGMP Report
 *    4. IGMP Leave
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_FLOOD_UNIGMP(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 8);
	reg_val |= ((unsigned short)enabled << 8);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_FLOOD_UNIGMP
 *
 *  Description:
 *    Get IGMP Flood Unknown IGMP enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_FLOOD_UNIGMP(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 8) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_FLOOD_IPM_CTRL
 *
 *  Description:
 *    Set IGMP Flood IP Multicast Control Packet enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Note �V IP multicast control packet: DMAC=01-00-5e-xx-xx-xx, DIP= 224.0.0.x and non-IGMP
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_FLOOD_IPM_CTRL(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 7);
	reg_val |= ((unsigned short)enabled << 7);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_FLOOD_IPM_CTRL
 *
 *  Description:
 *    Get IGMP Flood IP Multicast Control Packet enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_FLOOD_IPM_CTRL(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 7) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_UNIPM_MODE
 *
 *  Description:
 *    Set IGMP Unknown IP Multicast Data Mode.
 *  
 *  Parameters:
 *    mode    - discard                (0) or
 *              forward_to_CPU         (1) or
 *              flood_packet           (2) or
 *              forward_to_router_port (3)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_UNIPM_MODE(unsigned char mode)
{
	int reg_val;
	unsigned char tmp =  0x3;
	
	if (mode > forward_to_router_port)
		return C_FAIL;
	
	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~(tmp << 5);
	reg_val |= (mode << 5);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_UNIPM_MODE
 *
 *  Description:
 *    Get IGMP Unknown IP Multicast Data Mode.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - discard
 *    1       - forward_to_CPU
 *    2       - flood_packet
 *    3       - forward_to_router_port
 *    
 *********************************************************/
int igmp_get_UNIPM_MODE(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 5) & 0x3);
}
/**********************************************************
 *  Function: igmp_set_DISCARD_LEAVE
 *
 *  Description:
 *    Set IGMP Discard IGMP leave message enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_DISCARD_LEAVE(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 4);
	reg_val |= ((unsigned short)enabled << 4);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_DISCARD_LEAVE
 *
 *  Description:
 *    Get IGMP Discard IGMP leave message enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_DISCARD_LEAVE(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 4) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_FLOOD_RPT
 *
 *  Description:
 *    Set IGMP Flood report message to other ports enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_FLOOD_RPT(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 3);
	reg_val |= ((unsigned short)enabled << 3);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_FLOOD_RPT
 *
 *  Description:
 *    Get IGMP Flood report message to other ports enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_FLOOD_RPT(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 3) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_LRP_NULL_SIP
 *
 *  Description:
 *    Set IGMP Learn router port even if source IP address is 0.0.0.0 enable/disable.
 *
 *  Note �V It is valid only if LEARN_RP is enabled
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_LRP_NULL_SIP(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 2);
	reg_val |= ((unsigned short)enabled << 2);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_LRP_NULL_SIP
 *
 *  Description:
 *    Get IGMP Learn router port even if source IP address is 0.0.0.0 enable/disable.
 *
 *  Note �V It is valid only if LEARN_RP is enabled
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_LRP_NULL_SIP(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 2) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_LEARN_RP
 *
 *  Description:
 *    Set IGMP Learn Router Port enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_LEARN_RP(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 1);
	reg_val |= ((unsigned short)enabled << 1);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_LEARN_RP
 *
 *  Description:
 *    Get IGMP Learn Router Port enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_LEARN_RP(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 1) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_HW_IGMP_EN
 *
 *  Description:
 *    Set Hardware IGMP enable/disable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    
 *********************************************************/
int igmp_set_HW_IGMP_EN(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY21, IGMP_BASE_CONTROL);
	reg_val &= ~((unsigned short)0x1 << 0);
	reg_val |= ((unsigned short)enabled << 0);
	write_reg(PHY21, IGMP_BASE_CONTROL, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_HW_IGMP_EN
 *
 *  Description:
 *    Get Hardware IGMP enable/disable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int igmp_get_HW_IGMP_EN(void)
{	
	return (int)((read_reg(PHY21, IGMP_BASE_CONTROL) >> 0) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_DEFAULT_ROUTER_PORT
 *
 *  Description:
 *    Set IGMP default router port.
 *  
 *  Parameters:
 *    port    - port
 *    RP      - default router port disable (0) or
 *              default router port enable  (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_DEFAULT_ROUTER_PORT(unsigned char port, unsigned char RP)
{
  int reg_val=0;
  
  if (port>MAX_PORT_NUM-2 || RP>1)
    return C_FAIL;
  
  reg_val = read_reg(PHY21, IGMP_ROUTER_PORT);
  reg_val &= ~((unsigned short)0x1 << port);
  reg_val |= ((unsigned short)RP << port);
  write_reg(PHY21, IGMP_ROUTER_PORT, reg_val);
  
  return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_DEFAULT_ROUTER_PORT
 *
 *  Description:
 *    Get IGMP default router port.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - default router port disable
 *    1       - default router port enable
 *    
 *********************************************************/
int igmp_get_DEFAULT_ROUTER_PORT(unsigned char port)
{
  if (port>MAX_PORT_NUM-2)
    return C_FAIL;
  else
    return (int)((read_reg(PHY21, IGMP_ROUTER_PORT) >> port) & 0x1);
}
/**********************************************************
 *  Function: igmp_set_ROUTER_TIMEOUT_VLE
 *
 *  Description:
 *    Set IGMP Router Timeout Value.
 *  
 *  Parameters:
 *    value    - ROUTER_TIMEOUT_VLE
 *
 *  Note:
 *    Router Timeout = ROUTER_TIMEOUT_UNIT * ROUTER_TIMEOUT_VLE
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_ROUTER_TIMEOUT_VLE(unsigned char value)
{
	int reg_val;
	unsigned char tmp =  0xFF;
	
	reg_val = read_reg(PHY21, IGMP_ROUTER_TIMEOUT);
	reg_val &= ~(tmp << 4);
	reg_val |= (value << 4);
	write_reg(PHY21, IGMP_ROUTER_TIMEOUT, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_ROUTER_TIMEOUT_VLE
 *
 *  Description:
 *    Get IGMP Router Timeout Value.
 *  
 *  Parameters:
 *    None
 *
 *  Note:
 *    Router Timeout = ROUTER_TIMEOUT_UNIT * ROUTER_TIMEOUT_VLE
 *
 *  Return:
 *    value    - ROUTER_TIMEOUT_VLE
 *    
 *********************************************************/
int igmp_get_ROUTER_TIMEOUT_VLE(void)
{	
	return (int)((read_reg(PHY21, IGMP_ROUTER_TIMEOUT) >> 4) & 0xFF);
}
/**********************************************************
 *  Function: igmp_set_ROUTER_TIMEOUT_UNIT
 *
 *  Description:
 *    Set IGMP Router Timeout Unit.
 *  
 *  Parameters:
 *    unit    - 1 second (0) or
 *              2 second (1) or
 *              4 second (2) or
 *              8 second (3)
 *
 *  Note:
 *    Router Timeout = ROUTER_TIMEOUT_UNIT * ROUTER_TIMEOUT_VLE
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_ROUTER_TIMEOUT_UNIT(unsigned char unit)
{
	int reg_val;
	unsigned char tmp =  0x3;
	
	if (unit > 0x3)
		return C_FAIL;
	
	reg_val = read_reg(PHY21, IGMP_ROUTER_TIMEOUT);
	reg_val &= ~(tmp << 0);
	reg_val |= (unit << 0);
	write_reg(PHY21, IGMP_ROUTER_TIMEOUT, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_ROUTER_TIMEOUT_UNIT
 *
 *  Description:
 *    Get IGMP Router Timeout Unit.
 *  
 *  Parameters:
 *    None
 *
 *  Note:
 *    Router Timeout = ROUTER_TIMEOUT_UNIT * ROUTER_TIMEOUT_VLE
 *
 *  Return:
 *    0       - 1 second
 *    1       - 2 second
 *    2       - 4 second
 *    3       - 8 second
 *    
 *********************************************************/
int igmp_get_ROUTER_TIMEOUT_UNIT(void)
{	
	return (int)((read_reg(PHY21, IGMP_ROUTER_TIMEOUT) >> 0) & 0x3);
}
/**********************************************************
 *  Function: igmp_set_IGMP_TIMEOUT_VLE
 *
 *  Description:
 *    Set IGMP Timeout Value.
 *  
 *  Parameters:
 *    value    - IGMP_TIMEOUT_VLE
 *
 *  Note:
 *    IGMP Timeout = IGMP_TIMEOUT_UNIT * IGMP_TIMEOUT_VLE
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_IGMP_TIMEOUT_VLE(unsigned char value)
{
	int reg_val;
	unsigned char tmp =  0xFF;
	
	reg_val = read_reg(PHY21, IGMP_TIMEOUT);
	reg_val &= ~(tmp << 4);
	reg_val |= (value << 4);
	write_reg(PHY21, IGMP_TIMEOUT, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_IGMP_TIMEOUT_VLE
 *
 *  Description:
 *    Get IGMP Timeout Value.
 *  
 *  Parameters:
 *    None
 *
 *  Note:
 *    IGMP Timeout = IGMP_TIMEOUT_UNIT * IGMP_TIMEOUT_VLE
 *
 *  Return:
 *    value    - IGMP_TIMEOUT_VLE
 *    
 *********************************************************/
int igmp_get_IGMP_TIMEOUT_VLE(void)
{	
	return (int)((read_reg(PHY21, IGMP_TIMEOUT) >> 4) & 0xFF);
}
/**********************************************************
 *  Function: igmp_set_IGMP_TIMEOUT_UNIT
 *
 *  Description:
 *    Set IGMP Timeout Unit.
 *  
 *  Parameters:
 *    unit    - 1 second (0) or
 *              2 second (1) or
 *              4 second (2) or
 *              8 second (3)
 *
 *  Note:
 *    IGMP Timeout = IGMP_TIMEOUT_UNIT * IGMP_TIMEOUT_VLE
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int igmp_set_IGMP_TIMEOUT_UNIT(unsigned char unit)
{
	int reg_val;
	unsigned char tmp =  0x3;
	
	if (unit > 0x3)
		return C_FAIL;
	
	reg_val = read_reg(PHY21, IGMP_TIMEOUT);
	reg_val &= ~(tmp << 0);
	reg_val |= (unit << 0);
	write_reg(PHY21, IGMP_TIMEOUT, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: igmp_get_IGMP_TIMEOUT_UNIT
 *
 *  Description:
 *    Get IGMP Timeout Unit.
 *  
 *  Parameters:
 *    None
 *
 *  Note:
 *    IGMP Timeout = IGMP_TIMEOUT_UNIT * IGMP_TIMEOUT_VLE
 *
 *  Return:
 *    0       - 1 second
 *    1       - 2 second
 *    2       - 4 second
 *    3       - 8 second
 *    
 *********************************************************/
int igmp_get_IGMP_TIMEOUT_UNIT(void)
{	
	return (int)((read_reg(PHY21, IGMP_TIMEOUT) >> 0) & 0x3);
}
/**********************************************************
 *  Function: QOS_set_TOS_OVER_VLAN
 *
 *  Description:
 *    Set ToS Precedence over VLAN Priority.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_TOS_OVER_VLAN(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY20, QOS_MODE);
	reg_val &= ~((unsigned short)0x1 << 15);
	reg_val |= ((unsigned short)enabled << 15);
	write_reg(PHY20, QOS_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_TOS_OVER_VLAN
 *
 *  Description:
 *    Get ToS Precedence over VLAN Priority.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int QOS_get_TOS_OVER_VLAN(void)
{	
	return (int)((read_reg(PHY20, QOS_MODE) >> 15) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_SP_EN
 *
 *  Description:
 *    Set Strict Priority Enable.
 *  
 *  Parameters:
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_SP_EN(unsigned char enabled)
{
	int reg_val=0;

	reg_val = read_reg(PHY20, QOS_MODE);
	reg_val &= ~((unsigned short)0x1 << 14);
	reg_val |= ((unsigned short)enabled << 14);
	write_reg(PHY20, QOS_MODE, reg_val);
	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_SP_EN
 *
 *  Description:
 *    Get Strict Priority Enable.
 *  
 *  Parameters:
 *    None
 *
 *  Return:
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int QOS_get_SP_EN(void)
{	
	return (int)((read_reg(PHY20, QOS_MODE) >> 14) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_COS_EN
 *
 *  Description:
 *    Set port Class of Service Enable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_COS_EN(unsigned char port,unsigned char enabled)
{
	unsigned short u16dat;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_MODE);
	u16dat &= ~((unsigned short)0x1 << port);
	u16dat |= ((unsigned short)enabled << port);
	write_reg(PHY20, QOS_MODE, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_COS_EN
 *
 *  Description:
 *    Get port Class of Service Enable.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int QOS_get_COS_EN(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY20, QOS_MODE) >> port) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_PORT_PRI_EN
 *
 *  Description:
 *    Set Port Based QoS Enable.
 *  
 *  Parameters:
 *    port       - port
 *    enabled    - disable (0) or
 *                 enable (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_PORT_PRI_EN(unsigned char port,unsigned char enabled)
{
	unsigned short u16dat;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_PORT_EN);
	u16dat &= ~((unsigned short)0x1 << port);
	u16dat |= ((unsigned short)enabled << port);
	write_reg(PHY20, QOS_PORT_EN, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_PORT_PRI_EN
 *
 *  Description:
 *    Get Port Based QoS Enable.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - disable
 *    1       - enable
 *    
 *********************************************************/
int QOS_get_PORT_PRI_EN(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY20, QOS_PORT_EN) >> port) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_PORT_PRI
 *
 *  Description:
 *    Set Port Based QoS priority.
 *  
 *  Parameters:
 *    port       - port
 *    priority   - low priority (0) or
 *                 high priority (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_PORT_PRI(unsigned char port,unsigned char priority)
{
	unsigned short u16dat;

	if (port>MAX_PORT_NUM-2)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_PORT_PRI);
	u16dat &= ~((unsigned short)0x1 << port);
	u16dat |= ((unsigned short)priority << port);
	write_reg(PHY20, QOS_PORT_PRI, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_PORT_PRI
 *
 *  Description:
 *    Get Port Based QoS priority.
 *  
 *  Parameters:
 *    port    - port
 *
 *  Return:
 *    -1      - failed
 *    0       - low priority
 *    1       - high priority
 *    
 *********************************************************/
int QOS_get_PORT_PRI(unsigned char port)
{	
	if (port>MAX_PORT_NUM-2)
		return C_FAIL;
	else
		return (int)((read_reg(PHY20, QOS_PORT_PRI) >> port) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_VLAN_PRI2Q
 *
 *  Description:
 *    Set VLAN priority output queue.
 *  
 *  Parameters:
 *    vlan_pri   - vlan priority
 *    priority   - low priority (0) or
 *                 high priority (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_VLAN_PRI2Q(unsigned char vlan_pri,unsigned char priority)
{
	unsigned short u16dat;

	if (vlan_pri>7)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_VLAN_WRR);
	u16dat &= ~((unsigned short)0x1 << (vlan_pri+8));
	u16dat |= ((unsigned short)priority << (vlan_pri+8));
	write_reg(PHY20, QOS_VLAN_WRR, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_VLAN_PRI2Q
 *
 *  Description:
 *    Get VLAN priority output queue.
 *  
 *  Parameters:
 *    vlan_pri   - vlan priority
 *
 *  Return:
 *    -1      - failed
 *    0       - low priority
 *    1       - high priority
 *    
 *********************************************************/
int QOS_get_VLAN_PRI2Q(unsigned char vlan_pri)
{	
	if (vlan_pri>7)
		return C_FAIL;
	else
		return (int)((read_reg(PHY20, QOS_VLAN_WRR) >> (vlan_pri+8)) & 0x1);
}
/**********************************************************
 *  Function: QOS_set_WRR_Q0WEIGHT
 *
 *  Description:
 *    Set QOS WRR Queue 0 Weight.
 *  
 *  Parameters:
 *    weight     - weight
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_WRR_Q0WEIGHT(unsigned char weight)
{
	unsigned short u16dat;

	if (weight>15)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_VLAN_WRR);
	u16dat &= ~((unsigned short)0xF << 0);
	u16dat |= ((unsigned short)weight << 0);
	write_reg(PHY20, QOS_VLAN_WRR, u16dat);

	return C_SUCCESS;
}
int QOS_set_WRR_Q1WEIGHT(unsigned char weight)
{
	unsigned short u16dat;

	if (weight>15)
		return C_FAIL;

	u16dat = read_reg(PHY20, QOS_VLAN_WRR);
	u16dat &= ~((unsigned short)0xF << 4);
	u16dat |= ((unsigned short)weight << 4);
	write_reg(PHY20, QOS_VLAN_WRR, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_WRR_Q0WEIGHT
 *
 *  Description:
 *    Get QOS WRR Queue 0 Weight.
 *  
 *  Parameters:
 *    none
 *
 *  Return:
 *    -1      - failed
 *    0~0xF   - weight
 *    
 *********************************************************/
int QOS_get_WRR_Q0WEIGHT(void)
{	
	return (int)((read_reg(PHY20, QOS_VLAN_WRR) >> 0) & 0xF);
}
int QOS_get_WRR_Q1WEIGHT(void)
{	
	return (int)((read_reg(PHY20, QOS_VLAN_WRR) >> 4) & 0xF);
}
/**********************************************************
 *  Function: QOS_set_DSCP2Q
 *
 *  Description:
 *    Set DSCP to different output queue.
 *  
 *  Parameters:
 *    DSCP       - DSCP
 *    priority   - low priority (0) or
 *                 high priority (1)
 *
 *  Return:
 *    0     - success
 *    -1    - failed
 *    
 *********************************************************/
int QOS_set_DSCP2Q(unsigned char DSCP,unsigned char priority)
{
	unsigned short u16dat, reg, offset;

	if (DSCP>63)
		return C_FAIL;

	reg=DSCP/16;
	offset=DSCP%16;
	
	u16dat = read_reg(PHY20, QOS_DSCP2Q0+reg);
	u16dat &= ~((unsigned short)0x1 << offset);
	u16dat |= ((unsigned short)priority << offset);
	write_reg(PHY20, QOS_DSCP2Q0+reg, u16dat);

	return C_SUCCESS;
}
/**********************************************************
 *  Function: QOS_get_DSCP2Q
 *
 *  Description:
 *    Get DSCP to different output queue.
 *  
 *  Parameters:
 *    DSCP       - DSCP
 *
 *  Return:
 *    -1      - failed
 *    0       - low priority
 *    1       - high priority
 *    
 *********************************************************/
int QOS_get_DSCP2Q(unsigned char DSCP)
{	
	if (DSCP>63)
		return C_FAIL;
	else
		return (int)((read_reg(PHY20, QOS_DSCP2Q0+(DSCP/16)) >> (DSCP%16)) & 0x1);
}

//=============================================================================

#ifndef USER_API

EXPORT_SYMBOL(LUT_set_mode);
EXPORT_SYMBOL(LUT_get_mode);
EXPORT_SYMBOL(LUT_get_2k_hash_index);
EXPORT_SYMBOL(LUT_get_1k1k_hash_index);
EXPORT_SYMBOL(LUT_get_next_valid_entry);
EXPORT_SYMBOL(LUT_set_entry);
EXPORT_SYMBOL(LUT_get_entry);
EXPORT_SYMBOL(LUT_get_port_by_mac);
EXPORT_SYMBOL(LUT_flush_table);
EXPORT_SYMBOL(LUT_flush_port);
EXPORT_SYMBOL(LUT_flush_entry);

EXPORT_SYMBOL(VLAN_set_mode);
EXPORT_SYMBOL(VLAN_get_mode);
EXPORT_SYMBOL(VLAN_PB_set_member);
EXPORT_SYMBOL(VLAN_PB_get_member);
EXPORT_SYMBOL(VLAN_PB_set_PVID);
EXPORT_SYMBOL(VLAN_PB_get_PVID);
EXPORT_SYMBOL(VLAN_PB_set_add_tag);
EXPORT_SYMBOL(VLAN_PB_get_add_tag);
EXPORT_SYMBOL(VLAN_PB_set_remove_tag);
EXPORT_SYMBOL(VLAN_PB_get_remove_tag);
EXPORT_SYMBOL(VLAN_TB_set_entry);
EXPORT_SYMBOL(VLAN_TB_get_entry);
EXPORT_SYMBOL(VLAN_set_ingress_type);
EXPORT_SYMBOL(VLAN_get_ingress_type);
EXPORT_SYMBOL(VLAN_set_ingress_filter);
EXPORT_SYMBOL(VLAN_get_ingress_filter);
EXPORT_SYMBOL(VLAN_set_unknown_vid_mode);
EXPORT_SYMBOL(VLAN_get_unknown_vid_mode);
EXPORT_SYMBOL(port_set_enable);
EXPORT_SYMBOL(port_get_enable);
EXPORT_SYMBOL(port_set_flow_control);
EXPORT_SYMBOL(port_get_flow_control);
EXPORT_SYMBOL(port_set_extend_mode);
EXPORT_SYMBOL(port_get_speed);
EXPORT_SYMBOL(port_get_status);
EXPORT_SYMBOL(set_broadcast_storm_threshold);
EXPORT_SYMBOL(get_broadcast_storm_threshold);
EXPORT_SYMBOL(get_ipc_entry);
EXPORT_SYMBOL(port_get_ipc_mac);
EXPORT_SYMBOL(mirror_set_enable);
EXPORT_SYMBOL(mirror_get_enable);
EXPORT_SYMBOL(mirror_set_only_mir_pkt);
EXPORT_SYMBOL(mirror_get_only_mir_pkt);
EXPORT_SYMBOL(mirror_set_mode);
EXPORT_SYMBOL(mirror_get_mode);
EXPORT_SYMBOL(mirror_set_port_enable);
EXPORT_SYMBOL(mirror_get_port_enable);
EXPORT_SYMBOL(mirror_set_port_RX_enable);
EXPORT_SYMBOL(mirror_get_port_RX_enable);
EXPORT_SYMBOL(mirror_set_port_TX_enable);
EXPORT_SYMBOL(mirror_get_port_TX_enable);
EXPORT_SYMBOL(igmp_set_fast_leave);
EXPORT_SYMBOL(igmp_get_fast_leave);
EXPORT_SYMBOL(igmp_set_MG_INCLUDE_RP);
EXPORT_SYMBOL(igmp_get_MG_INCLUDE_RP);
EXPORT_SYMBOL(igmp_set_FLOOD_UNIGMP);
EXPORT_SYMBOL(igmp_get_FLOOD_UNIGMP);
EXPORT_SYMBOL(igmp_set_FLOOD_IPM_CTRL);
EXPORT_SYMBOL(igmp_get_FLOOD_IPM_CTRL);
EXPORT_SYMBOL(igmp_set_UNIPM_MODE);
EXPORT_SYMBOL(igmp_get_UNIPM_MODE);
EXPORT_SYMBOL(igmp_set_DISCARD_LEAVE);
EXPORT_SYMBOL(igmp_get_DISCARD_LEAVE);
EXPORT_SYMBOL(igmp_set_FLOOD_RPT);
EXPORT_SYMBOL(igmp_get_FLOOD_RPT);
EXPORT_SYMBOL(igmp_set_LRP_NULL_SIP);
EXPORT_SYMBOL(igmp_get_LRP_NULL_SIP);
EXPORT_SYMBOL(igmp_set_LEARN_RP);
EXPORT_SYMBOL(igmp_get_LEARN_RP);
EXPORT_SYMBOL(igmp_set_HW_IGMP_EN);
EXPORT_SYMBOL(igmp_get_HW_IGMP_EN);
EXPORT_SYMBOL(igmp_set_DEFAULT_ROUTER_PORT);
EXPORT_SYMBOL(igmp_get_DEFAULT_ROUTER_PORT);
EXPORT_SYMBOL(igmp_set_ROUTER_TIMEOUT_VLE);
EXPORT_SYMBOL(igmp_get_ROUTER_TIMEOUT_VLE);
EXPORT_SYMBOL(igmp_set_ROUTER_TIMEOUT_UNIT);
EXPORT_SYMBOL(igmp_get_ROUTER_TIMEOUT_UNIT);
EXPORT_SYMBOL(igmp_set_IGMP_TIMEOUT_VLE);
EXPORT_SYMBOL(igmp_get_IGMP_TIMEOUT_VLE);
EXPORT_SYMBOL(igmp_set_IGMP_TIMEOUT_UNIT);
EXPORT_SYMBOL(igmp_get_IGMP_TIMEOUT_UNIT);
EXPORT_SYMBOL(QOS_set_TOS_OVER_VLAN);
EXPORT_SYMBOL(QOS_get_TOS_OVER_VLAN);
EXPORT_SYMBOL(QOS_set_SP_EN);
EXPORT_SYMBOL(QOS_get_SP_EN);
EXPORT_SYMBOL(QOS_set_COS_EN);
EXPORT_SYMBOL(QOS_get_COS_EN);
EXPORT_SYMBOL(QOS_set_PORT_PRI_EN);
EXPORT_SYMBOL(QOS_get_PORT_PRI_EN);
EXPORT_SYMBOL(QOS_set_PORT_PRI);
EXPORT_SYMBOL(QOS_get_PORT_PRI);
EXPORT_SYMBOL(QOS_set_VLAN_PRI2Q);
EXPORT_SYMBOL(QOS_get_VLAN_PRI2Q);
EXPORT_SYMBOL(QOS_set_WRR_Q0WEIGHT);
EXPORT_SYMBOL(QOS_set_WRR_Q1WEIGHT);
EXPORT_SYMBOL(QOS_get_WRR_Q0WEIGHT);
EXPORT_SYMBOL(QOS_get_WRR_Q1WEIGHT);
EXPORT_SYMBOL(QOS_set_DSCP2Q);
EXPORT_SYMBOL(QOS_get_DSCP2Q);
#endif

