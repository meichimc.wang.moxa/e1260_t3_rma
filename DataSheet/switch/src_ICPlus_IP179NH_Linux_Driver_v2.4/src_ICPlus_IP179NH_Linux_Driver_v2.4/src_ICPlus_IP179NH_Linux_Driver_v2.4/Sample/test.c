#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "drvlib.h"

void main(void)
{
	int i, ret=0;
	unsigned char LutMode;
	struct LUT_entry lentry, getentry;
	unsigned short idx=0;
	unsigned int paddr;
	int fd;
	
	printf("Strart ....\n");
	fd=open("/dev/dev_switch",O_RDWR);
	if(fd<0)
	{
		printf("can't open dev_switch \n" );
		perror("open: ");
		return;
	}  
	else
	{
		printf("can open dev_switch \n" );
		close(fd);
	}  
  	
	LUT_set_mode(LUT_MODE_1K1K);
	LutMode = LUT_get_mode();
	printf("LUT Mode=%d\n", LutMode);
	
	//UCST
	lentry.valid = 0x1;
	lentry.type = LUT_MAC_UNICAST;
	lentry.entry = 0x3;
	lentry.fid = 0x7;
	lentry.MAC[0] = 0x08;
	lentry.MAC[1] = 0x99;
	lentry.MAC[2] = 0x32;
	lentry.MAC[3] = 0x1F;
	lentry.MAC[4] = 0x5B;
	lentry.MAC[5] = 0xE7;
	lentry.aged = 0x0;
	lentry.port_map = 0x40;  
	ret = LUT_set_entry(lentry);

	idx = LUT_get_1k1k_hash_index(LUT_MAC_UNICAST, lentry.fid, lentry.MAC, lentry.entry);
	ret = LUT_get_entry(idx, &getentry);  
	printf("\nidx=0x%04X:\n", idx);
	printf("valid=%d\n", getentry.valid);
	printf("type=%d\n", getentry.type);
	printf("entry=%d\n", getentry.entry);
	printf("fid=0x%X\n", getentry.fid);
	printf("MAC=");
	for (i=0; i < 6; i++)
		printf("0x%02X:", getentry.MAC[i]);
	printf("\naged=0x%X\n", getentry.aged);
	printf("port_map=0x%03X\n", getentry.port_map);
	
	//IGMP
	lentry.valid = 0x1;
	lentry.type = LUT_MAC_IGMP;
	lentry.entry = 0x1;
	lentry.fid = 0xE;
	lentry.MAC[0] = 0x01;
	lentry.MAC[1] = 0x00;
	lentry.MAC[2] = 0x5E;
	lentry.MAC[3] = 0x6A;
	lentry.MAC[4] = 0x29;
	lentry.MAC[5] = 0xC8;
	lentry.aged = 0x12D;
	lentry.port_map = 0x1FF;  
 
	ret = LUT_set_entry(lentry);
 
	idx = LUT_get_1k1k_hash_index(LUT_MAC_IGMP, lentry.fid, lentry.MAC, lentry.entry);
 
	ret = LUT_get_entry(idx, &getentry);  
	printf("\nidx=0x%04X:\n", idx);
	printf("valid=%d\n", getentry.valid);
	printf("type=%d\n", getentry.type);
	printf("entry=%d\n", getentry.entry);
	printf("fid=0x%X\n", getentry.fid);
	printf("MAC=");
	for (i=0; i < 6; i++)
		printf("0x%02X:", getentry.MAC[i]);
	printf("\naged=0x%X\n", getentry.aged);
	printf("port_map=0x%03X\n", getentry.port_map);
}
