#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "drvlib.h"

extern void switch_write_reg(unsigned char phyad, unsigned char regad, unsigned short Regdata);
extern unsigned short switch_read_reg(unsigned char phyad, unsigned char regad);
void sw_set_rstp(void);

int main(int argc,char *argv[])
{
    int i, ret=0;
    unsigned int iParm[10];
	  unsigned int uTempRead;
	  int fd;
	  
	  /* Skip command name */
	  argc--;
	  argv++;
	
	  if(argc <= 0)
	  {
		    printf("please input param... \n" );
		    return 0;
	  }
		
	
    printf("SW Tool Strart ....\n");
	  fd=open("/dev/dev_switch",O_RDWR);
	  if(fd<0)
	  {
		    printf("can't open dev_switch \n" );
		    return 0;
	  }  
	  else
	  {
		    printf("dev_switch is open..\n" );
		    close(fd);
	  }
	
    if(argv[0][0] == 's') //set ip179 reg
    {
        if(argc >= 4)
     	  {
     		    iParm[0] = strtol(argv[1], NULL, 10);
     		    iParm[1] = strtol(argv[2], NULL, 10);
     		    iParm[2] = strtol(argv[3], NULL, 16);
     		
     		    switch_write_reg(iParm[0],iParm[1],iParm[02]);
     		    printf("ip179 reg write:PHY=%d,MII=%d,value:0x%x.\n", iParm[0],iParm[1],iParm[2]);
     		    uTempRead = switch_read_reg(iParm[0], iParm[1]);
     		    printf("ip179 reg read back:PHY=%d,MII=%d,value:0x%x.\n", iParm[0],iParm[1],uTempRead);
     		    return 1;
        }
    }
     
    if(argv[0][0] == 'r') //read ip179 reg
    {
        if(argc >= 3)
     	  {
     		    iParm[0] = strtol(argv[1], NULL, 10);
     		    iParm[1] = strtol(argv[2], NULL, 10);
     	
     		    uTempRead = switch_read_reg(iParm[0], iParm[1]);
     		    printf("ip179 reg read:PHY=%d,MII=%d,value:0x%x.\n", iParm[0],iParm[1],uTempRead);
     		    return 1;
        }   	  
    }	
    
    if(argv[0][0] == 'm') //ip179 mod set
    {
    	  if(argc >= 2)
    	  {
    	      if(strcmp(argv[1],"rstp") == 0)
    	      {
    	          printf("ip179 start rstp mode...\n");
    	      	  sw_set_rstp();
    	      	  return 1;
    	      }	
    	  }
    }	 
     
    return 1;
}

void sw_set_rstp()
{
    /*  21.4=0x0305 (Fast aging) 
        20.7=0x00AD (1K unicast table and 1K multicast table; Discard unknown unicast and multicast DMAC; Enable learn constrain) 
    */
    switch_write_reg(21, 4, 0x0305);
    switch_write_reg(20, 7, 0xAD);
    //switch_write_reg(20, 3, 0x71); // Special tagging for RX enable

#if 1    
    
    /*  BPDU to CPU: MAC address=01 80 C2 00 00 00, FID=0
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x0100 (FID=0, Port MAP: port8) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC50C (Write hash index 50C)
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x100);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC50C);  
    
    /*  BPDU to port0: MAC address=01 80 C2 00 00 00, FID=1/ 
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x1001 (FID=1, Port MAP: port0) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC508 (Write hash index 508) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x1001);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC508);

    /*  BPDU to port1: MAC address=01 80 C2 00 00 00, FID=2/ 
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x2002 (FID=2, Port MAP: port1) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC504 (Write hash index 504) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x2002);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC504);

    /*  BPDU to port2: MAC address=01 80 C2 00 00 00, FID=3/ 
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x3004 (FID=3, Port MAP: port2) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC500 (Write hash index 500) 
     */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x3004);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC500);
        
    /*  BPDU to port3: MAC address=01 80 C2 00 00 00, FID=4/ 
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x4008 (FID=4, Port MAP: port3) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC51C (Write hash index 51C) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x4008);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC51C);
    
    /*  BPDU to port4: MAC address=01 80 C2 00 00 00, FID=5/ 
        21.9=0x0000 (MAC[15:0]=0000) 
        21.10=0xC200 (MAC[31:16]=C200) 
        21.11=0x0180 (MAC[47:32]=0180) 
        21.12=0x5010 (FID=5, Port MAP: port4) 
        21.13=0x8000 (Entry valid) 
        21.8=0xC518 (Write hash index 518) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x5010);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC518);
    
    /*
    /BPDU to port5: MAC address=01 80 C2 00 00 00, FID=6/ 
    21.9=0x0000 (MAC[15:0]=0000) 
    21.10=0xC200 (MAC[31:16]=C200) 
    21.11=0x0180 (MAC[47:32]=0180) 
    21.12=0x6020 (FID=6, Port MAP: port5) 
    21.13=0x8000 (Entry valid) 
    21.8=0xC514 (Write hash index 514) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x6020);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC514);
    
    /*
    /BPDU to port6: MAC address=01 80 C2 00 00 00, FID=7/ 
    21.9=0x0000 (MAC[15:0]=0000) 
    21.10=0xC200 (MAC[31:16]=C200) 
    21.11=0x0180 (MAC[47:32]=0180) 
    21.12=0x7040 (FID=7, Port MAP: port6) 
    21.13=0x8000 (Entry valid) 
    21.8=0xC510 (Write hash index 510) 
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x7040);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC510);
    
    /*
    /BPDU to port7: MAC address=01 80 C2 00 00 00, FID=8/ 
    21.9=0x0000 (MAC[15:0]=0000) 
    21.10=0xC200 (MAC[31:16]=C200) 
    21.11=0x0180 (MAC[47:32]=0180) 
    21.12=0x8080 (FID=8, Port MAP: port7) 
    21.13=0x8000 (Entry valid) 
    21.8=0xC52C (Write hash index 52C)
    */
    switch_write_reg(21, 9, 0x0);
    switch_write_reg(21, 10, 0xC200);
    switch_write_reg(21, 11, 0x180);
    switch_write_reg(21, 12, 0x8080);
    switch_write_reg(21, 13, 0x8000);
    switch_write_reg(21, 8, 0xC52C);
    
    /*
    20.4=0x01FF (Enable port0~port8 forwarding) 
    20.5=0x01FE (Disable port0 learning)
    */
    switch_write_reg(20, 4, 0x1FF);
    switch_write_reg(20, 5, 0x1FE); 
    
    /* 22.0=0x01FF (Tag based VLAN: port0~port8) */
    switch_write_reg(22, 0, 0x01FF);
    
    /* 22.1=0x00FF (PVID classification: port0~port7; VID classification: port8)*/
    switch_write_reg(22, 1, 0xFF); 
    
    /*
      22.6=0x0009 (Port0 PVID=9) 
      22.7=0x000A (Port1 PVID=10) 
      22.8=0x000B (Port2 PVID=11) 
      22.9=0x000C (Port3 PVID=12) 
      22.10=0x000D (Port4 PVID=13) 
      22.11=0x000E (Port5 PVID=14) 
      22.12=0x000F (Port6 PVID=15) 
      22.13=0x0010 (Port7 PVID=16) 
      22.14=0x000D (Port8 PVID=13) 
    */
	switch_write_reg(22, 6, 0x9);
	switch_write_reg(22, 7, 0xA);
	switch_write_reg(22, 8, 0xB);
	switch_write_reg(22, 9, 0xC);
	switch_write_reg(22, 10, 0xD);
	switch_write_reg(22, 11, 0xE);
	switch_write_reg(22, 12, 0xF);
	switch_write_reg(22, 13, 0x10);
	switch_write_reg(22, 14, 0xD);
    
    /*
    22.15=0xFFFF (Enable VLAN0~15) 
    24.0=0x1001 (FID=1, VLAN0 VID=1) 
    24.1=0x2002 (FID=2, VLAN1 VID=2) 
    24.2=0x3003 (FID=3, VLAN2 VID=3) 
    24.3=0x4004 (FID=4, VLAN3 VID=4) 
    24.4=0x5005 (FID=5, VLAN4 VID=5) 
    24.5=0x6006 (FID=6, VLAN5 VID=6) 
    24.6=0x7007 (FID=7, VLAN6 VID=7) 
    24.7=0x8008 (FID=8, VLAN7 VID=8) 
    24.8=0x0009 (FID=0, VLAN8 VID=9) 
    24.9=0x000A (FID=0, VLAN9 VID=10) 
    24.10=0x000B (FID=0, VLAN10 VID=11) 
    24.11=0x000C (FID=0, VLAN11 VID=12) 
    24.12=0x000D (FID=0, VLAN12 VID=13) 
    24.13=0x000E (FID=0, VLAN13 VID=14) 
    24.14=0x000F (FID=0, VLAN14 VID=15) 
    24.15=0x0010 (FID=0, VLAN15 VID=16)
    */
	switch_write_reg(22, 15, 0xFFFF);
	switch_write_reg(24, 0, 0x1001);
	switch_write_reg(24, 1, 0x2002);
	switch_write_reg(24, 2, 0x3003);
	switch_write_reg(24, 3, 0x4004);
	switch_write_reg(24, 4, 0x5005);
	switch_write_reg(24, 5, 0x6006);
	switch_write_reg(24, 6, 0x7007);
	switch_write_reg(24, 7, 0x8008);
	switch_write_reg(24, 8, 0x9);
	switch_write_reg(24, 9, 0xA);
	switch_write_reg(24, 10, 0xB);
	switch_write_reg(24, 11, 0xC);
	switch_write_reg(24, 12, 0xD);
	switch_write_reg(24, 13, 0xE);
	switch_write_reg(24, 14, 0xF);
	switch_write_reg(24, 15, 0x10);
    
    /*
    24.24=0x0100 (VLAN8 add tag: port8) 
    24.25=0x0100 (VLAN9 add tag: port8) 
    24.26=0x0100 (VLAN10 add tag: port8) 
    24.27=0x0100 (VLAN11 add tag: port8)
    24.28=0x0100 (VLAN12 add tag: port8) 
    24.29=0x0100 (VLAN13 add tag: port8) 
    24.30=0x0100 (VLAN14 add tag: port8) 
    24.31=0x0100 (VLAN15 add tag: port8) 
    25.0=0x0001 (VLAN0 remove tag: port0) 
    25.1=0x0002 (VLAN1 remove tag: port1) 
    25.2=0x0004 (VLAN2 remove tag: port2) 
    25.3=0x0008 (VLAN3 remove tag: port3) 
    25.4=0x0010 (VLAN4 remove tag: port4) 
    25.5=0x0020 (VLAN5 remove tag: port5) 
    25.6=0x0040 (VLAN6 remove tag: port6) 
    25.7=0x0080 (VLAN7 remove tag: port7) 
    25.16=0x0101 (VLAN0 member: port0 & port8) 
    25.17=0x0102 (VLAN1 member: port1 & port8) 
    25.18=0x0104 (VLAN2 member: port2 & port8) 
    25.19=0x0108 (VLAN3 member: port3 & port8) 
    25.20=0x0110 (VLAN4 member: port4 & port8) 
    25.21=0x0120 (VLAN5 member: port5 & port8) 
    25.22=0x0140 (VLAN6 member: port6 & port8) 
    25.23=0x0180 (VLAN7 member: port7 & port8) 
    25.24=0x0101 (VLAN8 member: port0 & port8) 
    25.25=0x01FE (VLAN9 member: port1 ~ port8) 
    25.26=0x01FE (VLAN10 member: port1 ~ port8) 
    25.27=0x01FE (VLAN11 member: port1 ~ port8) 
    25.28=0x01FF (VLAN12 member: port0 ~ port8) 
    25.29=0x01FF (VLAN13 member: port0 ~ port8) 
    25.30=0x01FE (VLAN14 member: port1 ~ port8) 
    25.31=0x01FE (VLAN15 member: port1 ~ port8)
    */
	switch_write_reg(24, 24, 0x100);
	switch_write_reg(24, 25, 0x100);
	switch_write_reg(24, 26, 0x100);
	switch_write_reg(24, 27, 0x100);
	switch_write_reg(24, 28, 0x100);
	switch_write_reg(24, 29, 0x100);
	switch_write_reg(24, 30, 0x100);
	switch_write_reg(24, 31, 0x100);
	switch_write_reg(25, 0, 0x1);
	switch_write_reg(25, 1, 0x2);
	switch_write_reg(25, 2, 0x4);
	switch_write_reg(25, 3, 0x8);
	switch_write_reg(25, 4, 0x10);
	switch_write_reg(25, 5, 0x20);
	switch_write_reg(25, 6, 0x40);
	switch_write_reg(25, 7, 0x80);
	switch_write_reg(25, 16, 0x101);
	switch_write_reg(25, 17, 0x102);
	switch_write_reg(25, 18, 0x104);
	switch_write_reg(25, 19, 0x108);
	switch_write_reg(25, 20, 0x110);
	switch_write_reg(25, 21, 0x120);
	switch_write_reg(25, 22, 0x140);
	switch_write_reg(25, 23, 0x180);
	switch_write_reg(25, 24, 0x101);
	switch_write_reg(25, 25, 0x1FE);
	switch_write_reg(25, 26, 0x1FE);
	switch_write_reg(25, 27, 0x1FE);
	switch_write_reg(25, 28, 0x1FF);
	switch_write_reg(25, 29, 0x1FF);
	switch_write_reg(25, 30, 0x1FE);
	switch_write_reg(25, 31, 0x1FE);
	
#endif
    
    return;
}



